--ENF CONFIGS
--Quando queremos criar\alterar uma chave, mas caso j� exista no cliente seja para MANTER o valor
EXEC PCK_ENF_CFG_PATCH.cfg_GR_ENF_CONFIG('WF_N_DIAS_VISIVEIS','Tempos m�ximos para um episodio ser visivel no workflow de urg�ncia','10');

--Quando queremos criar\alterar uma chave, e FOR�AR o novo valor no cliente
EXEC PCK_ENF_CFG_PATCH.cfg_GR_ENF_CONFIG('WF_N_DIAS_VISIVEIS','Tempos m�ximos para um episodio ser visivel no workflow de urg�ncia','10',true)

--Quando queremos criar\alterar uma chave, indicando a lista de valores possiveis
EXEC PCK_ENF_CFG_PATCH.cfg_GR_ENF_CONFIG('WF_N_DIAS_VISIVEIS','Tempos m�ximos para um episodio ser visivel no workflow de urg�ncia','10','[10][20][30]');

--Quando queremos criar\alterar uma chave, indicando a lista de valores possiveis
EXEC PCK_ENF_CFG_PATCH.cfg_GR_ENF_CONFIG('WF_N_DIAS_VISIVEIS','Tempos m�ximos para um episodio ser visivel no workflow de urg�ncia','10','[10][20][30]',true);

--NEFRO CONFIGS
--Quando queremos criar\alterar uma chave, mas caso j� exista no cliente seja para MANTER o valor
EXEC PCK_NF_CFG_PATCH.cfg_GR_NF_CONFIG('WF_N_DIAS_VISIVEIS','Tempos m�ximos para um episodio ser visivel no workflow de urg�ncia','10');

--Quando queremos criar\alterar uma chave, e FOR�AR o novo valor no cliente
EXEC PCK_NF_CFG_PATCH.cfg_GR_NF_CONFIG('WF_N_DIAS_VISIVEIS','Tempos m�ximos para um episodio ser visivel no workflow de urg�ncia','10',true)

--Quando queremos criar\alterar uma chave, indicando a lista de valores possiveis
EXEC PCK_NF_CFG_PATCH.cfg_GR_NF_CONFIG('WF_N_DIAS_VISIVEIS','Tempos m�ximos para um episodio ser visivel no workflow de urg�ncia','10','[10][20][30]');

--Quando queremos criar\alterar uma chave, indicando a lista de valores possiveis
EXEC PCK_NF_CFG_PATCH.cfg_GR_NF_CONFIG('WF_N_DIAS_VISIVEIS','Tempos m�ximos para um episodio ser visivel no workflow de urg�ncia','10','[10][20][30]',true);


--Quando queremos criar\alterar uma LOV
DECLARE
    new_LOV_QUERY VARCHAR2(4000) :=
'SELECT q0.NIVEL,
       RPAD('' '',(q0.NIVEL-1)*3,'' '')||q1.DESCR_ITEM DESCRDICAO,
       q1.COD_ITEM
  FROM GR_ENF_ARVORE_ITEM q0 , GR_ENF_ITEM q1
 WHERE q0.COD_ARVORE = ''0088''
   AND q0.COD_ITEM = q1.COD_ITEM
 ORDER BY q0.ORDEM';
--|
BEGIN
    PCK_ENF_CFG_PATCH.cfg_GR_ENF_LOVS('arv_main_ghen0088',new_LOV_QUERY);
END;

--| DAR GRANTS
BEGIN
    btec.btecp_execute_immediate('GRANT all ON gh.obtem_estado_doente TO giss');
END;
/

ga_install_objects DOC

In order to have some stability we MUST(!) to to use the same processes to install our changes.

To do this we use ga_install_objects in GA to install EVERYTHING.

LIST:
- drop_package;
- drop_package_body;
- drop_sequence;
- drop_synonym;
- drop_table;
- drop_trigger;
- drop_view;
- drop_primary_key;
- drop_foreign_key;
- drop_unique_key;
- execute_immediate;
- execute_immediate_clob;
- create_synonym;
- create_index;
- getuserindexpressions;
- add_alter_column;
- extract_table;
- create_alter_table;
- create_sequence;
- create_primary_key;
- create_foreign_key;
- create_unique_key;

EXAMPLES:
--******************************************************************************
drop_package
--| Drops a package

	BEGIN
		ga_install_objects.drop_package('package_name_varchar');
	END;
/
--******************************************************************************
drop_package_body
--| Drops a package_body

	BEGIN
		ga_install_objects.drop_package_body('package_body_name_varchar');
	END;
/	
--******************************************************************************
drop_sequence
--| Drops a sequence

	BEGIN
		ga_install_objects.drop_sequence('sequence_name_varchar');
	END;
/
--******************************************************************************
drop_synonym
--| Drops a synonym

	BEGIN
		ga_install_objects.drop_synonym('synonym_name_varchar');
	END;
/
--******************************************************************************
drop_table
--| Drops a table

	BEGIN
		ga_install_objects.drop_table('table_name_varchar');
	END;
/	
--******************************************************************************
drop_trigger
--| Drops a trigger

	BEGIN
		ga_install_objects.drop_trigger('trigger_name_varchar');
	END;
/
--******************************************************************************
drop_view
--| Drops a view

	BEGIN
		ga_install_objects.drop_view('view_name_varchar');
	END;
/
--******************************************************************************
drop_primary_key
--| Drops a primary key

	BEGIN
		ga_install_objects.drop_primary_key('i_table_owner_varchar', 'i_table_name_varchar');
	END;
/
--******************************************************************************
drop_foreign_key
--| Drops a view

	BEGIN
		ga_install_objects.drop_foreign_key('i_table_owner_varchar', 'i_table_name_varchar', 'i_fk_name_varchar');
	END;
/
--******************************************************************************
drop_unique_key
--| Drops a view

	BEGIN
		ga_install_objects.drop_unique_key('i_table_owner_varchar', 'i_table_name_varchar', 'i_uk_name_varchar');
	END;
/
--******************************************************************************
execute_immediate
--| Execute immediate with a varchar2 input

	BEGIN
		ga_install_objects.execute_immediate('sql_text_varchar');
	END;
/
--******************************************************************************
execute_immediate_clob
--| Execute immediate with a CLOB input

	BEGIN
		ga_install_objects.execute_immediate_clob('sql_text_CLOB');
	END;
/	
--******************************************************************************
create_synonym
--| Creates a synonym

	THIS
	-- CREATE OR REPLACE SYNONYM tact.sdtv_enf_lista_de_trabalho FOR enf.sdtv_enf_lista_de_trabalho;
	
	SHOULD NOW BE DONE LIKE THIS
	
	RUNS IN TACT
	BEGIN
		ga_install_objects.create_synonym('sdtv_enf_lista_de_trabalho', 'ENF', 'sdtv_enf_lista_de_trabalho');
	END;
/	
--******************************************************************************
create_index
--| Creates an index

DECLARE
    w_colunas DBMS_SQL.varchar2a;
BEGIN
        
    w_Colunas(w_colunas.count) := 'COLUNA1';
    w_Colunas(w_colunas.count) := 'COLUNA2';
    
    ga_install_objects.create_index('table_name_varchar', 'index_name_varchar', w_Colunas);

END;
/
--******************************************************************************
add_alter_column
--| Adds a column 

BEGIN
	GA_INSTALL_OBJECTS.add_alter_column
	(
		p_table_name         => 'GR_ENF_ARVORE',
		p_column_name        => 'COLUNA_DE_TESTE',
		p_column_type        => 'VARCHAR2',
		p_column_length      => 30,
		p_column_precision   => NULL,
		p_column_scale       => NULL,
		p_nullable           => TRUE,
		p_default_value      => NULL
	);
END;
/

--******************************************************************************
create_alter_table
--| Creates a new table / If it already exists it adds the columns that are missing in order

DECLARE
    w_colunas GA_INSTALL_OBJECTS.my_columns;
BEGIN   
    --| COLUNA #1
    w_Colunas(w_colunas.count).column_name          := 'COLUNA_CODIGO';
    w_Colunas(w_colunas.count-1).column_type        := 'NUMBER';
    w_Colunas(w_colunas.count-1).column_length      := 30;
    w_Colunas(w_colunas.count-1).column_precision   := null;
    w_Colunas(w_colunas.count-1).column_scale       := null;
    w_Colunas(w_colunas.count-1).nullable           := false;
    
    --| COLUNA #2
    w_Colunas(w_colunas.count).column_name          := 'COLUNA_DESCRICAO';
    w_Colunas(w_colunas.count-1).column_type        := 'VARCHAR2';
    w_Colunas(w_colunas.count-1).column_length      := 4000;
    w_Colunas(w_colunas.count-1).column_precision   := null;
    w_Colunas(w_colunas.count-1).column_scale       := null;
    w_Colunas(w_colunas.count-1).nullable           := false;

    --| COLUNA #3
    w_Colunas(w_colunas.count).column_name          := 'COLUNA_DESCRICAO_NOVA';
    w_Colunas(w_colunas.count-1).column_type        := 'VARCHAR2';
    w_Colunas(w_colunas.count-1).column_length      := 2000;
    w_Colunas(w_colunas.count-1).column_precision   := null;
    w_Colunas(w_colunas.count-1).column_scale       := null;
    w_Colunas(w_colunas.count-1).nullable           := false;
    
    GA_INSTALL_OBJECTS.create_alter_table('ENF', 'GR_ENF_TABELA_DE_TESTE', w_colunas);
END;
/
--******************************************************************************
create_sequence
--| Creates a sequence

BEGIN
    GA_INSTALL_OBJECTS.create_sequence
    (
        p_sequence_name   => 'sequence_name_varchar',
        p_increment       => 1,
        p_start_value     => NULL,
        p_minvalue        => NULL,
        p_maxvalue        => NULL,
        p_cycle           => FALSE,
        p_order           => FALSE,
        p_cache           => 20
    );
END;
/
--******************************************************************************
create_primary_key
--| Creates a primary key

DECLARE
    w_colunas DBMS_SQL.varchar2a;
BEGIN
        
    w_Colunas(w_colunas.count) := 'column_name_1_varchar';
    w_Colunas(w_colunas.count) := 'column_name_2_varchar';  
    
    ga_install_objects.create_primary_key ('table_owner_varchar',
                                           'table_name_varchar',
                                           'pk_name_varchar',
                                           w_Colunas);
END;
/
--******************************************************************************
create_foreign_key
--| Creates a foreign key

DECLARE
	w_colunas_present DBMS_SQL.varchar2a;
	w_Colunas_ref     DBMS_SQL.varchar2a;
BEGIN
	--columns IN the table
	w_Colunas_present(w_Colunas_present.count) := 'column_1_present';
	w_Colunas_present(w_Colunas_present.count) := 'column_2_present'; 
	
	--columns that the table REFS
	w_Colunas_ref(w_Colunas_ref.count) := 'column_1_ref'; 
	w_Colunas_ref(w_Colunas_ref.count) := 'column_2_ref'; 
	
	ga_install_objects.create_foreign_key ('table_owner_varchar',
										   'table_name_varchar',
										   'fk_name_varchar',
										   w_Colunas_present,
										   'table_owner_varchar',
										   'table_name_REF_varchar',
										   w_Colunas_ref --OPCIONAL
										   );
END;
/
--******************************************************************************
create_unique_key
--| Creates a unique key

DECLARE
    w_colunas DBMS_SQL.varchar2a;
BEGIN
        
    w_Colunas(w_colunas.count) := 'column_name_1_varchar';
    w_Colunas(w_colunas.count) := 'column_name_2_varchar';  
    
    ga_install_objects.create_unique_key ('table_owner_varchar',
                                          'table_name_varchar',
                                          'uk_name_varchar',
                                          w_Colunas);
END;
/
	
--*****************************************************************************************
Script to help you extract a table to the logic above
--*****************************************************************************************
DECLARE
    W_NOME_TABELA CONSTANT VARCHAR2(30) := 'table_name_varchar';
    w_param     CLOB;
    w_tamanho   NUMBER;
    W_TAMANHO_MAX CONSTANT NUMBER := 32000;
    W_VALOR_AUX VARCHAR2(32767);
    W_OFFSET NUMBER;
BEGIN
    DBMS_OUTPUT.enable (buffer_size => NULL);
    w_param := ga_install_objects.extract_table (W_NOME_TABELA);
    w_param :=
        REPLACE (w_param,
                 '    w_Colunas(w_colunas.count).column_name',
                 CHR (10) || '    w_Colunas(w_colunas.count).column_name');
    w_param :=
        REPLACE (w_param,
                 '    ga_install_objects.create_alter_table',
                 CHR (10) || '    ga_install_objects.create_alter_table');

    LOOP
        W_VALOR_AUX := DBMS_LOB.SUBSTR (w_param, W_TAMANHO_MAX, 1);
        IF LENGTH(W_VALOR_AUX) = W_TAMANHO_MAX THEN
          W_OFFSET := INSTR(W_VALOR_AUX, CHR(10), -1);
        END IF;
        IF NVL(W_OFFSET, 0) < 1 THEN
          W_OFFSET := LENGTH(W_VALOR_AUX);
        END IF;
        W_VALOR_AUX := DBMS_LOB.SUBSTR (w_param, W_OFFSET, 1);        
        DBMS_OUTPUT.put_LINE (W_VALOR_AUX);        
        W_PARAM := SUBSTR(w_param, W_OFFSET + 1);
    EXIT WHEN NVL(LENGTH(W_PARAM),0) = 0;
    END LOOP;

    DBMS_OUTPUT.put_line ('/'||CHR(10));
END;
/