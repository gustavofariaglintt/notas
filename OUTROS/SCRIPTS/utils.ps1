﻿# $assemblySourceDEV = "C:\Users\jcampos\Desktop\DEV"
# $assemblySourceCI = "C:\Users\jcampos\Desktop\CI"
# $assemblySource17 = "C:\Users\jcampos\Desktop\17"

$assemblySourceDEV = "C:\Users\gustavo.faria\Desktop\Connection Strings\Dll's\DEV"
$assemblySourceCI = "C:\Users\gustavo.faria\Desktop\Connection Strings\Dll's\CI"
$assemblySource17 = "C:\Users\gustavo.faria\Desktop\Connection Strings\Dll's\17"

function getExternalDependenciesFoldersDEV()
{
    $folders = @(    
            "C:\ATD\ExternalDependencies\"                                           ,
            "C:\DOTNET\API\ExternalDependencies"                                     ,
            "C:\DOTNET\Common\ExternalDependencies"                                  ,
            "C:\DOTNET\InfraEstructure\ExternalDependencies"                         ,
            "C:\DOTNET\Modules\ExternalDependencies"                                 ,
            "c:\DOTNET\Modules\Html\ExternalAssemblies"                              ,
            "C:\DOTNET\Services\ExternalAssemblies"                                  ,
            "C:\DOTNET\ServicesEF_V2\ExternalDependencies"                           ,
            "C:\DOTNET\Modules\Html\Glintths.Shell.WebServices\ExternalDependencies" 
        );
    return $folders
}

function getExternalDependenciesFoldersCI()
{
    $folders = @(                                                                      ,
            "C:\DOTNET-CI\ATD\ExternalDependencies\"                                   ,
            "C:\DOTNET-CI\API\ExternalDependencies"                                    ,
            "C:\DOTNET-CI\Common\ExternalDependencies"                                 ,
            "C:\DOTNET-CI\InfraEstructure\ExternalDependencies"                        ,
            "C:\DOTNET-CI\Modules\ExternalDependencies"                                ,
            "c:\DOTNET-CI\Modules\Html\ExternalAssemblies"                             ,
            "C:\DOTNET-CI\Services\ExternalAssemblies"                                 ,
            "C:\DOTNET-CI\ServicesEF_V2\ExternalDependencies"                          ,
            "C:\DOTNET-CI\Modules\Html\Glintths.Shell.WebServices\ExternalDependencies"
        );
    return $folders
} 

function getExternalDependenciesFolders17()
{
    $folders = @(
            "C:\17R1\ATD\ExternalDependencies\"                                     ,
            "C:\17R1\API\ExternalDependencies"                                      ,
            "C:\17R1\Common\ExternalDependencies"                                   ,
            "C:\17R1\InfraEstructure\ExternalDependencies"                          ,
            "C:\17R1\Modules\ExternalDependencies"                                  ,
            "c:\17R1\Modules\Html\ExternalAssemblies"                               ,
            "C:\17R1\Services\ExternalAssemblies"                                   ,
            "C:\17R1\ServicesEF_V2\ExternalDependencies"                            ,
            "C:\17R1\Modules\Html\Glintths.Shell.WebServices\ExternalDependencies"
        );
    return $folders
} 

function tabify([int]$count)
{
    for($i = 0; $i -lt $count; $i ++)
    {        
        Write-Host -NoNewline "`t"
    }
}

function ReplaceSourceFilesInFolder($source, $target, [bool]$withTFS, $outputFolder) 
{
	$items = Get-ChildItem $source
	
	foreach ($item in $items)
	{		
		$targetPath = Join-Path $target $item.Name

        if($null -ne $outputFolder)
        {
            $outputPath = Join-Path $outputFolder $item.Name
        }
        else
        {
            $outputPath = Join-Path $target $item.Name
        }

		
		if( Test-Path $targetPath -PathType Leaf )
		{
            $sourceHash = (Get-FileHash $item.FullName).Hash
            $targetHash = (Get-FileHash $targetPath).Hash

            tabify ($level+1)

            if ($sourceHash -ne $targetHash)
            {
                Write-Host -ForegroundColor Green "+$item"
                if($withTFS) {
                    tf checkout $outputPath
                    if($?) 
                    {
                        Copy-Item $item.FullName $outputPath
                    }
                }
                else
                {
                    New-Item -ItemType File -Path $outputPath -Force
                    Copy-Item $item.FullName $outputPath -force
                }
            }
            else
            {
                Write-Host -ForegroundColor White " $item"
            }
		}
	}
}

function ReplaceFilesIfExistent($source, $target, [bool]$recursive, [bool]$withTFS, $level = 0) 
{
    $targetObj = Get-Item $target

    tabify $level
    if($level -eq 0)
    {
	    echo $targetObj.FullName
    }
    else
    {
        echo $targetObj.Name
    }
	
    if($recursive) 
    {
	    $subfolders = Get-ChildItem -directory $target
	
	    foreach ($folder in $subfolders)
	    {
		    ReplaceFilesIfExistent $source $folder.FullName $recursive $withTFS ($level+1)	
	    }
    }
	
	ReplaceSourceFilesInFolder $source $target $withTFS
}



function getFromTFSForced($folders)
{
    foreach ($folder in $folders)
    {
        tf get $folder -recursive -force
    }
}

function getFromTFSForcedDEV()
{
    $folders = getExternalDependenciesFoldersDEV
    getFromTFSForced $folders
}

function getFromTFSForcedCI()
{
    $folders = getExternalDependenciesFoldersCI
    getFromTFSForced $folders
}

function getFromTFSForced17()
{
    $folders = getExternalDependenciesFolders17
    getFromTFSForced $folders
}


function replaceTFSFilesDEV()
{
    $folders = getExternalDependenciesFoldersDEV

    foreach ($folder in $folders)
    {
        ReplaceFilesIfExistent $assemblySourceDEV $folder $true  $true
    }
}

function replaceTFSFilesCI()
{
    $folders = getExternalDependenciesFoldersCI

    foreach ($folder in $folders)
    {
        ReplaceFilesIfExistent $assemblySourceCI $folder $true  $true
    }
}

function replaceTFSFiles17()
{
    $folders = getExternalDependenciesFolders17

    foreach ($folder in $folders)
    {
        ReplaceFilesIfExistent $assemblySource17 $folder $true  $true
    }
}


function openWebconfigs()
{
    $folders = @(                                     
        "Cpchs.Modules.ActivitiesManagement.WS"           ,
        "Cpchs.Modules.Base.WebServices"                  ,
        "Cpchs.Modules.BillingManagement.WS"              ,
        "Cpchs.Modules.CardiologyManagement.WS"           ,
        "Cpchs.Modules.ClinicalProcess.WebServices"       ,
        "Cpchs.Modules.Codifications.WebServices"         ,
        "Cpchs.Modules.DocumentManagement.WS"             ,
        "Cpchs.Modules.Entities.WebServices"              ,
        "Cpchs.Modules.Forms.WebServices"                 ,
        "Cpchs.Modules.InterfacesManagement.WebServices"  ,
        "Cpchs.Modules.LoggingManagement.WebServices"     ,
        "Cpchs.Modules.MCDTS.WebServices"                 ,
        "Cpchs.Modules.Pharmacy.WebServices"              ,
        "Cpchs.Modules.ReportsManagement.WS"              ,
        "Cpchs.Modules.TrackingManagement.WS"             ,
        "Cpchs.Modules.TrackingManagement.WS.ER"          ,
        "Cpchs.Modules.UpdateManagement.WebServices"      ,
        "Cpchs.Modules.UrgencyManagement.WS"              ,
        "Glintths.External.CIT.WebServices"               ,
        "Glintths.External.SINAVE.WebServices"            ,
        "GlinttHS.Infrastructure.BaseUtilitiesMng.WS"     ,
        "Glintths.Interfaces.ITSector.WebServices"        ,
        "Glintths.Modules.CallPanelsManagement.WS"        ,
        "Glintths.Modules.CommandsInvoke.WS"              ,
        "Glintths.Modules.CommonReports.WebServices"      ,
        "Glintths.Modules.CommonReportsRDLC.WebServices"  ,
        "Glintths.Modules.DataManagement.WebServices"     ,
        "Glintths.Modules.ExternalInterops.WebServices"   ,
        "Glintths.Modules.PrintForms.WebServices"         ,
        "Glintths.Modules.Protocols.WebServices"          ,
        "Glintths.Modules.Providers.WebServices"          ,
        "Glintths.Modules.Questionnaire.WebServices"      ,
        "Glintths.Modules.Subscriptions.WebServices"      ,
        "Glintths.Modules.ToxLab.WebServices"             ,
        "Glintths.Param.WebServices"                      ,
        "Glintths.Reports.WebServices"                    );

    $target = "\\demoapliis.glinttocm.com\GIIS\CI\GPLATFORM";

    foreach ($item in $folders)
	{		
		$targetPath = Join-Path $target $item
        $targetPath = Join-Path $targetPath "web.config"

        if( Test-Path $targetPath -PathType Leaf )
		{
            Start-Process $targetPath
        }
        else
        {
            echo "Missing: $targetPath"
        }
    }
}


function replaceDemoAplCI() 
{
    $target = "\\demoapliis.glinttocm.com\GIIS\CI\GPLATFORM";

    $folders = @(
        "Cpchs.Modules.ActivitiesManagement.WS"          ,
        "Cpchs.Modules.Base.WebServices"                 ,
        "Cpchs.Modules.BillingManagement.WS"             ,
        "Cpchs.Modules.CardiologyManagement.WS"          ,
        "Cpchs.Modules.ClinicalProcess.WebServices"      ,
        "Cpchs.Modules.Codifications.WebServices"        ,
        "Cpchs.Modules.DocumentManagement.WS"            ,
        "Cpchs.Modules.Entities.WebServices"             ,
        "Cpchs.Modules.Forms.WebServices"                ,
        "Cpchs.Modules.InterfacesManagement.WebServices" ,
        "Cpchs.Modules.LoggingManagement.WebServices"    ,
        "Cpchs.Modules.MCDTS.WebServices"                ,
        "Cpchs.Modules.Pharmacy.WebServices"             ,
        "Cpchs.Modules.ReportsManagement.WS"             ,
        "Cpchs.Modules.TrackingManagement.WS"            ,
        "Cpchs.Modules.UpdateManagement.WebServices"     ,
        "Cpchs.Modules.UrgencyManagement.WS"             ,
        "Glintths.External.CIT.WebServices"              ,
        "Glintths.External.SINAVE.WebServices"           ,
        "GlinttHS.Infrastructure.BaseUtilitiesMng.WS"    ,
        "Glintths.Interfaces.ITSector.WebServices"       ,
        "Glintths.Modules.CallPanelsManagement.WS"       ,
        "Glintths.Modules.CommandsInvoke.WS"             ,
        "Glintths.Modules.CommonReports.WebServices"     ,
        "Glintths.Modules.CommonReportsRDLC.WebServices" ,
        "Glintths.Modules.DataManagement.WebServices"    ,
        "Glintths.Modules.ExternalInterops.WebServices"  ,
        "Glintths.Modules.PrintForms.WebServices"        ,
        "Glintths.Modules.Protocols.WebServices"         ,
        "Glintths.Modules.Providers.WebServices"         ,
        "Glintths.Modules.Questionnaire.WebServices"     ,
        "Glintths.Modules.Subscriptions.WebServices"     ,
        "Glintths.Modules.ToxLab.WebServices"            ,
        "Glintths.Param.WebServices"                     ,
        "Glintths.Reports.WebServices"                   ,
        "Security.P3.WS.DOTNET"                          ,
        "Security.P3.WS.GD"                              ,
        "GlinttHS.Infrastructure.BaseUtilitiesMng.WS"    ,       
        "GlinttHS.API.Internal.WS"                       ,                         
        "Glintths.Authorization.Providers.WebServices"
    );

    foreach ($item in $folders)
	{
        $targetPath = Join-Path $target $item
        if( Test-Path $targetPath )
        {
            ReplaceFilesIfExistent $assemblySourceCI $targetPath $true $false 
        }
    }
}


function DiffFilesIfExistent($sourceFolder, $targetFolder, $outputFolder)
{
    $subfolders = Get-ChildItem -directory $targetFolder
	
	foreach ($folder in $subfolders)
	{
        $childOutputFolder = Join-Path $outputFolder $folder.Name

		DiffFilesIfExistent $sourceFolder $folder.FullName $childOutputFolder
	}

    # HERE
	ReplaceSourceFilesInFolder $sourceFolder $targetFolder $false $outputFolder
	# ReplaceSourceFilesInFolder $sourceFolder $targetFolder $false $null
}


function DiffATD() 
{
    DiffFilesIfExistent $assemblySource17 "\\demoapliis.glinttocm.com\GIIS\17R101\ATENDIMENTO_WEB\" "C:\temp\diff"
}

function DiffPEM() 
{
    DiffFilesIfExistent $assemblySource17 "\\demoapliis.glinttocm.com\GIIS\17R101\PEM\" "C:\temp\diff\PEM"
}

function DiffPIM() 
{
    DiffFilesIfExistent $assemblySource17 "\\demoapliis.glinttocm.com\GIIS\17R101\PIM\" "C:\temp\diff\PIM"
}

function DiffTest() {
    # DiffFilesIfExistent $assemblySourceCI "\\demoapliis.glinttocm.com\GIIS\CI\GPLATFORM\GlinttHS.Infrastructure.BaseUtilitiesMng.WS\bin" "C:\Users\gustavo.faria\Desktop\diff"
    DiffFilesIfExistent $assemblySource17 "C:\Users\gustavo.faria\Desktop\patch\17_old" "C:\Users\gustavo.faria\Desktop\patch\diffs"
}

function diffs($source, $target) {
    DiffFilesIfExistent $assemblySource17 $source $target
}


# DiffATD
# diffs "C:\Users\gustavo.faria\Desktop\patch\17_old" "C:\Users\gustavo.faria\Desktop\patch\diffs"
DiffTest

<#
getFromTFSForcedDEV
getFromTFSForcedCI
getFromTFSForced17
replaceTFSFilesDEV
replaceTFSFilesCI
replaceTFSFiles17

#>

#openWebconfigs