param (
    [string]$mode = "enable"
 )

 if ($mode -eq "enable") {

    if( Test-Path ".\connectionstrings.config" -PathType Leaf ) {
        Rename-Item -Path ".\connectionstrings.config" -NewName "--ConnectionStrings.config";        
    } else {
        Write-Output "No 'ConnectionStrings.config' file"
    }

    if( Test-Path ".\bin" -PathType Container ) {
        Rename-Item -Path ".\bin" -NewName "oldbin";    
    } else {
        Write-Output "No 'bin' folder"
    }

    if( Test-Path ".\newbin" -PathType Container ) {
        Rename-Item -Path ".\newbin" -NewName "bin";
    } else {
        Write-Output "No 'newbin' folder"
    }
 } elseif ($mode -eq "disable"){
     
    if( Test-Path ".\--connectionstrings.config" -PathType Leaf ) {
        Rename-Item -Path ".\--connectionstrings.config" -NewName "ConnectionStrings.config";        
    } else {
        Write-Output "No '--ConnectionStrings.config' file"
    }

    if( Test-Path ".\bin" -PathType Container ) {
        Rename-Item -Path ".\bin" -NewName "newbin";    
    } else {
        Write-Output "No 'bin' folder"
    }
    
    if( Test-Path ".\oldbin" -PathType Container ) {
        Rename-Item -Path ".\oldbin" -NewName "bin";
    } else {
        Write-Output "No 'oldbin' folder"
    }
 } else {
     Write-Output "Usage: .\sd.ps1 -mode <[enable]|disable>"
 }
