$base_configs = "000_BaseConfigs"

function listDirFolders($target) {
    $folders = Get-ChildItem $target | 
        Where-Object {$_.PSIsContainer} | 
        Foreach-Object {$_.Name}
    
    return $folders;
}

function listDirFiles($target) {
    $folders = Get-ChildItem $target | 
        Where-Object {!$_.PSIsContainer} | 
        Foreach-Object {$_.Name}
    
    return $folders;
}

function listDir($target) {
    $folders = Get-ChildItem $target | Foreach-Object {$_.Name}
    
    return $folders;
}

function initEmptyFolders($target) {
    $folders = listDirFolders $target

    $targetPath = Join-Path $target $folders[0]

    foreach ($item in $folders[1..$folders.Count]) {
        New-Item -Path $targetPath -Name $item -ItemType "directory"
    }
}

function deleteAllButBin($target) {
    Write-Host "Deleting contents: $target" -ForegroundColor green
    foreach ($item in listDir $target ) {
        if ($item -ne "bin"){
            Write-Host "`tDeleting $item"
            $item = Join-Path $target $item
            Remove-Item $item -Recurse
        }
    }
}

function deleteAllButBinMult($target) {
    foreach ($item in listDirFolders $target) {
        if ($item -ne $base_configs) {
            $item = Join-Path $target $item
            deleteAllButBin $item
        }
        
    }
}
# "\\demoapliis.glinttocm.com\GIIS\CI\GPLATFORM"

function keepOnlyWebConfig($target) {
    foreach ($item in listDir $target) {
        if ($item -ne "web.config") {
            $item = Join-Path $target $item
            Remove-Item $item -Recurse
        }
    }
}

function keepOnlyWebConfigs($target) {
    foreach ($item in listDir $target)
    {       
        $targetPath = Join-Path $target $item
        keepOnlyWebConfig $targetPath
    }
}

# initEmptyFolders "C:\Users\gustavo.faria\Desktop\patch\17_patch"
# deleteAllButBin "C:\Users\gustavo.faria\Desktop\patch\17_patch\Cpchs.Documents.WCF.Host"
# deleteAllButBinMult "C:\Users\gustavo.faria\Desktop\patch\17_patch"
# diffs "C:\Users\gustavo.faria\Desktop\patch\17_old", "C:\Users\gustavo.faria\Desktop\patch\diffs"
# keepOnlyWebConfig "C:\Users\gustavo.faria\Desktop\patch\patch_final\000_BaseConfigs\Cpchs.Modules.ActivitiesManagement.WS"
keepOnlyWebConfigs "C:\Users\gustavo.faria\Desktop\patch\patch_final\000_BaseConfigs"