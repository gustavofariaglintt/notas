param (
    [string]$source = $null,
    [string]$target = $null,
    [string]$outputFolder = $null,
    [switch]$help
 )
function DiffFilesIfExistent($sourceFolder, $targetFolder, $outputFolder)
{
    $subfolders = Get-ChildItem -directory $targetFolder
	
	foreach ($folder in $subfolders)
	{
        $childOutputFolder = Join-Path $outputFolder $folder.Name

		DiffFilesIfExistent $sourceFolder $folder.FullName $childOutputFolder
	}

	ReplaceSourceFilesInFolder $sourceFolder $targetFolder $false $outputFolder
}

function ReplaceSourceFilesInFolder($source, $target, [bool]$withTFS, $outputFolder) 
{
	$items = Get-ChildItem $source
	
	foreach ($item in $items)
	{		
		$targetPath = Join-Path $target $item.Name

        if($null -ne $outputFolder)
        {
            $outputPath = Join-Path $outputFolder $item.Name
        }
        else
        {
            $outputPath = Join-Path $target $item.Name
        }

		
		if( Test-Path $targetPath -PathType Leaf )
		{
            $sourceHash = (Get-FileHash $item.FullName).Hash
            $targetHash = (Get-FileHash $targetPath).Hash

            tabify ($level+1)

            if ($sourceHash -ne $targetHash)
            {
                Write-Host -ForegroundColor Green "+$item"
                if($withTFS) {
                    tf checkout $outputPath
                    if($?) 
                    {
                        Copy-Item $item.FullName $outputPath
                    }
                }
                else
                {
                    New-Item -ItemType File -Path $outputPath -Force
                    Copy-Item $item.FullName $outputPath -force
                }
            }
            else
            {
                Write-Host -ForegroundColor White " $item"
            }
		}
	}
}

# TODO

# carregar as 3 variaveis de entrada
# confirmar que as 3 são pastas e que existem
# criar pasta de output caso não exista
# chamar DiffFilesIfExistent
# done