
function Add_Xml_Node($fileName) {
    $xmlDoc = [System.Xml.XmlDocument](Get-Content $fileName);

    $newXmlAddElement = $xmlDoc.CreateElement("add");
    $newXmlAdd = $xmlDoc.connectionStrings.AppendChild($newXmlAddElement);
    $newXmlAdd.SetAttribute("name", "SERVICE_DISCOVERY_CPCHS_LOCAL_HSDEV");
    $newXmlAdd.SetAttribute("providerName", "Oracle.DataAccess.Client");
    $newXmlAdd.SetAttribute("connectionString", "DATA SOURCE = (DESCRIPTION = (ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.250.39.19)(PORT = 1521)))(CONNECT_DATA = (SERVICE_NAME = HSDEV))); PASSWORD = GPLATFORM; USER ID = GPLATFORM");
    $xmlDoc.Save($fileName);

    return
}

$path = "C:\CI_TESTE\GPLATFORM";

$folders = Get-ChildItem $path | Where-Object {$_.PSIsContainer} | Foreach-Object {$_.Name};

$i = 0;
foreach ($item in $folders) {
    $tmpPath = Join-Path $path $item;
    $targetPath = Join-Path $tmpPath "ConnectionStrings.config";

    if( Test-Path $targetPath -PathType Leaf ) {
        Add_Xml_Node $targetPath;
    } else {
        $i ++
        Write-Output "Missing: $targetPath - $i";
    }
}