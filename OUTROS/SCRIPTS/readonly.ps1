param (
    [string]$folderpath = $null,
    [string]$desiredParent = "WebService",
    [string]$desiredLeaf = "Web.Config",
    [switch]$test,
    [switch]$revert,
    [switch]$help
 )

 # displays command-line tool usage information
function help() {
    Write-Host "`n`n'Command line utility options:"

    Write-Host "`n`t-folderpath <C:\path\to\folder>"
    Write-Host "`t`tAbsolute path for the root directory."
    Write-Host "`t`tDefaults to the current working directory."

    Write-Host "`n`t-desiredParent <folder>"
    Write-Host "`t`tName of the folder where the desired file resides."
    Write-Host "`t`tDefaults to 'Glinths.Shell'."

    Write-Host "`n`t-desiredLeaf <file.extension>"
    Write-Host "`t`tName of the file to update."
    Write-Host "`t`tDefaults to 'Web.Config'."

    Write-Host "`n`t-revert"
    Write-Host "`t`tIf specified, returns the file to its original state."

    Write-Host "`n`t-test"
    Write-Host "`t`tIf specified, allows for the discovery of updatable files without performing any changes.`n`n"
}

# loads $filepath into a traversable DOM tree
function readXML($filepath) {

    if( Test-Path $filepath -PathType Leaf ) {
        [xml]$ret = Get-Content $filepath;
        return $ret;

    } else {
        Write-Host "File $($filepath) not found" -foregroundColor Red;
        return -1;
    }
}

# updates 'SessionState' node to the desired state
function updateSessionState($filepath) {
    $xml = readXML $filepath;
    $sessionstate = $xml.configuration.'system.web'.sessionstate;

    if ($null -ne $sessionstate) {
        # delete old 'sessionState' node
        $xml.configuration.'system.web'.removechild($sessionstate) | Out-Null;
    } else {
        Write-Host "$($filepath) has no 'sessionState' node" -foregroundColor Yellow;
        return;
    }

    # create new, empty, 'sessionState' node
    $node = $xml.createElement("sessionState");
    $sessionstate = $xml.configuration.'system.web'.appendChild($node);

    if (-Not $revert) {
        # Set required attributes
        $sessionstate.setattribute("mode", "StateServer");
        $sessionstate.setattribute("stateConnectionString", "tcpip=localhost:42424");
    } else {
        # set providers child properties
        $providers = $xml.createElement("providers");
        $add = $xml.createElement("add");
        $add.setAttribute("name", "MyOracleSessionStateStore");
        $add.setAttribute("type", "Oracle.Web.SessionState.OracleSessionStateStore, Oracle.Web, Version=2.111.6.20, Culture=neutral, PublicKeyToken=89b483f429c47342");
        $add.setAttribute("connectionStringName", "oracle_sessionstate");
        $providers.appendChild($add);
        $sessionstate.appendChild($providers);

        # Set sessionState attributes
        $sessionstate.setAttribute("mode", "Custom");
        $sessionstate.setAttribute("customProvider", "MyOracleSessionStateStore");
        
    }
    
    # common attributes
    $sessionstate.setAttribute("timeout", "2880");
    $sessionstate.setAttribute("cookieless", "false");
    

    # update file content
    $xml.save($filepath);

    Write-Host "$($filepath) successfully updated" -foregroundColor Green;

    return;
}

# recursively navigates through every directory and file within $basepath
# in order to find files named 'Web.Config' that reside inside a directory named 'Glintths.Shell'
function getShellWebConfigs($basepath) {
    # operator '@()' is used in order to force the variable to hold an array
    # (for the cases where there is only at most one subfolder or file)
    $subfolders = @(Get-ChildItem -directory $basepath);
    $files = @(Get-ChildItem -file $basepath);
    $parentName = Split-Path -Path $basepath -Leaf;

    # only iterates through the leafs if parent folder name is the same as the desired one
    # (Glintths.Shell at the time of writing)
    if ($parentName -eq $desiredParent) {
        foreach ($file in $files) {
            # checks if file is named correctly before advancing
            if ($file.name -like "*.svc" -Or $file.name -like "*.asmx"){
                Write-Host "Found one: $($file.fullname)" -ForegroundColor Green;

                # script can be run with a 'test' mode that doesn't alter files
                if (-Not $test) {
                    updateSessionState $file.fullname | Out-Null; 
                }
            }
        }
    }

    foreach ($folder in $subfolders) {
        # recursively navigates through the remaining folders
        getShellWebConfigs $folder.fullName;
    }

    return;
}

# displays help information if the switch is set
if ($help) {
    help;
    return;
}

# $folderpath defaults to the current working directory if it is not set by the user
if ([string]::IsNullOrEmpty($folderpath)) {
    $folderpath = Get-Location;
}

getShellWebConfigs $folderpath
