BEGIN
  gplatform_patch.ginf_config_metadata_vpd (
      i_context             => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                 => 'S_MCDT_CODIFICATIONS',
      i_description         => 'Service mcdt',
      i_configgroup         => 'Modules',
      i_allowedvalues       => '',
      i_allowedvaluestype   => '',
      i_required            => 'S',
      i_allowfacility       => 'N',
      i_requirefacility     => 'N',
      i_requireoverride     => 'N',
      i_allowmultiple       => 'N',
      i_version             => '1',
      i_hasresource         => 'N',
      i_search_value        => 'N',
      i_hasextrainfo        => 'N',
      i_extrainfoconfig     => '');
END;
/

BEGIN
  gplatform_patch.ginf_config_with_resource_vpd (
      i_context                => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                    => 'S_MCDT_CODIFICATIONS',
      i_system                 => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_new_system             => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_value                  => '#BASE_URL#Glintths.Mcdt.Codifications.Host/CodificationsManagementWs.svc',
      i_new_value              => '#BASE_URL#Glintths.Mcdt.Codifications.Host/CodificationsManagementWs.svc',
      i_smartkey               => 'S',
      i_isexternal             => 'N',
      i_externalendpoint       => '',
      i_version                => '1',
      i_position               => '',
      i_is_base_config         => 'S',
      i_extrainfo              => '',
      i_resource_domain        => NULL,
      i_resource_culture       => NULL,
      i_resource_text          => NULL,
      i_resource_search_text   => NULL);
END;
/

BEGIN
  gplatform_patch.ginf_config_metadata_vpd (
      i_context             => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                 => 'S_MCDT_ENTITIES',
      i_description         => 'Service mcdt',
      i_configgroup         => 'Modules',
      i_allowedvalues       => '',
      i_allowedvaluestype   => '',
      i_required            => 'S',
      i_allowfacility       => 'N',
      i_requirefacility     => 'N',
      i_requireoverride     => 'N',
      i_allowmultiple       => 'N',
      i_version             => '1',
      i_hasresource         => 'N',
      i_search_value        => 'N',
      i_hasextrainfo        => 'N',
      i_extrainfoconfig     => '');
END;
/

BEGIN
  gplatform_patch.ginf_config_with_resource_vpd (
      i_context                => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                    => 'S_MCDT_ENTITIES',
      i_system                 => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_new_system             => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_value                  => '#BASE_URL#Glintths.Mcdt.Entities.Host/EntitiesManagementWs.svc',
      i_new_value              => '#BASE_URL#Glintths.Mcdt.Entities.Host/EntitiesManagementWs.svc',
      i_smartkey               => 'S',
      i_isexternal             => 'N',
      i_externalendpoint       => '',
      i_version                => '1',
      i_position               => '',
      i_is_base_config         => 'S',
      i_extrainfo              => '',
      i_resource_domain        => NULL,
      i_resource_culture       => NULL,
      i_resource_text          => NULL,
      i_resource_search_text   => NULL);
END;
/

BEGIN
  gplatform_patch.ginf_config_metadata_vpd (
      i_context             => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                 => 'S_DFORMS_MANAGEMENT',
      i_description         => 'Service mcdt',
      i_configgroup         => 'Modules',
      i_allowedvalues       => '',
      i_allowedvaluestype   => '',
      i_required            => 'S',
      i_allowfacility       => 'N',
      i_requirefacility     => 'N',
      i_requireoverride     => 'N',
      i_allowmultiple       => 'N',
      i_version             => '1',
      i_hasresource         => 'N',
      i_search_value        => 'N',
      i_hasextrainfo        => 'N',
      i_extrainfoconfig     => '');
END;
/

BEGIN
  gplatform_patch.ginf_config_with_resource_vpd (
      i_context                => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                    => 'S_DFORMS_MANAGEMENT',
      i_system                 => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_new_system             => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_value                  => '#BASE_URL#Glintths.DF.Webservices/DForms/WebService/DFormsManagement.svc',
      i_new_value              => '#BASE_URL#Glintths.DF.Webservices/DForms/WebService/DFormsManagement.svc',
      i_smartkey               => 'S',
      i_isexternal             => 'N',
      i_externalendpoint       => '',
      i_version                => '1',
      i_position               => '',
      i_is_base_config         => 'S',
      i_extrainfo              => '',
      i_resource_domain        => NULL,
      i_resource_culture       => NULL,
      i_resource_text          => NULL,
      i_resource_search_text   => NULL);
END;
/

BEGIN
  gplatform_patch.ginf_config_metadata_vpd (
      i_context             => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                 => 'S_DFORMS_INFRASTRUCTURE',
      i_description         => 'Service mcdt',
      i_configgroup         => 'Modules',
      i_allowedvalues       => '',
      i_allowedvaluestype   => '',
      i_required            => 'S',
      i_allowfacility       => 'N',
      i_requirefacility     => 'N',
      i_requireoverride     => 'N',
      i_allowmultiple       => 'N',
      i_version             => '1',
      i_hasresource         => 'N',
      i_search_value        => 'N',
      i_hasextrainfo        => 'N',
      i_extrainfoconfig     => '');
END;
/

BEGIN
  gplatform_patch.ginf_config_with_resource_vpd (
      i_context                => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                    => 'S_DFORMS_INFRASTRUCTURE',
      i_system                 => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_new_system             => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_value                  => '#BASE_URL#GlinttHS.API.Internal.WS/Infrastructure/InfrastructureServices/InfrastructureServicesService.svc',
      i_new_value              => '#BASE_URL#GlinttHS.API.Internal.WS/Infrastructure/InfrastructureServices/InfrastructureServicesService.svc',
      i_smartkey               => 'S',
      i_isexternal             => 'N',
      i_externalendpoint       => '',
      i_version                => '1',
      i_position               => '',
      i_is_base_config         => 'S',
      i_extrainfo              => '',
      i_resource_domain        => NULL,
      i_resource_culture       => NULL,
      i_resource_text          => NULL,
      i_resource_search_text   => NULL);
END;
/

BEGIN
  gplatform_patch.ginf_config_metadata_vpd (
      i_context             => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                 => 'S_DFORMS_TOKEN',
      i_description         => 'Service mcdt',
      i_configgroup         => 'Modules',
      i_allowedvalues       => '',
      i_allowedvaluestype   => '',
      i_required            => 'S',
      i_allowfacility       => 'N',
      i_requirefacility     => 'N',
      i_requireoverride     => 'N',
      i_allowmultiple       => 'N',
      i_version             => '1',
      i_hasresource         => 'N',
      i_search_value        => 'N',
      i_hasextrainfo        => 'N',
      i_extrainfoconfig     => '');
END;
/

BEGIN
  gplatform_patch.ginf_config_with_resource_vpd (
      i_context                => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                    => 'S_DFORMS_TOKEN',
      i_system                 => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_new_system             => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_value                  => '#BASE_URL#Security.P3.WS.GD/Token/WebService/TokenManagement.svc',
      i_new_value              => '#BASE_URL#Security.P3.WS.GD/Token/WebService/TokenManagement.svc',
      i_smartkey               => 'S',
      i_isexternal             => 'N',
      i_externalendpoint       => '',
      i_version                => '1',
      i_position               => '',
      i_is_base_config         => 'S',
      i_extrainfo              => '',
      i_resource_domain        => NULL,
      i_resource_culture       => NULL,
      i_resource_text          => NULL,
      i_resource_search_text   => NULL);
END;
/

BEGIN
  gplatform_patch.ginf_config_metadata_vpd (
      i_context             => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                 => 'S_DFORMS_INVOKER',
      i_description         => 'Service mcdt',
      i_configgroup         => 'Modules',
      i_allowedvalues       => '',
      i_allowedvaluestype   => '',
      i_required            => 'S',
      i_allowfacility       => 'N',
      i_requirefacility     => 'N',
      i_requireoverride     => 'N',
      i_allowmultiple       => 'N',
      i_version             => '1',
      i_hasresource         => 'N',
      i_search_value        => 'N',
      i_hasextrainfo        => 'N',
      i_extrainfoconfig     => '');
END;
/

BEGIN
  gplatform_patch.ginf_config_with_resource_vpd (
      i_context                => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                    => 'S_DFORMS_INVOKER',
      i_system                 => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_new_system             => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_value                  => '#BASE_URL#Glintths.DF.WebServices.Invoker/Invoker.svc',
      i_new_value              => '#BASE_URL#Glintths.DF.WebServices.Invoker/Invoker.svc',
      i_smartkey               => 'S',
      i_isexternal             => 'N',
      i_externalendpoint       => '',
      i_version                => '1',
      i_position               => '',
      i_is_base_config         => 'S',
      i_extrainfo              => '',
      i_resource_domain        => NULL,
      i_resource_culture       => NULL,
      i_resource_text          => NULL,
      i_resource_search_text   => NULL);
END;
/