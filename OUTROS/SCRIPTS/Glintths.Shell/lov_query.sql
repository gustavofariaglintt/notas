SELECT *
  FROM gr_enf_lovs
 WHERE lov_query not like '%from gr_enf_parametrizacoes%';
-----------------------------------------------------------

SELECT *
  FROM gr_enf_lovs
 WHERE cod_lov = 'ap_ini_choro_neo';

--gr_enf_parametrizacoes é a tabela onde tenho que inserir as linhas
SELECT *
  FROM gr_enf_parametrizacoes
 WHERE agrupador = 'ap_ini_choro_neo';
 
 begin
    select lov_query
    into dyn_query
    from gr_enf_lovs
    where cod_lov = 'ap_ini_choro_neo';
    
    execute immediate dyn_query;
 end;
 
EXECUTE immediate (SELECT lov_query FROM gr_enf_lovs WHERE cod_lov = 'ap_ini_choro_neo');

SELECT codigo, descricao
  FROM ( (SELECT '1' codigo, 'ausente' descricao
            FROM DUAL)
        UNION
        (SELECT '2' codigo, 'fraco' descricao
           FROM DUAL)
        UNION
        (SELECT '3' codigo, 'normal' descricao
           FROM DUAL)
        UNION
        (SELECT '4' codigo, 'excessivo' descricao
           FROM DUAL))
ORDER BY codigo;

DECLARE
    CURSOR c1
    IS
        SELECT codigo, descricao
          FROM ( (SELECT '1' codigo,
                         ' 0- NÃO (O doente não caiu nos últimos 3 meses)'
                             descricao
                    FROM DUAL)
                UNION
                (SELECT '2' codigo,
                        '25- SIM (registo de queda fis. no actual intern. ou existe hist prévia de queda imed. antes do intern)'
                            descricao
                   FROM DUAL))
        ORDER BY TO_NUMBER (codigo);
BEGIN
    FOR c IN c1
    LOOP
        DBMS_OUTPUT.put_line (c.codigo);
    END LOOP;
END;

---------------------------------------------------------
---------------------------------------------------------
---------------------------------------------------------


DECLARE
    v_query       VARCHAR2 (2000);
    cursor_id     NUMBER;
    v_codigo      VARCHAR2 (50);
    v_descricao   VARCHAR2 (50);
    v_result      NUMBER;
    v_table       VARCHAR2 (50);
BEGIN
    v_table := 'ap_ini_choro_neo';

    SELECT lov_query
      INTO v_query
      FROM gr_enf_lovs
     WHERE cod_lov = v_table AND ROWNUM = 1;

    cursor_id := DBMS_SQL.open_cursor;

    DBMS_SQL.parse (cursor_id, v_query, DBMS_SQL.native);

    DBMS_SQL.define_column (cursor_id, 1, v_codigo, 50);
    DBMS_SQL.define_column (cursor_id, 2, v_descricao, 50);

    v_result := DBMS_SQL.execute (cursor_id);

    LOOP
        IF DBMS_SQL.fetch_rows (cursor_id) = 0
        THEN
            EXIT;
        ELSE
            DBMS_SQL.COLUMN_VALUE (cursor_id, 1, v_codigo);
            DBMS_SQL.COLUMN_VALUE (cursor_id, 2, v_descricao);

            IF v_descricao NOT IN (select * from )
            DBMS_OUTPUT.put_line ('Código: ' || v_codigo || ' || Descrição: ' || v_descricao);
        END IF;
    END LOOP;

    DBMS_SQL.close_cursor (cursor_id);
END;
/