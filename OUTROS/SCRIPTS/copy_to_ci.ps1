##
## Includes
##
. '.\buildVS.ps1'

##
## Constants
##
$components = 
    "BusinessEntities",
    "BusinessRules",
    "WebServices";
$fileTypes =
    "dll",
    "pdb";
$toWSpath = "C:\DOTNET-CI\Services\WebServices\Cpchs.Modules.ActivitiesManagement.WebServices\bin";
$path = "C:\DOTNET-CI\Services"
$firefox = "C:\Program Files\Mozilla Firefox\firefox.exe"

##
## Functions
##
function buildSourcePath ($component) {   
    return "C:\DOTNET\Services\$($component)\Cpchs.Modules.ActivitiesManagement.$($component)\obj\Debug\Cpchs.Modules.ActivitiesManagement.$($component).";
}

function buildDestinationPath ($component) {
    return (Join-Path $toWSpath "Cpchs.Modules.ActivitiesManagement.$($component).");
}

##
## Main
##
foreach ($component in $components) {
    # build projects
    # buildVS "$($path)\$($component)\Glintths.$($component).sln"

    # copy assemblies to WS
    foreach ($fileType in $fileTypes) {
        $sourceFile = buildSourcePath $component;
        $destinationFile = buildDestinationPath $component;
        Copy-Item -Path "$($sourceFile)$($filetype)" -Destination "$($destinationFile)$($filetype)";
    }
    Write-Host "Successfuly published $($component)" -foregroundColor Green;
}

$base = "C:\DOTNET\Services\BusinessEntities\Cpchs.Modules.Base.BusinessEntities\obj\Debug\";
$baseFile = "Cpchs.Modules.Base.BusinessEntities.";
foreach ($fileType in $fileTypes) {
    Copy-Item -Path "$($base)$($baseFile)$($fileType)" -Destination "$($toWSpath)\$($baseFile)$($fileType)";
}
Write-Host "Successfuly published Base BE" -foregroundColor Green;

# browse
# . $firefox "http://localhost:6002/GPLATFORM/Cpchs.Modules.ActivitiesManagement.WS/Transference/WebService/TransferenceManagement.svc"