# Código de Produto

1. Abrir SQL Navigator
   1. DB: DEMOQPR (p.ex.)
   2. Schema: GPALTFORM
   3. Pass: GPLATFORM
2. Informação de cada produto pode ser obtida correndo o seguinte query:

``` SQL
SELECT product,
       product_code,
       release_date,
       release_year,
       release_number,
       minor_release,
       patch,
       build,
       installed_version,
       install_date,
       who_installed,
       user_cri,
       dt_cri,
       user_act,
       dt_act,
       gcx_installed_version
  FROM ga_patch_instalacao
 WHERE (product_code, seq_patch_instalacao) IN
           (SELECT product_code, MAX (seq_patch_instalacao)
              FROM ga_patch_instalacao
            GROUP BY product_code)
ORDER BY product, product_code;
```
