# Guia de Associação a Patch quando há alterações a código da base de dados

1. Navegar até C:\\SVN\\MicrosoftID\\{Produto}\\ReleaseAberta
2. Botão direito no ficheiro {pck_xxx_xxx.sql}
   1. TortoiseSVN
   2. Get Lock
3. Abrir SQL Navigator na HSDEV
4. Fazer alterações
5. Menu:
   1. object
   2. extract ddl
   3. uncheck "Include Grants"
6. Abrir ficheiro em questão fora do SQL Navigator (Notepad++ p.ex.)
7. Copiar todo o text do ddl gerado (CTRL+A)
8. Substituir esse código pelo que estava no ficheiro do svn
9. Gravar ficheiro
10. Botão direito:
    1. SVN Commit
11. Arrastar ticket para TESTING (QUAL)

## NOTAS

Eventualmente vai ser preciso copiar as alterações para a versão fechada (TODO)
