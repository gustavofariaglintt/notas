param (
    [string[]]$hotfixs = $null,
    [switch]$help
 )

 # displays command-line tool usage information
function help() {
    Write-Host "`n`n'Command line utility options:"

    Write-Host "`n`t-folderpath <C:\path\to\folder>"
    Write-Host "`t`tAbsolute path for the root directory."
    Write-Host "`t`tDefaults to the current working directory."

    Write-Host "`n`t-desiredParent <folder>"
    Write-Host "`t`tName of the folder where the desired file resides."
    Write-Host "`t`tDefaults to 'Glinths.Shell'."

    Write-Host "`n`t-desiredLeaf <file.extension>"
    Write-Host "`t`tName of the file to update."
    Write-Host "`t`tDefaults to 'Web.Config'."

    Write-Host "`n`t-revert"
    Write-Host "`t`tIf specified, returns the file to its original state."

    Write-Host "`n`t-test"
    Write-Host "`t`tIf specified, allows for the discovery of updatable files without performing any changes.`n`n"
}

# loads $filepath into a traversable DOM tree
function writeEmail($hotfixs) {

    if( Test-Path $filepath -PathType Leaf ) {
        [xml]$ret = Get-Content $filepath;
        return $ret;

    } else {
        Write-Host "File $($filepath) not found" -foregroundColor Red;
        return -1;
    }
}

# displays help information if the switch is set
if ($help) {
    help;
    return;
}

# $folderpath defaults to the current working directory if it is not set by the user
if ([string]::IsNullOrEmpty($folderpath)) {
    $folderpath = Get-Location;
}

getShellWebConfigs $folderpath
