# Guia Hotfix

1. Colar o seguinte link no browser:
    <http://10.250.39.117:9001/GlinttHS.Tools.DeployServiceLibRest/api/preReleases/dabeaf4f-f59a-4468-b99b-b9d43c44f24a/{PRODUTO}/{VERSAO}>

    ex.: <http://10.250.39.117:9001/GlinttHS.Tools.DeployServiceLibRest/api/preReleases/dabeaf4f-f59a-4468-b99b-b9d43c44f24a/EPR_MOBILE/18R1201>

2. Só é criado um número novo de hotfix se o anterior já estiver fechado pela qualidade. Se  os vistos de I&D, etc já estiverem colocados e o de Qualidade não, validar primeiro com a Daniela Diogo/Elisabete Mendes se podemos colocar mais coisas lá.
3. Pasta criada fica acessivel a partir do ícone da pasta dentro do gTools -> Upgrade Tools -> Release Patch -> Canto Inferior Direito do Ecrã (Pasta com Lupa)
4. Navegar até à pasta criada.
5. Colar ficheiros pertinentes.
   1. NOTA: Se for hotfix do GPLATFORM a(s) dll(s) a copiar é(são) a(s) que foi(foram) compilada(s) na pasta: \\\\demoq\\d$\\GLINTTHS_IIS\\PRIV\\GPLATFORM\\IIS\\17R1
   2. Se foram alterações feitas ao código da base de dados, trabalhá-las sobre a versão mais recente instalada no cliente (<https://svn.glintths.com/svn/Releases/PorCertificar/>) (Abrir pelo repoBrowser do SVN)
      1. Para ver qual a versão instalada:
         1. DB: HSPROD (ou equivalente no ambiente onde se deve lançar o hotfix)
         2. USER: PROXY

         ``` SQL
         SELECT * FROM GA_PATCH_INSTALACAO
         WHERE PRODUCT_CODE = 'ow' -- mudar sigla
         ORDER BY DT_CRI DESC;
         ```

6. No gTools:
   1. Check 'PS'
   2. Validar scripts
   3. Check 'VEID'
7. Enviar e-mail:

    To: Daniela Diogo

    Cc: Francisco Correia; Francisco Vicente; Bruno Carolo;

    Subject: Hotfix HSEIT-3821

    Foram gerados os hotfix para a instalação no Cliente relativo aos tickets **HSEIT-3821 | HSEIT-3827 | HSEIT-3814**.

    * Ticket original que provocou este erro :  **NA**
    * Em que situações ocorre, qual o problema exato:
      * **HSEIT-3821 | HSEIT-3827**  -> Problemas na gravação e consulta de histórico do CIT;
      * **HSEIT-3814** -> No workflow da urgência não se conseguia abrir o processo do doente em doentes triados sem médico;
    * Patch a partir do qual ocorre o erro, por versão: **NA**
    * Sobre que patch(es) foi efetuado o hotfix: **GCX1908.01.01.01.jw | GCX1909.01.01.01.jw**
    * Patch(es) onde vamos colocar a correção: **GCX1910.01.01.01.jw**
    * Hotfix(es) gerado(s): **GCX1908.01.23.01.jw | GCX1909.01.15.01.jw**
