# Builds Automáticas Jenkins e Octopus

<http://devci.glintt.com:8080/>

<http://10.250.39.103:8085/>

## Overview

1. GTools
2. Jenkins
3. Octopus
   1. PreRelease
   2. Release

## Bitbucket

Criação Produto Globalcare

<https://bitbucket.org/glintt_sws/jenkins-shared-library/src/master/>

## Passos

Adicionar jenkinsfile (template) ao proj no bb (já vem no boilerplate?)

### Jenkins

Templates/Template-DEV

Vars a mudar:

1. repository-url
2. branches to build

### Octopus

2 passos: Octopus Dev e Octopus Live (necessário configurar os 2)

Settings -> Clone (direita cima)

2 comps: db e iis

### Configs GTools

.

### Notas

stagecheckout -> mesmo com slack vars a null, é obrigatória

vicente tem de pedir acesso ao jaime para os elems específicos de cada equipa
