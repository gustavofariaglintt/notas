# GX-7004

## Jira

**Epic** [GX-7004](https://glinttdev.atlassian.net/browse/GX-7004)

## Observações do Agendamento

**Cliente** HPP

**Patch** - GCX2003

**TDM** - Fernando Oliveira

**Produto** - Nuno Oliveira

**Frontend** - Filipe Martins

### Descrição

Como médico quero poder realizar vários registos de uma escala, visualizar os registos feitos por outros profissionais para a mesma escala no processo de um episódio.

### Mensagem Check-In

GX-7004: filtro por tipo adicional de escala e alternativa de fazer load_escalas por episodio ou por data
GX-7004: added attributes interventioncode, icontype and displaytype to localnursingindicatorsummaryvalue class

### Changesets

|       TFS      |   DEV    |    CI    |   17R1   |
|---------------:|:--------:|:--------:|:--------:|
| **Changesets** |  220820  |  220821  |  223541  |
|                |    --    |    --    |    --    |

|       SVN      |  HSDEV  |  DEMOQ  | DEMOPRIV |
|---------------:|:-------:|:-------:|:--------:|
|  **Revision**  |  CHECK  |  CHECK  |  CHECK   |
|                |   ---   |    --   |    --    |

### Hotfix

+ GlinttHS.API.Internal.WS
  + bin
    + GlinttHS.API.Internal.WS.dll
    + Glintths.Base.Internal.Entities.dll
    + GlinttHS.Services.Local.Nursing.DataModel.dll
    + GlinttHS.Services.Local.Nursing.Repository.dll
+ Database
  + Code
    + 00000-ENF-PCK_EPRE_INTERV_TYPES_spec.sql
    + 00000-ENF-PCK_EPRE_INTERV_body.sql
    + 00000-EF_LOCAL_SERVICES-PCK_EPRE_COMMON_body.sql

#### Controller Frontend URL

<http://demoapliis.glinttocm.com:6002/EPR_MOBILE/Glintths.Shell/xxxx/yyyy.svc>

#### Chave GINF_CONFIG

| key | value |
|:---:|:-----:|
|"S_GINF_CONFIG_KEY"|```"#BASE_URL#pathtofile.svc"```|

#### Projecto Backend

##### GlinttHS.API.Internal.WS\Local\Nursing

+ [x] **IIS** GlinttHS.API.Internal.WS\Local\Nursing
+ [x] **Lógica** enf.pck_epre_interv

#### Configurações

N/A

### Backend Lógica

#### C\#

![Changeset](changeset.png "Changeset")

```diff
public class LocalNursingIndicatorSummaryValue
    {
        /// <summary>
        /// COD_INDIC
        /// </summary>
        [DataMember]
        public string IndicatorCode { get; set; }

+       /// <summary>
+       /// COD_INTERV
+       /// </summary>
+       [DataMember]
+       public string InterventionCode { get; set; }

+       /// <summary>
+       /// ICON_TYPE
+       /// </summary>
+       [DataMember]
+       public string IconType { get; set; }

+       /// <summary>
+       /// DISPLAY_TYPE
+       /// </summary>
+       [DataMember]
+       public string DisplayType { get; set; }

        /// <summary>
        /// DATA_HORA
        /// </summary>
        [DataMember]
        public DateTimeOffset DateHour { get; set; }

        /// <summary>
        /// VALOR
        /// </summary>
        [DataMember]
        public decimal? Value { get; set; }

        /// <summary>
        /// VALOR_HIGH
        /// </summary>
        [DataMember]
        public decimal? ValueHigh { get; set; }

        /// <summary>
        /// COD_CHECK
        /// </summary>
        [DataMember]
        public decimal? CodeCheck { get; set; }
    }
```

```c#
public class LocalNursingIndicatorSummaryValue
    {
        /// <summary>
        /// COD_INDIC
        /// </summary>
        [DataMember]
        public string IndicatorCode { get; set; }

        /// <summary>
        /// COD_INTERV
        /// </summary>
        [DataMember]
        public string InterventionCode { get; set; }

        /// <summary>
        /// ICON_TYPE
        /// </summary>
        [DataMember]
        public string IconType { get; set; }

        /// <summary>
        /// DISPLAY_TYPE
        /// </summary>
        [DataMember]
        public string DisplayType { get; set; }

        /// <summary>
        /// DATA_HORA
        /// </summary>
        [DataMember]
        public DateTimeOffset DateHour { get; set; }

        /// <summary>
        /// VALOR
        /// </summary>
        [DataMember]
        public decimal? Value { get; set; }

        /// <summary>
        /// VALOR_HIGH
        /// </summary>
        [DataMember]
        public decimal? ValueHigh { get; set; }

        /// <summary>
        /// COD_CHECK
        /// </summary>
        [DataMember]
        public decimal? CodeCheck { get; set; }
    }
```

#### DB

+ ENF
  + pck_epre_interv
    + load_ECRA_REGISTO_INTERV_MED
    + load_ESCALAS_TABLE
    + load_INFORMACAO_GRAFICOS
  + pck_epre_interv_types
    + T_RESUMO_VALOR_INDICADOR
    + T_ESCALA_SCORES
  + pck_epre_common
    + conv_CLOB_into_T_RES_VAL_INDIC
    + conv_T_RES_VAL_INDIC_into_CLOB

##### pck_epre_interv_body.sql

###### load_ECRA_REGISTO_INTERV_MED

```diff
SELECT 'GR_ENF_INTERV' tabela, q1.cod_interv
  FROM gr_enf_interv_pessoal q0, gr_enf_interv q1, gr_enf_qdr_espec q2
  WHERE     q0.tabela = 'GR_ENF_INTERV'
        AND q0.t_pess_hosp = 'MED'
        AND q0.flg_pode_registar = 'S'
        AND q0.cod_interv = q1.cod_interv
        AND q1.cod_qdr = q2.cod_qdr(+)
-       AND q2.form IN ('GHEN0630','GHEN0070')
+       AND q2.form IN ('GHEN0630', 'GHEN0070', 'GHEN0610')
        AND NOT EXISTS
                    (SELECT 1
                      FROM sd_enf_plano_cuid_interv q3
                      WHERE     q3.t_doente = i_t_doente
                            AND q3.doente = i_doente
                            AND q3.t_episodio = i_t_episodio
                            AND q3.episodio = i_episodio
                            AND q3.tabela = 'GR_ENF_INTERV'
                            AND q3.cod_interv = q1.cod_interv)
        --| Glintt-HS-18006 adriano.alves
        AND EXISTS
                (SELECT 1
                  FROM sd_enf_interv_indicador q4
                  WHERE q0.cod_interv = q4.cod_interv)

***************************************************

SELECT q3.ID_AGRUPADOR
      ,q2.FORM
  FROM GR_ENF_INTERV_PESSOAL    q0
      ,GR_ENF_INTERV            q1
      ,GR_ENF_QDR_ESPEC         q2
      ,SD_ENF_PLANO_CUID_INTERV q3
  WHERE q0.TABELA            = q3.TABELA
    AND q0.T_PESS_HOSP       = 'MED'
    AND q0.FLG_PODE_REGISTAR = 'S'
    AND q0.COD_INTERV        = q1.COD_INTERV
    AND q1.COD_QDR           = q2.COD_QDR (+)
-   AND q2.FORM IN ( 'GHEN0630','GHEN0070' )
+   AND q2.FORM IN ( 'GHEN0630','GHEN0070', 'GHEN0610' )
    AND q3.T_DOENTE      = i_T_DOENTE
    AND q3.DOENTE        = i_DOENTE
    AND q3.T_EPISODIO    = i_T_EPISODIO
    AND q3.EPISODIO      = i_EPISODIO
    AND q3.TABELA        = 'GR_ENF_INTERV'
    AND q3.COD_INTERV    = q1.COD_INTERV
    AND trunc(i_DATA_HORA_TS,'MI') BETWEEN q3.DT_INI AND nvl(q3.DT_FIM,i_DATA_HORA_TS)

***************************************************

- IF INTERV.FORM IN ( 'GHEN0630','GHEN0070' )
+ IF INTERV.FORM IN ( 'GHEN0630','GHEN0070', 'GHEN0610')
    THEN
        o_QDR_ESPECIFICO(v_INDEX).TIPO    := 'REGISTO_DE_VALORES';
```

###### load_ESCALAS_TABLE

```diff
WITH O_REC AS
  (
      SELECT COD_CHECK,SDF_ENF_ADD_HOUR2DATE(DT_ITEM,HR_ITEM) DATA_HORA
              ,RANK() OVER (ORDER BY SDF_ENF_ADD_HOUR2DATE(DT_ITEM,HR_ITEM) DESC) RNK_
              ,N_MECAN_ENF_RESP
         FROM SD_ENF_CHECK_LIST
        WHERE T_DOENTE     = i_T_DOENTE
          AND DOENTE       = i_DOENTE
-         AND T_EPISODIO   = i_T_EPISODIO
-         AND EPISODIO     = i_EPISODIO
-         AND TABELA       = 'GR_ENF_INTERV'
-         AND COD_ITEM     = i_COD_INTERV
-         AND FLG_ESTADO   = 'S'
-         AND DT_ITEM                                  BETWEEN trunc(nvl(i_DT_INI,SYSDATE-2000)) AND nvl(i_DT_FIM,SYSDATE)
-         AND SDF_ENF_ADD_HOUR2DATE(DT_ITEM,HR_ITEM)   BETWEEN       nvl(i_DT_INI,SYSDATE-2000)  AND nvl(i_DT_FIM,SYSDATE)
+         AND TABELA       = 'GR_ENF_INTERV'
+         AND COD_ITEM     = i_COD_INTERV
+         AND FLG_ESTADO   = 'S'
+         AND ((
+             i_DT_INI IS NULL AND i_DT_FIM IS NULL
+                 AND T_EPISODIO   = NVL(i_T_EPISODIO, T_EPISODIO)
+                 AND EPISODIO     = NVL(i_EPISODIO, EPISODIO))
+             OR
+             (
+                 DT_ITEM                                  BETWEEN trunc(nvl(i_DT_INI,TO_DATE(1, 'J'))) AND nvl(i_DT_FIM,TO_DATE(9999, 'yyyy'))
+             AND SDF_ENF_ADD_HOUR2DATE(DT_ITEM,HR_ITEM)   BETWEEN       nvl(i_DT_INI,TO_DATE(1, 'J'))  AND nvl(i_DT_FIM,TO_DATE(9999, 'yyyy'))
+             )
+         )
  ),

```

```sql
WITH O_REC AS
  (
      SELECT COD_CHECK,
              SDF_ENF_ADD_HOUR2DATE (DT_ITEM, HR_ITEM) DATA_HORA,
              RANK () OVER (ORDER BY SDF_ENF_ADD_HOUR2DATE (DT_ITEM, HR_ITEM) DESC)
                  RNK_,
              N_MECAN_ENF_RESP
        FROM SD_ENF_CHECK_LIST
        WHERE     T_DOENTE = I_T_DOENTE
              AND DOENTE = I_DOENTE
              AND TABELA = 'GR_ENF_INTERV'
              AND COD_ITEM = I_COD_INTERV
              AND FLG_ESTADO = 'S'
              AND (   (    I_DT_INI IS NULL
                      AND I_DT_FIM IS NULL
                      AND T_EPISODIO = NVL (I_T_EPISODIO, T_EPISODIO)
                      AND EPISODIO = NVL (I_EPISODIO, EPISODIO))
                  OR (    DT_ITEM                                  BETWEEN trunc(nvl(i_DT_INI,TO_DATE(1, 'J'))) AND nvl(i_DT_FIM,TO_DATE(9999, 'yyyy'))
                      AND SDF_ENF_ADD_HOUR2DATE(DT_ITEM,HR_ITEM)   BETWEEN       nvl(i_DT_INI,TO_DATE(1, 'J'))  AND nvl(i_DT_FIM,TO_DATE(9999, 'yyyy'))
                      )
                  )
  ),
```

###### load_INFORMACAO_GRAFICOS (2)

```diff
SELECT  COD_CHECK,
        COD_INDIC,
+       COD_INTERV,
        DATAHORA,
        VALOR,
        VALOR_HIGH,
+       ICON_TYPE,
+       DISPLAY_TYPE
  BULK COLLECT INTO o_VALORES
  FROM
  (
    WITH INDICADORES AS
    (
+       SELECT q0.COD_CHECK,q0.COD_INDIC,q0.COD_INTERV,q0.DATAHORA,q0.VALOR,nvl(q1.COD_INDIC_AGRUP,q0.COD_INDIC) COD_INDIC_AGRUP,q1.ICON_TYPE,q1.DISPLAY_TYPE
          FROM SDT_ENF_INDICADORES q0 JOIN GR_ENF_INDIC q1 ON ( q0.COD_INDIC = q1.COD_INDIC )
          WHERE q0.VALOR IS NOT NULL
    )
    SELECT i_LOW.COD_CHECK
          ,NVL2(i_HIGH.COD_INDIC,i_HIGH.COD_INDIC|| '#','' )||i_LOW.COD_INDIC_AGRUP COD_INDIC
+         ,i_LOW.COD_INTERV COD_INTERV
+         ,i_LOW.ICON_TYPE ICON_TYPE
+         ,i_LOW.DISPLAY_TYPE DISPLAY_TYPE
          ,i_LOW.DATAHORA
          ,i_LOW.VALOR VALOR
          ,i_HIGH.VALOR VALOR_HIGH
      FROM ( SELECT * FROM INDICADORES WHERE COD_INDIC  = COD_INDIC_AGRUP) i_LOW
          ,( SELECT * FROM INDICADORES WHERE COD_INDIC <> COD_INDIC_AGRUP) i_HIGH
      WHERE i_LOW.COD_CHECK          = i_HIGH.COD_CHECK (+)
        AND i_LOW.COD_INDIC_AGRUP    = i_HIGH.COD_INDIC_AGRUP (+)
        AND (   p_DT_INICIO IS NULL
                OR
                i_LOW.DATAHORA BETWEEN p_DT_INICIO AND p_DT_FIM
            )
  );
```

```sql
SELECT  COD_CHECK,
        COD_INDIC,
        COD_INTERV,
        DATAHORA,
        VALOR,
        VALOR_HIGH,
        ICON_TYPE,
        DISPLAY_TYPE
  BULK COLLECT INTO o_VALORES
FROM
(
    WITH INDICADORES AS
    (
        SELECT q0.COD_CHECK,q0.COD_INDIC,q0.COD_INTERV,q0.DATAHORA,q0.VALOR,nvl(q1.COD_INDIC_AGRUP,q0.COD_INDIC) COD_INDIC_AGRUP,q1.ICON_TYPE,q1.DISPLAY_TYPE
          FROM SDT_ENF_INDICADORES q0 JOIN GR_ENF_INDIC q1 ON ( q0.COD_INDIC = q1.COD_INDIC )
          WHERE q0.VALOR IS NOT NULL
    )
    SELECT i_LOW.COD_CHECK
          ,NVL2(i_HIGH.COD_INDIC,i_HIGH.COD_INDIC|| '#','' )||i_LOW.COD_INDIC_AGRUP COD_INDIC
          ,i_LOW.COD_INTERV COD_INTERV
          ,i_LOW.ICON_TYPE ICON_TYPE
          ,i_LOW.DISPLAY_TYPE DISPLAY_TYPE
          ,i_LOW.DATAHORA
          ,i_LOW.VALOR VALOR
          ,i_HIGH.VALOR VALOR_HIGH
      FROM ( SELECT * FROM INDICADORES WHERE COD_INDIC  = COD_INDIC_AGRUP) i_LOW
          ,( SELECT * FROM INDICADORES WHERE COD_INDIC <> COD_INDIC_AGRUP) i_HIGH
      WHERE i_LOW.COD_CHECK          = i_HIGH.COD_CHECK (+)
        AND i_LOW.COD_INDIC_AGRUP    = i_HIGH.COD_INDIC_AGRUP (+)
        AND (   p_DT_INICIO IS NULL
                OR
                i_LOW.DATAHORA BETWEEN p_DT_INICIO AND p_DT_FIM
            )
);
```

##### pck_epre_interv_types_spec.sql

###### T_RESUMO_VALOR_INDICADOR

```diff
TYPE T_RESUMO_VALOR_INDICADOR IS RECORD
    (
        COD_CHECK       NUMBER,
        COD_INDIC       VARCHAR2(11),
+       COD_INTERV      VARCHAR2(100),
        DATA_HORA       DATE,
        VALOR           NUMBER,
        VALOR_HIGH      NUMBER,
+       ICON_TYPE       VARCHAR2(30),
+       DISPLAY_TYPE    VARCHAR2(30)
    );
```

```sql
TYPE T_RESUMO_VALOR_INDICADOR IS RECORD
    (
        COD_CHECK       NUMBER,
        COD_INDIC       VARCHAR2(11),
        COD_INTERV      VARCHAR2(100),
        DATA_HORA       DATE,
        VALOR           NUMBER,
        VALOR_HIGH      NUMBER,
        ICON_TYPE       VARCHAR2(30),
        DISPLAY_TYPE    VARCHAR2(30)
    );
```

###### T_ESCALA_SCORES

```diff
TYPE T_ESCALA_SCORES IS RECORD
    (
        COD_SCORE   VARCHAR2(10),
-       DESCR_SCORE VARCHAR2(50),
+       DESCR_SCORE VARCHAR2(300),
        MIN_SCORE   NUMBER,
        MAX_SCORE   NUMBER,
        ORDEM       NUMBER
    );
```

```sql
TYPE T_ESCALA_SCORES IS RECORD
    (
        COD_SCORE   VARCHAR2(10),
        DESCR_SCORE VARCHAR2(300),
        MIN_SCORE   NUMBER,
        MAX_SCORE   NUMBER,
        ORDEM       NUMBER
    );
```

##### pck_epre_common_body.sql

##### conv_CLOB_into_T_RES_VAL_INDIC

```diff
SELECT
      extractvalue(column_value,'/*/CodeCheck' ,w_namespace) COD_CHECK,
      extractvalue(column_value,'/*/IndicatorCode' ,w_namespace) COD_INDIC,
+     extractvalue(column_value,'/*/InterventionCode' ,w_namespace) COD_INTERV,
      pck_xml_utilities.get_xmldatetime_to_dbdate (extractvalue(column_value,'/*/DateHour' ,w_namespace)) DATA_HORA,
      extractvalue(column_value,'/*/Value' ,w_namespace) VALOR,
      extractvalue(column_value,'/*/ValueHigh' ,w_namespace) VALOR_HIGH,
+     extractvalue(column_value,'/*/IconType' ,w_namespace) ICON_TYPE,
+     extractvalue(column_value,'/*/DisplayType' ,w_namespace) DISPLAY_TYPE
FROM (TABLE (xmlsequence (EXTRACT (xmltype(i_CLOB),'//ArrayOfLocalNursingIndicatorSummaryValue/LocalNursingIndicatorSummary',w_namespace))))
```

```sql
SELECT
      extractvalue(column_value,'/*/CodeCheck' ,w_namespace) COD_CHECK,
      extractvalue(column_value,'/*/IndicatorCode' ,w_namespace) COD_INDIC,
      extractvalue(column_value,'/*/InterventionCode' ,w_namespace) COD_INTERV,
      pck_xml_utilities.get_xmldatetime_to_dbdate (extractvalue(column_value,'/*/DateHour' ,w_namespace)) DATA_HORA,
      extractvalue(column_value,'/*/Value' ,w_namespace) VALOR,
      extractvalue(column_value,'/*/ValueHigh' ,w_namespace) VALOR_HIGH,
      extractvalue(column_value,'/*/IconType' ,w_namespace) ICON_TYPE,
      extractvalue(column_value,'/*/DisplayType' ,w_namespace) DISPLAY_TYPE
FROM (TABLE (xmlsequence (EXTRACT (xmltype(i_CLOB),'//ArrayOfLocalNursingIndicatorSummaryValue/LocalNursingIndicatorSummary',w_namespace))))
```

##### conv_T_RES_VAL_INDIC_into_CLOB

```diff
IF (i_resumo_indic_values.EXISTS (i))
THEN
    w_curr_resumo_indic_value := i_resumo_indic_values (i);
    DBMS_LOB.append(o_CLOB,'<LocalNursingIndicatorSummaryValue>');
    pck_xml_utilities.get_dbTSwithTZ_to_xmldatetime(w_curr_resumo_indic_value.DATA_HORA, o_date_str, o_DATE_OFFSET_STR);
    my_DBMSLOBAPPEND(o_CLOB,'CodeCheck', w_curr_resumo_indic_value.COD_CHECK );
    my_DBMSLOBAPPEND_With_NS (o_CLOB, 'DateHour',  '<a:DateTime>'||o_date_str||'</a:DateTime><a:OffsetMinutes>'||o_DATE_OFFSET_STR||'</a:OffsetMinutes>' , 'xmlns:a="http://schemas.datacontract.org/2004/07/System"', false);
+   my_DBMSLOBAPPEND(o_CLOB,'DisplayType', w_curr_resumo_indic_value.DISPLAY_TYPE );
+   my_DBMSLOBAPPEND(o_CLOB,'IconType', w_curr_resumo_indic_value.ICON_TYPE );
    my_DBMSLOBAPPEND(o_CLOB,'IndicatorCode', w_curr_resumo_indic_value.COD_INDIC );
+   my_DBMSLOBAPPEND(o_CLOB,'InterventionCode', w_curr_resumo_indic_value.COD_INTERV );
    my_DBMSLOBAPPEND(o_CLOB,'Value', w_curr_resumo_indic_value.VALOR );
    my_DBMSLOBAPPEND(o_CLOB,'ValueHigh', w_curr_resumo_indic_value.VALOR_HIGH );
    DBMS_LOB.append(o_CLOB,'</LocalNursingIndicatorSummaryValue>');
END IF;
```

```sql
IF (i_resumo_indic_values.EXISTS (i))
THEN
    w_curr_resumo_indic_value := i_resumo_indic_values (i);
    DBMS_LOB.append(o_CLOB,'<LocalNursingIndicatorSummaryValue>');
    pck_xml_utilities.get_dbTSwithTZ_to_xmldatetime(w_curr_resumo_indic_value.DATA_HORA, o_date_str, o_DATE_OFFSET_STR);
    my_DBMSLOBAPPEND(o_CLOB,'CodeCheck', w_curr_resumo_indic_value.COD_CHECK );
    my_DBMSLOBAPPEND_With_NS (o_CLOB, 'DateHour',  '<a:DateTime>'||o_date_str||'</a:DateTime><a:OffsetMinutes>'||o_DATE_OFFSET_STR||'</a:OffsetMinutes>' , 'xmlns:a="http://schemas.datacontract.org/2004/07/System"', false);
    my_DBMSLOBAPPEND(o_CLOB,'DisplayType', w_curr_resumo_indic_value.DISPLAY_TYPE );
    my_DBMSLOBAPPEND(o_CLOB,'IconType', w_curr_resumo_indic_value.ICON_TYPE );
    my_DBMSLOBAPPEND(o_CLOB,'IndicatorCode', w_curr_resumo_indic_value.COD_INDIC );
    my_DBMSLOBAPPEND(o_CLOB,'InterventionCode', w_curr_resumo_indic_value.COD_INTERV );
    my_DBMSLOBAPPEND(o_CLOB,'Value', w_curr_resumo_indic_value.VALOR );
    my_DBMSLOBAPPEND(o_CLOB,'ValueHigh', w_curr_resumo_indic_value.VALOR_HIGH );
    DBMS_LOB.append(o_CLOB,'</LocalNursingIndicatorSummaryValue>');
END IF;
```
