# GX-5456

## Jira

**Epic** [GX-5456](https://glinttdev.atlassian.net/browse/GX-5456)

**User Story Dev** [GX-8392](https://glinttdev.atlassian.net/browse/GX-8392)

## Observações do Agendamento

**Cliente** xxx

**Patch** - GCX20XX

**TDM** - TODO

**Produto** - TODO

**Frontend** - TODO

### Descrição

TODO (copiar do JIRA)

### Mensagem Check-In

GX-5465|GX-8392
Reverter para ninguém parte 2
alter table column not null

### Changesets

|       TFS      |   DEV    |    CI    |   17R1   |   19R12  |   20R1   |
|---------------:|:--------:|:--------:|:--------:|:--------:|:--------:|
| **Changesets** |  216859  |  216863  |    --    |
|                |  216880  |  216881  |  218249  |
|                |  217064  |  217066  |    --    |
|                |  217170  |  217186  |    --    |
|                |  217189  |  217191  |  218250  |  218251  |  218252  |
|                |    --    |    --    |    --    |

|       SVN      |  HSDEV  |  DEMOQ  | DEMOPRIV |
|---------------:|:-------:|:-------:|:--------:|
|    **Code**    |  CHECK  |  CHECK  |  xxxxxx  |
|    **Alter**   |  CHECK  |  CHECK  |  xxxxxx  |
|                |   ---   |    --   |    --    |

### Notas

TODO

### Backend Configs

#### Chave GINF_CONFIG

| key | value |
|:---:|:-----:|
|"S_CLINICAL_EPR_TRANSFERENCE"|```"#BASE_URL#Cpchs.Modules.ActivitiesManagement.WS/Transference/WebService/TransferenceManagement.svc"```|

#### Projecto Backend

##### Cpchs.Modules.ActivitiesManagement.WebServices

+ [ ] **IIS** Services/Glintths.WebServices
+ [x] **Lógica** Services/Glintths.BusinessRules
+ [x] **Data Model** Services/Glintths.BusinessEntities

### Backend Lógica

#### C\#

![Changeset](changeset.png "Changeset")

![Changeset](changeset1.png "Changeset")

![Changeset](changeset3.png "Changeset")

#### DB

+ GC_DOTNET_CODE
  + PCK_URGENCY
    + assumepatient ("EPR/(ReleaseAberta/BD_DOTNET_DEV|17R101/BD_DOTNET_17R101)/PCK_URGENCY_(spec|body).sql")
      + spec

        ``` sql
            PROCEDURE transfertonoone (
                p_t_episodio            IN     urv_urgencies.urgencyepisodetype%TYPE,
                p_episodio              IN     urv_urgencies.urgencyepisodeid%TYPE,
                p_usersys               IN     VARCHAR2,
                p_result                OUT    NUMBER);
        ```

      + body

        ``` sql
        -- ----------------------------------------------------------------------------
        -- ----------------------------------------------------------------------------

        PROCEDURE transfertonoone (
            p_t_episodio   IN     urv_urgencies.urgencyepisodetype%TYPE,
            p_episodio     IN     urv_urgencies.urgencyepisodeid%TYPE,
            p_usersys      IN     VARCHAR2,
            p_result          OUT NUMBER)
        IS
        BEGIN
            tp_pck_utilizador_apl.act_util_apl (p_usersys);

            p_result :=
                pck_tracking_urgency.accept_patient (p_t_episodio,
                                                    p_episodio,
                                                    NULL,
                                                    'S',
                                                    NULL,
                                                    p_usersys);

            UPDATE urg_dados_adic
            SET medico_resp = NULL
            WHERE episodio = p_episodio AND t_episodio = p_t_episodio;

            -- urv_urgencies = union all (urv_urgencies_priv + urv_urgencies_pub) mas urv_urgencies nao e updatable
            UPDATE urv_urgencies_priv
            SET urgencyepisodedoctornmecan = NULL
            WHERE     urgencyepisodeid = p_episodio
                AND urgencyepisodetype = p_t_episodio;

            UPDATE urv_urgencies_pub
            SET urgencyepisodedoctornmecan = NULL
            WHERE     urgencyepisodeid = p_episodio
                AND urgencyepisodetype = p_t_episodio;

            COMMIT;
        EXCEPTION
            WHEN OTHERS
            THEN
                ROLLBACK;
                p_result := -1;
        END transfertonoone;
        ```

+ CPC_BD
  + script # remover constraint de not null na coluna n_mecan_resp

    ``` sql
    DECLARE
    V_IS_NULLABLE   NUMBER;
    BEGIN
        V_IS_NULLABLE := 0;

        SELECT COUNT (1)
          INTO V_IS_NULLABLE
          FROM USER_TAB_COLS
        WHERE     TABLE_NAME = 'UR_TRANSFERENCIAS'
              AND COLUMN_NAME = 'N_MECAN_RESP'
              AND NULLABLE = 'Y';

        IF V_IS_NULLABLE = 0
        THEN
            BEGIN
                GA_INSTALL_OBJECTS.EXECUTE_IMMEDIATE (
                    'ALTER TABLE CPC_BD.UR_TRANSFERENCIAS
                MODIFY (N_MECAN_RESP NULL)');
            END;
        END IF;
    END;
    /
    ```

### Notas Frontend
