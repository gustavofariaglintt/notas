# https://knightcodes.com/miscellaneous/2016/09/05/build-solutions-and-projects-with-powershell.html

# get msbuild fo
function get-MSBuild-Path()
{
   #$agentPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\BuildTools\MSBuild\15.0\Bin\msbuild.exe"
   #$devPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\msbuild.exe"
   #$proPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\msbuild.exe"
   #$communityPath = "$Env:programfiles (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\msbuild.exe"
   $fallback2019Path = "${Env:ProgramFiles(x86)}\Microsoft Visual Studio\2019\Professional\MSBuild\Current\Bin\msbuild.exe"
   $fallback2017Path = "${Env:ProgramFiles(x86)}\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\msbuild.exe"
   $fallback2015Path = "${Env:ProgramFiles(x86)}\MSBuild\14.0\Bin\MSBuild.exe"
   $fallback2013Path = "${Env:ProgramFiles(x86)}\MSBuild\12.0\Bin\MSBuild.exe"
   $fallbackPath = "C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"
        
   #If (Test-Path $agentPath) { return $agentPath }
   #If (Test-Path $devPath) { return $devPath }
   #If (Test-Path $proPath) { return $proPath }
   #If (Test-Path $communityPath) { return $communityPath }
   If (Test-Path $fallback2019Path) { return $fallback2019Path }
   If (Test-Path $fallback2017Path) { return $fallback2017Path }
   If (Test-Path $fallback2015Path) { return $fallback2015Path }
   If (Test-Path $fallback2013Path) { return $fallback2013Path }
   If (Test-Path $fallbackPath) { return $fallbackPath }
}

function buildVS
{
    param
    (
        [parameter(Mandatory=$true)]
        [String] $path,

        [parameter(Mandatory=$false)]
        [bool] $nuget = $true,
        
        [parameter(Mandatory=$false)]
        [bool] $clean = $true,
		
		[parameter(Mandatory=$false)]
        [String] $solution
		
    )
    process
    {
		$sw=[Diagnostics.Stopwatch]::StartNew()
		
        $msBuildExe = get-MSBuild-Path #'C:\Program Files (x86)\Microsoft Visual Studio\2017\Enterprise\MSBuild\15.0\Bin\MSBuild.exe'

        if ($nuget) {
            Write-Host "Restoring NuGet packages" -foregroundcolor green
            nuget restore "$($path)"
        }

        if ($clean) {
            Write-Host "Cleaning $($path)" -foregroundcolor green
            & "$($msBuildExe)" "$($path)" /t:Clean /m  /clp:ErrorsOnly /p:WarningLevel=0 #/noconsolelogger
        }

		if ($solution) {
			Write-Host "Building $($path)" -foregroundcolor green
			& "$($msBuildExe)" "$($path)"  /p:SolutionDir=$solution /t:Build /m  /clp:ErrorsOnly /p:WarningLevel=0 #/noconsolelogger
		}
		else
		{
			Write-Host "Building $($path)" -foregroundcolor green
			& "$($msBuildExe)" "$($path)" /t:Build /m  /clp:ErrorsOnly /p:WarningLevel=0 #/noconsolelogger
		}
		
		$sw.Stop()
		$Duration=$sw.Elapsed.ToString('dd\.hh\:mm\:ss')

		Write-host " "
		Write-host " "
		Write-host "Updating Finished in $Duration" -foregroundcolor Green
		Write-host " "
		Write-host " "
    }
}

#buildVS D:\DOTNET\InfraEstructure\GlinttHS.Infrastructure.BusinessLogic\GlinttHS.Infrastructure.BusinessLogic.sln  $false $true


# nuget restore, clean solution, and build solution
#buildVS .\path\to\solution.sln

# clean solution and build solution
#buildVS .\path\to\solution.sln $false $true

# build solution
#buildVS -path .\path\to\solution.sln -nuget $false -clean $false

# nuget restore and build solution
#buildVS -path .\path\to\solution.sln -clean $false

# etc.
