# CLINICAL-ACTIVITIES (Glintt-HS-22060)

Podes importar isto no postman

``` curl
curl -X GET "http://10.1.1.3:14800/api/v1/Efr/getefrbyepisode?tDoente=HS&doente=39&tEpisodio=Internamentos&episodio=14" -H "accept: text/plain" -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1bmlxdWVfbmFtZSI6Ik1FRCIsInN1YiI6Ik1FRCIsIm5hbWVpZCI6Ik1FRCIsImNsaWVudF9pZCI6IkdIIiwiY2xpZW50SW5mbyI6IntcIkFwcGxpY2F0aW9uSURcIjpcIkdIXCIsXCJBcHBsaWNhdGlvblZlcnNpb25cIjpudWxsLFwiVGVuYW50SURcIjpcIkRFRkFVTFRcIixcIlVzZXJJRFwiOlwiTUVEXCIsXCJVc2VybmFtZVwiOlwiTUVEXCIsXCJDdWx0dXJlXCI6XCJwdC1QVFwiLFwiRmFjaWxpdHlJRFwiOm51bGwsXCJFeHRlcm5hbFByb3ZpZGVyVXNlcm5hbWVcIjpudWxsLFwiRXh0ZXJuYWxQcm92aWRlclVzZXJJRFwiOm51bGwsXCJSb2xlc1wiOltdfSIsImlzcyI6IkdQTEFURk9STSIsImF1ZCI6IkdQTEFURk9STSIsImV4cCI6MTU3MTQ3ODk0NiwibmJmIjoxNTcxMzkyMjQ3fQ.lSKibhzNdKt8FmP5nETFfhebeshlNk2pIOxqyKStBVE"
```

Ou podes ir pela página de documentação do swagger:

<http://10.1.1.3:14800/api/documentation/index.html>

Um exemplo de resposta é:

``` json
{
  "cod_resp": "935640",
  "cod_resp_descr": "ADSE - SNS",
  "num_cartao": "00000000"
}
```
