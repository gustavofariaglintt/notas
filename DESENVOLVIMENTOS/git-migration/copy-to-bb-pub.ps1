param (
    [switch]$fromdemoapl = $false,
    [switch]$tolocalhost = $false,
    [switch]$totestdemoapl = $false
)

if ($fromdemoapl){
    $dirs = get-childitem "\\demoapliis.glinttocm.com\GIIS\BITBUCKET\MASTER\GPLATFORM" | where-object {$_.PSIscontainer};

    foreach ($dir in $dirs) {
        if (!(Test-Path "D:\webconfigs\$($dir.name)")) {
            New-Item -Path "D:\webconfigs" -Name $dir.Name -ItemType "directory"
        }

        if (Test-Path "\\demoapliis.glinttocm.com\GIIS\17R101\GPLATFORM\$($dir)\web.config") {
            Copy-Item -Path "\\demoapliis.glinttocm.com\GIIS\17R101\GPLATFORM\$($dir)\web.config" -Destination "D:\BB\webconfigs\$($dir)";
        }
    }
}

if ($tolocalhost) {
    $dirs = get-childitem "D:\webconfigs" | where-object {$_.PSIscontainer};

    foreach ($dir in $dirs) {
        if ((Test-Path "D:\BB\$($dir)") -And (Test-Path "D:\BB\$($dir)\web.config")) {
            Copy-Item -Path "D:\webconfigs\$($dir)\web.config" -Destination "D:\BB\$($dir)"
        }
    }
}

if ($totestdemoapl) {
    $dirs = get-childitem "D:\webconfigs" | where-object {$_.PSIscontainer};

    foreach ($dir in $dirs) {
        if ((Test-Path "\\demoapliis.glinttocm.com\GIIS\BITBUCKET\MASTER\GPLATFORM\$($dir)") -And (Test-Path "\\demoapliis.glinttocm.com\GIIS\BITBUCKET\MASTER\GPLATFORM\$($dir)\web.config")) {
            Copy-Item -Path "D:\webconfigs\$($dir)\web.config" -Destination "\\demoapliis.glinttocm.com\GIIS\BITBUCKET\MASTER\GPLATFORM\$($dir)"
            Write-Host "Copied file: \\demoapliis.glinttocm.com\GIIS\BITBUCKET\MASTER\GPLATFORM\$($dir)\web.config" -ForegroundColor Green
        }
    }
}