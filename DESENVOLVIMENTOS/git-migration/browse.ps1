param (
    [string]$site = $null,
    [string]$fromFile = $null,
    [string]$remoteUrl = $null,
    [string]$remotePath = $null,
    [switch]$list = $false,
    [switch]$verbose = $false,
    [switch]$help = $false
)

# # Constants
$IIS = "IIS:\Sites";
$baseFileName = "service"
$actionList = "List";
$actionStatus = "Status";
$version = 1;

function help() {
    Write-Host "Usage:`nbrowse.ps1 [-Site <IIS Site Name>] [-list] [-verbose]"
    Write-Host "`tif -Site is not explicit all sites on the server will be traversed"
    Write-Host "`tif -list switch is set healthcheck test wont be executed"
    Write-Host "`tif -verbose switch is not set only errors will be shown"
    Write-Host "`tif either -RemoteUrl or -RemotePath are set script execution will be based on explicit folders and not IIS configurations"
}

function browseService($servicePath) {
    if ($list) {
        if ($verbose)  { Write-Host "Logging: $($servicePath)"; }
        Add-Content "$($baseFileName)$($actionList)$($site)_v$($version).csv" $servicePath;
        return;
    }

    try {
        $response = Invoke-WebRequest -URI $servicePath -Method Get;
    }
    catch {
        if ($verbose)  { Write-Host $servicePath -ForegroundColor Red; }
        Add-Content "$($baseFileName)$($actionStatus)$($site)_v$($version).csv" "$($servicePath),0";
        return;
    }

    if ($null -ne $response -And $response.StatusCode -eq 200) {
        if ($verbose)  { Write-Host $servicePath -ForegroundColor Green; }
        Add-Content "$($baseFileName)$($actionStatus)$($site)_v$($version).csv" "$($servicePath),1";
        return;
    }

    if ($verbose)  { Write-Host $servicePath -ForegroundColor Red; }
    Add-Content "$($baseFileName)$($actionStatus)$($site)_v$($version).csv" "$($servicePath),0";
}

function getAllSVCs($folderpath) {
    if (Test-Path $folderpath -PathType Any) {
        $files =    Get-ChildItem $folderpath -Recurse |
                    Where-Object {
                        !$_.PSIsContainer -And (
                            $_.Extension -eq ".svc" -Or
                            $_.Extension -eq ".asmx"
                        )
                    } |
                    Select-Object -ExpandProperty Fullname -Unique;
    }
    else {
        Write-Host "Couldn't find path: $($folderpath)" -ForegroundColor Yellow;
    }

    return $files;
}

function sweepServices($folderpath, $baseUrl, $applicationPath){
    $serviceFiles = getAllSVCs $folderpath;
    $folderpath = $folderpath -Replace "\\","/";

    if ($null -eq $serviceFiles -Or $serviceFiles.Length -eq 0) {
        browseService "$($baseUrl)$($applicationPath)";
    }

    foreach ($file in $serviceFiles) {
        $file = $file -Replace "\\","/";
        
        $webServiceSpecific = ($file -Split $folderpath)[-1];
        $webServicePath = "$($baseUrl)$($applicationPath)$($webServiceSpecific)";

        browseService $webServicePath;
    }
}

function sweepSite($site) {
    $baseUrl = (Get-WebURL "$($IIS)\$($site)").ResponseUri.OriginalString;

    $applications = Get-WebApplication -Site $site;

    foreach ($application in $applications) {
        sweepServices $application.PhysicalPath $baseUrl $application.Path;
    }
}

function sweepAllSites() {
    $sites = Get-ChildItem $IIS | Select-Object -ExpandProperty Name;

    foreach ($site in $sites) {
        sweepSite $site;
    }
}

######## Explicit Directory Browse (Works Remotely) #######
function remoteBrowse($remoteUrl, $remotePath){
    $servicesFolders =  Get-ChildItem $remotePath |
                        Where-Object {$_.PSIsContainer} |
                        Select-Object -ExpandProperty Name;

    foreach ($service in $servicesFolders) {
        $folderpath = "$($remotePath)\$($service)\";
        $serviceFiles = getAllSVCs $folderpath;
        
        $folderpath = $folderpath -Replace "\\","/";

        if ($null -eq $serviceFiles -Or $serviceFiles.Length -eq 0) {
            browseService "$($remoteUrl)$($service)";
        }

        foreach ($file in $serviceFiles) {
            $file = $file -Replace "\\","/";
            
            $webServiceSpecific = ($file -Split $folderpath)[-1];
            $webServicePath = "$($remoteUrl)$($service)/$($webServiceSpecific)";

            browseService $webServicePath;
        }
    }
}

# # Main
if ($help) {
    help;
    return;
}

if (![string]::IsNullOrEmpty($remoteUrl) -Or ![string]::IsNullOrEmpty($remotePath)) {
    if (![string]::IsNullOrEmpty($remoteUrl) -And ![string]::IsNullOrEmpty($remotePath)) {
        remoteBrowse $remoteUrl $remotePath;
    }
    else {
        Write-Host "Both -RemoteUrl and -RemotePath must be set" -ForegroundColor Red;
    }
    return;
}

# initial webadministration command in order to initialize
# 'IIS:\Sites' virtualdirectory in this powershell session
Get-WebAppPoolState | Out-Null;

if ($list) { $action = $actionList } else { $action = $actionStatus }

$version = (Get-ChildItem |
            Where-Object {
                !$_.PSIsContainer -And
                $_.Name -match "^$($baseFileName)$($action)$($site)?_v(.*).csv$"
            }
        ).Count + 1;

if ($null -eq $site) {
    sweepAllSites;
}
else {
    sweepSite $site;
}
