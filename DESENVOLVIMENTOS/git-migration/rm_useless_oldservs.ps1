param (
    [string]$folderpath = $null,
    [switch]$help,
    [switch]$verbose
)

# CONSTANTS

# folders not to be deleted
$ignorables = @(
    "BusinessEntities",
    "BusinessRules",
    "Proxies",
    "WebServices"
);

# this scrip is only meant to run on this repo
$parentDirConstant = "glintths-old-services";

# displays command-line tool usage information
function help() {
    Write-Host "Command line utility options:";
    Write-Host "`n`t-folderpath <C:\path\to\folder>";
    Write-Host "`n`t-[verbose] Option to display output on deleted folders";
    Write-Host "`n";
}

# recursively navigates through every directory and file within $basepath
# in order to find folders not comprised of the ignorables
function deleteUnwantedFolders($basepath) {
    # operator '@()' is used in order to force the variable to hold an array
    # (for the cases where there is only at most one subfolder or file)
    $subfolders = @(Get-ChildItem -directory $basepath);
    $parentName = Split-Path -Path $basepath -Leaf;

    if ($parentName -eq $parentDirConstant) {
        foreach ($subfolder in $subfolders) {
            
            if (!$ignorables.Contains($subfolder.Name)) {
                Remove-Item $subfolder.FullName -Recurse;
                if ($verbose) {
                    Write-Host "Deleted $($subfolder)!";
                }
            }
        }
    }

    return;
}

# displays help information if the switch is set
if ($help) {
    help;
    return;
}

# if ([System.IO.File]::Exists($folderpath)){
    deleteUnwantedFolders $folderpath;
# }
# else {
    # Write-Error "`nPath <$($folderpath)> does not exist";
# }
