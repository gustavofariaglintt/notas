param (
    [string]$packageName = $null
)

# CONSTANTS
$MASTER = 0;
$BETA = 1;
$ALPHA = 2;

$REGEXVERSION = "^\d\d\d\d\.\d\d?\.\d+(-alpha|-beta)?$";
$REGEXRELEASE = @(
    "\d$",      # [MASTER]  -> last character is a digit
    "beta$",    # [BETA]    -> ends with "beta"
    "alpha$"    # [ALPHA]   -> ends with "alpha"
);

# GET web request to nuget.glinttlab.com api
function getNugetPackageInfo($packageName) {
    return Invoke-RestMethod -URI "https://nuget.glinttlab.com/v3/registration/$($packageName)/index.json";
}

# filters all packages that don't match the specified pattern
function getNugetPackageValidVersions($package) {
    $versions = @();
    foreach ($entry in $package.items.items) {
        $version = $entry.catalogentry.version;
        if ($version -match $REGEXVERSION) {
            $versions += $version;
        }
    }

    return $versions;
}

# filters by release (branch from which version originated)
# allows $release to be null, in which case returned versions will still be sorted
function getNugetPackageVersionsByRelease($package, $release) {

    $versions = @();
    foreach ($version in getNugetPackageValidVersions $package) {
        if ($null -eq $release -Or ($version -match $REGEXRELEASE[$release])) {
            $versions += $version;
        }
    }

    return $versions | Sort-Object -Descending;
}

# retrives only the most recent package version
function getLatestNugetPackageVersion($package, $release) {
    return (getNugetPackageVersionsByRelease $package $release)[0];
}

function getLatestNugetPackageVersionByPackageName($packageName, $release) {
    $package = getNugetPackageInfo $packageName;
    return (getNugetPackageVersionsByRelease $package $release)[0];
}

# # Main
# getLatestNugetPackageVersionByPackageName $packageName $BETA

# # Test versions

# $versions = @(
#     "20.1.1",
#     "2020.1.1",
#     "2020.11.1",
#     "2020.111.1",
#     "20202.1.1",
#     "2020.11.111111",
#     "2020.11.11111-beta",
#     "2020.11.11111-alpha",
#     "2020.11.11111-gama"
# );

# foreach ($version in $versions) {
#     if ($version -match "^\d\d\d\d\.\d\d?\.\d+(-alpha|-beta)?$") {
#         Write-Host $version;
#     }
# }