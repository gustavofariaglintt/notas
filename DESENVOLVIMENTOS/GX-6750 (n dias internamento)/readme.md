# GX-6750

## Jira

**Epic** [GX-6750](https://glinttdev.atlassian.net/browse/GX-6750)

**User Story Dev** [GX-7241](https://glinttdev.atlassian.net/browse/GX-7241)

## Observações do Agendamento

**Cliente** xxx

**Patch** - GCX2001

**TDM** - Fernando Oliveira

**Produto** - Nuno Oliveira

**Frontend** - 12 de Dezembro - Tiago Tavares

**Qualidade** - 9 a 13 de Dezembro

### Descrição

Foi detetado pelo cliente necessidade de ter acesso, no histórico clínico, à informação do número de dias de internamento.

Desse modo, é representado na maquete seguinte a área em que é representado o número de dias de internamento para os episódios do tipo internamento.

### Mensagem Check-In

GX-6750 (epic)
GX-7241 (issue)
Cálculo da duração do internamento tendo em base EpisodeEndDate e EpisodeStartDate

### Changesets

|                |   DEV    |    CI    |   17R1   |   19R12  |
|---------------:|:--------:|:--------:|:--------:|:--------:|
| **Changesets** |  214113  |  214114  |  214987  |  214992  |

### Notas

Ao fazer load inicial da página: <http://demoapliis.glinttocm.com:6002/EPR_MOBILE/Glintths.Shell/PatientData_HistorySummary/GetPatientHistoryByCliInfoV2>

**Solução Frontend** Glintths.PatientData

+ Controller: PatientData_HistorySummary.GetPatientHistoryByCliInfoV2()
+ DataManagement: EpisodeManagement.GetPatientHistoryByCliInfoV2()

### Backend Configs

**Chave ginf_config** "S_CLINICAL_EPR_EPISODE_MANAGEMENT" (EpisodeManagement.cs)
    **value** ```"#BASE_URL#Cpchs.Modules.ActivitiesManagement.WS/Episode/WebService/EpisodeManagement.svc"```

**Projecto Backend** Cpchs.Modules.ActivitiesManagement.WebServices
    **IIS** Services/Glintths.WebServices
    **Lógica** Services/Glintths.BusinessRules
    **Data Model** Services/Glintths.BusinessEntities

**DB Method and Procedure** PCK_EPR_MOBILE_EXT.GetPatientHistoryByCliInfoV2

**Configurações** N/A

### Backend Lógica

+ Tudo na BD (ver getDuration.sql)
+ Ficheiros alterados:
  + TFS:
    + Services\BusinessEntities\Cpchs.Modules.ActivitiesManagement.BusinessEntities\Episode\EpisodePatientHistoryBE.cs
  + SVN:
    + MicrosoftID\DataBase\EPR\ReleaseAberta\BD_DOTNET_DEV\PCK_EPR_MOBILE_EXT_spec.sql
    + MicrosoftID\DataBase\EPR\ReleaseAberta\BD_DOTNET_DEV\PCK_EPR_MOBILE_EXT_body.sql

### Notas Frontend

É necessário actualizar service reference na solução Glintths.PatientData: DataAccess -> EpisodeManagementWS

<http://demoapliis.glinttocm.com:6002/GPLATFORM/Cpchs.Modules.ActivitiesManagement.WS/Episode/WebService/EpisodeManagement.svc>

Classe EpisodePatientHistory passa a ter um novo atributo EpisodeDuration
