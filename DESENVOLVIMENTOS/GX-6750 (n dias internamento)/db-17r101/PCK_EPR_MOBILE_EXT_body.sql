CREATE OR REPLACE 
PACKAGE BODY pck_epr_mobile_ext
IS
    PROCEDURE GetPatientSpecialities (
      p_sessionID       IN       VARCHAR2,
      p_t_doente        IN       VARCHAR2,
      p_doente          IN       VARCHAR2,
      p_n_mecan         IN       VARCHAR2,
      p_ambito_acesso   IN       VARCHAR2,
      p_result          OUT      pck_types.external_cursor)
    IS
         v_session VARCHAR2(30);
    BEGIN
        v_session := PreloadPatientHistory(p_sessionID, p_t_doente, p_doente, p_n_mecan);

        open p_result for
            select DISTINCT ep.cod_serv_valencia MEDICALSPECIALITYCOD, serv.descr_serv MEDICALSPECIALITYDESCR
            from sd_episodio_tmp ep, sd_serv serv
            where ep.cod_serv_valencia = serv.cod_serv
            and sessao = v_session
            and t_doente = p_t_doente
            and doente = p_doente
            and nvl(ep.flg_estado, nvl(pck_activities_episode_base.get_flg_estado_by_descr(ep.descr), 'X')) != 'A'
            order by serv.descr_serv asc;
    END;

    FUNCTION PreloadPatientHistory (p_sessionID   IN VARCHAR2,
                                    p_t_doente    IN VARCHAR2,
                                    p_doente      IN VARCHAR2,
                                    p_n_mecan     IN VARCHAR2)
        RETURN VARCHAR2
    IS
        v_session     VARCHAR2 (30);
        w_calculado   VARCHAR2 (100);
    BEGIN
        v_session := SUBSTR ( p_sessionID ,0 , 30 ); -- Necess�rio tratar, por vezes esta chave � maior que 30 caracteres, e na sd_episoio_tmp nao aceita sessionID com mais de 30 caracteres.

        BEGIN
            SELECT 'S'
              INTO w_calculado
              FROM sd_episodio_tmp
             WHERE     sessao = v_session
                   AND t_doente = p_t_doente
                   AND doente = p_doente
                   AND ROWNUM = 1;
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                w_calculado := 'N';
        END;

        IF (w_calculado = 'N')
        THEN
            preenche_episodio_tmp (p_t_doente,
                                   p_doente,
                                   v_session,
                                   p_n_mecan,
                                   'N',
                                   'S');

            preenche_episodio_tmp_aux (p_t_doente, p_doente, v_session);

            pck_activities_episode_base.prepareepisodehistory_v2 (p_t_doente,
                                                                  p_doente,
                                                                  v_session,
                                                                  NULL);
        END IF;

        RETURN v_session;
    END;

    FUNCTION DecodeFuncionalEpisodeType (p_t_doente     IN VARCHAR2,
                                      p_doente       IN VARCHAR2,
                                      p_t_episodio   IN VARCHAR2,
                                      p_episodio     IN VARCHAR2)
        RETURN VARCHAR2
    IS
        w_cod_serv        VARCHAR2 (100);
        w_t_act_med       VARCHAR2 (100);
        w_t_act_med_aux   VARCHAR2 (100);
        w_serv_urg        VARCHAR2 (100);
        w_flg_cirugico    VARCHAR2 (100);
    BEGIN
        --Fazer as conversoes necess�rias
        IF p_t_episodio = 'Consultas' THEN
            BEGIN
                SELECT cod_serv, t_act_med, t_act_med_aux
                  INTO w_cod_serv, w_t_act_med, w_t_act_med_aux
                  FROM sd_cons_marc
                 WHERE n_cons = p_episodio and t_doente = p_t_doente and doente = p_doente;

                --IF (w_t_act_med_aux IS NOT NULL OR w_t_act_med <> w_t_act_med_aux)
                if (nvl(w_t_act_med_aux, w_t_act_med) != w_t_act_med   )
                THEN
                    RETURN 'MCD';
                END IF;

                SELECT serv_urg
                  INTO w_serv_urg
                  FROM sd_serv
                 WHERE cod_serv = w_cod_serv;

                IF (w_serv_urg = 'S')
                THEN
                    RETURN 'URGENCIAAP';
                END IF;

                RETURN 'CONSULTA';
            EXCEPTION WHEN NO_DATA_FOUND THEN
                RETURN p_t_episodio;
            END;

        ELSIF (p_t_episodio = 'Internamentos') THEN
            RETURN 'INTERNAMENTO';

        ELSIF (p_t_episodio = 'Urgencias') THEN
            RETURN 'URGENCIA';

        ELSIF (p_t_episodio = 'Prescricoes') THEN
            RETURN 'PRESCRICOES';

        ELSIF (p_t_episodio = 'Ficha-ID') THEN
            RETURN 'ADMISSAO';

        ELSIF (p_t_episodio = 'Plano-Oper') THEN
            --RETURN 'CIRURGIA_AGENDAMENTO';
            RETURN 'CIRURGIA';

        ELSIF (p_t_episodio = 'Prog-Cirurgico') THEN
            --RETURN 'CIRURGIA_PREAGENDAMENTO';
            RETURN 'CIRURGIA';

        ELSIF (p_t_episodio = 'Pre-Intern') THEN
            begin
                select nvl(flg_cirurgico, 'S')
                into w_flg_cirugico
                from sd_ped_inte
                where n_ped_inte = p_episodio
                and t_doente = p_t_doente and doente = p_doente;

                IF (w_flg_cirugico = 'S') THEN
                    RETURN 'CIRURGIA_PROPOSTA';
                else
                    RETURN 'INTERNAMENTO_PEDIDO';
                END IF;
            EXCEPTION WHEN NO_DATA_FOUND THEN
                RETURN p_t_episodio;
            end;

        ELSIF (p_t_episodio = 'Reg-Oper') THEN
            RETURN 'CIRURGIA';

        ELSIF (p_t_episodio in ('Credenciais', 'Cons-Telef', 'Cons-Inter')) THEN
            RETURN 'PEDIDO_CONSULTA';

        ELSIF (p_t_episodio = 'Exame')
        THEN
            RETURN 'MCD';
        ELSIF (p_t_episodio = 'Ambulatorio')
        THEN
            RETURN 'AMBULATORIO';
        ELSIF (p_t_episodio = 'Hosp-Dia')
        THEN
            RETURN 'HOSP_DIA';
        ELSIF (p_t_episodio = 'Acidente')
        THEN
            RETURN 'SINISTRO';
        ELSIF (p_t_episodio = 'Sessoes')
        THEN
            RETURN 'SESSAO_TRATAMENTO';
        END IF;

        RETURN p_t_episodio;
    END;

    PROCEDURE preenche_episodio_tmp_aux (p_t_doente   IN VARCHAR2,
                                         p_doente     IN VARCHAR2,
                                         p_session    IN VARCHAR2)
    IS
        w_n_mecan                VARCHAR2 (100);
        w_nome_medico            VARCHAR2 (4000);
        w_cod_serv_valencia      VARCHAR2 (100);
        w_t_episodio_funcional   VARCHAR2 (100);
    BEGIN
        FOR ep IN (SELECT t_episodio, episodio
                     FROM sd_episodio_tmp
                    WHERE sessao = p_session AND cod_serv_valencia IS NULL)
        LOOP
            pcp_getmedico_e_serv (ep.t_episodio,
                                  ep.episodio,
                                  w_n_mecan,
                                  w_nome_medico,
                                  w_cod_serv_valencia);
            w_t_episodio_funcional :=
                DecodeFuncionalEpisodeType (p_t_doente,
                                            p_doente,
                                            ep.t_episodio,
                                            ep.episodio);

            UPDATE sd_episodio_tmp
               SET cod_serv_valencia = nvl(cod_serv_valencia, w_cod_serv_valencia),
                   n_mecan = w_n_mecan,
                   t_episodio_funcional = w_t_episodio_funcional
             WHERE     sessao = p_session
                   AND episodio = ep.episodio
                   AND t_episodio = ep.t_episodio
                   AND t_doente = p_t_doente
                   AND doente = p_doente;
        END LOOP;
    END;

    PROCEDURE GetFunctionalEpisodeTypes (
        p_sessionID       IN     VARCHAR2,
        p_t_doente        IN     VARCHAR2,
        p_doente          IN     VARCHAR2,
        p_n_mecan         IN     VARCHAR2,
        p_ambito_acesso          VARCHAR2,
        p_result             OUT pck_types.external_cursor)
    IS
        v_session VARCHAR2(30);
    BEGIN
        v_session := PreloadPatientHistory(p_sessionID, p_t_doente, p_doente, p_n_mecan);

        open p_result for
            select DISTINCT ep.t_episodio_funcional EPISODETYPE,
                            PCK_ACTIVITIES_EPISODE.GetFuncionalEpisodeTypeDescr(ep.t_episodio_funcional) EPISODETYPEDESCR
            from sd_episodio_tmp ep
            where sessao = v_session
            and t_doente = p_t_doente and doente = p_doente
            and nvl(ep.flg_estado, nvl(pck_activities_episode_base.get_flg_estado_by_descr(ep.descr), 'X')) != 'A'
            order by 2 asc;
    END;

    PROCEDURE GetPatientHistory (
        p_sessionID              IN     VARCHAR2,
        p_t_doente               IN     VARCHAR2,
        p_doente                 IN     VARCHAR2,
        p_t_episodio_funcional   IN     VARCHAR2,
        p_cod_serv               IN     VARCHAR2,
        p_n_mecan                IN     VARCHAR2,
        p_ambito_acesso          IN     VARCHAR2,
        p_intervalo_tempo        IN     VARCHAR2,
        p_pagina                 IN     VARCHAR2,
        p_n_paginas              IN     NUMBER,
        p_elemento_de            IN     NUMBER,
        p_elemento_a             IN     NUMBER,
        p_ordem_por              IN     VARCHAR2,
        p_result                    OUT pck_types.external_cursor)
    IS
        v_session   VARCHAR2 (30);
        w_dt_ini    DATE;
        w_dt_fim    DATE;
        w_ordem_por VARCHAR2(100);
    BEGIN

        v_session := PreloadPatientHistory(p_sessionID, p_t_doente, p_doente, p_n_mecan);

        -- CHAMAR AQUI O RESTO DO PROCESSAMENTO
        pck_activities_episode.GetInterval (p_intervalo_tempo, w_dt_ini, w_dt_fim);

        w_ordem_por :=  nvl(p_ordem_por, 'ASC');

        OPEN p_result FOR
            SELECT ep.t_doente PATIENTTYPE,
                   ep.doente PATIENTID,
                   ep.t_episodio EPISODETYPE,
                   ep.episodio EPISODEID,
                   ep.t_episodio_pai EPISODEPARENTTYPE,
                   ep.episodio_pai EPISODEPARENTID,
                   ep.dt_ini EPISODESTARTDATE,
                   DECODE (ep.dt_fim,
                           TO_DATE ('99991231', 'YYYYMMDD'), TO_DATE (NULL),
                           ep.dt_fim)
                       EPISODEENDDATE,
                   ep.descr EPISODEDESCR,
                   ep.EPISODESTATE EPISODESTATE,
                   ep.cod_serv_valencia SERVICEID,
                   serv.descr_serv SERVICEDESCR,
                   ep.n_mecan RESPONSIBLENMECAN,
                   pess.nome RESPONSIBLENAME,
                   pck_activities_episode.IsFutureEpisode (ep.dt_ini) EPISODEISFUTURE,
                   ep.t_episodio_funcional FUNCTIONALEPISODETYPE,
                   PCK_ACTIVITIES_EPISODE.GetFuncionalEpisodeTypeDescr (ep.t_episodio_funcional)
                       FUNCTIONALEPISODETYPEDESCR
              FROM (SELECT a.*,
                           NVL (a.flg_estado,
                                pck_activities_episode_base.get_flg_estado_epis (
                                    a.t_doente,
                                    a.doente,
                                    a.t_episodio,
                                    a.episodio))
                               EPISODESTATE
                      FROM sd_episodio_tmp a
                     WHERE t_doente = p_t_doente AND doente = p_doente
                           AND (p_cod_serv IS NULL
                                OR a.cod_serv_valencia = p_cod_serv)
                           AND (a.dt_fim IS NULL OR w_dt_ini <= a.dt_fim)
                           AND (a.dt_ini IS NULL OR w_dt_fim >= a.dt_ini)
                           AND (p_t_episodio_funcional IS NULL
                                OR a.t_episodio_funcional =
                                       p_t_episodio_funcional)
                           AND sessao = v_session
                    ORDER BY CASE WHEN w_ordem_por = 'ASC' THEN dt_ini END ASC,
                             CASE WHEN w_ordem_por = 'DESC' THEN dt_ini END DESC,
                             --para garantir que, se existirem epis�dios com a mesma data do Ficha-ID, entao o Ficha-ID � considerado sempre como o 1s
                             CASE WHEN w_ordem_por = 'ASC' THEN decode(t_episodio, 'Ficha-ID', 1, 10) END ASC,
                             CASE WHEN w_ordem_por = 'DESC' THEN decode(t_episodio, 'Ficha-ID', 1, 10) END DESC,
                             episodio) ep,
                   sd_serv serv,
                   sd_pess_hosp_def pess
             WHERE     ep.cod_serv_valencia = serv.cod_serv(+)
                   AND ep.n_mecan = pess.n_mecan(+)
                   AND NVL (EPISODESTATE, 'X') not in ('NAO_EXISTE', 'A')
                   and nvl(ep.t_episodio_pai, 'NULL') != 'Agrupador' --RF: martelan�o tempor�rio, para nao carregar os filhos dos Agrupadores
                   ;

    END GetPatientHistory;

    FUNCTION GetValueFromXml ( p_xml IN   xmltype,
                               p_atribute_name IN   Varchar2
                            )
                            return varchar2
    is
        p_return varchar2(500) := '_NADA_';
        v_extract_condition varchar2(500) := '//Parameters/Parameter[@name='''||p_atribute_name||''']/@value';
    begin
        if p_xml is not null and p_atribute_name is not null then
            select extractvalue( p_xml, v_extract_condition)
            into p_return
            from dual;
        end if;

        return p_return;
    end;

    FUNCTION HasEpisodeDefaultFilter(
        p_scop      IN VARCHAR2,
        p_t_episode IN VARCHAR2,
        p_n_mecan   IN VARCHAR2)
    RETURN VARCHAR2
    IS
        p_has_view  varchar2(2):='N';
        p_return    varchar2(10);
        p_result    pck_types.external_cursor;
    BEGIN

        pck_codifications_clinical.GetGenParamByAllFilterOneAuxV2('DEFAULT_EPISODE_FILTERS', null, p_n_mecan, null, p_t_episode, null, devolve_empresa(), '1', p_scop, null, p_result);

        if(p_result%isOPEN) then
            declare
                r pck_codifications_clinical.ParamGeral;
            begin
               LOOP
                   FETCH p_result into r;
                   exit when p_result%notfound;

                   p_return := GetValueFromXml(xmltype(r.xml_config), p_t_episode);

                   if upper(p_return) = 'TRUE'
                   then
                      p_has_view := 'S';
                   end if;

                   EXIT;
               END LOOP;

            end;
            close p_result;
        end if;

        return p_has_view;

    END;

    PROCEDURE GetPatientHistoryV2 (
        p_sessionid              IN     VARCHAR2,
        p_t_doente               IN     VARCHAR2,
        p_doente                 IN     VARCHAR2,
        p_t_episodio_funcional   IN     VARCHAR2,
        p_cod_serv               IN     VARCHAR2,
        p_n_mecan                IN     VARCHAR2,
        p_ambito_acesso          IN     VARCHAR2,
        p_intervalo_tempo        IN     VARCHAR2,
        p_pagenumber             IN     NUMBER,
        p_itemsperpage           IN     NUMBER,
        p_ordem_por              IN     VARCHAR2,
        p_ordem_tipo             IN     VARCHAR2,
        p_result                    OUT pck_types.external_cursor)
    IS
        v_session           VARCHAR2 (30);
        w_dt_ini            DATE;
        w_dt_fim            DATE;
        w_ordem_por         VARCHAR2 (100);
        w_page              NUMBER;
        w_perpage           NUMBER;
    BEGIN
         --PAGINACAO
        w_page := p_pagenumber;
        w_perpage := p_itemsperpage;
        IF (w_page IS NULL) THEN w_page := 0; END IF;
        IF (w_perpage IS NULL) THEN w_perpage := 10; END IF;
        w_page := w_page + 1;

        pck_activities_episode.getinterval (p_intervalo_tempo, w_dt_ini, w_dt_fim);
        w_ordem_por := NVL (p_ordem_por, 'ASC');

        OPEN p_result FOR
         SELECT
                a.patienttype patienttype,
                a.patientid patientid,
                a.episodetype episodetype,
                a.episodeid episodeid,
                a.episodestartdate episodestartdate,
                a.episodeenddate episodeenddate,
                a.episodedescr episodedescr,
                sd_serv.cod_serv serviceid,
                sd_serv.descr_serv servicedescr,
                pck_activities_episode.isfutureepisode (a.episodestartdate) episodeisfuture,
                epis_type.descr functionalepisodetypedescr
                FROM (SELECT *
                          FROM (SELECT t1.*, ROWNUM rnum
                                  FROM (
                                        SELECT
                                            patienttype,
                                            patientid,
                                            episodetype,
                                            episodeid,
                                            episodestartdate,
                                            episodeenddate,
                                            episodedescr,
                                            ord_idx
                                          FROM (SELECT /*+ ORDERED*/
                                                   distinct
                                                   resumo.t_doente patienttype,
                                                   resumo.doente patientid,
                                                   nvl(resumo.t_episodio, 'Ficha-ID') episodetype,
                                                   nvl(resumo.episodio, p_doente) episodeid,
                                                   epis.dt_ini episodestartdate,
                                                   DECODE (epis.dt_fim, TO_DATE ('99991231', 'YYYYMMDD'), TO_DATE (NULL), epis.dt_fim) episodeenddate,
                                                   epis.descr episodedescr,
                                                   to_char (epis.dt_ini, 'yyyy-mm-dd_hh24:mi')  ||  '_' || LPAD (resumo.episodio, '16', '0') ord_idx
                                                        FROM
                                                        (
                                                            SELECT a.t_doente,
                                                                   a.doente,
                                                                   a.episodio,
                                                                   a.t_episodio,
                                                                   replace (
                                                                    (case a.t_episodio
                                                                        when 'Internamentos' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                                        when 'Ambulatorio' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                                        else a.cod_serv end) , '#', '') cod_serv,
                                                                   a.provider_name,
                                                                   a.source_detail
                                                              FROM pc_resumo_info_doente a
                                                             WHERE  a.t_doente = p_t_doente
                                                               AND  a.doente = p_doente
                                                               AND (
                                                                (
                                                                    p_t_episodio_funcional IS NULL OR DecodeFuncionalEpisodeType (a.t_doente, a.doente, a.t_episodio, a.episodio) = p_t_episodio_funcional
                                                                )
                                                                OR
                                                                (
                                                                    p_t_episodio_funcional = 'DEFAULT' AND HasEpisodeDefaultFilter('HistSequencial', a.t_episodio, p_n_mecan) = 'S'
                                                                )
                                                               )
                                                           ) resumo,
                                                            sd_episodio epis
                                                           WHERE
                                                               exists (
                                                                    select 1
                                                                    from pc_param_resumo a
                                                                    where dominio = 'RESUMO_EPR'
                                                                        AND flg_activo = 'S'
                                                                        AND contexto = 'EPR_MOBILE'
                                                                        AND  a.tipo = resumo.provider_name
                                                                        AND  resumo.t_episodio LIKE NVL (a.t_episodio, '%')
                                                                        AND  (resumo.source_detail IS NULL OR resumo.source_detail LIKE NVL (a.subtipo, '%'))
                                                                        AND  NVL (resumo.cod_serv, '%') LIKE NVL (a.cod_serv, '%')
                                                               )
                                                               AND   (p_cod_serv is null or resumo.cod_serv in (select item from table (pck_common.split (p_cod_serv,'#' ))))
                                                               AND  resumo.t_doente = epis.t_doente
                                                               AND  resumo.doente = epis.doente
                                                               AND  resumo.t_episodio = epis.t_episodio
                                                               AND  resumo.episodio = epis.episodio
                                                               AND ( epis.dt_ini between w_dt_ini and w_dt_fim
                                                                    OR (epis.dt_fim between w_dt_ini and w_dt_fim and epis.dt_fim is not null and epis.dt_fim !=  to_date('31-12-9999','DD-MM-YYYY')))
                                                               AND nvl(epis.flg_estado, 'X') != 'A'
                                                               AND (epis.t_episodio != 'Consultas' or not exists (select 1 from sd_cons_marc where n_cons = epis.episodio and flag_estado = 'R'))

                                               union all

                                                select /*+ ORDERED*/
                                                    t_doente patienttype,
                                                    doente patientid,
                                                    t_episodio episodetype,
                                                    episodio episodeid,
                                                    dt_ini episodestartdate,
                                                    DECODE (dt_fim, TO_DATE ('99991231', 'YYYYMMDD'), TO_DATE (NULL), dt_fim) episodeenddate,
                                                    descr episodedescr,
                                                    to_char (epis.dt_ini, 'yyyy-mm-dd_hh24mi')  ||  '_' || LPAD (episodio, '16', '0') ord_idx
                                                from sd_episodio epis
                                                where t_doente = p_t_doente
                                                    AND doente = p_doente
                                                    and not exists (
                                                        SELECT
                                                           1
                                                        FROM
                                                        (
                                                            SELECT a.t_doente,
                                                                   a.doente,
                                                                   a.episodio,
                                                                   a.t_episodio,
                                                                   replace (
                                                                    (case a.t_episodio
                                                                        when 'Internamentos' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                                        when 'Ambulatorio' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                                        else a.cod_serv end) , '#', '') cod_serv,
                                                                   a.provider_name,
                                                                   a.source_detail
                                                              FROM pc_resumo_info_doente a
                                                             WHERE     a.t_doente = p_t_doente
                                                                   AND a.doente = p_doente
                                                                   AND (
                                                                    (
                                                                        p_t_episodio_funcional IS NULL OR DecodeFuncionalEpisodeType (a.t_doente, a.doente, a.t_episodio, a.episodio) = p_t_episodio_funcional
                                                                    )
                                                                    OR
                                                                    (
                                                                        p_t_episodio_funcional = 'DEFAULT' AND HasEpisodeDefaultFilter('HistSequencial', a.t_episodio, '132') = 'S'
                                                                    )
                                                                  )
                                                           ) resumo
                                                           WHERE
                                                               exists (
                                                                    select 1
                                                                    from pc_param_resumo a
                                                                    where dominio = 'RESUMO_EPR'
                                                                        and flg_activo = 'S'
                                                                        and contexto = 'EPR_MOBILE'
                                                                        and  a.tipo = resumo.provider_name
                                                                        AND  resumo.t_episodio LIKE NVL (a.t_episodio, '%')
                                                                        AND  (resumo.source_detail IS NULL OR resumo.source_detail LIKE NVL (a.subtipo, '%'))
                                                                        AND  NVL (resumo.cod_serv, '%') LIKE NVL (a.cod_serv, '%')

                                                               )
                                                               and  resumo.t_doente = epis.t_doente
                                                               AND  resumo.doente = epis.doente
                                                               AND  resumo.t_episodio = epis.t_episodio
                                                               AND  resumo.episodio = epis.episodio

                                                    )

                                                    and ( epis.dt_ini between w_dt_ini and w_dt_fim
                                                        or (epis.dt_fim between w_dt_ini and w_dt_fim and epis.dt_fim is not null and epis.dt_fim !=  to_date('31-12-9999','DD-MM-YYYY'))
                                                        )
                                                    and nvl(epis.flg_estado, 'X') != 'A'
                                                    and  (epis.t_episodio != 'Consultas' or not exists (select 1 from sd_cons_marc where n_cons = epis.episodio and flag_estado = 'R'))
                                                    AND (
                                                            (
                                                                p_t_episodio_funcional IS NULL OR DecodeFuncionalEpisodeType (epis.t_doente, epis.doente, epis.t_episodio, epis.episodio) = p_t_episodio_funcional
                                                            )
                                                            OR
                                                            (
                                                                p_t_episodio_funcional = 'DEFAULT' AND HasEpisodeDefaultFilter('HistSequencial', epis.t_episodio, p_n_mecan) = 'S'
                                                            )
                                                       )
                                                    and  (p_cod_serv is null or
                                                        (replace (pcf_devolve_serv_last(epis.t_episodio, epis.episodio, epis.t_doente, epis.doente) , '#', '')) in (select item from table (pck_common.split (p_cod_serv,'#' ))))

                                            ) k1
                                            ORDER BY CASE WHEN p_ordem_tipo = 'DESC' THEN ord_idx END DESC,
                                                     CASE WHEN p_ordem_tipo = 'ASC' THEN ord_idx END ASC
                                        ) t1
                             )
                     WHERE rnum < ( (w_page * w_perpage) + 1) and rnum >= ( ( (w_page - 1) * w_perpage) + 1)) a, sd_serv, pc_param_geral epis_type
                 where sd_serv.cod_serv(+) = (replace (pcf_devolve_serv_last(a.episodetype, a.episodeid, a.patienttype, a.patientid) , '#', ''))
                    and epis_type.dominio = 'HISTORY_EPIS_TYPES'
                    and epis_type.t_episodio = a.episodetype
                 ORDER BY CASE WHEN p_ordem_tipo = 'DESC' THEN ord_idx END DESC,
                    CASE WHEN p_ordem_tipo = 'ASC' THEN ord_idx END ASC;

    END getpatienthistoryv2;

    PROCEDURE GetPatientHistoryByCliInfo (
        p_sessionid              IN     VARCHAR2,
        p_t_doente               IN     VARCHAR2,
        p_doente                 IN     VARCHAR2,
        p_selected_t_episodio    IN     VARCHAR2,
        p_selected_episodio      IN     VARCHAR2,
        p_t_episodio             IN     VARCHAR2,
        p_cod_serv               IN     VARCHAR2,
        p_n_mecan                IN     VARCHAR2,
        p_tipo                   IN     VARCHAR2,
        p_summay_domain          IN     VARCHAR2,
        p_contexto_acesso        IN     VARCHAR2,
        p_intervalo_tempo        IN     VARCHAR2,
        p_pagenumber             IN     NUMBER,
        p_itemsperpage           IN     NUMBER,
        p_ordem_por              IN     VARCHAR2,
        p_ordem_tipo             IN     VARCHAR2,
        p_result                    OUT pck_types.external_cursor)
    IS
        v_session           VARCHAR2 (30);
        w_dt_ini date := to_date('01-01-0001 00:00','DD-MM-YYYY HH24:MI');
        w_dt_fim date := to_date('31-12-9999 23:59','DD-MM-YYYY HH24:MI');

        w_ordem_por         VARCHAR2 (100);
        w_page              NUMBER;
        w_perpage           NUMBER;
        --w_tipo              varchar2(4000);
    BEGIN
        insertlogepr ('GetPatientHistoryByCliInfo p_t_doente:' || p_t_doente
                        || '-p_doente:' ||  p_doente
                        || '-p_selected_t_episodio:' ||  p_selected_t_episodio
                        || '-p_selected_episodio:' ||  p_selected_episodio
                        || '-p_t_episodio:' ||  p_t_episodio
                        || '-p_cod_serv:' ||  p_cod_serv
                        || '-p_n_mecan:' ||  p_n_mecan
                        || '-p_tipo:' ||  p_tipo
                        || '-p_summay_domain:' ||  p_summay_domain
                        || '-p_intervalo_tempo:' ||  p_intervalo_tempo);

        --PAGINACAO
        w_page := p_pagenumber;
        w_perpage := p_itemsperpage;
        IF (w_page IS NULL) THEN w_page := 0; END IF;
        IF (w_perpage IS NULL) THEN w_perpage := 10; END IF;
        w_page := w_page + 1;

        if nvl(p_intervalo_tempo, 'EPISODE') != 'EPISODE' then
            pck_activities_episode.getinterval (p_intervalo_tempo, w_dt_ini, w_dt_fim);
        end if;

        if p_tipo like '%#NO_INFO%' then

        OPEN p_result FOR
            SELECT a.*, sd_serv.descr_serv servicedescr,
                sd_serv.cod_serv serviceid,
                epis_type.descr functionalepisodetypedescr
              FROM (SELECT *
                      FROM (SELECT t1.*, ROWNUM rnum
                              FROM (
                                    SELECT
                                        patienttype,
                                        patientid,
                                        episodetype,
                                        episodeid,
                                        episodestartdate,
                                        episodeenddate,
                                        episodedescr,
                                        ord_idx
                                      FROM (
                                        SELECT /*+ ORDERED*/
                                       distinct
                                       resumo.t_doente patienttype,
                                       resumo.doente patientid,
                                       nvl(resumo.t_episodio, 'Ficha-ID') episodetype,
                                       nvl(resumo.episodio, p_doente) episodeid,
                                       epis.dt_ini episodestartdate,
                                       DECODE (epis.dt_fim, TO_DATE ('99991231', 'YYYYMMDD'), TO_DATE (NULL), epis.dt_fim) episodeenddate,
                                       epis.descr episodedescr,
                                       to_char (epis.dt_ini, 'yyyy-mm-dd_hh24mi') ||  '_' || to_char (epis.dt_fim, 'yyyy-mm-dd_hh24mi') ||  '_' || to_char (epis.dt_cri, 'yyyy-mm-dd_hh24mi') ||  '_' || LPAD (epis.episodio, '16', '0') ord_idx
                                            FROM
                                            (
                                                SELECT a.t_doente,
                                                       a.doente,
                                                       a.episodio,
                                                       a.t_episodio,
                                                       replace (
                                                        (case a.t_episodio
                                                            when 'Internamentos' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                            when 'Ambulatorio' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                            else a.cod_serv end) , '#', '') cod_serv,
                                                       a.provider_name,
                                                       a.source_detail
                                                  FROM pc_resumo_info_doente a
                                                 WHERE     a.t_doente = p_t_doente
                                                       AND a.doente = p_doente
                                                       and (p_t_episodio is null or a.t_episodio in (select item from table (pck_common.split (p_t_episodio,'#' ))))
                                                       and (nvl(p_intervalo_tempo, 'X') != 'EPISODE' or (a.t_episodio = p_selected_t_episodio and a.episodio = p_selected_episodio))
                                               ) resumo,
                                                sd_episodio epis
                                               WHERE
                                                   exists (
                                                        select 1
                                                        from pc_param_resumo a
                                                        where dominio = p_summay_domain
                                                            and flg_activo = 'S'
                                                            and contexto = p_contexto_acesso
                                                            and ( p_tipo like '%#ALL#%'or codigo in (select item from table (pck_common.split (p_tipo,'#' ))))
                                                            and  a.tipo = resumo.provider_name
                                                            AND  resumo.t_episodio LIKE NVL (a.t_episodio, '%')
                                                            AND  (resumo.source_detail IS NULL OR resumo.source_detail LIKE NVL (a.subtipo, '%'))
                                                            AND  NVL (resumo.cod_serv, '%') LIKE NVL (a.cod_serv, '%')
                                                   )
                                                   and   (p_cod_serv is null or resumo.cod_serv in (select item from table (pck_common.split (p_cod_serv,'#' ))))
                                                   and  resumo.t_doente = epis.t_doente
                                                   AND  resumo.doente = epis.doente
                                                   AND  resumo.t_episodio = epis.t_episodio
                                                   AND  resumo.episodio = epis.episodio

                                                   and ( epis.dt_ini between w_dt_ini and w_dt_fim
                                                        or (epis.dt_fim between w_dt_ini and w_dt_fim and epis.dt_fim is not null and epis.dt_fim !=  to_date('31-12-9999','DD-MM-YYYY')))

                                                   and nvl(epis.flg_estado, 'X') != 'A'
                                                   and (epis.t_episodio != 'Consultas' or not exists (select 1 from sd_cons_marc where n_cons = epis.episodio and flag_estado = 'R'))


                                    union all

                                    select /*+ ORDERED*/
                                        t_doente patienttype,
                                        doente patientid,
                                        t_episodio episodetype,
                                        episodio episodeid,
                                        dt_ini episodestartdate,
                                        DECODE (dt_fim, TO_DATE ('99991231', 'YYYYMMDD'), TO_DATE (NULL), dt_fim) episodeenddate,
                                        descr episodedescr,
                                        to_char (epis.dt_ini, 'yyyy-mm-dd_hh24mi') ||  '_' || to_char (epis.dt_fim, 'yyyy-mm-dd_hh24mi') ||  '_' || to_char (epis.dt_cri, 'yyyy-mm-dd_hh24mi') ||  '_' || LPAD (epis.episodio, '16', '0') ord_idx
                                    from sd_episodio epis
                                    where t_doente = p_t_doente
                                        AND doente = p_doente
                                        and p_tipo like '%#NO_INFO%'
                                        and (nvl(p_intervalo_tempo, 'X') != 'EPISODE' or (epis.t_episodio = p_selected_t_episodio and epis.episodio = p_selected_episodio))
                                        and not exists (
                                            SELECT
                                               1
                                            FROM
                                            (
                                                SELECT a.t_doente,
                                                       a.doente,
                                                       a.episodio,
                                                       a.t_episodio,
                                                       replace (
                                                        (case a.t_episodio
                                                            when 'Internamentos' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                            when 'Ambulatorio' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                            else a.cod_serv end) , '#', '') cod_serv,
                                                       a.provider_name,
                                                       a.source_detail
                                                  FROM pc_resumo_info_doente a
                                                 WHERE     a.t_doente = p_t_doente
                                                       AND a.doente = p_doente
                                                       and (p_t_episodio is null or a.t_episodio in (select item from table (pck_common.split (p_t_episodio,'#' ))))
                                                       and (nvl(p_intervalo_tempo, 'X') != 'EPISODE' or (a.t_episodio = p_selected_t_episodio and a.episodio = p_selected_episodio))
                                               ) resumo
                                               WHERE
                                                   exists (
                                                        select 1
                                                        from pc_param_resumo a
                                                        where dominio = p_summay_domain
                                                            and flg_activo = 'S'
                                                            and contexto = p_contexto_acesso
                                                            and  a.tipo = resumo.provider_name
                                                            AND  resumo.t_episodio LIKE NVL (a.t_episodio, '%')
                                                            AND  (resumo.source_detail IS NULL OR resumo.source_detail LIKE NVL (a.subtipo, '%'))
                                                            AND  NVL (resumo.cod_serv, '%') LIKE NVL (a.cod_serv, '%')

                                                   )
                                                   and  resumo.t_doente = epis.t_doente
                                                   AND  resumo.doente = epis.doente
                                                   AND  resumo.t_episodio = epis.t_episodio
                                                   AND  resumo.episodio = epis.episodio

                                        )

                                        and ( epis.dt_ini between w_dt_ini and w_dt_fim
                                            or (epis.dt_fim between w_dt_ini and w_dt_fim and epis.dt_fim is not null and epis.dt_fim !=  to_date('31-12-9999','DD-MM-YYYY'))
                                            )
                                        and nvl(epis.flg_estado, 'X') != 'A'
                                        and (epis.t_episodio != 'Consultas' or not exists (select 1 from sd_cons_marc where n_cons = epis.episodio and flag_estado = 'R'))
                                        and (p_t_episodio is null or epis.t_episodio in (select item from table (pck_common.split (p_t_episodio,'#' ))))
                                        and   (p_cod_serv is null or
                                            (replace (pcf_devolve_serv_last(epis.t_episodio, epis.episodio, epis.t_doente, epis.doente) , '#', '')) in (select item from table (pck_common.split (p_cod_serv,'#' ))))
                                        ) k1
                                        ORDER BY CASE WHEN p_ordem_tipo = 'DESC' THEN ord_idx END DESC,
                                                 CASE WHEN p_ordem_tipo = 'ASC' THEN ord_idx END ASC
                                    ) t1
                         )
                 WHERE rnum < ( (w_page * w_perpage) + 1) and rnum >= ( ( (w_page - 1) * w_perpage) + 1)) a, sd_serv, pc_param_geral epis_type
                 where sd_serv.cod_serv(+) = (replace (pcf_devolve_serv_last(a.episodetype, a.episodeid, a.patienttype, a.patientid) , '#', ''))
                    and epis_type.dominio = 'HISTORY_EPIS_TYPES'
                    and epis_type.t_episodio = a.episodetype
                 ORDER BY CASE WHEN p_ordem_tipo = 'DESC' THEN ord_idx END DESC,
                    CASE WHEN p_ordem_tipo = 'ASC' THEN ord_idx END ASC
                 ;

        else

        OPEN p_result FOR
            SELECT a.*, sd_serv.descr_serv servicedescr,
                sd_serv.cod_serv serviceid,
                epis_type.descr functionalepisodetypedescr
              FROM (SELECT *
                      FROM (SELECT t1.*, ROWNUM rnum
                              FROM (
                                    SELECT
                                        patienttype,
                                        patientid,
                                        episodetype,
                                        episodeid,
                                        episodestartdate,
                                        episodeenddate,
                                        episodedescr,
                                        ord_idx
                                      FROM (SELECT /*+ ORDERED*/
                                               distinct
                                               resumo.t_doente patienttype,
                                               resumo.doente patientid,
                                               nvl(resumo.t_episodio, 'Ficha-ID') episodetype,
                                               nvl(resumo.episodio, p_doente) episodeid,
                                               epis.dt_ini episodestartdate,
                                               DECODE (epis.dt_fim, TO_DATE ('99991231', 'YYYYMMDD'), TO_DATE (NULL), epis.dt_fim) episodeenddate,
                                               epis.descr episodedescr,
                                               to_char (epis.dt_ini, 'yyyy-mm-dd_hh24mi') ||  '_' || to_char (epis.dt_fim, 'yyyy-mm-dd_hh24mi') ||  '_' || to_char (epis.dt_cri, 'yyyy-mm-dd_hh24mi') ||  '_' || LPAD (epis.episodio, '16', '0') ord_idx
                                                    FROM
                                                    (
                                                        SELECT a.t_doente,
                                                               a.doente,
                                                               a.episodio,
                                                               a.t_episodio,
                                                               replace (
                                                                (case a.t_episodio
                                                                    when 'Internamentos' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                                    when 'Ambulatorio' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                                    else a.cod_serv end) , '#', '') cod_serv,
                                                               a.provider_name,
                                                               a.source_detail
                                                          FROM pc_resumo_info_doente a
                                                         WHERE     a.t_doente = p_t_doente
                                                               AND a.doente = p_doente
                                                               AND (p_t_episodio is null or a.t_episodio in (select item from table (pck_common.split (p_t_episodio,'#' ))))
                                                               AND (nvl(p_intervalo_tempo, 'X') != 'EPISODE' or (a.t_episodio = p_selected_t_episodio and a.episodio = p_selected_episodio))
                                                               AND provider_name not in ('DOCUMENTOS', 'RESULTADOS')
                                                       ) resumo,
                                                        sd_episodio epis
                                                       WHERE
                                                           exists (
                                                                select 1
                                                                from pc_param_resumo a
                                                                where dominio = p_summay_domain
                                                                    and flg_activo = 'S'
                                                                    and contexto = p_contexto_acesso
                                                                    and (/*p_tipo is null or*/ p_tipo like '%#ALL#%'or codigo in (select item from table (pck_common.split (p_tipo,'#' ))))
                                                                    and  a.tipo = resumo.provider_name
                                                                    AND  resumo.t_episodio LIKE NVL (a.t_episodio, '%')
                                                                    AND  (resumo.source_detail IS NULL OR resumo.source_detail LIKE NVL (a.subtipo, '%'))
                                                                    AND  NVL (resumo.cod_serv, '%') LIKE NVL (a.cod_serv, '%')
                                                                union all
                                                                select 1 from dual where p_tipo like '%#NO_INFO%'

                                                           )
                                                           and   (p_cod_serv is null or resumo.cod_serv in (select item from table (pck_common.split (p_cod_serv,'#' ))))
                                                           and  resumo.t_doente = epis.t_doente
                                                           AND  resumo.doente = epis.doente
                                                           AND  resumo.t_episodio = epis.t_episodio
                                                           AND  resumo.episodio = epis.episodio
                                                          and ( epis.dt_ini between w_dt_ini and w_dt_fim
                                                                or (epis.dt_fim between w_dt_ini and w_dt_fim and epis.dt_fim is not null and epis.dt_fim !=  to_date('31-12-9999','DD-MM-YYYY')))

                                                           and nvl(epis.flg_estado, 'X') != 'A'
                                                           and (epis.t_episodio != 'Consultas' or not exists (select 1 from sd_cons_marc where n_cons = epis.episodio and flag_estado = 'R'))


                                        ) k1
                                        ORDER BY CASE WHEN p_ordem_tipo = 'DESC' THEN ord_idx END DESC,
                                                 CASE WHEN p_ordem_tipo = 'ASC' THEN ord_idx END ASC
                                    ) t1
                         --WHERE ROWNUM < ( (w_page * w_perpage) + 1)
                         )
                 WHERE rnum < ( (w_page * w_perpage) + 1) AND rnum >= ( ( (w_page - 1) * w_perpage) + 1)) a, sd_serv, pc_param_geral epis_type
                 WHERE sd_serv.cod_serv(+) = (replace (pcf_devolve_serv_last(a.episodetype, a.episodeid, a.patienttype, a.patientid) , '#', ''))
                    AND epis_type.dominio = 'HISTORY_EPIS_TYPES'
                    AND epis_type.t_episodio = a.episodetype
                    ORDER BY CASE WHEN p_ordem_tipo = 'DESC' THEN ord_idx END DESC,
                    CASE WHEN          p_ordem_tipo = 'ASC' THEN ord_idx END ASC;
        end if;

    END GetPatientHistoryByCliInfo;

    FUNCTION GetDuration (
        p_n_int IN VARCHAR2
    )
        RETURN VARCHAR2
    IS
        p_dt_ini DATE;
        p_dt_end DATE;
    BEGIN
        BEGIN
            SELECT dt_int, dt_alta
            INTO p_dt_ini, p_dt_end
            FROM sd_ints
            WHERE n_int = p_n_int;

            EXCEPTION WHEN NO_DATA_FOUND THEN
                RETURN NULL;
        END;

        RETURN trunc(
            trunc(nvl(p_dt_end, sysdate)) - (trunc(p_dt_ini))
        );
    END;

    PROCEDURE GetPatientHistoryByCliInfoV2 (
        p_sessionid              IN     VARCHAR2,
        p_t_doente               IN     VARCHAR2,
        p_doente                 IN     VARCHAR2,
        p_selected_t_episodio    IN     VARCHAR2,
        p_selected_episodio      IN     VARCHAR2,
        p_t_episodio             IN     VARCHAR2,
        p_cod_serv               IN     VARCHAR2,
        p_n_mecan                IN     VARCHAR2,
        p_tipo                   IN     VARCHAR2,
        p_summay_domain          IN     VARCHAR2,
        p_contexto_acesso        IN     VARCHAR2,
        p_intervalo_tempo        IN     VARCHAR2,
        p_episodios_futuros      IN     VARCHAR2,
        p_pagenumber             IN     NUMBER,
        p_itemsperpage           IN     NUMBER,
        p_ordem_por              IN     VARCHAR2,
        p_ordem_tipo             IN     VARCHAR2,
        p_result                    OUT pck_types.external_cursor)
    IS
        v_session           VARCHAR2 (30);
        w_dt_ini date := to_date('01-01-0001 00:00','DD-MM-YYYY HH24:MI');
        w_dt_fim date := to_date('31-12-9999 23:59','DD-MM-YYYY HH24:MI');

        w_ordem_por         VARCHAR2 (100);
        w_page              NUMBER;
        w_perpage           NUMBER;
        --w_tipo              varchar2(4000);
    BEGIN
        insertlogepr ('GetPatientHistoryByCliInfoV2 p_t_doente:' || p_t_doente
                        || '-p_doente:' ||  p_doente
                        || '-p_selected_t_episodio:' ||  p_selected_t_episodio
                        || '-p_selected_episodio:' ||  p_selected_episodio
                        || '-p_t_episodio:' ||  p_t_episodio
                        || '-p_cod_serv:' ||  p_cod_serv
                        || '-p_n_mecan:' ||  p_n_mecan
                        || '-p_tipo:' ||  p_tipo
                        || '-p_summay_domain:' ||  p_summay_domain
                        || '-p_intervalo_tempo:' ||  p_intervalo_tempo
                        || '-p_episodios_futuros: ' || p_episodios_futuros);

        --PAGINACAO
        w_page := p_pagenumber;
        w_perpage := p_itemsperpage;
        IF (w_page IS NULL) THEN w_page := 0; END IF;
        IF (w_perpage IS NULL) THEN w_perpage := 10; END IF;
        w_page := w_page + 1;

        if nvl(p_intervalo_tempo, 'EPISODE') != 'EPISODE' then
            pck_activities_episode.getinterval (p_intervalo_tempo, w_dt_ini, w_dt_fim);
        end if;

        if nvl(p_episodios_futuros, 'N') = 'S' then

            w_dt_fim := to_date('31-12-9999 23:59','DD-MM-YYYY HH24:MI');
        else
            w_dt_fim := sysdate;
        end if;

        if p_tipo like '%#NO_INFO%' then

        OPEN p_result FOR
            SELECT a.*, sd_serv.descr_serv servicedescr,
                sd_serv.cod_serv serviceid,
                epis_type.descr functionalepisodetypedescr
              FROM (SELECT *
                      FROM (SELECT t1.*, ROWNUM rnum
                              FROM (
                                    SELECT
                                        patienttype,
                                        patientid,
                                        episodetype,
                                        episodeid,
                                        episodestartdate,
                                        episodeenddate,
                                        episodedescr,
                                        ord_idx,
                                        episodetypedescr
                                      FROM (
                                        SELECT /*+ ORDERED*/
                                       distinct
                                       resumo.t_doente patienttype,
                                       resumo.doente patientid,
                                       nvl(resumo.t_episodio, 'Ficha-ID') episodetype,
                                       nvl(resumo.episodio, p_doente) episodeid,
                                       epis.dt_ini episodestartdate,
                                       DECODE (epis.dt_fim, TO_DATE ('99991231', 'YYYYMMDD'), TO_DATE (NULL), epis.dt_fim) episodeenddate,
                                       epis.descr episodedescr,
                                       decode (epis.t_episodio, 'Ficha-ID', '1_', '2_') ||
                                       to_char (epis.dt_ini, 'yyyy-mm-dd_hh24mi') ||  '_' || to_char (epis.dt_fim, 'yyyy-mm-dd_hh24mi') ||  '_' || to_char (epis.dt_cri, 'yyyy-mm-dd_hh24mi') ||  '_' || LPAD (epis.episodio, '16', '0') ord_idx,
                                       t_epis.descr episodetypedescr
                                            FROM
                                            (
                                                SELECT a.t_doente,
                                                       a.doente,
                                                       a.episodio,
                                                       a.t_episodio,
                                                       replace (
                                                        (case a.t_episodio
                                                            when 'Internamentos' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                            when 'Ambulatorio' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                            else a.cod_serv end) , '#', '') cod_serv,
                                                       a.provider_name,
                                                       a.source_detail
                                                  FROM pc_resumo_info_doente a
                                                 WHERE     a.t_doente = p_t_doente
                                                       AND a.doente = p_doente
                                                       and (p_t_episodio is null or a.t_episodio in (select item from table (pck_common.split (p_t_episodio,'#' ))))
                                                       and (nvl(p_intervalo_tempo, 'X') != 'EPISODE' or (a.t_episodio = p_selected_t_episodio and a.episodio = p_selected_episodio))
                                               ) resumo,
                                                sd_episodio epis, sd_t_episodio t_epis
                                               WHERE epis.t_episodio = t_epis.t_episodio
                                               AND exists (
                                                        select 1
                                                        from pc_param_resumo a
                                                        where dominio = p_summay_domain
                                                            and flg_activo = 'S'
                                                            and contexto = p_contexto_acesso
                                                            and ( p_tipo like '%#ALL#%'or codigo in (select item from table (pck_common.split (p_tipo,'#' ))))
                                                            and  a.tipo = resumo.provider_name
                                                            AND  resumo.t_episodio LIKE NVL (a.t_episodio, '%')
                                                            AND  (resumo.source_detail IS NULL OR resumo.source_detail LIKE NVL (a.subtipo, '%'))
                                                            AND  NVL (resumo.cod_serv, '%') LIKE NVL (a.cod_serv, '%')
                                                   )
                                                   and   (p_cod_serv is null or resumo.cod_serv in (select item from table (pck_common.split (p_cod_serv,'#' ))))
                                                   and  resumo.t_doente = epis.t_doente
                                                   AND  resumo.doente = epis.doente
                                                   AND  resumo.t_episodio = epis.t_episodio
                                                   AND  resumo.episodio = epis.episodio

                                                   and ( epis.dt_ini between w_dt_ini and w_dt_fim
                                                        or (epis.dt_fim between w_dt_ini and w_dt_fim and epis.dt_fim is not null and epis.dt_fim !=  to_date('31-12-9999','DD-MM-YYYY')))

                                                   and nvl(epis.flg_estado, 'X') != 'A'
                                                   and (epis.t_episodio != 'Consultas' or not exists (select 1 from sd_cons_marc where n_cons = epis.episodio and flag_estado = 'R'))


                                    union all

                                    select /*+ ORDERED*/
                                        t_doente patienttype,
                                        doente patientid,
                                        epis.t_episodio episodetype,
                                        episodio episodeid,
                                        dt_ini episodestartdate,
                                        DECODE (dt_fim, TO_DATE ('99991231', 'YYYYMMDD'), TO_DATE (NULL), dt_fim) episodeenddate,
                                        epis.descr episodedescr,
                                        decode (epis.t_episodio, 'Ficha-ID', '1_', '2_') ||
                                        to_char (epis.dt_ini, 'yyyy-mm-dd_hh24mi') ||  '_' || to_char (epis.dt_fim, 'yyyy-mm-dd_hh24mi') ||  '_' || to_char (epis.dt_cri, 'yyyy-mm-dd_hh24mi') ||  '_' || LPAD (epis.episodio, '16', '0') ord_idx,
                                        t_epis.descr episodetypedescr
                                    from sd_episodio epis, sd_t_episodio t_epis
                                    where epis.t_episodio = t_epis.t_episodio
                                        AND t_doente = p_t_doente
                                        AND doente = p_doente
                                        and p_tipo like '%#NO_INFO%'
                                        and (nvl(p_intervalo_tempo, 'X') != 'EPISODE' or (epis.t_episodio = p_selected_t_episodio and epis.episodio = p_selected_episodio))
                                        and not exists (
                                            SELECT
                                               1
                                            FROM
                                            (
                                                SELECT a.t_doente,
                                                       a.doente,
                                                       a.episodio,
                                                       a.t_episodio,
                                                       replace (
                                                        (case a.t_episodio
                                                            when 'Internamentos' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                            when 'Ambulatorio' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                            else a.cod_serv end) , '#', '') cod_serv,
                                                       a.provider_name,
                                                       a.source_detail
                                                  FROM pc_resumo_info_doente a
                                                 WHERE     a.t_doente = p_t_doente
                                                       AND a.doente = p_doente
                                                       and (p_t_episodio is null or a.t_episodio in (select item from table (pck_common.split (p_t_episodio,'#' ))))
                                                       and (nvl(p_intervalo_tempo, 'X') != 'EPISODE' or (a.t_episodio = p_selected_t_episodio and a.episodio = p_selected_episodio))
                                               ) resumo
                                               WHERE
                                                   exists (
                                                        select 1
                                                        from pc_param_resumo a
                                                        where dominio = p_summay_domain
                                                            and flg_activo = 'S'
                                                            and contexto = p_contexto_acesso
                                                            and  a.tipo = resumo.provider_name
                                                            AND  resumo.t_episodio LIKE NVL (a.t_episodio, '%')
                                                            AND  (resumo.source_detail IS NULL OR resumo.source_detail LIKE NVL (a.subtipo, '%'))
                                                            AND  NVL (resumo.cod_serv, '%') LIKE NVL (a.cod_serv, '%')

                                                   )
                                                   and  resumo.t_doente = epis.t_doente
                                                   AND  resumo.doente = epis.doente
                                                   AND  resumo.t_episodio = epis.t_episodio
                                                   AND  resumo.episodio = epis.episodio

                                        )

                                        and ( epis.dt_ini between w_dt_ini and w_dt_fim
                                            or (epis.dt_fim between w_dt_ini and w_dt_fim and epis.dt_fim is not null and epis.dt_fim !=  to_date('31-12-9999','DD-MM-YYYY'))
                                            )
                                        and nvl(epis.flg_estado, 'X') != 'A'
                                        and (epis.t_episodio != 'Consultas' or not exists (select 1 from sd_cons_marc where n_cons = epis.episodio and flag_estado = 'R'))
                                        and (p_t_episodio is null or epis.t_episodio in (select item from table (pck_common.split (p_t_episodio,'#' ))))
                                        and   (p_cod_serv is null or
                                            (replace (pcf_devolve_serv_last(epis.t_episodio, epis.episodio, epis.t_doente, epis.doente) , '#', '')) in (select item from table (pck_common.split (p_cod_serv,'#' ))))
                                        ) k1
                                        ORDER BY CASE WHEN p_ordem_tipo = 'DESC' THEN ord_idx END DESC,
                                                 CASE WHEN p_ordem_tipo = 'ASC' THEN ord_idx END ASC
                                    ) t1
                         )
                 WHERE rnum < ( (w_page * w_perpage) + 1) and rnum >= ( ( (w_page - 1) * w_perpage) + 1)) a, sd_serv, pc_param_geral epis_type
                 where sd_serv.cod_serv(+) = (replace (pcf_devolve_serv_last(a.episodetype, a.episodeid, a.patienttype, a.patientid) , '#', ''))
                    and epis_type.dominio = 'HISTORY_EPIS_TYPES'
                    and epis_type.t_episodio = a.episodetype
                 ORDER BY CASE WHEN p_ordem_tipo = 'DESC' THEN ord_idx END DESC,
                    CASE WHEN p_ordem_tipo = 'ASC' THEN ord_idx END ASC
                 ;

        else

        OPEN p_result FOR
            SELECT a.*, sd_serv.descr_serv servicedescr,
                sd_serv.cod_serv serviceid,
                epis_type.descr functionalepisodetypedescr
              FROM (SELECT *
                      FROM (SELECT t1.*, ROWNUM rnum
                              FROM (
                                    SELECT
                                        patienttype,
                                        patientid,
                                        episodetype,
                                        episodeid,
                                        episodestartdate,
                                        episodeenddate,
                                        episodedescr,
                                        ord_idx,
                                        episodetypedescr
                                      FROM (SELECT /*+ ORDERED*/
                                               distinct
                                               resumo.t_doente patienttype,
                                               resumo.doente patientid,
                                               nvl(resumo.t_episodio, 'Ficha-ID') episodetype,
                                               nvl(resumo.episodio, p_doente) episodeid,
                                               epis.dt_ini episodestartdate,
                                               DECODE (epis.dt_fim, TO_DATE ('99991231', 'YYYYMMDD'), TO_DATE (NULL), epis.dt_fim) episodeenddate,
                                               epis.descr episodedescr,
                                               decode (epis.t_episodio, 'Ficha-ID', '1_', '2_') ||
                                               to_char (epis.dt_ini, 'yyyy-mm-dd_hh24mi') ||  '_' || to_char (epis.dt_fim, 'yyyy-mm-dd_hh24mi') ||  '_' || to_char (epis.dt_cri, 'yyyy-mm-dd_hh24mi') ||  '_' || LPAD (epis.episodio, '16', '0') ord_idx,
                                               t_epis.descr episodetypedescr
                                                    FROM
                                                    (
                                                        SELECT a.t_doente,
                                                               a.doente,
                                                               a.episodio,
                                                               a.t_episodio,
                                                               replace (
                                                                (case a.t_episodio
                                                                    when 'Internamentos' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                                    when 'Ambulatorio' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                                                    else a.cod_serv end) , '#', '') cod_serv,
                                                               a.provider_name,
                                                               a.source_detail
                                                          FROM pc_resumo_info_doente a
                                                         WHERE     a.t_doente = p_t_doente
                                                               AND a.doente = p_doente
                                                               AND (p_t_episodio is null or a.t_episodio in (select item from table (pck_common.split (p_t_episodio,'#' ))))
                                                               AND (nvl(p_intervalo_tempo, 'X') != 'EPISODE' or (a.t_episodio = p_selected_t_episodio and a.episodio = p_selected_episodio))
                                                               AND provider_name not in ('DOCUMENTOS', 'RESULTADOS')
                                                       ) resumo,
                                                        sd_episodio epis, sd_t_episodio t_epis
                                                       WHERE
                                                       epis.t_episodio = t_epis.t_episodio AND
                                                           exists (
                                                                select 1
                                                                from pc_param_resumo a
                                                                where dominio = p_summay_domain
                                                                    and flg_activo = 'S'
                                                                    and contexto = p_contexto_acesso
                                                                    and (/*p_tipo is null or*/ p_tipo like '%#ALL#%'or codigo in (select item from table (pck_common.split (p_tipo,'#' ))))
                                                                    and  a.tipo = resumo.provider_name
                                                                    AND  resumo.t_episodio LIKE NVL (a.t_episodio, '%')
                                                                    AND  (resumo.source_detail IS NULL OR resumo.source_detail LIKE NVL (a.subtipo, '%'))
                                                                    AND  NVL (resumo.cod_serv, '%') LIKE NVL (a.cod_serv, '%')
                                                                union all
                                                                select 1 from dual where p_tipo like '%#NO_INFO%'

                                                           )
                                                           and   (p_cod_serv is null or resumo.cod_serv in (select item from table (pck_common.split (p_cod_serv,'#' ))))
                                                           and  resumo.t_doente = epis.t_doente
                                                           AND  resumo.doente = epis.doente
                                                           AND  resumo.t_episodio = epis.t_episodio
                                                           AND  resumo.episodio = epis.episodio
                                                          and ( epis.dt_ini between w_dt_ini and w_dt_fim
                                                                or (epis.dt_fim between w_dt_ini and w_dt_fim and epis.dt_fim is not null and epis.dt_fim !=  to_date('31-12-9999','DD-MM-YYYY')))

                                                           and nvl(epis.flg_estado, 'X') != 'A'
                                                           and (epis.t_episodio != 'Consultas' or not exists (select 1 from sd_cons_marc where n_cons = epis.episodio and flag_estado = 'R'))


                                        ) k1
                                        ORDER BY CASE WHEN p_ordem_tipo = 'DESC' THEN ord_idx END DESC,
                                                 CASE WHEN p_ordem_tipo = 'ASC' THEN ord_idx END ASC
                                    ) t1
                         --WHERE ROWNUM < ( (w_page * w_perpage) + 1)
                         )
                 WHERE rnum < ( (w_page * w_perpage) + 1) AND rnum >= ( ( (w_page - 1) * w_perpage) + 1)) a, sd_serv, pc_param_geral epis_type
                 WHERE sd_serv.cod_serv(+) = (replace (pcf_devolve_serv_last(a.episodetype, a.episodeid, a.patienttype, a.patientid) , '#', ''))
                    AND epis_type.dominio = 'HISTORY_EPIS_TYPES'
                    AND epis_type.t_episodio = a.episodetype
                    ORDER BY CASE WHEN p_ordem_tipo = 'DESC' THEN ord_idx END DESC,
                    CASE WHEN          p_ordem_tipo = 'ASC' THEN ord_idx END ASC;
        end if;

    END GetPatientHistoryByCliInfoV2;

    PROCEDURE GetPatientHistoryFilters (
        p_sessionid              IN     VARCHAR2,
        p_t_doente               IN     VARCHAR2,
        p_doente                 IN     VARCHAR2,
        p_selected_t_episodio    IN     VARCHAR2,
        p_selected_episodio      IN     VARCHAR2,
        p_t_episodio             IN     VARCHAR2,
        p_cod_serv               IN     VARCHAR2,
        p_n_mecan                IN     VARCHAR2,
        p_tipo                   IN     VARCHAR2,
        p_summay_domain          IN     VARCHAR2,
        p_contexto_acesso        IN     VARCHAR2,
        p_intervalo_tempo        IN     VARCHAR2,
        p_result                    OUT pck_types.external_cursor)
    IS
        w_dt_ini date := to_date('01-01-0001 00:00','DD-MM-YYYY HH24:MI');
        w_dt_fim date := to_date('31-12-9999 23:59','DD-MM-YYYY HH24:MI');
        v_t_pess_hosp varchar2(30);
    BEGIN

    insertlogepr('TT FILTROS  p_sessionid '||  p_sessionid
        ||'p_t_doente '           || p_t_doente
       || 'p_doente '             || p_doente
        ||'p_selected_t_episodio ' || p_selected_t_episodio
       || 'p_selected_episodio '  ||  p_selected_episodio
       || 'p_t_episodio  '         ||  p_t_episodio
       || 'p_cod_serv  '         ||    p_cod_serv
       || 'p_n_mecan '           ||   p_n_mecan
       || 'p_tipo '              ||   p_tipo
       || 'p_summay_domain '     ||    p_summay_domain
       || 'p_contexto_acesso '|| p_contexto_acesso
       || 'p_intervalo_tempo '|| p_intervalo_tempo);

        if nvl(p_intervalo_tempo, 'EPISODE') != 'EPISODE' then
            pck_activities_episode.getinterval (p_intervalo_tempo, w_dt_ini, w_dt_fim);
        end if;

        begin
            select t_pess_hosp into v_t_pess_hosp from sd_pess_hosp_def where n_mecan = p_n_mecan;
            exception when others then
            v_t_pess_hosp := '';
        end;

        OPEN p_result FOR
            SELECT
                episodetype,
                episodetypedescr,
                serviceid,
                servicedescr,
                provider,
                prodidervdescr
              FROM (SELECT /*+ ORDERED*/
                       distinct
                       epis_type.codigo episodetype,
                       epis_type.descr episodetypedescr,
                       serv.descr_serv servicedescr,
                       serv.cod_serv serviceid,
                       param_resumo.codigo provider,
                       param_resumo.descr prodidervdescr
                            FROM
                            (
                                SELECT a.t_doente,
                                       a.doente,
                                       a.episodio,
                                       a.t_episodio,
                                       replace (
                                        (case a.t_episodio
                                            when 'Internamentos' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                            when 'Ambulatorio' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                            else a.cod_serv end) , '#', '') cod_serv,
                                       a.provider_name,
                                       a.source_detail
                                  FROM pc_resumo_info_doente a
                                 WHERE     a.t_doente = p_t_doente
                                       AND a.doente = p_doente
                                       AND (p_t_episodio is null or a.t_episodio in (select item from table (pck_common.split (p_t_episodio,'#' ))))
                                       AND (nvl(p_intervalo_tempo, 'X') != 'EPISODE' or (a.t_episodio = p_selected_t_episodio and a.episodio = p_selected_episodio))
                               ) resumo,
                                sd_serv serv,
                                sd_episodio epis,
                                pc_param_geral epis_type,
                                pc_param_resumo param_resumo
                               WHERE  resumo.cod_serv = serv.cod_serv(+)
                                   AND   (p_cod_serv is null or resumo.cod_serv in (select item from table (pck_common.split (p_cod_serv,'#' ))))
                                   -- PC_PARAM_RESUMO
                                   AND param_resumo.dominio = p_summay_domain
                                   AND param_resumo.flg_activo = 'S'
                                   AND param_resumo.contexto = p_contexto_acesso
                                   AND param_resumo.tipo = resumo.provider_name
                                   AND resumo.t_episodio LIKE NVL (param_resumo.t_episodio, '%')
                                   AND (resumo.source_detail IS NULL OR resumo.source_detail LIKE NVL (param_resumo.subtipo, '%'))
                                   AND NVL (resumo.cod_serv, '%') LIKE NVL (param_resumo.cod_serv, '%')
                                   AND (p_tipo is null or p_tipo like '%#ALL#%'or param_resumo.codigo in (select item from table (pck_common.split (p_tipo,'#' ))))
                                   AND nvl(param_resumo.t_pess_hosp, nvl(v_t_pess_hosp, '#')) = nvl(v_t_pess_hosp, '#')
                                   -- SD_EPISODIO
                                   AND  resumo.t_doente = epis.t_doente
                                   AND  resumo.doente = epis.doente
                                   AND  resumo.t_episodio = epis.t_episodio
                                   AND  resumo.episodio = epis.episodio
                                   AND (epis.dt_fim IS NULL OR w_dt_ini <= epis.dt_fim)
                                   and w_dt_fim >= epis.dt_ini
                                   AND ( epis.dt_ini between w_dt_ini and w_dt_fim
                                          or (epis.dt_fim between w_dt_ini and w_dt_fim and epis.dt_fim is not null and epis.dt_fim !=  to_date('31-12-9999','DD-MM-YYYY')))
                                   AND nvl(epis.flg_estado, 'X') != 'A'
                                   AND (epis.t_episodio != 'Consultas' or not exists (select 1 from sd_cons_marc where n_cons = epis.episodio and flag_estado = 'R'))
                                   -- PC_PARAM_GERAL
                                   AND epis_type.dominio = 'HISTORY_EPIS_TYPES'
                                   AND epis_type.t_episodio = nvl(resumo.t_episodio, 'Ficha-ID')

                       union all
                       -- episodios sem informa�ao clinica
                       SELECT /*+ ORDERED*/
                            distinct
                            epis_type.codigo episodetype,
                            epis_type.descr episodetypedescr,
                            serv.descr_serv servicedescr,
                            serv.cod_serv serviceid,
                            '' provider,
                            '' prodidervdescr
                            FROM
                            (
                                SELECT a.t_doente,
                                       a.doente,
                                       a.episodio,
                                       a.t_episodio,
                                       replace (
                                        (case a.t_episodio
                                            when 'Internamentos' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                            when 'Ambulatorio' then pcf_devolve_serv_last(a.t_episodio, a.episodio, a.t_doente, a.doente)
                                            else a.cod_serv end) , '#', '') cod_serv,
                                       a.provider_name,
                                       a.source_detail
                                  FROM pc_resumo_info_doente a
                                 WHERE     a.t_doente = p_t_doente
                                       AND a.doente = p_doente
                                       and  p_tipo like '%#NO_INFO%'
                                       and (p_t_episodio is null or a.t_episodio in (select item from table (pck_common.split (p_t_episodio,'#' ))))
                                       and (nvl(p_intervalo_tempo, 'X') != 'EPISODE' or (a.t_episodio = p_selected_t_episodio and a.episodio = p_selected_episodio))
                               ) resumo,
                                sd_serv serv,
                                sd_episodio epis,
                                pc_param_geral epis_type

                               WHERE  resumo.cod_serv = serv.cod_serv(+)
                                   and   (p_cod_serv is null or resumo.cod_serv in (select item from table (pck_common.split (p_cod_serv,'#' ))))
                                   -- PC_PARAM_RESUMO
                                   and not exists (
                                       select 1 from pc_param_resumo param_resumo
                                       where  param_resumo.dominio = p_summay_domain
                                       AND param_resumo.flg_activo = 'S'
                                       AND param_resumo.contexto = p_contexto_acesso
                                       AND param_resumo.tipo = resumo.provider_name
                                       AND resumo.t_episodio LIKE NVL (param_resumo.t_episodio, '%')
                                       AND (resumo.source_detail IS NULL OR resumo.source_detail LIKE NVL (param_resumo.subtipo, '%'))
                                       AND NVL (resumo.cod_serv, '%') LIKE NVL (param_resumo.cod_serv, '%')
                                       AND (p_tipo is null or p_tipo like '%#ALL#%'or codigo in (select item from table (pck_common.split (p_tipo,'#' ))))
                                       AND nvl(param_resumo.t_pess_hosp, nvl(v_t_pess_hosp, '#')) = nvl(v_t_pess_hosp, '#')
                                   )
                                   -- SD_EPISODIO
                                   and  resumo.t_doente = epis.t_doente
                                   AND  resumo.doente = epis.doente
                                   AND  resumo.t_episodio = epis.t_episodio
                                   AND  resumo.episodio = epis.episodio
                                   --AND (epis.dt_fim IS NULL OR w_dt_ini <= epis.dt_fim)
                                   --and w_dt_fim >= epis.dt_ini
                                   AND ( epis.dt_ini between w_dt_ini and w_dt_fim
                                          or (epis.dt_fim between w_dt_ini and w_dt_fim and epis.dt_fim is not null and epis.dt_fim !=  to_date('31-12-9999','DD-MM-YYYY')))
                                   AND nvl(epis.flg_estado, 'X') != 'A'
                                   AND (epis.t_episodio != 'Consultas' or not exists (select 1 from sd_cons_marc where n_cons = epis.episodio and flag_estado = 'R'))
                                   -- PC_PARAM_GERAL
                                   AND epis_type.dominio = 'HISTORY_EPIS_TYPES'
                                   AND epis_type.t_episodio = nvl(resumo.t_episodio, 'Ficha-ID')
                            )
              order by 1, 3, 5;

    END GetPatientHistoryFilters;

end;
/
