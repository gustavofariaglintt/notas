CREATE OR REPLACE 
PACKAGE pck_epr_mobile_ext
IS



    FUNCTION HasEpisodeDefaultFilter(
        p_scop      IN VARCHAR2,
        p_t_episode IN VARCHAR2,
        p_n_mecan   IN VARCHAR2)
    RETURN VARCHAR2;

    FUNCTION PreloadPatientHistory (p_sessionID   IN VARCHAR2,
                                    p_t_doente    IN VARCHAR2,
                                    p_doente      IN VARCHAR2,
                                    p_n_mecan     IN VARCHAR2)
    return varchar2;

    PROCEDURE GetPatientHistory (
      p_sessionID               IN       VARCHAR2,
      p_t_doente                IN       VARCHAR2,
      p_doente                  IN       VARCHAR2,
      p_t_episodio_funcional     IN       VARCHAR2,
      p_cod_serv                IN       VARCHAR2,
      p_n_mecan                 IN       VARCHAR2,
      p_ambito_acesso           IN       VARCHAR2,
      p_intervalo_tempo         IN       VARCHAR2,
      p_pagina                  IN       VARCHAR2,
      p_n_paginas               IN       NUMBER,
      p_elemento_de             IN       NUMBER,
      p_elemento_a              IN       NUMBER,
      p_ordem_por               IN       VARCHAR2,
      p_result                  OUT      pck_types.external_cursor
   );

    PROCEDURE GetFunctionalEpisodeTypes (
        p_sessionID       IN     VARCHAR2,
        p_t_doente        IN     VARCHAR2,
        p_doente          IN     VARCHAR2,
        p_n_mecan         IN     VARCHAR2,
        p_ambito_acesso          VARCHAR2,
        p_result             OUT pck_types.external_cursor);

    PROCEDURE GetPatientSpecialities (
      p_sessionID       IN       VARCHAR2,
      p_t_doente        IN       VARCHAR2,
      p_doente          IN       VARCHAR2,
      p_n_mecan         IN       VARCHAR2,
      p_ambito_acesso   IN       VARCHAR2,
      p_result          OUT      pck_types.external_cursor);

    FUNCTION DecodeFuncionalEpisodeType (p_t_doente     IN VARCHAR2,
                                      p_doente       IN VARCHAR2,
                                      p_t_episodio   IN VARCHAR2,
                                      p_episodio     IN VARCHAR2)
        RETURN VARCHAR2;

    PROCEDURE preenche_episodio_tmp_aux (p_t_doente   IN VARCHAR2,
                                         p_doente     IN VARCHAR2,
                                         p_session    IN VARCHAR2);

PROCEDURE GetPatientHistoryV2 ( --com pagina�ao e ordena��o...
        p_sessionID              IN     VARCHAR2,
        p_t_doente               IN     VARCHAR2,
        p_doente                 IN     VARCHAR2,
        p_t_episodio_funcional   IN     VARCHAR2,
        p_cod_serv               IN     VARCHAR2,
        p_n_mecan                IN     VARCHAR2,
        p_ambito_acesso          IN     VARCHAR2,
        p_intervalo_tempo        IN     VARCHAR2,
        p_pagenumber                 IN     NUMBER,
        p_itemsperpage              IN     NUMBER,
        p_ordem_por              IN     VARCHAR2,
        p_ordem_tipo          IN     VARCHAR2,
        p_result                    OUT pck_types.external_cursor);

PROCEDURE GetPatientHistoryByCliInfo (
        p_sessionid              IN     VARCHAR2,
        p_t_doente               IN     VARCHAR2,
        p_doente                 IN     VARCHAR2,
        p_selected_t_episodio    IN     VARCHAR2,
        p_selected_episodio      IN     VARCHAR2,
        p_t_episodio             IN     VARCHAR2,
        p_cod_serv               IN     VARCHAR2,
        p_n_mecan                IN     VARCHAR2,
        p_tipo                   IN     VARCHAR2,
        p_summay_domain          IN     VARCHAR2,
        p_contexto_acesso        IN     VARCHAR2,
        p_intervalo_tempo        IN     VARCHAR2,
        p_pagenumber             IN     NUMBER,
        p_itemsperpage           IN     NUMBER,
        p_ordem_por              IN     VARCHAR2,
        p_ordem_tipo             IN     VARCHAR2,
        p_result                    OUT pck_types.external_cursor);

PROCEDURE GetPatientHistoryFilters (
        p_sessionid              IN     VARCHAR2,
        p_t_doente               IN     VARCHAR2,
        p_doente                 IN     VARCHAR2,
        p_selected_t_episodio    IN     VARCHAR2,
        p_selected_episodio      IN     VARCHAR2,
        p_t_episodio             IN     VARCHAR2,
        p_cod_serv               IN     VARCHAR2,
        p_n_mecan                IN     VARCHAR2,
        p_tipo                   IN     VARCHAR2,
        p_summay_domain          IN     VARCHAR2,
        p_contexto_acesso        IN     VARCHAR2,
        p_intervalo_tempo        IN     VARCHAR2,
        p_result                    OUT pck_types.external_cursor);

FUNCTION GetDuration(p_n_int IN  VARCHAR2)
RETURN VARCHAR2;

    PROCEDURE GetPatientHistoryByCliInfoV2 (
        p_sessionid              IN     VARCHAR2,
        p_t_doente               IN     VARCHAR2,
        p_doente                 IN     VARCHAR2,
        p_selected_t_episodio    IN     VARCHAR2,
        p_selected_episodio      IN     VARCHAR2,
        p_t_episodio             IN     VARCHAR2,
        p_cod_serv               IN     VARCHAR2,
        p_n_mecan                IN     VARCHAR2,
        p_tipo                   IN     VARCHAR2,
        p_summay_domain          IN     VARCHAR2,
        p_contexto_acesso        IN     VARCHAR2,
        p_intervalo_tempo        IN     VARCHAR2,
        p_episodios_futuros      IN     VARCHAR2,
        p_pagenumber             IN     NUMBER,
        p_itemsperpage           IN     NUMBER,
        p_ordem_por              IN     VARCHAR2,
        p_ordem_tipo             IN     VARCHAR2,
        p_result                    OUT pck_types.external_cursor);

end;
/
