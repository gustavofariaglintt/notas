FUNCTION GetDuration (
    p_n_int IN VARCHAR2
)
    RETURN VARCHAR
IS
    p_dt_ini DATE;
    p_dt_end DATE;
BEGIN
    SELECT dt_int, dt_alta
    INTO p_dt_ini, p_dt_end
    FROM sd_ints
    WHERE n_int = p_n_int;

    RETURN trunc(
        trunc(nvl(p_dt_end, sysdate)) - (trunc(p_dt_ini))
    );
END;