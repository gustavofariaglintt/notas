# Novas parametrizações PC_PARAM_GERAL

Condições:

+ DOMINIO = "HISTORY_EPIS_DEFAULT";
+ CODIGO e DESCR de acordo com o que existe na tabela PC_PARAM_RESUMO
+ FLG_ACTIVO in ('S' | 'N')
+ N_MECAN de acordo com os valores da tabela SD_PESS_HOSP_DEF
+ N_MECAN vazio significa preferência geral da Empresa, N_MECAN preenchido significa preferencia do utilizador
+ EMPRESA de acordo com o valor da companyDB das Connection Strings
