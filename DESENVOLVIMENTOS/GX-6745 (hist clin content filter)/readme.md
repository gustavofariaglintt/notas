# GX-6745

## Jira

**Epic** [GX-6745](https://glinttdev.atlassian.net/browse/GX-6745)

**User Story Dev** [GX-7240](https://glinttdev.atlassian.net/browse/GX-7240)

## Observações do Agendamento

**Cliente** xxx

**Patch** - 2020/01

**TDM** - Fernando Oliveira

**Produto** - Nuno Oliveira

**Frontend** - 17 a 19 de Dezembro - Rui Meireles

**Qualidade** - 18 Novembro

### Descrição

Na sequência de identificação pelo Cliente foi descrita incongruência de lógica da ordenação das opções do Filtro Conteúdo no Histórico Clínico e impossibilidade de definição de informação default pre-selecionada.

### Mensagem Check-In

GX-6745 (epic)
GX-7240 (issue)
Filtro do conteúdo no Histórico Clínico passa a ter informação acerca de quais opções vêm seleccionadas por predefinição

### Changesets

|                |   DEV    |    CI    |   17R1   |   19R12  |
|---------------:|:--------:|:--------:|:--------:|:--------:|
| **Changesets** |  214008  |  214009  |  215030  |  000000  |
|                |  214905  |  214906  |  000000  |  000000  |
|                |  215024  |  215025  |  215031  |  215032  |

### Notas

Ao fazer load inicial da página: <http://demoapliis.glinttocm.com:9002/EPR_MOBILE/Glintths.Shell/PatientData_HistorySummary/GetPatientHistoryFilters>

**Solução Frontend** Glintths.PatientData

+ Controller: PatientDat_HistorySummary.GetPatientHistoryFilters()
+ DataManagement: EpisodeManagement.GetPatientHistoryFilters()

### Backend Configs

**Chave ginf_config** "S_CLINICAL_EPR_EPISODE_MANAGEMENT" (EpisodeManagement.cs)
    **value** ```"#BASE_URL#Cpchs.Modules.ActivitiesManagement.WS/Episode/WebService/EpisodeManagement.svc"```

**Projecto Backend** Cpchs.Modules.ActivitiesManagement.WebServices
    **IIS** Services/Glintths.WebServices
    **Lógica** Services/Glintths.BusinessRules
    **Data Model** Services/Glintths.BusinessEntities

**DB Method and Procedure** PCK_EPR_MOBILE_EXT.GetPatientHistoryFilters

**Configurações** ```SELECT * FROM GH_PC.PC_PARAM_RESUMO WHERE dominio = 'RESUMO_EPR' --coluna 'ORD'```

### Backend Lógica
