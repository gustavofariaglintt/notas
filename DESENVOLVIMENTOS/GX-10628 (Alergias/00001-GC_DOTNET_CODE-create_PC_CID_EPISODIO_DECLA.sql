DECLARE
    w_colunas   ga_install_objects.my_columns;
BEGIN
    w_colunas (w_colunas.COUNT).column_name := 'T_DOENTE';
    w_colunas (w_colunas.COUNT - 1).column_type := 'VARCHAR2';
    w_colunas (w_colunas.COUNT - 1).column_length := 5;
    w_colunas (w_colunas.COUNT - 1).column_precision := NULL;
    w_colunas (w_colunas.COUNT - 1).column_scale := NULL;
    w_colunas (w_colunas.COUNT - 1).nullable := FALSE;
    w_colunas (w_colunas.COUNT - 1).column_comment := q'{}';

    w_colunas (w_colunas.COUNT).column_name := 'DOENTE';
    w_colunas (w_colunas.COUNT - 1).column_type := 'VARCHAR2';
    w_colunas (w_colunas.COUNT - 1).column_length := 30;
    w_colunas (w_colunas.COUNT - 1).column_precision := NULL;
    w_colunas (w_colunas.COUNT - 1).column_scale := NULL;
    w_colunas (w_colunas.COUNT - 1).nullable := FALSE;
    w_colunas (w_colunas.COUNT - 1).column_comment := q'{}';

    w_colunas (w_colunas.COUNT).column_name := 'T_EPISODIO';
    w_colunas (w_colunas.COUNT - 1).column_type := 'VARCHAR2';
    w_colunas (w_colunas.COUNT - 1).column_length := 15;
    w_colunas (w_colunas.COUNT - 1).column_precision := NULL;
    w_colunas (w_colunas.COUNT - 1).column_scale := NULL;
    w_colunas (w_colunas.COUNT - 1).nullable := FALSE;
    w_colunas (w_colunas.COUNT - 1).column_comment := q'{}';

    w_colunas (w_colunas.COUNT).column_name := 'EPISODIO';
    w_colunas (w_colunas.COUNT - 1).column_type := 'VARCHAR2';
    w_colunas (w_colunas.COUNT - 1).column_length := 30;
    w_colunas (w_colunas.COUNT - 1).column_precision := NULL;
    w_colunas (w_colunas.COUNT - 1).column_scale := NULL;
    w_colunas (w_colunas.COUNT - 1).nullable := TRUE;
    w_colunas (w_colunas.COUNT - 1).column_comment := q'{}';

    w_colunas (w_colunas.COUNT).column_name := 'TIPO_CID';
    w_colunas (w_colunas.COUNT - 1).column_type := 'VARCHAR2';
    w_colunas (w_colunas.COUNT - 1).column_length := 80;
    w_colunas (w_colunas.COUNT - 1).column_precision := NULL;
    w_colunas (w_colunas.COUNT - 1).column_scale := NULL;
    w_colunas (w_colunas.COUNT - 1).nullable := TRUE;
    w_colunas (w_colunas.COUNT - 1).column_comment := q'{}';

    w_colunas (w_colunas.COUNT).column_name := 'CHAVE';
    w_colunas (w_colunas.COUNT - 1).column_type := 'VARCHAR2';
    w_colunas (w_colunas.COUNT - 1).column_length := 50;
    w_colunas (w_colunas.COUNT - 1).column_precision := NULL;
    w_colunas (w_colunas.COUNT - 1).column_scale := NULL;
    w_colunas (w_colunas.COUNT - 1).nullable := TRUE;
    w_colunas (w_colunas.COUNT - 1).column_comment := q'{}';

    w_colunas (w_colunas.COUNT).column_name := 'RESPOSTA';
    w_colunas (w_colunas.COUNT - 1).column_type := 'VARCHAR2';
    w_colunas (w_colunas.COUNT - 1).column_length := 50;
    w_colunas (w_colunas.COUNT - 1).column_precision := NULL;
    w_colunas (w_colunas.COUNT - 1).column_scale := NULL;
    w_colunas (w_colunas.COUNT - 1).nullable := TRUE;
    w_colunas (w_colunas.COUNT - 1).column_comment := q'{}';

    w_colunas (w_colunas.COUNT).column_name := 'ACTIVO';
    w_colunas (w_colunas.COUNT - 1).column_type := 'VARCHAR2';
    w_colunas (w_colunas.COUNT - 1).column_length := 1;
    w_colunas (w_colunas.COUNT - 1).column_precision := NULL;
    w_colunas (w_colunas.COUNT - 1).column_scale := NULL;
    w_colunas (w_colunas.COUNT - 1).nullable := TRUE;
    w_colunas (w_colunas.COUNT - 1).column_comment := q'{}';

    w_colunas (w_colunas.COUNT).column_name := 'DT_CRI';
    w_colunas (w_colunas.COUNT - 1).column_type := 'DATE';
    w_colunas (w_colunas.COUNT - 1).column_length := NULL;
    w_colunas (w_colunas.COUNT - 1).column_precision := NULL;
    w_colunas (w_colunas.COUNT - 1).column_scale := NULL;
    w_colunas (w_colunas.COUNT - 1).nullable := TRUE;
    w_colunas (w_colunas.COUNT - 1).column_comment := q'{}';

    w_colunas (w_colunas.COUNT).column_name := 'DT_ACT';
    w_colunas (w_colunas.COUNT - 1).column_type := 'DATE';
    w_colunas (w_colunas.COUNT - 1).column_length := NULL;
    w_colunas (w_colunas.COUNT - 1).column_precision := NULL;
    w_colunas (w_colunas.COUNT - 1).column_scale := NULL;
    w_colunas (w_colunas.COUNT - 1).nullable := TRUE;
    w_colunas (w_colunas.COUNT - 1).column_comment := q'{}';

    w_colunas (w_colunas.COUNT).column_name := 'USER_CRI';
    w_colunas (w_colunas.COUNT - 1).column_type := 'DATE';
    w_colunas (w_colunas.COUNT - 1).column_length := NULL;
    w_colunas (w_colunas.COUNT - 1).column_precision := NULL;
    w_colunas (w_colunas.COUNT - 1).column_scale := NULL;
    w_colunas (w_colunas.COUNT - 1).nullable := TRUE;
    w_colunas (w_colunas.COUNT - 1).column_comment := q'{}';

    w_colunas (w_colunas.COUNT).column_name := 'USER_ACT';
    w_colunas (w_colunas.COUNT - 1).column_type := 'DATE';
    w_colunas (w_colunas.COUNT - 1).column_length := NULL;
    w_colunas (w_colunas.COUNT - 1).column_precision := NULL;
    w_colunas (w_colunas.COUNT - 1).column_scale := NULL;
    w_colunas (w_colunas.COUNT - 1).nullable := TRUE;
    w_colunas (w_colunas.COUNT - 1).column_comment := q'{}';

    ga_install_objects.create_alter_table ('PC_CID_EPISODIO_DECLA',
                                           w_colunas);
END;
/

BEGIN
    ga.ga_install_objects.create_check_constraint (
        i_table_owner    => 'GH_PC',
        i_table_name     => 'PC_CID_EPISODIO_DECLA',
        i_cc_name        => 'PC_CID_EPISODIO_DECLA_CHK01',
        i_cc_condition   => 'ACTIVO IN (''S'',''N'')',
        i_deferrable     => FALSE,
        i_deferred       => FALSE);
END;
/

CREATE OR REPLACE TRIGGER pc_cid_episodio_decla_bef_ins
    BEFORE INSERT
    ON pc_cid_episodio_decla
    FOR EACH ROW
BEGIN
    :new.user_cri := tp_pck_utilizador_apl.le_util_apl;
    :new.dt_cri := SYSDATE;
END;
/

CREATE OR REPLACE TRIGGER pc_cid_episodio_decla_upd_act
    BEFORE INSERT
    ON pc_cid_episodio_decla
    FOR EACH ROW
BEGIN
    :new.user_act := tp_pck_utilizador_apl.le_util_apl;
    :new.dt_act := SYSDATE;
END;