PACKAGE PCKAPI_ENF_CAREPLAN_TPS IS

    --| CONSTANTS
    c_INTERVENTION_ID               CONSTANT VARCHAR2(30) := 'IdCpIntervention';
    c_INTERVENTION_CODIFICATION     CONSTANT VARCHAR2(30) := 'Codification';
    c_FREQUENCY                     CONSTANT VARCHAR2(30) := 'Frequency';

    --| TYPES
    TYPE T_NURSING_SUBJECT IS RECORD
    (
        PATIENT_KEY             VARCHAR2(50)        /*{"keep_name_case": true, "new_column_name":"PatientKey"       ,"use_column": true, "raml_addon":{description:"Key of the subject"             ,example:"HS.100"           ,required_for_raml:false}}*/,
        PATIENT_TYPE            VARCHAR2(6)         /*{"keep_name_case": true, "new_column_name":"PatientType"      ,"use_column": true, "raml_addon":{description:"Type of the patient"            ,example:"HS"               ,required_for_raml:false}}*/,
        PATIENT_ID              VARCHAR2(12)        /*{"keep_name_case": true, "new_column_name":"PatientId"        ,"use_column": true, "raml_addon":{description:"Number of the patient"          ,example:"100"              ,required_for_raml:false}}*/
    );

    TYPE T_NURSING_ENCOUNTER IS RECORD
    (
        ENCOUNTER_KEY           VARCHAR2(50)        /*{"keep_name_case": true, "new_column_name":"EncounterKey"     ,"use_column": true, "raml_addon":{description:"Key of the encounter"   ,example:"Internamentos.5000"           ,required_for_raml:false}}*/,
        ENCOUNTER_TYPE          VARCHAR2(14)        /*{"keep_name_case": true, "new_column_name":"EncounterType"    ,"use_column": true, "raml_addon":{description:"Type of the episode"    ,example:"Internamentos"    ,required_for_raml:false}}*/,
        ENCOUNTER_ID            VARCHAR2(15)        /*{"keep_name_case": true, "new_column_name":"EncounterId"      ,"use_column": true, "raml_addon":{description:"Number of the episode"  ,example:"5000"             ,required_for_raml:false}}*/
    );

    TYPE T_RECORD_INFO IS RECORD
    (
        CREATION_DATE       TIMESTAMP (6) WITH TIME ZONE    /*{"keep_name_case": true, "new_column_name":"CreationDate"     ,"use_column": true, "raml_addon":{description:"Date when this object was created"          ,example:"2014-04-07T14:47:50+01:00"    ,required_for_raml:false}}*/,
        CREATION_USER       VARCHAR2(30)                    /*{"keep_name_case": true, "new_column_name":"CreationUser"     ,"use_column": true, "raml_addon":{description:"User that created this object"              ,example:"U0012"                        ,required_for_raml:false}}*/,
        LAST_UPDATE_DATE    TIMESTAMP (6) WITH TIME ZONE    /*{"keep_name_case": true, "new_column_name":"LastUpdateDate"   ,"use_column": true, "raml_addon":{description:"Last date when this object was modified"    ,example:"2014-04-07T14:47:50+01:00"    ,required_for_raml:false}}*/,
        LAST_UPDATE_USER    VARCHAR2(30)                    /*{"keep_name_case": true, "new_column_name":"LastUpdateUser"   ,"use_column": true, "raml_addon":{description:"Last user that changed this object"         ,example:"U0012"                        ,required_for_raml:false}}*/,
        RECORD_VERSION      NUMBER                          /*{"keep_name_case": true, "new_column_name":"RecordVersion"    ,"use_column": true, "raml_addon":{description:"Version of the object"                      ,example:"123"                          ,required_for_raml:false}}*/,
        UPDATE_ALLOWED      BOOLEAN                         /*{"keep_name_case": true, "new_column_name":"UpdateAllowed"    ,"use_column": true, "raml_addon":{description:"Is this object locked"                      ,example:"false"                        ,required_for_raml:false}}*/,
        MESSAGE             VARCHAR2(1000)                  /*{"keep_name_case": true, "new_column_name":"Message"          ,"use_column": true, "raml_addon":{description:"Why is this object locked"                  ,example:"Episode is closed"            ,required_for_raml:false}}*/
    );

    TYPE T_NURSING_INTERVENTION IS RECORD (
        CODIFICATION            VARCHAR2(30)    /*{"keep_name_case": true, "new_column_name":"Codification" ,"use_column": true, "raml_addon":{description:"Codification of the intervention"       ,example:"GR_ENF_INTERV"            ,required_for_raml:false}}*/,
        CODE                    VARCHAR2(100)   /*{"keep_name_case": true, "new_column_name":"Code"         ,"use_column": true, "raml_addon":{description:"Code of the intervention"               ,example:"200001"            ,required_for_raml:false}}*/,
        DESCRIPTION             VARCHAR2(100)   /*{"keep_name_case": true, "new_column_name":"Description"  ,"use_column": true, "raml_addon":{description:"Description of the intervention"        ,example:"Clean Wound"            ,required_for_raml:false}}*/,
        FORM_TYPE               VARCHAR2(30)    /*{"keep_name_case": true, "new_column_name":"FormType"     ,"use_column": true, "raml_addon":{description:"Form needed to document the execution"  ,example:"U0012"            ,required_for_raml:false}}*/
    );
    TYPE TT_NURSING_INTERVENTION IS TABLE OF T_NURSING_INTERVENTION INDEX BY BINARY_INTEGER;

    TYPE T_NURSING_INDICATOR IS RECORD (
        CODE                    VARCHAR2(100)   /*{"keep_name_case": true, "new_column_name":"Code"         ,"use_column": true, "raml_addon":{description:"Code of the intervention"               ,example:"200001"            ,required_for_raml:false}}*/,
        DESCRIPTION             VARCHAR2(100)   /*{"keep_name_case": true, "new_column_name":"Description"  ,"use_column": true, "raml_addon":{description:"Description of the intervention"        ,example:"Temperature"            ,required_for_raml:false}}*/
    );
    TYPE TT_NURSING_INDICATOR IS TABLE OF T_NURSING_INDICATOR INDEX BY BINARY_INTEGER;

    TYPE T_NURSING_CALENDAR_DAILY IS RECORD
    (
        EACH_X_DAYS         NUMBER      /*{"keep_name_case": true, "new_column_name":"EachXDays"    ,"use_column": true, "raml_addon":{description:"X",example:"X",required_for_raml:false}}*/
    );

    TYPE T_NURSING_CALENDAR_ADM IS RECORD
    (
        EACH_X_ADMINISTRATIONS         NUMBER      /*{"keep_name_case": true, "new_column_name":"EachXAdministrations"    ,"use_column": true, "raml_addon":{description:"X",example:"X",required_for_raml:false}}*/
    );

    TYPE T_NURSING_CALENDAR_WEEKLY IS RECORD
    (
        SUNDAY              BOOLEAN /*{"keep_name_case": true, "new_column_name":"Sunday"    ,"use_column": true, "raml_addon":{description:"Scheduled for this day of week",example:"true",required_for_raml:false}}*/,
        MONDAY              BOOLEAN /*{"keep_name_case": true, "new_column_name":"Monday"    ,"use_column": true, "raml_addon":{description:"Scheduled for this day of week",example:"true",required_for_raml:false}}*/,
        TUESDAY             BOOLEAN /*{"keep_name_case": true, "new_column_name":"Tuesday"   ,"use_column": true, "raml_addon":{description:"Scheduled for this day of week",example:"true",required_for_raml:false}}*/,
        WEDNESDAY           BOOLEAN /*{"keep_name_case": true, "new_column_name":"Wednesday" ,"use_column": true, "raml_addon":{description:"Scheduled for this day of week",example:"true",required_for_raml:false}}*/,
        THURSDAY            BOOLEAN /*{"keep_name_case": true, "new_column_name":"Thursday"  ,"use_column": true, "raml_addon":{description:"Scheduled for this day of week",example:"true",required_for_raml:false}}*/,
        FRIDAY              BOOLEAN /*{"keep_name_case": true, "new_column_name":"Friday"    ,"use_column": true, "raml_addon":{description:"Scheduled for this day of week",example:"true",required_for_raml:false}}*/,
        SATURDAY            BOOLEAN /*{"keep_name_case": true, "new_column_name":"Saturday"  ,"use_column": true, "raml_addon":{description:"Scheduled for this day of week",example:"true",required_for_raml:false}}*/
    );

    TYPE T_NURSING_CALENDAR_MONTHLY IS RECORD
    (
        EACH_X_MONTHS       NUMBER      /*{"keep_name_case": true, "new_column_name":"EachXMonths"  ,"use_column": true, "raml_addon":{description:"Interval of months" ,example:"1"        ,required_for_raml:false}}*/,
        MONTH_DAY           NUMBER      /*{"keep_name_case": true, "new_column_name":"MonthDay"     ,"use_column": true, "raml_addon":{description:"MonthDay"           ,example:"28"       ,required_for_raml:false}}*/,
        WEEK_NUMBER         NUMBER      /*{"keep_name_case": true, "new_column_name":"WeekNumber"   ,"use_column": true, "raml_addon":{description:"Number of the Week" ,example:"2"        ,required_for_raml:false}}*/,
        WEEK_DAY            VARCHAR2(9) /*{"keep_name_case": true, "new_column_name":"WeekDay"      ,"use_column": true, "raml_addon":{description:"Day of the week"    ,example:"saturday" ,required_for_raml:false}}*/
    );

    TYPE T_NURSING_SCHEDULING_DAYS IS RECORD
    (
        SPECIFIC_DAY        DATE        /*{"keep_name_case": true, "new_column_name":"SpecificDay"  ,"use_column": true, "raml_addon":{description:"Date"                                               ,example:"2001-09-17"       ,required_for_raml:false}}*/,
        ACTION              VARCHAR2(1) /*{"keep_name_case": true, "new_column_name":"Action"       ,"use_column": true, "raml_addon":{description:"Is this to include or exclude +- from the pattern"  ,example:"2001-09-17"       ,required_for_raml:false}}*/
    );
    TYPE TT_NURSING_SCHEDULING_DAYS IS TABLE OF T_NURSING_SCHEDULING_DAYS INDEX BY BINARY_INTEGER;

    TYPE T_NURSING_SCHEDULING IS RECORD
    (
        START_DATE              DATE            /*{"keep_name_case": true, "new_column_name":"StartDate"    ,"use_column": true, "raml_addon":{description:"Start of the period when the schedule is active"    ,example:"2014-09-26T12:05:00"       ,required_for_raml:true}}*/,
        ENDS_IN_X_DAYS          NUMBER          /*{"keep_name_case": true, "new_column_name":"EndsInXDays"  ,"use_column": true, "raml_addon":{description:"Length of the scheduling in days"                   ,example:"X",required_for_raml:false}}*/,
        END_DATE                DATE            /*{"keep_name_case": true, "new_column_name":"EndDate"      ,"use_column": true, "raml_addon":{description:"End of the period when the schedule is active"      ,example:"2014-09-28T18:00:00"       ,required_for_raml:false}}*/,
        FREQUENCY               VARCHAR2(30)    /*{"keep_name_case": true, "new_column_name":"Frequency"    ,"use_column": true, "raml_addon":{description:"Frequency of this plan"                             ,example:"8-8H",required_for_raml:false}}*/,
        SCHEDULE                VARCHAR2(80)    /*{"keep_name_case": true, "new_column_name":"Schedule"     ,"use_column": true, "raml_addon":{description:"Schedule of plan",example:"0H - 8H - 16H"           ,required_for_raml:false}}*/,
        CALENDAR_MODE           VARCHAR2(30)    /*{"keep_name_case": true, "new_column_name":"CalendarMode" ,"use_column": true, "raml_addon":{description:"Calendar Mode (daily, weekly or monthly)"           ,example:"",required_for_raml:false}}*/,
        CALENDAR_DAILY          T_NURSING_CALENDAR_DAILY    /*{"keep_name_case": true, "new_column_name":"CalendarDaily"    ,"use_column": true, "raml_addon":{description:"See object details",example:"X",required_for_raml:false}}*/,
        CALENDAR_ADMINISTRATION T_NURSING_CALENDAR_ADM      /*{"keep_name_case": true, "new_column_name":"CalendarAdministration"   ,"use_column": true, "raml_addon":{description:"See object details",required_for_raml:false}}*/,
        CALENDAR_WEEK           T_NURSING_CALENDAR_WEEKLY   /*{"keep_name_case": true, "new_column_name":"CalendarWeek"     ,"use_column": true, "raml_addon":{description:"See object details",example:"X",required_for_raml:false}}*/,
        CALENDAR_MONTH          T_NURSING_CALENDAR_MONTHLY  /*{"keep_name_case": true, "new_column_name":"CalendarMonth"    ,"use_column": true, "raml_addon":{description:"See object details",example:"X",required_for_raml:false}}*/,
        CALENDAR_DAYS           TT_NURSING_SCHEDULING_DAYS  /*{"keep_name_case": true, "new_column_name":"CalendarDays"     ,"use_column": true, "raml_addon":{description:"See object details",example:"X",required_for_raml:false}}*/
    );

    TYPE T_NURSING_PLAN_INTERVENTION IS RECORD
    (
        ID_CP_INTERVENTION      NUMBER                  /*{"keep_name_case": true, "new_column_name":"IdCpIntervention"     ,"use_column": true, "raml_addon":{description:"Id of this intervention plan"                       ,example:"444312"                   ,required_for_raml:false}}*/,
        INTERVENTION            T_NURSING_INTERVENTION  /*{"keep_name_case": true, "new_column_name":"Intervention"         ,"use_column": true, "raml_addon":{description:"Details of the planned intervention"                ,example:"X",required_for_raml:false}}*/,
        FULL_DESCRIPTION        VARCHAR2(2000)          /*{"keep_name_case": true, "new_column_name":"FullDescription"      ,"use_column": true, "raml_addon":{description:"Description of this intervention plan"              ,example:"Clean: Wound, Left Arm"   ,required_for_raml:false}}*/,
        SUBJECT                 T_NURSING_SUBJECT       /*{"keep_name_case": true, "new_column_name":"Subject"              ,"use_column": true, "raml_addon":{description:"Subject of this intervention"                       ,example:"X",required_for_raml:false}}*/,
        ENCOUNTER               T_NURSING_ENCOUNTER     /*{"keep_name_case": true, "new_column_name":"Encounter"            ,"use_column": true, "raml_addon":{description:"Encounter when this intervention should be applied" ,example:"X",required_for_raml:false}}*/,
        SCHEDULING              T_NURSING_SCHEDULING    /*{"keep_name_case": true, "new_column_name":"Scheduling"           ,"use_column": true, "raml_addon":{description:"Schedule of this intervention"                      ,example:"X",required_for_raml:false}}*/,
        OBSERVATIONS            VARCHAR2(4000)          /*{"keep_name_case": true, "new_column_name":"Observations"         ,"use_column": true, "raml_addon":{description:"Observations of this intervention"                  ,example:"X",required_for_raml:false}}*/,
        RECORD_INFO             T_RECORD_INFO           /*{"keep_name_case": true, "new_column_name":"RecordInfo"           ,"use_column": true, "raml_addon":{description:"Technical information"                              ,example:"X",required_for_raml:false}}*/
    );

    --***************************************************************************************************
    -- CATALOGOS EXTERNOS
    TYPE T_NURSING_FREQUENCY IS RECORD
    (
        FREQUENCY_CODE          VARCHAR2(30) /*{"keep_name_case": true, "new_column_name":"FrequencyCode"  ,"use_column": true, "raml_addon":{description:"same as frequency"                                  ,example:"8-8H",required_for_raml:false}}*/,
        FREQUENCY               VARCHAR2(30) /*{"keep_name_case": true, "new_column_name":"Frequency"    ,"use_column": true, "raml_addon":{description:"Frequency of this plan"                             ,example:"8-8H",required_for_raml:false}}*/
    );

    TYPE TT_NURSING_FREQUENCY IS TABLE OF T_NURSING_FREQUENCY INDEX BY BINARY_INTEGER;

    TYPE T_NURSING_SCHEDULE IS RECORD
    (
        SCHEDULE_CODE           VARCHAR2(80) /*{"keep_name_case": true, "new_column_name":"ScheduleCode"  ,"use_column": true, "raml_addon":{description:"same as schedulo",example:"0H - 8H - 16H"           ,required_for_raml:false}}*/,
        SCHEDULE                VARCHAR2(80) /*{"keep_name_case": true, "new_column_name":"Schedule"     ,"use_column": true, "raml_addon":{description:"Schedule of plan",example:"0H - 8H - 16H"           ,required_for_raml:false}}*/
    );

    TYPE TT_NURSING_SCHEDULE IS TABLE OF T_NURSING_SCHEDULE INDEX BY BINARY_INTEGER;

    TYPE T_NURSING_DATE IS RECORD
    (
        SPECIFIC_DAY            DATE        /*{"keep_name_case": true, "new_column_name":"SpecificDay"  ,"use_column": true, "raml_addon":{description:"Date"                                               ,example:"2001-09-17"       ,required_for_raml:false}}*/
    );

    TYPE TT_NURSING_DATES IS TABLE OF T_NURSING_DATE INDEX BY BINARY_INTEGER;

END PCKAPI_ENF_CAREPLAN_TPS;