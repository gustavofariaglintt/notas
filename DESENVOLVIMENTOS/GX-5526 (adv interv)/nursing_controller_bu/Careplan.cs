using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Glintt.DotNetCore.Api.Authentication.Providers;
using Glintt.DotNetCore.Api.Base;
using Glintt.DotNetCore.Api.HttpExceptionHandling.Model;
using GlinttHS.NURSINGAPI.Api;
using GlinttHS.NURSINGAPI.Dto;
using GlinttHS.NURSINGAPI.Infrastructure.Interfaces.Services;
using GlinttHS.NURSINGAPI.Infrastructure.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GlinttHS.NURSINGAPI.Api.Controllers
{
   [Route("api/v1/careplan")]
   [ApiController]
   [Authorize]
   public class Careplan : BaseController
   {
       private readonly IPckapiEnfCareplanApi_CareplanService _theService;

       public Careplan(IPckapiEnfCareplanApi_CareplanService theService, IAuthenticationProvider authenticationProvider) : base(
           authenticationProvider, new IServiceBase[] { theService })
       {
           _theService = theService;
       }

        /// <summary>
        /// Get the details of one planned intervention.
        /// </summary>        

       // GET: api/experience/NURSINGAPI//careplan/intervention/{IdCpIntervention}
       [HttpGet("intervention/{IdCpIntervention}")]
       public virtual async Task<PckapiEnfCareplanTps_TNursingPlanInterventionClientModelDTOModel> GetCareplanIntervention2(string IdCpIntervention)
       {
           try
           {
               if (String.IsNullOrEmpty(IdCpIntervention))
                  throw new Exception("Invalid IdCpIntervention");

           }
           catch (Exception ex)
           {
               this.Response.StatusCode = 500;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }

           try
           {
               return await _theService.GetCareplanIntervention2(IdCpIntervention);
           }
           catch (HttpResponseException ex)
           {
               this.Response.StatusCode = ex.StatusCode;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }
       }

        /// <summary>
        /// Gets the list of the existing intervetions
        /// </summary>        
        /// <param name="skip">First registry of the search Example:0</param>
        /// <param name="take">Maximum number of records of the search Example:0</param>
        /// <param name="Codification">Codification of the intervention Example:GR_ENF_INTERV</param>
        /// <param name="search">Text to search Example:medir</param>
        /// <param name="EncounterKey">Episode in which the episode will take place Example:Internamentos.5000</param>

       // GET: api/experience/NURSINGAPI//careplan/catalog/interventions?skip={skip}&take={take}
       [HttpGet("catalog/interventions")]
       public virtual async Task<List<PckapiEnfCareplanTps_TNursingInterventionClientModelDTOModel>> GetIntervention5([FromQuery] string skip, [FromQuery] string take, [FromQuery] string Codification, [FromQuery] string search, [FromQuery] string EncounterKey)
       {
           try
           {
               if (String.IsNullOrEmpty(skip))
                  throw new Exception("Invalid skip");
               if (String.IsNullOrEmpty(take))
                  throw new Exception("Invalid take");

           }
           catch (Exception ex)
           {
               this.Response.StatusCode = 500;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }

           try
           {
               return await _theService.GetIntervention5(skip, take, Codification, search, EncounterKey);
           }
           catch (HttpResponseException ex)
           {
               this.Response.StatusCode = ex.StatusCode;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }
       }

        /// <summary>
        /// Get the list of the existing frequencies to plan interventions
        /// </summary>        
        /// <param name="skip">First registry of the search Example:0</param>
        /// <param name="take">Maximum number of records of the search Example:0</param>
        /// <param name="search">Text to search Example:medir</param>

       // GET: api/experience/NURSINGAPI//careplan/catalog/frequencies?skip={skip}&take={take}
       [HttpGet("catalog/frequencies")]
       public virtual async Task<List<PckapiEnfCareplanTps_TNursingFrequencyClientModelDTOModel>> GetFrequencies6([FromQuery] string skip, [FromQuery] string take, [FromQuery] string search)
       {
           try
           {
               if (String.IsNullOrEmpty(skip))
                  throw new Exception("Invalid skip");
               if (String.IsNullOrEmpty(take))
                  throw new Exception("Invalid take");

           }
           catch (Exception ex)
           {
               this.Response.StatusCode = 500;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }

           try
           {
               return await _theService.GetFrequencies6(skip, take, search);
           }
           catch (HttpResponseException ex)
           {
               this.Response.StatusCode = ex.StatusCode;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }
       }

        /// <summary>
        /// Get the list of the existing schedules to plan interventions
        /// </summary>        
        /// <param name="skip">First registry of the search Example:0</param>
        /// <param name="take">Maximum number of records of the search Example:0</param>
        /// <param name="frequency">Text to search Example:8h-8h</param>
        /// <param name="search">Text to search Example:medir</param>

       // GET: api/experience/NURSINGAPI//careplan/catalog/schedules?skip={skip}&take={take}
       [HttpGet("catalog/schedules")]
       public virtual async Task<List<PckapiEnfCareplanTps_TNursingScheduleClientModelDTOModel>> GetSchedules7([FromQuery] string skip, [FromQuery] string take, [FromQuery] string frequency, [FromQuery] string search)
       {
           try
           {
               if (String.IsNullOrEmpty(skip))
                  throw new Exception("Invalid skip");
               if (String.IsNullOrEmpty(take))
                  throw new Exception("Invalid take");

           }
           catch (Exception ex)
           {
               this.Response.StatusCode = 500;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }

           try
           {
               return await _theService.GetSchedules7(skip, take, frequency, search);
           }
           catch (HttpResponseException ex)
           {
               this.Response.StatusCode = ex.StatusCode;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }
       }

        /// <summary>
        /// Gets the list of the existing indicators
        /// </summary>        
        /// <param name="skip">First registry of the search Example:0</param>
        /// <param name="take">Maximum number of records of the search Example:0</param>
        /// <param name="search">Text to search Example:medir</param>
        /// <param name="datatype">DataType of the indicator Example:number</param>

       // GET: api/experience/NURSINGAPI//careplan/catalog/indicators?skip={skip}&take={take}
       [HttpGet("catalog/indicators")]
       public virtual async Task<List<PckapiEnfCareplanTps_TNursingIndicatorClientModelDTOModel>> GetIndicators8([FromQuery] string skip, [FromQuery] string take, [FromQuery] string search, [FromQuery] string datatype)
       {
           try
           {
               if (String.IsNullOrEmpty(skip))
                  throw new Exception("Invalid skip");
               if (String.IsNullOrEmpty(take))
                  throw new Exception("Invalid take");

           }
           catch (Exception ex)
           {
               this.Response.StatusCode = 500;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }

           try
           {
               return await _theService.GetIndicators8(skip, take, search, datatype);
           }
           catch (HttpResponseException ex)
           {
               this.Response.StatusCode = ex.StatusCode;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }
       }

        /// <summary>
        /// Creates one planned intervention.
        /// </summary>

       // POST: api/experience/NURSINGAPI//careplan/intervention
       [HttpPost("intervention")]
       public virtual async Task<PckapiEnfCareplanTps_TNursingPlanInterventionClientModelDTOModel> PostCareplanIntervention1([FromBody] PckapiEnfCareplanTps_TNursingPlanInterventionClientModelDTOModel _data)
       {
           try
           {

               if (_data is null)
                   throw new Exception("Invalid data");
           }
           catch (Exception ex)
           {
               this.Response.StatusCode = 500;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }

           try
           {
               return await _theService.PostCareplanIntervention1(_data);

           }
           catch (HttpResponseException ex)
           {
               this.Response.StatusCode = ex.StatusCode;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }
       }

        /// <summary>
        /// Updates the properties of one planned intervention.
        /// </summary>

       // POST: api/experience/NURSINGAPI//careplan/intervention/{IdCpIntervention}
       [HttpPost("intervention/{IdCpIntervention}")]
       public virtual async Task<PckapiEnfCareplanTps_TNursingPlanInterventionClientModelDTOModel> PostCareplanIntervention3(string IdCpIntervention,[FromBody] PckapiEnfCareplanTps_TNursingPlanInterventionClientModelDTOModel _data)
       {
           try
           {
               if (String.IsNullOrEmpty(IdCpIntervention))
                  throw new Exception("Invalid IdCpIntervention");

               if (_data is null)
                   throw new Exception("Invalid data");
           }
           catch (Exception ex)
           {
               this.Response.StatusCode = 500;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }

           try
           {
               return await _theService.PostCareplanIntervention3(IdCpIntervention,_data);

           }
           catch (HttpResponseException ex)
           {
               this.Response.StatusCode = ex.StatusCode;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }
       }

        /// <summary>
        /// Updates intervention scheduling
        /// </summary>

       // POST: api/experience/NURSINGAPI//careplan/intervention/{IdCpIntervention}/schedule
       [HttpPost("intervention/{IdCpIntervention}/schedule")]
       public virtual async Task<PckapiEnfCareplanTps_TNursingPlanInterventionClientModelDTOModel> PostSchedule4(string IdCpIntervention,[FromBody] PckapiEnfCareplanTps_TNursingSchedulingClientModelDTOModel _data)
       {
           try
           {
               if (String.IsNullOrEmpty(IdCpIntervention))
                  throw new Exception("Invalid IdCpIntervention");

               if (_data is null)
                   throw new Exception("Invalid data");
           }
           catch (Exception ex)
           {
               this.Response.StatusCode = 500;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }

           try
           {
               return await _theService.PostSchedule4(IdCpIntervention,_data);

           }
           catch (HttpResponseException ex)
           {
               this.Response.StatusCode = ex.StatusCode;
               await this.Response.WriteAsync(ex.Message);
               return null;
           }
       }

   }
}
