PACKAGE BODY PCK_ENF_CAREPLAN
IS
    FUNCTION traduz_msgbox ( texto ga_dic_textos.texto_portugues%TYPE ) RETURN ga_dic_traducoes.texto_traduzido%TYPE IS BEGIN RETURN ga_pck_dicionario.traduz_texto_msgbox ( texto,'BD',sdf_enf_current_schema || '.' || $$plsql_unit); END;
    FUNCTION traduz_msgbox ( texto ga_dic_textos.texto_portugues%TYPE, i_var VARCHAR2 ) RETURN ga_dic_traducoes.texto_traduzido%TYPE IS BEGIN RETURN ga_pck_dicionario.traduz_texto_msgbox ( texto,i_var,NULL,'BD',sdf_enf_current_schema || '.' || $$plsql_unit); END;
    FUNCTION traduz_msgbox ( texto ga_dic_textos.texto_portugues%TYPE, i_vars DBMS_SQL.varchar2a ) RETURN ga_dic_traducoes.texto_traduzido%TYPE IS BEGIN RETURN ga_pck_dicionario.traduz_texto_msgbox ( texto, i_vars, 'BD', sdf_enf_current_schema || '.' || $$plsql_unit); END;
    FUNCTION traduz_msgbox ( texto ga_dic_textos.texto_portugues%TYPE, i_vars VARCHAR2, i_separador VARCHAR2 ) RETURN ga_dic_traducoes.texto_traduzido%TYPE IS BEGIN RETURN ga_pck_dicionario.traduz_texto_msgbox ( texto, i_vars, i_separador, 'BD', sdf_enf_current_schema || '.' || $$plsql_unit ); END;
    FUNCTION traduz ( texto ga_dic_textos.texto_portugues%TYPE ) RETURN ga_dic_traducoes.texto_traduzido%TYPE IS BEGIN RETURN ga_pck_dicionario.traduz_texto ( texto, 'BD',sdf_enf_current_schema || '.' || $$plsql_unit ); END;
    FUNCTION traduz ( texto ga_dic_textos.texto_portugues%TYPE, i_var VARCHAR2 )  RETURN ga_dic_traducoes.texto_traduzido%TYPE IS BEGIN RETURN ga_pck_dicionario.traduz_texto ( texto, i_var, NULL, 'BD', sdf_enf_current_schema || '.' || $$plsql_unit ); END;
    FUNCTION traduz ( texto ga_dic_textos.texto_portugues%TYPE, i_vars DBMS_SQL.varchar2a ) RETURN ga_dic_traducoes.texto_traduzido%TYPE IS BEGIN RETURN ga_pck_dicionario.traduz_texto ( texto, i_vars, 'BD', sdf_enf_current_schema || '.' || $$plsql_unit ); END;
    FUNCTION current_USER RETURN VARCHAR2 IS BEGIN RETURN TP_PCK_UTILIZADOR_APL.LE_UTIL_APL; END;

    FUNCTION aux_GET_STAMP(i_DATE DATE) RETURN NUMBER
    IS
    BEGIN
        RETURN 86400 * ( i_DATE - to_date('2000','yyyy' ) );
    END aux_GET_STAMP;

--**************************************************************************************************
/**
Method that validates one T_NURSING_PLAN_INTERVENTION object
@param  I_QUERY_PARAMETERS  List of query parameters
@param  I_URI_PARAMETERS    List of uri parameters
@param  I_OBJECT            JSON representing the input object
@return O_OBJECT            JSON representing the output object
@return O_REQUEST_STATUS    JSON representing the request status
*/
    PROCEDURE validate_PLAN_INTERVENTION
    (
        io_PLAN_INTERVENTION     IN OUT PCKAPI_ENF_CAREPLAN_TPS.T_NURSING_PLAN_INTERVENTION,
        o_MESSAGE               OUT VARCHAR2
    ) IS
        L_PATIENT_KEY       VARCHAR2(100);
        L_ENCOUNTER_KEY     VARCHAR2(100);
        L_T_DOENTE          SD_ENF_PLANO_CUID_INTERV.T_DOENTE%TYPE;
        L_DOENTE            SD_ENF_PLANO_CUID_INTERV.DOENTE%TYPE;
        L_T_EPISODIO        SD_ENF_PLANO_CUID_INTERV.T_EPISODIO%TYPE;
        L_EPISODIO          SD_ENF_PLANO_CUID_INTERV.EPISODIO%TYPE;
        L_COUNT             NUMBER;
        L_REFDATE           DATE;

        PROCEDURE RAISE_THIS(i_TEXT VARCHAR2) IS
        BEGIN
            o_MESSAGE   := i_TEXT;
            RAISE GUTILSK_API_COMMON.E_SAIR_SEM_ERRO;
        END RAISE_THIS;
    BEGIN

        --******************************************************************************************
        --| Validar doente
        IF io_PLAN_INTERVENTION.SUBJECT.PATIENT_KEY IS NULL AND
        (
            io_PLAN_INTERVENTION.SUBJECT.PATIENT_TYPE IS NULL OR
            io_PLAN_INTERVENTION.SUBJECT.PATIENT_ID IS NULL
        )
        THEN
            RAISE_THIS('Missing mandatory field: Subject!');
        END IF;

        IF io_PLAN_INTERVENTION.SUBJECT.PATIENT_KEY IS NOT NULL
        THEN
            L_T_DOENTE      := substr(io_PLAN_INTERVENTION.SUBJECT.PATIENT_KEY,1,instr(io_PLAN_INTERVENTION.SUBJECT.PATIENT_KEY,'.')-1);
            L_DOENTE        := substr(io_PLAN_INTERVENTION.SUBJECT.PATIENT_KEY,instr(io_PLAN_INTERVENTION.SUBJECT.PATIENT_KEY,'.')+1,100);

            IF L_T_DOENTE <> nvl(io_PLAN_INTERVENTION.SUBJECT.PATIENT_TYPE   ,L_T_DOENTE) OR
               L_DOENTE   <> nvl(io_PLAN_INTERVENTION.SUBJECT.PATIENT_ID     ,L_DOENTE)
            THEN
                RAISE_THIS('Patient mismatch!');
            END IF;

            io_PLAN_INTERVENTION.SUBJECT.PATIENT_TYPE   := L_T_DOENTE;
            io_PLAN_INTERVENTION.SUBJECT.PATIENT_ID     := L_DOENTE;
        END IF;

        IF io_PLAN_INTERVENTION.SUBJECT.PATIENT_ID IS NOT NULL
        THEN
            L_PATIENT_KEY := io_PLAN_INTERVENTION.SUBJECT.PATIENT_TYPE||'.'||io_PLAN_INTERVENTION.SUBJECT.PATIENT_ID;

            SELECT COUNT(1)
              INTO L_COUNT
              FROM SD_DOENTE
             WHERE T_DOENTE = io_PLAN_INTERVENTION.SUBJECT.PATIENT_TYPE
               AND DOENTE   = io_PLAN_INTERVENTION.SUBJECT.PATIENT_ID;

            IF L_COUNT = 0
            THEN
                RAISE_THIS('Patient does not exist!');
            END IF;

        END IF;

        --******************************************************************************************
        --| Validar episodio
        IF io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_KEY IS NULL AND
        (
            io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_TYPE IS NULL OR
            io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_ID IS NULL
        )
        THEN
            RAISE_THIS('Missing mandatory field: ENCOUNTER!');
        END IF;

        IF io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_KEY IS NOT NULL
        THEN
            L_T_EPISODIO    := substr(io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_KEY,1,instr(io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_KEY,'.')-1);
            L_EPISODIO      := substr(io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_KEY,instr(io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_KEY,'.')+1,100);

            IF L_T_EPISODIO <> nvl(io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_TYPE   ,L_T_EPISODIO) OR
               L_EPISODIO   <> nvl(io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_ID     ,L_EPISODIO)
            THEN
                RAISE_THIS('ENCOUNTER mismatch!');
            END IF;

            io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_TYPE   := L_T_EPISODIO;
            io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_ID     := L_EPISODIO;
        END IF;

        IF io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_ID IS NOT NULL
        THEN
            L_ENCOUNTER_KEY := io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_TYPE||'.'||io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_ID;

            SELECT COUNT(1)
              INTO L_COUNT
              FROM SD_EPISODIO
             WHERE T_DOENTE     = io_PLAN_INTERVENTION.SUBJECT.PATIENT_TYPE
               AND DOENTE       = io_PLAN_INTERVENTION.SUBJECT.PATIENT_ID
               AND T_EPISODIO   = io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_TYPE
               AND EPISODIO     = io_PLAN_INTERVENTION.ENCOUNTER.ENCOUNTER_ID;

            IF L_COUNT = 0
            THEN
                RAISE_THIS('Patient or Episode mismatch!');
            END IF;

        END IF;

        --******************************************************************************************
        --| Validar intervenções
        SELECT COUNT(1)
          INTO L_COUNT
          FROM PHV_N_MEDICAMENTOSOS
         WHERE TABELA   = io_PLAN_INTERVENTION.INTERVENTION.CODIFICATION
           AND CODIGO   = io_PLAN_INTERVENTION.INTERVENTION.CODE;

        IF L_COUNT = 0
        THEN
            RAISE_THIS('Invalid Code or Codification!');
        END IF;

        --******************************************************************************************
        --| Validar agendamento
        IF io_PLAN_INTERVENTION.SCHEDULING.FREQUENCY IS NULl
        THEN
            RAISE_THIS('Invalid or missing frequency');
        END IF;

        IF io_PLAN_INTERVENTION.SCHEDULING.SCHEDULE IS NULl
        THEN
            RAISE_THIS('Invalid or missing schedule');
        END IF;

        IF io_PLAN_INTERVENTION.SCHEDULING.START_DATE IS NULL
        THEN
            RAISE_THIS('Invalid start date');
        END IF;

        IF io_PLAN_INTERVENTION.SCHEDULING.END_DATE < io_PLAN_INTERVENTION.SCHEDULING.START_DATE
        THEN
            RAISE_THIS('End date cannot be lower than the start date');
        END IF;

        --******************************************************************************************
        --| Validar stamp
        IF io_PLAN_INTERVENTION.ID_CP_INTERVENTION IS NOT NULL
        THEN

            BEGIN
                SELECT nvl(DT_ACT,DT_CRI)
                  INTO L_REFDATE
                  FROM SD_ENF_PLANO_CUID_INTERV
                 WHERE ID_AGRUPADOR = io_PLAN_INTERVENTION.ID_CP_INTERVENTION;

                IF aux_GET_STAMP(L_REFDATE) <> io_PLAN_INTERVENTION.RECORD_INFO.RECORD_VERSION
                THEN
                    RAISE_THIS('Record version does not match. Cannot update the record!');
                END IF;

            EXCEPTION WHEN NO_DATA_FOUND THEN
                RAISE_THIS('Invalid IdCpInterv ' ||io_PLAN_INTERVENTION.ID_CP_INTERVENTION);
            END;

        END IF;

    EXCEPTION WHEN GUTILSK_API_COMMON.E_SAIR_SEM_ERRO
    THEN
        RETURN;
    END validate_PLAN_INTERVENTION;



--**************************************************************************************************
/**
Method that returns on planned intervention by id

@param  I_QUERY_PARAMETERS  List of query parameters
@param  I_URI_PARAMETERS    List of uri parameters
@param  I_OBJECT            JSON representing the input object
@return O_OBJECT            JSON representing the output object
@return O_REQUEST_STATUS    JSON representing the request status
*/
    FUNCTION GET_INTERVENTION
    (
        i_ID_AGRUPADOR      IN     NUMBER,
        io_MSG              IN OUT VARCHAR2
    )
    RETURN PCKAPI_ENF_CAREPLAN_TPS.T_NURSING_PLAN_INTERVENTION
    IS
        row_                    SD_ENF_PLANO_CUID_INTERV%ROWTYPE;
        L_CP_INTERVENTION       PCKAPI_ENF_CAREPLAN_TPS.T_NURSING_PLAN_INTERVENTION;
        L_CP_CALENDAR           PCKAPI_ENF_CAREPLAN_TPS.T_NURSING_SCHEDULING;
        L_PLH_CALENDAR          PHK_INTER_VAL_CALEND_TPS.T_SCHEDULING;
    BEGIN
        BEGIN
            SELECT *
              INTO row_
              FROM SD_ENF_PLANO_CUID_INTERV
             WHERE ID_AGRUPADOR = i_ID_AGRUPADOR;
        EXCEPTION WHEN NO_DATA_FOUND THEN
            io_MSG := 'Intervention does not exist [i_ID_AGRUPADOR-'|| i_ID_AGRUPADOR || ']';
            RETURN NULL;
        END;

        L_CP_INTERVENTION.ID_CP_INTERVENTION           := row_.ID_AGRUPADOR;
        L_CP_INTERVENTION.OBSERVATIONS                 := row_.OBS;
        L_CP_INTERVENTION.SCHEDULING.START_DATE        := row_.DT_INI;
        L_CP_INTERVENTION.SCHEDULING.END_DATE          := row_.DT_FIM;
        L_CP_INTERVENTION.SCHEDULING.FREQUENCY         := row_.COD_FREQ;
        L_CP_INTERVENTION.SCHEDULING.SCHEDULE          := row_.HORARIO;
        L_CP_INTERVENTION.INTERVENTION.CODIFICATION    := row_.TABELA;
        L_CP_INTERVENTION.INTERVENTION.CODE            := row_.COD_INTERV;

        L_CP_INTERVENTION.SUBJECT.PATIENT_KEY           := row_.T_DOENTE || '.' || row_.DOENTE;
        L_CP_INTERVENTION.SUBJECT.PATIENT_TYPE          := row_.T_DOENTE;
        L_CP_INTERVENTION.SUBJECT.PATIENT_ID            := row_.DOENTE;

        L_CP_INTERVENTION.ENCOUNTER.ENCOUNTER_KEY       := row_.T_EPISODIO || '.' || row_.EPISODIO;
        L_CP_INTERVENTION.ENCOUNTER.ENCOUNTER_TYPE      := row_.T_EPISODIO;
        L_CP_INTERVENTION.ENCOUNTER.ENCOUNTER_ID        := row_.EPISODIO;

        BEGIN
            SELECT DESCRICAO
              INTO L_CP_INTERVENTION.INTERVENTION.DESCRIPTION
              FROM PHV_N_MEDICAMENTOSOS
             WHERE TABELA   = row_.TABELA
               AND CODIGO   = row_.COD_INTERV;
        END;


        L_CP_INTERVENTION.RECORD_INFO.CREATION_DATE     := row_.DT_CRI;
        L_CP_INTERVENTION.RECORD_INFO.CREATION_USER     := row_.USER_CRI;
        L_CP_INTERVENTION.RECORD_INFO.LAST_UPDATE_DATE  := row_.DT_ACT;
        L_CP_INTERVENTION.RECORD_INFO.LAST_UPDATE_USER  := row_.USER_ACT;
        L_CP_INTERVENTION.RECORD_INFO.RECORD_VERSION    := AUX_GET_STAMP(nvl(row_.DT_ACT,row_.DT_CRI));

        IF NVL(row_.DT_FIM,GA_SYSDATE+1) < GA_SYSDATE THEN
            L_CP_INTERVENTION.RECORD_INFO.UPDATE_ALLOWED    := false;
        ELSE
            L_CP_INTERVENTION.RECORD_INFO.UPDATE_ALLOWED    := true;
        END IF;

        PHK_INTER_VAL_CALEND.READ_CALENDARY(
            'INTERVS',
            row_.T_DOENTE,
            row_.DOENTE,
            i_ID_AGRUPADOR,
            NULL,
            L_PLH_CALENDAR,
            io_MSG
        );

        L_CP_CALENDAR   := PCK_EPRE_COMMON_FNCS.PLH_SCHED_to_ENF_SCHED(L_PLH_CALENDAR);

        L_CP_INTERVENTION.SCHEDULING.ENDS_IN_X_DAYS := L_CP_CALENDAR.ENDS_IN_X_DAYS;
        L_CP_INTERVENTION.SCHEDULING.CALENDAR_MODE := L_CP_CALENDAR.CALENDAR_MODE;
        L_CP_INTERVENTION.SCHEDULING.CALENDAR_DAILY := L_CP_CALENDAR.CALENDAR_DAILY;
        L_CP_INTERVENTION.SCHEDULING.CALENDAR_ADMINISTRATION := L_CP_CALENDAR.CALENDAR_ADMINISTRATION;
        L_CP_INTERVENTION.SCHEDULING.CALENDAR_WEEK := L_CP_CALENDAR.CALENDAR_WEEK;
        L_CP_INTERVENTION.SCHEDULING.CALENDAR_MONTH := L_CP_CALENDAR.CALENDAR_MONTH;
        L_CP_INTERVENTION.SCHEDULING.CALENDAR_DAYS := L_CP_CALENDAR.CALENDAR_DAYS;

        RETURN L_CP_INTERVENTION;

    EXCEPTION WHEN OTHERS
    THEN
        io_MSG := 'GET_INTERVENTION [i_ID_AGRUPADOR-'|| i_ID_AGRUPADOR || '] | '|| sqlerrm;
    END;


    FUNCTION CREATE_INTERVENTION
    (
        i_NEW_INTERVENTION   IN  PCKAPI_ENF_CAREPLAN_TPS.T_NURSING_PLAN_INTERVENTION,
        io_MSG               IN OUT VARCHAR2
    )
    RETURN VARCHAR2
    IS
        l_MSG               VARCHAR2(4000);
        l_id_agrupador      sd_enf_plano_cuid_interv.id_agrupador%type;
        w_tp_user           varchar2(1);
        w_current_user      varchar2(100) := current_USER;

        PROCEDURE RAISE_THIS(i_TEXT VARCHAR2) IS
        BEGIN
            io_MSG   := i_TEXT;
            RAISE GUTILSK_API_COMMON.E_SAIR_SEM_ERRO;
        END RAISE_THIS;
    BEGIN

        BEGIN
            SELECT SUBSTR(T_PESS_HOSP,1,1)
              INTO w_tp_user
              FROM SD_PESS_HOSP_DEF
             WHERE USER_SYS = w_current_user;
         EXCEPTION WHEN OTHERS THEN
            RAISE_THIS('Invalid user '||w_current_user);
         END;


        l_id_agrupador :=
           pckt_enf_plano_cuidados.insere_plano_cuidados_mobile(
                                                      i_t_doente        =>     i_NEW_INTERVENTION.SUBJECT.PATIENT_TYPE,
                                                      i_doente          =>     i_NEW_INTERVENTION.SUBJECT.PATIENT_ID,
                                                      i_t_episodio      =>     i_NEW_INTERVENTION.ENCOUNTER.ENCOUNTER_TYPE,
                                                      i_episodio        =>     i_NEW_INTERVENTION.ENCOUNTER.ENCOUNTER_ID,
                                                      i_cod_interv      =>     i_NEW_INTERVENTION.INTERVENTION.CODE,
                                                      i_cod_freq        =>     i_NEW_INTERVENTION.SCHEDULING.FREQUENCY,
                                                      i_horario         =>     i_NEW_INTERVENTION.SCHEDULING.SCHEDULE,
                                                      i_dt_ini          =>     i_NEW_INTERVENTION.SCHEDULING.START_DATE,
                                                      i_dt_fim          =>     i_NEW_INTERVENTION.SCHEDULING.END_DATE,
                                                      i_tabela          =>     i_NEW_INTERVENTION.INTERVENTION.CODIFICATION,
                                                      i_obs             =>     i_NEW_INTERVENTION.OBSERVATIONS,
                                                      i_tp_user         =>     w_tp_user,
                                                      i_user_sys        =>     w_current_user,
                                                      o_msg             =>     l_MSG);

        IF NVL(l_MSG,'OK') <> 'OK'
        THEN
            io_MSG := l_MSG;
        ELSE COMMIT;
        END IF;

        RETURN l_id_agrupador;

    EXCEPTION WHEN OTHERS
    THEN
        io_MSG := 'CREATE_INTERVENTION ERRO -> '|| sqlerrm;
    END;


    FUNCTION UPDATE_INTERVENTION
    (
        i_INTERVENTION       IN  PCKAPI_ENF_CAREPLAN_TPS.T_NURSING_PLAN_INTERVENTION,
        io_MSG               IN OUT VARCHAR2
    )
    RETURN VARCHAR2
    IS
        l_MSG               VARCHAR2(4000);
        l_id_agrupador      sd_enf_plano_cuid_interv.id_agrupador%type;
    BEGIN

        l_id_agrupador :=
            pckt_enf_plano_cuidados.update_plano_cuidados_mobile(
                                                      i_id_agrupador    =>     i_INTERVENTION.ID_CP_INTERVENTION,
                                                      i_cod_freq        =>     i_INTERVENTION.SCHEDULING.FREQUENCY,
                                                      i_horario         =>     i_INTERVENTION.SCHEDULING.SCHEDULE,
                                                      i_dt_ini          =>     i_INTERVENTION.SCHEDULING.START_DATE,
                                                      i_dt_fim          =>     i_INTERVENTION.SCHEDULING.END_DATE,
                                                      i_tabela          =>     i_INTERVENTION.INTERVENTION.CODIFICATION,
                                                      i_obs             =>     i_INTERVENTION.OBSERVATIONS,
                                                      o_msg             =>     l_MSG);

        IF NVL(l_MSG,'OK') <> 'OK'
        THEN
            io_MSG := l_MSG;
        ELSE COMMIT;
        END IF;

        RETURN l_id_agrupador;

    EXCEPTION WHEN OTHERS
    THEN
        io_MSG := 'UPDATE_INTERVENTION ERRO -> '|| sqlerrm;
    END;

END PCK_ENF_CAREPLAN;