--| PH
grant execute on PH.phk_inter_val_calend to proxy with grant option
/
grant execute on PH.phk_inter_val_calend_tps to proxy with grant option
/

--| PROXY
begin
    ga.ga_install_objects.create_synonym('PROXY','phk_inter_val_calend','PH','phk_inter_val_calend');
    ga.ga_install_objects.create_synonym('PROXY','phk_inter_val_calend_tps','PH','phk_inter_val_calend_tps');
end;    
/
grant execute on PH.phk_inter_val_calend to ENF
/
grant execute on PH.phk_inter_val_calend_tps to ENF
/

--| ENF
begin
    ga.ga_install_objects.create_synonym('ENF','phk_inter_val_calend','PROXY','phk_inter_val_calend');
    ga.ga_install_objects.create_synonym('ENF','phk_inter_val_calend_tps','PROXY','phk_inter_val_calend_tps');
end;    
/
