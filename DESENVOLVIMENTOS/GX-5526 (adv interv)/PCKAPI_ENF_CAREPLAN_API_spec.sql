PACKAGE PCKAPI_ENF_CAREPLAN_API
  IS

    --**********************************************************************************************
    --**********************************************************************************************
    /*#GMONKEYREST#
    {
        "project_name":"NURSINGAPI",
        "generated":false,
        "micro_service":{
            "name":"careplan",
            "descricao":"bio_measure API",
            "base_path":"",
            "base_uri":"http://locahost:8086/MedOn/api/nursing/careplan",
            "dnet_controller_name": "Careplan"
        },
        "paths":[
            {"id_path":1,                       "name":"intervention"       ,"is_uri_param": false},
            {"id_path":2,"parent_id_path":1,    "name":"IdCpIntervention"   ,"is_uri_param": true},
            {"id_path":3,                       "name":"catalog"            ,"is_uri_param": false},
            {"id_path":4,"parent_id_path":3,    "name":"interventions"      ,"is_uri_param": false},
            {"id_path":5,"parent_id_path":3,    "name":"frequencies"        ,"is_uri_param": false},
            {"id_path":6,"parent_id_path":3,    "name":"schedules"          ,"is_uri_param": false},
            {"id_path":7,"parent_id_path":2,    "name":"schedule"           ,"is_uri_param": false},
            {"id_path":8,"parent_id_path":3,    "name":"indicators"         ,"is_uri_param": false},
            {"id_path":9,                       "name":"calendar"           ,"is_uri_param": false},
            {"id_path":10,                      "name":"endDate"            ,"is_uri_param": false}
        ],
        "methods":[
            {
              "method_type":"GET",
              "id_path":2,
              "name":"careplan_intervention",
              "descricao":"Get the details of one planned intervention.",
              "uri_parameters":[
                {
                  "name":"IdCpIntervention",
                  "description":"The id of the planned intervention",
                  "type":"number",
                  "required":true,
                  "example":10000
                }],
              "query_parameters":[],
              "procedure_name":"GET_CP_INTERVENTION",
              "parameter_out_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"t_nursing_plan_intervention"
              },
              "parameter_in_type":{},
              "traits":[{"name":"has400Errors"}],
              "system":true,
              "process":false,
              "experience":true
            },
            {
              "method_type":"POST",
              "id_path":2,
              "name":"careplan_intervention",
              "descricao":"Updates the properties of one planned intervention.",
              "uri_parameters":[
                {
                  "name":"IdCpIntervention",
                  "description":"The id of the planned intervention",
                  "type":"number",
                  "required":true,
                  "example":10000
                }],
              "query_parameters":[],
              "procedure_name":"PUT_OR_POST_CP_INTERVENTION",
              "parameter_out_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"t_nursing_plan_intervention"
              },
              "parameter_in_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"t_nursing_plan_intervention"
              },
              "traits":[{"name":"has400Errors"}],
              "system":true,
              "process":false,
              "experience":true
            },
            {
              "method_type":"POST",
              "id_path":1,
              "name":"careplan_intervention",
              "descricao":"Creates one planned intervention.",
              "uri_parameters":[],
              "query_parameters":[],
              "procedure_name":"PUT_OR_POST_CP_INTERVENTION",
              "parameter_out_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"t_nursing_plan_intervention"
              },
              "parameter_in_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"t_nursing_plan_intervention"
              },
              "traits":[{"name":"has400Errors"}],
              "system":true,
              "process":false,
              "experience":true
            },
            {
              "method_type":"GET",
              "id_path":4,
              "name":"intervention",
              "descricao":"Gets the list of the existing intervetions",
              "uri_parameters":[],
              "query_parameters":[
                {
                  "name":"Codification",
                  "description":"Codification of the intervention",
                  "type":"string",
                  "required":false,
                  "example":"GR_ENF_INTERV"
                },
                {
                  "name":"search",
                  "description":"Text to search",
                  "type":"string",
                  "required":false,
                  "example":"medir"
                },
                {
                  "name":"EncounterKey",
                  "description":"Episode in which the episode will take place",
                  "type":"string",
                  "required":false,
                  "example":"Internamentos.5000"
                }
              ],
              "procedure_name":"GET_CATALOG_INTERVENTIONS",
              "parameter_out_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"tt_nursing_intervention"
              },
              "parameter_in_type":{},
              "traits":[{"name":"has400Errors"},{"name":"pageableRequired"}],
              "system":true,
              "process":false,
              "experience":true
            },
            {
              "method_type":"GET",
              "id_path":5,
              "name":"frequencies",
              "descricao":"Get the list of the existing frequencies to plan interventions",
              "uri_parameters":[],
              "query_parameters":[
                {
                  "name":"search",
                  "description":"Text to search",
                  "type":"string",
                  "required":false,
                  "example":"medir"
                }
              ],
              "procedure_name":"GET_CATALOG_FREQUENCIES",
              "parameter_out_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"tt_nursing_frequency"
              },
              "parameter_in_type":{},
              "traits":[{"name":"has400Errors"},{"name":"pageableRequired"}],
              "system":true,
              "process":false,
              "experience":true
            },
            {
              "method_type":"GET",
              "id_path":6,
              "name":"schedules",
              "descricao":"Get the list of the existing schedules to plan interventions",
              "uri_parameters":[],
              "query_parameters":[
                {
                  "name":"frequency",
                  "description":"Text to search",
                  "type":"string",
                  "required":false,
                  "example":"8h-8h"
                },
                {
                  "name":"search",
                  "description":"Text to search",
                  "type":"string",
                  "required":false,
                  "example":"medir"
                }
              ],
              "procedure_name":"GET_CATALOG_SCHEDULES",
              "parameter_out_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"tt_nursing_schedule"
              },
              "parameter_in_type":{},
              "traits":[{"name":"has400Errors"},{"name":"pageableRequired"}],
              "system":true,
              "process":false,
              "experience":true
            },
            {
              "method_type":"POST",
              "id_path":7,
              "name":"schedule",
              "descricao":"Updates intervention scheduling",
              "uri_parameters":[
                {
                  "name":"IdCpIntervention",
                  "description":"The id of the planned intervention",
                  "type":"number",
                  "required":true,
                  "example":10000
                }],
              "query_parameters":[],
              "procedure_name":"POST_INTERVENTION_SCHEDULE",
              "parameter_out_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"t_nursing_plan_intervention"
              },
              "parameter_in_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"t_nursing_scheduling"
              },
              "traits":[{"name":"has400Errors"}],
              "system":true,
              "process":false,
              "experience":true
            },
            {
              "method_type":"GET",
              "id_path":8,
              "name":"indicators",
              "descricao":"Gets the list of the existing indicators",
              "uri_parameters":[],
              "query_parameters":[
                {
                  "name":"search",
                  "description":"Text to search",
                  "type":"string",
                  "required":false,
                  "example":"medir"
                },
                {
                  "name":"datatype",
                  "description":"DataType of the indicator",
                  "type":"string",
                  "required":false,
                  "example":"number"
                }
              ],
              "procedure_name":"GET_CATALOG_INDICATORS",
              "parameter_out_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"tt_nursing_indicator"
              },
              "parameter_in_type":{},
              "traits":[{"name":"has400Errors"},{"name":"pageableRequired"}],
              "system":true,
              "process":false,
              "experience":true
            },
            {
                "method_type":"POST",
                "id_path":9,
                "name":"calendar",
                "descricao":"Calculates calendar days with sheduled administrations",
                "uri_parameters":[
                {
                    "name":"IdCpIntervention",
                    "description":"The id of the planned intervention",
                    "type":"number",
                    "required":true,
                    "example":10000
                }],
                "query_parameters":[],
                "procedure_name":"POST_CP_CALENDAR",
                "parameter_out_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"tt_nursing_scheduling_days"
                },
                "parameter_in_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"t_nursing_scheduling"
                },
                "traits":[{"name":"has400Errors"},{"name":"pageableRequired"}],
                "system":true,
                "process":false,
                "experience":true
            },
            {
                "method_type":"POST",
                "id_path":10,
                "name":"calendar",
                "descricao":"Calculates end date based on chosen frequency",
                "uri_parameters":[
                {
                    "name":"IdCpIntervention",
                    "description":"The id of the planned intervention",
                    "type":"number",
                    "required":true,
                    "example":10000
                }],
                "query_parameters":[],
                "procedure_name":"POST_CP_END_DATE",
                "parameter_out_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"t_nursing_date"
                },
                "parameter_in_type":{
                "user_db":"ENF",
                "package_name":"PCKAPI_ENF_CAREPLAN_TPS",
                "type_name":"t_nursing_scheduling"
                },
                "traits":[{"name":"has400Errors"}],
                "system":true,
                "process":false,
                "experience":true
            }
        ]
    } #ENDGMONKEYREST#*/
    --**********************************************************************************************
    --**********************************************************************************************
    PROCEDURE GET_CP_INTERVENTION
    (
        I_QUERY_PARAMETERS      IN      CLOB,
        I_URI_PARAMETERS        IN      CLOB,
        O_OBJECT                OUT     CLOB,
        O_REQUEST_STATUS        OUT     CLOB
    );


    PROCEDURE PUT_OR_POST_CP_INTERVENTION
    (
        I_QUERY_PARAMETERS      IN      CLOB,
        I_URI_PARAMETERS        IN      CLOB,
        I_OBJECT                IN      CLOB,
        O_OBJECT                OUT     CLOB,
        O_REQUEST_STATUS        OUT     CLOB
    );

    PROCEDURE GET_CATALOG_INTERVENTIONS
    (
        I_QUERY_PARAMETERS      IN      CLOB,
        I_URI_PARAMETERS        IN      CLOB,
        O_OBJECT                OUT     CLOB,
        O_REQUEST_STATUS        OUT     CLOB
    );

    PROCEDURE GET_CATALOG_INDICATORS
    (
        I_QUERY_PARAMETERS      IN      CLOB,
        I_URI_PARAMETERS        IN      CLOB,
        O_OBJECT                OUT     CLOB,
        O_REQUEST_STATUS        OUT     CLOB
    );

    PROCEDURE GET_CATALOG_FREQUENCIES
    (
        I_QUERY_PARAMETERS      IN      CLOB,
        I_URI_PARAMETERS        IN      CLOB,
        O_OBJECT                OUT     CLOB,
        O_REQUEST_STATUS        OUT     CLOB
    );

    PROCEDURE GET_CATALOG_SCHEDULES
    (
        I_QUERY_PARAMETERS      IN      CLOB,
        I_URI_PARAMETERS        IN      CLOB,
        O_OBJECT                OUT     CLOB,
        O_REQUEST_STATUS        OUT     CLOB
    );

    PROCEDURE POST_INTERVENTION_SCHEDULE
    (
        I_QUERY_PARAMETERS      IN      CLOB,
        I_URI_PARAMETERS        IN      CLOB,
        I_OBJECT                IN      CLOB,
        O_OBJECT                OUT     CLOB,
        O_REQUEST_STATUS        OUT     CLOB
    );

    PROCEDURE POST_CP_CALENDAR
    (
        I_QUERY_PARAMETERS      IN      CLOB,
        I_URI_PARAMETERS        IN      CLOB,
        I_OBJECT                IN      CLOB,
        O_OBJECT                OUT     CLOB,
        O_REQUEST_STATUS        OUT     CLOB
    );

    PROCEDURE POST_CP_END_DATE
    (
        I_QUERY_PARAMETERS      IN      CLOB,
        I_URI_PARAMETERS        IN      CLOB,
        I_OBJECT                IN      CLOB,
        O_OBJECT                OUT     CLOB,
        O_REQUEST_STATUS        OUT     CLOB
    );

END PCKAPI_ENF_CAREPLAN_API;