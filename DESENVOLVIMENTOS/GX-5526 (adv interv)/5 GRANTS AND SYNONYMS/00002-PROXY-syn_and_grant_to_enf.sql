begin
    ga.ga_install_objects.create_synonym('PROXY','phk_inter_val_calend','PLH','phk_inter_val_calend');
    ga.ga_install_objects.create_synonym('PROXY','phk_inter_val_calend_tps','PLH','phk_inter_val_calend_tps');
end;    
/
grant execute on PLH.phk_inter_val_calend to ENF
/
grant execute on PLH.phk_inter_val_calend_tps to ENF
/