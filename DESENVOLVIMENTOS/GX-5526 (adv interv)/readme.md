# GX-5526

## Jira

**Epic** [GX-5526](https://glinttdev.atlassian.net/browse/GX-5526)

**User Story Dev** [GX-8127](https://glinttdev.atlassian.net/browse/GX-8127)

## Observações do Agendamento

**Cliente** HSEIT

**Patch** - GCX2003

**TDM** - Fernando Oliveira

**Produto** - Nuno Oliveira

**Frontend** - Paulo Pereira

### Descrição

Como enfermeiro quero realizar diretamente no registo terapêutico (HTML) a gestão da calendarização de intervenções definindo padrões de planeamento.

### Mensagem Check-In

GX-5526|GX-8127
TODO

#### Projecto Backend

##### NURSINGAPI

### Hotfix

+ PCK_EPRE_COMMON_FNCS_body.sql
+ PCK_EPRE_COMMON_FNCS_SPEC.sql
+ PCKAPI_ENF_CAREPLAN_API_body.sql
+ PCKAPI_ENF_CAREPLAN_API_spec.sql
+ PCKAPI_ENF_CAREPLAN_SER_body.sql
+ PCKAPI_ENF_CAREPLAN_SER_spec.sql
+ PCKAPI_ENF_CAREPLAN_TPS_spec.sql
+ PCK_ENF_CAREPLAN_body.sql
+ PCK_ENF_CAREPLAN_spec.sql (?)
+ Publish Glintths.NursingApi (MASTER)
