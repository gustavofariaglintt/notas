-- Start of DDL Script for View EF_BASE.EF_URGENCY_MCDTS
-- Generated 31/10/2019 13:44:13 from EF_BASE@DEMOPRIV

CREATE OR REPLACE FORCE VIEW ef_urgency_mcdts (
   urgency_mcdt_episode_id,
   urgency_mcdt_name,
   urgency_mcdt_exec_serv,
   urgency_mcdt_exec_serv_descr,
   urgency_mcdt_id,
   urgency_mcdt_req_id,
   urgency_mcdt_req_date,
   urgency_mcdt_state,
   urgency_mcdt_start_date,
   urgency_mcdt_end_date,
   urgency_mcdt_tracking_icon,
   urgency_mcdt_state_descr,
   urgency_mcdt_state_to_order_by,
   urgency_mcdt_group )
					   
BEQUEATH DEFINER
AS
SELECT
                --   p_T_EPISODIO URGENCY_MCDT_EPISODE_TYPE,
                   q0.EPISODIO URGENCY_MCDT_EPISODE_ID,
                   q3.valor URGENCY_MCDT_NAME,
                   q4.valor URGENCY_MCDT_EXEC_SERV,
                   q5.descr_serv URGENCY_MCDT_EXEC_SERV_DESCR,
                   q1.ID URGENCY_MCDT_ID,
                   q1.N_REQ_EXAME URGENCY_MCDT_REQ_ID,
                   PRIMEIRO_REGISTO URGENCY_MCDT_REQ_DATE,
                   q1.FLG_ESTADO URGENCY_MCDT_STATE,
                   PRIMEIRO_REGISTO URGENCY_MCDT_START_DATE,
                   ULTIMO_REGISTO URGENCY_MCDT_END_DATE,
                   q2.ICON_FORMS URGENCY_MCDT_TRACKING_ICON,
                   q2.DESCR URGENCY_MCDT_STATE_DESCR,
                   decode(q1.FLG_ESTADO,'DR',1,'D',2,'EC',3,'I',4,5) URGENCY_MCDT_STATE_TO_ORDER_BY,
                   q0.GRUPO URGENCY_MCDT_GROUP
											  
                   FROM (
                        SELECT EPISODIO,
                               COD_EXT_GRUPO,
                               COD_EXT ,
							   GRUPO,
                               max(ID_TRACKING) ID_TRACKING,
                               min(DATA) PRIMEIRO_REGISTO,
                               max(DATA) ULTIMO_REGISTO
                        FROM  TRACKING_ACTIONS
                        WHERE --EPISODIO = p_EPISODIO
                        --AND
                        COD_EVENTO like 'URG_MCDT%'
                        GROUP BY EPISODIO, COD_EXT_GRUPO, COD_EXT, GRUPO) q0,
                        SD_REQ_EXAMES_DET q1,
                        TRACKING_ACTIONS q2,
                        TR_ACCOES_DET q3,
                        TR_ACCOES_DET q4,
                        sd_serv q5
                        WHERE q0.COD_EXT_GRUPO = q1.N_REQ_EXAME
                        AND   q0.COD_EXT = q1.ID
                        AND   q0.ID_TRACKING = q2.ID_TRACKING
                        AND   q0.ID_TRACKING = q3.ID
                        AND   q3.CHAVE like 'DESCR_EXAME'
                        AND   q0.ID_TRACKING = q4.ID
                        AND   q4.CHAVE like 'COD_SERV_EXEC'
                        AND   q4.valor = q5.cod_serv
                        --AND   episodio = p_episodio
                        --ORDER BY URGENCY_MCDT_STATE_TO_ORDER_BY ASC, URGENCY_MCDT_REQ_DATE, URGENCY_MCDT_NAME
/

-- End of DDL Script for View EF_BASE.EF_URGENCY_MCDTS

