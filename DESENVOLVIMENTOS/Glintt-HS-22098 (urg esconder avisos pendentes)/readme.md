# NOTAS

View EF_BASE.EF_URGENCY_MCDTS foi alterada

Nova coluna URGENCY_MCDT_GROUP

Chave gr_param_base: GHPC_URG_DISCHARGE_HIDDEN_MCDTS_COD_SERVS

## Script para inserir a chave

``` SQL
exec PCK_EPR_CONFIG_PATCHS.CRIA_CHAVE_GR_PARAM_BASE(
    p_chave=> 'GHPC_URG_DISCHARGE_HIDDEN_MCDTS_COD_SERVS',
    p_valor=> '',
    p_descritivo=> 'Definição dos serviços (cod_servs separados por #) dos avisos dos mcdts que são para esconder na alta da urgência',
    p_modulo=> 'EPR'
    );
/
commit
/
```

### Changesets

1. DEV:
    210428
    210437
    210443
2. CI:
    210430
    210440
    210445

Todas as alterações estão contidas no dir:
$/SolucoesClinicas/DOTNET/ServicesEF_V2/GlinttHS.Services.Urgency
