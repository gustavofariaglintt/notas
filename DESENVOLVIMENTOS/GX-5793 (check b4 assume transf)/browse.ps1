. '.\buildVS.ps1'

$path = "C:\DOTNET\Services"
$firefox = "C:\Program Files\Mozilla Firefox\firefox.exe"

buildVS $path\BusinessEntities\GlinttHS.BusinessEntities.sln 
buildVS $path\BusinessRules\GlinttHS.BusinessRules.sln
buildVS $path\WebServices\GlinttHS.WebServices.sln

# Load Assemblies into the GAC
# [System.Reflection.Assembly]::Load("System.EnterpriseServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")            
# $publish = New-Object System.EnterpriseServices.Internal.Publish            
# $publish.GacInstall("C:\DOTNET\Services\WebServices\Cpchs.Modules.ActivitiesManagement.WebServices\bin\Cpchs.Modules.ActivitiesManagement.BusinessEntities.dll")
# $publish.GacInstall("C:\DOTNET\Services\WebServices\Cpchs.Modules.ActivitiesManagement.WebServices\bin\Cpchs.Modules.ActivitiesManagement.BusinessRules.dll")
# $publish.GacInstall("C:\DOTNET\Services\WebServices\Cpchs.Modules.ActivitiesManagement.WebServices\bin\Cpchs.Modules.ActivitiesManagement.WebServices.dll")

# If installing into the GAC on a server hosting web applications in IIS, you need to restart IIS for the applications to pick up the change.

# iisreset

# $sm = Get-IISServerManager
# $sm.ApplicationPools["CI"].Recycle()

# Start-WebAppPool -Name "CI"
# Start-IISSite -Name "CI"

# . $firefox "http://localhost:6002/GPLATFORM/Cpchs.Modules.ActivitiesManagement.WS/Transference/WebService/TransferenceManagement.svc"