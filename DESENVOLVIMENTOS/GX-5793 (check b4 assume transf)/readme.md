# GX-6723

## Jira

**Epic** [GX-5793](https://glinttdev.atlassian.net/browse/GX-5793)

**User Story Dev** [GX-6944](https://glinttdev.atlassian.net/browse/GX-6944)

## Observações do Agendamento

**Cliente** xxx

**Patch** - GCX2001

**TDM** - Ricardo Pinheiro

**Produto** - Heloísa Sobral

**Frontend** - Filipe Martins

**Qualidade** - 9 a 16 de Dezembro

### Descrição

Lista Urgência - forçar refresh da lista quando um doente é transferido.

### Mensagem Check-In

GX-5793 (epic)
GX-7249 (6944)
Validar que doente não foi assumido no momento da atribuição do médico

### Changesets

|                |   DEV    |    CI    |   17R1   |  19R12   |  20R01   |
|---------------:|:--------:|:--------:|:--------:|:--------:|:--------:|
| **Changesets** |  215047  |  215050  |    --    |    --    |    --    |
|                |  215967  |  215968  |    --    |    --    |    --    |
|                |  215984  |  215985  |  218139  |  218140  |  218142  |
|                |    --    |    --    |    --    |    --    |    --    |

### Notas

``` sql
select * from ur_transferencias
where episodio = '966428'
order by dt_cri desc;
select * from urg_dados_adic where episodio = '966428' order by dt_cri desc;
select * from urv_urgencies_priv where urgencyepisodeid = '966428';

select * from sd_episodio where episodio = '966710';
```

### Backend Configs

**Chave ginf_config** "S_CLINICAL_EPR_TRANSFERENCE" (TransferenceManagement.cs)
    **value** ```"#BASE_URL#Cpchs.Modules.ActivitiesManagement.WS/Transference/WebService/TransferenceManagement.svc"```

**Projecto Backend** Cpchs.Modules.ActivitiesManagement.WebServices
    **IIS** Services/Glintths.WebServices
    **Lógica** Services/Glintths.BusinessRules
    **Data Model** Services/Glintths.BusinessEntities

**Configurações** N/A

### Backend Lógica

+ BusinessRules/Cpchs.Modules.ActivitiesManagement.BusinessRules
  + Transference/BusinessRules
    + TransferenceManagementBr.cs

        ``` c#
        else
        {
            if (transferPermission)
            {
                //Valida se no tempo em que o ecrã esteve aberto sem fazer refresh este doente não foi assumido por outro médico
                if (string.IsNullOrWhiteSpace(urgencyEpisode.UrgencyEpisodeDoctorNmecan))
                {
                    messageControlList.Add(CreateTransferenceQuestionMessageControl("EPR", "TRANSFERENCE_ASSUME_OR_TRANSFER", null, ActivitiesManagementBRResources.strAssumeOrTransfereQuestion, false, true, true, false, false, true, null, urgencyEpisode.UrgencyEpisode.EpisodePatient));
                    return false;
                }
                else
                {
                    string content = $"{ActivitiesManagementBRResources.strAlreadyAssumedQuestion_1}{EntitiesManagementBER.Instance.GetCollaboratorById(companyDb, urgencyEpisode.UrgencyEpisodeDoctorNmecan)}.<br/>{ActivitiesManagementBRResources.strAlreadyAssumedQuestion_2} </span>";
                    messageControlList.Add(CreateTransferenceQuestionMessageControl("EPR", "ALREADY_ASSUMED", null, content, false, true, true, false, false, true, null, urgencyEpisode.UrgencyEpisode.EpisodePatient));
                    return false;
                }

            }
            else
            {
                /// Verifica se o recurso executante é o mesmo do recurso de transferência
                if (transference.ActionCollaboratorNmecan.Equals(transference.SpecialtyCollaboratorNMecan))
                {
                    if (string.IsNullOrWhiteSpace(urgencyEpisode.UrgencyEpisodeDoctorNmecan))
                    {
                        return true;
                    }
                    else
                    {
                        string content = $"{ActivitiesManagementBRResources.strAlreadyAssumedQuestion_1}{EntitiesManagementBER.Instance.GetCollaboratorById(companyDb, urgencyEpisode.UrgencyEpisodeDoctorNmecan)}.<br/>{ActivitiesManagementBRResources.strAlreadyAssumedQuestion_3} </span>";
                        messageControlList.Add(CreateTransferenceQuestionMessageControl("EPR", "ALREADY_ASSUMED", null, content, false, true, true, false, false, true, null, urgencyEpisode.UrgencyEpisode.EpisodePatient));
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
        }
        ```

  + ActivitiesManagementBRResources.Designer.cs

    ``` xml
    <data name="strAlreadyAssumedQuestion_1" xml:space="preserve">
        <value>O doente em questão já foi assumido pelo médico </value>
    </data>
    <data name="strAlreadyAssumedQuestion_2" xml:space="preserve">
        <value>Pretende assumir o doente, ou proceder com a transferência?</value>
    </data>
    ```

  + ActivitiesManagementBRResources.resx

### Notas Frontend

N/A
