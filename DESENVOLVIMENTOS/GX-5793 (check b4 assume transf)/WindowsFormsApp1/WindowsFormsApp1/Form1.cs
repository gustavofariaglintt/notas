﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Service.TransferenceManagementClient client = new Service.TransferenceManagementClient();

            Service.OperationContext op = new Service.OperationContext();
            op.CompanyName = "CPCHS_LOCAL_HSDEV";
            op.UserName = "MED";//132

            Service.Transference t1 = new Service.Transference();
            t1.ActionCollaboratorNmecan = op.UserName;
            t1.EpisodeId = "966428";
            t1.EpisodeType = "Consultas";
            t1.PatientId = "2151";
            t1.PatientType = "HS";
            t1.SpecialtyCollaboratorNMecan = op.UserName;
            t1.Transport = false;

            Service.TransferenceList tl = new Service.TransferenceList();
            tl.Items = new Service.Transference[1];
            tl.Items[0] = t1;

            Service.MessageControlList ml = new Service.MessageControlList();

            bool result = client.SetTransference(op, tl, ref ml);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            Service.TransferenceManagementClient client = new Service.TransferenceManagementClient();

            Service.OperationContext op = new Service.OperationContext();
            op.CompanyName = "DEMOPRIV";
            op.UserName = "MED";//132

            Service.Transference t1 = new Service.Transference();
            t1.ActionCollaboratorNmecan = op.UserName;
            t1.EpisodeId = "44642";
            t1.EpisodeType = "Consultas";
            t1.PatientId = "445702";
            t1.PatientType = "HS";
            t1.SpecialtyCollaboratorNMecan = op.UserName;
            t1.Transport = false;

            Service.TransferenceList tl = new Service.TransferenceList();
            tl.Items = new Service.Transference[1];
            tl.Items[0] = t1;

            Service.MessageControlList ml = new Service.MessageControlList();

            bool result = client.SetTransference(op, tl, ref ml);
        }
    }
}
