. '.\buildVS.ps1'

buildvs C:\DOTNET\Services\BusinessEntities\Cpchs.Modules.ActivitiesManagement.BusinessEntities\Cpchs.Modules.ActivitiesManagement.BusinessEntities.csproj -solution C:\DOTNET\Services\BusinessEntities
buildvs C:\DOTNET\Services\BusinessRules\Cpchs.Modules.ActivitiesManagement.BusinessRules\Cpchs.Modules.ActivitiesManagement.BusinessRules.csproj -solution C:\DOTNET\Services\BusinessRules
buildvs C:\DOTNET\Services\WebServices\Cpchs.Modules.ActivitiesManagement.WebServices\Cpchs.Modules.ActivitiesManagement.WebServices.csproj -solution C:\DOTNET\Services\WebServices