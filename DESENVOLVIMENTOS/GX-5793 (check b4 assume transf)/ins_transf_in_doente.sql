insert into ur_transferencias
(T_DOENTE, DOENTE, T_EPISODIO, EPISODIO, DATA_PEDIDO, DATA_FIM, COD_SERV, COD_SALA, TRANSPORTE, N_MECAN_RESP, ESTADO, USER_CRI, N_MECAN_CRI, ID_ESPECIALIDADE, TIPO, COD_SERV_ANT, COD_SALA_ANT, ID_ESPECIALIDADE_ANT, N_MECAN)
values
('HS', '2151', 'Consultas', '966428', sysdate, sysdate, 'UG', '*', 'N', '5000985', 'FIM', 'MED', '5000985', '1', 'MED', 'UG', '*', '1', '5000985');

update urg_dados_adic
set medico_resp = '5000985'
where episodio = '966428';

update urv_urgencies_priv
set urgencyepisodedoctornmecan = '5000985'
where urgencyepisodeid = '966428';