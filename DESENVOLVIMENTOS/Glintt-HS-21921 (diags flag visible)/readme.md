# Glintt-HS-21921 (e Glintt-HS-21925)

## Jira

**Epic** [GX-2115](https://glinttdev.atlassian.net/browse/GX-2115)

**User Story Dev** [GX-3994](https://glinttdev.atlassian.net/browse/GX-3994)

## Observações do Agendamento

**Cliente** HSEIT/HPA

**Patch** - 2019/12

**TDM** - TBD

**Produto** - Heloisa Sobral

**Frontend** - DONE

**Qualidade** - DONE

### Descrição

Na lista principal do ecrã de Diagnósticos e Problemas, devo poder marcar como principal registos dos tipos diagnósticos/problemas/alertas/procedimentos.

### Mensagem Check-In

Glintt-HS-21921
GX-5356
GX-2115
Diagnosticos e problemas: Nova flag CidLightMainVisible

### Changesets

#### Dev

210245
210269

#### CI

210247
210270

#### 17R1

211649

### Merge

#### DEV -> CI

DONE

#### CI -> 17R1

DONE

### Notas

**Projecto Frontend** Clinical Discharge ? Diagnostics ?

**Projecto Backend** Cpchs.Modules.ClinicalProcess.WS

    **IIS** Services/Glintths.WebServices

    **Lógica** Services/Glintths.BusinessRules

    **Data Model** Services/Glintths.BusinessEntities

#### Lógica a Mudar
