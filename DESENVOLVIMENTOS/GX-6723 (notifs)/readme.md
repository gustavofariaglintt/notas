# GX-6723

## Jira

**Epic** [GX-6723](https://glinttdev.atlassian.net/browse/GX-6723)

**User Story Dev** [GX-7249](https://glinttdev.atlassian.net/browse/GX-7249)

## Observações do Agendamento

**Cliente** xxx

**Patch** - GCX2001

**TDM** - Fernando Oliveira

**Produto** - Heloísa Sobral

**Frontend** - Sara Araujo

**Qualidade** - 9 a 16 de Dezembro

### Descrição

Deve ser possível aceder diretamente ao pedido de parecer clínico a partir da janela das notificações.

### Mensagem Check-In

GX-6723 (epic)
GX-7249 (issue)
TODO

### Changesets

|                |   DEV    |    CI    |   17R1   |
|---------------:|:--------:|:--------:|:--------:|
| **Changesets** |  xxxxxx  |  xxxxxx  |  xxxxxx  |

### Notas

DOTNET_DEV/GC_DOTNET_CODE -> PCK_CLINICAL_OPINION -> SendClinicalOpinionNotif -> CTRL+F: "requestId"
