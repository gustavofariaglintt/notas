BEGIN
    ga_install_objects.execute_immediate(q'{
        DECLARE
            w_dominio   pc_param_geral.dominio%TYPE;

        BEGIN
            w_dominio   := 'EPRMOBILE_URGENCY_DISCHARGE_FILTER';

            DELETE FROM pc_param_geral
            WHERE dominio = w_dominio;

            INSERT INTO pc_param_geral
            (dominio, codigo, descr, flg_activo, ord)
            VALUES
            (w_dominio, 'ALL', 'Todos', 'S', 1);
            
            INSERT INTO pc_param_geral
            (dominio, codigo, descr, flg_activo, ord)
            VALUES
            (w_dominio, 'ADM', 'Apenas Alta Administrativa', 'S', 2);
            
            INSERT INTO pc_param_geral
            (dominio, codigo, descr, flg_activo, ord)
            VALUES
            (w_dominio, 'CLI', 'Apenas Alta Clínica', 'S', 3);
            
            INSERT INTO pc_param_geral
            (dominio, codigo, descr, flg_activo, ord)
            VALUES
            (w_dominio, 'ADMCLI', 'Alta Clínica e Administrativa', 'S', 4);
            
            INSERT INTO pc_param_geral
            (dominio, codigo, descr, flg_activo, ord)
            VALUES
            (w_dominio, 'NONE', 'Sem Alta', 'S', 5);

            COMMIT;

        EXCEPTION
            WHEN OTHERS THEN
                ROLLBACK;
                RAISE;

        END;
    }');
END;
/