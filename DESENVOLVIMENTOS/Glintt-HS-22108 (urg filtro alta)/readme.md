# Glintt-HS-22108

## Jira

**Epic** [GX-5250](https://glinttdev.atlassian.net/browse/GX-5250)

**User Story Dev** [GX-5491](https://glinttdev.atlassian.net/browse/GX-5491)

## Observações do Agendamento

**Cliente** HPA ?

**Patch** - 2019/12

**TDM** - Ricardo Pinheiro

**Produto** - Heloisa Sobral

**Frontend** - 14 a 15 de Novembro - Diogo Rocha

**Qualidade** - 18 Novembro

(possivelmente é necessário hot-feature)

### Descrição

Melhorias no filtro "Alta" na lista de doentes da Urgência.

### Mensagem Check-In

Glintt-HS-22108
GX-5250 (epic)
GX-5491 (issue)
Novo filtro: 'Alta Clínica e Administrativa',
'Alta Clínica' -> 'Apenas Alta Clínica'
'Alta Administrativa' -> 'Apenas Alta Administrativa'

### Changesets

#### Dev

211667

#### CI

211668

#### 17R1

213037

### Merge

#### DEV -> CI

DONE

#### CI -> 17R1

DONE

### Patch

DONE

### Notas

Ao fazer load inicial da página: <http://localhost:6002/EPR_MOBILE/Glintths.Shell/ClinicalDesktop_Urgency/GetConfigurations>

**Projecto Frontend** Clinical Desktop (Urgency)

### Backend Configs

**Chamada** ```GetGenParamByAllFiltersOneAux(bi, clinicalManagement, "EPRMOBILE_URGENCY_DISCHARGE_FILTER", scope);```

**Chave ginf_config** "S_CLINICAL_EPR_CODIFICATIONS" (ClinicalManagement.cs)
    **value** "#BASE_URL#Cpchs.Modules.Codifications.WebServices/Clinical/WebService/ClinicalManagement.svc"

**Projecto Backend** Cpchs.Modules.Codifications.WebServices
    **IIS** Services/Glintths.WebServices
    **Lógica** Services/Glintths.BusinessRules
    **Data Model** Services/Glintths.BusinessEntities

**Configurações** ```PCK_CODIFICATIONS_CLINICAL.GetGenParamByAllFiltersOneAux```

### Backend Lógica

**Chave ginf_config** "S_CLINICAL_EPR_CODIFICATIONS" (ClinicalManagement.cs)
    **value** "#BASE_URL#Cpchs.Modules.Codifications.WebServices/Clinical/WebService/ClinicalManagement.svc"

**Projecto Backend** GlinttHS.Services.Urgency

Alterações às configs feitas no ficheiro insert_config.sql

#### Lógica a Mudar
