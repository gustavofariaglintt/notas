# GX-xxxx

## Jira

**Epic** [GX-xxxx](https://glinttdev.atlassian.net/browse/GX-xxxx)

**User Story Dev** [GX-xxxx](https://glinttdev.atlassian.net/browse/GX-xxxx)

## Observações do Agendamento

**Cliente** xxx

**Patch** - GCX20XX

**TDM** - TODO

**Produto** - TODO

**Frontend** - TODO

### Descrição

TODO (copiar do JIRA)

### Mensagem Check-In

GX-xxxx
TODO

### Changesets

|       TFS      |   DEV    |    CI    |   17R1   |
|---------------:|:--------:|:--------:|:--------:|
| **Changesets** |  xxxxxx  |  xxxxxx  |  xxxxxx  |
|                |    --    |    --    |    --    |

|       SVN      |  HSDEV  |  DEMOQ  | DEMOPRIV |
|---------------:|:-------:|:-------:|:--------:|
|  **Revision**  |  xxxxx  |  xxxxx  |  xxxxxx  |
|                |   ---   |    --   |    --    |

### Notas

TODO

### Backend Configs

#### Controller Frontend URL

<http://demoapliis.glinttocm.com:6002/EPR_MOBILE/Glintths.Shell/xxxx/yyyy.svc>

#### Chave GINF_CONFIG

| key | value |
|:---:|:-----:|
|"S_GINF_CONFIG_KEY"|```"#BASE_URL#pathtofile.svc"```|

#### Projecto Backend

##### Cpchs.Modules.ActivitiesManagement.WebServices

+ [x] **IIS** Services/Glintths.WebServices
+ [x] **Lógica** Services/Glintths.BusinessRules
+ [x] **Data Model** Services/Glintths.BusinessEntities

#### Configurações

N/A

### Backend Lógica

#### C\#

![Changeset](changeset.png "Changeset")

#### DB

+ SCHEMA
  + PACKAGE
    + procedure

### Notas Frontend

N/A
