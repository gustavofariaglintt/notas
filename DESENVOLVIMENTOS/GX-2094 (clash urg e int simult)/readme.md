# GX-2094

## Jira

**Epic** [GX-2094](https://glinttdev.atlassian.net/browse/GX-2094)

**User Story Dev** [GX-xxxx](https://glinttdev.atlassian.net/browse/GX-xxxx)

## Observações do Agendamento

**Cliente** xxx

**Patch** - GCX2001

**TDM** - Fernando Oliveira

**Produto** - Heloísa Sobral

**Frontend** - Filipe Martins

### Descrição

Desktop consulta - Não permitir a existência de urgências ou internamentos em simultâneo com consultas

### Mensagem Check-In

GX-2094 (epic)
GX-7782 (sub-task)
Mensagem de aviso no momento de dar início a consulta externa.

### Changesets

|       TFS      |   DEV    |    CI    |   17R1   |
|---------------:|:--------:|:--------:|:--------:|
| **Changesets** |   DONE   |   DONE   |   DONE   |
|                |    --    |    --    |    --    |

|       SVN      |  HSDEV  |  DEMOQ  | DEMOPRIV |
|---------------:|:-------:|:-------:|:--------:|
|  **Revision**  |  27726  |  CHECK  |  xxxxxx  |
|                |   ---   |    --   |    --    |

### Notas

TODO

### Backend Configs

#### Controller Frontend URL

<http://demoapliis.glinttocm.com:6002/EPR_MOBILE/Glintths.Shell/ClinicalDesktop_DoctorAppointment/StartDoctorAppointment>

#### Chave GINF_CONFIG

| key | value |
|:---:|:-----:|
|"S_CLINICAL_EPR_DOCTOR_APP"|```"#BASE_URL#Cpchs.Modules.ActivitiesManagement.WS/DoctorAppointment/WebService/DoctorAppointmentManagement.svc"```|

#### Projecto Backend

##### Cpchs.Modules.ActivitiesManagement.WebServices

+ [x] **IIS** Services/Glintths.WebServices
+ [x] **Lógica** Services/Glintths.BusinessRules
+ [x] **Data Model** Services/Glintths.BusinessEntities

#### Configurações

``` sql
select * from gr_param_base where chave = 'GHPC_NET_MOBILE_START_END_DOC_APP';
select * from gr_param_base where chave = 'GHPC_DURACAO_CONSULTA';--voltar a pôr 5
select * from gr_param_base where chave = 'PERMITE_IN_UR_SIMULTANEO';--deixar com 'A' ou 'N'
select * from pc_param_geral where dominio = 'GHPC_NET_DOUBLECLICK_ADMITPATIENT';
```

### Backend Lógica

#### C\#

N/A

#### DB

+ DOTNET_DEV
  + PCK_MOBILE_DOC_APP
    + ValidateStartDoctorAppointment

    ``` sql
    w_hr_pr_cons             sd_cons_marc.hr_pr_cons%TYPE;
    ```

    ``` sql
    SELECT dt_cons,  medico, hr_inicio_real, flag_estado, t_doente, hr_pr_cons--, data_ok
    INTO v_dt_cons,  v_medico,  v_hr_ini,  v_flag_estado, v_t_doente, w_hr_pr_cons--, w_data_ok
    FROM sd_cons_marc
    WHERE n_cons = p_n_cons;
    ```

    ``` sql
    IF w_hr_pr_cons IS NULL THEN
        w_func_res := valida_cons_int_urg_func(p_n_cons, sysdate, sysdate, null, null, w_o_msg, w_o_raise);
        o_start_msg := w_func_res;

        IF w_func_res IS NOT NULL AND w_o_raise = TRUE THEN
            o_esta_em_condicoes_de_iniciar := 'N';
            RETURN;
        ELSIF w_func_res IS NOT NULL AND w_o_raise = FALSE then
            o_esta_em_condicoes_de_iniciar := 'S';
        END IF;

        IF w_func_res IS NOT NULL THEN
            w_func_res := w_func_res || '<br/>';
        END IF;

     END IF;
    ```

    ``` sql
    o_start_msg := w_func_res || v_start_message;
    ```

    ``` sql
    ELSE
        o_start_msg := v_start_message;
        RETURN;
    ```

+ GH
  + PCK_CONSULTA
    + valida_cons_int_urg: sem alterações

### Notas Frontend

N/A
