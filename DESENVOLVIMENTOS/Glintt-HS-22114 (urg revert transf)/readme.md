# Glintt-HS-22114

## Jira

**Epic** [GX-5356](https://glinttdev.atlassian.net/browse/GX-5356)

**User Story Dev** [GX-5535](https://glinttdev.atlassian.net/browse/GX-5535)

## Observações do Agendamento

**Cliente** HSEIT

**Patch** - 2019/12

**TDM** - Ricardo Pinheiro

**Produto** - Heloisa Sobral

**Frontend** - 12 Novembro - Filipe Martins

**Qualidade** - 13 Novembro

(possivelmente é necessário hot-feature)

### Descrição

Na lista de doentes da Urgência, ao assumir/transferir um doente sem médico, nos 15 minutos seguintes devo poder reverter a transferência que realizei.

### Mensagem Check-In

Glintt-HS-22114
GX-5356
GX-5535
Possibilidade de reverter doente para ninguém

### Changesets

#### Dev

211523
211526
211699
211737
211851
211960

#### CI

211528
211717
211738
211862
211961

#### 17R1

212315

### Merge

#### DEV -> CI

DONE

#### CI -> 17R1

DONE

### Notas

Ao clicar na estrela: <http://demoapliis.glinttocm.com:9002/EPR_MOBILE/Glintths.Shell/ClinicalDesktop_Transference/SetTransference>

**Projecto Frontend** Clinical Desktop

**Chave ginf_config** "S_CLINICAL_EPR_TRANSFERENCE"
    **value** "#BASE_URL#Cpchs.Modules.ActivitiesManagement.WS/Transference/WebService/TransferenceManagement.svc"

**Projecto Backend** Cpchs.Modules.ActivitiesManagement.WS
    **IIS** Services/Glintths.WebServices
    **Lógica** Services/Glintths.BusinessRules
    **Data Model** Services/Glintths.BusinessEntities

#### Lógica a Mudar

Cpchs.Modules.ActivitiesManagement.BusinessRules.ValidateTransferenceUrgency ?
Cpchs.Modules.ActivitiesManagement.BusinessRules.ValidateReverseTransference ? acho que é aqui
Cpchs.Modules.ActivitiesManagement.BusinessRules.ActionTransferenceInpatientCommit (db)
Cpchs.Modules.ActivitiesManagement.BusinessEntities.UpdateResourceResponsable <--
UpdateResourceResponsableDB

```messageControlList.Add(CreateTransferenceControlMessageControl("EPR", "MY_KEY", "MY_KEY", true, false, true, false, function, inpatientEpisode.EpisodePatient));```

Já há um popup para reverter. Mas vai ter de ser outro pop-up só para este caso?

O que já existe:
    Abort: false
    AdditionalInfo: ""
    CompanyDB: ""
    ConvertProperty: ""
    DtAct: null
    DtCri: null
    Executed: false
    IsDirty: true
    Message: "Transferiu recentemente a responsabilidade dos(s) doentes(s)selecionados(s), o que pretende fazer?"
    MessageControlType: QUESTION
    MessageKey: "TRANSFERENCE_REVERSE_OR_TRANSFER"
    MessageOptions:
    MessageType: null
    Obj: &lt;Patient&gt;
    ObjectState: Added
    ObjectStateBackup: Unchanged
    Response: null
    Scope: "EPR"
    UserAct: ""
    UserCri: ""

Novo:
    Message: "Pretende transferir ou reverter a transferência anterior do(s) doente(s)?"
    Aviso: "Se reverter a transferência, o doente ficará sem médico responsável."

----------------------------------------------

messageControlList.Add(CreateTransferenceQuestionMessageControl("EPR", "TRANSFERENCE_REVERSE_OR_TRANSFER", null, ActivitiesManagementBRResources.strTransfereOrReverseQuestion, true, false, true, false, false, true, null, inpatientEpisode.EpisodePatient));

----------------------------------------------

``` sql
select * from ur_transferencias where episodio = '42162' order by dt_cri desc;
select * from urg_dados_adic where episodio = '42162' order by dt_cri desc;
select * from urv_urgencies_priv where urgencyepisodeid = '42162';

select * from log_epr_2 where txt like 'TRANSFURG%' order by data desc;
select * from log_epr_2 where txt like 'TRANSFURG: insert_tr_action_details_pub%' order by data desc;

select episodio from sd_episodio
where (
    select count(1) from ur_transferencias
    where sd_episodio.episodio = ur_transferencias.episodio
    ) > 0
and (
    select count(1) from urg_dados_adic
    where sd_episodio.episodio = urg_dados_adic.episodio
    ) > 0
and (
    select count(1) from urv_urgencies_priv
    where sd_episodio.episodio = urv_urgencies_priv.urgencyepisodeid
    ) > 0;
```
