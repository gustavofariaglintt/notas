DECLARE
    w_transfusionid   VARCHAR2 (38);
BEGIN
    w_transfusionid := 'E4551E7F-84DE-6008-E053-1501010A3DD1';

    DELETE FROM gcli_signvitals
          WHERE externalid IN (SELECT transfusionunitvitalsignid
                                 FROM glab_transfusionunitvitalsigns
                                WHERE transfusionid = w_transfusionid);

    DELETE FROM gcli_adversereaction
          WHERE externalid IN (SELECT transfusionunitid
                                 FROM glab_transfusionunit
                                WHERE transfusionid = w_transfusionid);

    UPDATE glab_transfusionunit
       SET administrationenddate = NULL,
           administrationstartdate = NULL,
           status = 'Pendent',
           administrationstartperformedby = NULL,
           administrationendperformedby = NULL,
           hadadversereaction = 'N'
     WHERE transfusionid = w_transfusionid;

    UPDATE glab_transfusion
       SET status = 'Pendent', comments = NULL
     WHERE transfusionid = w_transfusionid;

    COMMIT;
EXCEPTION
    WHEN OTHERS
    THEN
        ROLLBACK;
        RAISE;
END;