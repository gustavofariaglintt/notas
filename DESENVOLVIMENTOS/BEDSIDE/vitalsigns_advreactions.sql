SELECT *
  FROM gcli_signvitals
 WHERE externalid IN
           (SELECT transfusionunitvitalsignid
              FROM glab_transfusionunitvitalsigns
             WHERE transfusionid = 'E4551E7F-84DE-6008-E053-1501010A3DD1');

SELECT *
  FROM gcli_adversereaction
 WHERE externalid IN
           (SELECT transfusionunitid
              FROM glab_transfusionunit
             WHERE transfusionid = 'E4551E7F-84DE-6008-E053-1501010A3DD1');