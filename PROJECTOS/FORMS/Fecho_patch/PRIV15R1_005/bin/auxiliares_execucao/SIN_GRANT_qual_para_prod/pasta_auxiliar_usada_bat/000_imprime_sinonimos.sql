set define off
set serveroutput on

spool .\auxiliares_execucao\SIN_GRANT_qual_para_prod\pasta_auxiliar_usada_bat\000_imprime_sinonimos.log

exec dbms_output.enable(null);

exec pk_util.impr_grants_que_user_da ;
exec pk_util.impr_grants_que_user_recebe ;
exec pk_util.impr_sinonimos_privados ;


spool off

exit 
