set echo on
begin
    FOR c1 in(select sid,"SERIAL#" SERIAL from v$session where username = 'BTEC')
    LOOP            
        begin
            --dbms_output.put_line('ALTER SYSTEM DISCONNECT SESSION ''' || c1.sid || ',' || c1.SERIAL ||''' IMMEDIATE; ');            
            --dbms_output.put_line('ALTER SYSTEM DISCONNECT SESSION '':1,:2'' IMMEDIATE');
            --execute immediate 'ALTER SYSTEM DISCONNECT SESSION '':1,:2'' IMMEDIATE' using c1.sid,c1.SERIAL;
            execute immediate 'ALTER SYSTEM DISCONNECT SESSION '''|| c1.sid ||','|| c1.SERIAL ||''' IMMEDIATE';
        end;
    END LOOP;
end;
/
exit;