'---------------------------------------------------------------------------
If Not WScript.Arguments.Named.Exists("elevate") Then
  CreateObject("Shell.Application").ShellExecute WScript.FullName , WScript.ScriptFullName & " /elevate", "", "runas", 1
  WScript.Quit
End If
'---------------------------------------------------------------------------
Dim strPath
Dim DB_NAME
Dim PWD_SYS
Dim IMP_PATH
Dim answer
Dim output
Dim ORACLE_HOME
'DROP BTEC
Dim WshShell_DROP
Dim oExec_DROP
Dim input_DROP
'CREATE
Dim WshShell_CREATE
Dim oExec_CREATE
Dim input_CREATE
'IMPORT
Dim WshShell_IMPORT
Dim oExec_IMPORT
Dim input_IMPORT
'INSTALL
Dim WshShell_INSTALL
Dim oExec_INSTALL
Dim input_INSTALL
'INSTALL
Dim WshShell_COMPILE
Dim oExec_COMPILE
Dim input_COMPILE
'-----------------------------------------------------------------
Set objShell = CreateObject("Wscript.Shell")
strPath = Wscript.ScriptFullName
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.GetFile(strPath)
strFolder = objFSO.GetParentFolderName(objFile)
strPath = strFolder
'wscript.echo strPath 
'-----------------------------------------------------------------
'-----------------------------------------------------------------
'DB_NAME = InputBox("Insira a base de dados:","Base de Dados", "itdemo_rede")
'PWD_SYS = InputBox("Insira a password do SYS:","PWD SYS", "sysdemo")
'IMP_PATH = InputBox("Insira o caminho do IMP.exe (sem \ no final):","Caminho do IMP", "D:\oracle\product\11.2.0\client_1\BIN")
'ORACLE_HOME = InputBox("Insira vari�vel ORACLE_HOME:","ORACLE_HOME", "D:\oracle\product\11.2.0\client_1")
'-----------------------------------------------------------------
'-----------------------------------------------------------------
DB_NAME = InputBox("Insira a base de dados:","Base de Dados", "")
PWD_SYS = InputBox("Insira a password do SYS:","PWD SYS", "")
IMP_PATH = InputBox("Insira o caminho do IMP.exe (sem \ no final):","Caminho do IMP", "")
'ORACLE_HOME = InputBox("Insira vari�vel ORACLE_HOME:","ORACLE_HOME", "")
'-----------------------------------------------------------------
answer = vbno 
answer = msgbox("Tem a certeza que pretende instalar o BTEC?" & vbCrlf & " <> DB_NAME = " & DB_NAME & vbCrlf & " <> PWD_SYS= " & PWD_SYS & vbCrlf &  " <> IMP_PATH= " & IMP_PATH  ,vbyesno, "Instalar BTEC?") '& vbCrlf &  " <> ORACLE_HOME= " & ORACLE_HOME
if answer <> vbYes then 
	wscript.quit 
end if
'-----------------------------------------------------------------
'FAZ DROP BTEC
set WshShell_DROP = CreateObject("WScript.Shell")
set oEnv=WshShell_DROP.Environment("Process") 
cmdString = IMP_PATH & "\" & "sqlplus.exe ""sys/" & PWD_SYS & "@" & DB_NAME & " as sysdba"" @" & "" & strPath & "\DROP_BTEC.sql" & ""
'result = InputBox("","", cmdString)
Set oExec_DROP = WshShell_DROP.Exec(cmdString) 
'WScript.Echo "Status" & oExec_DROP.Status
Do While oExec_DROP.Status = 0 
    WScript.Sleep 2 
Loop 
input_DROP = "" 
Do While Not oExec_DROP.StdOut.AtEndOfStream 
	input_DROP = input_DROP & oExec_DROP.StdOut.Read(1) 
Loop 
wscript.echo "RESULT DROP: " & input_DROP 
'END DROP BTEC
'-----------------------------------------------------------------
'-----------------------------------------------------------------
'FAZ CREATE BTEC
set WshShell_CREATE = CreateObject("WScript.Shell")
set oEnv=WshShell_CREATE.Environment("Process") 
cmdString = IMP_PATH & "\" & "sqlplus.exe ""sys/" & PWD_SYS & "@" & DB_NAME & " as sysdba"" @" & "" & strPath & "\create_btec.sql" & ""
result = WshShell_CREATE.Run(cmdString,1, true) 
'result = InputBox("","", cmdString)
'WScript.Echo "Status" & oExec_CREATE.Status
'Do While oExec_CREATE.Status = 0 
'    WScript.Sleep 2 
'Loop 
input_CREATE = "" 
''Do While Not oExec_CREATE.StdOut.AtEndOfStream 
''	input_CREATE = input_CREATE & oExec_CREATE.StdOut.Read(1) 
'Loop 
wscript.echo "RESULT CREATE: FIM CREATE " '& input_CREATE 
'END CREATE BTEC
'-----------------------------------------------------------------
'-----------------------------------------------------------------
'FAZ IMPORT BTEC
'---> SET ORACLE_HOME
'Set wshShell = CreateObject( "WScript.Shell" )
'Set wshSystemEnv = wshShell.Environment("Process")
'wshSystemEnv("ORACLE_HOME") = ORACLE_HOME
'---> FAZ IMPORT BTEC
set WshShell_IMPORT = CreateObject("WScript.Shell")
set oEnv=WshShell_IMPORT.Environment("Process") 
cmdString = IMP_PATH & "\imp.exe BTEC/BTEC@" & DB_NAME & " file=" & strPath & "\exportbtec.dmp log=" & strPath & "\importbtec.log statistics=none buffer=10000000"
'result = InputBox("","", cmdString)
result = WshShell_IMPORT.Run(cmdString,1, true) 
'WScript.Echo result
'Set oExec_IMPORT = WshShell_IMPORT.Exec(cmdString) 
'WScript.Echo "Status" & oExec_IMPORT.Status
'Do While oExec_IMPORT.Status = 0 
'    WScript.Sleep 2 
'Loop 
'input_IMPORT = "" 
'Do While Not oExec_IMPORT.StdOut.AtEndOfStream 
'	input_IMPORT = input_IMPORT & oExec_IMPORT.StdOut.Read(1) 
'Loop 
wscript.echo "RESULT IMPORT: FIM" '& input_IMPORT 
'END IMPORT BTEC
'-----------------------------------------------------------------
'-----------------------------------------------------------------
'FAZ INSTALL BTEC
set WshShell_INSTALL = CreateObject("WScript.Shell")
set oEnv=WshShell_INSTALL.Environment("Process") 
cmdString = IMP_PATH & "\" & "sqlplus.exe ""sys/" & PWD_SYS & "@" & DB_NAME & " as sysdba"" @" & "" & strPath & "\instala_btec.sql" & ""
Set oExec_INSTALL = WshShell_INSTALL.Exec(cmdString) 
'WScript.Echo "Status" & oExec_INSTALL.Status
Do While oExec_INSTALL.Status = 0 
    WScript.Sleep 2 
Loop 
input_INSTALL = "" 
Do While Not oExec_INSTALL.StdOut.AtEndOfStream 
	input_INSTALL = input_INSTALL & oExec_INSTALL.StdOut.Read(1) 
Loop 
wscript.echo "RESULT INSTALL: " & input_INSTALL 
'END INSTALL BTEC
'-----------------------------------------------------------------
'-----------------------------------------------------------------
'FAZ COMPILE BTEC
set WshShell_COMPILE = CreateObject("WScript.Shell")
set oEnv=WshShell_COMPILE.Environment("Process") 
cmdString = IMP_PATH & "\" & "sqlplus.exe ""sys/" & PWD_SYS & "@" & DB_NAME & " as sysdba"" @" & "" & strPath & "\COMPILE_USER_BTEC.sql" & ""
Set oExec_COMPILE = WshShell_COMPILE.Exec(cmdString) 
'WScript.Echo "Status" & oExec_COMPILE.Status
Do While oExec_COMPILE.Status = 0 
    WScript.Sleep 2 
Loop 
input_COMPILE = "" 
Do While Not oExec_COMPILE.StdOut.AtEndOfStream 
	input_COMPILE = input_COMPILE & oExec_COMPILE.StdOut.Read(1) 
Loop 
wscript.echo "RESULT COMPILE: " & input_COMPILE 
'END COMPILE BTEC
'-----------------------------------------------------------------
wscript.echo "*******!!!!     INSTALL BTEC FIM     !!!!*******"