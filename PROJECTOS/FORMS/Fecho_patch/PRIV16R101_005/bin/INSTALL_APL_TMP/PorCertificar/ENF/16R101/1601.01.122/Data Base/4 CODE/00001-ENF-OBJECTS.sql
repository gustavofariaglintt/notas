

/*######################################
###### PCK_ENF_AVAL_INIT_spec.sql ######
######################################*/
  CREATE OR REPLACE PACKAGE PCK_ENF_AVAL_INIT IS
    TYPE r_DADOS_NOTA_ALTA IS RECORD
    (
      descricao varchar2(4000),
      valor varchar2(4000)
    );

    TYPE tab_DADOS_NOTA_ALTA IS TABLE OF r_DADOS_NOTA_ALTA;

    TYPE r_DADOS_BD     IS RECORD
    (
        tipo_linha      VARCHAR2(1),
       ordem            VARCHAR2(20),
       nivel            VARCHAR2(1),
       pai              VARCHAR2(30),
       cod              VARCHAR2(30),
       descricao        VARCHAR2(2000),
       valor            VARCHAR2(2000),
       valor_init       VARCHAR2(2000),
       n_ep_arv         NUMBER,
       tipo             VARCHAR2(20),
       tipo_dados       VARCHAR2(20),
       mascara          VARCHAR2(200),
       valor1           VARCHAR2(2000),
       valor2           VARCHAR2(200),
       ler              VARCHAR2(200),
       gravar           VARCHAR2(200),
       codigo           VARCHAR2(100),
       pode_alterar     VARCHAR2(1),
       usr              VARCHAR2(50),
       dt               VARCHAR2(50)
    );

    TYPE TB_DADOS_Bd    IS TABLE OF r_DADOS_BD
    INDEX BY BINARY_INTEGER;
    --TYPE TB_DADOS_BD    IS TABLE OF SDV_ENF_AVAL_INIT_DADOS%ROWTYPE
    --INDEX BY BINARY_INTEGER;

    TYPE R_LOG IS RECORD
    (
        ID          NUMBER,
        N_EP_ARV    NUMBER,
        COD_EP_ITEM_DET NUMBER,
        VALOR_INI   VARCHAR2(300),
        VALOR_FIM   VARCHAR2(300),
        USER_CRI    VARCHAR2(200),
        USER_ALT    VARCHAR2(200),
        DT          DATE,
        MOTIVO      VARCHAR2(4000),
        FLG_AMBITO  VARCHAR2(3)
    );

    TYPE t_log          IS TABLE OF R_LOG;

    arg_DADOS_BD    tb_dados_bd;
    arg_T_DOENTE    VARCHAR2(30);
    arg_DOENTE      VARCHAR2(30);
    arg_T_EPISODIO  VARCHAR2(30);
    arg_EPISODIO    VARCHAR2(30);
    arg_SERV        VARCHAR2(30);

    arg_ROW_SESSION VARCHAR(30);
    arg_ROW_ARGS    VARCHAR2(64);
    arg_N_EP_ARV    NUMBER;


    --| CONSTANTES
    display_DATE_FM     VARCHAR2(20) := 'DD-MM-YYYY HH24:MI';
    display_SEPARADOR   VARCHAR2(10) := ' | ';
    display_TAB         VARCHAR2(10) := '     ';

    FUNCTION get_COUNT_ROWS      RETURN NUMBER;

    FUNCTION get_arg_ROW_SESSION RETURN VARCHAR2;
    FUNCTION get_arg_ROW_ARGS    RETURN VARCHAR2;
    PROCEDURE set_arg_SESSION_ARGS(i_ROW_SESSION VARCHAR2,i_ROW_ARGS VARCHAR2);

    PROCEDURE apagar_resumo2 (i_row_session IN VARCHAR2); --CHTViseu-1713
    
    PROCEDURE get_RESUMO
    (
        o_ROW_SESSION   OUT VARCHAR2,
        o_ROW_ARGS      OUT VARCHAR2
    );

    FUNCTION get_TEM_HIST_ALTERACOES
    (
             i_N_EP_ARV           IN NUMBER,
             i_COD_EP_ITEM_DET    IN VARCHAR2
    )
    RETURN BOOLEAN;


/*  FUNCTION get_CHANGELOG_ITEM
    (
        i_COD_EP_ITEM_DET   IN  NUMBER,
        i_N_EP_ARV          IN  NUMBER
    )
    RETURN t_log;*/

    FUNCTION get_CHANGELOG
    RETURN T_LOG_aval_init;

    FUNCTION get_VALOR_FINAL_DET( i_N_EP_ARV NUMBER , i_COD_EP_ITEM_DET VARCHAR2 , i_VALOR VARCHAR2 )
    RETURN  VARCHAR2;

    PROCEDURE LIMPA_RESUMO;

    FUNCTION GET_VALOR_ITEM_DET( i_N_EP_ARV NUMBER , i_COD_EP_ITEM_DET VARCHAR2)
    RETURN VARCHAR2;

    PROCEDURE init (i_t_doente     IN VARCHAR2,
                    i_doente       IN VARCHAR2,
                    i_t_episodio   IN VARCHAR2,
                    i_episodio     IN VARCHAR2,
                    i_n_ep_arv     IN VARCHAR2,
                    i_COD_SERV     IN VARCHAR2);

    PROCEDURE log_dev (i_debug IN VARCHAR2 );

    FUNCTION GET_DADOS_DB( i_COD_ITEM IN VARCHAR2) RETURN tb_dados_bd;

    FUNCTION COUNT_ITEMS(i_COD_ITEM IN VARCHAR2)
    RETURN NUMBER;

    FUNCTION get_folha(i_n_ep_arv   NUMBER, i_cod_ep_item VARCHAR2)
    RETURN TB_DADOS_BD;

    PROCEDURE load_n_ep_arv (i_t_doente     IN      VARCHAR2,
                         i_doente       IN      VARCHAR2,
                         i_t_episodio   IN      VARCHAR2,
                         i_episodio     IN      VARCHAR2,
                         i_cod_ep_arv   IN      VARCHAR2,
                         i_n_ep_arv         OUT NUMBER,
                         i_n_ep_arv_h       OUT NUMBER,
                         i_flg_arv_h        OUT VARCHAR2
                         );

     FUNCTION obtem_formula ( i_COD_indic   IN  VARCHAR2)
     return varchar2;

     FUNCTION GET_ARG_SERV
     RETURN VARCHAR2;

     FUNCTION GET_RESUMO_NOTA_ALTA (i_t_doente in varchar2,
                                   i_doente in varchar2,
                                   i_t_episodio in varchar2,
                                   i_episodio in varchar2)
      RETURN tab_DADOS_NOTA_ALTA;
END;
/


/*######################################
###### PCK_ENF_AVAL_INIT_body.sql ######
######################################*/
CREATE OR REPLACE PACKAGE BODY PCK_ENF_AVAL_INIT
IS
    FUNCTION traduz_msgbox (texto ga_dic_textos.texto_portugues%TYPE)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto_msgbox (texto, 'BD', sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz_msgbox (texto ga_dic_textos.texto_portugues%TYPE, i_var VARCHAR2)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto_msgbox (texto,
                                                      i_var,
                                                      NULL,
                                                      'BD',
                                                      sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz_msgbox (texto ga_dic_textos.texto_portugues%TYPE, i_vars DBMS_SQL.varchar2a)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto_msgbox (texto,
                                                      i_vars,
                                                      'BD',
                                                      sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz_msgbox (texto ga_dic_textos.texto_portugues%TYPE, i_vars VARCHAR2, i_separador VARCHAR2)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto_msgbox (texto,
                                                      i_vars,
                                                      i_separador,
                                                      'BD',
                                                      sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz (texto ga_dic_textos.texto_portugues%TYPE)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto (texto, 'BD', sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz (texto ga_dic_textos.texto_portugues%TYPE, i_var VARCHAR2)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto (texto,
                                               i_var,
                                               NULL,
                                               'BD',
                                               sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz (texto ga_dic_textos.texto_portugues%TYPE, i_vars DBMS_SQL.varchar2a)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto (texto,
                                               i_vars,
                                               'BD',
                                               sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION get_count_rows
        RETURN NUMBER
    IS
        o_number   NUMBER;
    BEGIN
        SELECT COUNT (*)
          INTO o_number
          FROM sdt_enf_resumo_aval_init
         WHERE row_session = arg_row_session AND row_args = arg_row_args;

        RETURN o_number;
    END;

    --******************************************************************************
    FUNCTION get_arg_row_session
        RETURN VARCHAR2
    IS
    BEGIN
        RETURN arg_row_session;
    END;

    FUNCTION get_arg_row_args
        RETURN VARCHAR2
    IS
    BEGIN
        RETURN arg_row_args;
    END;

    PROCEDURE set_arg_session_args (i_row_session VARCHAR2, i_row_args VARCHAR2)
    IS
    BEGIN
        arg_row_session := i_row_session;
        arg_row_args := i_row_args;
    END;

    --******************************************************************************
    PROCEDURE apagar_resumo (i_row_session IN VARCHAR2, i_row_args IN VARCHAR2)
    IS
    BEGIN
        pck_pe_logging.LOG ('PCK_ENF_BLOCO_PROVIDERS.apagar_RESUMO', '0', '[i_ROW_SESSION ' || i_row_session || ' i_ROW_ARGS ' || i_row_args || ']');

        DELETE FROM sdt_enf_resumo_aval_init
         WHERE row_session = i_row_session AND row_args = i_row_args;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            pck_pe_logging.LOG ('PCK_ENF_BLOCO_PROVIDERS.apagar_RESUMO', SQLERRM, 'n�o apagou');
    END;

    --******************************************************************************
    PROCEDURE apagar_resumo2 (i_row_session IN VARCHAR2)
    IS
    BEGIN
        --CHTViseu-1713: Processo que vai eliminar os registos da tabela 'SDT_ENF_RESUMO_AVAL_INIT' mas s� para a sess�o indicada pela variavel 'i_row_session'.     
        pck_pe_logging.LOG ('PCK_ENF_BLOCO_PROVIDERS.apagar_RESUMO2', '0', '[i_ROW_SESSION ' || i_row_session || ']');

        DELETE FROM sdt_enf_resumo_aval_init
         WHERE row_session = i_row_session;

        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            pck_pe_logging.LOG ('PCK_ENF_BLOCO_PROVIDERS.apagar_RESUMO2', SQLERRM, 'n�o apagou');
    END;    

    --******************************************************************************
    PROCEDURE get_resumo_inner (i_tipo_resumo IN VARCHAR2, o_row_session OUT VARCHAR2, o_row_args OUT VARCHAR2)
    IS
    BEGIN
        apagar_resumo (arg_row_session, arg_row_args);

        o_row_session := arg_row_session;
        o_row_args := arg_row_args;

        BEGIN
            SELECT n_ep_arv
              INTO arg_n_ep_arv
              FROM sd_enf_ep_arv
             WHERE t_doente = arg_t_doente AND doente = arg_doente AND t_episodio = arg_t_episodio AND episodio = arg_episodio;
        EXCEPTION
            WHEN OTHERS THEN
                arg_n_ep_arv := NULL;
                RAISE;
        END;

        --####### O DITO DO INSERT PARA A TABELA COMO  RESUMO DA AVALIA��O INICIAL.

        --CHTViseu-1713 : Adicionou-se /*+APPEND*/ ao seguinte comando e as colunas 'ROW_SESSION' e 'ROW_ARGS'. 
        INSERT /*+APPEND*/ INTO sdt_enf_resumo_aval_init (tipo,
                                                          ordem,
                                                          nivel,
                                                          pai,
                                                          cod,
                                                          descricao,
                                                          valor,
                                                          ROW_SESSION,
                                                          ROW_ARGS)
            SELECT tipo,
                   ROWNUM ordem,
                   nivel,
                   substr(pai, 2) pai,
                   substr(cod, 2) cod,
                   descricao,
                   valor,
                   arg_row_session, --CHTViseu-1713 : passar valor de ROW_SESSION
                   arg_row_args     --CHTViseu-1713 : passar valor de ROW_ARGS
              FROM (WITH items_0
                         AS (SELECT 'I' tipo,
                                    q0.ordem ordem,
                                    cod_ep_item,
                                    q0.cod_ep_item_det,
                                    nvl(decode(i_tipo_resumo, 'NOTA_ALTA', q0.DESCR_NOTA_ALTA, null), q0.descr_ep_item_det) descr_ep_item_det,
                                    pck_enf_ep_arv.devolve_valor_item_det (arg_t_doente,
                                                                           arg_doente,
                                                                           arg_t_episodio,
                                                                           arg_episodio,
                                                                           NULL,
                                                                           q0.tipo,
                                                                           q0.valor1,
                                                                           q0.ler,
                                                                           q2.valor,
                                                                           q2.codigo)
                                        valor
                               FROM (select * from gr_enf_ep_item_det  aa
                                             where nvl(aa.FLG_PASSA_NOTA_ALTA, 'X') = decode(i_tipo_resumo, 'NOTA_ALTA', 'S', nvl(aa.FLG_PASSA_NOTA_ALTA, 'X'))) q0,
                                    sd_enf_ep_item_det q2
                              WHERE q2.n_ep_arv = arg_n_ep_arv AND q2.valor IS NOT NULL AND q0.cod_ep_item_det = q2.cod_ep_item_det),
                         arvore_0
                         AS (SELECT 'E' tipo,
                                    ordem,
                                    nivel,
                                    cod_ep_item_pai,
                                    cod_ep_item,
                                    descr_ep_item
                               FROM gr_enf_ep_item q1
                              WHERE cod_ep_arv = '1'--      START WITH COD_EP_ITEM IN ( SELECT COD_EP_ITEM FROM ITEMS_0  )
                                                    --      CONNECT BY COD_EP_ITEM = PRIOR COD_EP_ITEM_PAI
                            )
                    SELECT *
                      FROM (SELECT tipo,
                                   '1_'||ordem ordem,
                                   nivel,
                                   decode(cod_ep_item_pai, null, null, tipo||cod_ep_item_pai) pai,
                                   tipo||cod_ep_item cod,
                                   decode(i_tipo_resumo, 'NOTA_ALTA', '', LPAD (' ', nivel * 4, ' ')) || descr_ep_item descricao,
                                   '' valor
                              FROM arvore_0
                            WHERE PCK_ENF_EP_ARV.tem_detalhes(arG_N_EP_ARV, arvore_0.COD_EP_ITEM , NULL, 'N')='S'
                            UNION
                            (SELECT items_0.tipo,
                                    '0_'||arvore_0.ordem || '|' || items_0.ordem ordem,
                                    arvore_0.nivel + 1 nivel,
                                    'E'||items_0.cod_ep_item pai,
                                    items_0.tipo||cod_ep_item_det cod,
                                    decode(i_tipo_resumo, 'NOTA_ALTA', '', LPAD (' ', (arvore_0.nivel + 1) * 4, ' ')) || descr_ep_item_det descricao,
                                    items_0.valor
                               FROM items_0, arvore_0
                              WHERE items_0.cod_ep_item = arvore_0.cod_ep_item))
                    START WITH pai IS NULL
                    CONNECT BY PRIOR cod = pai
                     ORDER SIBLINGS BY ORDEM
                   )
               where tipo = decode(i_tipo_resumo, 'NOTA_ALTA', 'I', tipo);


        COMMIT;
    EXCEPTION
        WHEN OTHERS THEN
            pck_pe_logging.LOG (
                'PCK_ENF_BLOCO_PROVIDERS.get_RESUMO',
                SQLERRM,
                   '[i_T_DOENTE='
                || arg_t_doente
                || '][i_DOENTE='
                || arg_doente
                || '][i_T_EPISODIO='
                || arg_t_episodio
                || '][i_EPISODIO='
                || arg_episodio
                || '][o_ROW_SESSION='
                || o_row_session
                || '][o_ROW_ARGS='
                || o_row_args
                || ']');
    END;

    --******************************************************************************
    PROCEDURE get_resumo (o_row_session OUT VARCHAR2, o_row_args OUT VARCHAR2)
    IS
      w_ROW_SESSION VARCHAR(30);
      w_ROW_ARGS    VARCHAR2(64);
    BEGIN
      get_resumo_inner ('NORMAL', w_ROW_SESSION, w_ROW_ARGS);
      o_row_session := w_row_session;
      o_ROW_ARGS := w_ROW_ARGS;
    END;

    FUNCTION get_resumo_nota_alta (i_t_doente in varchar2,
                                   i_doente in varchar2,
                                   i_t_episodio in varchar2,
                                   i_episodio in varchar2)
      RETURN tab_DADOS_NOTA_ALTA
    IS
      w_aux tab_DADOS_NOTA_ALTA := tab_DADOS_NOTA_ALTA();
      w_ROW_SESSION varchar2(200);
      w_ROW_ARGS varchar2(200);
    BEGIN
      init(i_t_doente, i_doente, i_t_episodio, i_episodio, null, null);

      get_resumo_inner ('NOTA_ALTA', w_ROW_SESSION, w_ROW_ARGS);

      w_aux.delete;

      select descricao, valor
      bulk collect into w_aux
      from sdt_enf_resumo_aval_init
      where ROW_SESSION = w_ROW_SESSION and ROW_ARGS = w_ROW_ARGS;

      return w_aux;
    EXCEPTION WHEN OTHERS THEN
            pck_pe_logging.LOG (
                'PCK_ENF_AVAL_INIT.get_resumo_nota_alta',
                SQLERRM,
                   '[i_T_DOENTE='
                || arg_t_doente
                || '][i_DOENTE='
                || arg_doente
                || '][i_T_EPISODIO='
                || arg_t_episodio
                || '][i_EPISODIO='
                || arg_episodio
                || '][o_ROW_SESSION='
                || w_row_session
                || '][o_ROW_ARGS='
                || w_row_args
                || ']');
    END;

    --******************************************************************************
    -- VERIFICA SE DADO ITEM TEM HISTORIAL DE ALTERA��ES.

    FUNCTION get_tem_hist_alteracoes (i_n_ep_arv IN NUMBER, i_cod_ep_item_det IN varchar2)
        RETURN BOOLEAN
    IS
        w_count   NUMBER;
    BEGIN
        SELECT COUNT (0)
          INTO w_count
          FROM sd_enf_aval_init_log
         WHERE n_ep_arv = i_n_ep_arv AND cod_ep_item_det = i_cod_ep_item_det;

        IF w_count > 1 THEN
            RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    END;


    /* FUNCTION get_CHANGELOG_ITEM
     (
      i_COD_EP_ITEM_DET IN NUMBER,
      i_N_EP_ARV   IN NUMBER
     )
     RETURN t_log
     IS
      t_resp pck_enf_aval_init.t_log;

     BEGIN
       SELECT ID, N_EP_ARV, COD_EP_ITEM_DET, VALOR_INI, VALOR_FIM, USER_CRI, USER_ALT, DT, MOTIVO, FLG_AMBITO
         BULK COLLECT INTO t_resp
         FROM SD_ENF_AVAL_INIT_LOG
        WHERE N_EP_ARV = i_N_EP_ARV
          AND COD_EP_ITEM_DET = i_COD_EP_ITEM_dET
          ORDER BY dt desc;




     END;*/
    FUNCTION get_changelog
        RETURN t_log_aval_init
    IS
        t_resp       pck_enf_aval_init.t_log;

        t_log        t_log_aval_init;
        p_item_det   gr_enf_ep_item_det%ROWTYPE;
    BEGIN
        SELECT r_log_aval_init (id,
                                n_ep_arv,
                                cod_ep_item_det,
                                get_valor_final_det (n_ep_arv, cod_ep_item_det, valor_ini),
                                get_valor_final_det (n_ep_arv, cod_ep_item_det, valor_fim),
                                user_cri,
                                user_alt,
                                dt,
                                motivo,
                                flg_ambito)
          BULK COLLECT INTO t_log
          FROM sd_enf_aval_init_log
         WHERE n_ep_arv = arg_n_ep_arv
        ORDER BY dt DESC;

        --t_log := t_resp;

        RETURN t_log;
    EXCEPTION WHEN OTHERS THEN
        pck_pe_logging.log('PCK_ENF_AVAL_INIT','ERRO - '|| arg_n_ep_arv ||' - '|| SQLERRM);
        RETURN NULL;
    END;

    FUNCTION get_valor_final_det (i_n_ep_arv NUMBER, i_cod_ep_item_det VARCHAR2, i_valor VARCHAR2)
        RETURN VARCHAR2
    IS
        w_valor    VARCHAR2 (2000);
        w_tipo     gr_enf_ep_item_det.tipo%TYPE;
        w_valor1   gr_enf_ep_item_det.valor1%TYPE;
        w_ler      gr_enf_ep_item_det.ler%TYPE;
    BEGIN
        SELECT tipo, valor1, ler
          INTO w_tipo, w_valor1, w_ler
          FROM gr_enf_ep_item_det q0
         WHERE q0.cod_ep_item_det = i_cod_ep_item_det;

        IF w_ler IS NULL THEN
            w_valor :=
                pck_enf_ep_arv.devolve_valor_item_det (arg_t_doente,
                                                       arg_doente,
                                                       arg_t_episodio,
                                                       arg_episodio,
                                                       NULL,
                                                       w_tipo,
                                                       w_valor1,
                                                       w_ler,
                                                       i_valor);
        ELSE
            w_valor := i_valor;
        END IF;

        RETURN w_valor;
    END;

    PROCEDURE limpa_resumo
    IS
    BEGIN
        apagar_resumo (arg_row_session, arg_row_args);
    END;

    FUNCTION GET_VALOR_ITEM_DET( i_N_EP_ARV NUMBER , i_COD_EP_ITEM_DET VARCHAR2)
    RETURN VARCHAR2
    IS
        o_cod_ep_item_det       GR_ENF_EP_ITEM_DET.COD_EP_ITEM_DET%TYPE;
        o_descr_ep_item_det     GR_ENF_EP_ITEM_DET.DESCR_EP_ITEM_DET%TYPE;
        o_valor                 SD_ENF_EP_ITEM_DET.VALOR%TYPE;
        o_tipo                  GR_ENF_EP_ITEM_DET.TIPO%TYPE;
        o_valor1                GR_ENF_EP_ITEM_DET.VALOR1%TYPE;
        o_valor2                GR_ENF_EP_ITEM_DET.VALOR2%TYPE;
        w_t_doente              VARCHAR2(50);
        w_doente                VARCHAR2(50);
        w_t_episodio            VARCHAR2(50);
        w_episodio              VARCHAR2(50);
        w_cod_serv              VARCHAR2(100);
        w_query                 VARCHAR2(2000);
        ret                     varchar2(2000);
    BEGIN
        SELECT  cod_ep_item_det,
                descr_ep_item_det,
                valor,
                tipo,
                valor1,
                valor2
        INTO    o_cod_ep_item_det,
                o_descr_ep_item_det,
                o_valor,
                o_tipo,
                o_valor1,
                o_valor2
        FROM (SELECT
                q0.cod_ep_item_det,
                descr_ep_item_det,
                q2.valor valor,
                q0.tipo,
                q0.valor1,
                q0.valor2
            FROM     gr_enf_ep_item_det q0
             LEFT OUTER JOIN
                 (SELECT *
                    FROM sd_enf_ep_item_det
                   WHERE n_ep_arv = i_N_EP_ARV) q2
             ON q0.cod_ep_item_det = q2.cod_ep_item_det)
        WHERE cod_ep_item_det = i_COD_EP_ITEM_DET AND tipo <> 'HIDDEN' and rownum < 2;

        -- tratar os varios tipos de item

        -- ## LOVS.
        IF o_tipo = 'LOV' THEN

            IF o_valor IS NOT NULL THEN
                ret := pck_enf_ep_arv.devolve_valor_lov (o_valor1, --cod_lov
                                                            o_valor);
            END IF;

        END IF;

        -- ## CHECKS
        IF o_tipo = 'CHECK' THEN

            IF NVL (o_valor2, 'NuLL') = '2' THEN
                o_tipo := 'CHECK_SN';
            END IF;

            IF o_valor = 'S' THEN
                ret := 'Sim';
            ELSIF o_valor = 'N' THEN
                ret := 'N�o';
            END IF;

        END IF;

        -- ## DEFEITO
        IF o_tipo = 'DEFEITO' THEN
            IF NVL (o_valor1, 'NuLL') <> 'NuLL' THEN
                SELECT t_doente, doente, t_episodio, episodio
                  INTO w_t_doente, w_doente, w_t_episodio, w_episodio
                  FROM sd_enf_ep_arv
                 WHERE n_ep_arv = i_n_ep_arv;

                w_cod_serv :=
                    sdf_enf_obter_serv (i_t_doente  => w_t_doente,
                                        i_doente    => w_doente,
                                        i_t_episodio => w_t_episodio,
                                        i_episodio  => w_episodio);

                w_query :=
                    pck_enf_ep_arv.altera_valores (i_valor1    => o_valor1,
                                                   i_t_doente  => w_t_doente,
                                                   i_doente    => w_doente,
                                                   i_t_episodio => w_t_episodio,
                                                   i_episodio  => w_episodio,
                                                   i_cod_serv  => w_cod_serv);

                ret := sdf_enf_devolve_query (w_query);

            END IF;
        END IF;

        IF ret IS NULL THEN
            ret := o_valor;
        END IF;

        RETURN ret;
    EXCEPTION WHEN OTHERS THEN
        RETURN NULL;
    END;

    PROCEDURE init (i_t_doente     IN VARCHAR2,
                    i_doente       IN VARCHAR2,
                    i_t_episodio   IN VARCHAR2,
                    i_episodio     IN VARCHAR2,
                    i_n_ep_arv     IN VARCHAR2,
                    i_COD_SERV     IN VARCHAR2)
    IS
    BEGIN
        arg_t_doente := i_t_doente;
        arg_doente := i_doente;
        arg_t_episodio := i_t_episodio;
        arg_episodio := i_episodio;
        arg_n_ep_arv := i_n_ep_arv;
        arg_SERV    := i_COD_SERV;
        arg_row_session := USERENV ('SESSIONID');
        arg_row_args := arg_t_doente || '#' || arg_doente || '#' || arg_t_episodio || '#' || arg_episodio;

        log_dev (i_debug => 'PCK_ENF_AVAL_INIT.INIT');
    /*
      SELECT *
        BULK COLLECT INTO arg_DADOS_BD
        FROM SDV_ENF_AVAL_INIT_DADOS
       WHERE N_EP_ARV = i_N_EP_ARV
         OR N_EP_ARV IS NULL;*/

    END;

    PROCEDURE log_dev (i_debug IN VARCHAR2)
    IS
    BEGIN
        pck_pe_logging.LOG (
            i_origem   => 'DEV_DEBUG',
            i_msg      => i_debug,
            i_args     =>    '[N_EP_ARV: '
                          || arg_n_ep_arv
                          || ']'
                          || '[arg_T_DOENTE: '
                          || arg_t_doente
                          || ']'
                          || '[arg_DOENTE: '
                          || arg_doente
                          || ']'
                          || '[arg_T_EPISODIO: '
                          || arg_t_episodio
                          || ']'
                          || '[arg_ROW_SESSION: '
                          || arg_row_session
                          || ']');
    END;

    FUNCTION get_dados_db (i_cod_item IN VARCHAR2)
        RETURN tb_dados_bd
    IS
        w_dados_item   tb_dados_bd;
    BEGIN
        --FROM TABLE(CAST(revised AS CourseList)) new,

        /*
        SELECT *
          BULK COLLECT INTO w_dados_item
          FROM sdv_enf_aval_init_dados
         WHERE ( tipo_linha = 'E' AND cod = i_cod_item OR tipo_linha = 'I' AND pai = i_cod_item)
          AND  ( n_ep_arv = arg_N_EP_ARV OR n_ep_arv IS NULL )
         ;*/

        SELECT *                                                                                                                                      --DISTINCT PAI, COD, DESCRICAO
          BULK COLLECT INTO w_dados_item
          FROM sdv_enf_aval_init_dados q1
         WHERE     (tipo_linha = 'I' AND pai = i_cod_item)
               AND (n_ep_arv = arg_n_ep_arv OR n_ep_arv IS NULL)
               AND (       valor IS NULL
                       AND NOT EXISTS
                               (SELECT 0                                                                                                              --DISTINCT PAI, COD, DESCRICAO
                                  FROM sdv_enf_aval_init_dados q2
                                 WHERE q1.ordem = q2.ordem AND q2.n_ep_arv IS NOT NULL AND q2.n_ep_arv = arg_n_ep_arv)
                    OR valor IS NOT NULL);

        log_dev ('GET_DADOS_DB count ' || w_dados_item.COUNT);

        RETURN w_dados_item;
    EXCEPTION
        WHEN OTHERS THEN
            log_dev (SQLERRM);
    END;


    FUNCTION count_items (i_cod_item IN VARCHAR2)
        RETURN NUMBER
    IS
        w_res   NUMBER;
    BEGIN
        SELECT COUNT (1)                                                                                                                              --DISTINCT PAI, COD, DESCRICAO
          INTO w_res
          FROM sdv_enf_aval_init_dados q1
         WHERE     (tipo_linha = 'I' AND pai = i_cod_item)
               AND (n_ep_arv = arg_n_ep_arv OR n_ep_arv IS NULL)
               AND (       valor IS NULL
                       AND NOT EXISTS
                               (SELECT 0                                                                                                              --DISTINCT PAI, COD, DESCRICAO
                                  FROM sdv_enf_aval_init_dados q2
                                 WHERE q1.ordem = q2.ordem AND q2.n_ep_arv IS NOT NULL AND q2.n_ep_arv = arg_n_ep_arv)
                    OR valor IS NOT NULL);

        RETURN w_res;
    END;


    FUNCTION get_folha (i_n_ep_arv NUMBER, i_cod_ep_item VARCHAR2)
        RETURN tb_dados_bd
    IS
        w_folha   tb_dados_bd;
        i         NUMBER := 0;
    --w_folha SDV_ENF_AVAL_INIT_DADOS%rowtype;
    BEGIN
        /*
        -- CHTViseu-1844 e Jira : Lentid�o no acesso a folhas da �rvore da Avalia��o Inicial. 
        --                        Passar a considerar os cursores e n�o a query abaixo.      
        SELECT tipo_linha,
               ordem,
               nivel,
               pai,
               cod,
               descricao,
               valor,
               valor_init,
               n_ep_arv,
               tipo,
               tipo_dados,
               mascara,
               valor1,
               valor2,
               ler,
               gravar,
               codigo,
               pode_alterar,
               usr,
               dt
          BULK COLLECT INTO w_folha
          FROM (SELECT q2.n_ep_arv n_ep_arv,
                       'I' tipo_linha,
                       q0.ordem ordem,
                       NULL nivel,
                       cod_ep_item pai,
                       q0.cod_ep_item_det cod,
                       descr_ep_item_det descricao,
                       q2.valor valor,          -- pck_enf_ep_arv.devolve_valor_item_det('HS', '444705', 'Internamentos', '1171', NULL, Q0.TIPO , Q0.VALOR1, Q0.LER, Q2.VALOR) VALOR
                       NULL valor_init,
                       q0.tipo,
                       q0.tipo_dados,
                       q0.mascara,
                       q0.valor1,
                       q0.valor2,
                       q0.ler,
                       q0.gravar,
                       q2.codigo,
                       '' pode_alterar,
                       NVL (q2.user_act, q2.user_cri) usr,
                       NVL (TO_CHAR (q2.dt_act, 'yyyy-mm-dd hh24:mi'), TO_CHAR (q2.dt_cri, 'yyyy-mm-dd hh24:mi')) dt
                  FROM gr_enf_ep_item_det q0
                       LEFT OUTER JOIN (SELECT *
                                          FROM sd_enf_ep_item_det
                                         WHERE n_ep_arv = i_n_ep_arv) q2
                           ON q0.cod_ep_item_det = q2.cod_ep_item_det
                    WHERE NOT EXISTS (SELECT * FROM GR_ENF_EP_N_ITEM_DET Q3 WHERE Q0.COD_EP_ITEM_DET = Q3.COD_EP_ITEM_DET AND arg_SERV = Q3.COD_SERV)
                           )
         WHERE pai = i_cod_ep_item
        ORDER BY ordem;
        */

        DECLARE
            CURSOR c1 IS
                  SELECT TIPO_LINHA,
                         ORDEM,
                         NIVEL,
                         PAI,
                         COD,
                         DESCRICAO,
                         VALOR_INIT,
                         TIPO,
                         TIPO_DADOS,
                         MASCARA,
                         VALOR1,
                         VALOR2,
                         LER,
                         GRAVAR,
                         PODE_ALTERAR 
                    FROM (
                             SELECT 
                                    'I'                    TIPO_LINHA,
                                    Q0.ORDEM               ORDEM,
                                    NULL                   NIVEL,
                                    q0.COD_EP_ITEM         PAI,
                                    Q0.COD_EP_ITEM_DET     COD,
                                    DESCR_EP_ITEM_DET      DESCRICAO,
                                    NULL                   VALOR_INIT,
                                    Q0.TIPO,
                                    Q0.TIPO_DADOS,
                                    Q0.MASCARA,
                                    Q0.VALOR1,
                                    Q0.VALOR2,
                                    Q0.LER,
                                    Q0.GRAVAR,
                                    ''                     PODE_ALTERAR 
                               FROM GR_ENF_EP_ITEM_DET Q0
                              WHERE NOT EXISTS
                                        (SELECT *
                                           FROM GR_ENF_EP_N_ITEM_DET Q3
                                          WHERE Q0.COD_EP_ITEM_DET = Q3.COD_EP_ITEM_DET AND arg_SERV = Q3.COD_SERV)
                         )
                   WHERE PAI = i_cod_ep_item
                ORDER BY ORDEM;

            w_contador   NUMBER := 0;
        BEGIN
            FOR c1_reg IN c1 LOOP
                w_contador := w_contador + 1;
                w_folha (w_contador).tipo_linha := c1_reg.tipo_linha;
                w_folha (w_contador).ordem := c1_reg.ordem;
                w_folha (w_contador).nivel := c1_reg.nivel;
                w_folha (w_contador).pai := c1_reg.pai;
                w_folha (w_contador).cod := c1_reg.cod;
                w_folha (w_contador).descricao := c1_reg.descricao;
                w_folha (w_contador).valor_init := c1_reg.valor_init;
                w_folha (w_contador).n_ep_arv := i_n_ep_arv;
                w_folha (w_contador).tipo := c1_reg.tipo;
                w_folha (w_contador).tipo_dados := c1_reg.tipo_dados;
                w_folha (w_contador).mascara := c1_reg.mascara;
                w_folha (w_contador).valor1 := c1_reg.valor1;
                w_folha (w_contador).valor2 := c1_reg.valor2;
                w_folha (w_contador).ler := c1_reg.ler;
                w_folha (w_contador).gravar := c1_reg.gravar;
                w_folha (w_contador).pode_alterar := c1_reg.pode_alterar;

                DECLARE
                    CURSOR c2 IS
                        SELECT Q2.valor,
                               Q2.codigo,
                               NVL (Q2.USER_ACT, Q2.USER_CRI)                                                                 USR,
                               NVL (TO_CHAR (Q2.DT_ACT, 'yyyy-mm-dd hh24:mi'), TO_CHAR (Q2.DT_CRI, 'yyyy-mm-dd hh24:mi'))     DT
                          FROM SD_ENF_EP_ITEM_DET Q2
                         WHERE Q2.N_EP_ARV = i_n_ep_arv AND Q2.COD_EP_ITEM_DET = c1_reg.COD;

                    w_contador2   NUMBER := 1;
                BEGIN
                    FOR c2_reg IN c2 LOOP
                        IF w_contador2 = 1 THEN
                            w_contador2 := w_contador2 + 1;
                        ELSE
                            w_contador := w_contador + 1;
                            w_folha (w_contador).tipo_linha := c1_reg.tipo_linha;
                            w_folha (w_contador).ordem := c1_reg.ordem;
                            w_folha (w_contador).nivel := c1_reg.nivel;
                            w_folha (w_contador).pai := c1_reg.pai;
                            w_folha (w_contador).cod := c1_reg.cod;
                            w_folha (w_contador).descricao := c1_reg.descricao;
                            w_folha (w_contador).valor_init := c1_reg.valor_init;
                            w_folha (w_contador).n_ep_arv := i_n_ep_arv;
                            w_folha (w_contador).tipo := c1_reg.tipo;
                            w_folha (w_contador).tipo_dados := c1_reg.tipo_dados;
                            w_folha (w_contador).mascara := c1_reg.mascara;
                            w_folha (w_contador).valor1 := c1_reg.valor1;
                            w_folha (w_contador).valor2 := c1_reg.valor2;
                            w_folha (w_contador).ler := c1_reg.ler;
                            w_folha (w_contador).gravar := c1_reg.gravar;
                            w_folha (w_contador).pode_alterar := c1_reg.pode_alterar;
                        END IF;

                        w_folha (w_contador).valor := c2_reg.valor;
                        w_folha (w_contador).codigo := c2_reg.codigo;
                        w_folha (w_contador).usr := c2_reg.usr;
                        w_folha (w_contador).dt := c2_reg.dt;
                    END LOOP;
                END;
            END LOOP;
        END;        
        
        RETURN w_folha;

    EXCEPTION WHEN OTHERS THEN
    PCK_PE_LOGGING.LOG(I_ORIGEM=> 'get_folha_dados', I_MSG=> 'excep��o '||sqlerrm);
    END;

    PROCEDURE load_n_ep_arv (i_t_doente     IN      VARCHAR2,
                             i_doente       IN      VARCHAR2,
                             i_t_episodio   IN      VARCHAR2,
                             i_episodio     IN      VARCHAR2,
                             i_cod_ep_arv   IN      VARCHAR2,
                             i_n_ep_arv         OUT NUMBER,
                             i_n_ep_arv_h       OUT NUMBER,
                             i_flg_arv_h        OUT VARCHAR2
                             )
     IS
     BEGIN


        pck_enf_ep_arv.tem_aval_init (i_t_doente         => i_t_doente,
                                      i_doente           => i_doente,
                                      i_t_episodio       => i_t_episodio,
                                      i_episodio         => i_episodio,
                                      i_cod_arv          => i_cod_ep_arv,
                                      o_n_ep_arv         => i_n_ep_arv,
                                      o_n_ep_arv_antes   => i_n_ep_arv_h);

        IF i_n_ep_arv_h IS NULL OR i_n_ep_arv IS NOT NULL THEN
            i_flg_arv_h := 'N';
        ELSIF i_n_ep_arv_h IS NOT NULL AND i_n_ep_arv IS NULL THEN
            i_flg_arv_h := 'S';
        ELSE -- ## JUST IN CASE..
            i_flg_arv_h := 'N';
        END IF;


     END;

    FUNCTION obtem_formula (i_cod_indic IN VARCHAR2)
        RETURN VARCHAR2
    IS
        w_formula   VARCHAR2 (100);
    BEGIN

        SELECT formula_indicador_derivado
          INTO w_formula
          FROM gr_enf_indic
         WHERE formula_indicador_derivado IS NOT NULL AND cod_indic = i_cod_indic;

         return w_formula;

         exception when others then
         pck_pe_logging.log(I_ORIGEM=>'formula', I_MSG=>'i_cod_indic '||i_cod_indic);
         return null;

    END;

    FUNCTION GET_ARG_SERV
    RETURN VARCHAR2
    IS
    BEGIN
         RETURN arg_serv;
    END;

END;
/


/*######################################
###### PCK_ENF_HISTORICO_body.sql ######
######################################*/
CREATE OR REPLACE PACKAGE BODY pck_enf_historico
IS
    w_const_diag_herdado     CONSTANT VARCHAR2 (100) := 'Diagn�stico Herdado';
    w_const_interv_herdado   CONSTANT VARCHAR2 (100) := 'Interven��o Herdada';

    FUNCTION traduz_msgbox (texto ga_dic_textos.texto_portugues%TYPE)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto_msgbox (texto, 'BD', sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz_msgbox (texto ga_dic_textos.texto_portugues%TYPE, i_var VARCHAR2)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto_msgbox (texto,
                                                      i_var,
                                                      NULL,
                                                      'BD',
                                                      sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz_msgbox (texto ga_dic_textos.texto_portugues%TYPE, i_vars DBMS_SQL.varchar2a)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto_msgbox (texto,
                                                      i_vars,
                                                      'BD',
                                                      sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz_msgbox (texto ga_dic_textos.texto_portugues%TYPE, i_vars VARCHAR2, i_separador VARCHAR2)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto_msgbox (texto,
                                                      i_vars,
                                                      i_separador,
                                                      'BD',
                                                      sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz (texto ga_dic_textos.texto_portugues%TYPE)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto (texto, 'BD', sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz (texto ga_dic_textos.texto_portugues%TYPE, i_var VARCHAR2)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto (texto,
                                               i_var,
                                               NULL,
                                               'BD',
                                               sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz (texto ga_dic_textos.texto_portugues%TYPE, i_vars DBMS_SQL.varchar2a)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto (texto,
                                               i_vars,
                                               'BD',
                                               sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    -----------------------------------------------------------
    -- CONVERT_NMECAN_TO_USERSYS ------------------------------
    -----------------------------------------------------------
    FUNCTION convert_nmecan_to_usersys (i_n_mec VARCHAR2)
        RETURN sd_pess_hosp_def.user_sys%TYPE
    IS
        o_user_sys   sd_pess_hosp_def.user_sys%TYPE;
    BEGIN
        SELECT user_sys
          INTO o_user_sys
          FROM sd_pess_hosp_def
         WHERE n_mecan = i_n_mec;

        RETURN o_user_sys;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    PROCEDURE set_t_pess_hosp (i_t_pess_hosp IN gr_enf_historico_config_det.t_pess_hosp%TYPE)
    IS
    BEGIN
        p_t_pess_hosp := i_t_pess_hosp;
    END;

    PROCEDURE del_t_pess_hosp
    IS
    BEGIN
        p_t_pess_hosp := NULL;
    END;


    FUNCTION get_t_pess_hosp
        RETURN gr_enf_historico_config_det.t_pess_hosp%TYPE
    IS
    BEGIN
        RETURN p_t_pess_hosp;
    END;

    FUNCTION get_modo (i_t_doente     IN VARCHAR2,
                       i_doente       IN VARCHAR2,
                       i_t_episodio   IN VARCHAR2,
                       i_episodio     IN VARCHAR2)
        RETURN VARCHAR2 DETERMINISTIC
    IS
        o_modo   VARCHAR2 (10);
    BEGIN
        IF (i_t_episodio = 'Internamentos')
        THEN
            o_modo := 'M_INT';
        ELSIF (pk_episodio.ghf_verif_se_episodio_urg (i_t_doente,
                                                      i_doente,
                                                      i_t_episodio,
                                                      i_episodio,
                                                      NULL))
        THEN
            o_modo := 'M_URG';
        ELSIF (i_t_episodio = 'Ambulatorio')
        THEN
            o_modo := 'M_AMB';
        ELSIF (i_t_episodio = 'Consultas')
        THEN
            o_modo := 'M_CON';
        ELSE
            o_modo := 'M_OUT';
        END IF;

        RETURN o_modo;
    END;

    /**************************************************/
    FUNCTION processa_texto (i_texto CLOB)
        RETURN CLOB
    IS
        vinstr1    NUMBER;
        vinstr2    NUMBER;
        res        CLOB := NULL;
        i_texto2   CLOB := i_texto;
    BEGIN
        IF i_texto IS NULL
        THEN
            RETURN '';
        END IF;

        IF NVL (LENGTH (i_texto), 0) = 0
        THEN
            RETURN '';
        END IF;

        --IF (LENGTH (i_texto) <= vmax)
        --THEN
        --  RETURN REPLACE (i_texto, CHR (10), '<CHR10>');
        --END IF;

        WHILE (LENGTH (i_texto2) > vmax)
        LOOP
            -- ultimo espa??o
            vinstr1 := INSTR (DBMS_LOB.SUBSTR (i_texto2, vmax, 1), ' ', -1);
            -- menor ENTER
            vinstr2 := INSTR (DBMS_LOB.SUBSTR (i_texto2, vmax, 1), CHR (10), 1);

            IF (vinstr2 > 0)
            THEN
                res := res || DBMS_LOB.SUBSTR (i_texto2, vinstr2 - 1, 1) || '<CHR10>';
                i_texto2 := DBMS_LOB.SUBSTR (i_texto2, LENGTH (i_texto2) - vinstr2, vinstr2 + 1);
            ELSIF (vinstr1 > 0)
            THEN
                res := res || DBMS_LOB.SUBSTR (i_texto2, vinstr1 - 1, 1)                                                                                             --|| '<CHR32>';
                                                                        || '<CHR10>';
                i_texto2 := DBMS_LOB.SUBSTR (i_texto2, LENGTH (i_texto2) - vinstr1, vinstr1 + 1);
            ELSE
                res := res || DBMS_LOB.SUBSTR (i_texto2, vmax, 1) || '<CHR32>';
                i_texto2 := DBMS_LOB.SUBSTR (i_texto2, LENGTH (i_texto2) - vmax, vmax + 1);
            END IF;
        END LOOP;

        i_texto2 := REPLACE (i_texto2, CHR (10), '<CHR10>');

        IF (LENGTH (res) > 0)
        THEN
            DBMS_LOB.writeappend (res, LENGTH (i_texto2), i_texto2);
        ELSE
            res := i_texto2;
        END IF;

        RETURN res;
    END;

    /**************************************************/
    -- Limpa tabelas tempor�rias IPO-P-13791 ----------------------------------
    PROCEDURE limpa_tabela is
    begin
            EXECUTE IMMEDIATE 'TRUNCATE TABLE SDT_ENF_HISTORICO_STATUS';
            EXECUTE IMMEDIATE 'TRUNCATE TABLE SDT_ENF_HISTORICO';
            EXECUTE IMMEDIATE 'TRUNCATE TABLE SDT_ENF_HISTORICO_2';
    end;

    /**************************************************/
    -- processa_TEXTO ----------------------------------
    PROCEDURE processa_tabela
    IS
        /*
            CURSOR rec_tipo
            IS
                SELECT tipo,
                       data,
                       t_episodio,
                       episodio,
                       utilizador                                              --,
                  --activo
                  FROM sdt_enf_historico
                 WHERE     (data >= NVL (var_dt_inicio, data))
                       AND (data <= NVL (var_dt_fim, data))
                       AND (episodio = NVL (var_episodio, episodio))
                       AND texto IS NOT NULL
                GROUP BY tipo,
                         data,
                         t_episodio,
                         episodio,
                         --data,
                         utilizador                                           -- ,
                                   --activo
        ;

            CURSOR a_processar (
                i_tipo       IN sdt_enf_historico.tipo%TYPE,
                i_data       IN sdt_enf_historico.data%TYPE,
                i_util       IN sdt_enf_historico.utilizador%TYPE,
                i_t_episodio in sdt_enf_historico.t_episodio%TYPE,
                i_episodio   in sdt_enf_historico.episodio%TYPE
                )
            IS
                SELECT t_episodio,
                       episodio,
                       data,
                       tipo,
                       texto,
                       utilizador,
                       activo
                  FROM sdt_enf_historico
                 WHERE     (var_activo IS NULL OR var_activo = activo)
                       AND (episodio = NVL (i_episodio, episodio))
                       and t_episodio = nvl(i_t_episodio,episodio)
                       AND (tipo = i_tipo)
                       AND (data = i_data)
                       AND (texto IS NOT NULL)
                       AND (utilizador = i_util)
                  order BY tipo,
                         data,
                         t_episodio,
                         episodio,
                         --data,
                         utilizador ;
            */

        CURSOR a_processar
        IS
              SELECT t_episodio,
                     episodio,
                     data,
                     tipo,
                     texto,
                     utilizador,
                     activo,
                     id
                FROM sdt_enf_historico
               WHERE ( (LENGTH (texto) < 160 AND INSTR (texto, CHR (10)) > 0) OR LENGTH (texto) > 160)
            ORDER BY tipo,
                     data,
                     t_episodio,
                     episodio,
                     utilizador;

        CURSOR processado
        IS
              SELECT t_episodio,
                     episodio,
                     data,
                     tipo,
                     texto,
                     utilizador,
                     activo,
                     id
                FROM sdt_enf_historico
               WHERE NOT LENGTH (texto) > 160 AND NOT INSTR (texto, CHR (10)) > 0
            ORDER BY tipo,
                     data,
                     t_episodio,
                     episodio,
                     utilizador;

        vinstr1           NUMBER;
        vinstr2           NUMBER;
        vtexto            CLOB;
        w_tipo            sdt_enf_historico.tipo%TYPE;
        w_data            sdt_enf_historico.data%TYPE;
        w_util            sdt_enf_historico.utilizador%TYPE;
        i_texto_inserir   CLOB;
        teste             CLOB;

        PROCEDURE insere (i_t_episodio     VARCHAR2,
                          i_episodio       VARCHAR2,
                          i_data           DATE,
                          i_tipo           VARCHAR2,
                          i_texto_comp     VARCHAR2,
                          i_texto          VARCHAR2,
                          i_texto_linha    NUMBER,
                          i_utilizador     VARCHAR2,
                          i_activo         VARCHAR2,
                          id               VARCHAR2)
        IS
            v_modo     VARCHAR2 (10);
            i_ordena   NUMBER;
        BEGIN
            BEGIN
                SELECT NVL (MAX (ordena), 0) INTO i_ordena FROM sdt_enf_historico_2;
            EXCEPTION
                WHEN OTHERS
                THEN
                    i_ordena := 0;
            END;

            i_ordena := i_ordena + 1;

            v_modo :=
                get_modo (var_t_doente,
                          var_doente,
                          i_t_episodio,
                          i_episodio);


            INSERT /*+ APPEND */
                  INTO  sdt_enf_historico_2 (t_episodio,
                                             episodio,
                                             data,
                                             tipo,
                                             texto_completo,
                                             texto,
                                             texto_linha,
                                             utilizador,
                                             activo,
                                             modo,
                                             id,
                                             ordena)
                 VALUES (i_t_episodio,
                         i_episodio,
                         i_data,
                         i_tipo,
                         i_texto_comp,
                         TRIM (LEADING CHR (10) FROM i_texto),
                         i_texto_linha,
                         i_utilizador,
                         i_activo,
                         v_modo,
                         id,
                         i_ordena);
        END;
    BEGIN
        FOR rec IN a_processar
        LOOP
            DECLARE
                w_t_episodio   sdt_enf_historico.t_episodio%TYPE;
                w_episodio     sdt_enf_historico.episodio%TYPE;
                w_activo       sdt_enf_historico.activo%TYPE;
                texto_ins      sdt_enf_historico.texto%TYPE;
                w_utilizador   sdt_enf_historico.utilizador%TYPE;
                w_id           sdt_enf_historico.id%TYPE;
                vindex         NUMBER := 0;
                vinstr         NUMBER;
            BEGIN
                i_texto_inserir := TRIM (LEADING CHR (10) FROM rec.texto) || CHR (10);

                texto_ins := TRIM (LEADING CHR (10) FROM rec.texto);
                w_t_episodio := rec.t_episodio;
                w_episodio := rec.episodio;
                w_activo := rec.activo;
                w_utilizador := rec.utilizador;
                w_id := rec.id;

                vtexto := '';

                --fborges 22032010
                vtexto := REGEXP_REPLACE (i_texto_inserir, '[[:cntrl:]]{2,}', CHR (10));
                -- Altera??o pois todos os newline eram tranformados em espa?os,
                -- e no caso dos diagn?sticos com qualificadores ficava estranha a leitura.
                i_texto_inserir := REGEXP_REPLACE (vtexto, '[' || CHR (10) || ']{3,}', CHR (10) || CHR (10));
                vtexto := REGEXP_REPLACE (i_texto_inserir, CHR (10), '<ENTER>');
                i_texto_inserir := REGEXP_REPLACE (vtexto, '[[:space:]]{2,}', ' ');
                vtexto := REGEXP_REPLACE (i_texto_inserir, '<ENTER>', CHR (10));
                --fborges 22032010
                vtexto := processa_texto (vtexto);

                LOOP
                    --dbms_output.put_line(vtexto);

                    IF (vtexto IS NULL OR vtexto = EMPTY_CLOB ())
                    THEN
                        EXIT;
                    END IF;

                    vindex := vindex + 1;
                    vinstr := INSTR (vtexto, '<CHR');

                    WHILE vinstr = 1
                    LOOP
                        vtexto := DBMS_LOB.SUBSTR (vtexto, LENGTH (vtexto), 7);
                        --vindex := vindex + 1;
                        vinstr := INSTR (vtexto, '<CHR');
                    END LOOP;

                    IF (vinstr = 0)
                    THEN
                        teste := vtexto;
                        insere (w_t_episodio,                                                                                                                     --rec1.t_episodio,
                                w_episodio,                                                                                                                         --rec1.episodio,
                                rec.data,
                                rec.tipo,
                                texto_ins,                                                                                                                              --rec.texto,
                                vtexto,
                                vindex,
                                w_utilizador,                                                                                                                     --rec1.utilizador,
                                w_activo,
                                w_id                                                                                                                                   --rec1.activo
                                    );
                        EXIT;
                    ELSE
                        teste := vtexto;

                        IF DBMS_LOB.SUBSTR (vtexto, vinstr - 1, 1) IS NOT NULL                                                 --DBMS_LOB.SUBSTR (vtexto, vinstr - 1, 1) IS NOT NULL
                        THEN
                            insere (w_t_episodio,                                                                                                                 --rec1.t_episodio,
                                    w_episodio,                                                                                                                     --rec1.episodio,
                                    rec.data,
                                    rec.tipo,
                                    DBMS_LOB.SUBSTR (vtexto, vinstr - 1, 1),                                                                                            --rec.texto,
                                    DBMS_LOB.SUBSTR (vtexto, vinstr - 1, 1),
                                    vindex,
                                    w_utilizador,                                                                                                                 --rec1.utilizador,
                                    w_activo,                                                                                                                         --rec1.activo,
                                    w_id);
                        END IF;

                        vtexto := DBMS_LOB.SUBSTR (vtexto, LENGTH (vtexto) - (vinstr + 6), vinstr + 7);
                    END IF;
                END LOOP;
            EXCEPTION
                WHEN OTHERS
                THEN
                    pck_pe_logging.erro ('pck_enf_historico.processa_TABELA 1' || w_tipo, SQLERRM, teste);
            END;
        END LOOP;


        FOR rec3 IN processado
        LOOP
            DECLARE
                vindex   NUMBER := 0;
            BEGIN
                vindex := vindex + 1;
                --vinstr := INSTR (vtexto, '<CHR');

                insere (rec3.t_episodio,
                        rec3.episodio,
                        rec3.data,
                        rec3.tipo,
                        TRIM (LEADING CHR (10) FROM rec3.texto),                                                                                                        --rec.texto,
                        rec3.texto,
                        vindex,
                        rec3.utilizador,
                        rec3.activo,
                        rec3.id);
            END;
        END LOOP;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('pck_enf_historico.processa_TABELA 2', SQLERRM, teste);
    END;

    /**************************************************/
    -- insert_HISTORICO --------------------------------
    PROCEDURE insert_historico (i_t_episodio   IN VARCHAR2,
                                i_episodio     IN VARCHAR2,
                                i_data         IN DATE,
                                i_text         IN CLOB,
                                i_utilizador   IN VARCHAR2,
                                i_activo       IN VARCHAR2)
    IS
    BEGIN
        insert_historico (i_t_episodio   => i_t_episodio,
                          i_episodio     => i_episodio,
                          i_data         => i_data,
                          i_text         => i_text,
                          i_utilizador   => i_utilizador,
                          i_activo       => i_activo,
                          id             => 'VAZIO');
    END;

    /**************************************************/
    -- insert_HISTORICO --------------------------------
    PROCEDURE insert_historico (i_t_episodio   IN VARCHAR2,
                                i_episodio     IN VARCHAR2,
                                i_data         IN DATE,
                                i_text         IN CLOB,
                                i_utilizador   IN VARCHAR2,
                                i_activo       IN VARCHAR2,
                                id             IN VARCHAR2)
    IS
        v_modo   VARCHAR2 (10);
        v_text   CLOB;
    BEGIN
        v_modo :=
            get_modo (var_t_doente,
                      var_doente,
                      i_t_episodio,
                      i_episodio);

        INSERT /*+ APPEND */
              INTO  sdt_enf_historico (t_episodio,
                                       episodio,
                                       data,
                                       tipo,
                                       texto,
                                       utilizador,
                                       modo,
                                       activo,
                                       id)
             VALUES (i_t_episodio,
                     i_episodio,
                     i_data,
                     t_tipo,
                     TRIM (LEADING CHR (10) FROM i_text),
                     i_utilizador,
                     v_modo,
                     i_activo,
                     id);
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            pck_pe_logging.erro ('pck_enf_historico.insert_HISTORICO', SQLERRM);
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('pck_enf_historico.insert_HISTORICO', SQLERRM);
    END;

    PROCEDURE add_form_din
    IS
        CURSOR form_din
        IS
              SELECT DECODE (versao_activa, 0, 'N', 'S') activo, a.*
                FROM sd_enf_form_dinamicos a
               WHERE t_doente = var_t_doente AND doente = var_doente AND t_episodio = NVL (var_t_episodio, t_episodio) AND episodio = NVL (var_episodio, episodio)
            ORDER BY id_form,
                     id_resp,
                     instancia_resp,
                     versao_resp;
    BEGIN
        FOR reg IN form_din
        LOOP
            t_tipo := reg.id_form;
            insert_historico (i_t_episodio   => reg.t_episodio,
                              i_episodio     => reg.episodio,
                              i_data         => NVL (reg.dt_act, reg.dt_cri),
                              i_text         => reg.resumo,
                              i_utilizador   => NVL (reg.user_act, reg.user_cri),
                              i_activo       => reg.activo);
        END LOOP;
    END;

    PROCEDURE add_form_din_v1
    IS
        w_info         VARCHAR2 (100)
                           := 'T_DOENTE-' || var_t_doente || '_DOENTE-' || var_doente || '_T_EPISODIO-' || NVL (var_t_episodio, '%') || '_EPISODIO-' || NVL (var_episodio, '%');
        w_t_episodio   sd_episodio.t_episodio%TYPE;
        w_episodio     sd_episodio.episodio%TYPE;
        w_result       CLOB;

        CURSOR forms_din (i_key IN VARCHAR2)
        IS
            SELECT a.id_form,
                   a.aplicacao_context_key,
                   b.resp_xml,
                   b.user_cri,
                   b.dt_cri
              FROM fd_form_resp a, fd_form_resp_det b
             WHERE TO_CHAR (id_form) IN (SELECT tipo FROM gr_enf_historico_config) AND aplicacao_context_key LIKE i_key AND a.id_resp = b.id_resp;
    BEGIN
        FOR reg IN forms_din (w_info)
        LOOP
            t_tipo := reg.id_form;

            IF var_t_episodio IS NULL OR var_episodio IS NULL
            THEN
                SELECT SUBSTR (str2,
                                 INSTR (str2,
                                        '#',
                                        1,
                                        2)
                               + 1,
                                 INSTR (str2,
                                        '#',
                                        1,
                                        3)
                               - INSTR (str2,
                                        '#',
                                        1,
                                        2)
                               - 1)
                           t_episodio,
                       SUBSTR (str2,
                                 INSTR (str2,
                                        '#',
                                        1,
                                        3)
                               + 1,
                               LENGTH (str2))
                           episodio
                  INTO w_t_episodio, w_episodio
                  FROM (SELECT REPLACE (REPLACE (REPLACE (REPLACE (reg.aplicacao_context_key, 'T_DOENTE-', ''), '_DOENTE-', '#'), '_T_EPISODIO-', '#'), '_EPISODIO-', '#') str2
                          FROM DUAL);
            ELSE
                w_t_episodio := var_t_episodio;
                w_episodio := var_episodio;
            END IF;

            w_result := pck_enf_form_din.get_value_version (i_id_form => reg.id_form, i_xml_data => reg.resp_xml);

            insert_historico (i_t_episodio   => w_t_episodio,
                              i_episodio     => w_episodio,
                              i_data         => reg.dt_cri,
                              i_text         => DBMS_LOB.SUBSTR (w_result, 4000, 1),
                              i_utilizador   => reg.user_cri,
                              i_activo       => 'S');
        END LOOP;
    END add_form_din_v1;

    /**************************************************/
    -- add_NOTAS_ENFERMAGEM ----------------------------
    PROCEDURE add_notas_enfermagem
    IS
        CURSOR rec_ntenf
        IS
            /*SELECT t_episodio, episodio, dt_notas DATA,
                   NVL (user_act, user_cri) user_cri, notas_enf, 'S' activo
              FROM sd_enf_notas_enf
             WHERE t_doente = var_t_doente AND doente = var_doente;*/
            SELECT DISTINCT s.t_episodio,
                            s.episodio,
                            s1.data data,
                            s1.utilizador user_cri,
                            s1.notas_enf,
                            'S' activo
              FROM sd_enf_notas s, sd_enf_notas_det s1
             WHERE s.id_notas = s1.id_notas AND s.t_doente = var_t_doente AND s.doente = var_doente AND (s.episodio = NVL (var_episodio, s.episodio))
            UNION
            SELECT t_episodio,
                   episodio,
                   observacoes_data,
                   observacoes_user,
                   observacoes,
                   'S'
              FROM sd_enf_avaliacao_saida q0
             WHERE doente = var_t_doente AND doente = var_doente AND episodio = NVL (var_episodio, episodio) AND observacoes IS NOT NULL;


        CURSOR rec_ntenf_balcao
        IS
            --Alterado por Andreia Br??zida
            /*SELECT T_EPISODIO,EPISODIO,NVL(DT_ACT,DT_CRI) DATA , NVL(USER_ACT,USER_CRI) USER_CRI , NOTAS_ENF
              FROM SD_ENF_REG_URG
             WHERE TIPO_REGISTO = 'notasenf'
               AND T_DOENTE     = var_t_doente
               AND DOENTE       = var_doente*/
            SELECT t_episodio,
                   episodio,
                   NVL (dt_hora, NVL (dt_act, dt_cri)) data,
                   cod_enf user_cri,                                                                                                             --NVL(USER_ACT,USER_CRI) USER_CRI ,
                   notas_enf,
                   'S' activo
              FROM sd_enf_reg_notas_enf
             WHERE t_doente = var_t_doente AND doente = var_doente;
    BEGIN
        t_tipo := 'NOTAS';

        FOR rec IN rec_ntenf_balcao
        LOOP
            insert_historico (i_t_episodio   => rec.t_episodio,
                              i_episodio     => rec.episodio,
                              i_data         => rec.data,
                              i_text         => rec.notas_enf,
                              i_utilizador   => rec.user_cri,
                              i_activo       => rec.activo);
        END LOOP;

        FOR rec IN rec_ntenf
        LOOP
            insert_historico (i_t_episodio   => rec.t_episodio,
                              i_episodio     => rec.episodio,
                              i_data         => rec.data,
                              i_text         => rec.notas_enf,
                              i_utilizador   => rec.user_cri,
                              i_activo       => rec.activo);
        END LOOP;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            NULL;
    END;

    /**************************************************/
    -- add_PARAMETROS ----------------------------------
    PROCEDURE add_parametros
    IS
        CURSOR param_rec
        IS
            SELECT DISTINCT t_episodio,
                            episodio,
                            data,
                            descr_indic || ': ' || valor texto,
                            utilizador,
                            --'M_URG'
                            get_modo (var_t_doente,
                                      var_doente,
                                      t_episodio,
                                      episodio)
                                modo,
                            DECODE (maxi, data, 'S', 'N') activo
              FROM (SELECT t_episodio,
                           episodio,
                           cod_indic,
                           descr_indic,
                           valor,
                           data_hora data,
                           user_act utilizador,
                           MAX (data_hora) OVER (PARTITION BY cod_indic) maxi
                      FROM sdv_enf_sinais_vitais a
                     WHERE     t_doente = var_t_doente
                           AND doente = var_doente
                           AND (episodio = NVL (var_episodio, episodio))
                           AND valor IS NOT NULL
                           AND NOT EXISTS
                                   (SELECT 0
                                      FROM gr_enf_indic b
                                     WHERE a.cod_indic = b.cod_indic AND b.flg_indic_externo = 'SCORE'));
    BEGIN
        t_tipo := 'PARAMETROS';

        FOR rec IN param_rec
        LOOP
            insert_historico (i_t_episodio   => rec.t_episodio,
                              i_episodio     => rec.episodio,
                              i_data         => rec.data,
                              i_text         => rec.texto,
                              i_utilizador   => rec.utilizador,
                              i_activo       => rec.activo);
        END LOOP;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            NULL;
    END;


    /**************************************************/
    -- add_OBS_MEDICA----------------------------------
    PROCEDURE add_obs_medica
    IS
        row_pess_hosp_def    sd_pess_hosp_def%ROWTYPE;
        w_param_obs_medica   VARCHAR2 (30) := UPPER (NVL (sdf_enf_obter_param ('OBS_MEDICA_TABELA'), '%'));

        CURSOR param_obs_medica1 (i_t_pess_hosp VARCHAR2, i_n_mecan VARCHAR2)
        IS
            SELECT t1.t_doente,
                   t1.doente,
                   t1.t_episodio,
                   t1.episodio,
                   observacao t_long,
                   t1.dt_cri,
                   t1.data_epis dt_act,
                   t1.n_mecan,
                   --t1.user_cri,
                   --t1.user_act,
                   'S' activo
              FROM pc_area_priv_medico t1, sd_pess_hosp_def t2
             WHERE     t2.t_pess_hosp = 'MED'
                   AND t1.t_doente = var_t_doente
                   AND t1.doente = var_doente
                   AND t1.n_mecan = t2.n_mecan
                   AND NVL (t1.flag_estado, 'X') <> 'A'
                   AND (t1.episodio = NVL (var_episodio, t1.episodio))
                   --| COPY FROM PK_EXAME | Glintt-HS-6332
                   AND (   (t1.flag_pessoal IN ('N', 'T', 'NT'))
                        OR (    t1.flag_pessoal IN ('E', 'ET')
                            AND t1.cod_serv IN (SELECT cod_serv
                                                  FROM sd_pess_hosp_det
                                                 WHERE n_mecan = i_n_mecan))
                        OR (t1.flag_pessoal = 'S' AND t1.n_mecan = i_n_mecan))
                   AND ( (t1.flag_pessoal IN ('ET', 'NT') AND t2.t_pess_hosp = i_t_pess_hosp) OR t1.flag_pessoal NOT IN ('ET', 'NT'))                       --filtra por t_pess_hosp
                                                                                                                                     --| COPY FROM PK_EXAME | Glintt-HS-6332
    ;

        CURSOR param_obs_medica2 (
            i_t_pess_hosp    VARCHAR2,
            i_n_mecan        VARCHAR2)
        IS
            SELECT t_doente,
                   doente,
                   t_episodio,
                   episodio,
                   observ t_long,
                   t2.dt_cri,
                   t2.dt_act,
                   t2.n_mecan,
                   'S' activo
              FROM pc_epis_det t2, sd_pess_hosp_def t1
             WHERE     t1.t_pess_hosp = 'MED'
                   AND t2.t_doente = var_t_doente
                   AND t2.doente = var_doente
                   AND t1.n_mecan = t2.n_mecan
                   AND t2.flg_priv = 'N'
                   AND NVL (t2.flg_estado, 'X') <> 'A'
                   AND (t2.episodio = NVL (var_episodio, t2.episodio));
    BEGIN
        t_tipo := 'OBS_MEDICA';


        SELECT *
          INTO row_pess_hosp_def
          FROM sd_pess_hosp_def
         WHERE user_sys = tp_pck_utilizador_apl.le_util_apl;

        IF w_param_obs_medica IN ('PC_AREA_PRIV_MEDICO', '%')
        THEN
            FOR rec IN param_obs_medica1 (row_pess_hosp_def.t_pess_hosp, row_pess_hosp_def.n_mecan)
            LOOP
                insert_historico (i_t_episodio   => rec.t_episodio,
                                  i_episodio     => rec.episodio,
                                  i_data         => NVL (rec.dt_act, rec.dt_cri),
                                  i_text         => rec.t_long,
                                  i_utilizador   => convert_nmecan_to_usersys (rec.n_mecan),
                                  i_activo       => rec.activo);
            END LOOP;
        END IF;

        IF w_param_obs_medica IN ('PC_EPIS_DET', '%')
        THEN
            FOR rec IN param_obs_medica2 (row_pess_hosp_def.t_pess_hosp, row_pess_hosp_def.n_mecan)
            LOOP
                insert_historico (i_t_episodio   => rec.t_episodio,
                                  i_episodio     => rec.episodio,
                                  i_data         => NVL (rec.dt_act, rec.dt_cri),
                                  i_text         => rec.t_long,
                                  i_utilizador   => convert_nmecan_to_usersys (rec.n_mecan),
                                  i_activo       => rec.activo);
            END LOOP;
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            NULL;
    END;


    /**************************************************/
    -- add_OBS_TECNICOS----------------------------------
    PROCEDURE add_obs_tecnicos
    IS
        row_pess_hosp_def   sd_pess_hosp_def%ROWTYPE;

        CURSOR param_obs_medica1 (i_t_pess_hosp VARCHAR2, i_n_mecan VARCHAR2)
        IS
            SELECT t1.t_doente,
                   t1.doente,
                   t1.t_episodio,
                   t1.episodio,
                   observacao t_long,
                   t1.dt_cri,
                   t1.data_epis dt_act,
                   t1.n_mecan,
                   --t1.user_cri,
                   --t1.user_act,
                   'S' activo
              FROM pc_area_priv_medico t1, sd_pess_hosp_def t2
             WHERE     t2.t_pess_hosp <> 'MED'
                   AND t1.t_doente = var_t_doente
                   AND t1.doente = var_doente
                   AND t1.n_mecan = t2.n_mecan
                   AND NVL (t1.flag_estado, 'X') <> 'A'
                   AND (t1.episodio = NVL (var_episodio, t1.episodio))
                   --| COPY FROM PK_EXAME | Glintt-HS-6332
                   AND (   (t1.flag_pessoal IN ('N', 'T', 'NT'))
                        OR (    t1.flag_pessoal IN ('E', 'ET')
                            AND t1.cod_serv IN (SELECT cod_serv
                                                  FROM sd_pess_hosp_det
                                                 WHERE n_mecan = i_n_mecan))
                        OR (t1.flag_pessoal = 'S' AND t1.n_mecan = i_n_mecan))
                   AND ( (t1.flag_pessoal IN ('ET', 'NT') AND t2.t_pess_hosp = i_t_pess_hosp) OR t1.flag_pessoal NOT IN ('ET', 'NT'))                       --filtra por t_pess_hosp
                                                                                                                                     --| COPY FROM PK_EXAME | Glintt-HS-6332
    ;

        CURSOR param_obs_medica2 (
            i_t_pess_hosp    VARCHAR2,
            i_n_mecan        VARCHAR2)
        IS
            SELECT t_doente,
                   doente,
                   t_episodio,
                   episodio,
                   observ t_long,
                   t2.dt_cri,
                   t2.dt_act,
                   t2.n_mecan                                                                                                                                  /*USER_CRI,USER_ACT*/
                             ,
                   'S' activo
              FROM pc_epis_det t2, sd_pess_hosp_def t1
             WHERE     t1.t_pess_hosp <> 'MED'
                   AND t2.t_doente = var_t_doente
                   AND t2.doente = var_doente
                   AND t1.n_mecan = t2.n_mecan
                   AND t2.flg_priv = 'N'
                   AND NVL (t2.flg_estado, 'X') <> 'A'
                   AND (t2.episodio = NVL (var_episodio, t2.episodio));
    BEGIN
        t_tipo := 'OBS_TECNICOS';

        SELECT *
          INTO row_pess_hosp_def
          FROM sd_pess_hosp_def
         WHERE user_sys = tp_pck_utilizador_apl.le_util_apl;

        FOR rec IN param_obs_medica1 (row_pess_hosp_def.t_pess_hosp, row_pess_hosp_def.n_mecan)
        LOOP
            insert_historico (i_t_episodio   => rec.t_episodio,
                              i_episodio     => rec.episodio,
                              i_data         => NVL (rec.dt_act, rec.dt_cri),
                              i_text         => rec.t_long,
                              i_utilizador   => convert_nmecan_to_usersys (rec.n_mecan),
                              i_activo       => rec.activo);
        END LOOP;

        FOR rec IN param_obs_medica2 (row_pess_hosp_def.t_pess_hosp, row_pess_hosp_def.n_mecan)
        LOOP
            insert_historico (i_t_episodio   => rec.t_episodio,
                              i_episodio     => rec.episodio,
                              i_data         => NVL (rec.dt_act, rec.dt_cri),
                              i_text         => rec.t_long,
                              i_utilizador   => convert_nmecan_to_usersys (rec.n_mecan),
                              i_activo       => rec.activo);
        END LOOP;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            NULL;
    END;



    /**************************************************/
    -- add_DIAG_MED----------------------------------
    PROCEDURE add_diag_med
    IS
        w_tag_fim   VARCHAR2 (100) := traduz ('fim');

        CURSOR lista_diag_med
        IS
            SELECT /*a.T_DOENTE,a.DOENTe,a.T_EPISODIO,a.EPISODIO,e.USER_SYS,
                   a.TIPO,a.FLG_PRINC,a.COD_SERV,a.N_MECAN,a.DT_REG,
                   a.CODIGO,a.DESCR_DIAG,a.SPEC_INFO,a.ID_CID,a.DT_REG_FIM,
                   c.DESCR_SERV ESPECIALIDADE,
                   f.CODIGO,f.CODIFICACAO,F.DESCRICAO
                   */
                  a.t_episodio,
                   a.episodio,
                   TRUNC (a.dt_reg, 'MI') dt_reg,
                   e.user_sys,
                   NVL (a.descr_diag, f.descricao) || NVL2 (a.dt_reg_fim, ' (' || w_tag_fim || ':' || TO_CHAR (a.dt_reg_fim, 'dd-mm-yyyy') || ')', '') texto,
                   'S' activo
              FROM pc_cid_episodio a,
                   sd_serv c,
                   sd_pess_hosp_def e,
                   ana_cid_diag f
             WHERE     a.t_doente = var_t_doente
                   AND a.doente = var_doente
                   AND a.tipo = 'DIAG'
                   AND a.n_mecan = e.n_mecan
                   AND a.cod_serv = c.cod_serv
                   AND a.codificacao = f.codificacao
                   AND a.codigo = f.codigo
                   AND NVL (a.estado, '.') <> 'A'
                   AND (a.episodio = NVL (var_episodio, a.episodio));
    BEGIN
        t_tipo := 'DIAG_MED';


        FOR rec IN lista_diag_med
        LOOP
            insert_historico (i_t_episodio   => rec.t_episodio,
                              i_episodio     => rec.episodio,
                              i_data         => rec.dt_reg,
                              i_text         => rec.texto,
                              i_utilizador   => rec.user_sys,
                              i_activo       => rec.activo);
        END LOOP;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            NULL;
    END;

    /**************************************************/
    -- add_TERAPEURICA_ADM -----------------------------
    PROCEDURE add_terapeurica_adm
    IS
        debug_info   VARCHAR2 (20);
        w_tag_dose   VARCHAR2 (100) := traduz ('Dose');
        w_tag_obs    VARCHAR2 (100) := traduz ('Obs');
    BEGIN
        t_tipo := 'TERAPEUTICA_ADM';

        INSERT /*+ APPEND */ INTO sdt_enf_historico (tipo,t_episodio,episodio,data,texto,utilizador,modo)
        SELECT 'TERAPEUTICA_ADM'
              ,a.T_EPISODIO
              ,a.EPISODIO
              ,a.HORA_ADM
              ,COALESCE(b.NOME_COMERCIAL,a.NOME_MED_ADM,b.NOME_CIENT)
               ||' ( '||w_TAG_DOSE
               || TO_CHAR (a.DOSE,'FM99999999990D999999')
               || ' '
               || a.UNID_MED
               || ' )'
               || DECODE(a.OBS_ADM,'','',w_TAG_OBS||' ['||a.OBS_ADM||']')
              ,a.ENF
              ,PCK_ENF_HISTORICO.get_modo(var_T_DOENTE,var_DOENTE,a.T_EPISODIO,a.EPISODIO)
          FROM PH_MED_ADM       a
              ,PH_MEDICAMENTOS  b
              ,(
                SELECT DISTINCT d.ID_ACT
                  FROM PH_PRESCRICOES   c
                      ,PH_MED_PRES      d
                 WHERE c.PRESCRICAO     = d.PRESCRICAO
                   AND c.T_DOENTE       = var_T_DOENTE
                   AND c.DOENTE         = var_DOENTE
                   AND d.OBS_MED IS NOT NULL
               ) e
         WHERE a.T_DOENTE       = var_T_DOENTE
           AND a.DOENTE         = var_DOENTE
           AND a.MEDICAMENTO    = b.MEDICAMENTO (+)
           AND a.ID_ACT         = e.ID_ACT      (+);

    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            NULL;
    END;

    /**************************************************/
    -- add_TERAPEURICA_N_ADM ---------------------------
    PROCEDURE add_terapeurica_n_adm
    IS
        CURSOR terap_n_adm
        IS
            SELECT DISTINCT t3.t_episodio,
                            t3.episodio,
                            t1.medicamento,
                            t1.desc_justif,
                            TO_DATE (TO_CHAR (t1.data, 'YYYY-MM-DD') || TO_CHAR (t1.hora, 'HH24:MI'), 'YYYY-MM-DD HH24:MI') data,
                            t1.resp utilizador,
                               'Obs: '
                            || (SELECT t4.obs_med
                                  FROM ph_med_pres t4
                                 WHERE t1.prescricao = t4.prescricao AND t1.id_act = t4.id_act AND t1.medicamento = t4.medicamento)
                                obs_med,
                            NVL (t2.nome_comercial, t2.nome_cient) nome_cient,
                            'S' activo
              FROM ph_med_n_adm t1, ph_medicamentos t2, ph_prescricoes t3
             WHERE     t1.medicamento = t2.medicamento(+)
                   AND t1.t_doente = var_t_doente
                   AND t1.doente = var_doente
                   AND t1.prescricao = t3.prescricao
                   AND t3.t_doente = var_t_doente
                   AND t3.doente = var_doente;

        debug_info   VARCHAR2 (20);
    BEGIN
        t_tipo := 'TERAPEUTICA_N_ADM';

        FOR rec IN terap_n_adm
        LOOP
            insert_historico (i_t_episodio   => rec.t_episodio,
                              i_episodio     => rec.episodio,
                              i_data         => rec.data,
                              i_text         => rec.nome_cient || ' ( ' || rec.desc_justif || ' )' || CHR (13) || CHR (10) || rec.obs_med,
                              i_utilizador   => rec.utilizador,
                              i_activo       => rec.activo);
        END LOOP;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            NULL;
    END;

    /**************************************************/
    -- add_INTERV_ENFERMAGEM ---------------------------
    PROCEDURE add_interv_enfermagem
    IS
        w_tag_freq         VARCHAR2 (100) := traduz ('Frequ�ncia');
        w_tag_horario      VARCHAR2 (100) := traduz ('Hor�rio');
        w_tag_termino      VARCHAR2 (100) := traduz ('T�rmino �s');
        w_tag_ultima       VARCHAR2 (100) := traduz ('�ltima altera��o por');
        w_tag_data_ini     VARCHAR2 (100) := traduz ('In�cio em');

        w_t_episodio_ant   sd_episodio.t_episodio%TYPE := NULL;
        w_episodio_ant     sd_episodio.episodio%TYPE := NULL;
        w_data_ant         DATE := NULL;
        w_utilizador       sdt_enf_historico.utilizador%TYPE := NULL;
        w_activo_ant       sdt_enf_historico.activo%TYPE := NULL;
        w_separador        VARCHAR2 (10);
        w_descr_interv     VARCHAR2 (8000) := NULL;--|HVRedonda-2506 (edgar.parada) aumento do tamanho para carregar as interven��es
                                                        -- com descr demasiado grandes
        w_caract           VARCHAR2 (4000) := NULL;


        CURSOR interv_enfermagem
        IS
              SELECT DISTINCT
                     t_episodio,
                     episodio,
                     t1.id_agrupador,
                     t1.cod_freq,
                     t1.horario,
                     t1.user_act,
                     dt_ini,
                     dt_fim,
                     t1.user_cri,
                     t1.dt_cri,
                     DECODE (dt_fim, NULL, 'N', 'S') activo,
                     (SELECT w_const_interv_herdado
                        FROM sd_enf_val_plano_cuid a, sd_enf_val_plano_cuid_det b
                       WHERE     t1.t_doente = a.t_doente
                             AND t1.doente = a.doente
                             AND a.id_plano = b.id_plano
                             AND b.tipo_reg = 'INTERV'
                             AND t1.id_agrupador = b.id_reg
                             AND b.aceitou = 'S'
                             AND ROWNUM = 1
                      UNION
                      SELECT w_const_interv_herdado
                        FROM sd_enf_val_plano_cuid a, sd_enf_val_plano_cuid_det b, sd_enf_plano_cuid_interv_det t12
                       WHERE     t1.t_doente = a.t_doente
                             AND t1.doente = a.doente
                             AND a.id_plano = b.id_plano
                             AND t12.id_agrupador = t1.id_agrupador
                             AND b.tipo_reg IN ('DIAG', 'AT')
                             AND DECODE (b.tipo_reg, 'DIAG', t12.id_diag, t12.id_at_terap) = b.id_reg
                             AND b.aceitou = 'S'
                             AND ROWNUM = 1)
                         heranca
                FROM sd_enf_plano_cuid_interv t1
               WHERE     t_doente = var_t_doente
                     AND doente = var_doente
                     AND t1.episodio = NVL (var_episodio, t1.episodio)
                     AND t1.check_interv = 'S'
                     AND NOT EXISTS
                             (SELECT 0
                                FROM sd_enf_interv_indicador t10, gr_enf_indic t2
                               WHERE t1.cod_interv = t10.cod_interv AND  t2.cod_indic = t10.cod_indic AND t2.flg_indic_externo = 'SCORE')
            ORDER BY dt_cri,
                     user_cri,
                     t_episodio,
                     episodio,
                     activo;
    BEGIN
        t_tipo := 'INTERV_ENFERMAGEM';

        FOR rec IN interv_enfermagem
        LOOP
            IF w_t_episodio_ant != rec.t_episodio OR w_episodio_ant != rec.episodio OR w_data_ant != rec.dt_cri OR w_utilizador != rec.user_cri OR w_activo_ant != rec.activo
            THEN
                insert_historico (i_t_episodio   => w_t_episodio_ant,
                                  i_episodio     => w_episodio_ant,
                                  i_data         => w_data_ant,
                                  i_text         => w_descr_interv,
                                  i_utilizador   => w_utilizador,
                                  i_activo       => w_activo_ant);
                w_descr_interv := NULL;
                w_separador := NULL;
            ELSIF w_descr_interv IS NOT NULL
            THEN
                w_separador := CHR (10) || CHR (10);
            ELSE
                w_separador := NULL;
            END IF;

            w_descr_interv := w_descr_interv || w_separador || pckt_enf_plano_cuidados.get_descr_interv (rec.id_agrupador);

            IF rec.heranca IS NOT NULL
            THEN
                w_descr_interv := w_descr_interv || ' [' || traduz (rec.heranca) || ']';
            END IF;

            w_descr_interv := w_descr_interv || CHR (10) || ' - ' || w_tag_freq || ':' || rec.cod_freq || ' - ' || w_tag_horario || ':' || rec.horario;

            w_descr_interv := w_descr_interv || ' - ' || w_tag_data_ini || ' ' || TO_CHAR (rec.dt_ini, 'YYYY-MM-DD HH24:MI');

            -- Glintt-HS-5943
            IF rec.dt_fim IS NOT NULL
            THEN
                w_descr_interv := w_descr_interv || ' - ' || w_tag_termino || ' ' || TO_CHAR (rec.dt_fim, 'YYYY-MM-DD HH24:MI');
            END IF;

            IF rec.user_cri != NVL (rec.user_act, rec.user_cri)
            THEN
                w_descr_interv := w_descr_interv || ' - ' || w_tag_ultima || ' <' || rec.user_act || ' - ' || sdf_urg_obtem_pess_hosp_nome (rec.user_act) || '>';
            END IF;

            -- Glintt-HS-5943

            --HVREDONDA-24
            w_caract :=
                pck_enf_caract.get_caracterizacao (rec.id_agrupador,
                                                   '(',
                                                   ')',
                                                   TRIM (': '),
                                                   ' ',
                                                   '<CHR10>   ');

            IF w_caract IS NOT NULL
            THEN
                w_descr_interv := w_descr_interv || w_caract;
            END IF;

            --HVREDONDA-24

            w_t_episodio_ant := rec.t_episodio;
            w_episodio_ant := rec.episodio;
            w_data_ant := rec.dt_cri;
            w_utilizador := rec.user_cri;
            w_activo_ant := rec.activo;
        END LOOP;

        IF w_descr_interv IS NOT NULL
        THEN
            insert_historico (i_t_episodio   => w_t_episodio_ant,
                              i_episodio     => w_episodio_ant,
                              i_data         => w_data_ant,
                              i_text         => w_descr_interv,
                              i_utilizador   => w_utilizador,
                              i_activo       => w_activo_ant);
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            NULL;
    END;

    /**************************************************/
    -- add_ACTOS_ENFERMAGEM ----------------------------
    PROCEDURE add_actos_enfermagem
    IS
        CURSOR actos_enfermagem
        IS
            SELECT DISTINCT t1.t_episodio,
                            t1.episodio,
                            t1.cod_item,
                            t2.descr_rubr,
                            t1.data_reg,
                            t1.user_cri utilizador,
                            'S' activo
              FROM sd_enf_reg_urg t1, fa_rubr t2
             WHERE     t1.tipo_registo = 'Proc'
                   AND t1.cod_item = to_char(t2.cod_rubr)
                   AND t1.t_doente = var_t_doente
                   AND t1.doente = var_doente
                   AND t1.episodio = NVL (var_episodio, t1.episodio);
    BEGIN
        t_tipo := 'ACTOS_ENFERMAGEM';

        FOR rec IN actos_enfermagem
        LOOP
            insert_historico (i_t_episodio   => rec.t_episodio,
                              i_episodio     => rec.episodio,
                              i_data         => rec.data_reg,
                              i_text         => rec.descr_rubr,
                              i_utilizador   => rec.utilizador,
                              i_activo       => rec.activo);
        END LOOP;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            NULL;
    END;

    /**************************************************/
    -- add_ENSINAMENTOS --------------------------------
    PROCEDURE add_ensinamentos
    IS
        CURSOR ensinamentos
        IS
            SELECT DISTINCT t1.t_episodio,
                            t1.episodio,
                            t1.data_hora,
                            t1.user_cri,
                            t2.nome_ensinamento,
                            'S' activo
              FROM sd_enf_reg_ensinamentos t1, gr_enf_ensinamentos t2
             WHERE t1.codigo = t2.codigo AND t1.t_doente = var_t_doente AND t1.doente = var_doente AND t1.episodio = NVL (var_episodio, t1.episodio);
    BEGIN
        t_tipo := 'ENSINAMENTOS';

        FOR rec IN ensinamentos
        LOOP
            insert_historico (i_t_episodio   => rec.t_episodio,
                              i_episodio     => rec.episodio,
                              i_data         => rec.data_hora,
                              i_text         => rec.nome_ensinamento,
                              i_utilizador   => rec.user_cri,
                              i_activo       => rec.activo);
        END LOOP;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            NULL;
    END;

    /**************************************************/
    -- add_ENF_DIAG ------------------------------------

    PROCEDURE add_enf_diag
    IS
        v_text             CLOB;
        w_codigo           VARCHAR2 (100);
        w_eixo             VARCHAR2 (1);

        w_tag_dt_fim       VARCHAR2 (100) := traduz ('Data Fim');
        w_tag_demons       VARCHAR2 (100) := traduz ('Demonstrado');
        w_tag_n_demons     VARCHAR2 (100) := traduz ('N�o demonstrado');
        w_tag_n_aplic      VARCHAR2 (100) := traduz ('N�o aplic�vel');
        w_tag_data_ini     VARCHAR2 (100) := traduz ('Data In�cio');

        w_t_episodio_ant   sd_episodio.t_episodio%TYPE := NULL;
        w_episodio_ant     sd_episodio.episodio%TYPE := NULL;
        w_data_ant         DATE := NULL;
        w_utilizador       sdt_enf_historico.utilizador%TYPE := NULL;
        w_activo_ant       sdt_enf_historico.activo%TYPE := NULL;
        w_separador        VARCHAR2 (10);

        CURSOR ensinamentos
        IS
              SELECT DISTINCT
                     t1.t_episodio,
                     t1.episodio,
                     t1.cod_standard,
                     t1.eixo_a,
                     t1.eixo_b,
                     t1.eixo_g,
                     t1.dt_ini,
                     t1.dt_cri,
                     DECODE (dt_fim, NULL, NULL, '    ' || w_tag_dt_fim || ': ' || TO_CHAR (dt_fim, 'DD-MM-YYYY hh24:mi')) dt_fim,
                     t1.user_cri,
                     DECODE (dt_fim, NULL, 'S', 'N') activo,
                     id,
                     (SELECT w_const_diag_herdado
                        FROM sd_enf_val_plano_cuid a, sd_enf_val_plano_cuid_det b
                       WHERE     t1.t_doente = a.t_doente
                             AND t1.doente = a.doente
                             AND a.id_plano = b.id_plano
                             AND b.tipo_reg = 'DIAG'
                             AND t1.id = b.id_reg
                             AND b.aceitou = 'S'
                             AND ROWNUM = 1)
                         heranca
                FROM sd_enf_diagnostico t1
               WHERE t1.t_doente = var_t_doente AND t1.doente = var_doente AND t1.episodio = NVL (var_episodio, t1.episodio)
            ORDER BY t_episodio,
                     episodio,
                     dt_cri,
                     user_cri,
                     activo;

        CURSOR qualifs (
            v_t_doente    VARCHAR2,
            v_doente      VARCHAR2,
            v_codigo      VARCHAR2,
            v_eixo        VARCHAR2)
        IS
              SELECT    q1.descr_qualificador
                     || DECODE (pck_enf_qualificadores.demonstra_qualificador (t_doente, doente, q0.cod_qualificador),
                                'S', ', ' || w_tag_demons,
                                'N', ', ' || w_tag_n_demons,
                                ', ' || w_tag_n_aplic)
                     || ' ('
                     || TO_CHAR (q2.data_estado, 'dd-mm-yyyy hh24:mi')
                     || ' ; '
                     || q2.user_cri
                     || ')'
                         descr
                FROM sd_enf_qualificador q0, gr_enf_qualificador q1, sd_enf_qualificador_det q2
               WHERE     q0.t_doente = v_t_doente
                     AND q0.doente = v_doente
                     AND q0.cod_qualificador = q1.cod_qualificador
                     AND q0.cod_qualificador IN (SELECT cod_qualificador
                                                   FROM gr_enf_qualificador_diag
                                                  WHERE codigo = v_codigo AND eixo = v_eixo)
                     AND q0.id = q2.id_master
                     AND q2.id = (SELECT MAX (id)
                                    FROM sd_enf_qualificador_det q3
                                   WHERE q3.id_master = q0.id)
            ORDER BY q1.cod_qualificador;

        FUNCTION tem_qualifs (v_t_doente    VARCHAR2,
                              v_doente      VARCHAR2,
                              v_codigo      VARCHAR2,
                              v_eixo        VARCHAR2)
            RETURN NUMBER
        IS
            n   NUMBER;
        BEGIN
            SELECT COUNT (*)
              INTO n
              FROM sd_enf_qualificador q0, gr_enf_qualificador q1
             WHERE     q0.t_doente = v_t_doente
                   AND q0.doente = v_doente
                   AND q0.cod_qualificador = q1.cod_qualificador
                   AND q0.cod_qualificador IN (SELECT cod_qualificador
                                                 FROM gr_enf_qualificador_diag
                                                WHERE codigo = v_codigo AND eixo = v_eixo);

            RETURN n;
        END tem_qualifs;
    BEGIN
        t_tipo := 'ENF_DIAG';
        w_separador := NULL;

        FOR rec IN ensinamentos
        LOOP
            IF w_t_episodio_ant != rec.t_episodio OR w_episodio_ant != rec.episodio OR w_data_ant != rec.dt_cri OR w_utilizador != rec.user_cri OR w_activo_ant != rec.activo
            THEN
                insert_historico (i_t_episodio   => w_t_episodio_ant,
                                  i_episodio     => w_episodio_ant,
                                  i_data         => w_data_ant,
                                  i_text         => v_text,
                                  i_utilizador   => w_utilizador,
                                  i_activo       => w_activo_ant);
                v_text := NULL;
                w_separador := NULL;
            ELSIF v_text IS NOT NULL
            THEN
                w_separador := CHR (10) || CHR (10);
            ELSE
                w_separador := NULL;
            END IF;

            v_text := v_text || w_separador || RTRIM (pckt_enf_diagnosticos.get_descr_fenom (rec.id), ',');

            IF rec.heranca IS NOT NULL
            THEN
                v_text := v_text || ' [' || traduz (rec.heranca) || ']';
            END IF;

            v_text := v_text || CHR (10) || ' ' || w_tag_data_ini || ': ' || TO_CHAR (rec.dt_ini, 'DD-MM-YYYY hh24:mi') || rec.dt_fim;

            SELECT DECODE (rec.eixo_b, NULL, rec.eixo_g, rec.eixo_b), DECODE (rec.eixo_b, NULL, 'G', 'B')
              INTO w_codigo, w_eixo
              FROM DUAL;

            IF tem_qualifs (var_t_doente,
                            var_doente,
                            w_codigo,
                            w_eixo) > 0
            THEN
                v_text := v_text || CHR (10) || ' ' || sdf_enf_obter_param_det ('TEM_QUALIFICADORES', 'DESCRICAO_ARVORE');
            END IF;


            FOR r IN qualifs (var_t_doente,
                              var_doente,
                              w_codigo,
                              w_eixo)
            LOOP
                v_text := v_text || CHR (10) || ' - ' || r.descr;
            END LOOP;

            w_t_episodio_ant := rec.t_episodio;
            w_episodio_ant := rec.episodio;
            w_data_ant := rec.dt_cri;
            w_utilizador := rec.user_cri;
            w_activo_ant := rec.activo;
        END LOOP;

        IF v_text IS NOT NULL
        THEN
            insert_historico (i_t_episodio   => w_t_episodio_ant,
                              i_episodio     => w_episodio_ant,
                              i_data         => w_data_ant,
                              i_text         => v_text,
                              i_utilizador   => w_utilizador,
                              i_activo       => w_activo_ant);
        END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            NULL;
    END;

    /**************************************************/
    -- add_TRIAGEM -------------------------------------
    PROCEDURE add_triagem
    IS
        CURSOR rec_triagem
        IS
            SELECT DISTINCT k0.t_doente,
                            k0.doente,
                            k0.t_episodio,
                            k0.episodio,
                            k0.user_cri,
                            k0.dt_registo,
                            k2.descr_prioridade,
                            'S' activo
              FROM sd_enf_tr_prioridade k0,
                   (SELECT t_doente,
                           doente,
                           t_episodio,
                           episodio,
                           (SELECT MAX (n_triag)
                              FROM sd_enf_tr_prioridade t2
                             WHERE     t1.t_doente = t2.t_doente
                                   AND t1.doente = t2.doente
                                   AND t1.t_episodio = t2.t_episodio
                                   AND t1.episodio = t2.episodio
                                   AND (cod_prioridade IS NOT NULL OR destino IS NOT NULL))
                               n_triag
                      FROM sdv_urg_urgencia t1, sd_serv t3
                     WHERE t_doente = var_t_doente AND doente = var_doente AND t1.cod_serv = t3.cod_serv AND t3.serv_urg = 'S') k1,
                   gr_enf_tr_prioridades k2
             WHERE     k0.t_doente = k1.t_doente
                   AND k0.doente = k1.doente
                   AND k0.t_episodio = k1.t_episodio
                   AND k0.episodio = k1.episodio
                   AND k0.n_triag = k1.n_triag
                   AND k0.cod_prioridade = k2.cod_prioridade;

        v_text   CLOB;
    BEGIN
        t_tipo := 'TRIAGEM';

        FOR rec IN rec_triagem
        LOOP
            v_text :=
                REPLACE (sdf_enf_tr_ultimo_resultado (rec.t_doente,
                                                      rec.doente,
                                                      rec.t_episodio,
                                                      rec.episodio,
                                                      'N'),
                         CHR (10),
                         ';');
            insert_historico (i_t_episodio   => rec.t_episodio,
                              i_episodio     => rec.episodio,
                              i_data         => rec.dt_registo,
                              i_text         => UPPER (rec.descr_prioridade) || '; ' || v_text,
                              i_utilizador   => rec.user_cri ,
                              i_activo       => rec.activo);
        END LOOP;
    /*
    EXCEPTION
      WHEN NO_DATA_FOUND
        THEN NULL;
      WHEN OTHERS
        THEN null;
        */
    END;

    /**************************************************/
    -- add_MCDT ----------------------------------------
    PROCEDURE add_mcdt
    IS
        CURSOR rec_mcdt
        IS
            SELECT DISTINCT t1.t_doente,
                            t1.doente,
                            t1.t_episodio_pai,
                            t1.episodio_pai,
                            t1.dt_pedida,
                            t1.inf_clinica,
                            t2.cod_exame,
                            t2.qt_exame,
                            t2.flg_estado,
                            t2.dt_cri,
                            t2.dt_act,
                            t3.descr_rubr,
                            t2.user_cri,
                            t2.user_act,
                            'S' activo
              FROM sd_req_exames t1, sd_req_exames_det t2, fa_rubr t3
             WHERE t1.n_req_exame = t2.n_req_exame AND t2.cod_exame = to_char(t3.cod_rubr) AND t1.t_doente = var_t_doente AND t1.doente = var_doente;

        v_aux    VARCHAR2 (40);
        v_util   VARCHAR2 (30);
    BEGIN
        t_tipo := 'MCDT';

        FOR rec IN rec_mcdt
        LOOP
            IF (rec.flg_estado IS NULL OR rec.flg_estado = 'P')
            THEN
                v_aux := traduz ('Requisitado');
            ELSIF (rec.flg_estado = 'AS')
            THEN
                v_aux := traduz ('Anulado Servi�o');
            ELSIF (rec.flg_estado = 'AM')
            THEN
                v_aux := traduz ('Anulado M�dico');
            ELSIF (rec.flg_estado = 'I')
            THEN
                v_aux := traduz ('Iniciado');
            ELSIF (rec.flg_estado = 'D')
            THEN
                v_aux := traduz ('Dispon�vel');
            ELSIF (rec.flg_estado = 'DR')
            THEN
                v_aux := traduz ('Dispon�vel c/ relat�rio');
            ELSIF (rec.flg_estado = 'C')
            THEN
                v_aux := traduz ('Colhido');
            ELSIF (rec.flg_estado = 'EC')
            THEN
                v_aux := traduz ('Em curso');
            END IF;

            v_util := NVL (rec.user_act, rec.user_cri);

            IF (v_util LIKE '%HL7%')
            THEN
                v_util := NVL (sdf_enf_obter_param ('UTILIZADOR_INTERFACE'), 'SERV_EXEC');
            END IF;

            insert_historico (i_t_episodio   => rec.t_episodio_pai,
                              i_episodio     => rec.episodio_pai,
                              i_data         => NVL (rec.dt_act, rec.dt_cri),
                              i_text         => rec.descr_rubr || ' (' || v_aux || ')',
                              i_utilizador   => v_util,
                              i_activo       => rec.activo);
        END LOOP;
    /*
    EXCEPTION
      WHEN NO_DATA_FOUND
        THEN NULL;
      WHEN OTHERS
        THEN null;
        */
    END;

    /**************************************************/
    -- add_ESCALAS -------------------------------------
    PROCEDURE add_escalas
    IS
        CURSOR reg_escalas
        IS
            SELECT DISTINCT t1.t_episodio,
                            t1.episodio,
                            --t1.cod_check,
                            t1.aval_seq,
                            --t2.cod_item cod_interv,
                            --t2.dt_check,
                            --t2.hr_item,
                            NVL (t2.dt_act, t2.dt_cri) dt_cri,
                            NVL (t2.user_act, t2.user_cri) user_cri,
                            t3.valor,
                            sdf_enf_descr_interv_by_presc (t2.cod_item, t2.prescricao) descr_interv                                                                              --,
              --pck_enf_avaliacoes.get_score_descr (t2.cod_item, t3.valor)
              --    score1,
              --t3.valor score
              FROM sd_enf_avaliacoes t1, sd_enf_check_list t2, sd_enf_check_indic t3
             WHERE t1.t_doente = var_t_doente AND t1.doente = var_doente AND t1.cod_check = t2.cod_check AND t1.cod_check = t3.cod_check AND t3.valor IS NOT NULL
            UNION ALL
            SELECT DISTINCT t2.t_episodio,
                            t2.episodio,
                            --t1.cod_check,
                            NULL aval_seq,
                            --t2.cod_item cod_interv,
                            --t2.dt_check,
                            --t2.hr_item,
                            NVL (t2.dt_act, t2.dt_cri) dt_cri,
                            NVL (t2.user_act, t2.user_cri) user_cri,
                            t3.valor,
                            sdf_enf_descr_interv_by_presc (t2.cod_item, t2.prescricao) descr_interv                                                                              --,
              --pck_enf_avaliacoes.get_score_descr (t2.cod_item, t3.valor)
              --    score1,
              --t3.valor score
              FROM sd_enf_check_list t2, sd_enf_check_indic t3
             WHERE     t2.t_doente = var_t_doente
                   AND t2.doente = var_doente
                   AND t2.cod_check = t3.cod_check
                   AND t3.valor IS NOT NULL
                   AND EXISTS
                           (SELECT 0
                              FROM gr_enf_indic t4
                             WHERE t3.cod_indic = t4.cod_indic AND t4.flg_indic_externo = 'SCORE')
            ORDER BY dt_cri, user_cri;

        CURSOR reg_escalas_det (i_aval_seq      NUMBER,
                                i_t_doente      VARCHAR2,
                                i_doente        VARCHAR2,
                                i_t_episodio    VARCHAR2,
                                i_episodio      VARCHAR2)
        IS
              SELECT t2.pergunta,
                     pck_enf_avaliacoes.obtem_descr_resposta (t1.cod_resposta, t1.cod_pergunta) resposta,
                     pck_enf_avaliacoes.obtem_score_pergunta (t1.aval_seq,
                                                              t2.cod_pergunta,
                                                              i_t_doente,
                                                              i_doente,
                                                              i_t_episodio,
                                                              i_episodio)
                         pontuacao
                FROM sd_enf_avaliacoes_det t1, gr_enf_aval_questoes t2                                                                                                           --,
               --gr_enf_aval_respostas t3
               WHERE aval_seq = i_aval_seq AND t1.cod_pergunta = t2.cod_pergunta
            --   AND t1.cod_resposta = t3.cod_resposta
            ORDER BY t2.ordem;

        full_d             CLOB;

        w_t_episodio_ant   sdt_enf_historico.t_episodio%TYPE := NULL;
        w_episodio_ant     sdt_enf_historico.episodio%TYPE := NULL;
        w_data_ant         DATE := NULL;
        w_utilizador       sdt_enf_historico.utilizador%TYPE := NULL;
        w_separador        VARCHAR2 (10) := NULL;
    BEGIN
        t_tipo := 'ESCALAS';

        FOR reg IN reg_escalas
        LOOP
            IF w_t_episodio_ant != reg.t_episodio OR w_episodio_ant != reg.episodio OR w_data_ant != reg.dt_cri OR w_utilizador != reg.user_cri
            THEN
                insert_historico (i_t_episodio   => w_t_episodio_ant,
                                  i_episodio     => w_episodio_ant,
                                  i_data         => w_data_ant,
                                  i_text         => full_d,
                                  i_utilizador   => w_utilizador,
                                  i_activo       => 'S');
                full_d := NULL;
                w_separador := NULL;
            ELSIF full_d IS NOT NULL
            THEN
                w_separador := CHR (10) || CHR (10);
            ELSE
                w_separador := NULL;
            END IF;

            full_d := full_d || w_separador || reg.descr_interv || ' [' || reg.valor                                                                     --                || ' -> '
                                                                                     --                || reg.score
                      || ']';

            IF reg.aval_seq IS NOT NULL
            THEN
                FOR reg_det IN reg_escalas_det (reg.aval_seq,
                                                var_t_doente,
                                                var_doente,
                                                reg.t_episodio,
                                                reg.episodio)
                LOOP
                                        IF reg_det.pontuacao IS NOT NULL THEN
                                                full_d := full_d || CHR (10) || CHR (9) || reg_det.pergunta || ' => ' || reg_det.resposta || ' [' || reg_det.pontuacao || ']';
                                        END IF;
                END LOOP;
            END IF;

            w_t_episodio_ant := reg.t_episodio;
            w_episodio_ant := reg.episodio;
            w_data_ant := reg.dt_cri;
            w_utilizador := reg.user_cri;
        END LOOP;

        IF full_d IS NOT NULL
        THEN
            insert_historico (i_t_episodio   => w_t_episodio_ant,
                              i_episodio     => w_episodio_ant,
                              i_data         => w_data_ant,
                              i_text         => full_d,
                              i_utilizador   => w_utilizador,
                              i_activo       => 'S');
        END IF;
    END;

    /**************************************************/
    -- add_FERIDAS -------------------------------------
    PROCEDURE add_feridas
    IS
        CURSOR rec_inter
        IS
            SELECT cod_interv, titulo FROM gr_enf_feridas;

        CURSOR rec_feridas (
            i_cod_interv   IN gr_enf_feridas.cod_interv%TYPE)
        IS
            SELECT DISTINCT t_doente,
                            doente,
                            t_episodio,
                            episodio,
                            s.dt_cri,
                            s.dt_act,
                            s.user_act,
                            s.user_cri,
                            pck_enf_feridas.resumo_dados (a.n_ferida) titulo,
                            'S' activo,
                            a.n_ferida,
                            dt_inicio,
                            dt_fim,
                            a.localizacao,
                            s.data_hora
              FROM sd_enf_feridas a, sd_enf_feridas_registo s, gr_enf_feridas g
             WHERE     t_doente = var_t_doente
                   AND doente = var_doente
                   AND a.cod_interv = i_cod_interv
                   AND a.cod_interv = g.cod_interv
                   AND a.n_ferida = s.n_ferida
                   AND a.episodio = NVL (var_episodio, a.episodio);

        CURSOR rec_registos (i_n_ferida IN sd_enf_feridas.n_ferida%TYPE, i_data_hora IN sd_enf_feridas_registo.data_hora%TYPE)
        IS
            SELECT DBMS_LOB.SUBSTR (pck_enf_feridas.resumo (g.cod_ep_arv_reg, s.n_registo), 32000, 1) texto, s.data_hora --pck_enf_feridas.resumo (g.cod_ep_arv_reg, s.n_registo) texto
              FROM sd_enf_feridas_registo s, sd_enf_feridas s2, gr_enf_feridas g
             WHERE s.n_ferida = s2.n_ferida AND s2.cod_interv = g.cod_interv AND s2.n_ferida = i_n_ferida AND s.data_hora = i_data_hora;

        v_util         VARCHAR2 (30);
        w_cod_interv   gr_enf_feridas.cod_interv%TYPE;
        w_tipo         VARCHAR2 (150);
        w_ferida       VARCHAR2 (100);
        w_insere       CLOB;
        w_data_hora    DATE;
    BEGIN
        --CURSOR DE INTERVE??ES
        FOR rec1 IN rec_inter
        LOOP
            t_tipo := rec1.cod_interv;
            w_cod_interv := rec1.cod_interv;
            w_insere := NULL;

            FOR rec IN rec_feridas (i_cod_interv => w_cod_interv)
            LOOP
                w_insere := NULL;
                v_util := NVL (rec.user_act, rec.user_cri);
                w_ferida := rec.n_ferida;



                w_data_hora := rec.data_hora;

                FOR rec2 IN rec_registos (i_n_ferida => w_ferida, i_data_hora => w_data_hora)
                LOOP
                    /*   if (rec.dt_fim is not null) then
                           if(rec.localizacao is not null) then
                               w_insere := w_insere ||'Data Inicio: '|| rec.dt_inicio|| '   Data Fim: '||rec.dt_fim|| '   Localizacao: '|| rec.localizacao|| chr(10)|| rec2.texto ;
                           else
                               w_insere := w_insere ||'Data Inicio: '|| rec.dt_inicio|| '   Data Fim: '||rec.dt_fim|| chr(10)|| rec2.texto ;
                           end if;
                           w_insere := w_insere ||'Data Inicio: '|| rec.dt_inicio|| '   Data Fim: '||rec.dt_fim|| chr(10)|| rec2.texto ;
                       else
                           if(rec.localizacao is not null) then
                               w_insere := w_insere ||'Data Inicio: '|| rec.dt_inicio||'   Localizacao: '|| rec.localizacao|| chr(10)|| rec2.texto ;
                           else
                               w_insere := w_insere ||'Data Inicio: '|| rec.dt_inicio|| chr(10)|| rec2.texto ;
                           end if;
                       end if;*/
                    w_insere := w_insere || rec2.texto || CHR (10);
                END LOOP;

                w_insere := RTRIM (LTRIM (w_insere, CHR (10)), CHR (10));

                IF w_insere != EMPTY_CLOB
                THEN
                    w_insere := rec.titulo || CHR (10) || w_insere;
                END IF;


                insert_historico (i_t_episodio   => rec.t_episodio,
                                  i_episodio     => rec.episodio,
                                  i_data         => w_data_hora,                                                                                                  --NVL (rec.dt_act,
                                  --rec.dt_cri),
                                  i_text         => LTRIM (w_insere, CHR (10)),
                                  i_utilizador   => v_util,
                                  i_activo       => rec.activo);
            END LOOP;
        END LOOP;
    END;
	
	/**************************************************/
    -- add_diet -------------------------------------
    PROCEDURE add_diet
    IS
        v_text varchar2(4000);
        v_text_interv varchar2(4000);
        v_text_diag varchar2(4000);

        v_score_total number;        -- JMS-244521: nova variavel
        v_idade_anos  varchar2(100); -- JMS-244521: nova variavel
        v_idade_aux   number;        -- JMS-244521: nova variavel
    BEGIN
        t_tipo := 'DIET';

        -- JMS-244521 : Obter idade do doente
        BEGIN
            select sdf_enf_calcula_idade_doe(var_t_doente, var_doente)
            into v_idade_anos
            from dual;
        EXCEPTION
            WHEN OTHERS THEN
                v_idade_anos := null;
        END;
        IF v_idade_anos IS NOT NULL
        THEN
            v_idade_anos := substr(v_idade_anos, 1, instr(v_idade_anos, ' '));
        END IF;
        -- !!
		
        $IF PCK_ENF_MODULOS_ATIVOS.DIET
        $THEN
            DECLARE
                v_diet_data diet.dietk_estado_nutricional.t_rastreio_doente;
            BEGIN
                v_diet_data := diet.dietk_interface_ext.comunicar_dados_nutricional(I_T_DOENTE=>var_t_doente, I_DOENTE=>var_doente);

                --VERIFICAR SE RECEBEU ALGUM DADO
                IF V_DIET_DATA.COUNT > 0 THEN
                    FOR i IN v_diet_data.FIRST .. v_diet_data.LAST
                    LOOP

                        --escalas adulto
                        --VERIFICAR SE TEM ALGUMA ESCALA ADULTO
                        IF v_diet_data(i).rastreio.COUNT > 0 THEN
                            FOR k IN v_diet_data(i).rastreio.FIRST .. v_diet_data(i).rastreio.LAST
                            LOOP

                                -- JMS-244521: Se a idade do doente � >= 70 anos => somar 1 ao valor do score total; caso contr�rio, n�o fazer nada;
                                v_score_total := v_diet_data(i).rastreio(k).estado_nutricional + v_diet_data(i).rastreio(k).severidade_doenca;
                                IF to_number(v_idade_anos) >= 70 THEN
                                    v_score_total := v_score_total + 1;
                                    v_idade_aux := 1;
                                END IF;
                                --!!
								
                                v_text := v_text || chr(10) || chr(10) || chr(10) || '-----Escala Risco Nutricional do Adulto ('||v_diet_data(i).rastreio(k).USER_CRI || ', '||TO_CHAR(v_diet_data(i).rastreio(k).DT_CRI,'DD-MM-YYYY HH24:MI:SS') || ' ['||traduz ('score total')||': '|| v_score_total ||')-----'; -- JMS-244521 : Incluir informa��o 'V_SCORE_TOTAL'.
                                v_text := v_text || chr(10) || 'IMC <20.5    -> ' || v_diet_data(i).rastreio(k).imc_menor20;
                                v_text := v_text || chr(10) || 'Utente perdeu peso nos �ltimos 3 meses    -> ' || v_diet_data(i).rastreio(k).perde_peso3m;
                                v_text := v_text || chr(10) || 'Utente reduziu a sua ingest�o alimentar na �ltima semana    -> ' || v_diet_data(i).rastreio(k).reduz_ing_alimsem;
                                v_text := v_text || chr(10) || 'Utente est� severamente doente (ex: Internado UCI)    -> ' || v_diet_data(i).rastreio(k).severamente_doente;
                                v_text := v_text || chr(10) || '        Deteriora��o do estado nutricional    -> ' || v_diet_data(i).rastreio(k).estado_nutricional;
                                v_text := v_text || chr(10) || '        Gravidade de doenca ( aumento nas necessidades)    -> ' || v_diet_data(i).rastreio(k).severidade_doenca;
								v_text := v_text || chr(10) || '        Idade >= 70 anos    -> ' || NVL(v_idade_aux, 0); -- JMS-244521 : Incluir

                                --INTERVENCOES ADULTO
                                IF v_diet_data(i).rastreio(k).intervencoes.COUNT > 0 THEN
                                    for l IN v_diet_data(i).rastreio(k).intervencoes.FIRST .. v_diet_data(i).rastreio(k).intervencoes.LAST
                                    LOOP
                                        if v_text_interv is null then v_text_interv := 'Interven��es'; end if;
                                        v_text_interv := v_text_interv || chr(10) || '  ' || v_diet_data(i).rastreio(k).intervencoes(l).descr_interv || '   (' || v_diet_data(i).rastreio(k).intervencoes(l).user_cri || ', ' ||  v_diet_data(i).rastreio(k).intervencoes(l).dt_cri || ')';
                                    END LOOP;
                                END IF;
                                --DIAGS ADULTO
                                IF v_diet_data(i).rastreio(k).diagnosticos.COUNT > 0 THEN
                                    for l IN v_diet_data(i).rastreio(k).diagnosticos.FIRST .. v_diet_data(i).rastreio(k).diagnosticos.LAST
                                    LOOP
                                        if v_text_diag is null then v_text_diag := 'Diagn�sticos'; end if;
                                        v_text_diag := v_text_diag || chr(10) || '  ' || v_diet_data(i).rastreio(k).diagnosticos(l).descr_diag || '   (' || v_diet_data(i).rastreio(k).diagnosticos(l).user_cri || ', ' ||  v_diet_data(i).rastreio(k).diagnosticos(l).dt_cri || ')';
                                    END LOOP;
                                END IF;

                                v_text := v_text || chr(10) || v_text_interv || chr(10) || v_text_diag;

                                insert_historico(i_t_episodio=>v_diet_data(i).rastreio(k).t_episodio,
                                                    i_episodio=>v_diet_data(i).rastreio(k).episodio,
                                                    i_data=>v_diet_data(i).rastreio(k).DIA,
                                                    i_text=>v_text,
                                                    i_utilizador=>v_diet_data(i).rastreio(k).USER_CRI,
                                                    i_activo=>'S');
                                v_text := null;
                                v_text_interv := null;
                                v_text_diag := null;
                            END LOOP;
                        END IF;

                        --escalas crianca
                        --VERIFICAR SE TEM ALGUMA ESCALA PEDIATRICA
                        IF v_diet_data(i).rastreio_ped.COUNT > 0 THEN
                            FOR k IN v_diet_data(i).rastreio_ped.FIRST .. v_diet_data(i).rastreio_ped.LAST
                            LOOP

                                v_text := v_text || chr(10) || chr(10) || chr(10) || '-----Escala StrongKids ('||v_diet_data(i).rastreio_ped(k).USER_CRI || ', '||TO_CHAR(v_diet_data(i).rastreio_ped(k).DT_CRI,'DD-MM-YYYY HH24:MI:SS') || ' ['||traduz ('score total')||': '|| v_diet_data(i).rastreio_ped(k).SCORE ||')-----'; -- JMS-244521 : Incluir informa��o 'SCORE'.
                                v_text := v_text || chr(10) || 'Existe alguma patologia subjacente que contribua para o risco de desnutri��o (ver lista*) ou � esperada alguma cirurgia maior?    -> ' || v_diet_data(i).rastreio_ped(k).op_1;
                                v_text := v_text || chr(10) || 'O doente apresenta um estado nutricional deficit�rio, quando avaliado de uma forma subjetiva?    -> ' || v_diet_data(i).rastreio_ped(k).op_2;
                                v_text := v_text || chr(10) || 'Estao presentes alguns dos seguintes itens: Diarreia ('|| unistr('\2265') ||' 5 vezes/dia) e/ou vomitos (>3 vezes/dia). Reducao da ingestao alimentar nos ultimos dias. Intervencao nutricional previa. Ingestao insuficiente devido a dor    -> ' || v_diet_data(i).rastreio_ped(k).op_3;
                                v_text := v_text || chr(10) || 'Ocorreu perda de peso ou aus�ncia de ganho de peso (crian�as < 1 ano) durante as �ltimas semanas/meses?    -> ' || v_diet_data(i).rastreio_ped(k).op_4;

                                --INTERVENCOES CRIANCA
                                IF v_diet_data(i).rastreio_ped(k).intervencoes.COUNT > 0 THEN
                                    for l IN v_diet_data(i).rastreio_ped(k).intervencoes.FIRST .. v_diet_data(i).rastreio_ped(k).intervencoes.LAST
                                    LOOP
                                        if v_text_interv is null then v_text_interv := 'Interven��es'; end if;
                                        v_text_interv := v_text_interv || chr(10) || '  ' || v_diet_data(i).rastreio_ped(k).intervencoes(l).descr_interv || '   (' || v_diet_data(i).rastreio_ped(k).intervencoes(l).user_cri || ', ' ||  v_diet_data(i).rastreio_ped(k).intervencoes(l).dt_cri || ')';
                                    END LOOP;
                                END IF;

                                --DIAGS ADULTO
                                IF v_diet_data(i).rastreio_ped(k).diagnosticos.COUNT > 0 THEN
                                    for l IN v_diet_data(i).rastreio_ped(k).diagnosticos.FIRST .. v_diet_data(i).rastreio_ped(k).diagnosticos.LAST
                                    LOOP
                                        if v_text_diag is null then v_text_diag := 'Diagn�sticos'; end if;
                                        v_text_diag := v_text_diag || chr(10) || '  ' || v_diet_data(i).rastreio_ped(k).diagnosticos(l).descr_diag || '   (' || v_diet_data(i).rastreio_ped(k).diagnosticos(l).user_cri || ', ' ||  v_diet_data(i).rastreio_ped(k).diagnosticos(l).dt_cri || ')';
                                    END LOOP;
                                END IF;

                                v_text := v_text || chr(10) || v_text_interv || chr(10) || v_text_diag;

                                insert_historico(i_t_episodio=>v_diet_data(i).rastreio_ped(k).t_episodio,
                                                    i_episodio=>v_diet_data(i).rastreio_ped(k).episodio,
                                                    i_data=>v_diet_data(i).rastreio_ped(k).DIA,
                                                    i_text=>v_text,
                                                    i_utilizador=>v_diet_data(i).rastreio_ped(k).USER_CRI,
                                                    i_activo=>'S');
                                v_text := null;
                                v_text_interv := null;
                                v_text_diag := null;
                            END LOOP;
                        END IF;
                    END LOOP;
                END IF;
            END;
        $END

    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('pck_enf_historico.add_diet',SQLERRM);
    END;

    /**************************************************/
    -- add_TESTE_COMBUR -------------------------------------
    PROCEDURE add_combur
    IS
        p_cod_item     NUMBER;
        p_valor        NUMBER;
        v_text         CLOB;
        p_obs          VARCHAR2 (4000);
        p_data_obs     DATE;
        p_user_act     VARCHAR2 (30);
        p_user_cri     VARCHAR2 (30);
        p_dt_act       DATE;
        p_dt_cri       DATE;
        p_t_episodio   VARCHAR2 (30);
        p_episodio     VARCHAR2 (30);
        w_tag_obs      VARCHAR2 (100) := traduz ('Obs: ');

        CURSOR rec_combur
        IS
            SELECT t1.obs,
                   t1.user_act,
                   t1.user_cri,
                   t1.t_episodio,
                   t1.episodio,
                   t1.dt_act,
                   t1.dt_cri,
                   t1.id_combur,
                   t1.dt_hr_registo
              FROM sd_enf_combur t1
             WHERE t1.t_doente = var_t_doente AND t1.doente = var_doente;

        --  AND t1.episodio = NVL (var_episodio, t1.episodio)
        -- AND t1.t_episodio = var_t_episodio;


        CURSOR rec_det (i_id_combur NUMBER)
        IS
              SELECT descr_item, t3.cod_item, valor
                FROM gr_enf_combur t2, sd_enf_combur_det t3
               WHERE t2.cod_item = t3.cod_item AND i_id_combur = id_combur
            ORDER BY cod_item DESC;
    BEGIN
        t_tipo := 'TESTE_COMBUR';

        FOR rec IN rec_combur
        LOOP
            p_data_obs := rec.dt_hr_registo;
            v_text := '';
            v_text := TO_CHAR (p_data_obs, 'DD-MM-YYYY HH24:MI') || '  ' || NVL (rec.user_act, rec.user_cri);

            FOR rec_d IN rec_det (rec.id_combur)
            LOOP
                v_text := v_text || CHR (10) || rec_d.descr_item || ' -> ' || NVL (converte_combur (rec_d.valor), 'n/a');
            --          || '('||rec_d.valor||')';

            END LOOP;

            v_text := v_text || CHR (10) || w_tag_obs || ' : ' || NVL (rec.obs, ' ');

            insert_historico (i_t_episodio   => rec.t_episodio,
                              i_episodio     => rec.episodio,
                              i_data         => p_data_obs,
                              i_text         => LTRIM (v_text, CHR (10)),
                              i_utilizador   => NVL (rec.user_act, rec.user_cri),
                              i_activo       => 'S');
        END LOOP;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            pck_pe_logging.erro (SQLERRM);
        WHEN OTHERS
        THEN
            NULL;
    END;

    /**************************************************/
    -- add_GLICEMIAS -------------------------------------
    PROCEDURE add_glicemias
    IS
        CURSOR reg_glicemias
        IS
            /*
                SELECT t1.t_episodio, t1.episodio,t5.descr_interv,t1.glicemia,t2.descr_tipo_ins,t1.unid_insulina,t3.descr_loc_admin,t1.obs, t1.hr_registo, t1.user_cri, t1.user_act, t4.id_agrupador
                FROM sd_enf_aval_glicemia t1,
                    gr_enf_tipo_insulina t2,
                    gr_enf_local_admin t3,
                    sd_enf_check_list t4,
                    gr_enf_interv t5
                WHERE t1.frgn_cod_tipo_ins = t2.cod_tipo_ins
                    and t1.frgn_cod_loc_admin = t3.cod_loc_admin
                    and t1.cod_check = t4.cod_check
                    and t4.cod_item = t5.cod_interv
            */
            SELECT DISTINCT t1.t_episodio,
                            t1.episodio,
                            t5.descr_interv,
                            t1.glicemia,
                            DECODE (t1.frgn_cod_tipo_ins, NULL, NULL, t2.descr_tipo_ins) descr_tipo_ins,
                            t1.unid_insulina,
                            DECODE (t1.frgn_cod_loc_admin, NULL, NULL, t3.descr_loc_admin) descr_loc_admin,
                            t1.obs,
                            t1.hr_registo,
                            t1.user_cri,
                            t1.user_act,
                            t4.id_agrupador
              FROM sd_enf_aval_glicemia t1,
                   gr_enf_tipo_insulina t2,
                   gr_enf_local_admin t3,
                   sd_enf_check_list t4,
                   gr_enf_interv t5
             WHERE     (t1.frgn_cod_tipo_ins = t2.cod_tipo_ins OR t1.frgn_cod_tipo_ins IS NULL)
                   AND (t1.frgn_cod_loc_admin = t3.cod_loc_admin OR t1.frgn_cod_loc_admin IS NULL)
                   AND t1.cod_check = t4.cod_check
                   AND t4.cod_item = t5.cod_interv
                   AND t1.t_doente = var_t_doente
                   AND t1.doente = var_doente;

        v_text   CLOB;
    BEGIN
        t_tipo := 'GLICEMIAS';

        FOR reg IN reg_glicemias
        LOOP
            v_text :=                           --pckt_enf_plano_cuidados.get_descr_interv(reg.id_agrupador)||CHR(10)--'Monitorizar ('||reg.descr_interv||') no indiv??duo'||CHR(10)
                     'Glicemia: ' || reg.glicemia;

            /*
            ||'; Tipo: '||reg.descr_tipo_ins
            ||'; Unidades: '||reg.unid_insulina
            ||'; Local de Administra??o: '||reg.descr_loc_admin;
            */
            IF reg.descr_tipo_ins IS NOT NULL
            THEN
                v_text := v_text || '; Tipo: ' || reg.descr_tipo_ins;
            END IF;

            IF reg.unid_insulina IS NOT NULL
            THEN
                v_text := v_text || '; Unidades: ' || reg.unid_insulina;
            END IF;

            IF reg.descr_loc_admin IS NOT NULL
            THEN
                v_text := v_text || '; Local de Administra��o: ' || reg.descr_loc_admin;
            END IF;

            IF reg.obs IS NOT NULL
            THEN
                v_text := v_text || '; Observa��es: ' || reg.obs;
            END IF;

            insert_historico (i_t_episodio   => reg.t_episodio,
                              i_episodio     => reg.episodio,
                              i_data         => reg.hr_registo,
                              i_text         => v_text,
                              i_utilizador   => NVL (reg.user_act, reg.user_cri),
                              i_activo       => 'S');
        END LOOP;
    END;

    /**************************************************/
    -- add_VIGILANCIAS -------------------------------------
    PROCEDURE add_vigilancias
    IS
    BEGIN

        INSERT /*+ APPEND */ INTO sdt_enf_historico
        ( t_episodio,episodio,data,tipo,texto,utilizador,activo,id,modo )
        SELECT t1.T_EPISODIO
              ,t1.EPISODIO
              ,t1.HR_REGISTO
              ,'VIGILANCIAS'
              ,t1.RESUMO
              ,nvl(t1.USER_ACT,t1.USER_CRI)
              ,'S'
              ,t1.NUM_VIGIL
              ,PCK_ENF_HISTORICO.get_Modo(var_T_DOENTE,var_DOENTE,t1.T_EPISODIO,t1.EPISODIO)
          FROM sd_enf_vigil t1
         WHERE t1.t_doente = var_t_doente AND t1.doente = var_doente;

    END;

    -- add_VIGILANCIAS -------------------------------------
    PROCEDURE add_vigilancias_dif
    IS
        CURSOR pi1
        IS
            SELECT t1.num_vigil,
                   t1.t_episodio,
                   t1.episodio,
                   t1.hr_registo,
                   t1.resumo,
                   t1.obs,
                   t1.id_agrupador,
                   t1.user_cri,
                   t1.user_act
              FROM sd_enf_vigil t1
             WHERE t1.t_doente = var_t_doente AND t1.doente = var_doente;

        CURSOR pi2 (
            var_num_vigil    VARCHAR2)
        IS
              SELECT t1.descricao,
                     t3.valor_bd,
                     t3.cod_item,
                     t1.tipo,
                     t2.descr_item,
                     t4.dt_registo,
                     t4.hr_registo,
                     t3.num_vigil
                FROM gr_enf_vigil_det t1,
                     gr_enf_qdr_espec_item t2,
                     sd_enf_vigil_det t3,
                     sd_enf_vigil t4
               WHERE     t3.num_vigil = t4.num_vigil
                     AND t4.num_vigil = var_num_vigil
                     AND t3.num_vigil = var_num_vigil
                     AND t3.cod_vigil_det = t1.cod_vigil_det
                     AND t3.cod_vigil = t1.cod_vigil
                     AND t1.cod_vigil = t2.cod_qdr
                     AND t1.cod_item = t2.cod_item
                     AND t2.cod_item = t3.cod_item
                     AND t1.visible = 'S'
                     AND (t1.tipo = 'LISTA' OR t1.tipo = 'TEXTO' OR (t1.tipo = 'CHECK' AND valor_bd = 'S'))
                     AND t4.t_doente = var_t_doente
                     AND t4.doente = var_doente
            ORDER BY dt_registo,
                     TO_DATE (TO_CHAR (dt_registo, 'dd-mm-yyyy ') || TO_CHAR (hr_registo, 'hh24:mi'), 'dd-mm-yyyy hh24:mi') ASC,
                     t2.ordem ASC,
                     t1.ordem ASC;

        cod_item         NUMBER;
        v_text           CLOB;
        dummy_cod_item   VARCHAR2 (50) := '314159265358979323846264338327950288';
        dummy_temp       NUMBER;
    BEGIN
        t_tipo := 'VIGILANCIAS';

        FOR k1 IN pi1
        LOOP
            cod_item := dummy_cod_item;
            v_text := pckt_enf_plano_cuidados.get_descr_interv (k1.id_agrupador) || CHR (10);
            dummy_temp := 0;

            FOR k2 IN pi2 (k1.num_vigil)
            LOOP
                -- poe . ou ;
                IF cod_item <> k2.cod_item AND cod_item <> dummy_cod_item
                THEN
                    v_text := v_text || '.';
                ELSE
                    IF (k2.tipo = 'LISTA' OR k2.tipo = 'TEXTO' OR (k2.tipo = 'CHECK' AND k2.valor_bd = 'S')) AND cod_item <> dummy_cod_item
                    THEN
                        v_text := v_text || '; ';
                    END IF;
                END IF;

                -- poe o titulo
                IF cod_item <> k2.cod_item
                THEN
                    IF cod_item <> dummy_cod_item
                    THEN
                        v_text := v_text || CHR (10) || CHR (10);
                    END IF;

                    v_text := v_text || '   ' || k2.descr_item || ': ';
                    cod_item := k2.cod_item;
                END IF;

                -- constroi o texto
                IF k2.tipo = 'LISTA' OR k2.tipo = 'TEXTO'
                THEN
                    v_text := v_text || k2.descricao || ': ' || k2.valor_bd;
                    dummy_temp := 1;
                ELSIF k2.tipo = 'CHECK' AND k2.valor_bd = 'S'
                THEN
                    v_text := v_text || k2.descricao;
                    dummy_temp := 1;
                END IF;
            END LOOP;

            IF dummy_temp = 1
            THEN
                v_text := v_text || '.';
            END IF;

            IF k1.obs IS NOT NULL
            THEN
                IF dummy_temp = 1
                THEN
                    v_text := v_text || CHR (10) || '   ' || traduz ('Observa��es') || ': ' || k1.obs;
                ELSE
                    v_text := v_text || '   ' || traduz ('Observa��es') || ': ' || k1.obs;
                END IF;
            END IF;

            insert_historico (i_t_episodio   => k1.t_episodio,
                              i_episodio     => k1.episodio,
                              i_data         => k1.hr_registo,
                              i_text         => v_text,
                              i_utilizador   => NVL (k1.user_act, k1.user_cri),
                              i_activo       => 'S',
                              id             => k1.num_vigil);
        END LOOP;
    END;

    ---------##########################
    FUNCTION converte_combur (i_valor IN NUMBER)
        RETURN VARCHAR2
    IS
        i     NUMBER := 1;
        res   VARCHAR2 (14) := '';
    BEGIN
        IF i_valor <> 0
        THEN
            FOR i IN 1 .. ABS (i_valor)
            LOOP
                IF i_valor > 0
                THEN
                    res := res || ' +';
                ELSE
                    res := res || ' -';
                END IF;
            END LOOP;
        END IF;

        RETURN res;
    END;


    /**************************************************/
    -- add_intervencoes_realizadas -------------------------------------
    PROCEDURE add_intervencoes_realizadas
    IS
        CURSOR rec_checkl
        IS
            SELECT t_episodio,
                   episodio,
                   TO_DATE (TO_CHAR (dt_item, 'yyyymmdd') || ' ' || hr_item, 'yyyymmdd hh24:mi') data,
                   NVL (user_act, user_cri) utilizador,
                   NVL (pckt_enf_plano_cuidados.get_descr_interv (NVL (id_agrupador,
                                                                       pckt_enf_plano_cuidados.obtem_id_agrupador (
                                                                           t_doente,
                                                                           doente,
                                                                           t_episodio,
                                                                           episodio,
                                                                           cod_item,
                                                                           tabela,
                                                                           TO_DATE (TO_CHAR (dt_item, 'yyyymmdd') || ' ' || hr_item, 'yyyymmdd hh24:mi'),
                                                                           TO_DATE (TO_CHAR (dt_item, 'yyyymmdd') || ' ' || hr_item, 'yyyymmdd hh24:mi') + INTERVAL '1' HOUR))),
                        (SELECT descr_interv
                           FROM gr_enf_interv w0
                          WHERE w0.cod_interv = q0.cod_item))
                       descr_interv,
                   'S' activo
              FROM sd_enf_check_list q0
             WHERE     t_doente = var_t_doente
                   AND doente = var_doente
                   AND (episodio = NVL (var_episodio, episodio))
                   AND flg_estado = 'S'
                   AND NOT EXISTS
                           (SELECT 0
                              FROM sd_enf_check_indic q1, gr_enf_indic q2
                             WHERE q0.cod_check = q1.cod_check AND q1.cod_indic = q2.cod_indic AND q2.flg_indic_externo = 'SCORE');
    BEGIN
        t_tipo := 'INTERV_REALIZADA';

        FOR rec IN rec_checkl
        LOOP
            insert_historico (i_t_episodio   => rec.t_episodio,
                              i_episodio     => rec.episodio,
                              i_data         => rec.data,                                                                                  --TO_DATE (rec.data, 'yyyymmdd hh24:mi'),
                              i_text         => rec.descr_interv,
                              i_utilizador   => rec.utilizador,
                              i_activo       => rec.activo);
        END LOOP;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            NULL;
    END;


    /**************************************************/
    -- add_atos_medicos ----------------------------------------
    PROCEDURE add_mtos_medicos
    IS
        CURSOR rec_atos_med
        IS
            SELECT b.dt_act_med dt_execucao,
                   a.cod_exame,
                   c.descr_rubr descr_rubr,
                   t_doente,
                   doente,
                   t_episodio,
                   episodio,
                   (SELECT user_sys
                      FROM sd_pess_hosp_def
                     WHERE n_mecan = a.n_mecan)
                       user_sys
              FROM sd_episod_exame a, sd_episod_act_med b, fa_rubr c
             WHERE a.n_act_med = b.n_act_med AND a.cod_exame = to_char(c.cod_rubr) AND a.cod_serv_exec = '9' AND t_doente = var_t_doente AND doente = var_doente;

        v_aux    VARCHAR2 (40);
        v_util   VARCHAR2 (30);
    BEGIN
        t_tipo := 'ATOS_MED';

        FOR rec_atos IN rec_atos_med
        LOOP
            insert_historico (i_t_episodio   => rec_atos.t_episodio,
                              i_episodio     => rec_atos.episodio,
                              i_data         => rec_atos.dt_execucao,
                              i_text         => rec_atos.descr_rubr,
                              i_utilizador   => rec_atos.user_sys,
                              i_activo       => 'S');
        END LOOP;
    /*
    EXCEPTION
      WHEN NO_DATA_FOUND
        THEN NULL;
      WHEN OTHERS
        THEN null;
        */
    END;

    ----------------------------------------------------
    PROCEDURE gera_historico (i_t_doente IN VARCHAR2, i_doente IN VARCHAR2)
    IS
    BEGIN
        gera_historico (i_t_doente,
                        i_doente,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL);
    END;

    ----------------------------------------------------
    PROCEDURE gera_historico (i_t_doente     IN VARCHAR2,
                              i_doente       IN VARCHAR2,
                              i_episodio     IN VARCHAR2,
                              i_t_episodio   IN VARCHAR2)
    IS
    BEGIN
        gera_historico (i_t_doente,
                        i_doente,
                        NULL,
                        NULL,
                        i_episodio,
                        i_t_episodio,
                        NULL,
                        NULL);
    END;

    ----------------------------------------------------
    PROCEDURE gera_historico (i_t_doente     IN VARCHAR2,
                              i_doente       IN VARCHAR2,
                              i_dt_inicio    IN DATE,
                              i_dt_fim       IN DATE,
                              i_episodio     IN VARCHAR2,
                              i_t_episodio   IN VARCHAR2,
                              i_activo       IN VARCHAR2)
    IS
    BEGIN
        gera_historico (i_t_doente,
                        i_doente,
                        i_dt_inicio,
                        i_dt_fim,
                        i_episodio,
                        i_t_episodio,
                        i_activo,
                        NULL);
    END;

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
    PROCEDURE fast_SCAN
    (
        i_t_doente     IN VARCHAR2,
        i_doente       IN VARCHAR2
    )
    IS
        v_COUNT NUMBER;
    BEGIN

        var_t_doente    := i_t_doente;
        var_doente      := i_doente;

        EXECUTE IMMEDIATE 'TRUNCATE TABLE SDT_ENF_HISTORICO_STATUS';

        --PCK_ENF_HISTORICO.GERA_HISTORICO | Assim j� so processo o que o utilizador pode ver
        FOR T IN
        (
            SELECT TIPO,ORDEM
              FROM
            (
                SELECT TIPO,ORDEM
                  FROM GR_ENF_HISTORICO_CONFIG q0
                 WHERE (    NOT EXISTS ( SELECT 1 FROM GR_ENF_HISTORICO_CONFIG_DET q1 WHERE q0.TIPO = q1.TIPO )
                         OR     EXISTS ( SELECT 1 FROM GR_ENF_HISTORICO_CONFIG_DET q1 WHERE q0.TIPO = q1.TIPO AND q1.T_PESS_HOSP = 'ENF'/*get_t_pess_hosp*/ )
                       )
                   AND is_number(TIPO) = 'FALSE'

                UNION ALL

                SELECT 'FERIDAS' TIPO,98
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1
                                  FROM GR_ENF_FERIDAS           q0
                                      ,GR_ENF_HISTORICO_CONFIG  q1
                                 WHERE q0.COD_INTERV = q1.TIPO
                              )

                UNION ALL

                SELECT 'FORMS_DIN' TIPO,99
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1
                                  FROM FD_FORM                  q0
                                      ,GR_ENF_HISTORICO_CONFIG  q1
                                 WHERE q0.ID_FORM||'' = q1.TIPO
                              )
            )
             ORDER BY ORDEM
        )
        LOOP

            IF T.TIPO = 'TESTE_COMBUR'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_ENF_COMBUR
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
            ELSIF T.TIPO = 'ACTOS_ENFERMAGEM'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_ENF_REG_URG
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
            ELSIF T.TIPO = 'ENF_DIAG'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_ENF_DIAGNOSTICO
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
            ELSIF T.TIPO = 'ENSINAMENTOS'
            THEN
                    SELECT count(1)
                      INTO v_COUNT
                      FROM DUAL
                     WHERE EXISTS ( SELECT 1 FROM SD_ENF_REG_ENSINAMENTOS
                                     WHERE T_DOENTE = var_t_doente
                                       AND DOENTE   = var_doente
                                  );
            ELSIF T.TIPO = 'INTERV_ENFERMAGEM'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_ENF_PLANO_CUID_INTERV t1
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                                   AND CHECK_INTERV  = 'S'
                                   AND NOT EXISTS
                                         (SELECT 0
                                            FROM sd_enf_interv_indicador t10, gr_enf_indic t2
                                           WHERE t1.cod_interv = t10.cod_interv AND  t2.cod_indic = t10.cod_indic  AND  t2.flg_indic_externo = 'SCORE')
                              );

            ELSIF T.TIPO = 'NOTAS'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_ENF_NOTAS
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
                IF v_COUNT = 0
                THEN
                    SELECT count(1)
                      INTO v_COUNT
                      FROM DUAL
                     WHERE EXISTS ( SELECT 1 FROM SD_ENF_AVALIACAO_SAIDA
                                     WHERE T_DOENTE = var_t_doente
                                       AND DOENTE   = var_doente
                                       AND OBSERVACOES IS NOT NULL
                                  );
                END IF;
            ELSIF T.TIPO = 'PARAMETROS'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SDV_ENF_SINAIS_VITAIS
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
            ELSIF T.TIPO = 'TERAPEUTICA_ADM'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM PH_MED_ADM
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
            ELSIF T.TIPO = 'TERAPEUTICA_N_ADM'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM PH_MED_N_ADM
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
            ELSIF T.TIPO = 'TRIAGEM'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_ENF_TR_PRIORIDADE
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
            ELSIF T.TIPO = 'MCDT'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_REQ_EXAMES
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
            ELSIF T.TIPO = 'ATOS_MED'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_EPISOD_ACT_MED
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
            ELSIF T.TIPO = 'OBS_MEDICA'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM PC_AREA_PRIV_MEDICO   t1
                                             ,SD_PESS_HOSP_DEF      t2
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                                   AND nvl(t1.FLAG_ESTADO,'X') <> 'A'
                                   AND t1.N_MECAN       = t2.N_MECAN
                                   AND t2.T_PESS_HOSP   = 'MED'
                              );
                IF v_COUNT = 0
                THEN
                    SELECT count(1)
                      INTO v_COUNT
                      FROM DUAL
                     WHERE EXISTS ( SELECT 1 FROM PC_EPIS_DET       t1
                                                 ,SD_PESS_HOSP_DEF  t2
                                     WHERE T_DOENTE = var_t_doente
                                       AND DOENTE   = var_doente
                                       AND nvl(t1.FLG_ESTADO,'X') <> 'A'
                                       AND t1.N_MECAN       = t2.N_MECAN
                                       AND t2.T_PESS_HOSP   = 'MED'
                                  );
                END IF;
            ELSIF T.TIPO = 'OBS_TECNICOS'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM PC_AREA_PRIV_MEDICO   t1
                                             ,SD_PESS_HOSP_DEF      t2
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                                   AND nvl(t1.FLAG_ESTADO,'X') <> 'A'
                                   AND t1.N_MECAN       = t2.N_MECAN
                                   AND t2.T_PESS_HOSP  <> 'MED'
                              );
                IF v_COUNT = 0
                THEN
                    SELECT count(1)
                      INTO v_COUNT
                      FROM DUAL
                     WHERE EXISTS ( SELECT 1 FROM PC_EPIS_DET       t1
                                                 ,SD_PESS_HOSP_DEF  t2
                                     WHERE T_DOENTE = var_t_doente
                                       AND DOENTE   = var_doente
                                       AND nvl(t1.FLG_ESTADO,'X') <> 'A'
                                       AND t1.N_MECAN       = t2.N_MECAN
                                       AND t2.T_PESS_HOSP  <> 'MED'
                                  );
                END IF;
            ELSIF T.TIPO = 'ESCALAS'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_ENF_AVALIACOES
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
                IF v_COUNT = 0
                THEN
                    SELECT count(1)
                      INTO v_COUNT
                      FROM DUAL
                     WHERE EXISTS ( SELECT 1 FROM SDV_ENF_SINAIS_VITAIS t1
                                                 ,GR_ENF_INDIC t2
                                     WHERE T_DOENTE = var_t_doente
                                       AND DOENTE   = var_doente
                                       AND t1.COD_INDIC         = t2.COD_INDIC
                                       AND t2.FLG_INDIC_EXTERNO = 'SCORE'
                                  );
                END IF;
            ELSIF T.TIPO = 'DIAG_MED'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM PC_CID_EPISODIO
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                                   AND TIPO     = 'DIAG'
                                   AND NVL(ESTADO,'.') <> 'A'
                              );
            ELSIF T.TIPO = 'INTERV_REALIZADA'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_ENF_CHECK_LIST q0
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                                   AND FLG_ESTADO   = 'S'
                                   AND NOT EXISTS
                                       (SELECT 0
                                          FROM sd_enf_check_indic q1, gr_enf_indic q2
                                         WHERE q0.cod_check = q1.cod_check AND q1.cod_indic = q2.cod_indic AND q2.flg_indic_externo = 'SCORE')
                              );
            ELSIF T.TIPO = 'GLICEMIAS'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_ENF_AVAL_GLICEMIA
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
            ELSIF T.TIPO = 'VIGILANCIAS'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_ENF_VIGIL
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
            ELSIF T.TIPO = 'FERIDAS'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_ENF_FERIDAS q1
                                             ,SD_ENF_FERIDAS_REGISTO q2
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                                   AND q1.N_FERIDA = q2.N_FERIDA
                              );
            ELSIF T.TIPO = 'FORMS_DIN'
            THEN
                SELECT count(1)
                  INTO v_COUNT
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1 FROM SD_ENF_FORM_DINAMICOS
                                 WHERE T_DOENTE = var_t_doente
                                   AND DOENTE   = var_doente
                              );
            ELSE
                v_COUNT := -1;
            END IF;

            IF v_COUNT >= 0
            THEN
                INSERT INTO SDT_ENF_HISTORICO_STATUS(TIPO,COM_DADOS,JA_PROCESSADO)
                VALUES( T.TIPO,v_COUNT,'N');
            END IF;

        END LOOP;

        COMMIT;

    END;

    /**************************************************/
    -- gera_HISTORICO ( MAIN ) -------------------------
    PROCEDURE gera_historico (i_t_doente     IN VARCHAR2,
                              i_doente       IN VARCHAR2,
                              i_dt_inicio    IN DATE,
                              i_dt_fim       IN DATE,
                              i_episodio     IN VARCHAR2,
                              i_t_episodio   IN VARCHAR2,
                              i_activo       IN VARCHAR2,
                              i_vigil        IN VARCHAR2,
                              i_MENU_LEFT    IN VARCHAR2
                              )
    IS
        aux_COUNT NUMBER;
    BEGIN

        --| VERIFICAR SE O DOENTE � O MESMO. SE N�O FOR FAZ LOGO O TRUNCATE
        IF (NVL(VAR_T_DOENTE,'XcV') <> NVL(I_T_DOENTE,'XcB') OR
            NVL(VAR_DOENTE,'XcV') <> NVL(I_DOENTE,'XcB'))
        THEN
            EXECUTE IMMEDIATE 'TRUNCATE TABLE SDT_ENF_HISTORICO_STATUS';
            EXECUTE IMMEDIATE 'TRUNCATE TABLE SDT_ENF_HISTORICO';
            EXECUTE IMMEDIATE 'TRUNCATE TABLE SDT_ENF_HISTORICO_2';
        ELSE

            DECLARE
                v_COUNT NUMBER;
            BEGIN
                SELECT COUNT(1)
                  INTO v_COUNT
                  FROM SDT_ENF_HISTORICO_STATUS
                 WHERE JA_PROCESSADO = 'S';

                IF v_COUNT = 0
                THEN
                    EXECUTE IMMEDIATE 'TRUNCATE TABLE SDT_ENF_HISTORICO';
                    EXECUTE IMMEDIATE 'TRUNCATE TABLE SDT_ENF_HISTORICO_2';
                END IF;
            END;
        END IF;

        var_t_doente    := i_t_doente;
        var_doente      := i_doente;
        var_dt_inicio   := i_dt_inicio;
        var_dt_fim      := i_dt_fim + 1;
        var_episodio    := i_episodio;
        var_t_episodio  := i_t_episodio;
        var_activo      := i_activo;

        --PCK_ENF_HISTORICO.GERA_HISTORICO | Assim j� so processo o que o utilizador pode ver
        FOR T IN
        (
            SELECT TIPO,ORDEM
              FROM
            (
                SELECT TIPO,ORDEM
                  FROM GR_ENF_HISTORICO_CONFIG q0
                 WHERE (    NOT EXISTS ( SELECT 1 FROM GR_ENF_HISTORICO_CONFIG_DET q1 WHERE q0.TIPO = q1.TIPO )
                         OR     EXISTS ( SELECT 1 FROM GR_ENF_HISTORICO_CONFIG_DET q1 WHERE q0.TIPO = q1.TIPO AND q1.T_PESS_HOSP = get_t_pess_hosp )
                       )
                   AND is_number(TIPO) = 'FALSE'

                UNION ALL

                SELECT 'FERIDAS' TIPO,98
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1
                                  FROM GR_ENF_FERIDAS           q0
                                      ,GR_ENF_HISTORICO_CONFIG  q1
                                 WHERE q0.COD_INTERV = q1.TIPO
                                   AND TIPO LIKE nvl(i_MENU_LEFT,'%')
                              )

                UNION ALL

                SELECT 'FORMS_DIN' TIPO,99
                  FROM DUAL
                 WHERE EXISTS ( SELECT 1
                                  FROM FD_FORM                  q0
                                      ,GR_ENF_HISTORICO_CONFIG  q1
                                 WHERE q0.ID_FORM||'' = q1.TIPO
                                   AND TIPO LIKE nvl(i_MENU_LEFT,'%')
                              )
            ) w0
             WHERE TIPO LIKE nvl(i_MENU_LEFT,'%')
               AND NOT EXISTS
                (
                    SELECT 1 FROM SDT_ENF_HISTORICO_STATUS w1
                     WHERE w0.TIPO = w1.TIPO
                       AND w1.JA_PROCESSADO = 'S'
                )

             ORDER BY ORDEM
        )
        LOOP

            IF T.TIPO = 'TESTE_COMBUR'
            THEN add_combur;
            ELSIF T.TIPO = 'ACTOS_ENFERMAGEM'
            THEN add_actos_enfermagem;
            ELSIF T.TIPO = 'ENF_DIAG'
            THEN add_enf_diag;
            ELSIF T.TIPO = 'ENSINAMENTOS'
            THEN add_ensinamentos;
            ELSIF T.TIPO = 'INTERV_ENFERMAGEM'
            THEN add_interv_enfermagem;
            ELSIF T.TIPO = 'NOTAS'
            THEN add_notas_enfermagem;
            ELSIF T.TIPO = 'PARAMETROS'
            THEN add_parametros; COMMIT;
            ELSIF T.TIPO = 'TERAPEUTICA_ADM'
            THEN add_terapeurica_adm; COMMIT;
            ELSIF T.TIPO = 'TERAPEUTICA_N_ADM'
            THEN add_terapeurica_n_adm;
            ELSIF T.TIPO = 'TRIAGEM'
            THEN add_triagem;
            ELSIF T.TIPO = 'MCDT'
            THEN add_mcdt;
            ELSIF T.TIPO = 'ATOS_MED'
            THEN add_mtos_medicos;
            ELSIF T.TIPO = 'OBS_MEDICA'
            THEN add_obs_medica;
            ELSIF T.TIPO = 'OBS_TECNICOS'
            THEN add_obs_tecnicos;
            ELSIF T.TIPO = 'ESCALAS'
            THEN add_escalas;
            ELSIF T.TIPO = 'DIAG_MED'
            THEN add_diag_med;
            ELSIF T.TIPO = 'INTERV_REALIZADA'
            THEN add_intervencoes_realizadas;
            ELSIF T.TIPO = 'GLICEMIAS'
            THEN add_glicemias;
            ELSIF T.TIPO = 'VIGILANCIAS'
            THEN
                IF i_vigil = 1
                THEN add_vigilancias_dif;
                ELSE add_vigilancias;
                END IF;
            ELSIF T.TIPO = 'FERIDAS'
            THEN
                add_feridas;
            ELSIF T.TIPO = 'FORMS_DIN'
            THEN
                add_form_din;
			ELSIF T.TIPO = 'DIET'
            THEN
                add_diet;
            END IF;

            pck_pe_logging.log('PCK_ENF_HISTORICO','PROCESSEI ['||T.TIPO||']','');

            MERGE INTO SDT_ENF_HISTORICO_STATUS q0
             USING ( SELECT T.TIPO          TIPO
                           ,'S'             JA_PROCESSADO
                           ,( SELECT COUNT(1)
                                FROM DUAL
                               WHERE EXISTS( SELECT 1 FROM SDT_ENF_HISTORICO WHERE TIPO = T.TIPO )
                            )               COM_DADOS
                       FROM DUAL
                   ) q1
               ON (q0.TIPO = q1.TIPO )
             WHEN MATCHED THEN
                UPDATE SET q0.JA_PROCESSADO     = q1.JA_PROCESSADO
                          ,q0.COM_DADOS         = q1.COM_DADOS
             WHEN NOT MATCHED THEN
                INSERT (q0.TIPO,q0.COM_DADOS,q0.JA_PROCESSADO)
                VALUES (q1.TIPO,q1.COM_DADOS,q1.JA_PROCESSADO);

        END LOOP;

        --| Definir um "ID" unico por linha
        UPDATE SDT_ENF_HISTORICO
           SET ROWNUM_  = ROWNUM
              ,ACTIVO   = nvl(ACTIVO,'S');

        COMMIT;

    END;


    PROCEDURE gera_historico (i_t_doente     IN VARCHAR2,
                              i_doente       IN VARCHAR2,
                              i_dt_inicio    IN DATE,
                              i_dt_fim       IN DATE,
                              i_episodio     IN VARCHAR2,
                              i_t_episodio   IN VARCHAR2,
                              i_activo       IN VARCHAR2,
                              i_vigil        IN VARCHAR2)
    IS
    BEGIN
        gerA_historico(i_T_DOENTE,i_DOENTE,i_DT_INICIO,i_DT_FIM,i_EPISODIO,i_T_EPISODIO,i_ACTIVO,i_VIGIL,'%');
    END;

    PROCEDURE gera_HISTORICO
    (
        i_T_DOENTE  VARCHAR2,
        i_DOENTE    VARCHAR2,
        i_MENU_LEFT VARCHAR2
    ) IS
    BEGIN
        gera_HISTORICO
        (
            i_T_DOENTE,
            i_DOENTE,
            NULL,
            NULL,
            NULL,
            NULL,
            NULL,
            1,
            i_MENU_LEFT
        );
    END;



    FUNCTION is_from_other_episode (i_texto IN VARCHAR2)
        RETURN BOOLEAN
    IS
        w_retorno   BOOLEAN := FALSE;
    BEGIN
        IF INSTR (i_texto, traduz (w_const_diag_herdado)) > 0 OR INSTR (i_texto, traduz (w_const_interv_herdado)) > 0
        THEN
            w_retorno := TRUE;
        END IF;

        RETURN w_retorno;
    END is_from_other_episode;
/**************************************************/
-- THE END OF THE PACKAGE ( GET A LIVE NOW ) ------- *LIFE NOT LIVE
END;
/



/*######################################
###### BASE_SD_ENF_CHECK_LIST.sql ######
######################################*/
CREATE OR REPLACE TRIGGER BASE_SD_ENF_CHECK_LIST
 BEFORE 
 INSERT OR DELETE OR UPDATE
 ON SD_ENF_CHECK_LIST
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW 
DECLARE
    trg_SYSTIMESTAMP    TIMESTAMP WITH TIME ZONE    := F_get_USER_TIMESTAMP;
    trg_SYSDATE         DATE                        := GA_SYSDATE;
    
    row_CHECK_LIST      SD_ENF_CHECK_LIST%ROWTYPE;
    
    execucao_S_CRI      BOOLEAN                     := FALSE;
    execucao_S_DEL      BOOLEAN                     := FALSE;
    execucao_N_CRI      BOOLEAN                     := FALSE;
    execucao_N_DEL      BOOLEAN                     := FALSE;
    
    var_RAISE           BOOLEAN                     := FALSE;

    var_CATETER         VARCHAR2(1000);

    var_UTILIZADOR      SD_ENF_CHECK_LIST.N_MECAN_ENF_RESP%TYPE;
    var_LOCAL           VARCHAR2(200) := SDF_ENF_CURRENT_SCHEMA || '.' || $$PLSQL_UNIT;
    
    var_MSG VARCHAR2(4000);
    
    step varchar2(10);
---- ESTE TRIGGER INCLUI OS ANTIGOS:
-- trg      base_SD_ENF_CHECK_LIST
-- trg      BEF_UD_APAGA_DADOS
-- trg      BEF_IU_SD_ENF_CHECK_LIST_USER
-- trg      TRG_ENFPAINEL_SYNC_LIST
-- trg      TRG_SD_ENF_CHECK_LIST_RUBR

    FUNCTION exists_N_SEQ_PROG_ELIMINAVEL( i_COD_CHECK NUMBER ) RETURN BOOLEAN
    IS
        CURSOR N_SEQ_PROG_PARA_VALIDAR IS
        SELECT N_SEQ_PROG FROM SD_ENF_CHECK_RUBR WHERE COD_CHECK = i_COD_CHECK;                       
    BEGIN
        
        FOR N IN N_SEQ_PROG_PARA_VALIDAR
        LOOP          
            SDP_ENF_POSSO_ELIM_N_SEQ_PROG(N.N_SEQ_PROG,var_MSG);
            
            IF( nvl(var_MSG,'OK') <> 'OK' )
            THEN
                var_RAISE := TRUE;
                RETURN FALSE;
            END IF;
        END LOOP;                    

        RETURN TRUE;
           
    EXCEPTION WHEN OTHERS THEN
        var_MSG := 'zz'||sqlerrm;
        step := '07';
        RETURN FALSE;
    END;
    
    FUNCTION exists_RUBRICA( i_COD_SERV VARCHAR2 , i_COD_INTERV VARCHAR2 ) RETURN BOOLEAN
    IS
        o_COUNT NUMBER;
    BEGIN
        SELECT COUNT(*)
          INTO o_COUNT
          FROM GR_ENF_INTERV_RUBRICAS
         WHERE nvl(COD_SERV,i_COD_SERV)   = i_COD_SERV
           AND COD_INTERV                 = i_COD_INTERV;

        RETURN o_COUNT > 0;
                
    EXCEPTION WHEN OTHERS THEN
        step := '09';
        RETURN FALSE;
    END exists_RUBRICA;

    FUNCTION exists_N_SEQ_PROG( i_COD_CHECK NUMBER ) RETURN BOOLEAN
    IS
        o_COUNT NUMBER;
    BEGIN
        SELECT COUNT(*)
          INTO o_COUNT
          FROM SD_ENF_CHECK_RUBR
         WHERE COD_CHECK = i_COD_CHECK
           AND N_SEQ_PROG IS NOT NULL;

        RETURN o_COUNT > 0;
                
    EXCEPTION WHEN OTHERS THEN
        step := '08';
        RETURN FALSE;
    END exists_N_SEQ_PROG;
	
	FUNCTION is_user_enf (i_enf VARCHAR2)
        RETURN BOOLEAN
    IS
        w_cont   NUMBER;
    BEGIN
        SELECT COUNT (*)
          INTO w_cont
          FROM sd_pess_hosp_def
         WHERE t_pess_hosp = 'ENF' AND user_sys = i_enf;

        IF w_cont > 0
        THEN
            RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    END;

BEGIN
    step := '01';
    --| Condi��es para n�o disparar o trigger
    IF NOT PCK_CONTROLA_TRIGGERS.UPDATE_ALLOWED OR ( UPDATING AND :OLD.DOENTE <> :NEW.DOENTE ) THEN RETURN; END IF;

    IF INSERTING
    THEN
        --| Garantir a sequ�ncia
        IF :NEW.COD_CHECK IS NULL
        THEN
            SELECT S_SD_ENF_CHECK_LIST.NEXTVAL
              INTO :NEW.COD_CHECK FROM DUAL;
        END IF;
        
        --| Preencher as colunas de cria��o
        :NEW.USER_CRI       := TP_PCK_UTILIZADOR_APL.LE_UTIL_APL;
        :NEW.DT_CRI         := trg_SYSDATE;
        :NEW.DT_CRI_TS      := trg_SYSTIMESTAMP;
        
        --| Variavel que indica se estou a criar uma execu��o
        execucao_S_CRI := :NEW.FLG_ESTADO = 'S';
        execucao_N_CRI := :NEW.FLG_ESTADO = 'N';
        
        --| Preencher variaveis
        row_CHECK_LIST.T_DOENTE     := :NEW.T_DOENTE;
        row_CHECK_LIST.DOENTE       := :NEW.DOENTE;
        row_CHECK_LIST.T_EPISODIO   := :NEW.T_EPISODIO;
        row_CHECK_LIST.EPISODIO     := :NEW.EPISODIO;
        row_CHECK_LIST.DT_ITEM      := :NEW.DT_ITEM;
        row_CHECK_LIST.HR_ITEM      := :NEW.HR_ITEM;
        row_CHECK_LIST.COD_CHECK    := :NEW.COD_CHECK;
        row_CHECK_LIST.ID_AGRUPADOR := :NEW.ID_AGRUPADOR;
        row_CHECK_LIST.COD_ITEM     := :NEW.COD_ITEM;
        step := '01.1';
    ELSIF UPDATING
    THEN
        
        --| Preencher as colunas de atualiza��o
        :NEW.USER_ACT       := TP_PCK_UTILIZADOR_APL.LE_UTIL_APL;
        :NEW.DT_ACT         := trg_SYSDATE;
        :NEW.DT_ACT_TS      := trg_SYSTIMESTAMP;
		
		IF is_user_enf(TP_PCK_UTILIZADOR_APL.LE_UTIL_APL) THEN
            :NEW.dt_act_enf := trg_sysdate;
        END IF;

        --| Variavel que indica se estou a criar\apagar uma execu��o
        execucao_S_CRI := ( :OLD.FLG_ESTADO NOT IN ('S') AND :NEW.FLG_ESTADO     IN ('S') );
        execucao_S_DEL := ( :OLD.FLG_ESTADO     IN ('S') AND :NEW.FLG_ESTADO NOT IN ('S') );
        execucao_N_CRI := ( :OLD.FLG_ESTADO NOT IN ('N') AND :NEW.FLG_ESTADO     IN ('N') );
        execucao_N_DEL := ( :OLD.FLG_ESTADO     IN ('N') AND :NEW.FLG_ESTADO NOT IN ('N') );
        
        --| Preencher variaveis
        row_CHECK_LIST.T_DOENTE     := :NEW.T_DOENTE;
        row_CHECK_LIST.DOENTE       := :NEW.DOENTE;
        row_CHECK_LIST.T_EPISODIO   := :NEW.T_EPISODIO;
        row_CHECK_LIST.EPISODIO     := :NEW.EPISODIO;
        row_CHECK_LIST.DT_ITEM      := :NEW.DT_ITEM;
        row_CHECK_LIST.HR_ITEM      := :NEW.HR_ITEM;
        row_CHECK_LIST.COD_CHECK    := :NEW.COD_CHECK;
        row_CHECK_LIST.ID_AGRUPADOR := :NEW.ID_AGRUPADOR;
        row_CHECK_LIST.COD_ITEM     := :NEW.COD_ITEM;
        step := '01.2';       
    ELSIF DELETING
    THEN
        execucao_S_DEL := :OLD.FLG_ESTADO IN ('S');
        execucao_N_DEL := :OLD.FLG_ESTADO IN ('N');

        --| Preencher variaveis
        row_CHECK_LIST.T_DOENTE     := :OLD.T_DOENTE;
        row_CHECK_LIST.DOENTE       := :OLD.DOENTE;
        row_CHECK_LIST.T_EPISODIO   := :OLD.T_EPISODIO;
        row_CHECK_LIST.EPISODIO     := :OLD.EPISODIO;
        row_CHECK_LIST.DT_ITEM      := :OLD.DT_ITEM;
        row_CHECK_LIST.HR_ITEM      := :OLD.HR_ITEM;
        row_CHECK_LIST.COD_CHECK    := :OLD.COD_CHECK;
        row_CHECK_LIST.ID_AGRUPADOR := :OLD.ID_AGRUPADOR;
        row_CHECK_LIST.COD_ITEM     := :OLD.COD_ITEM;
        step := '01.3';
    END IF;
    step := '02';
    --**************************************************************************
    -- Codigo relacionado com valida��es de cria��o de registos SN ou anula��o de registos SN
    
    --| Se estiver a gravar uma execu��o
    IF execucao_S_CRI OR execucao_N_CRI
    THEN
        --| Atualiza a data de execu��o
        begin
            IF :NEW.DATAHORA_ITEM_TS IS NULL
            THEN
                :NEW.DATAHORA_ITEM_TS := F_convert_to_USER_TIMEZONE(SDF_ENF_ADD_HOUR2DATE(:NEW.DT_ITEM,:NEW.HR_ITEM));
            END IF;
        exception when others then
            :NEW.DATAHORA_ITEM_TS := null;
            PCK_PE_LOGGING.erro('base_SD_ENF_CHECK_LIST','step='||step||' | '||SQLERRM,'[T_DOENTE='||row_CHECK_LIST.T_DOENTE||'][DOENTE='||row_CHECK_LIST.DOENTE||'][T_EPISODIO='||row_CHECK_LIST.T_EPISODIO||'][EPISODIO='||row_CHECK_LIST.EPISODIO||'][COD_CHECK='||row_CHECK_LIST.COD_CHECK||'][ID_AGRUPADOR='||row_CHECK_LIST.ID_AGRUPADOR||'][N_MECAN_ENF_RESP='||row_CHECK_LIST.N_MECAN_ENF_RESP||'][DT_ITEM='||to_CHAR(row_CHECK_LIST.DT_ITEM,'YYYY-MM-DD')||'][HR_ITEM='||row_CHECK_LIST.HR_ITEM||'][COD_ITEM='||row_CHECK_LIST.COD_ITEM||']');
        end;           
        step := '02.1';
        --| Garantir os utilizadores em USER_SYS
        IF :NEW.N_MECAN_ENF IS NOT NULL
        THEN
            var_UTILIZADOR := nvl(SDF_ENF_VALIDA_UTILIZADOR(:NEW.N_MECAN_ENF),:NEW.N_MECAN_ENF);
            IF var_UTILIZADOR IS NULL THEN raise_application_error(-20201,traduz('O utilizador %1 n�o � valido. (base_SD_ENF_CHECK_LIST)',:NEW.N_MECAN_ENF,var_LOCAL)); END IF;
            :NEW.N_MECAN_ENF := var_UTILIZADOR;
            /*
        ELSE
            raise_application_error(-20201,traduz('A coluna N_MECAN_ENF n�o pode ser nula. (base_SD_ENF_CHECK_LIST)',:NEW.N_MECAN_ENF,var_LOCAL));
            */
        END IF;
        step := '02.2';
        IF :NEW.N_MECAN_ENF_RESP IS NOT NULL
        THEN
            var_UTILIZADOR := nvl(SDF_ENF_VALIDA_UTILIZADOR(:NEW.N_MECAN_ENF_RESP),:NEW.N_MECAN_ENF_RESP);
            IF var_UTILIZADOR IS NULL THEN raise_application_error(-20201,traduz('O utilizador %1 n�o � valido. (base_SD_ENF_CHECK_LIST)',:NEW.N_MECAN_ENF_RESP,var_LOCAL)); END IF;
            :NEW.N_MECAN_ENF_RESP := var_UTILIZADOR;
        ELSE
            raise_application_error(-20201,traduz('A coluna N_MECAN_ENF_RESP n�o pode ser nula. (base_SD_ENF_CHECK_LIST)',:NEW.N_MECAN_ENF_RESP,var_LOCAL));
        END IF;

    END IF;
    step := '03';
    --| Se estiver a anular uma execu��o
    IF execucao_S_DEL OR execucao_N_DEL
    THEN
        --| Remove data de execu��o
        :NEW.DATAHORA_ITEM_TS := to_DATE(NULL);
        
        --| Tratar da informa��o pendurada da SD_ENF_CHECK_INDIC
        INSERT INTO LOG_SD_ENF_CHECK_INDIC (COD_CHECK,COD_INDIC,COD_INTERV,VALOR,USER_CRI,DT_CRI,USER_ACT,DT_ACT,OBSERV,N_TRIAG,FLG_VISIVEL)
        SELECT COD_CHECK,COD_INDIC,COD_INTERV,VALOR,USER_CRI,DT_CRI,USER_ACT,DT_ACT,OBSERV,N_TRIAG,FLG_VISIVEL
          FROM SD_ENF_CHECK_INDIC
         WHERE COD_CHECK = :NEW.COD_CHECK;
        step := '03.1';
        IF UPDATING
        THEN
            DELETE FROM SD_ENF_CHECK_INDIC_CARACT WHERE COD_CHECK = :OLD.COD_CHECK;
            UPDATE SD_ENF_CHECK_INDIC
               SET VALOR     = NULL
             WHERE COD_CHECK = :OLD.COD_CHECK;
             step := '03.2';
        ELSIF DELETING
        THEN
            DELETE FROM SD_ENF_CHECK_INDIC
             WHERE COD_CHECK = :OLD.COD_CHECK;
             step := '03.3';
        END IF;             
        
    END IF;
    step := '04';
    --**************************************************************************
    --| Sincronizar com o painel os cateteres
    IF (execucao_S_CRI OR execucao_N_CRI OR execucao_S_DEL OR execucao_N_DEL) AND row_CHECK_LIST.T_EPISODIO IN ('Internamentos','Ambulatorio')
    THEN

        --| Obter o cateter
        BEGIN
            IF pck_enf_painel.valida_CFG_ASSOC_ITEM('TRAT_CATETER',row_CHECK_LIST.COD_ITEM) = 'S'
            THEN
                --| Le a descricao
                SELECT DESCR
                  INTO var_CATETER
                  FROM GR_ENF_PAINEL_ASSOC_ITEMS
                 WHERE CODIGO = 'TRAT_CATETER'
                   AND (CAMPO1 = row_CHECK_LIST.COD_ITEM OR CAMPO2 = row_CHECK_LIST.COD_ITEM);
            END IF;
        EXCEPTION WHEN OTHERS THEN
            var_CATETER := NULL;
            step := '04.1';
        END;    
        
        IF var_CATETER IS NOT NULL
        THEN
            --| Adiciona na SD_ENF_CTRL_PROGS se n�o existir por processar este EPISODIO|INDICADOR, se existir complementa as OBSERVACOES do que est� por processar
            MERGE INTO SD_ENF_CTRL_PROGS q0
             USING ( SELECT 'ENFPAINEL_SYNC'        TIPO,
                            'CATETERISMO'           TABELA,
                            row_CHECK_LIST.EPISODIO ID_TABELA,
                            'NEW'                   ESTADO,
                            var_CATETER             AUX_1
                       FROM DUAL
                   ) q1
               ON (     q0.TIPO         = q1.TIPO
                    AND q0.TABELA       = q1.TABELA
                    AND q0.ID_TABELA    = q1.ID_TABELA
                    AND q0.ESTADO       = q1.ESTADO
                    AND q0.AUX_1        = q1.AUX_1
                  )
            WHEN NOT MATCHED THEN
            INSERT (q0.TIPO,q0.TABELA,q0.ID_TABELA,q0.ESTADO,q0.AUX_1)
            VALUES (q1.TIPO,q1.TABELA,q1.ID_TABELA,q1.ESTADO,q1.AUX_1);
            step := '04.2';
        END IF;        
    END IF;
    step := '05';
    --**************************************************************************
    --| Sincronismo RASTREIO_NUTRICIONAL
    IF execucao_S_DEL AND row_CHECK_LIST.COD_ITEM IN (PCK_ENF_CONFIG.obter_PARAM('ESCALA_RISCO_NUTRICIONAL'),PCK_ENF_CONFIG.obter_PARAM('ESCALA_NRS_2002'),PCK_ENF_CONFIG.obter_PARAM('ESCALA_STRONGKIDS'))
    THEN
        PCK_ENF_BRIDGE_DIET.sync_RASTREIO_NUTRICIONAL_DEL
        (
            i_T_DOENTE      => row_CHECK_LIST.T_DOENTE,
            i_DOENTE        => row_CHECK_LIST.DOENTE,
            i_T_EPISODIO    => row_CHECK_LIST.T_EPISODIO,
            i_EPISODIO      => row_CHECK_LIST.EPISODIO,
            i_DATA          => trunc(row_CHECK_LIST.DT_ITEM),
            i_COD_CHECK_DEL => row_CHECK_LIST.COD_CHECK
        );
    END IF;        
    step := '06';
    --**************************************************************************
    --| Gest�o das rubricas
    IF nvl(PCK_ENF_CONFIG.obter_param('RUBRICAS_NA_HORA'),'N') = 'S' AND ( execucao_S_CRI OR execucao_S_DEL )
    THEN
        DECLARE
            v_ACAO  VARCHAR2(10);
        BEGIN
            --Preencher vari�vel
            row_CHECK_LIST.N_MECAN_ENF_RESP := :NEW.N_MECAN_ENF_RESP;
            
            IF execucao_S_CRI THEN v_ACAO := 'S'; ELSE v_ACAO := 'N'; END IF;
            
            -- S� adiciono rubrica se houver associada ao item
            -- s� elimino se tiver um N_SEQ_PROG_ELIMINAVEL
            
            row_CHECK_LIST.COD_SERV := SDF_ENF_OBTER_SERV(row_CHECK_LIST.T_EPISODIO,row_CHECK_LIST.EPISODIO,row_CHECK_LIST.T_DOENTE,row_CHECK_LIST.DOENTE);
            step := '06.1';
            IF( exists_RUBRICA(row_CHECK_LIST.COD_SERV,row_CHECK_LIST.COD_ITEM)
                AND
                (
                    ( execucao_S_CRI AND NOT exists_N_SEQ_PROG(row_CHECK_LIST.COD_CHECK) )
                    OR
                    ( execucao_S_DEL AND exists_N_SEQ_PROG_ELIMINAVEL(row_CHECK_LIST.COD_CHECK) )
                )
              )
            THEN
                SDP_ENF_CTRL_PROCESSAMENTO
                (   i_t_doente         => row_CHECK_LIST.T_DOENTE,
                    i_doente           => row_CHECK_LIST.DOENTE,
                    i_t_episodio       => row_CHECK_LIST.T_EPISODIO,
                    i_episodio         => row_CHECK_LIST.EPISODIO,
                    i_dT_item          => row_CHECK_LIST.DT_ITEM,
                    i_hr_item          => row_CHECK_LIST.HR_ITEM,
                    i_n_mecan_enf_resp => row_CHECK_LIST.N_MECAN_ENF_RESP,
                    i_id_agrupador     => row_CHECK_LIST.ID_AGRUPADOR,
                    i_cod_check        => row_CHECK_LIST.COD_CHECK,
                    i_cod_item         => row_CHECK_LIST.COD_ITEM,
                    i_accao            => v_ACAO
                );
                step := '06.2';
            END IF;
        END;           
    END IF; 
    step := '10';
EXCEPTION WHEN OTHERS THEN
    PCK_PE_LOGGING.erro('base_SD_ENF_CHECK_LIST','step='||step||' | '||SQLERRM,'[T_DOENTE='||row_CHECK_LIST.T_DOENTE||'][DOENTE='||row_CHECK_LIST.DOENTE||'][T_EPISODIO='||row_CHECK_LIST.T_EPISODIO||'][EPISODIO='||row_CHECK_LIST.EPISODIO||'][COD_CHECK='||row_CHECK_LIST.COD_CHECK||'][ID_AGRUPADOR='||row_CHECK_LIST.ID_AGRUPADOR||'][N_MECAN_ENF_RESP='||row_CHECK_LIST.N_MECAN_ENF_RESP||'][DT_ITEM='||to_CHAR(row_CHECK_LIST.DT_ITEM,'YYYY-MM-DD')||'][HR_ITEM='||row_CHECK_LIST.HR_ITEM||'][COD_ITEM='||row_CHECK_LIST.COD_ITEM||']');
    RAISE;    
END;
/


/*#######################################
###### PCK_ENF_RESUMO_EPR_spec.sql ######
#######################################*/

  CREATE OR REPLACE PACKAGE PCK_ENF_RESUMO_EPR 
IS
    /*INICIO DA LISTA DE FUN��ES PRINCIPAIS*/
    --******************************************************************************
    FUNCTION get_triagens_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id VARCHAR2)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_triagens (i_t_doente      VARCHAR2,
                           i_doente        VARCHAR2,
                           i_t_episodio    VARCHAR2,
                           i_episodio      VARCHAR2,
                           i_cod_serv      VARCHAR2,
                           i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_escalas_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER, i_show_resume_header VARCHAR2)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_escalas (i_t_doente      VARCHAR2,
                          i_doente        VARCHAR2,
                          i_t_episodio    VARCHAR2,
                          i_episodio      VARCHAR2,
                          i_cod_serv      VARCHAR2,
                          i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_feridas_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_feridas (i_t_doente      VARCHAR2,
                          i_doente        VARCHAR2,
                          i_t_episodio    VARCHAR2,
                          i_episodio      VARCHAR2,
                          i_cod_serv      VARCHAR2,
                          i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_combur_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_combur (i_t_doente      VARCHAR2,
                         i_doente        VARCHAR2,
                         i_t_episodio    VARCHAR2,
                         i_episodio      VARCHAR2,
                         i_cod_serv      VARCHAR2,
                         i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_vpc_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_vpc (i_t_doente      VARCHAR2,
                      i_doente        VARCHAR2,
                      i_t_episodio    VARCHAR2,
                      i_episodio      VARCHAR2,
                      i_cod_serv      VARCHAR2,
                      i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_vigilancias_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_vigilancias (i_t_doente      VARCHAR2,
                              i_doente        VARCHAR2,
                              i_t_episodio    VARCHAR2,
                              i_episodio      VARCHAR2,
                              i_cod_serv      VARCHAR2,
                              i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_aval_intra_oper_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_aval_intra_oper (i_t_doente      VARCHAR2,
                                  i_doente        VARCHAR2,
                                  i_t_episodio    VARCHAR2,
                                  i_episodio      VARCHAR2,
                                  i_cod_serv      VARCHAR2,
                                  i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_prod_elim_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_prod_elim (i_t_doente      VARCHAR2,
                            i_doente        VARCHAR2,
                            i_t_episodio    VARCHAR2,
                            i_episodio      VARCHAR2,
                            i_cod_serv      VARCHAR2,
                            i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_aval_inicial_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_aval_inicial (i_t_doente      VARCHAR2,
                               i_doente        VARCHAR2,
                               i_t_episodio    VARCHAR2,
                               i_episodio      VARCHAR2,
                               i_cod_serv      VARCHAR2,
                               i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_by_by_dt (i_t_doente      VARCHAR2,
                           i_doente        VARCHAR2,
                           i_t_episodio    VARCHAR2,
                           i_episodio      VARCHAR2,
                           i_cod_serv      VARCHAR2,
                           i_n_mecan       VARCHAR2,
                           i_date          DATE)
        RETURN pck_types_nv.t_resumo;

    FUNCTION get_bh (i_t_doente      VARCHAR2,
                     i_doente        VARCHAR2,
                     i_t_episodio    VARCHAR2,
                     i_episodio      VARCHAR2,
                     i_cod_serv      VARCHAR2,
                     i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_terapeutica (i_t_doente      VARCHAR2,
                              i_doente        VARCHAR2,
                              i_t_episodio    VARCHAR2,
                              i_episodio      VARCHAR2,
                              i_cod_serv      VARCHAR2,
                              i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_notas_enf_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_notas_enf (i_t_doente      VARCHAR2,
                            i_doente        VARCHAR2,
                            i_t_episodio    VARCHAR2,
                            i_episodio      VARCHAR2,
                            i_cod_serv      VARCHAR2,
                            i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_notas_enf_interv_by_id (i_t_doente         VARCHAR2,
                                         i_doente           VARCHAR2,
                                         i_id               NUMBER,
                                         i_source_detail    VARCHAR2)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_notas_enf_interv (i_t_doente      VARCHAR2,
                                   i_doente        VARCHAR2,
                                   i_t_episodio    VARCHAR2,
                                   i_episodio      VARCHAR2,
                                   i_cod_serv      VARCHAR2,
                                   i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_sala_obs_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_sala_obs (i_t_doente      VARCHAR2,
                           i_doente        VARCHAR2,
                           i_t_episodio    VARCHAR2,
                           i_episodio      VARCHAR2,
                           i_cod_serv      VARCHAR2,
                           i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_glicemia_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_glicemia (i_t_doente      VARCHAR2,
                           i_doente        VARCHAR2,
                           i_t_episodio    VARCHAR2,
                           i_episodio      VARCHAR2,
                           i_cod_serv      VARCHAR2,
                           i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    FUNCTION get_escala_glasgow_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_escala_glasgow (i_t_doente      VARCHAR2,
                                 i_doente        VARCHAR2,
                                 i_t_episodio    VARCHAR2,
                                 i_episodio      VARCHAR2,
                                 i_cod_serv      VARCHAR2,
                                 i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo;

    --******************************************************************************
    /*FIM DA LISTA DE FUN��ES PRINCIPAIS*/


    FUNCTION concatenate_sin_vitais (i_t_doente      VARCHAR2,
                                     i_doente        VARCHAR2,
                                     i_t_episodio    VARCHAR2,
                                     i_episodio      VARCHAR2,
                                     i_n_triagem     NUMBER)
        RETURN VARCHAR;

    FUNCTION get_n_mecan (i_user_sys VARCHAR)
        RETURN VARCHAR;

    FUNCTION get_locais_electro (i_ids_locais VARCHAR)
        RETURN VARCHAR;


    FUNCTION convert_combur_notation (i_valor NUMBER)
        RETURN VARCHAR;

    FUNCTION concatenate_resp_vigil (i_n_vigil VARCHAR2, i_cod_item VARCHAR2)
        RETURN VARCHAR;

    FUNCTION create_texto_esc_glasgow (i_olhos NUMBER, i_verbal NUMBER, i_motora NUMBER)
        RETURN VARCHAR;

    FUNCTION get_prod_elim_valor_hora (i_n_cod_reg_prod VARCHAR, i_dt_registo DATE, i_turno VARCHAR)
        RETURN VARCHAR;

    FUNCTION concatenate_resumo_ai (i_t_doente      VARCHAR,
                                    i_doente        VARCHAR,
                                    i_t_episodio    VARCHAR,
                                    i_episodio      VARCHAR)
        RETURN VARCHAR;

    FUNCTION create_base_rec (p_t_doente        IN VARCHAR2,
                              p_doente          IN VARCHAR2,
                              p_t_episodio      IN VARCHAR2,
                              p_episodio        IN VARCHAR2,
                              p_cod_serv        IN VARCHAR2,
                              p_user_sys        IN VARCHAR2,
                              p_n_mecan         IN VARCHAR2,
                              p_descr_n_mecan   IN VARCHAR2,
                              p_dt_registo      IN DATE)
        RETURN pck_types_nv.r_resumo;

    FUNCTION get_ultima_terap (i_data          DATE,
                               i_doente        VARCHAR,
                               i_t_doente      VARCHAR,
                               i_episodio      VARCHAR,
                               i_t_episodio    VARCHAR)
        RETURN VARCHAR;

    FUNCTION concatenate_terap (i_data          DATE,
                                i_doente        VARCHAR,
                                i_t_doente      VARCHAR,
                                i_episodio      VARCHAR,
                                i_t_episodio    VARCHAR)
        RETURN VARCHAR;

    FUNCTION aux_balanco_hidrico_por_epis (i_t_doente      VARCHAR2,
                                           i_doente        VARCHAR2,
                                           i_t_episodio    VARCHAR2,
                                           i_episodio      VARCHAR2,
                                           i_data          DATE)
        RETURN VARCHAR2;


    FUNCTION aux_balanco_hidrico_interv (i_t_doente      VARCHAR2,
                                         i_doente        VARCHAR2,
                                         i_t_episodio    VARCHAR2,
                                         i_episodio      VARCHAR2,
                                         utilizador      VARCHAR2,
                                         i_data          DATE)
        RETURN VARCHAR2;

    FUNCTION aux_bh_24h_total (i_t_doente      VARCHAR2,
                               i_doente        VARCHAR2,
                               i_t_episodio    VARCHAR2,
                               i_episodio      VARCHAR2,
                               i_data          DATE)
        RETURN VARCHAR2;

    FUNCTION convert_long_aval_inicial (i_n_ep_arv IN sd_enf_ep_arv.n_ep_arv%TYPE, i_cod_ep_item IN gr_enf_ep_item.cod_ep_item%TYPE)
        RETURN VARCHAR;
		
	FUNCTION get_diet      (i_t_doente      VARCHAR2,
                            i_doente        VARCHAR2,
                            i_t_episodio    VARCHAR2,
                            i_episodio      VARCHAR2,
                            i_cod_serv      VARCHAR2,
                            i_n_mecan       VARCHAR2,
                            i_show_resume_header VARCHAR2 DEFAULT 'S')
        RETURN pck_types_nv.t_resumo;


    FUNCTION traduz (texto ga_dic_textos.texto_portugues%TYPE)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE;

    FUNCTION traduz (texto ga_dic_textos.texto_portugues%TYPE, i_var VARCHAR2)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE;

    FUNCTION traduz (texto ga_dic_textos.texto_portugues%TYPE, i_vars DBMS_SQL.varchar2a)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE;
END;
/


/*#######################################
###### PCK_ENF_RESUMO_EPR_body.sql ######
#######################################*/
CREATE OR REPLACE PACKAGE BODY PCK_ENF_RESUMO_EPR 
IS
    FUNCTION traduz_msgbox (texto ga_dic_textos.texto_portugues%TYPE)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto_msgbox (texto, 'BD', sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz_msgbox (texto ga_dic_textos.texto_portugues%TYPE, i_var VARCHAR2)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto_msgbox (texto,
                                                      i_var,
                                                      NULL,
                                                      'BD',
                                                      sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz_msgbox (texto ga_dic_textos.texto_portugues%TYPE, i_vars DBMS_SQL.varchar2a)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto_msgbox (texto,
                                                      i_vars,
                                                      'BD',
                                                      sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz_msgbox (texto ga_dic_textos.texto_portugues%TYPE, i_vars VARCHAR2, i_separador VARCHAR2)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto_msgbox (texto,
                                                      i_vars,
                                                      i_separador,
                                                      'BD',
                                                      sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz (texto ga_dic_textos.texto_portugues%TYPE)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto (texto, 'BD', sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz (texto ga_dic_textos.texto_portugues%TYPE, i_var VARCHAR2)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto (texto,
                                               i_var,
                                               NULL,
                                               'BD',
                                               sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION traduz (texto ga_dic_textos.texto_portugues%TYPE, i_vars DBMS_SQL.varchar2a)
        RETURN ga_dic_traducoes.texto_traduzido%TYPE
    IS
    BEGIN
        RETURN ga_pck_dicionario.traduz_texto (texto,
                                               i_vars,
                                               'BD',
                                               sdf_enf_current_schema || '.' || $$plsql_unit);
    END;

    FUNCTION usersys2nmecan (i_user_sys VARCHAR2)
        RETURN VARCHAR2
    IS
        o_nmecan   VARCHAR2 (30);
    BEGIN
        SELECT n_mecan
          INTO o_nmecan
          FROM sd_pess_hosp_def
         WHERE user_sys = i_user_sys;

        RETURN o_nmecan;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN NULL;
    END;

    --******************************************************************************
    --******************************************************************************
    FUNCTION get_feridas_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo
    IS
        texto_localizacao   VARCHAR (200) := traduz ('Localiza��o');
        texto_topologia     VARCHAR (200) := traduz ('Topologia');
        texto_dt_inicio     VARCHAR (200) := traduz ('Data de In�cio');
        texto_inicial       VARCHAR (2000);
        texto_completo      VARCHAR (10000);
        v_rec               pck_types_nv.r_resumo;
    BEGIN
        SELECT t_doente,
               doente,
               t_episodio,
               episodio,
               get_n_mecan (utilizador),
               utilizador,
               data_hora,
               n_registo,
               'FERIDAS#' || n_registo,
               NULL,
               texto_dt_inicio || ': ' || TO_CHAR (dt_inicio, 'dd-mm-yyyy') || CHR (10) || CHR (10) || texto_dt_inicio || ': ' || TO_CHAR (dt_inicio, 'dd-mm-yyyy') || CHR (10),
               'S'
          INTO v_rec.t_doente,
               v_rec.doente,
               v_rec.t_episodio,
               v_rec.episodio,
               v_rec.n_mecan,
               v_rec.user_sys,
               v_rec.dt_registo,
               v_rec.id_bd,
               v_rec.id_resumo,
               v_rec.digest,
               v_rec.texto_completo,
               v_rec.has_more_than_digest
          FROM (SELECT f.localizacao,
                       f.topologia,
                       i.titulo,
                       f.dt_inicio,
                       f.dt_fim,
                       pck_enf_feridas.resumo1 (i.cod_ep_arv_reg, fr.n_registo) resumo,
                       f.t_doente,
                       f.doente,
                       f.episodio,
                       f.t_episodio,
                       NVL (f.user_act, f.user_cri) utilizador,
                       fr.data_hora,
                       fr.n_registo
                  FROM sd_enf_feridas f, gr_enf_feridas i, sd_enf_feridas_registo fr
                 WHERE fr.n_ferida = f.n_ferida AND i.cod_interv = f.cod_interv AND fr.n_ferida = i_id AND f.t_doente = i_t_doente AND f.doente = i_doente);

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_FERIDAS_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_feridas (i_t_doente      VARCHAR2,
                          i_doente        VARCHAR2,
                          i_t_episodio    VARCHAR2,
                          i_episodio      VARCHAR2,
                          i_cod_serv      VARCHAR2,
                          i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec               pck_types_nv.r_resumo;
        v_feridas           pck_types_nv.t_resumo;

        CURSOR feridas
        IS
            SELECT f.localizacao,
                   f.topologia,
                   i.titulo,
                   f.dt_inicio,
                   f.dt_fim,
                   pck_enf_feridas.resumo1 (i.cod_ep_arv_reg, fr.n_registo) resumo,
                   f.t_doente,
                   f.doente,
                   f.episodio,
                   f.t_episodio,
                   NVL (f.user_act, f.user_cri) utilizador,
                   fr.data_hora,
                   fr.n_registo
              FROM sd_enf_feridas f, gr_enf_feridas i, sd_enf_feridas_registo fr
             WHERE     fr.n_ferida = f.n_ferida
                   AND i.cod_interv = f.cod_interv
                   AND f.t_doente = i_t_doente
                   AND f.doente = i_doente
                   AND f.t_episodio = i_t_episodio
                   AND f.episodio = i_episodio;

        texto_localizacao   VARCHAR (200) := traduz ('Localiza��o');
        texto_topologia     VARCHAR (200) := traduz ('Topologia');
        texto_dt_inicio     VARCHAR (200) := traduz ('Data de In�cio');
        texto_inicial       VARCHAR (2000);
        texto_completo      VARCHAR (10000);
    BEGIN
        FOR c IN feridas
        LOOP
            texto_inicial := c.titulo || ' ' || texto_localizacao || ': ' || c.localizacao || ' ' || texto_topologia || ': ' || c.topologia;
            texto_completo := texto_dt_inicio || ': ' || TO_CHAR (c.dt_inicio, 'dd-mm-yyyy') || CHR (10);

            IF c.resumo IS NOT NULL
            THEN
                texto_completo := texto_completo || c.resumo;
                v_rec :=
                    create_base_rec (c.t_doente,
                                     c.doente,
                                     c.t_episodio,
                                     c.episodio,
                                     NULL,
                                     c.utilizador,
                                     get_n_mecan (c.utilizador),
                                     '',
                                     c.data_hora);
                v_rec.id_bd := c.n_registo;
                v_rec.id_resumo := 'FERIDAS#' || c.n_registo;
                v_rec.digest := NULL;
                v_rec.texto_completo := texto_inicial || CHR (10) || texto_completo;
                v_rec.has_more_than_digest := 'S';
                v_feridas (v_feridas.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_feridas;
    END;

    --******************************************************************************
    --******************************************************************************

    FUNCTION get_escalas_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER, i_show_resume_header VARCHAR2)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec               pck_types_nv.r_resumo;
        texto_score_total   VARCHAR (200) := traduz ('Score Total');
		v_header              varchar2(4000);
    BEGIN
        SELECT pckt_enf_plano_cuidados.get_descr_interv (q0.id_agrupador) || ' - ' || texto_score_total || ': ' || q1.valor,
               q0.t_doente,
               q0.doente,
               q0.t_episodio,
               q0.episodio,
               get_n_mecan (q0.n_mecan_enf_resp),
               q0.n_mecan_enf_resp,
               q0.cod_check,
               'ESCALAS#' || q0.cod_check,
               sdf_enf_add_hour2date (dt_item, hr_item)
          INTO v_rec.texto_completo,
               v_rec.t_doente,
               v_rec.doente,
               v_rec.t_episodio,
               v_rec.episodio,
               v_rec.n_mecan,
               v_rec.user_sys,
               v_rec.id_bd,
               v_rec.id_resumo,
               v_rec.dt_registo
          FROM sd_enf_check_list q0, sd_enf_check_indic q1
         WHERE q0.cod_check = TO_NUMBER (i_id) AND q0.t_doente = i_t_doente AND q0.doente = i_doente AND q0.cod_check = q1.cod_check;
		
		--JMS-244523 adriano.alves
        v_header := '---- '||TO_CHAR(v_rec.dt_registo,'DD-MM-YYYY HH24:MI')||', '||SDF_URG_OBTEM_PESS_HOSP_NOME(v_rec.user_sys)||' ----'||chr(10);
        
        IF i_show_resume_header = 'S' THEN
            v_rec.texto_completo := v_header||v_rec.texto_completo;
        END IF;
		
        FOR resp IN (  SELECT q1.pergunta,
                              pck_enf_avaliacoes.obtem_descr_resposta (q0.cod_resposta, q0.cod_pergunta) resposta,
                              pck_enf_avaliacoes.obtem_score_pergunta (q0.aval_seq,
                                                                       q0.cod_pergunta,
                                                                       v_rec.t_doente,
                                                                       v_rec.doente,
                                                                       v_rec.t_episodio,
                                                                       v_rec.episodio)
                                  score
                         FROM sd_enf_avaliacoes_det q0, gr_enf_aval_questoes q1
                        WHERE aval_seq = TO_NUMBER (i_id) AND q0.cod_pergunta = q1.cod_pergunta
                     ORDER BY q1.ordem)
        LOOP
            v_rec.texto_completo := v_rec.texto_completo || CHR (10) || resp.pergunta || ': ' || resp.resposta || ' - ' || resp.score || ';';
        END LOOP;

        v_rec.has_more_than_digest := 'S';

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_ESCALAS_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_escalas (i_t_doente      VARCHAR2,
                          i_doente        VARCHAR2,
                          i_t_episodio    VARCHAR2,
                          i_episodio      VARCHAR2,
                          i_cod_serv      VARCHAR2,
                          i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec               pck_types_nv.r_resumo;
        v_escalas           pck_types_nv.t_resumo;
        texto_inicial       VARCHAR (1000);
        texto_respostas     VARCHAR (4000);
        texto_score_total   VARCHAR (200) := traduz ('score total');
		v_header              varchar2(4000);
		
        CURSOR escalas_total
        IS
              SELECT ad.aval_seq,
                     SUM (pck_enf_avaliacoes.obtem_score_pergunta (a.aval_seq,
                                                                   ad.cod_pergunta,
                                                                   a.t_doente,
                                                                   a.doente,
                                                                   a.t_episodio,
                                                                   a.episodio))
                         score,
                     i.descr_interv escala,
                     a.t_doente,
                     a.doente,
                     a.t_episodio,
                     a.episodio,
                     NVL (a.user_act, a.user_cri) utilizador,
                     a.data,
                     chk_l.cod_serv cod_serv
                FROM sd_enf_avaliacoes_det ad,
                     sd_enf_avaliacoes a,
                     sd_enf_check_list chk_l,
                     gr_enf_interv i
               WHERE     ad.aval_seq = a.aval_seq
                     AND a.cod_check = chk_l.cod_check
                     AND chk_l.cod_item = i.cod_interv
                     AND a.t_doente = i_t_doente
                     AND a.doente = i_doente
                     AND a.t_episodio = i_t_episodio
                     AND a.episodio = i_episodio
            GROUP BY ad.aval_seq,
                     i.descr_interv,
                     a.t_doente,
                     a.doente,
                     a.t_episodio,
                     a.episodio,
                     NVL (a.user_act, a.user_cri),
                     data,
                     cod_serv;
    BEGIN
        FOR c IN escalas_total
        LOOP
			--JMS-244523 adriano.alves
            v_header := '---- '||TO_CHAR(c.DATA,'DD-MM-YYYY HH24:MI')||', '||SDF_URG_OBTEM_PESS_HOSP_NOME(c.utilizador)||' ----'||chr(10); 
            texto_inicial := v_header||c.escala || ' - ' || texto_score_total || ': ' || c.score;
            texto_respostas := '';

            FOR d
                IN (SELECT ad.aval_seq,
                           ad.cod_pergunta,
                           ad.cod_resposta,
                           q.pergunta,
                           pck_enf_avaliacoes.obtem_score_pergunta (b.aval_seq,
                                                                    ad.cod_pergunta,
                                                                    b.t_doente,
                                                                    b.doente,
                                                                    b.t_episodio,
                                                                    b.episodio)
                               score,
                           pck_enf_avaliacoes.obtem_descr_resposta (ad.cod_resposta, ad.cod_pergunta) resposta
                      FROM sd_enf_avaliacoes_det ad, sd_enf_avaliacoes b, gr_enf_aval_questoes q
                     WHERE     ad.aval_seq = c.aval_seq
                           AND ad.aval_seq = b.aval_seq
                           AND ad.cod_pergunta = q.cod_pergunta
                           AND b.t_doente = i_t_doente
                           AND b.doente = i_doente
                           AND b.t_episodio = i_t_episodio
                           AND b.episodio = i_episodio)
            LOOP
                texto_respostas := texto_respostas || d.pergunta || ': ' || d.resposta || ' - ' || d.score || '; ' || CHR (10);
            END LOOP;

            texto_respostas := texto_inicial || CHR (10) || texto_respostas;

            IF texto_respostas IS NOT NULL
            THEN
                v_rec :=
                    create_base_rec (c.t_doente,
                                     c.doente,
                                     c.t_episodio,
                                     c.episodio,
                                     c.cod_serv,
                                     c.utilizador,
                                     get_n_mecan (c.utilizador),
                                     '',
                                     c.data);
                v_rec.id_bd := c.aval_seq;
                v_rec.id_resumo := 'ESCALAS#' || c.aval_seq;
                v_rec.digest := NULL;
                v_rec.texto_completo := texto_respostas;
                v_rec.has_more_than_digest := 'S';
                v_escalas (v_escalas.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_escalas;
    END;

    --******************************************************************************
    --******************************************************************************
    FUNCTION get_combur_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec               pck_types_nv.r_resumo;

        texto_completo      VARCHAR (2000);
        var_id_combur       NUMBER;
        var_doente          VARCHAR (100);
        var_t_doente        VARCHAR (100);
        var_episodio        VARCHAR (100);
        var_t_episodio      VARCHAR (100);
        var_dt_hr_registo   DATE;
        var_obs             VARCHAR (400);
        var_utilizador      VARCHAR (100);
        var_texto           VARCHAR (1000);
    BEGIN
        SELECT c.id_combur,
               c.t_episodio,
               c.episodio,
               c.t_doente,
               c.doente,
               c.dt_hr_registo,
               c.obs,
               NVL (c.user_act, c.user_cri) utilizador
          INTO var_id_combur,
               var_t_episodio,
               var_episodio,
               var_t_doente,
               var_doente,
               var_dt_hr_registo,
               var_obs,
               var_utilizador
          FROM sd_enf_combur c
         WHERE c.dt_hr_registo IS NOT NULL AND c.id_combur = i_id AND c.t_doente = i_t_doente AND c.doente = i_doente;

          SELECT gc.descr_item || ': ' || pck_enf_resumo_epr.convert_combur_notation (cd.valor) texto
            INTO var_texto
            FROM sd_enf_combur c, sd_enf_combur_det cd, gr_enf_combur gc
           WHERE c.id_combur = cd.id_combur AND gc.cod_item = cd.cod_item AND c.dt_hr_registo IS NOT NULL AND c.cod_check = i_id AND c.t_doente = i_t_doente AND c.doente = i_doente
        ORDER BY c.id_combur, cd.cod_item DESC;

        texto_completo := TO_CHAR (var_dt_hr_registo, 'dd-mm-yyyy') || CHR (10) || var_texto || CHR (10);

        IF texto_completo IS NOT NULL
        THEN
            v_rec.t_doente := var_t_doente;
            v_rec.doente := var_doente;
            v_rec.t_episodio := var_t_episodio;
            v_rec.episodio := var_episodio;
            v_rec.n_mecan := get_n_mecan (var_utilizador);
            v_rec.user_sys := var_utilizador;
            v_rec.dt_registo := var_dt_hr_registo;
            v_rec.id_bd := var_id_combur;
            v_rec.id_resumo := 'COMBUR#' || var_id_combur;
            v_rec.digest := NULL;
            v_rec.texto_completo := texto_completo;
            v_rec.has_more_than_digest := 'S';
        END IF;

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_COMBUR_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_combur (i_t_doente      VARCHAR2,
                         i_doente        VARCHAR2,
                         i_t_episodio    VARCHAR2,
                         i_episodio      VARCHAR2,
                         i_cod_serv      VARCHAR2,
                         i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec            pck_types_nv.r_resumo;
        v_combur         pck_types_nv.t_resumo;

        texto_completo   VARCHAR (2000);

        CURSOR combur
        IS
            SELECT c.id_combur,
                   c.t_episodio,
                   c.episodio,
                   c.t_doente,
                   c.doente,
                   c.dt_hr_registo,
                   c.obs,
                   NVL (c.user_act, c.user_cri) utilizador
              FROM sd_enf_combur c
             WHERE c.dt_hr_registo IS NOT NULL AND c.t_doente = i_t_doente AND c.doente = i_doente AND c.t_episodio = i_t_episodio AND c.episodio = i_episodio;

        CURSOR combur_det
        IS
              SELECT c.id_combur, gc.descr_item || ': ' || pck_enf_resumo_epr.convert_combur_notation (cd.valor) texto
                FROM sd_enf_combur c, sd_enf_combur_det cd, gr_enf_combur gc
               WHERE     c.id_combur = cd.id_combur
                     AND gc.cod_item = cd.cod_item
                     AND c.dt_hr_registo IS NOT NULL
                     AND c.t_doente = i_t_doente
                     AND c.doente = i_doente
                     AND c.t_episodio = i_t_episodio
                     AND c.episodio = i_episodio
            ORDER BY c.id_combur, cd.cod_item DESC;
    BEGIN
        FOR reg IN combur
        LOOP
            texto_completo := TO_CHAR (reg.dt_hr_registo, 'dd-mm-yyyy') || CHR (10);

            FOR det IN combur_det
            LOOP
                IF det.id_combur = reg.id_combur
                THEN
                    texto_completo := texto_completo || det.texto || CHR (10);
                END IF;
            END LOOP;

            IF texto_completo IS NOT NULL
            THEN
                v_rec :=
                    create_base_rec (reg.t_doente,
                                     reg.doente,
                                     reg.t_episodio,
                                     reg.episodio,
                                     NULL,
                                     reg.utilizador,
                                     get_n_mecan (reg.utilizador),
                                     '',
                                     reg.dt_hr_registo);
                v_rec.id_bd := reg.id_combur;
                v_rec.id_resumo := 'COMBUR#' || reg.id_combur;
                v_rec.digest := NULL;
                v_rec.texto_completo := texto_completo;
                v_rec.has_more_than_digest := 'S';
                v_combur (v_combur.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_combur;
    END;

    --******************************************************************************
    --******************************************************************************
    FUNCTION get_triagens_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id VARCHAR2)
        RETURN pck_types_nv.r_resumo
    IS
        texto_prioridade      VARCHAR (200) := traduz ('Prioridade');
        texto_queixa          VARCHAR (200) := traduz ('Queixa');
        texto_fluxograma      VARCHAR (200) := traduz ('Fluxograma');
        texto_discriminador   VARCHAR (200) := traduz ('Discriminador');
        texto_destino         VARCHAR (200) := traduz ('Destino');
        texto_obs             VARCHAR2 (200) := traduz ('Observa��es');

        temp_texto            VARCHAR2 (4000);
        v_rec                 pck_types_nv.r_resumo;
    BEGIN
        SELECT t_doente,
               doente,
               t_episodio,
               episodio,
               cod_serv,
               get_n_mecan (utilizador),
               utilizador,
               dt_registo,
               n_triag,
               'TRIAGEM#' || n_triag,
               NULL,
               obs,
               'S',
               texto
          INTO v_rec.t_doente,
               v_rec.doente,
               v_rec.t_episodio,
               v_rec.episodio,
               v_rec.cod_serv,
               v_rec.n_mecan,
               v_rec.user_sys,
               v_rec.dt_registo,
               v_rec.id_bd,
               v_rec.id_resumo,
               v_rec.digest,
               v_rec.texto_completo,
               v_rec.has_more_than_digest,
               temp_texto
          FROM (SELECT                                                                                                                                          /* TEXTO_COMPLETO */
                      texto_prioridade
                       || ': '
                       || NVL (prioridade, cor)
                       || CHR (10)
                       || texto_queixa
                       || ': '
                       || queixa
                       || '; '
                       || NVL2 (sintoma_manchester, texto_fluxograma || ': ' || sintoma_manchester || '; ', '')
                       || NVL2 (item_manchester, texto_discriminador || ': ' || item_manchester || '; ', '')
                       || CHR (10)
                       || NVL2 (destino, texto_destino || ': ' || destino, '')
                       || CHR (10)
                       || sinais_vitais
                           texto,
                          /* DIGEST */
                          texto_prioridade
                       || ': '
                       || NVL (prioridade, cor)
                       || ' '
                       || NVL2 (sintoma_manchester, texto_fluxograma || ': ' || sintoma_manchester || ' ;', '')
                       || NVL2 (item_manchester, texto_discriminador || ': ' || item_manchester || '; ', '')
                       || CHR (10)
                       || NVL2 (destino, texto_destino || ': ' || destino, '')
                           resumo,
                       NVL (user_act, user_cri) utilizador,
                       obs,
                       dt_registo,
                       t_doente,
                       doente,
                       t_episodio,
                       episodio,
                       n_triag,
                       cod_serv
                  FROM (SELECT q0.t_doente,
                               q0.doente,
                               q0.t_episodio,
                               q0.episodio,
                               q1.descr_prioridade prioridade,
                               q1.cor,
                               q0.queixa,
                               q0.obs obs,
                               sdf_enf_obter_param_det ('TRIAGEM_DESTINOS', destino) destino,
                               q2.cod_serv,
                               (SELECT q3.cod_sintoma || ' - ' || q4.descr_sintoma
                                  FROM sd_enf_tr_sintomas q3, gr_enf_tr_sintomas q4
                                 WHERE     q3.t_doente = q0.t_doente
                                       AND q3.doente = q0.doente
                                       AND q3.t_episodio = q0.t_episodio
                                       AND q3.episodio = q0.episodio
                                       AND q3.cod_sintoma = q4.cod_sintoma
                                       AND q3.n_triag = q0.n_triag)
                                   sintoma_manchester,
                               (SELECT q6.descr_item
                                  FROM sd_enf_tr_items q5, gr_enf_tr_items q6
                                 WHERE     q5.t_doente = q0.t_doente
                                       AND q5.doente = q0.doente
                                       AND q5.t_episodio = q0.t_episodio
                                       AND q5.episodio = q0.episodio
                                       AND q5.cod_item = q6.cod_item
                                       AND q5.n_triag = q0.n_triag
                                       AND q5.valor = 'S')
                                   item_manchester,
                               concatenate_sin_vitais (q0.t_doente,
                                                       q0.doente,
                                                       q0.t_episodio,
                                                       q0.episodio,
                                                       q0.n_triag)
                                   sinais_vitais,
                               q0.user_cri,
                               q0.user_act,
                               q0.dt_registo,
                               q0.n_triag
                          FROM sd_enf_tr_prioridade q0, gr_enf_tr_prioridades q1, urg_dados_adic q2
                         WHERE     q0.t_doente || '#' || q0.doente || '#' || q0.t_episodio || '#' || q0.episodio || '#' || q0.n_triag = i_id
                               AND q0.t_doente = i_t_doente
                               AND q0.doente = i_doente
                               AND q0.t_doente = q2.t_doente
                               AND q0.doente = q2.doente
                               AND q0.t_episodio = q2.t_episodio
                               AND q0.episodio = q2.episodio
                               AND q0.cod_prioridade = q1.cod_prioridade));

        IF v_rec.texto_completo IS NOT NULL
        THEN
            v_rec.texto_completo := temp_texto || CHR (10) || texto_obs || ': ' || v_rec.texto_completo;
        ELSE
            v_rec.texto_completo := temp_texto;
        END IF;

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_TRIAGENS_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_triagens (i_t_doente      VARCHAR2,
                           i_doente        VARCHAR2,
                           i_t_episodio    VARCHAR2,
                           i_episodio      VARCHAR2,
                           i_cod_serv      VARCHAR2,
                           i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        texto_prioridade      VARCHAR (200) := traduz ('Prioridade');
        texto_queixa          VARCHAR (200) := traduz ('Queixa');
        texto_fluxograma      VARCHAR (200) := traduz ('Fluxograma');
        texto_discriminador   VARCHAR (200) := traduz ('Discriminador');
        texto_destino         VARCHAR (200) := traduz ('Destino');
        texto_obs             VARCHAR2 (200) := traduz ('Observa��es');

        --w_OBS                 VARCHAR2(4000) := 'observa��es';
        CURSOR triagens
        IS
            SELECT                                                                                                                                              /* TEXTO_COMPLETO */
                  texto_prioridade
                   || ': '
                   || NVL (prioridade, cor)
                   || CHR (10)
                   || texto_queixa
                   || ': '
                   || queixa
                   || '; '
                   || NVL2 (sintoma_manchester, texto_fluxograma || ': ' || sintoma_manchester || '; ', '')
                   || NVL2 (item_manchester, texto_discriminador || ': ' || item_manchester || '; ', '')
                   || CHR (10)
                   || NVL2 (destino, texto_destino || ': ' || destino, '')
                   || CHR (10)
                   || sinais_vitais
                       texto,
                      /* DIGEST */
                      texto_prioridade
                   || ': '
                   || NVL (prioridade, cor)
                   || ' '
                   || NVL2 (sintoma_manchester, texto_fluxograma || ': ' || sintoma_manchester || ' ;', '')
                   || NVL2 (item_manchester, texto_discriminador || ': ' || item_manchester || '; ', '')
                   || CHR (10)
                   || NVL2 (destino, texto_destino || ': ' || destino, '')
                       resumo,
                   NVL (user_act, user_cri) utilizador,
                   obs,
                   dt_registo,
                   t_doente,
                   doente,
                   t_episodio,
                   episodio,
                   n_triag,
                   cod_serv
              FROM (SELECT q0.t_doente,
                           q0.doente,
                           q0.t_episodio,
                           q0.episodio,
                           q1.descr_prioridade prioridade,
                           q1.cor,
                           q0.queixa--,SDF_ENF_SUBSTR_LOB(q0.obs,400,0) OBS

                           ,
                           q0.obs obs,
                           sdf_enf_obter_param_det ('TRIAGEM_DESTINOS', destino) destino,
                           q2.cod_serv,
                           (SELECT q3.cod_sintoma || ' - ' || q4.descr_sintoma
                              FROM sd_enf_tr_sintomas q3, gr_enf_tr_sintomas q4
                             WHERE     q3.t_doente = q0.t_doente
                                   AND q3.doente = q0.doente
                                   AND q3.t_episodio = q0.t_episodio
                                   AND q3.episodio = q0.episodio
                                   AND q3.cod_sintoma = q4.cod_sintoma
                                   AND q3.n_triag = q0.n_triag)
                               sintoma_manchester,
                           (SELECT q6.descr_item
                              FROM sd_enf_tr_items q5, gr_enf_tr_items q6
                             WHERE     q5.t_doente = q0.t_doente
                                   AND q5.doente = q0.doente
                                   AND q5.t_episodio = q0.t_episodio
                                   AND q5.episodio = q0.episodio
                                   AND q5.cod_item = q6.cod_item
                                   AND q5.n_triag = q0.n_triag
                                   AND q5.valor = 'S')
                               item_manchester,
                           concatenate_sin_vitais (q0.t_doente,
                                                   q0.doente,
                                                   q0.t_episodio,
                                                   q0.episodio,
                                                   q0.n_triag)
                               sinais_vitais,
                           q0.user_cri,
                           q0.user_act,
                           q0.dt_registo,
                           q0.n_triag
                      FROM sd_enf_tr_prioridade q0, gr_enf_tr_prioridades q1, urg_dados_adic q2
                     WHERE     q0.t_doente = i_t_doente
                           AND q0.doente = i_doente
                           AND q0.t_episodio = i_t_episodio
                           AND q0.episodio = i_episodio
                           AND q0.t_doente = q2.t_doente
                           AND q0.doente = q2.doente
                           AND q0.t_episodio = q2.t_episodio
                           AND q0.episodio = q2.episodio
                           AND q0.cod_prioridade = q1.cod_prioridade);

        v_rec                 pck_types_nv.r_resumo;
        v_triagens            pck_types_nv.t_resumo;
    BEGIN
        FOR a IN triagens
        LOOP
            IF a.texto IS NOT NULL
            THEN
                --      IF a.obs IS NOT NULL THEN
                --              w_OBS   :=      SDF_ENF_SUBSTR_LOB(TEXTO=> a.OBS, LIM_MAX=> 4000, LIM_MIN=> 0);
                --      END IF;
                v_rec :=
                    create_base_rec (a.t_doente,
                                     a.doente,
                                     a.t_episodio,
                                     a.episodio,
                                     a.cod_serv,
                                     a.utilizador,
                                     get_n_mecan (a.utilizador),
                                     '',
                                     a.dt_registo);
                v_rec.id_bd := a.n_triag;
                v_rec.id_resumo := 'TRIAGEM#' || a.n_triag;
                v_rec.digest := NULL;

                IF a.obs IS NOT NULL
                THEN
                    v_rec.texto_completo := a.texto || CHR (10) || texto_obs || ': ' || a.obs;
                ELSE
                    v_rec.texto_completo := a.texto;
                END IF;

                v_rec.has_more_than_digest := 'S';
                v_triagens (v_triagens.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_triagens;
    END;

    --******************************************************************************
    --******************************************************************************
    FUNCTION get_aval_inicial_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec   pck_types_nv.r_resumo;
        texto   VARCHAR2 (30000);
    BEGIN
        SELECT t_doente,
               doente,
               t_episodio,
               episodio,
               get_n_mecan (utilizador),
               utilizador,
               data,
               n_ep_arv,
               'AVALINICIAL#' || n_ep_arv,
               LTRIM (pck_enf_resumo_epr.concatenate_resumo_ai (t_doente,
                                                                doente,
                                                                t_episodio,
                                                                episodio),
                      CHR (10))
          INTO v_rec.t_doente,
               v_rec.doente,
               v_rec.t_episodio,
               v_rec.episodio,
               v_rec.n_mecan,
               v_rec.user_sys,
               v_rec.dt_registo,
               v_rec.id_bd,
               v_rec.id_resumo,
               v_rec.texto_completo
          FROM (SELECT t.t_doente,
                       t.doente,
                       t.t_episodio,
                       t.episodio,
                       NVL (t.user_act, t.user_cri) utilizador,
                       t.n_ep_arv,
                       NVL (t.dt_act, t.dt_cri) data
                  FROM sd_enf_ep_arv t
                 WHERE t.doente = i_doente AND t.t_doente = i_t_doente AND t.n_ep_arv = i_id);

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_AVAL_INICIAL_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_aval_inicial (i_t_doente      VARCHAR2,
                               i_doente        VARCHAR2,
                               i_t_episodio    VARCHAR2,
                               i_episodio      VARCHAR2,
                               i_cod_serv      VARCHAR2,
                               i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec    pck_types_nv.r_resumo;
        v_aval   pck_types_nv.t_resumo;
        texto    VARCHAR2 (30000);

        CURSOR avals
        IS
            SELECT t.t_doente,
                   t.doente,
                   t.t_episodio,
                   t.episodio,
                   NVL (t.user_act, t.user_cri) utilizador,
                   t.n_ep_arv,
                   NVL (t.dt_act, t.dt_cri) data
              FROM sd_enf_ep_arv t
             WHERE t.doente = i_doente AND t.t_doente = i_t_doente AND t.t_episodio = i_t_episodio AND t.episodio = i_episodio;
    BEGIN
        FOR av IN avals
        LOOP
            texto :=
                pck_enf_resumo_epr.concatenate_resumo_ai (av.t_doente,
                                                          av.doente,
                                                          av.t_episodio,
                                                          av.episodio);

            IF texto IS NOT NULL
            THEN
                v_rec :=
                    create_base_rec (av.t_doente,
                                     av.doente,
                                     av.t_episodio,
                                     av.episodio,
                                     NULL,
                                     av.utilizador,
                                     get_n_mecan (av.utilizador),
                                     '',
                                     av.data);
                v_rec.id_bd := av.n_ep_arv;
                v_rec.id_resumo := 'AVALINICIAL#' || av.n_ep_arv;
                v_rec.texto_completo := LTRIM (texto, CHR (10));
                v_aval (v_aval.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_aval;
    END;

    --******************************************************************************
    --******************************************************************************

    FUNCTION get_vpc_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec              pck_types_nv.r_resumo;

        CURSOR registos_vpc (
            arg_cod_reg_qdr    NUMBER)
        IS
            SELECT qei.descr_item,
                   qesi.descr_sub_item,
                   vcd.activo2,
                   vcd.activo,
                   vcd.cod_reg_qdr,
                   vcd.user_activo1,
                   vcd.user_activo2,
                   TO_CHAR (vcd.dt_activo1, 'dd-mm-yyyy hh24:mi') dt_activo1,
                   TO_CHAR (vcd.dt_activo2, 'dd-mm-yyyy hh24:mi') dt_activo2
              FROM sd_enf_verif_cirurg_det vcd,
                   sd_enf_verif_cirurg qvc,
                   gr_enf_qdr_espec_item qei,
                   gr_enf_qdr_espec_sub_item qesi
             WHERE     vcd.cod_reg_qdr = qvc.cod_reg_qdr
                   AND qvc.cod_reg_qdr = arg_cod_reg_qdr
                   AND qvc.t_doente = i_t_doente
                   AND qvc.doente = i_doente
                   AND qvc.cod_reg_qdr = i_id
                   AND vcd.cod_qdr = qei.cod_qdr
                   AND vcd.cod_item = qei.cod_item
                   AND vcd.cod_qdr = qesi.cod_qdr
                   AND vcd.cod_item = qesi.cod_item
                   AND qesi.cod_sub_item = vcd.cod_sub_item
                   AND (vcd.activo2 = 'S' OR vcd.activo = 'S');

        CURSOR vpc_terap_adm (
            arg_cod_reg_qdr    NUMBER)
        IS
              SELECT TO_CHAR (t1.hora_adm, 'RRRR-MM-DD HH24:MI') hora,
                     NVL (t2.nome_cient, t1.nome_med_adm) nome,
                     TO_CHAR (t1.dose, 'FM999990.00') || ' ' || t1.unid_med dose,
                     obs_adm obs,
                     svc.cod_reg_qdr,
                     t1.enf utilizador
                FROM ph_med_adm t1, ph_medicamentos t2, sd_enf_verif_cirurg svc
               WHERE     t1.medicamento = t2.medicamento(+)
                     AND t1.t_doente = svc.t_doente
                     AND t1.doente = svc.doente
                     AND svc.cod_reg_qdr = i_id
                     AND svc.cod_reg_qdr = arg_cod_reg_qdr
                     AND svc.t_doente = i_t_doente
                     AND svc.doente = i_doente
                     AND t1.hora_adm BETWEEN TRUNC (svc.dt_registo) - NVL (sdf_enf_obter_param ('VPC_MEDIC_DIAS_ANTES'), '0') AND svc.dt_registo
            ORDER BY t1.hora_adm DESC;

        CURSOR vpc_terap_n_adm (
            arg_cod_reg_qdr    NUMBER)
        IS
              SELECT TO_CHAR (t1.hora, 'RRRR-MM-DD HH24:MI') hora,
                     NVL (t2.nome_cient, t1.nome_med_adm) nome,
                     TO_CHAR (t1.dose, 'FM999990.00') || ' ' || t1.unid_med dose,
                     t1.desc_justif obs,
                     svc.cod_reg_qdr,
                     t1.resp utilizador
                FROM ph_med_n_adm t1, ph_medicamentos t2, sd_enf_verif_cirurg svc
               WHERE     t1.medicamento = t2.medicamento(+)
                     AND t1.t_doente = svc.t_doente
                     AND t1.doente = svc.doente
                     AND svc.cod_reg_qdr = arg_cod_reg_qdr
                     AND svc.t_doente = i_t_doente
                     AND svc.doente = i_doente
                     AND svc.cod_reg_qdr = i_id
                     AND t1.hora BETWEEN TRUNC (svc.dt_registo) - NVL (sdf_enf_obter_param ('VPC_MEDIC_DIAS_ANTES'), '0') AND svc.dt_registo
            ORDER BY t1.hora DESC;

        texto_inicial      VARCHAR (200);
        texto_completo     VARCHAR2 (32000);
        texto_servico      VARCHAR2 (500) := traduz ('Servi�o');
        texto_bloco        VARCHAR2 (500) := traduz ('Bloco');
        flg_terap_adm      NUMBER := 0;
        flg_terap_n_adm    NUMBER := 0;

        var_doente         VARCHAR (100);
        var_t_doente       VARCHAR (100);
        var_episodio       VARCHAR (100);
        var_t_episodio     VARCHAR (100);
        var_dt_registo     DATE;
        var_observ         VARCHAR (2000);
        var_utilizador     VARCHAR (2000);
        var_cirurgia_txt   VARCHAR (2000);
        var_descr_qdr      VARCHAR (100);
        var_cod_reg_qdr    NUMBER;
        var_cod_qdr        VARCHAR (100);
    BEGIN
        SELECT svg.t_doente,
               svg.doente,
               svg.t_episodio,
               svg.episodio,
               svg.dt_registo,
               svg.observ,
               NVL (svg.user_act, svg.user_cri) utilizador,
               pck_enf_vpc_tools.obtem_cirurgias (svg.cod_reg_qdr, svg.id_cir, svg.cirurgia_txt) cirurgia_txt,
               gqe.descr_qdr,
               svg.cod_reg_qdr,
               gqe.cod_qdr
          INTO var_t_doente,
               var_doente,
               var_t_episodio,
               var_episodio,
               var_dt_registo,
               var_observ,
               var_utilizador,
               var_cirurgia_txt,
               var_descr_qdr,
               var_cod_reg_qdr,
               var_cod_qdr
          FROM sd_enf_verif_cirurg svg, gr_enf_qdr_espec gqe
         WHERE svg.t_doente = i_t_doente AND svg.doente = i_doente AND svg.cod_qdr = gqe.cod_qdr;

        IF var_cirurgia_txt IS NULL
        THEN
            texto_inicial := var_descr_qdr || CHR (10);
        ELSE
            texto_inicial := var_cirurgia_txt || CHR (10);
        END IF;

        texto_completo := '';

        FOR rvpc IN registos_vpc (i_id)
        LOOP
            IF NVL (sdf_enf_obter_param_det ('VPC_DUPLA', var_cod_qdr), 'N') = 'S'
            THEN
                IF (rvpc.activo = 'S' AND rvpc.activo2 = 'S')
                THEN
                    texto_completo :=
                           texto_completo
                        || rvpc.descr_item
                        || ': '
                        || rvpc.descr_sub_item
                        || ' ('
                        || texto_servico
                        || ' ('
                        || rvpc.user_activo1
                        || ' '
                        || rvpc.dt_activo1
                        || ')'
                        || ', '
                        || texto_bloco
                        || ' ('
                        || rvpc.user_activo2
                        || ' '
                        || rvpc.dt_activo2
                        || ')'
                        || ')'
                        || ';'
                        || CHR (10);
                ELSIF (rvpc.activo = 'S')
                THEN
                    texto_completo :=
                           texto_completo
                        || rvpc.descr_item
                        || ': '
                        || rvpc.descr_sub_item
                        || ' ('
                        || texto_servico
                        || ' ('
                        || rvpc.user_activo1
                        || ' '
                        || rvpc.dt_activo1
                        || ')'
                        || ')'
                        || ';'
                        || CHR (10);
                ELSIF (rvpc.activo2 = 'S')
                THEN
                    texto_completo :=
                           texto_completo
                        || rvpc.descr_item
                        || ': '
                        || rvpc.descr_sub_item
                        || ' ('
                        || texto_bloco
                        || ' ('
                        || rvpc.user_activo2
                        || ' '
                        || rvpc.dt_activo2
                        || ')'
                        || ')'
                        || ';'
                        || CHR (10);
                END IF;
            ELSE
                texto_completo := texto_completo || rvpc.descr_item || ': ' || rvpc.descr_sub_item || ' (' || rvpc.user_activo1 || ' ' || rvpc.dt_activo1 || ');' || CHR (10);
            END IF;
        END LOOP;

        flg_terap_adm := 0;

        FOR terap IN vpc_terap_adm (i_id)
        LOOP
            IF flg_terap_adm = 0
            THEN
                texto_completo := texto_completo || traduz ('Terap�utica Administrada') || ':' || CHR (10);
                flg_terap_adm := 1;
            END IF;

            texto_completo := texto_completo || terap.nome || ' ' || terap.dose || ' (' || terap.utilizador || ' ' || terap.hora || ')' || ';' || CHR (10);
        END LOOP;

        flg_terap_n_adm := 0;

        FOR n_terap IN vpc_terap_n_adm (i_id)
        LOOP
            IF flg_terap_n_adm = 0
            THEN
                texto_completo := texto_completo || traduz ('Terap�utica N�o Administrada') || ':' || CHR (10);
                flg_terap_n_adm := 1;
            END IF;

            texto_completo :=
                texto_completo || n_terap.nome || ' ' || n_terap.dose || ' (' || n_terap.obs || ')' || ' (' || n_terap.utilizador || ' ' || n_terap.hora || ');' || CHR (10);
        END LOOP;

        v_rec.t_doente := var_t_doente;
        v_rec.doente := var_doente;
        v_rec.t_episodio := var_t_episodio;
        v_rec.episodio := var_episodio;
        v_rec.n_mecan := get_n_mecan (var_utilizador);
        v_rec.dt_registo := var_dt_registo;
        v_rec.id_bd := var_cod_reg_qdr;
        v_rec.id_resumo := 'VPC#' || var_cod_reg_qdr;
        v_rec.digest := NULL;
        v_rec.texto_completo := var_descr_qdr || CHR (10) || CHR (10) || texto_completo;
        v_rec.dt_registo_display := TO_CHAR (var_dt_registo, 'dd-mm-yyyy');
        v_rec.has_more_than_digest := 'S';

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.v_rec pck_types_nv.r_resumo', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_vpc (i_t_doente      VARCHAR2,
                      i_doente        VARCHAR2,
                      i_t_episodio    VARCHAR2,
                      i_episodio      VARCHAR2,
                      i_cod_serv      VARCHAR2,
                      i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec             pck_types_nv.r_resumo;
        v_vpc             pck_types_nv.t_resumo;

        CURSOR vpc
        IS
            SELECT svg.t_doente,
                   svg.doente,
                   svg.t_episodio,
                   svg.episodio,
                   svg.dt_registo,
                   svg.observ,
                   NVL (svg.user_act, svg.user_cri) utilizador,
                   pck_enf_vpc_tools.obtem_cirurgias (svg.cod_reg_qdr, svg.id_cir, svg.cirurgia_txt) cirurgia_txt,
                   gqe.descr_qdr,
                   svg.cod_reg_qdr,
                   gqe.cod_qdr
              FROM sd_enf_verif_cirurg svg, gr_enf_qdr_espec gqe
             WHERE svg.t_doente = i_t_doente AND svg.doente = i_doente AND svg.cod_qdr = gqe.cod_qdr;

        CURSOR registos_vpc (
            arg_cod_reg_qdr    NUMBER)
        IS
            SELECT qei.descr_item,
                   qesi.descr_sub_item,
                   vcd.activo2,
                   vcd.activo,
                   vcd.cod_reg_qdr,
                   vcd.user_activo1,
                   vcd.user_activo2,
                   TO_CHAR (vcd.dt_activo1, 'dd-mm-yyyy hh24:mi') dt_activo1,
                   TO_CHAR (vcd.dt_activo2, 'dd-mm-yyyy hh24:mi') dt_activo2
              FROM sd_enf_verif_cirurg_det vcd,
                   sd_enf_verif_cirurg qvc,
                   gr_enf_qdr_espec_item qei,
                   gr_enf_qdr_espec_sub_item qesi
             WHERE     vcd.cod_reg_qdr = qvc.cod_reg_qdr
                   AND qvc.cod_reg_qdr = arg_cod_reg_qdr
                   AND qvc.t_doente = i_t_doente
                   AND qvc.doente = i_doente
                   AND vcd.cod_qdr = qei.cod_qdr
                   AND vcd.cod_item = qei.cod_item
                   AND vcd.cod_qdr = qesi.cod_qdr
                   AND vcd.cod_item = qesi.cod_item
                   AND qesi.cod_sub_item = vcd.cod_sub_item
                   AND (vcd.activo2 = 'S' OR vcd.activo = 'S');

        CURSOR vpc_terap_adm (
            arg_cod_reg_qdr    NUMBER)
        IS
              SELECT TO_CHAR (t1.hora_adm, 'RRRR-MM-DD HH24:MI') hora,
                     NVL (t2.nome_cient, t1.nome_med_adm) nome,
                     TO_CHAR (t1.dose, 'FM999990.00') || ' ' || t1.unid_med dose,
                     obs_adm obs,
                     svc.cod_reg_qdr,
                     t1.enf utilizador
                FROM ph_med_adm t1, ph_medicamentos t2, sd_enf_verif_cirurg svc
               WHERE     t1.medicamento = t2.medicamento(+)
                     AND t1.t_doente = svc.t_doente
                     AND t1.doente = svc.doente
                     AND svc.cod_reg_qdr = arg_cod_reg_qdr
                     AND svc.t_doente = i_t_doente
                     AND svc.doente = i_doente
                     AND t1.hora_adm BETWEEN TRUNC (svc.dt_registo) - NVL (sdf_enf_obter_param ('VPC_MEDIC_DIAS_ANTES'), '0') AND svc.dt_registo
            ORDER BY t1.hora_adm DESC;

        CURSOR vpc_terap_n_adm (
            arg_cod_reg_qdr    NUMBER)
        IS
              SELECT TO_CHAR (t1.hora, 'RRRR-MM-DD HH24:MI') hora,
                     NVL (t2.nome_cient, t1.nome_med_adm) nome,
                     TO_CHAR (t1.dose, 'FM999990.00') || ' ' || t1.unid_med dose,
                     t1.desc_justif obs,
                     svc.cod_reg_qdr,
                     t1.resp utilizador
                FROM ph_med_n_adm t1, ph_medicamentos t2, sd_enf_verif_cirurg svc
               WHERE     t1.medicamento = t2.medicamento(+)
                     AND t1.t_doente = svc.t_doente
                     AND t1.doente = svc.doente
                     AND svc.cod_reg_qdr = arg_cod_reg_qdr
                     AND svc.t_doente = i_t_doente
                     AND svc.doente = i_doente
                     AND t1.hora BETWEEN TRUNC (svc.dt_registo) - NVL (sdf_enf_obter_param ('VPC_MEDIC_DIAS_ANTES'), '0') AND svc.dt_registo
            ORDER BY t1.hora DESC;

        texto_inicial     VARCHAR (200);
        texto_completo    VARCHAR2 (32000);
        texto_servico     VARCHAR2 (500) := traduz ('Servi�o');
        texto_bloco       VARCHAR2 (500) := traduz ('Bloco');
        flg_terap_adm     NUMBER := 0;
        flg_terap_n_adm   NUMBER := 0;
    BEGIN
        FOR v IN vpc
        LOOP
            IF v.cirurgia_txt IS NULL
            THEN
                texto_inicial := v.descr_qdr || CHR (10);
            ELSE
                texto_inicial := v.cirurgia_txt || CHR (10);
            END IF;

            texto_completo := '';

            FOR rvpc IN registos_vpc (v.cod_reg_qdr)
            LOOP
                IF rvpc.cod_reg_qdr = v.cod_reg_qdr
                THEN
                    IF NVL (sdf_enf_obter_param_det ('VPC_DUPLA', v.cod_qdr), 'N') = 'S'
                    THEN
                        IF (rvpc.activo = 'S' AND rvpc.activo2 = 'S')
                        THEN
                            texto_completo :=
                                   texto_completo
                                || rvpc.descr_item
                                || ': '
                                || rvpc.descr_sub_item
                                || ' ('
                                || texto_servico
                                || ' ('
                                || rvpc.user_activo1
                                || ' '
                                || rvpc.dt_activo1
                                || ')'
                                || ', '
                                || texto_bloco
                                || ' ('
                                || rvpc.user_activo2
                                || ' '
                                || rvpc.dt_activo2
                                || ')'
                                || ')'
                                || ';'
                                || CHR (10);
                        ELSIF (rvpc.activo = 'S')
                        THEN
                            texto_completo :=
                                   texto_completo
                                || rvpc.descr_item
                                || ': '
                                || rvpc.descr_sub_item
                                || ' ('
                                || texto_servico
                                || ' ('
                                || rvpc.user_activo1
                                || ' '
                                || rvpc.dt_activo1
                                || ')'
                                || ')'
                                || ';'
                                || CHR (10);
                        ELSIF (rvpc.activo2 = 'S')
                        THEN
                            texto_completo :=
                                   texto_completo
                                || rvpc.descr_item
                                || ': '
                                || rvpc.descr_sub_item
                                || ' ('
                                || texto_bloco
                                || ' ('
                                || rvpc.user_activo2
                                || ' '
                                || rvpc.dt_activo2
                                || ')'
                                || ')'
                                || ';'
                                || CHR (10);
                        END IF;
                    ELSE
                        texto_completo :=
                            texto_completo || rvpc.descr_item || ': ' || rvpc.descr_sub_item || ' (' || rvpc.user_activo1 || ' ' || rvpc.dt_activo1 || ');' || CHR (10);
                    END IF;
                END IF;
            END LOOP;

            flg_terap_adm := 0;

            FOR terap IN vpc_terap_adm (v.cod_reg_qdr)
            LOOP
                IF terap.cod_reg_qdr = v.cod_reg_qdr
                THEN
                    IF flg_terap_adm = 0
                    THEN
                        texto_completo := texto_completo || traduz ('Terap�utica Administrada') || ':' || CHR (10);
                        flg_terap_adm := 1;
                    END IF;

                    texto_completo := texto_completo || terap.nome || ' ' || terap.dose || ' (' || terap.utilizador || ' ' || terap.hora || ')' || ';' || CHR (10);
                END IF;
            END LOOP;

            flg_terap_n_adm := 0;

            FOR n_terap IN vpc_terap_n_adm (v.cod_reg_qdr)
            LOOP
                IF n_terap.cod_reg_qdr = v.cod_reg_qdr
                THEN
                    IF flg_terap_n_adm = 0
                    THEN
                        texto_completo := texto_completo || traduz ('Terap�utica N�o Administrada') || ':' || CHR (10);
                        flg_terap_n_adm := 1;
                    END IF;

                    texto_completo :=
                           texto_completo
                        || n_terap.nome
                        || ' '
                        || n_terap.dose
                        || ' ('
                        || n_terap.obs
                        || ')'
                        || ' ('
                        || n_terap.utilizador
                        || ' '
                        || n_terap.hora
                        || ');'
                        || CHR (10);
                END IF;
            END LOOP;

            IF texto_completo IS NOT NULL
            THEN
                v_rec :=
                    create_base_rec (v.t_doente,
                                     v.doente,
                                     v.t_episodio,
                                     v.episodio,
                                     NULL,
                                     v.utilizador,
                                     get_n_mecan (v.utilizador),
                                     '',
                                     v.dt_registo);
                v_rec.id_bd := v.cod_reg_qdr;
                v_rec.id_resumo := 'VPC#' || v.cod_reg_qdr;
                v_rec.digest := NULL;
                v_rec.texto_completo := v.descr_qdr || CHR (10) || CHR (10) || texto_completo;
                v_rec.dt_registo_display := TO_CHAR (v.dt_registo, 'dd-mm-yyyy');
                v_rec.has_more_than_digest := 'S';
                v_vpc (v_vpc.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_vpc;
    END;

    --******************************************************************************
    --******************************************************************************

    FUNCTION get_aval_intra_oper_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec                  pck_types_nv.r_resumo;

        /*DADOS DA AVALIA��O E CIRURGIA*/
        CURSOR aval_intra_oper
        IS
            SELECT aci.descricao,
                   asaida.t_doente,
                   asaida.doente,
                   asaida.t_episodio,
                   asaida.episodio,
                   asaida.n_reg_oper,
                   asaida.data_hora,
                   NVL (asaida.user_act, asaida.user_cri) utilizador,
                   sro.cod_serv
              FROM sd_enf_avaliacao_saida asaida,
                   sd_reg_oper_det rod,
                   ana_cid_interv aci,
                   sd_reg_oper sro
             WHERE     asaida.id_aval = i_id
                   AND asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asaida.n_reg_oper = rod.n_reg_oper(+)
                   AND rod.n_reg_oper = sro.n_reg_oper(+)
                   AND rod.cod_oper = aci.codigo(+)
                   AND rod.codificacao = aci.codificacao(+);

        /*DADOS DAS DRENAGENS*/
        CURSOR drenagens
        IS
            SELECT gd.descricao, asd.observacao, asaida.n_reg_oper
              FROM sd_enf_aval_saida_drenagens asd, gr_enf_drenos gd, sd_enf_avaliacao_saida asaida
             WHERE asaida.id_aval = i_id AND asaida.t_doente = i_t_doente AND asaida.doente = i_doente AND asd.cod_dreno = gd.codigo AND asd.id_aval = asaida.id_aval;

        /*DADOS GARROTE*/
        CURSOR garrote
        IS
            SELECT ge.descr_garr,
                   asg.pressao,
                   asg.dt_ini,
                   asg.dt_fim,
                   asaida.n_reg_oper
              FROM sd_enf_aval_saida_garrote asg, gr_enf_garrote ge, sd_enf_avaliacao_saida asaida
             WHERE asaida.id_aval = i_id AND asaida.t_doente = i_t_doente AND asaida.doente = i_doente AND asg.cod_garr = ge.cod_garr AND asg.id_aval = asaida.id_aval;

        /*DADOS COMPRESSAS*/
        CURSOR compressas
        IS
            SELECT ec.descricao,
                   com.dadas,
                   com.recebidas,
                   com.mesa,
                   asaida.n_reg_oper
              FROM sd_enf_aval_saida_compressas com, gr_enf_compressas ec, sd_enf_avaliacao_saida asaida
             WHERE asaida.id_aval = i_id AND asaida.t_doente = i_t_doente AND asaida.doente = i_doente AND com.tipo_compressa = ec.tipo_compressa AND com.id_aval = asaida.id_aval;

        CURSOR posicionamento
        IS
            SELECT gp.descr_posi, asp.descr_aux, asaida.n_reg_oper
              FROM sd_enf_aval_saida_posi asp, gr_enf_posic gp, sd_enf_avaliacao_saida asaida
             WHERE asaida.id_aval = i_id AND asaida.t_doente = i_t_doente AND asaida.doente = i_doente AND asp.cod_posi = gp.cod_posi AND asp.id_aval = asaida.id_aval;

        CURSOR electro
        IS
            SELECT pck_enf_resumo_epr.get_locais_electro (cods_local) locais, pck_enf_resumo_epr.get_locais_electro (cods_placa) placas, asaida.n_reg_oper
              FROM sd_enf_aval_saida_electro a, sd_enf_avaliacao_saida asaida
             WHERE asaida.id_aval = i_id AND asaida.t_doente = i_t_doente AND asaida.doente = i_doente AND a.id_aval = asaida.id_aval;

        CURSOR dm
        IS
            SELECT cd.valor descr,
                   asdm.valor,
                   asdm.observacao,
                   asaida.n_reg_oper
              FROM sd_enf_aval_saida_dm asdm, gr_enf_config_det cd, sd_enf_avaliacao_saida asaida
             WHERE     asaida.id_aval = i_id
                   AND asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asdm.cod_dm = cd.codigo
                   AND cd.cod_pai = 'BLOCO_DM'
                   AND asdm.id_aval = asaida.id_aval;

        CURSOR prep_oper
        IS
            SELECT gpo.descr_prep_oper, asaida.n_reg_oper
              FROM sd_enf_aval_saida_prep_oper aspo, gr_enf_prep_oper gpo, sd_enf_avaliacao_saida asaida
             WHERE asaida.id_aval = i_id AND asaida.t_doente = i_t_doente AND asaida.doente = i_doente AND aspo.cod_prep_oper = gpo.cod_prep_oper AND aspo.id_aval = asaida.id_aval;

        CURSOR puncao
        IS
            SELECT gbp.descricao, asp.obs_puncao, asaida.n_reg_oper
              FROM sd_enf_aval_saida_puncao asp, gr_enf_bloco_puncao gbp, sd_enf_avaliacao_saida asaida
             WHERE asaida.id_aval = i_id AND asaida.t_doente = i_t_doente AND asaida.doente = i_doente AND asp.cod_puncao = gbp.codigo AND asp.id_aval = asaida.id_aval;

        CURSOR anatomia
        IS
            SELECT gba.descr_ana,
                   asa.n_pecas,
                   asa.observacoes,
                   asaida.n_reg_oper
              FROM sd_enf_aval_saida_anatomia asa, gr_enf_bloco_anat gba, sd_enf_avaliacao_saida asaida
             WHERE asaida.id_aval = i_id AND asaida.t_doente = i_t_doente AND asaida.doente = i_doente AND asa.cod_ana = gba.cod_ana AND asa.id_aval = asaida.id_aval;

        CURSOR feridas
        IS
            SELECT gbf.descricao desc_local,
                   gbf2.descricao desc_prod,
                   asf.obs_local,
                   asf.obs_prod,
                   asaida.n_reg_oper
              FROM sd_enf_aval_saida_feridas asf,
                   gr_enf_bloco_ferida gbf,
                   gr_enf_bloco_ferida gbf2,
                   sd_enf_avaliacao_saida asaida
             WHERE     asaida.id_aval = i_id
                   AND asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asf.cod_local = gbf.codigo
                   AND asf.cod_prod = gbf2.codigo
                   AND asf.id_aval = asaida.id_aval;

        CURSOR saida_serv
        IS
            SELECT gbs.descricao, ass.obs_serv, asaida.n_reg_oper
              FROM sd_enf_aval_saida_serv ass, gr_enf_bloco_serv gbs, sd_enf_avaliacao_saida asaida
             WHERE asaida.id_aval = i_id AND asaida.t_doente = i_t_doente AND asaida.doente = i_doente AND ass.cod_serv = gbs.codigo AND ass.id_aval = asaida.id_aval;

        texto_inicial          VARCHAR (200);
        texto_drenagens        VARCHAR (200);       --:= traduz ('Drenagens');
        flg_drenagens          NUMBER := 0;
        texto_garrote          VARCHAR (200);-- := traduz ('Garrote');
        flg_garrote            NUMBER := 0;
        texto_compressas       VARCHAR (200) := traduz ('Compressas');
        texto_dadas            VARCHAR (200) := traduz ('Dadas');
        texto_recebidas        VARCHAR (200) := traduz ('Recebidas');
        texto_mesa             VARCHAR (200) := traduz ('Mesa');
        flg_compressas         NUMBER := 0;
        texto_posicionamento   VARCHAR (200);-- := traduz ('Posicionamento');
        flg_posicionamento     NUMBER := 0;
        texto_electro          VARCHAR (200);-- := traduz ('Placas');
        texto_placas           VARCHAR (200) := traduz ('Placas');
        texto_locais           VARCHAR (200) := traduz ('Locais');
        flg_electro            NUMBER := 0;
        texto_dm               VARCHAR (200);-- := traduz ('Contagem de DM');
        flg_dm                 NUMBER := 0;
        texto_prep_oper        VARCHAR (200);-- := traduz ('Prepara��o');
        flg_prep_oper          NUMBER := 0;
        texto_puncao           VARCHAR (200);-- := traduz ('Pun��o');
        flg_puncao             NUMBER := 0;
        texto_anatomia         VARCHAR (200);-- := traduz ('Anatomia');
        texto_pecas            VARCHAR (200) := traduz ('N� Pe�as');
        flg_anatomia           NUMBER := 0;
        texto_feridas          VARCHAR (200);-- := traduz ('Feridas');
        texto_local            VARCHAR (200) := traduz ('Local');
        texto_prod             VARCHAR (200) := traduz ('Produto');
        flg_feridas            NUMBER := 0;
        texto_saida_serv       VARCHAR (200) := traduz ('Servi�o');
        flg_saida_serv         NUMBER := 0;
        texto_total            VARCHAR2 (32000);

        var_doente             VARCHAR (100);
        var_t_doente           VARCHAR (100);
        var_episodio           VARCHAR (100);
        var_t_episodio         VARCHAR (100);
        var_descricao          VARCHAR (200);
        var_n_reg_oper         VARCHAR (100);
        var_data_hora          DATE;
        var_utilizador         VARCHAR (100);
        var_cod_serv           VARCHAR (100);
    BEGIN
        FOR aval IN aval_intra_oper
        LOOP
            texto_inicial := aval.descricao ;
            texto_total := texto_total || texto_inicial || CHR (10);

            var_t_doente := aval.t_doente;
            var_doente := aval.doente;
            var_t_episodio := aval.t_episodio;
            var_episodio := aval.episodio;
            var_n_reg_oper := aval.n_reg_oper;
            var_data_hora := aval.data_hora;
            var_utilizador := aval.utilizador;
            var_cod_serv := aval.cod_serv;
        END LOOP;

        IF texto_total IS NOT NULL
        THEN
            flg_drenagens := 0;
            
            SELECT descr
              INTO texto_drenagens
              FROM gr_enf_resumo_bloco gr
             WHERE tipo = 'ENCERRAMENTO_INT' AND grupo = 'INT';

            FOR dre IN drenagens
            LOOP
                IF flg_drenagens = 0
                THEN
                    texto_total := texto_total || CHR (10) || texto_drenagens || ':' || CHR (10);
                    flg_drenagens := 1;
                END IF;

                texto_total := texto_total || CHR (9) || dre.descricao || ' - ' || dre.observacao || ';' || CHR (10);
            END LOOP;

            flg_garrote := 0;
            
            SELECT descr
              INTO texto_garrote
              FROM gr_enf_resumo_bloco
             WHERE tipo = 'GARROTE' AND grupo = 'INT';

            FOR gar IN garrote
            LOOP
                IF flg_garrote = 0
                THEN
                    texto_total := texto_total || CHR (10) || texto_garrote || ':' || CHR (10);
                    flg_garrote := 1;
                END IF;

                texto_total :=
                       texto_total
                    || CHR (9)
                    || gar.descr_garr
                    || ' - '
                    || gar.pressao
                    || ' - '
                    || TO_CHAR (gar.dt_ini, 'dd-mm-yyyy hh24:mi')
                    || '/'
                    || TO_CHAR (gar.dt_fim, 'dd-mm-yyyy hh24:mi')
                    || ';'
                    || CHR (10);
            END LOOP;

            flg_compressas := 0;

            FOR com IN compressas
            LOOP
                IF flg_compressas = 0
                THEN
                    texto_total := texto_total || CHR (10) || texto_compressas || ':' || CHR (10);
                    flg_compressas := 1;
                END IF;

                texto_total :=
                       texto_total
                    || CHR (9)
                    || com.descricao
                    || ' - '
                    || texto_dadas
                    || ': '
                    || com.dadas
                    || ', '
                    || texto_recebidas
                    || ': '
                    || com.recebidas
                    || ', '
                    || texto_mesa
                    || ': '
                    || com.mesa
                    || ';'
                    || CHR (10);
            END LOOP;

            flg_posicionamento := 0;
            
            SELECT descr
              INTO texto_posicionamento
              FROM gr_enf_resumo_bloco
             WHERE tipo = 'POSICIONAMENTO' AND grupo = 'INT';

            FOR pos IN posicionamento
            LOOP
                IF flg_posicionamento = 0
                THEN
                    texto_total := texto_total || CHR (10) || texto_posicionamento || ':' || CHR (10);
                    flg_posicionamento := 1;
                END IF;

                texto_total := texto_total || CHR (9) || pos.descr_posi || ' - ' || pos.descr_aux || ';' || CHR (10);
            END LOOP;

            flg_electro := 0;
            
            SELECT descr
              INTO texto_electro
              FROM gr_enf_resumo_bloco
             WHERE tipo = 'ELECTRO' AND grupo = 'INT';

            FOR elec IN electro
            LOOP
                IF flg_electro = 0
                THEN
                    texto_total := texto_total || CHR (10) || texto_electro || ':' || CHR (10);
                    flg_electro := 1;
                END IF;

                texto_total := texto_total || CHR (9) || texto_placas || ': ' || elec.placas || ' ' || texto_locais || ': ' || elec.locais || ';' || CHR (10);
            END LOOP;

            flg_dm := 0;
            
            SELECT descr
              INTO texto_dm
              FROM gr_enf_resumo_bloco
             WHERE tipo = 'CONTAGEM_DM' AND grupo = 'INT';

            FOR d IN dm
            LOOP
                IF flg_dm = 0
                THEN
                    texto_total := texto_total || CHR (10) || texto_dm || ':' || CHR (10);
                    flg_dm := 1;
                END IF;

                texto_total := texto_total || CHR (9) || d.descr || ': ' || d.valor || ' - ' || d.observacao || ';' || CHR (10);
            END LOOP;

            flg_prep_oper := 0;
            
            SELECT descr
              INTO texto_prep_oper
              FROM gr_enf_resumo_bloco
             WHERE tipo = 'PREP_OPER' AND grupo = 'INT';

            FOR prep IN prep_oper
            LOOP
                IF flg_prep_oper = 0
                THEN
                    texto_total := texto_total || CHR (10) || texto_prep_oper || ':' || CHR (10);
                    flg_prep_oper := 1;
                END IF;

                texto_total := texto_total || CHR (9) || prep.descr_prep_oper || ';' || CHR (10);
            END LOOP;

            flg_puncao := 0;
            
            SELECT descr
              INTO texto_puncao
              FROM gr_enf_resumo_bloco
             WHERE tipo = 'PUNCAO' AND grupo = 'INT';

            FOR pun IN puncao
            LOOP
                IF flg_puncao = 0
                THEN
                    texto_total := texto_total || CHR (10) || texto_puncao || ':' || CHR (10);
                    flg_puncao := 1;
                END IF;

                texto_total := texto_total || CHR (9) || pun.descricao || ' - ' || pun.obs_puncao || ';' || CHR (10);
            END LOOP;

            flg_anatomia := 0;
            
            SELECT descr
              INTO texto_anatomia
              FROM gr_enf_resumo_bloco
             WHERE tipo = 'ANATOMIA' AND grupo = 'INT';

            FOR ana IN anatomia
            LOOP
                IF flg_anatomia = 0
                THEN
                    texto_total := texto_total || CHR (10) || texto_anatomia || ':' || CHR (10);
                    flg_anatomia := 1;
                END IF;

                texto_total := texto_total || CHR (9) || ana.descr_ana || ' ' || texto_pecas || ': ' || ana.n_pecas || ' - ' || ana.observacoes || ';' || CHR (10);
            END LOOP;

            flg_feridas := 0;
            
            SELECT descr
              INTO texto_feridas
              FROM gr_enf_resumo_bloco
             WHERE tipo = 'FERIDA'  AND grupo = 'INT';

            FOR fer IN feridas
            LOOP
                IF flg_feridas = 0
                THEN
                    texto_total := texto_total || CHR (10) || texto_feridas || ':' || CHR (10);
                    flg_feridas := 1;
                END IF;

                texto_total :=
                       texto_total
                    || CHR (9)
                    || texto_local
                    || ': '
                    || fer.desc_local
                    || ' ('
                    || fer.obs_local
                    || '), '
                    || texto_prod
                    || ': '
                    || fer.desc_prod
                    || ' ('
                    || fer.obs_prod
                    || ');'
                    || CHR (10);
            END LOOP;

            flg_saida_serv := 0;

            FOR sai IN saida_serv
            LOOP
                IF flg_saida_serv = 0
                THEN
                    texto_total := texto_total || CHR (10) || texto_saida_serv || ':' || CHR (10);
                    flg_saida_serv := 1;
                END IF;

                texto_total := texto_total || CHR (9) || sai.descricao || ' (' || sai.obs_serv || ');' || CHR (10);
            END LOOP;


            v_rec.t_doente := var_t_doente;
            v_rec.doente := var_doente;
            v_rec.t_episodio := var_t_episodio;
            v_rec.episodio := var_episodio;
            v_rec.cod_serv := var_cod_serv;
            v_rec.n_mecan := get_n_mecan (var_utilizador);
            v_rec.dt_registo := var_data_hora;
            v_rec.id_bd := var_n_reg_oper;
            v_rec.id_resumo := 'AVAL_IO#' || var_n_reg_oper;
            v_rec.digest := NULL;
            v_rec.texto_completo := texto_total;
            v_rec.has_more_than_digest := 'S';
        END IF;

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_AVAL_INTRA_OPER_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_aval_intra_oper (i_t_doente      VARCHAR2,
                                  i_doente        VARCHAR2,
                                  i_t_episodio    VARCHAR2,
                                  i_episodio      VARCHAR2,
                                  i_cod_serv      VARCHAR2,
                                  i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec                  pck_types_nv.r_resumo;
        v_aval_intra_oper      pck_types_nv.t_resumo;

        /*DADOS DA AVALIA��O E CIRURGIA*/
        CURSOR aval_intra_oper
        IS
            SELECT aci.descricao,
                   asaida.t_doente,
                   asaida.doente,
                   asaida.t_episodio,
                   asaida.episodio,
                   asaida.n_reg_oper,
                   asaida.data_hora,
                   NVL (asaida.user_act, asaida.user_cri) utilizador,
                   sro.cod_serv
              FROM sd_enf_avaliacao_saida asaida,
                   sd_reg_oper_det rod,
                   ana_cid_interv aci,
                   sd_reg_oper sro
             WHERE     asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asaida.t_episodio = i_t_episodio
                   AND asaida.episodio = i_episodio
                   AND asaida.n_reg_oper = rod.n_reg_oper(+)
                   AND rod.n_reg_oper = sro.n_reg_oper(+)
                   AND rod.cod_oper = aci.codigo(+)
                   AND rod.codificacao = aci.codificacao(+);

        /*DADOS DAS DRENAGENS*/
        CURSOR drenagens
        IS
            SELECT gd.descricao, asd.observacao, asaida.n_reg_oper
              FROM sd_enf_aval_saida_drenagens asd, gr_enf_drenos gd, sd_enf_avaliacao_saida asaida
             WHERE     asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asaida.t_episodio = i_t_episodio
                   AND asaida.episodio = i_episodio
                   AND asd.cod_dreno = gd.codigo
                   AND asd.id_aval = asaida.id_aval;

        /*DADOS GARROTE*/
        CURSOR garrote
        IS
            SELECT ge.descr_garr,
                   asg.pressao,
                   asg.dt_ini,
                   asg.dt_fim,
                   asaida.n_reg_oper
              FROM sd_enf_aval_saida_garrote asg, gr_enf_garrote ge, sd_enf_avaliacao_saida asaida
             WHERE     asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asaida.t_episodio = i_t_episodio
                   AND asaida.episodio = i_episodio
                   AND asg.cod_garr = ge.cod_garr
                   AND asg.id_aval = asaida.id_aval;

        /*DADOS COMPRESSAS*/
        CURSOR compressas
        IS
            SELECT ec.descricao,
                   com.dadas,
                   com.recebidas,
                   com.mesa,
                   asaida.n_reg_oper
              FROM sd_enf_aval_saida_compressas com, gr_enf_compressas ec, sd_enf_avaliacao_saida asaida
             WHERE     asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asaida.t_episodio = i_t_episodio
                   AND asaida.episodio = i_episodio
                   AND com.tipo_compressa = ec.tipo_compressa
                   AND com.id_aval = asaida.id_aval;

        CURSOR posicionamento
        IS
            SELECT gp.descr_posi, asp.descr_aux, asaida.n_reg_oper
              FROM sd_enf_aval_saida_posi asp, gr_enf_posic gp, sd_enf_avaliacao_saida asaida
             WHERE     asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asaida.t_episodio = i_t_episodio
                   AND asaida.episodio = i_episodio
                   AND asp.cod_posi = gp.cod_posi
                   AND asp.id_aval = asaida.id_aval;

        CURSOR electro
        IS
            SELECT pck_enf_resumo_epr.get_locais_electro (cods_local) locais, pck_enf_resumo_epr.get_locais_electro (cods_placa) placas, asaida.n_reg_oper
              FROM sd_enf_aval_saida_electro a, sd_enf_avaliacao_saida asaida
             WHERE asaida.t_doente = i_t_doente AND asaida.doente = i_doente AND asaida.t_episodio = i_t_episodio AND asaida.episodio = i_episodio AND a.id_aval = asaida.id_aval;

        CURSOR dm
        IS
            SELECT cd.valor descr,
                   asdm.valor,
                   asdm.observacao,
                   asaida.n_reg_oper
              FROM sd_enf_aval_saida_dm asdm, gr_enf_config_det cd, sd_enf_avaliacao_saida asaida
             WHERE     asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asaida.t_episodio = i_t_episodio
                   AND asaida.episodio = i_episodio
                   AND asdm.cod_dm = cd.codigo
                   AND cd.cod_pai = 'BLOCO_DM'
                   AND asdm.id_aval = asaida.id_aval;

        CURSOR prep_oper
        IS
            SELECT gpo.descr_prep_oper, asaida.n_reg_oper
              FROM sd_enf_aval_saida_prep_oper aspo, gr_enf_prep_oper gpo, sd_enf_avaliacao_saida asaida
             WHERE     asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asaida.t_episodio = i_t_episodio
                   AND asaida.episodio = i_episodio
                   AND aspo.cod_prep_oper = gpo.cod_prep_oper
                   AND aspo.id_aval = asaida.id_aval;

        CURSOR puncao
        IS
            SELECT gbp.descricao, asp.obs_puncao, asaida.n_reg_oper
              FROM sd_enf_aval_saida_puncao asp, gr_enf_bloco_puncao gbp, sd_enf_avaliacao_saida asaida
             WHERE     asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asaida.t_episodio = i_t_episodio
                   AND asaida.episodio = i_episodio
                   AND asp.cod_puncao = gbp.codigo
                   AND asp.id_aval = asaida.id_aval;

        CURSOR anatomia
        IS
            SELECT gba.descr_ana,
                   asa.n_pecas,
                   asa.observacoes,
                   asaida.n_reg_oper
              FROM sd_enf_aval_saida_anatomia asa, gr_enf_bloco_anat gba, sd_enf_avaliacao_saida asaida
             WHERE     asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asaida.t_episodio = i_t_episodio
                   AND asaida.episodio = i_episodio
                   AND asa.cod_ana = gba.cod_ana
                   AND asa.id_aval = asaida.id_aval;

        CURSOR feridas
        IS
            SELECT gbf.descricao desc_local,
                   gbf2.descricao desc_prod,
                   asf.obs_local,
                   asf.obs_prod,
                   asaida.n_reg_oper
              FROM sd_enf_aval_saida_feridas asf,
                   gr_enf_bloco_ferida gbf,
                   gr_enf_bloco_ferida gbf2,
                   sd_enf_avaliacao_saida asaida
             WHERE     asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asaida.t_episodio = i_t_episodio
                   AND asaida.episodio = i_episodio
                   AND asf.cod_local = gbf.codigo
                   AND asf.cod_prod = gbf2.codigo
                   AND asf.id_aval = asaida.id_aval;

        CURSOR saida_serv
        IS
            SELECT gbs.descricao, ass.obs_serv, asaida.n_reg_oper
              FROM sd_enf_aval_saida_serv ass, gr_enf_bloco_serv gbs, sd_enf_avaliacao_saida asaida
             WHERE     asaida.t_doente = i_t_doente
                   AND asaida.doente = i_doente
                   AND asaida.t_episodio = i_t_episodio
                   AND asaida.episodio = i_episodio
                   AND ass.cod_serv = gbs.codigo
                   AND ass.id_aval = asaida.id_aval;

        texto_inicial          VARCHAR (200);
        texto_drenagens        VARCHAR (200) /*:= traduz ('Drenagens')*/;
        flg_drenagens          NUMBER := 0;
        texto_garrote          VARCHAR (200);-- := traduz ('Garrote');
        flg_garrote            NUMBER := 0;
        texto_compressas       VARCHAR (200) := traduz ('Compressas');
        texto_dadas            VARCHAR (200) := traduz ('Dadas');
        texto_recebidas        VARCHAR (200) := traduz ('Recebidas');
        texto_mesa             VARCHAR (200) := traduz ('Mesa');
        flg_compressas         NUMBER := 0;
        texto_posicionamento   VARCHAR (200);-- := traduz ('Posicionamento');
        flg_posicionamento     NUMBER := 0;
        texto_electro          VARCHAR (200);-- := traduz ('Placas');
        texto_placas           VARCHAR (200) := traduz ('Placas');
        texto_locais           VARCHAR (200) := traduz ('Locais');
        flg_electro            NUMBER := 0;
        texto_dm               VARCHAR (200);-- := traduz ('Contagem de DM');
        flg_dm                 NUMBER := 0;
        texto_prep_oper        VARCHAR (200);-- := traduz ('Prepara��o');
        flg_prep_oper          NUMBER := 0;
        texto_puncao           VARCHAR (200);-- := traduz ('Pun��o');
        flg_puncao             NUMBER := 0;
        texto_anatomia         VARCHAR (200);-- := traduz ('Anatomia');
        texto_pecas            VARCHAR (200) := traduz ('N� Pe�as');
        flg_anatomia           NUMBER := 0;
        texto_feridas          VARCHAR (200);-- := traduz ('Feridas');
        texto_local            VARCHAR (200) := traduz ('Local');
        texto_prod             VARCHAR (200) := traduz ('Produto');
        flg_feridas            NUMBER := 0;
        texto_saida_serv       VARCHAR (200) := traduz ('Servi�o');
        flg_saida_serv         NUMBER := 0;
        texto_total            VARCHAR2 (32000);
    BEGIN
        FOR aval IN aval_intra_oper
        LOOP
            texto_inicial := aval.descricao ;
            texto_total := texto_inicial || CHR (10);
            flg_drenagens := 0;

            FOR dre IN drenagens
            LOOP
                IF dre.n_reg_oper = aval.n_reg_oper
                THEN
                
                    SELECT descr
                      INTO texto_drenagens
                      FROM gr_enf_resumo_bloco
                     WHERE tipo = 'ENCERRAMENTO_INT';
                     
                    IF flg_drenagens = 0
                    THEN
                        texto_total := texto_total || CHR (10) || texto_drenagens || ':' || CHR (10);
                        flg_drenagens := 1;
                    END IF;

                    texto_total := texto_total || CHR (9) || dre.descricao || ' - ' || dre.observacao || ';' || CHR (10);
                END IF;
            END LOOP;

            flg_garrote := 0;
            
            SELECT descr
            INTO texto_garrote
            FROM gr_enf_resumo_bloco
            WHERE tipo = 'GARROTE' AND grupo = 'INT';

            FOR gar IN garrote
            LOOP
                IF gar.n_reg_oper = aval.n_reg_oper
                THEN
                    IF flg_garrote = 0
                    THEN
                        texto_total := texto_total || CHR (10) || texto_garrote || ':' || CHR (10);
                        flg_garrote := 1;
                    END IF;

                    texto_total :=
                           texto_total
                        || CHR (9)
                        || gar.descr_garr
                        || ' - '
                        || gar.pressao
                        || ' - '
                        || TO_CHAR (gar.dt_ini, 'dd-mm-yyyy hh24:mi')
                        || '/'
                        || TO_CHAR (gar.dt_fim, 'dd-mm-yyyy hh24:mi')
                        || ';'
                        || CHR (10);
                END IF;
            END LOOP;

            flg_compressas := 0;

            FOR com IN compressas
            LOOP
                IF com.n_reg_oper = aval.n_reg_oper
                THEN
                    IF flg_compressas = 0
                    THEN
                        texto_total := texto_total || CHR (10) || texto_compressas || ':' || CHR (10);
                        flg_compressas := 1;
                    END IF;

                    texto_total :=
                           texto_total
                        || CHR (9)
                        || com.descricao
                        || ' - '
                        || texto_dadas
                        || ': '
                        || com.dadas
                        || ', '
                        || texto_recebidas
                        || ': '
                        || com.recebidas
                        || ', '
                        || texto_mesa
                        || ': '
                        || com.mesa
                        || ';'
                        || CHR (10);
                END IF;
            END LOOP;

            flg_posicionamento := 0;
            
            SELECT descr
            INTO texto_posicionamento
            FROM gr_enf_resumo_bloco
            WHERE tipo = 'POSICIONAMENTO' AND grupo = 'INT';

            FOR pos IN posicionamento
            LOOP
                IF pos.n_reg_oper = aval.n_reg_oper
                THEN
                    IF flg_posicionamento = 0
                    THEN
                        texto_total := texto_total || CHR (10) || texto_posicionamento || ':' || CHR (10);
                        flg_posicionamento := 1;
                    END IF;

                    texto_total := texto_total || CHR (9) || pos.descr_posi || ' - ' || pos.descr_aux || ';' || CHR (10);
                END IF;
            END LOOP;

            flg_electro := 0;
            
            SELECT descr
            INTO texto_electro
            FROM gr_enf_resumo_bloco
            WHERE tipo = 'ELECTRO' AND grupo = 'INT';

            FOR elec IN electro
            LOOP
                IF elec.n_reg_oper = aval.n_reg_oper
                THEN
                    IF flg_electro = 0
                    THEN
                        texto_total := texto_total || CHR (10) || texto_electro || ':' || CHR (10);
                        flg_electro := 1;
                    END IF;

                    texto_total := texto_total || CHR (9) || texto_placas || ': ' || elec.placas || ' ' || texto_locais || ': ' || elec.locais || ';' || CHR (10);
                END IF;
            END LOOP;

            flg_dm := 0;
            
            SELECT descr
            INTO texto_dm
            FROM gr_enf_resumo_bloco
            WHERE tipo = 'CONTAGEM_DM' AND grupo = 'INT';

            FOR d IN dm
            LOOP
                IF d.n_reg_oper = aval.n_reg_oper
                THEN
                    IF flg_dm = 0
                    THEN
                        texto_total := texto_total || CHR (10) || texto_dm || ':' || CHR (10);
                        flg_dm := 1;
                    END IF;

                    texto_total := texto_total || CHR (9) || d.descr || ': ' || d.valor || ' - ' || d.observacao || ';' || CHR (10);
                END IF;
            END LOOP;

            flg_prep_oper := 0;
            
            SELECT descr
            INTO texto_prep_oper
            FROM gr_enf_resumo_bloco
            WHERE tipo = 'PREP_OPER' AND grupo = 'INT';

            FOR prep IN prep_oper
            LOOP
                IF prep.n_reg_oper = aval.n_reg_oper
                THEN
                    IF flg_prep_oper = 0
                    THEN
                        texto_total := texto_total || CHR (10) || texto_prep_oper || ':' || CHR (10);
                        flg_prep_oper := 1;
                    END IF;

                    texto_total := texto_total || CHR (9) || prep.descr_prep_oper || ';' || CHR (10);
                END IF;
            END LOOP;

            flg_puncao := 0;
            
            SELECT descr
            INTO texto_puncao
            FROM gr_enf_resumo_bloco
            WHERE tipo = 'PUNCAO' AND grupo = 'INT';

            FOR pun IN puncao
            LOOP
                IF pun.n_reg_oper = aval.n_reg_oper
                THEN
                    IF flg_puncao = 0
                    THEN
                        texto_total := texto_total || CHR (10) || texto_puncao || ':' || CHR (10);
                        flg_puncao := 1;
                    END IF;

                    texto_total := texto_total || CHR (9) || pun.descricao || ' - ' || pun.obs_puncao || ';' || CHR (10);
                END IF;
            END LOOP;

            flg_anatomia := 0;
            
            SELECT descr
            INTO texto_anatomia
            FROM gr_enf_resumo_bloco
            WHERE tipo = 'ANATOMIA' AND grupo = 'INT';

            FOR ana IN anatomia
            LOOP
                IF ana.n_reg_oper = aval.n_reg_oper
                THEN
                    IF flg_anatomia = 0
                    THEN
                        texto_total := texto_total || CHR (10) || texto_anatomia || ':' || CHR (10);
                        flg_anatomia := 1;
                    END IF;

                    texto_total := texto_total || CHR (9) || ana.descr_ana || ' ' || texto_pecas || ': ' || ana.n_pecas || ' - ' || ana.observacoes || ';' || CHR (10);
                END IF;
            END LOOP;

            flg_feridas := 0;
            
            SELECT descr
            INTO texto_feridas
            FROM gr_enf_resumo_bloco
            WHERE tipo = 'FERIDA'  AND grupo = 'INT';

            FOR fer IN feridas
            LOOP
                IF fer.n_reg_oper = aval.n_reg_oper
                THEN
                    IF flg_feridas = 0
                    THEN
                        texto_total := texto_total || CHR (10) || texto_feridas || ':' || CHR (10);
                        flg_feridas := 1;
                    END IF;

                    texto_total :=
                           texto_total
                        || CHR (9)
                        || texto_local
                        || ': '
                        || fer.desc_local
                        || ' ('
                        || fer.obs_local
                        || '), '
                        || texto_prod
                        || ': '
                        || fer.desc_prod
                        || ' ('
                        || fer.obs_prod
                        || ');'
                        || CHR (10);
                END IF;
            END LOOP;

            flg_saida_serv := 0;

            FOR sai IN saida_serv
            LOOP
                IF sai.n_reg_oper = aval.n_reg_oper
                THEN
                    IF flg_saida_serv = 0
                    THEN
                        texto_total := texto_total || CHR (10) || texto_saida_serv || ':' || CHR (10);
                        flg_saida_serv := 1;
                    END IF;

                    texto_total := texto_total || CHR (9) || sai.descricao || ' (' || sai.obs_serv || ');' || CHR (10);
                END IF;
            END LOOP;

            IF texto_total IS NOT NULL
            THEN
                v_rec :=
                    create_base_rec (aval.t_doente,
                                     aval.doente,
                                     aval.t_episodio,
                                     aval.episodio,
                                     aval.cod_serv,
                                     aval.utilizador,
                                     get_n_mecan (aval.utilizador),
                                     '',
                                     aval.data_hora);
                v_rec.id_bd := aval.n_reg_oper;
                v_rec.id_resumo := 'AVAL_IO#' || aval.n_reg_oper;
                v_rec.digest := NULL;
                v_rec.texto_completo := texto_total;
                v_rec.has_more_than_digest := 'S';
                v_aval_intra_oper (v_aval_intra_oper.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_aval_intra_oper;
    END;

    --******************************************************************************
    --******************************************************************************
    FUNCTION get_terapeutica_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec              pck_types_nv.r_resumo;
        texto_init_terap   VARCHAR (200);
        texto_completo     VARCHAR (10000);
        var_utilizador     VARCHAR (100);
    BEGIN
        SELECT t_doente,
               doente,
               t_episodio,
               episodio,
               cod_serv,
               get_n_mecan (utilizador),
               utilizador,
               data,
               adm,
               NULL,
               pck_enf_resumo_epr.concatenate_terap (data,
                                                     doente,
                                                     t_doente,
                                                     episodio,
                                                     episodio),
               'S'
          INTO v_rec.t_doente,
               v_rec.doente,
               v_rec.t_episodio,
               v_rec.episodio,
               v_rec.cod_serv,
               v_rec.n_mecan,
               v_rec.user_sys,
               v_rec.dt_registo,
               v_rec.id_bd,
               v_rec.digest,
               v_rec.texto_completo,
               v_rec.has_more_than_digest
          FROM (  SELECT DISTINCT TRUNC (hora, 'hh') data,
                                  t_doente,
                                  doente,
                                  t_episodio,
                                  episodio,
                                  cod_serv,
                                  utilizador,
                                  MAX (administracao) adm
                    FROM (SELECT t1.hora_adm hora,
                                 t1.t_doente,
                                 t1.doente,
                                 t1.t_episodio,
                                 t1.episodio,
                                 t1.cod_serv,
                                 t1.administracao,
                                 t1.enf utilizador
                            FROM ph_med_adm t1, ph_medicamentos t2
                           WHERE t1.medicamento = t2.medicamento(+) AND t1.administracao = i_id
                          UNION ALL
                          SELECT t1.hora hora,
                                 t1.t_doente,
                                 t1.doente,
                                 ' ',
                                 ' ',
                                 t1.cod_serv,
                                 t1.administracao,
                                 t1.resp utilizador
                            FROM ph_med_n_adm t1, ph_medicamentos t2
                           WHERE t1.medicamento = t2.medicamento(+) AND t1.t_doente = i_t_doente AND t1.doente = i_doente AND t1.administracao = i_id)
                GROUP BY TRUNC (hora, 'hh'),
                         t_doente,
                         doente,
                         cod_serv,
                         utilizador
                ORDER BY data);

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_TERAPEUTICA_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_terapeutica (i_t_doente      VARCHAR2,
                              i_doente        VARCHAR2,
                              i_t_episodio    VARCHAR2,
                              i_episodio      VARCHAR2,
                              i_cod_serv      VARCHAR2,
                              i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec              pck_types_nv.r_resumo;
        v_terap            pck_types_nv.t_resumo;

        CURSOR hora_terapeutica
        IS
              SELECT DISTINCT TRUNC (hora, 'hh') data,
                              t_doente,
                              doente,
                              cod_serv,
                              utilizador,
                              MAX (administracao) adm
                FROM (SELECT t1.hora_adm hora,
                             t1.t_doente,
                             t1.doente,
                             t1.cod_serv,
                             t1.administracao,
                             t1.enf utilizador
                        FROM ph_med_adm t1, ph_medicamentos t2
                       WHERE t1.medicamento = t2.medicamento(+) AND t1.t_doente = i_t_doente AND t1.doente = i_doente AND t1.t_episodio = i_t_episodio AND t1.episodio = i_episodio
                      UNION ALL
                      SELECT t1.hora hora,
                             t1.t_doente,
                             t1.doente,
                             t1.cod_serv,
                             t1.administracao,
                             t1.resp utilizador
                        FROM ph_med_n_adm t1, ph_medicamentos t2
                       WHERE t1.medicamento = t2.medicamento(+) AND t1.t_doente = i_t_doente AND t1.doente = i_doente)
            GROUP BY TRUNC (hora, 'hh'),
                     t_doente,
                     doente,
                     cod_serv,
                     utilizador
            ORDER BY data;

        texto_init_terap   VARCHAR (200);
        texto_completo     VARCHAR (10000);
        var_utilizador     VARCHAR (100);
    BEGIN
        FOR ter IN hora_terapeutica
        LOOP
            -- texto_init_terap := pck_enf_resumo_epr.get_ultima_terap(ter.data, ter.doente, ter.t_doente, i_episodio, i_t_episodio);
            texto_completo :=
                pck_enf_resumo_epr.concatenate_terap (ter.data,
                                                      ter.doente,
                                                      ter.t_doente,
                                                      i_episodio,
                                                      i_t_episodio);

            IF texto_completo IS NOT NULL
            THEN
                var_utilizador := ter.utilizador;                                                                        --substr(texto_init_terap,1,instr(texto_init_terap,'#')-1);
                v_rec :=
                    create_base_rec (ter.t_doente,
                                     ter.doente,
                                     i_t_episodio,
                                     i_episodio,
                                     ter.cod_serv,
                                     var_utilizador,
                                     get_n_mecan (var_utilizador),
                                     '',
                                     ter.data);
                v_rec.id_bd := ter.adm;
                --v_rec.dt_registo_display := to_char(ter.data, 'dd-mm-yyyy - hh24:mm')||'h';
                v_rec.id_resumo := 'TERAPEUTICA#' || ter.adm;
                v_rec.digest := NULL;                                                                                      --substr(texto_init_terap,instr(texto_init_terap,'#')+1);
                v_rec.texto_completo := texto_completo;
                v_rec.has_more_than_digest := 'S';
                v_terap (v_terap.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_terap;
    END;

    --******************************************************************************
    --******************************************************************************

    FUNCTION get_ultima_terap_by_id (i_data        DATE,
                                     i_doente      VARCHAR,
                                     i_t_doente    VARCHAR,
                                     i_id          NUMBER)
        RETURN VARCHAR
    IS
        ult_terap        VARCHAR (200);
        var_hora         VARCHAR (10);
        var_nome         VARCHAR (100);
        var_dose         VARCHAR (50);
        var_utilizador   VARCHAR (100);
    BEGIN
        SELECT hora,
               nome,
               dose,
               utilizador
          INTO var_hora,
               var_nome,
               var_dose,
               var_utilizador
          FROM (SELECT TO_CHAR (t1.hora_adm, 'hh24:mi') hora,
                       NVL (t2.nome_cient, t1.nome_med_adm) nome,
                       TO_CHAR (t1.dose, 'FM999990.00') || ' ' || t1.unid_med dose,
                       obs_adm obs,
                       'ADM' flag,
                       t1.t_doente,
                       t1.doente,
                       t1.t_episodio,
                       t1.episodio,
                       NVL (t1.user_act, t1.user_cri) utilizador
                  FROM ph_med_adm t1, ph_medicamentos t2
                 WHERE t1.medicamento = t2.medicamento(+) AND t1.administracao = i_id AND TRUNC (t1.hora_adm, 'hh') = i_data
                UNION
                SELECT TO_CHAR (t1.hora, 'hh24:mi') hora,
                       NVL (t2.nome_cient, t1.nome_med_adm) nome,
                       TO_CHAR (t1.dose, 'FM999990.00') || ' ' || t1.unid_med dose,
                       t1.desc_justif obs,
                       'N_ADM' flag,
                       t1.t_doente,
                       t1.doente,
                       '',
                       '',
                       NVL (t1.user_act, t1.user_cri) utilizador
                  FROM ph_med_n_adm t1, ph_medicamentos t2
                 WHERE t1.medicamento = t2.medicamento(+) AND t1.t_doente = i_t_doente AND t1.doente = i_doente AND t1.administracao = i_id AND TRUNC (t1.hora, 'hh') = i_data
                ORDER BY hora)
         WHERE ROWNUM = 1;

        ult_terap := var_utilizador || '#' || var_hora || ' ' || var_nome || ' ' || var_dose;
        RETURN ult_terap;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_ULTIMA_TERAP_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN ult_terap;
    END;

    --******************************************************************************

    FUNCTION get_ultima_terap (i_data          DATE,
                               i_doente        VARCHAR,
                               i_t_doente      VARCHAR,
                               i_episodio      VARCHAR,
                               i_t_episodio    VARCHAR)
        RETURN VARCHAR
    IS
        ult_terap        VARCHAR (200);
        var_hora         VARCHAR (10);
        var_nome         VARCHAR (100);
        var_dose         VARCHAR (50);
        var_utilizador   VARCHAR (100);
    BEGIN
        SELECT hora,
               nome,
               dose,
               utilizador
          INTO var_hora,
               var_nome,
               var_dose,
               var_utilizador
          FROM (SELECT TO_CHAR (t1.hora_adm, 'hh24:mi') hora,
                       NVL (t2.nome_cient, t1.nome_med_adm) nome,
                       TO_CHAR (t1.dose, 'FM999990.00') || ' ' || t1.unid_med dose,
                       obs_adm obs,
                       'ADM' flag,
                       t1.t_doente,
                       t1.doente,
                       t1.t_episodio,
                       t1.episodio,
                       NVL (t1.user_act, t1.user_cri) utilizador
                  FROM ph_med_adm t1, ph_medicamentos t2
                 WHERE     t1.medicamento = t2.medicamento(+)
                       AND t1.t_doente = i_t_doente
                       AND t1.doente = i_doente
                       AND t1.t_episodio = i_t_episodio
                       AND t1.episodio = i_episodio
                       AND TRUNC (t1.hora_adm, 'hh') = i_data
                UNION
                SELECT TO_CHAR (t1.hora, 'hh24:mi') hora,
                       NVL (t2.nome_cient, t1.nome_med_adm) nome,
                       TO_CHAR (t1.dose, 'FM999990.00') || ' ' || t1.unid_med dose,
                       t1.desc_justif obs,
                       'N_ADM' flag,
                       t1.t_doente,
                       t1.doente,
                       '',
                       '',
                       NVL (t1.user_act, t1.user_cri) utilizador
                  FROM ph_med_n_adm t1, ph_medicamentos t2
                 WHERE t1.medicamento = t2.medicamento(+) AND t1.t_doente = i_t_doente AND t1.doente = i_doente AND TRUNC (t1.hora, 'hh') = i_data
                ORDER BY hora)
         WHERE ROWNUM = 1;

        ult_terap := var_utilizador || '#' || var_hora || ' ' || var_nome || ' ' || var_dose;
        RETURN ult_terap;
    END;

    --******************************************************************************
    --******************************************************************************

    FUNCTION concatenate_terap (i_data          DATE,
                                i_doente        VARCHAR,
                                i_t_doente      VARCHAR,
                                i_episodio      VARCHAR,
                                i_t_episodio    VARCHAR)
        RETURN VARCHAR
    IS
        texto                VARCHAR (10000);
        texto_terap_adm      VARCHAR (300) := traduz ('Terap�utica Administrada');
        texto_obs            VARCHAR (200) := traduz ('Observa��o');
        flg_terap_adm        NUMBER := 0;
        texto_terap_n_adm    VARCHAR (300) := traduz ('Terap�utica N�o Administrada');
        texto_justificacao   VARCHAR (300) := traduz ('Justifica��o');
        flg_terap_n_adm      NUMBER := 0;

        CURSOR terap_adm
        IS
              SELECT t1.hora_adm hora,
                     NVL (t2.nome_cient, t1.nome_med_adm) nome,
                     TO_CHAR (t1.dose, 'FM999990.00') || ' ' || t1.unid_med dose,
                     obs_adm obs,
                     'ADM' flag,
                     t1.t_doente,
                     t1.doente,
                     t1.t_episodio,
                     t1.episodio,
                     NVL (t1.user_act, t1.user_cri) utilizador
                FROM ph_med_adm t1, ph_medicamentos t2
               WHERE     t1.medicamento = t2.medicamento(+)
                     AND t1.t_doente = i_t_doente
                     AND t1.doente = i_doente
                     AND t1.t_episodio = i_t_episodio
                     AND t1.episodio = i_episodio
                     AND TRUNC (t1.hora_adm, 'hh') = i_data
            ORDER BY hora;

        CURSOR terap_n_adm
        IS
              SELECT t1.hora hora,
                     NVL (t2.nome_cient, t1.nome_med_adm) nome,
                     TO_CHAR (t1.dose, 'FM999990.00') || ' ' || t1.unid_med dose,
                     t1.desc_justif obs,
                     'N_ADM' flag,
                     t1.t_doente,
                     t1.doente,
                     NVL (t1.user_act, t1.user_cri) utilizador
                FROM ph_med_n_adm t1, ph_medicamentos t2
               WHERE t1.medicamento = t2.medicamento(+) AND t1.t_doente = i_t_doente AND t1.doente = i_doente AND TRUNC (t1.hora, 'hh') = i_data
            ORDER BY hora;
    BEGIN
        FOR t IN terap_adm
        LOOP
            IF flg_terap_adm = 0
            THEN
                texto := texto_terap_adm || CHR (10);
                flg_terap_adm := 1;
            END IF;

            texto := texto || TO_CHAR (t.hora, 'hh24:mi') || ': ' || t.nome || ' ' || t.dose || ' ' || texto_obs || ': ' || t.obs || ' - ' || t.utilizador || '; ' || CHR (10);
        END LOOP;

        FOR tn IN terap_n_adm
        LOOP
            IF flg_terap_n_adm = 0
            THEN
                texto := texto || texto_terap_n_adm || CHR (10);
                flg_terap_n_adm := 1;
            END IF;

            texto :=
                   texto
                || TO_CHAR (tn.hora, 'hh24:mi')
                || ': '
                || tn.nome
                || ' '
                || tn.dose
                || ' '
                || texto_justificacao
                || ': '
                || tn.obs
                || ' - '
                || tn.utilizador
                || '; '
                || CHR (10);
        END LOOP;

        RETURN texto;
    END;

    --******************************************************************************
    --******************************************************************************
    FUNCTION get_vigilancias_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec              pck_types_nv.r_resumo;
        texto_vigil        VARCHAR (200) := traduz ('Vigil�ncia');
        texto_inicial      VARCHAR (500);
        v_t_doente         VARCHAR (20);
        v_doente           VARCHAR (20);
        v_t_episodio       VARCHAR (20);
        v_episodio         VARCHAR (20);
        v_dt_registo       DATE;
        v_utilizador       VARCHAR (20);
        v_descricao        VARCHAR (1000);
        texto_dt_in�cio   VARCHAR (30) := traduz ('Data de In�cio');

        var_obs            VARCHAR2 (4000);
    BEGIN
        SELECT t_doente,
               doente,
               t_episodio,
               episodio,
               get_n_mecan (NVL (user_act, user_cri)) n_mecan,
               NVL (user_act, user_cri),
               sdf_enf_add_hour2date (dt_registo, TO_CHAR (hr_registo, 'HH24:MI')) data_registo,
               cod_check,
               'VIGILANCIA#' || cod_check,
               '',
               'S',
               pckt_enf_plano_cuidados.get_descr_interv (id_agrupador) descricao_vigilancia,
               obs
          INTO v_rec.t_doente,
               v_rec.doente,
               v_rec.t_episodio,
               v_rec.episodio,
               v_rec.n_mecan,
               v_rec.user_sys,
               v_rec.dt_registo,
               v_rec.id_bd,
               v_rec.id_resumo,
               v_rec.digest,
               v_rec.has_more_than_digest,
               v_rec.texto_completo,
               var_obs
          FROM sd_enf_vigil
         WHERE t_doente = i_t_doente AND doente = i_doente AND cod_check = i_id;

        v_rec.texto_completo := v_rec.texto_completo || CHR (10) || CHR (10);

        FOR details
            IN (  SELECT DISTINCT q2.descr_item, pck_enf_resumo_epr.concatenate_resp_vigil (q0.num_vigil, q1.cod_item) respostas, q2.ordem
                    FROM sd_enf_vigil q0, sd_enf_vigil_det q1, gr_enf_qdr_espec_item q2
                   WHERE t_doente = i_t_doente AND doente = i_doente AND cod_check = i_id AND q0.num_vigil = q1.num_vigil AND q2.cod_qdr = q0.cod_vigil AND q2.cod_item = q1.cod_item
                ORDER BY q2.ordem)
        LOOP
            v_rec.texto_completo := v_rec.texto_completo || '> ' || details.descr_item || CHR (10);
            v_rec.texto_completo := v_rec.texto_completo || details.respostas || CHR (10);
        END LOOP;

        IF var_obs IS NOT NULL
        THEN
            v_rec.texto_completo := v_rec.texto_completo || '> Observa��es' || CHR (10) || '    ' || var_obs;
        END IF;

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_VIGILANCIAS_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_vigilancias (i_t_doente      VARCHAR2,
                              i_doente        VARCHAR2,
                              i_t_episodio    VARCHAR2,
                              i_episodio      VARCHAR2,
                              i_cod_serv      VARCHAR2,
                              i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec              pck_types_nv.r_resumo;
        v_vigil            pck_types_nv.t_resumo;

        CURSOR registos_vig
        IS
              SELECT DISTINCT
                     sdf_enf_add_hour2date (vig.dt_registo, TO_CHAR (vig.hr_registo, 'HH24:MI')) data_registo,
                     det.num_vigil,
                     pck_enf_resumo_epr.concatenate_resp_vigil (det.num_vigil, det.dt_cri) || NVL2 (vig.obs, 'Observa��es: ' || vig.obs, '') respostas
                FROM sd_enf_vigil_det det, sd_enf_vigil vig
               WHERE det.num_vigil = vig.num_vigil AND vig.t_doente = i_t_doente AND vig.doente = i_doente AND vig.t_episodio = i_t_episodio AND vig.episodio = i_episodio
            ORDER BY det.num_vigil;

        texto_vigil        VARCHAR (200) := traduz ('Vigil�ncia');
        texto_inicial      VARCHAR (500);
        v_t_doente         VARCHAR (20);
        v_doente           VARCHAR (20);
        v_t_episodio       VARCHAR (20);
        v_episodio         VARCHAR (20);
        v_dt_registo       DATE;
        v_utilizador       VARCHAR (20);
        v_descricao        VARCHAR (1000);
        texto_dt_in�cio   VARCHAR (30) := traduz ('Data de In�cio');
    BEGIN
        FOR a IN registos_vig
        LOOP
            IF a.respostas IS NOT NULL
            THEN
                SELECT s.t_doente,
                       s.doente,
                       s.t_episodio,
                       s.episodio,
                       s.dt_registo,
                       NVL (s.user_act, s.user_cri) utilizador,
                       v.descricao
                  INTO v_t_doente,
                       v_doente,
                       v_t_episodio,
                       v_episodio,
                       v_dt_registo,
                       v_utilizador,
                       v_descricao
                  FROM sd_enf_vigil s, gr_enf_vigil v
                 WHERE s.cod_vigil = v.cod_vigil AND s.num_vigil = a.num_vigil;

                v_rec :=
                    create_base_rec (v_t_doente,
                                     v_doente,
                                     v_t_episodio,
                                     v_episodio,
                                     NULL,
                                     v_utilizador,
                                     get_n_mecan (v_utilizador),
                                     '',
                                     a.data_registo);

                v_rec.id_bd := a.num_vigil;

                v_rec.id_resumo := 'VIGILANCIA#' || a.num_vigil;

                v_rec.digest := NULL;                                                                                                          --texto_vigil || ': ' || v_descricao;

                v_rec.texto_completo := v_descricao || CHR (10) || texto_dt_in�cio || ': ' || TO_CHAR (v_dt_registo, 'dd-mm-yyyy') || CHR (10) || a.respostas;
                v_rec.has_more_than_digest := 'S';
                v_vigil (v_vigil.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_vigil;
    END;

    --******************************************************************************
    --******************************************************************************
    FUNCTION get_by_by_dt (i_t_doente      VARCHAR2,
                           i_doente        VARCHAR2,
                           i_t_episodio    VARCHAR2,
                           i_episodio      VARCHAR2,
                           i_cod_serv      VARCHAR2,
                           i_n_mecan       VARCHAR2,
                           i_date          DATE)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec          pck_types_nv.r_resumo;
        v_bh           pck_types_nv.t_resumo;
        texto_digest   VARCHAR2 (300);

        CURSOR bh
        IS
              SELECT t_doente,
                     doente,
                     t_episodio,
                     episodio,
                     utilizador,
                     data,
                     texto_interv || CHR (10) || texto texto,
                     MAX (cod_bh) cod
                FROM (SELECT c.t_doente,
                             c.doente,
                             c.t_episodio,
                             c.episodio,
                             NVL (c.user_act, c.user_cri) utilizador,
                             TRUNC (d.dt_hora, 'dd') data,
                             d.dt_hora,
                             c.cod_bh,
                             pck_enf_resumo_epr.aux_balanco_hidrico_interv (c.t_doente,
                                                                            c.doente,
                                                                            c.t_episodio,
                                                                            c.episodio,
                                                                            sdf_enf_devolve_utilizador (c.cod_bh),
                                                                            d.dt_hora)
                                 texto_interv,
                             pck_enf_resumo_epr.aux_balanco_hidrico_por_epis (c.t_doente,
                                                                              c.doente,
                                                                              c.t_episodio,
                                                                              c.episodio,
                                                                              d.dt_hora)
                                 texto
                        FROM sd_enf_registo_bh_cabec c, sd_enf_registo_bh_det d
                       WHERE     c.cod_bh = d.cod_bh
                             AND c.t_doente = i_t_doente
                             AND c.doente = i_doente
                             AND c.t_episodio = i_t_episodio
                             AND c.episodio = i_episodio
                             AND TRUNC (d.dt_hora, 'dd') = i_date)
            GROUP BY t_doente,
                     doente,
                     t_episodio,
                     episodio,
                     utilizador,
                     data,
                     texto,
                     texto_interv;
    BEGIN
        FOR b IN bh
        LOOP
            IF b.texto IS NOT NULL
            THEN
                v_rec :=
                    create_base_rec (b.t_doente,
                                     b.doente,
                                     b.t_episodio,
                                     b.episodio,
                                     NULL,
                                     b.utilizador,
                                     get_n_mecan (b.utilizador),
                                     '',
                                     b.data);
                v_rec.id_bd := b.cod;
                v_rec.id_resumo := 'BH#' || b.cod;
                v_rec.digest := NULL;
                v_rec.has_more_than_digest := 'S';
                v_rec.texto_completo := b.texto;
                v_rec.dt_registo_display := TO_CHAR (b.data, 'dd-mm-yyyy');
                v_bh (v_bh.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_bh;
    END;

    --******************************************************************************

    FUNCTION get_bh (i_t_doente      VARCHAR2,
                     i_doente        VARCHAR2,
                     i_t_episodio    VARCHAR2,
                     i_episodio      VARCHAR2,
                     i_cod_serv      VARCHAR2,
                     i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec          pck_types_nv.r_resumo;
        v_bh           pck_types_nv.t_resumo;
        texto_digest   VARCHAR2 (300);

        CURSOR bh
        IS
              SELECT t_doente,
                     doente,
                     t_episodio,
                     episodio,
                     utilizador,
                     data,
                     texto_interv || CHR (10) || texto texto,
                     MAX (cod_bh) cod
                FROM (SELECT c.t_doente,
                             c.doente,
                             c.t_episodio,
                             c.episodio,
                             NVL (c.user_act, c.user_cri) utilizador,
                             TRUNC (d.dt_hora, 'dd') data,
                             d.dt_hora,
                             c.cod_bh,
                             pck_enf_resumo_epr.aux_balanco_hidrico_interv (c.t_doente,
                                                                            c.doente,
                                                                            c.t_episodio,
                                                                            c.episodio,
                                                                            sdf_enf_devolve_utilizador (c.cod_bh),
                                                                            d.dt_hora)
                                 texto_interv,
                             pck_enf_resumo_epr.aux_balanco_hidrico_por_epis (c.t_doente,
                                                                              c.doente,
                                                                              c.t_episodio,
                                                                              c.episodio,
                                                                              d.dt_hora)
                                 texto                                                                          /*,
pck_enf_resumo_epr.aux_bh_24h_total(c.t_doente, c.doente, c.t_episodio, c.episodio, d.dt_hora) texto_digest             n�o � usado e causava lentid�o...*/
                        FROM sd_enf_registo_bh_cabec c, sd_enf_registo_bh_det d
                       WHERE c.cod_bh = d.cod_bh AND c.t_doente = i_t_doente AND c.doente = i_doente AND c.t_episodio = i_t_episodio AND c.episodio = i_episodio)
            GROUP BY t_doente,
                     doente,
                     t_episodio,
                     episodio,
                     utilizador,
                     data,
                     texto,
                     texto_interv;
    BEGIN
        FOR b IN bh
        LOOP
            IF b.texto IS NOT NULL
            THEN
                v_rec :=
                    create_base_rec (b.t_doente,
                                     b.doente,
                                     b.t_episodio,
                                     b.episodio,
                                     NULL,
                                     b.utilizador,
                                     get_n_mecan (b.utilizador),
                                     '',
                                     b.data);
                v_rec.id_bd := b.cod;
                v_rec.id_resumo := 'BH#' || b.cod;
                v_rec.digest := NULL;
                v_rec.has_more_than_digest := 'S';
                v_rec.texto_completo := b.texto;
                v_rec.dt_registo_display := TO_CHAR (b.data, 'dd-mm-yyyy');
                v_bh (v_bh.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_bh;
    END;

    --******************************************************************************
    --******************************************************************************
    FUNCTION get_prod_elim_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec          pck_types_nv.r_resumo;
        texto_coloc    VARCHAR (200) := traduz ('Data de Coloca��o');
        descr_turno    VARCHAR (200);
        texto_digest   VARCHAR (200) := traduz ('Turno');
        texto_valor    VARCHAR (20) := traduz ('Valor');
    BEGIN
        SELECT doente,
               t_doente,
               episodio,
               t_episodio,
               cod_reg_prod,
               NULL,
                  descr_prod_elim
               || CHR (10)
               || texto_coloc
               || ': '
               || TO_CHAR (dt_coloc, 'dd-mm-yyyy')
               || CHR (10)
               || texto
               || CHR (10)
               || texto_digest
               || ': '
               || descr_turno
               || ' - '
               || texto_valor
               || ': '
               || valor_ac
                   texto_completo,
               TO_CHAR (dia_enf, 'dd-mm-yyyy'),
               'S'
          INTO v_rec.doente,
               v_rec.t_doente,
               v_rec.episodio,
               v_rec.t_episodio,
               v_rec.id_bd,
               v_rec.digest,
               v_rec.texto_completo,
               v_rec.dt_registo_display,
               v_rec.has_more_than_digest
          FROM (  SELECT SUM (valor) valor_ac,
                         turno,
                         dia_enf,
                         cod_reg_prod,
                         t_doente,
                         doente,
                         t_episodio,
                         episodio,
                         dt_coloc,
                         utilizador,
                         pck_enf_resumo_epr.get_prod_elim_valor_hora (cod_reg_prod, dia_enf, turno) texto,
                         descr_prod_elim,
                         (SELECT q1.descr_turno descr_turno
                            FROM gr_enf_turno q1
                           WHERE q1.cod_turno = turno)
                    FROM (SELECT val.valor,
                                 val.cod_reg_prod,
                                 prod.t_doente,
                                 prod.doente,
                                 prod.t_episodio,
                                 prod.episodio,
                                 prod.dt_coloc,
                                 NVL (prod.user_act, prod.user_cri) utilizador,
                                 sdf_enf_devolve_turno_por_data (
                                     TO_DATE (TO_CHAR (val.dt_registo, 'dd-mm-yyyy') || ' ' || TO_CHAR (val.hr_registo, 'hh24:mi'), 'dd-mm-yyyy hh24:mi'))
                                     turno,
                                 sdf_obtem_dia_enfermagem (TO_DATE (TO_CHAR (val.dt_registo, 'dd-mm-yyyy') || ' ' || TO_CHAR (val.hr_registo, 'hh24:mi'), 'dd-mm-yyyy hh24:mi'))
                                     dia_enf,
                                 gpe.descr_prod_elim
                            FROM sd_enf_reg_prod_elim_valor val, sd_enf_reg_prod_elim prod, gr_enf_prod_elim gpe
                           WHERE val.cod_reg_prod = prod.cod_reg_prod AND prod.cod_prod_elim = gpe.cod_prod_elim AND prod.cod_reg_prod = i_id) tab
                GROUP BY turno,
                         dia_enf,
                         cod_reg_prod,
                         t_doente,
                         doente,
                         cod_reg_prod,
                         dt_coloc,
                         utilizador,
                         descr_prod_elim
                ORDER BY dia_enf);

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_PROD_ELM_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_prod_elim (i_t_doente      VARCHAR2,
                            i_doente        VARCHAR2,
                            i_t_episodio    VARCHAR2,
                            i_episodio      VARCHAR2,
                            i_cod_serv      VARCHAR2,
                            i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec          pck_types_nv.r_resumo;
        v_prod_elim    pck_types_nv.t_resumo;

        CURSOR prod_elim
        IS
              SELECT SUM (valor) valor_ac,
                     turno,
                     dia_enf,
                     cod_reg_prod,
                     t_doente,
                     doente,
                     t_episodio,
                     episodio,
                     dt_coloc,
                     utilizador,
                     pck_enf_resumo_epr.get_prod_elim_valor_hora (cod_reg_prod, dia_enf, turno) texto,
                     descr_prod_elim
                FROM (SELECT val.valor,
                             val.cod_reg_prod,
                             prod.t_doente,
                             prod.doente,
                             prod.t_episodio,
                             prod.episodio,
                             prod.dt_coloc,
                             NVL (prod.user_act, prod.user_cri) utilizador,
                             sdf_enf_devolve_turno_por_data (TO_DATE (TO_CHAR (val.dt_registo, 'dd-mm-yyyy') || ' ' || TO_CHAR (val.hr_registo, 'hh24:mi'), 'dd-mm-yyyy hh24:mi'))
                                 turno,
                             sdf_obtem_dia_enfermagem (TO_DATE (TO_CHAR (val.dt_registo, 'dd-mm-yyyy') || ' ' || TO_CHAR (val.hr_registo, 'hh24:mi'), 'dd-mm-yyyy hh24:mi')) dia_enf,
                             gpe.descr_prod_elim
                        FROM sd_enf_reg_prod_elim_valor val, sd_enf_reg_prod_elim prod, gr_enf_prod_elim gpe
                       WHERE     val.cod_reg_prod = prod.cod_reg_prod
                             AND prod.cod_prod_elim = gpe.cod_prod_elim
                             AND prod.t_doente = i_t_doente
                             AND prod.doente = i_doente
                             AND prod.t_episodio = i_t_episodio
                             AND prod.episodio = i_episodio) tab
            GROUP BY turno,
                     dia_enf,
                     cod_reg_prod,
                     t_doente,
                     doente,
                     t_episodio,
                     episodio,
                     dt_coloc,
                     utilizador,
                     descr_prod_elim
            ORDER BY dia_enf;

        texto_coloc    VARCHAR (200) := traduz ('Data de Coloca��o');
        descr_turno    VARCHAR (200);
        texto_digest   VARCHAR (200) := traduz ('Turno');
        texto_valor    VARCHAR (20) := traduz ('Valor');
    BEGIN
        FOR prod IN prod_elim
        LOOP
            IF prod.texto IS NOT NULL
            THEN
                SELECT turno.descr_turno
                  INTO descr_turno
                  FROM gr_enf_turno turno
                 WHERE turno.cod_turno = prod.turno;

                v_rec :=
                    create_base_rec (prod.t_doente,
                                     prod.doente,
                                     prod.t_episodio,
                                     prod.episodio,
                                     NULL,
                                     prod.utilizador,
                                     get_n_mecan (prod.utilizador),
                                     '',
                                     prod.dia_enf);
                v_rec.id_bd := prod.cod_reg_prod;
                v_rec.id_resumo := 'PROD_ELIM#' || prod.cod_reg_prod;
                v_rec.digest := NULL;                                       --prod.descr_prod_elim||' - '||texto_digest||': '||descr_turno||' - '||texto_valor||': '||prod.valor_ac;
                v_rec.texto_completo :=
                       prod.descr_prod_elim
                    || CHR (10)
                    || texto_coloc
                    || ': '
                    || TO_CHAR (prod.dt_coloc, 'dd-mm-yyyy')
                    || CHR (10)
                    || prod.texto
                    || CHR (10)
                    || texto_digest
                    || ': '
                    || descr_turno
                    || ' - '
                    || texto_valor
                    || ': '
                    || prod.valor_ac;
                v_rec.dt_registo_display := TO_CHAR (prod.dia_enf, 'dd-mm-yyyy');
                v_rec.has_more_than_digest := 'S';
                v_prod_elim (v_prod_elim.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_prod_elim;
    END;

    --******************************************************************************
    --******************************************************************************

    FUNCTION get_escala_glasgow_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec     pck_types_nv.r_resumo;
        v_valor   VARCHAR (100) := traduz ('Valor');
    BEGIN
        SELECT clist.doente,
               clist.t_doente,
               clist.episodio,
               clist.t_episodio,
               clist.cod_check,
               'GLASGOW#' || clist.cod_check,
               NULL,
               pck_enf_resumo_epr.create_texto_esc_glasgow (gla.olhos, gla.verbal, gla.motora) texto_completo,
               TO_CHAR (TO_DATE (TO_CHAR (clist.dt_item, 'dd-mm-yyyy') || ' ' || clist.hr_item, 'dd-mm-yyyy hh24:mi'), 'dd-mm-yyyy') dt_registo_display,
               'S' has_more_than_digest
          INTO v_rec.doente,
               v_rec.t_doente,
               v_rec.episodio,
               v_rec.t_episodio,
               v_rec.id_bd,
               v_rec.id_resumo,
               v_rec.digest,
               v_rec.texto_completo,
               v_rec.dt_registo_display,
               v_rec.has_more_than_digest
          FROM sd_enf_escala_glasgow gla, sd_enf_check_list clist
         WHERE gla.cod_check = clist.cod_check AND gla.cod_check = i_id AND clist.t_doente = i_t_doente AND clist.doente = i_doente;

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_ESCALA_GLASGOW_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_escala_glasgow (i_t_doente      VARCHAR2,
                                 i_doente        VARCHAR2,
                                 i_t_episodio    VARCHAR2,
                                 i_episodio      VARCHAR2,
                                 i_cod_serv      VARCHAR2,
                                 i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec           pck_types_nv.r_resumo;
        v_esc_glasgow   pck_types_nv.t_resumo;
        v_valor         VARCHAR (100) := traduz ('Valor');

        CURSOR esc_glasgow
        IS
            SELECT clist.cod_check,
                   clist.doente,
                   clist.t_doente,
                   clist.t_episodio,
                   clist.episodio,
                   TO_DATE (TO_CHAR (clist.dt_item, 'dd-mm-yyyy') || ' ' || clist.hr_item, 'dd-mm-yyyy hh24:mi') data,
                   NVL (gla.user_act, gla.user_cri) utilizador,
                   pck_enf_resumo_epr.create_texto_esc_glasgow (gla.olhos, gla.verbal, gla.motora) texto_completo,
                   v_valor || ': ' || TO_CHAR (gla.olhos + gla.verbal + gla.motora) texto
              FROM sd_enf_escala_glasgow gla, sd_enf_check_list clist
             WHERE gla.cod_check = clist.cod_check AND clist.t_doente = i_t_doente AND clist.doente = i_doente AND clist.t_episodio = i_t_episodio AND clist.episodio = i_episodio;
    BEGIN
        FOR reg IN esc_glasgow
        LOOP
            IF reg.texto IS NOT NULL
            THEN
                v_rec :=
                    create_base_rec (reg.t_doente,
                                     reg.doente,
                                     reg.t_episodio,
                                     reg.episodio,
                                     NULL,
                                     reg.utilizador,
                                     get_n_mecan (reg.utilizador),
                                     '',
                                     reg.data);
                v_rec.id_bd := reg.cod_check;
                v_rec.id_resumo := 'GLASGOW#' || reg.cod_check;
                v_rec.digest := NULL;                                                                                                                                   --reg.texto;
                v_rec.texto_completo := reg.texto_completo;
                v_rec.dt_registo_display := TO_CHAR (reg.data, 'dd-mm-yyyy');
                v_rec.has_more_than_digest := 'S';
                v_esc_glasgow (v_esc_glasgow.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_esc_glasgow;
    END;

    --******************************************************************************
    --******************************************************************************
    FUNCTION get_glicemia_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec        pck_types_nv.r_resumo;
        v_glicemia   VARCHAR (100) := traduz ('Glic�mia');
        v_tipo       VARCHAR (100) := traduz ('Tipo');
        v_unidades   VARCHAR (100) := traduz ('Unidades');
        v_local      VARCHAR (100) := traduz ('Local');
        v_obs        VARCHAR (100) := traduz ('Obs');
    BEGIN
        SELECT gli.t_doente,
               gli.doente,
               gli.t_episodio,
               gli.episodio,
               gli.cod_check,
               'GLICEMIA#' || cod_check,
               NULL digest,
                  --nvl(gli.user_act,gli.user_cri) utilizador,
                  v_glicemia
               || ': '
               || gli.glicemia
               || ';'
               || CHR (10)
               || DECODE (tipo_ins.descr_tipo_ins, NULL, '', v_tipo || ': ' || tipo_ins.descr_tipo_ins || ';' || CHR (10))
               || DECODE (unid_insulina, NULL, '', v_unidades || ': ' || unid_insulina || ';' || CHR (10))
               || DECODE (loc_admin.descr_loc_admin, NULL, '', v_local || ': ' || loc_admin.descr_loc_admin || ';' || CHR (10))
               || DECODE (gli.obs, NULL, '', v_obs || ': ' || gli.obs || ';')
                   texto_completo,
               TO_CHAR (TO_DATE (TO_CHAR (gli.dt_registo, 'dd-mm-yyyy') || ' ' || TO_CHAR (gli.hr_registo, 'hh24:mi'), 'dd-mm-yyyy hh24:mi'), 'dd-mm-yyyy hh24:mi') dt_registo_display,
               --'Glic�mia' ||': '||gli.glicemia texto,
               'S' has_more_than_digest
          INTO v_rec.t_doente,
               v_rec.doente,
               v_rec.t_episodio,
               v_rec.episodio,
               v_rec.id_bd,
               v_rec.id_resumo,
               v_rec.digest,
               v_rec.texto_completo,
               v_rec.dt_registo_display,
               v_rec.has_more_than_digest
          FROM sd_enf_aval_glicemia gli, gr_enf_tipo_insulina tipo_ins, gr_enf_local_admin loc_admin
         WHERE     gli.frgn_cod_tipo_ins = tipo_ins.cod_tipo_ins(+)
               AND gli.frgn_cod_loc_admin = loc_admin.cod_loc_admin(+)
               AND gli.cod_check = i_id
               AND gli.t_doente = i_t_doente
               AND gli.doente = i_doente;

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_GLICEMIA_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_glicemia (i_t_doente      VARCHAR2,
                           i_doente        VARCHAR2,
                           i_t_episodio    VARCHAR2,
                           i_episodio      VARCHAR2,
                           i_cod_serv      VARCHAR2,
                           i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec            pck_types_nv.r_resumo;
        v_esc_glicemia   pck_types_nv.t_resumo;
        v_glicemia       VARCHAR (100) := traduz ('Glic�mia');
        v_tipo           VARCHAR (100) := traduz ('Tipo');
        v_unidades       VARCHAR (100) := traduz ('Unidades');
        v_local          VARCHAR (100) := traduz ('Local');
        v_obs            VARCHAR (100) := traduz ('Obs');

        CURSOR esc_glicemia
        IS
            SELECT gli.cod_check,
                   gli.t_doente,
                   gli.doente,
                   gli.t_episodio,
                   gli.episodio,
                   TO_DATE (TO_CHAR (gli.dt_registo, 'dd-mm-yyyy') || ' ' || TO_CHAR (gli.hr_registo, 'hh24:mi'), 'dd-mm-yyyy hh24:mi') data,
                   NVL (gli.user_act, gli.user_cri) utilizador,
                      v_glicemia
                   || ': '
                   || gli.glicemia
                   || ';'
                   || CHR (10)
                   || DECODE (tipo_ins.descr_tipo_ins, NULL, '', v_tipo || ': ' || tipo_ins.descr_tipo_ins || ';' || CHR (10))
                   || DECODE (unid_insulina, NULL, '', v_unidades || ': ' || unid_insulina || ';' || CHR (10))
                   || DECODE (loc_admin.descr_loc_admin, NULL, '', v_local || ': ' || loc_admin.descr_loc_admin || ';' || CHR (10))
                   || DECODE (gli.obs, NULL, '', v_obs || ': ' || gli.obs || ';')
                       texto_completo,
                   v_glicemia || ': ' || gli.glicemia texto
              FROM sd_enf_aval_glicemia gli, gr_enf_tipo_insulina tipo_ins, gr_enf_local_admin loc_admin
             WHERE     gli.frgn_cod_tipo_ins = tipo_ins.cod_tipo_ins(+)
                   AND gli.frgn_cod_loc_admin = loc_admin.cod_loc_admin(+)
                   AND gli.t_doente = i_t_doente
                   AND gli.doente = i_doente
                   AND gli.t_episodio = i_t_episodio
                   AND gli.episodio = i_episodio;
    BEGIN
        FOR reg IN esc_glicemia
        LOOP
            IF reg.texto IS NOT NULL
            THEN
                v_rec :=
                    create_base_rec (reg.t_doente,
                                     reg.doente,
                                     reg.t_episodio,
                                     reg.episodio,
                                     NULL,
                                     reg.utilizador,
                                     get_n_mecan (reg.utilizador),
                                     '',
                                     reg.data);
                v_rec.id_bd := reg.cod_check;
                v_rec.id_resumo := 'GLICEMIA#' || reg.cod_check;
                v_rec.digest := NULL;                                                                                                                                   --reg.texto;
                v_rec.texto_completo := reg.texto_completo;
                v_rec.dt_registo_display := TO_CHAR (reg.data, 'dd-mm-yyyy hh24:mi');
                v_rec.has_more_than_digest := 'S';
                v_esc_glicemia (v_esc_glicemia.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_esc_glicemia;
    END;

    --******************************************************************************
    --******************************************************************************
    FUNCTION get_sala_obs_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec   pck_types_nv.r_resumo;
    BEGIN
        SELECT t_doente,
               doente,
               t_episodio,
               episodio,
               n_intern id_bd,
               'SALA_OBS#' || n_intern id_resumo,
               NULL digest,
               texto_completo,
               TO_CHAR (data, 'dd-mm-yyyy') dt_registo_display
          INTO v_rec.t_doente,
               v_rec.doente,
               v_rec.t_episodio,
               v_rec.episodio,
               v_rec.id_bd,
               v_rec.id_resumo,
               v_rec.digest,
               v_rec.texto_completo,
               v_rec.dt_registo_display
          FROM (SELECT doente,
                       t_doente,
                       episodio,
                       t_episodio,
                       n_intern,
                       traduz ('Pedido de coloca��o em SO') texto_completo,
                       TRUNC (dt_cri, 'MI') data,
                       n_mec_ped utilizador
                  FROM sd_urg_ints
                 WHERE id = i_id AND local IS NULL
                --AND cod_so IS NULL
                UNION ALL
                SELECT doente,
                       t_doente,
                       episodio,
                       t_episodio,
                       n_intern,
                          traduz ('In�cio : ')
                       || descr_para_workflow
                       || NVL2 (q2.local, ', ' || q2.local, '')
                       || DECODE (n_mec_ini, n_mec_ped, '', traduz (' ( Pedido por %1)', sdf_urg_obtem_pess_hosp_nome_n (n_mec_ped)))
                           texto_completo,
                       TRUNC (data_ini, 'MI') data,
                       n_mec_ini utilizador
                  FROM sd_urg_ints q0, gr_urg_int_salas q1, gr_urg_int_locais_virtuais q2
                 WHERE id = i_id AND q0.cod_so = q1.cod_so AND q0.cod_so = q2.cod_so(+) AND q0.local = q2.codigo(+) AND data_ini IS NOT NULL AND n_mec_ini IS NOT NULL
                UNION ALL
                SELECT doente,
                       t_doente,
                       episodio,
                       t_episodio,
                       n_intern,
                       traduz ('Fim    : %1', descr_para_workflow) || NVL2 (q2.local, ', ' || q2.local, '') texto_completo,
                       TRUNC (data_fim, 'MI') data,
                       n_mec_fim utilizador
                  FROM sd_urg_ints q0, gr_urg_int_salas q1, gr_urg_int_locais_virtuais q2
                 WHERE id = i_id AND q0.cod_so = q1.cod_so AND q0.cod_so = q2.cod_so(+) AND q0.local = q2.codigo(+) AND data_fim IS NOT NULL AND n_mec_fim IS NOT NULL
                ORDER BY data);

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_SALA_OBS_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************

    FUNCTION get_sala_obs (i_t_doente      VARCHAR2,
                           i_doente        VARCHAR2,
                           i_t_episodio    VARCHAR2,
                           i_episodio      VARCHAR2,
                           i_cod_serv      VARCHAR2,
                           i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec       pck_types_nv.r_resumo;
        v_esc_obs   pck_types_nv.t_resumo;

        CURSOR cur_so
        IS
            SELECT n_intern,
                   traduz ('Pedido de coloca��o em SO') texto_completo,
                   TRUNC (dt_cri, 'MI') data,
                   n_mec_ped utilizador
              FROM sd_urg_ints
             WHERE t_doente = i_t_doente AND doente = i_doente AND t_episodio = i_t_episodio AND episodio = i_episodio AND local IS NULL
            --AND cod_so IS NULL
            UNION ALL
            SELECT n_intern,
                      traduz ('In�cio : ')
                   || descr_para_workflow
                   || NVL2 (q2.local, ', ' || q2.local, '')
                   || DECODE (n_mec_ini, n_mec_ped, '', traduz (' ( Pedido por %1)', sdf_urg_obtem_pess_hosp_nome_n (n_mec_ped)))
                       texto_completo,
                   TRUNC (data_ini, 'MI') data,
                   n_mec_ini utilizador
              FROM sd_urg_ints q0, gr_urg_int_salas q1, gr_urg_int_locais_virtuais q2
             WHERE     t_doente = i_t_doente
                   AND doente = i_doente
                   AND t_episodio = i_t_episodio
                   AND episodio = i_episodio
                   AND q0.cod_so = q1.cod_so
                   AND q0.cod_so = q2.cod_so(+)
                   AND q0.local = q2.codigo(+)
                   AND data_ini IS NOT NULL
                   AND n_mec_ini IS NOT NULL
            UNION ALL
            SELECT n_intern,
                   traduz ('Fim    : %1', descr_para_workflow) || NVL2 (q2.local, ', ' || q2.local, '') texto_completo,
                   TRUNC (data_fim, 'MI') data,
                   n_mec_fim utilizador
              FROM sd_urg_ints q0, gr_urg_int_salas q1, gr_urg_int_locais_virtuais q2
             WHERE     t_doente = i_t_doente
                   AND doente = i_doente
                   AND t_episodio = i_t_episodio
                   AND episodio = i_episodio
                   AND q0.cod_so = q1.cod_so
                   AND q0.cod_so = q2.cod_so(+)
                   AND q0.local = q2.codigo(+)
                   AND data_fim IS NOT NULL
                   AND n_mec_fim IS NOT NULL
            ORDER BY data;
    BEGIN
        FOR reg IN cur_so
        LOOP
            IF reg.texto_completo IS NOT NULL
            THEN
                v_rec :=
                    create_base_rec (i_t_doente,
                                     i_doente,
                                     i_t_episodio,
                                     i_episodio,
                                     NULL,
                                     '',
                                     reg.utilizador,
                                     '',
                                     reg.data);
                v_rec.id_bd := reg.n_intern;
                v_rec.id_resumo := 'SALA_OBS#' || reg.n_intern;
                v_rec.digest := NULL;
                -- v_rec.digest := reg.texto;
                v_rec.texto_completo := reg.texto_completo;
                v_rec.dt_registo_display := TO_CHAR (reg.data, 'dd-mm-yyyy');
                --v_rec.has_more_than_digest := 'S';
                v_esc_obs (v_esc_obs.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_esc_obs;
    END;

    --******************************************************************************
    --******************************************************************************

    FUNCTION get_notas_enf (i_t_doente      VARCHAR2,
                            i_doente        VARCHAR2,
                            i_t_episodio    VARCHAR2,
                            i_episodio      VARCHAR2,
                            i_cod_serv      VARCHAR2,
                            i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec         pck_types_nv.r_resumo;
        v_esc_notas   pck_types_nv.t_resumo;

        CURSOR rec_ntenf
        IS
              SELECT s1.data,
                     s1.utilizador utilizador,
                     s1.notas_enf texto_completo,
                     s.id_notas
                FROM sd_enf_notas s, sd_enf_notas_det s1
               WHERE s.id_notas = s1.id_notas AND s.t_doente = i_t_doente AND s.doente = i_doente AND s.t_episodio = i_t_episodio AND s.episodio = i_episodio
            ORDER BY s1.data;
    BEGIN
        FOR reg IN rec_ntenf
        LOOP
            IF reg.texto_completo IS NOT NULL
            THEN
                v_rec :=
                    create_base_rec (i_t_doente,
                                     i_doente,
                                     i_t_episodio,
                                     i_episodio,
                                     NULL,
                                     reg.utilizador,
                                     usersys2nmecan (reg.utilizador),
                                     '',
                                     reg.data);
                v_rec.id_bd := reg.id_notas;
                v_rec.id_resumo := 'NOTAS_ENF#' || reg.id_notas;
                v_rec.digest := NULL;
                -- v_rec.digest := reg.texto;
                v_rec.texto_completo := reg.texto_completo;
                v_rec.dt_registo_display := TO_CHAR (reg.data, 'dd-mm-yyyy');
                --v_rec.has_more_than_digest := 'S';
                v_esc_notas (v_esc_notas.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_esc_notas;
    END;

    --******************************************************************************
    FUNCTION get_notas_enf_by_id (i_t_doente VARCHAR2, i_doente VARCHAR2, i_id NUMBER)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec                  pck_types_nv.r_resumo;
        row_sd_enf_notas_det   sd_enf_notas_det%ROWTYPE;
    BEGIN
        SELECT s.t_doente,
               s.doente,
               s.t_episodio,
               s.episodio,
               s1.data,
               TO_CHAR (s1.data, 'YYYY-MM-DD HH24:MI'),
               s1.utilizador,
               s1.notas_enf,
               s1.id_notas_det,
               'NOTAS_ENF#' || s1.id_notas_det
          INTO v_rec.t_doente,
               v_rec.doente,
               v_rec.t_episodio,
               v_rec.episodio,
               v_rec.dt_registo,
               v_rec.dt_registo_display,
               v_rec.user_sys,
               v_rec.texto_completo,
               v_rec.id_bd,
               v_rec.id_resumo
          FROM sd_enf_notas s, sd_enf_notas_det s1
         WHERE s.id_notas = s1.id_notas AND s1.id_notas_det = i_id;

        v_rec.n_mecan := usersys2nmecan (v_rec.user_sys);

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_NOTAS_ENF_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    FUNCTION get_notas_enf_interv (i_t_doente      VARCHAR2,
                                   i_doente        VARCHAR2,
                                   i_t_episodio    VARCHAR2,
                                   i_episodio      VARCHAR2,
                                   i_cod_serv      VARCHAR2,
                                   i_n_mecan       VARCHAR2)
        RETURN pck_types_nv.t_resumo
    IS
        v_rec         pck_types_nv.r_resumo;
        v_esc_notas   pck_types_nv.t_resumo;

        CURSOR rec_ntenf
        IS
              SELECT cod_check,
                     s1.data,
                        pckt_enf_plano_cuidados.get_descr_interv (NVL (id_agrupador,
                                                                       pckt_enf_plano_cuidados.obtem_id_agrupador (s1.t_doente,
                                                                                                                   s1.doente,
                                                                                                                   s1.t_episodio,
                                                                                                                   s1.episodio,
                                                                                                                   cod_item,
                                                                                                                   tabela,
                                                                                                                   s1.data,
                                                                                                                   s1.data + INTERVAL '1' HOUR)))
                     || ' - '
                     || obs
                     || CHR (10)
                     || pck_enf_caract.get_caracterizacao (id_agrupador)
                         obs,
                     s1.utilizador
                FROM (SELECT t_doente,
                             doente,
                             t_episodio,
                             episodio,
                             cod_check,
                             id_agrupador,
                             cod_item,
                             tabela,
                             sdf_enf_add_hour2date (dt_item, hr_item) data,
                             obs,
                             NVL (user_act, user_cri) utilizador
                        FROM sd_enf_check_list s
                       WHERE     s.t_doente = i_t_doente
                             AND s.doente = i_doente
                             AND s.t_episodio = i_t_episodio
                             AND s.episodio = i_episodio
                             AND s.tipo_item = 'Interv'
                             AND s.hr_item LIKE '__:__'
                             AND LENGTH (s.hr_item) = 5
                             AND s.obs IS NOT NULL) s1
            ORDER BY s1.data;

        CURSOR rec_ntenf2
        IS
              SELECT s1.data,
                     s1.utilizador utilizador,
                     s1.notas_enf texto_completo,
                     s.id_notas
                FROM sd_enf_notas s, sd_enf_notas_det s1
               WHERE s.id_notas = s1.id_notas AND s.t_doente = i_t_doente AND s.doente = i_doente AND s.t_episodio = i_t_episodio AND s.episodio = i_episodio
            ORDER BY s1.data;
    BEGIN
        FOR reg IN rec_ntenf
        LOOP
            v_rec :=
                create_base_rec (i_t_doente,
                                 i_doente,
                                 i_t_episodio,
                                 i_episodio,
                                 NULL,
                                 reg.utilizador,
                                 usersys2nmecan (reg.utilizador),
                                 '',
                                 reg.data);
            v_rec.id_bd := reg.cod_check;
            v_rec.id_resumo := 'NOTAS_ENF_INT#' || reg.cod_check;
            v_rec.sub_tipo := 'NOTAS_ENF_INT';
            v_rec.digest := NULL;
            v_rec.dt_registo_display := TO_CHAR (reg.data, 'dd-mm-rrrr');
            v_rec.texto_completo := reg.obs;
            v_esc_notas (v_esc_notas.COUNT) := v_rec;
        END LOOP;

        FOR reg IN rec_ntenf2
        LOOP
            IF reg.texto_completo IS NOT NULL
            THEN
                v_rec :=
                    create_base_rec (i_t_doente,
                                     i_doente,
                                     i_t_episodio,
                                     i_episodio,
                                     NULL,
                                     reg.utilizador,
                                     usersys2nmecan (reg.utilizador),
                                     '',
                                     reg.data);
                v_rec.id_bd := reg.id_notas;
                v_rec.id_resumo := 'NOTAS_ENF_INT#' || reg.id_notas;
                v_rec.sub_tipo := 'NOTAS_ENF';
                v_rec.digest := NULL;
                -- v_rec.digest := reg.texto;
                v_rec.texto_completo := reg.texto_completo;
                v_rec.dt_registo_display := TO_CHAR (reg.data, 'dd-mm-yyyy');
                --v_rec.has_more_than_digest := 'S';
                v_esc_notas (v_esc_notas.COUNT) := v_rec;
            END IF;
        END LOOP;

        RETURN v_esc_notas;
    END;

    --******************************************************************************
    FUNCTION get_notas_enf_interv_by_id (i_t_doente         VARCHAR2,
                                         i_doente           VARCHAR2,
                                         i_id               NUMBER,
                                         i_source_detail    VARCHAR2)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec   pck_types_nv.r_resumo;
    BEGIN
        CASE i_source_detail
            WHEN 'NOTAS_ENF_INT'
            THEN
                SELECT t_doente,
                       doente,
                       t_episodio,
                       episodio,
                       sdf_enf_add_hour2date (dt_item, hr_item) dt_registo,
                       TO_CHAR (sdf_enf_add_hour2date (dt_item, hr_item), 'dd-mm-rrrr') dt_registo_display,
                       NVL (user_act, user_cri) user_sys,
                          pckt_enf_plano_cuidados.get_descr_interv (
                              NVL (id_agrupador,
                                   pckt_enf_plano_cuidados.obtem_id_agrupador (t_doente,
                                                                               doente,
                                                                               t_episodio,
                                                                               episodio,
                                                                               cod_item,
                                                                               tabela,
                                                                               sdf_enf_add_hour2date (dt_item, hr_item),
                                                                               sdf_enf_add_hour2date (dt_item, hr_item) + INTERVAL '1' HOUR)))
                       || ' - '
                       || obs
                       || CHR (10)
                       || pck_enf_caract.get_caracterizacao (id_agrupador)
                           texto_completo,
                       cod_check id_bd,
                       'NOTAS_ENF_INT#' || cod_check id_resumo,
                       'NOTAS_ENF_INT'
                  INTO v_rec.t_doente,
                       v_rec.doente,
                       v_rec.t_episodio,
                       v_rec.episodio,
                       v_rec.dt_registo,
                       v_rec.dt_registo_display,
                       v_rec.user_sys,
                       v_rec.texto_completo,
                       v_rec.id_bd,
                       v_rec.id_resumo,
                       v_rec.sub_tipo
                  FROM sd_enf_check_list
                 WHERE cod_check = i_id;
            ELSE
                SELECT s.t_doente,
                       s.doente,
                       s.t_episodio,
                       s.episodio,
                       s1.data,
                       TO_CHAR (s1.data, 'YYYY-MM-DD HH24:MI'),
                       s1.utilizador,
                       s1.notas_enf,
                       s1.id_notas_det,
                       'NOTAS_ENF_INT#' || s1.id_notas_det,
                       'NOTAS_ENF'
                  INTO v_rec.t_doente,
                       v_rec.doente,
                       v_rec.t_episodio,
                       v_rec.episodio,
                       v_rec.dt_registo,
                       v_rec.dt_registo_display,
                       v_rec.user_sys,
                       v_rec.texto_completo,
                       v_rec.id_bd,
                       v_rec.id_resumo,
                       v_rec.sub_tipo
                  FROM sd_enf_notas s, sd_enf_notas_det s1
                 WHERE s.id_notas = s1.id_notas AND s1.id_notas_det = i_id;
        END CASE;

        v_rec.n_mecan := usersys2nmecan (v_rec.user_sys);

        RETURN v_rec;
    EXCEPTION
        WHEN OTHERS
        THEN
            pck_pe_logging.erro ('PCK_ENF_RESUMO_EPR.get_NOTAS_ENF_INTERV_by_ID', SQLERRM, '[i_T_DOENTE=' || i_t_doente || '][i_DOENTE=' || i_doente || '][i_ID=' || i_id || ']');
            RETURN v_rec;
    END;

    --******************************************************************************
    --******************************************************************************
    FUNCTION create_texto_esc_glasgow (i_olhos NUMBER, i_verbal NUMBER, i_motora NUMBER)
        RETURN VARCHAR
    IS
        texto          VARCHAR (1000);
        texto_olhos    VARCHAR (100) := traduz ('Olhos');
        texto_verbal   VARCHAR (100) := traduz ('Resposta verbal');
        texto_motora   VARCHAR (100) := traduz ('Resposta motora');
        texto_valor    VARCHAR (100) := traduz ('Valor');
        score_final    NUMBER;
    BEGIN
        score_final := i_olhos + i_verbal + i_motora;
        texto :=
               texto_olhos
            || ': '
            || i_olhos
            || ';'
            || CHR (10)
            || texto_verbal
            || ': '
            || i_verbal
            || ';'
            || CHR (10)
            || texto_motora
            || ': '
            || i_motora
            || ';'
            || CHR (10)
            || CHR (10)
            || texto_valor
            || ': '
            || score_final;
        RETURN texto;
    END;


    FUNCTION concatenate_resp_vigil (i_n_vigil VARCHAR2, i_cod_item VARCHAR2)
        RETURN VARCHAR
    IS
        texto   VARCHAR (2000);

        CURSOR resps
        IS
              SELECT '    ' || CASE WHEN tipo = 'CHECK' AND valor_bd = 'S' THEN gr.descricao ELSE                                                             /*NVL(det.valor_ut, */
                                                                                                 gr.descricao || ': ' || det.valor_bd                                          /*)*/
                                                                                                                                     END resposta
                FROM sd_enf_vigil_det det, gr_enf_vigil_det gr
               WHERE     det.num_vigil = i_n_vigil
                     AND gr.cod_item = i_cod_item
                     AND gr.cod_vigil = det.cod_vigil
                     AND gr.cod_item = det.cod_item
                     AND gr.cod_vigil_det = det.cod_vigil_det
                     AND TRIM (det.valor_bd) IS NOT NULL
                     AND NOT (tipo = 'CHECK' AND NVL (UPPER (det.valor_bd), 'N') = 'N')
            ORDER BY gr.ordem;
    BEGIN
        FOR resp IN resps
        LOOP
            texto := texto || resp.resposta || CHR (10);
        END LOOP;

        RETURN RTRIM (texto, ', ');
    END;



    FUNCTION concatenate_sin_vitais (i_t_doente      VARCHAR2,
                                     i_doente        VARCHAR2,
                                     i_t_episodio    VARCHAR2,
                                     i_episodio      VARCHAR2,
                                     i_n_triagem     NUMBER)
        RETURN VARCHAR
    IS
        texto   VARCHAR (2000);

        CURSOR c_sin_vitais
        IS
              SELECT p_indic.descr_indic || ': ' || c_indic.valor || ' ' || p_indic.unidade ind
                FROM sd_enf_check_indic c_indic, gr_enf_indic p_indic, sd_enf_check_list c_list
               WHERE     c_indic.n_triag = i_n_triagem
                     AND c_list.t_doente = i_t_doente
                     AND c_list.doente = i_doente
                     AND c_list.t_episodio = i_t_episodio
                     AND c_list.episodio = i_episodio
                     AND c_indic.cod_indic = p_indic.cod_indic
                     AND c_indic.cod_check = c_list.cod_check
                     AND c_indic.valor IS NOT NULL
            ORDER BY c_indic.n_triag;

        flg     NUMBER;
    BEGIN
        texto := '';
        flg := 0;

        FOR c IN c_sin_vitais
        LOOP
            texto := RTRIM (texto, ' ') || '; ' || c.ind;
            flg := 1;
        END LOOP;

        IF flg = 1
        THEN
            texto := traduz ('Outros dados') || ': ' || LTRIM (texto, '; ');
            RETURN texto;
        ELSE
            RETURN LTRIM (texto, ';');
        END IF;
    END;

    FUNCTION get_n_mecan (i_user_sys VARCHAR)
        RETURN VARCHAR
    IS
        n_mecan_v   VARCHAR (2000);
    BEGIN
        SELECT hd.n_mecan
          INTO n_mecan_v
          FROM sd_pess_hosp_def hd
         WHERE hd.user_sys = i_user_sys;

        RETURN n_mecan_v;
    EXCEPTION
        WHEN OTHERS
        THEN
            RETURN '';
    END;

    /*FUN��O QUE RECEBE STRING COM IDS DOS LOCAIS DAS PLACAS E DEVOLVE OS LOCAIS CONCATENADOS*/

    FUNCTION get_locais_electro (i_ids_locais VARCHAR)
        RETURN VARCHAR
    IS
        locais          VARCHAR (1000);
        cur_id_locais   VARCHAR (1000);
        cur_id_local    VARCHAR (20);
        cur_local       VARCHAR (100);
    BEGIN
        cur_id_locais := i_ids_locais;

        IF INSTR (cur_id_locais, '#') = 1
        THEN
            cur_id_locais := SUBSTR (cur_id_locais, 2);
        END IF;

        WHILE LENGTH (cur_id_locais) > 1
        LOOP
            cur_id_local := SUBSTR (cur_id_locais, 1, INSTR (cur_id_locais, '#') - 1);

            SELECT descr_local_placa
              INTO cur_local
              FROM gr_enf_local_placa
             WHERE cod_local_placa = cur_id_local;

            locais := locais || cur_local || ', ';
            cur_id_locais := SUBSTR (cur_id_locais, INSTR (cur_id_locais, '#') + 1);
        END LOOP;

        RETURN RTRIM (locais, ', ');
    END;

    FUNCTION convert_long_aval_inicial (i_n_ep_arv IN sd_enf_ep_arv.n_ep_arv%TYPE, i_cod_ep_item IN gr_enf_ep_item.cod_ep_item%TYPE)
        RETURN VARCHAR
    IS
        texto   VARCHAR (10000);
    BEGIN
        texto := pck_enf_ep_arv.devolve_texto_dets (i_n_ep_arv, i_cod_ep_item, '');
        RETURN texto;
    END;

    FUNCTION get_prod_elim_valor_hora (i_n_cod_reg_prod VARCHAR, i_dt_registo DATE, i_turno VARCHAR)
        RETURN VARCHAR
    IS
        texto   VARCHAR (2000);

        CURSOR valores
        IS
            SELECT TO_CHAR (dt_registo, 'dd-mm-yyyy') data, TO_CHAR (hr_registo, 'hh24:mi') hora, valor
              FROM sd_enf_reg_prod_elim_valor
             WHERE     cod_reg_prod = i_n_cod_reg_prod
                   AND sdf_obtem_dia_enfermagem (TO_DATE (TO_CHAR (dt_registo, 'dd-mm-yyyy') || ' ' || TO_CHAR (hr_registo, 'hh24:mi'), 'dd-mm-yyyy hh24:mi')) = i_dt_registo
                   AND sdf_enf_obtem_cod_turno (TO_DATE (TO_CHAR (dt_registo, 'dd-mm-yyyy') || ' ' || TO_CHAR (hr_registo, 'hh24:mi'), 'dd-mm-yyyy hh24:mi')) = i_turno;
    BEGIN
        FOR val IN valores
        LOOP
            texto := texto || val.data || ' ' || val.hora || ': ' || val.valor || CHR (10);
        END LOOP;

        RETURN texto;
    END;

    FUNCTION aux_balanco_hidrico_por_epis (i_t_doente      VARCHAR2,
                                           i_doente        VARCHAR2,
                                           i_t_episodio    VARCHAR2,
                                           i_episodio      VARCHAR2,
                                           i_data          DATE)
        RETURN VARCHAR2
    IS
        v_acc   VARCHAR2 (4000) := '';

        TYPE rc IS REF CURSOR;

        vrc     rc;

        v_v1    NUMBER;
        v_v2    VARCHAR2 (400);
        v_v3    VARCHAR2 (400);
    BEGIN
        pck_enf_bh_total_acumulado.sdp_enf_bh_tot_acu (i_t_episodio,
                                                       i_episodio,
                                                       i_t_doente,
                                                       i_doente,
                                                       i_data,
                                                       i_data - 300,
                                                       vrc);

        LOOP
            FETCH vrc INTO v_v1, v_v2;                                                                                                                                      --,v_V3;

            EXIT WHEN vrc%NOTFOUND;
            v_acc := v_acc || INITCAP (v_v2) || v_v1 || ';' || CHR (10);
        END LOOP;

        CLOSE vrc;

        RETURN v_acc;
    END;

    FUNCTION aux_bh_24h_total (i_t_doente      VARCHAR2,
                               i_doente        VARCHAR2,
                               i_t_episodio    VARCHAR2,
                               i_episodio      VARCHAR2,
                               i_data          DATE)
        RETURN VARCHAR2
    IS
        v_acc       VARCHAR2 (4000) := '';

        TYPE rc IS REF CURSOR;

        vrc         rc;

        v_v1        NUMBER;
        v_v2        VARCHAR2 (400);
        v_v3        VARCHAR2 (400);
        count_cur   NUMBER := 0;
    BEGIN
        pck_enf_bh_total_acumulado.sdp_enf_bh_tot_acu (i_t_episodio,
                                                       i_episodio,
                                                       i_t_doente,
                                                       i_doente,
                                                       i_data,
                                                       i_data - 300,
                                                       vrc);

        LOOP
            FETCH vrc INTO v_v1, v_v2;                                                                                                                                      --,v_V3;

            EXIT WHEN vrc%NOTFOUND;
            count_cur := count_cur + 1;

            IF count_cur = 1 OR count_cur = 5
            THEN
                v_acc := v_acc || INITCAP (v_v2) || v_v1 || ';' || CHR (10);
            END IF;
        END LOOP;

        CLOSE vrc;

        RETURN v_acc;
    END;


    FUNCTION aux_balanco_hidrico_interv (i_t_doente      VARCHAR2,
                                         i_doente        VARCHAR2,
                                         i_t_episodio    VARCHAR2,
                                         i_episodio      VARCHAR2,
                                         utilizador      VARCHAR2,
                                         i_data          DATE)
        RETURN VARCHAR2
    IS
        v_acc   VARCHAR2 (4000) := '';

        TYPE rc IS REF CURSOR;

        vrc     rc;

        v_v1    DATE;
        v_v2    VARCHAR2 (400);
        v_v3    VARCHAR2 (400);
    BEGIN
        pck_enf_bh_total_acumulado.sdp_enf_bh_interv (i_t_episodio,
                                                      i_episodio,
                                                      i_t_doente,
                                                      i_doente,
                                                      i_data,
                                                      i_data - 300,
                                                      utilizador,
                                                      vrc);

        LOOP
            FETCH vrc INTO v_v1, v_v2, v_v3;

            EXIT WHEN vrc%NOTFOUND;
            v_acc :=
                   v_acc
                || SUBSTR (v_v3, 0, INSTR (v_v3, ':') - 1)
                || ' | '
                || v_v2
                || ' ('
                || TO_CHAR (v_v1, 'dd-mm-yyyy hh24:mi')
                || '): '
                || SUBSTR (v_v3, INSTR (v_v3, ':') + 1)
                || CHR (10);
        END LOOP;

        CLOSE vrc;

        RETURN v_acc;
    END;

    FUNCTION concatenate_resumo_ai (i_t_doente      VARCHAR,
                                    i_doente        VARCHAR,
                                    i_t_episodio    VARCHAR,
                                    i_episodio      VARCHAR)
        RETURN VARCHAR
    IS
        texto       VARCHAR (12000);
        long_conv   VARCHAR (12000);

        CURSOR resumos
        IS
              SELECT DISTINCT eid.n_ep_arv, eei.cod_ep_item, eei.descr_ep_item
                FROM sd_enf_ep_item_det eid,
                     gr_enf_ep_item eei,
                     gr_enf_ep_item_det geid,
                     sd_enf_ep_arv seei
               WHERE     eid.cod_ep_item_det = geid.cod_ep_item_det
                     AND geid.cod_ep_item = eei.cod_ep_item
                     AND eid.n_ep_arv = seei.n_ep_arv
                     AND seei.t_doente = i_t_doente
                     AND seei.doente = i_doente
                     AND seei.t_episodio = i_t_episodio
                     AND seei.episodio = i_episodio
            ORDER BY eei.descr_ep_item;

        cur_desc    VARCHAR (300);
    BEGIN
        FOR r IN resumos
        LOOP
            long_conv := pck_enf_resumo_epr.convert_long_aval_inicial (r.n_ep_arv, r.cod_ep_item);

            IF r.descr_ep_item = cur_desc
            THEN
                texto := texto || CHR (9) || REPLACE (long_conv, '. ', ';' || CHR (10) || CHR (9)) || CHR (10);
            ELSE
                texto := texto || CHR (10) || r.descr_ep_item || ':' || CHR (10);
                texto := texto || CHR (9) || REPLACE (long_conv, '. ', ';' || CHR (10) || CHR (9)) || CHR (10);
                cur_desc := r.descr_ep_item;
            END IF;
        END LOOP;

        RETURN texto;
    END;

    /*FUN��O PARA CONVERTER VALOR DE COMBUR PARA NOTA��O (+ E -)*/
    FUNCTION convert_combur_notation (i_valor NUMBER)
        RETURN VARCHAR
    IS
        conversao   VARCHAR (300);
        val_atual   NUMBER := i_valor;
    BEGIN
        IF val_atual > 0
        THEN
            WHILE val_atual > 0
            LOOP
                conversao := conversao || '+';
                val_atual := val_atual - 1;
            END LOOP;
        ELSE
            WHILE val_atual < 0
            LOOP
                conversao := conversao || '-';
                val_atual := val_atual + 1;
            END LOOP;
        END IF;

        RETURN conversao;
    END;

    FUNCTION create_base_rec (p_t_doente        IN VARCHAR2,
                              p_doente          IN VARCHAR2,
                              p_t_episodio      IN VARCHAR2,
                              p_episodio        IN VARCHAR2,
                              p_cod_serv        IN VARCHAR2,
                              p_user_sys        IN VARCHAR2,
                              p_n_mecan         IN VARCHAR2,
                              p_descr_n_mecan   IN VARCHAR2,
                              p_dt_registo      IN DATE)
        RETURN pck_types_nv.r_resumo
    IS
        v_rec   pck_types_nv.r_resumo;
    BEGIN
        v_rec.t_doente := p_t_doente;
        v_rec.doente := p_doente;
        v_rec.t_episodio := p_t_episodio;
        v_rec.episodio := p_episodio;
        v_rec.cod_serv := p_cod_serv;
        v_rec.user_sys := p_user_sys;
        v_rec.n_mecan := p_n_mecan;
        v_rec.descr_n_mecan := p_descr_n_mecan;
        v_rec.dt_registo := p_dt_registo;
        RETURN v_rec;
    END;
	
	--******************************************************************************

    FUNCTION get_diet      (i_t_doente      VARCHAR2,
                            i_doente        VARCHAR2,
                            i_t_episodio    VARCHAR2,
                            i_episodio      VARCHAR2,
                            i_cod_serv      VARCHAR2,
                            i_n_mecan       VARCHAR2,
                            i_show_resume_header VARCHAR2 DEFAULT 'S')
        RETURN pck_types_nv.t_resumo
    IS
        v_rec          pck_types_nv.r_resumo;
        v_diet         pck_types_nv.t_resumo;

        v_text varchar2(4000);
        v_text_interv varchar2(4000);
        v_text_diag varchar2(4000);

        v_score_total number;        -- JMS-244521: nova variavel
        v_idade_anos  varchar2(100); -- JMS-244521: nova variavel
        v_idade_aux   number;        -- JMS-244521: nova variavel	
		v_header      varchar2(4000);		
    BEGIN

        --t_tipo := 'DIET';

        -- JMS-244521 : Obter idade do doente
        BEGIN
            select sdf_enf_calcula_idade_doe(i_t_doente, i_doente)
            into v_idade_anos
            from dual;
        EXCEPTION
            WHEN OTHERS THEN
                v_idade_anos := null;
        END;
        IF v_idade_anos IS NOT NULL
        THEN
            v_idade_anos := substr(v_idade_anos, 1, instr(v_idade_anos, ' '));
        END IF;
        -- !!
		
        $IF PCK_ENF_MODULOS_ATIVOS.DIET
        $THEN
            DECLARE
                v_diet_data diet.dietk_estado_nutricional.t_rastreio_doente;
            BEGIN

                v_diet_data := diet.dietk_interface_ext.comunicar_dados_nutricional(I_T_DOENTE=>i_t_doente, I_DOENTE=>i_doente, I_T_EPISODIO=>i_t_episodio, I_EPISODIO=>i_episodio);

                --VERIFICAR SE RECEBEU ALGUM DADO
                IF V_DIET_DATA.COUNT > 0 THEN
                    FOR i IN v_diet_data.FIRST .. v_diet_data.LAST
                    LOOP
                        --escalas adulto
                        --VERIFICAR SE TEM ALGUMA ESCALA ADULTO
                        IF v_diet_data(i).rastreio.COUNT > 0 THEN
                            FOR k IN v_diet_data(i).rastreio.FIRST .. v_diet_data(i).rastreio.LAST
                            LOOP

                                -- JMS-244521: Se a idade do doente � >= 70 anos => somar 1 ao valor do score total; caso contr�rio, n�o fazer nada;
                                v_score_total := v_diet_data(i).rastreio(k).estado_nutricional + v_diet_data(i).rastreio(k).severidade_doenca;
                                IF to_number(v_idade_anos) >= 70 THEN
                                    v_score_total := v_score_total + 1;
                                    v_idade_aux := 1;
                                END IF;
                                --!!
								
								--JMS-244523 adriano.alves
                                IF i_show_resume_header = 'S' then
                                    v_header := SDF_URG_OBTEM_PESS_HOSP_NOME(v_diet_data(i).rastreio(k).USER_CRI) || ', '|| TO_CHAR(v_diet_data(i).rastreio(k).DT_CRI,'DD-MM-YYYY HH24:MI:SS');
                                ELSE
                                    v_header := '';
                                END IF;
								
                                v_text := v_text || chr(10) || chr(10) || 'Escala Risco Nutricional do Adulto ('||v_header || ' ['||traduz ('score total')||': '|| v_score_total ||'] )'; -- JMS-244521 : Incluir informa��o 'USER_CRI' , 'DT_CRI' e 'V_SCORE_TOTAL'. 
                                v_text := v_text || chr(10) || 'IMC <20.5 -> ' || v_diet_data(i).rastreio(k).imc_menor20;
                                v_text := v_text || chr(10) || 'Utente perdeu peso nos �ltimos 3 meses -> ' || v_diet_data(i).rastreio(k).perde_peso3m;
                                v_text := v_text || chr(10) || 'Utente reduziu a sua ingest�o alimentar na �ltima semana -> ' || v_diet_data(i).rastreio(k).reduz_ing_alimsem;
                                v_text := v_text || chr(10) || 'Utente est� severamente doente (ex: Internado UCI) -> ' || v_diet_data(i).rastreio(k).severamente_doente;
                                v_text := v_text || chr(10) || '        Deteriora��o do estado nutricional -> ' || v_diet_data(i).rastreio(k).estado_nutricional;
                                v_text := v_text || chr(10) || '        Gravidade de doenca ( aumento nas necessidades) -> ' || v_diet_data(i).rastreio(k).severidade_doenca;
								v_text := v_text || chr(10) || '        Idade >= 70 anos -> ' || NVL(v_idade_aux, 0); -- JMS-244521 : Incluir

                                --INTERVENCOES ADULTO
                                IF v_diet_data(i).rastreio(k).intervencoes.COUNT > 0 THEN
                                    --intervs
                                    for l IN v_diet_data(i).rastreio(k).intervencoes.FIRST .. v_diet_data(i).rastreio(k).intervencoes.LAST
                                    LOOP
                                        if v_text_interv is null then v_text_interv := 'Interven��es'; end if;
                                        v_text_interv := v_text_interv || chr(10) || v_diet_data(i).rastreio(k).intervencoes(l).descr_interv || '   (' || v_diet_data(i).rastreio(k).intervencoes(l).user_cri || ', ' ||  v_diet_data(i).rastreio(k).intervencoes(l).dt_cri || ')';
                                    END LOOP;
                                END IF;
                                --diags
                                --DIAGS ADULTO
                                IF v_diet_data(i).rastreio(k).diagnosticos.COUNT > 0 THEN
                                    for l IN v_diet_data(i).rastreio(k).diagnosticos.FIRST .. v_diet_data(i).rastreio(k).diagnosticos.LAST
                                    LOOP
                                        if v_text_diag is null then v_text_diag := 'Diagn�sticos'; end if;
                                        v_text_diag := v_text_diag || chr(10) || v_diet_data(i).rastreio(k).diagnosticos(l).descr_diag || '   (' || v_diet_data(i).rastreio(k).diagnosticos(l).user_cri || ', ' ||  v_diet_data(i).rastreio(k).diagnosticos(l).dt_cri || ')';
                                    END LOOP;
                                END IF;

                                v_text := v_text || chr(10) || v_text_interv || chr(10)||  v_text_diag;

                                v_rec := create_base_rec (i_t_doente, i_doente, v_diet_data(i).rastreio(k).t_episodio, v_diet_data(i).rastreio(k).episodio, NULL, v_diet_data(i).rastreio(k).USER_CRI, get_n_mecan(v_diet_data(i).rastreio(k).USER_CRI), '', v_diet_data(i).rastreio(k).dt_cri);
                                v_rec.id_bd := TO_CHAR (v_diet_data(i).rastreio(k).dt_cri, 'dd-mm-yyyy');
                                v_rec.id_resumo := 'DIET#' || TO_CHAR (v_diet_data(i).rastreio(k).dt_cri, 'dd-mm-yyyy');
                                v_rec.digest := NULL;
                                v_rec.texto_completo := v_text;
								v_text := '';
                                v_rec.dt_registo_display := TO_CHAR (v_diet_data(i).rastreio(k).dt_cri, 'dd-mm-yyyy');
                                v_rec.has_more_than_digest := 'S';
                                v_diet (v_diet.COUNT) := v_rec;

                            END LOOP;
                        END IF;

                        v_text_interv := null;
                        v_text_diag := null;

                        --escalas crianca
                        --VERIFICAR SE TEM ALGUMA ESCALA PEDIATRICA
                        IF v_diet_data(i).rastreio_ped.COUNT > 0 THEN
                            FOR k IN v_diet_data(i).rastreio_ped.FIRST .. v_diet_data(i).rastreio_ped.LAST
                            LOOP
							
								--JMS-244523 adriano.alves
                                IF i_show_resume_header = 'S' then
                                    v_header := SDF_URG_OBTEM_PESS_HOSP_NOME(v_diet_data(i).rastreio_ped(k).USER_CRI) || ', '|| TO_CHAR(v_diet_data(i).rastreio_ped(k).DT_CRI,'DD-MM-YYYY HH24:MI:SS');
                                ELSE
                                    v_header := '';
                                END IF;

                                v_text := v_text || chr(10) || chr(10) || 'Escala StrongKids ('||v_header || ' ['||traduz ('score total')||': '|| v_diet_data(i).rastreio_ped(k).SCORE ||'] )'; -- JMS-244521 : Incluir informa��o 'USER_CRI' , 'DT_CRI' e 'SCORE'. 
                                v_text := v_text || chr(10) || 'Existe alguma patologia subjacente que contribua para o risco de desnutri��o (ver lista*) ou � esperada alguma cirurgia maior? -> ' || v_diet_data(i).rastreio_ped(k).op_1;
                                v_text := v_text || chr(10) || 'O doente apresenta um estado nutricional deficit�rio, quando avaliado de uma forma subjetiva? -> ' || v_diet_data(i).rastreio_ped(k).op_2;
                                v_text := v_text || chr(10) || 'Estao presentes alguns dos seguintes itens: Diarreia ('|| unistr('\2265') ||' 5 vezes/dia) e/ou vomitos (>3 vezes/dia). Reducao da ingestao alimentar nos ultimos dias. Intervencao nutricional previa. Ingestao insuficiente devido a dor -> ' || v_diet_data(i).rastreio_ped(k).op_3;
                                v_text := v_text || chr(10) || 'Ocorreu perda de peso ou aus�ncia de ganho de peso (crian�as < 1 ano) durante as �ltimas semanas/meses? -> ' || v_diet_data(i).rastreio_ped(k).op_4;


                                --intervs
                                --INTERVENCOES CRIANCA
                                IF v_diet_data(i).rastreio_ped(k).intervencoes.COUNT > 0 THEN
                                    for l IN v_diet_data(i).rastreio_ped(k).intervencoes.FIRST .. v_diet_data(i).rastreio_ped(k).intervencoes.LAST
                                    LOOP
                                        if v_text_interv is null then v_text_interv := 'Interven��es'; end if;
                                        v_text_interv := v_text_interv || chr(10) || v_diet_data(i).rastreio_ped(k).intervencoes(l).descr_interv || '   (' || v_diet_data(i).rastreio_ped(k).intervencoes(l).user_cri || ', ' ||  v_diet_data(i).rastreio_ped(k).intervencoes(l).dt_cri || ')';
                                    END LOOP;
                                END IF;
                                --diags
                                --DIAGS CRIANCA
                                IF v_diet_data(i).rastreio_ped(k).diagnosticos.COUNT > 0 THEN
                                    for l IN v_diet_data(i).rastreio_ped(k).diagnosticos.FIRST .. v_diet_data(i).rastreio_ped(k).diagnosticos.LAST
                                    LOOP
                                        if v_text_diag is null then v_text_diag := 'Diagn�sticos'; end if;
                                        v_text_diag := v_text_diag || chr(10) || v_diet_data(i).rastreio_ped(k).diagnosticos(l).descr_diag || '   (' || v_diet_data(i).rastreio_ped(k).diagnosticos(l).user_cri || ', ' ||  v_diet_data(i).rastreio_ped(k).diagnosticos(l).dt_cri || ')';
                                    END LOOP;
                                END IF;

                                v_text := v_text || chr(10) || v_text_interv || chr(10) ||  v_text_diag;

                                v_rec := create_base_rec (i_t_doente, i_doente, v_diet_data(i).rastreio_ped(k).t_episodio, v_diet_data(i).rastreio_ped(k).episodio, NULL, v_diet_data(i).rastreio_ped(k).USER_CRI, get_n_mecan(v_diet_data(i).rastreio_ped(k).USER_CRI), '', v_diet_data(i).rastreio_ped(k).dt_cri);
                                v_rec.id_bd := TO_CHAR (v_diet_data(i).rastreio_ped(k).dt_cri, 'dd-mm-yyyy');
                                v_rec.id_resumo := 'DIET#' || TO_CHAR (v_diet_data(i).rastreio_ped(k).dt_cri, 'dd-mm-yyyy');
                                v_rec.digest := NULL;
                                v_rec.texto_completo := v_text;
								v_text := '';
                                v_rec.dt_registo_display := TO_CHAR (v_diet_data(i).rastreio_ped(k).dt_cri, 'dd-mm-yyyy');
                                v_rec.has_more_than_digest := 'S';
                                v_diet (v_diet.COUNT) := v_rec;

                            END LOOP;
                        END IF;
                    END LOOP;
                END IF;
            END;
        $END

        RETURN v_diet;
    END;
END;
/


/*#########################################
###### SDV_ENF_RESUMO_ENFERMAGEM.sql ######
#########################################*/

  CREATE OR REPLACE FORCE VIEW SDV_ENF_RESUMO_ENFERMAGEM ("T_DOENTE", "DOENTE", "T_EPISODIO", "EPISODIO", "DATA_HORA", "DESIGNACAO", "ESTADO", "INDICADOR", "NOTAS_ENF", "RESUMO", "VALOR", "VALOR_TERAP", "VIA_ADM", "UTILIZADOR", "UTIL", "OBS1", "OBS2", "JUSTIFICACAO", "COD_ITEM", "TIPO_REGISTO", "ORDEM", "FLAG_INFO", "DT_ACT", "DT_CRI","DT_ACT_ENF", "ID_AGRUPADOR", "COD_CHECK", "DT_FIM") AS 
  SELECT   distinct t_doente, doente, t_episodio, episodio,
            TO_DATE (TO_CHAR (dt_item, 'dd-mm-yyyy') || ' ' || hr_item,
                     'dd-mm-yyyy hh24:mi'
                    ) data_hora,
            pckt_enf_plano_cuidados.get_descr_interv (id_agrupador) designacao,
            traduz(DECODE (flg_estado,
                    'S', 'Executado',
                    'N', 'N�o Executado',
                    'Por Executar'
                   ),null,sdf_enf_current_schema || '.SDV_ENF_RESUMO_ENFERMAGEM') estado,
            NULL indicador, NULL notas_enf,
            sdf_enf_resumo_qdr_espec (cod_check) resumo, NULL valor,
            NULL valor_terap, NULL via_adm,
            DECODE (flg_estado,
                    'I', NULL,
                    DECODE (utilizador, NULL, util, utilizador)
                   ) utilizador,
            DECODE (flg_estado, 'I', NULL, util) util, obs1, obs2,
            justificacao, cod_item,
            DECODE (flg_estado, 'I', 'IPE', 'RT') tipo_registo,
            DECODE (flg_estado, 'S', 4, 'N', 5, 6) ordem, 'INTERV' flag_info,
            dt_act, dt_cri,dt_act_enf, id_agrupador, cod_check, dt_fim
       FROM ( SELECT DISTINCT a.t_doente, a.doente, a.t_episodio, a.episodio,
                           a.cod_item, a.tabela,
                           a.flg_estado, a.dt_item, a.hr_item, a.obs obs1,
                           b.observ obs2, desc_justif justificacao,
                           NVL (b.user_act, b.user_cri) util,
                           NVL (b.dt_act, b.dt_cri) DATA, 'RT' tipo_registo,
                           a.cod_check,
                           /*NVL (a.user_act, a.user_cri)*/ --| SCMP-6372 adriano.alves --| passa a trazer o n_mecan_enf_resp
                           a.n_mecan_enf_resp utilizador,
                           a.dt_act dt_act, a.dt_cri dt_cri,a.dt_act_enf dt_act_enf, c.id_agrupador,
                           case a.t_episodio
                               when 'Internamentos' THEN (SELECT nvl2(dt_alta,dt_alta+(hr_alta-trunc(hr_alta)),null) FROM sd_ints ints WHERE  a.doente = ints.doente
                                                                                      AND a.t_doente = ints.t_doente
                                                                                      AND a.episodio = ints.n_int)
                               ELSE null
                           END dt_fim
                      FROM sd_enf_check_list a,
                           sd_enf_check_indic b,
                           sd_enf_plano_cuid_interv c
                           --sd_ints ints
                     WHERE hr_item LIKE '__:%'
                       AND LENGTH (hr_item) = 5
                       AND a.cod_check = b.cod_check(+)
                       AND a.t_doente = c.t_doente
                       AND a.doente = c.doente
                       AND a.t_episodio = c.t_episodio
                       AND a.episodio = c.episodio
                       --AND a.doente = ints.doente
                       --AND a.t_doente = ints.t_doente
                       --AND a.episodio = ints.n_int
                       AND c.cod_interv = a.cod_item
/* IPO-P-6742
                       AND TO_DATE (TO_CHAR (dt_item, 'yyyymmdd') || ' ' || hr_item,'yyyymmdd hh24:mi') >= c.dt_ini
                       AND (TO_DATE ( TO_CHAR (dt_item, 'yyyymmdd') || ' ' || hr_item,'yyyymmdd hh24:mi') <= c.dt_fim or c.dt_fim is null)
*/
                       AND (a.flg_estado != 'I' or (a.flg_estado = 'I' and to_number(substr(hr_item, 0, 2)) in (select hora_adm from ph_freq_pres_hora where freq_pres = c.cod_freq)))
                       and nvl(a.id_agrupador,
                           pckt_enf_plano_cuidados.obtem_id_agrupador(a.t_doente,a.doente,a.t_episodio,a.episodio,a.cod_item,a.tabela,
                                                              TO_DATE (TO_CHAR (dt_item, 'yyyymmdd') || ' ' || hr_item,'yyyymmdd hh24:mi'),
                                                              TO_DATE (TO_CHAR (dt_item, 'yyyymmdd') || ' ' || hr_item,'yyyymmdd hh24:mi') + interval '1' hour))
                            = c.id_agrupador)
   UNION ALL
   --Notas de Evolucao
   SELECT DISTINCT t_doente, doente, t_episodio, episodio, DATA data_hora,
                   NULL designacao, NULL estado, NULL indicador, notas_enf,
                   NULL resumo, NULL valor, NULL valor_terap, NULL via_adm,
                   NVL (a.user_act, utilizador) utilizador, NULL util,
                   NULL obs1, NULL obs2, NULL justificacao, NULL cod_item,
                   'NE' tipo_registo, 3 ordem, 'NOTAS' flag_info,
                   b.dt_act dt_act, b.dt_cri dt_cri,null dt_act_enf, null, b.id_notas_det, NULL dt_fim
              FROM sd_enf_notas a, sd_enf_notas_det b
             WHERE a.id_notas = b.id_notas
   UNION ALL
   SELECT DISTINCT t_doente, doente,
                   (SELECT t_episodio
                      FROM ph_prescricoes c
                     WHERE c.prescricao = a.prescricao) t_episodio,
                   (SELECT episodio
                      FROM ph_prescricoes c
                     WHERE c.prescricao = a.prescricao) episodio,
                   hora_adm data_hora,
                   DECODE(a.grupo,
                                 null , NVL(a.nome_med_adm, b.nome_cient)
                                      , b.nome_cient || '-' || a.nome_med_adm) designacao,
                   traduz('Administrado',null,sdf_enf_current_schema || '.SDV_ENF_RESUMO_ENFERMAGEM') estado, NULL indicador, NULL notas_enf,
                   NULL resumo, NULL valor,
                    decode(a.qt_adm,null,'--',decode(b.dose_med,null,'--',rtrim(to_char(a.dose,'FM99999999990.999999'),'.') ||' '||a.unid_med))
                   || DECODE (a.tipo_md,
                              NULL, NULL,
                              ' [' || a.tipo_md || ']'
                             ) valor_terap,
                   via_adm, a.enf utilizador,
                   NULL util, a.obs_adm obs1, (select obs_med from ph_med_pres x where a.id_act = x.id_act and a.prescricao = x.prescricao) obs2, null justificacao,
                   a.medicamento cod_item, 'TA' tipo_registo, 1 ordem,
                   'TERAP' flag_info, a.dt_act dt_act, a.dt_cri dt_cri,null dt_act_enf, null, a.ADMINISTRACAO,
                   NULL dt_fim
              FROM ph_med_adm a, ph_medicamentos b
             WHERE a.medicamento = b.medicamento(+)
   UNION ALL
   -- terapeutica nao administrada
   SELECT DISTINCT t_doente, doente,
                   (SELECT t_episodio
                      FROM ph_prescricoes c
                     WHERE c.prescricao = a.prescricao) t_episodio,
                   (SELECT episodio
                      FROM ph_prescricoes c
                     WHERE c.prescricao = a.prescricao) episodio,
                   hora data_hora,
                   DECODE(a.grupo,
                                 null , NVL(a.nome_med_adm, b.nome_cient)
                                      , b.nome_cient || '-' || a.nome_med_adm) designacao,
                   traduz('N�o Administrado',null,sdf_enf_current_schema || '.SDV_ENF_RESUMO_ENFERMAGEM') estado, NULL indicador, NULL notas_enf,
                   NULL resumo, NULL valor,
                   DECODE (dose,
                           NULL, NULL,
                           dose || '' || a.unid_med
                          ) valor_terap,
                   via_adm, NVL (a.user_act, a.user_cri) utilizador,
                   NULL util, NULL obs1, (select obs_med from ph_med_pres x where a.id_act = x.id_act and a.prescricao = x.prescricao) obs2, desc_justif justificacao,
                   a.medicamento cod_item, 'TNA' tipo_registo, 2 ordem,
                   'TERAP' flag_info, a.dt_act dt_act, a.dt_cri dt_cri,null dt_act_enf, null,a.administracao,
                   NULL dt_fim
              FROM ph_med_n_adm a, ph_medicamentos b
             WHERE a.medicamento = b.medicamento(+)
   ORDER BY        data_hora DESC, tipo_registo, designacao
/
