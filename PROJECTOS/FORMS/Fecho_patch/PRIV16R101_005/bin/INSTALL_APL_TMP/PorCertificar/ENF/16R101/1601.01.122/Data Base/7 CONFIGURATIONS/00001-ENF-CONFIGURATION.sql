/*#########################
###### CONFIGURATION ######
#########################*/
DECLARE
  w_count NUMBER;
BEGIN
  SELECT count(0)
    INTO w_count
    FROM ga_patch_instalacao
   WHERE product = 'Enfermagem'
     AND product_code = 'e'
     AND release_year = 2016
     AND release_number = 1
     AND minor_release = 1
     AND patch = 122
     AND build = 1;
  IF w_count = 0 THEN
    INSERT INTO ga_patch_instalacao (product, product_code, release_date, release_year, release_number, minor_release, patch, build, who_installed)
         VALUES ('Enfermagem', 
                 'e', 
                 TO_DATE ('17032020', 'ddmmyyyy'), 
                 2016, 
                 1, 
                 1, 
                 122, 
                 1,
                 nvl(null/*a preencher por quem instala*/,userenv('TERMINAL')));
    COMMIT;
  END IF;
END;
/

