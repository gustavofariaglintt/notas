

/*#########################################
###### 0001-ENF-ALTER-dt_act_enf.sql ######
#########################################*/
--| Adds a column 
BEGIN
    GA_INSTALL_OBJECTS.add_alter_column
    (
        p_table_owner         => user,
        p_table_name         => 'SD_ENF_CHECK_LIST',
        p_column_name        => 'DT_ACT_ENF',
        p_column_type        => 'DATE',
        p_column_length      => 7,
        p_column_precision   => NULL,
        p_column_scale       => NULL,
        p_nullable           => TRUE,
        p_default_value      => NULL
    );
END;
/
