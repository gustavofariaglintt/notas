
@ECHO OFF

call .\variaveis_upgrade.bat

ECHO.
ECHO.
ECHO.
ECHO.
ECHO.
ECHO.
ECHO Copia de sinonimos e grants entre bases de dados 
ECHO.
ECHO Garantir que as variaveis estao bem configuradas (variaveis_upgrade.bat):
ECHO.
ECHO      (Base de dados origem) 
ECHO      CONN_STRING_BTEC_DEV = %CONN_STRING_BTEC_DEV%
ECHO      (Base de dados destino) 
ECHO      CONN_STRING_SYS = %CONN_STRING_SYS%
ECHO.
ECHO Se nao estiverem, ainda vais a tempo de corrigir : (altera o ficheiro variaveis_upgrade.bat)
ECHO Se o ficheiro variaveis_upgrade.bat j� estiver OK, carrega numa tecla para continuar.
ECHO.
ECHO.
ECHO.
ECHO.
ECHO.
ECHO.

pause

cls

call .\variaveis_upgrade.bat

ECHO.
ECHO.
ECHO.
ECHO.
ECHO.
ECHO.
ECHO Copia de sinonimos e grants entre bases de dados 
ECHO.
ECHO Valida��o final:
ECHO.
ECHO      (Base de dados origem) 
ECHO      CONN_STRING_BTEC_DEV = %CONN_STRING_BTEC_DEV%
ECHO      (Base de dados destino) 
ECHO      CONN_STRING_SYS = %CONN_STRING_SYS%
ECHO.
ECHO Se estiver OK, carrega numa tecla para continuar.
ECHO Se estiver mal, fecha a janela.
ECHO.
ECHO.
ECHO.
ECHO.
ECHO.
ECHO.

pause

@ECHO ON


sqlplus %CONN_STRING_BTEC_DEV% @.\auxiliares_execucao\SIN_GRANT_qual_para_prod\pasta_auxiliar_usada_bat\000_imprime_sinonimos.sql

sqlplus %CONN_STRING_SYS% @.\auxiliares_execucao\SIN_GRANT_qual_para_prod\pasta_auxiliar_usada_bat\001_carrega_sinonimos_prod.sql 

