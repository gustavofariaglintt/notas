PROMPT ====================================================
PROMPT      <<< ENF >>>
PROMPT ====================================================
set define off
spool on
exec DBMS_OUTPUT.ENABLE();
set sqlblanklines on
exec pck_log_alt_bd.begin_version;
PROMPT STARTING FILE 00001-ENF-OBJECTS.sql...
spool %SCRIPTS_PATH%\ENF\16R101\122\4CODE\ENF\00001-ENF-OBJECTS.sql.log
@@%SCRIPTS_PATH%\ENF\16R101\122\4CODE\ENF\00001-ENF-OBJECTS.sql
exec pck_log_alt_bd.end_version('16R1.01.122.01.e');
spool off
--pause END_RUNNING_FILE 00001-ENF-OBJECTS.sql - Press enter to continue...
exit


