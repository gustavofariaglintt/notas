/*#########################
###### CONFIGURATION ######
#########################*/
DECLARE
  w_count NUMBER;
BEGIN
  SELECT count(0)
    INTO w_count
    FROM ga_patch_instalacao
   WHERE product = 'Enfermagem'
     AND product_code = 'e'
     AND release_year = 2015
     AND release_number = 1
     AND minor_release = 1
     AND patch = 149
     AND build = 1;
  IF w_count = 0 THEN
    INSERT INTO ga_patch_instalacao (product, product_code, release_date, release_year, release_number, minor_release, patch, build, who_installed)
         VALUES ('Enfermagem', 
                 'e', 
                 TO_DATE ('23042018', 'ddmmyyyy'), 
                 2015, 
                 1, 
                 1, 
                 149, 
                 1,
                 nvl(null/*a preencher por quem instala*/,userenv('TERMINAL')));
    COMMIT;
  END IF;
END;
/

