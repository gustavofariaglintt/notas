PROMPT ====================================================
PROMPT      <<< ENF >>>
PROMPT ====================================================
set define off
set echo on
set serveroutput on
spool on
exec DBMS_OUTPUT.ENABLE();
set sqlblanklines on
exec pck_log_alt_bd.begin_version;
PROMPT STARTING FILE 00001-ENF-CONFIGURATION.sql...
spool %SCRIPTS_PATH%\ENF\15R101\149\7CONFIGURATIONS\ENF\00001-ENF-CONFIGURATION.sql.log
@@%SCRIPTS_PATH%\ENF\15R101\149\7CONFIGURATIONS\ENF\00001-ENF-CONFIGURATION.sql
exec pck_log_alt_bd.end_version('15R1.01.149.01.e');
spool off
--pause END_RUNNING_FILE 00001-ENF-CONFIGURATION.sql - Press enter to continue...
set echo off
set serveroutput off
exit


