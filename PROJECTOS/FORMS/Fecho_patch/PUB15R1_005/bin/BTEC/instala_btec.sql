

-- ***********************************************                              
-- instalar (correr com o sys)                                                  
-- ***********************************************                              
CREATE OR REPLACE                                                               
FUNCTION sys.get_ddl (i_tipo varchar2, i_owner varchar2, i_nome varchar2) return
clob is                                                                         
begin                                                                           
return dbms_metadata.get_ddl(i_tipo,i_nome,i_owner);                            
end;                                                                            
/                                                                               
grant select on dba_jobs to GH;                                                 
grant select on dba_jobs to BTEC;                                               
grant select on dba_scheduler_jobs to btec;                                     
grant select on v_$session to BTEC;                                             
grant create any trigger to BTEC;                                               
grant select on gv_$session to btec;                                            
grant ADMINISTER DATABASE TRIGGER to btec;                                      
grant alter system to btec;                                                     
grant execute on dbms_stats to btec;                                            
grant select on v_$lock to btec;                                                
grant select on v_$locked_object to btec;                                       
grant select on dba_objects to btec;                                            
grant select on gv_$lock to btec;                                               
grant create any table to btec;                                                 
grant all on gh.sd_serv to btec;                                                
grant all on gh.sD_episodio to btec;                                            
grant all on gh.sD_agenda_def to btec;                                          
grant all on gh.sd_cons_marc to btec;                                           
grant all on gh.pck_controla_triggers to btec;                                  
grant all on gh.sd_agenda_calend to btec;                                       
grant all on gh.gh_calend_fecha to btec;                                        
grant all on gh.gh_calend_agenda to btec;                                       
grant all on gh.gh_remarca_consulta to btec;                                    
grant all on gh.sd_arq_sessao_segmento to btec;                                 
grant all on gh.pck_ghce1060 to btec;                                           
grant all on gh.sd_int_servs to btec;                                           
grant all on gh.sd_episod_acomp to btec;                                        
grant all on gh.gr_seq_arvore_camas to btec;                                    
grant all on gh.sd_cama to btec;                                                
grant all on gh.gr_despo_camas to btec;                                         
grant all on gh.ghv_arvore_camas to btec;                                       
grant all on dba_mviews to btec;                                                
grant all on gh_fa.context_package to btec;                                     
grant all on dba_tab_columns to btec;                                           
grant all on dba_mview_logs to btec;                                            
grant all on dba_tables to btec;                                                
grant all on dba_triggers to btec;                                              
grant all on dba_indexes to btec;                                               
grant all on dba_constraints to btec;                                           
grant all on gh_fa.fa_rubr to btec;                                             
grant all on gh_fa.fa_grup_rubr to btec;                                        
grant all on dba_ind_columns to btec;                                           
grant all on gh.gr_param_base to btec;                                          
grant all on gh_fa.fa_paramet to btec;                                          
grant all on gh.gr_param_base to btec;                                          
grant all on gh.gr_param_base to btec;                                          
grant alter any table to btec;                                                  
grant drop any table to btec;                                                   
grant execute on utl_recomp to btec;                                            
grant drop any index to btec;                                                   
grant alter any trigger to btec;                                                
grant create any index to btec;                                                 
grant select on dba_tab_cols to btec;                                           
grant alter any index to btec;                                                  
grant select any table to btec;                                                 
grant update any table to btec;                                                 
grant delete any table to btec;                                                 
grant insert any table to btec;                                                 
grant select on dba_sequences to btec;                                          
grant select on DBA_TAB_PRIVS to btec;                                          
grant select on dba_lobs to btec;                                               
grant alter any sequence to btec;                                               
grant select any sequence to btec;                                              
grant all on gh.sd_cons_pend_intf to btec;                                      
grant all on gh.sd_cons_pend_intf_diags to btec;                                
grant all on gh.sd_dadosutente_rnu to btec;                                     
grant all on gh.sd_cons_pend_intf_dadosutente to btec;                          
grant all on gh.sd_cons_pend_intf_docs to btec;                                 
grant select on sys.dba_ts_quotas to btec;                                      
grant select on sys.dba_users to btec;                                          
grant select on sys.dba_segments to btec;                                       
grant select on sys.DBA_RECYCLEBIN to btec;                                     
grant execute on sys.dbms_metadata to btec;                                     
grant select on sys.dba_sys_privs to btec;                                      
grant all on gh.devolve_empresa to btec;                                        
grant all on gh.pk_param_base_manutencao to btec;                               
grant select on dba_synonyms to btec;                                           
grant create any synonym to btec;                                               
grant select on v_$datafile to btec;                                            
grant select on v_$tempfile to btec;                                            
grant select on v_$logfile to btec;                                             
grant select on gv_$process to btec;                                            
grant select on v_$process to btec;                                             
grant select on dba_views to btec;                                              
grant create table to btec;                                                     
grant select on dba_cons_columns to btec;                                       
grant all on ga.GA_PCK_UTILIZADOR_APL to btec;                                  
grant all on ga.ga_pck_autenticacao to btec;                                    
grant execute on sys.get_ddl to btec;                                           
grant all on gh.sd_doente to btec;                                              
grant drop any synonym to btec;                                                 
grant select on sys.dba_source to btec;                                         
grant select on sys.DBA_TABLESPACES to btec;                                    
grant select on sys.user$ to btec;                                              
grant execute on dbms_scheduler to btec;                                        
grant select on v_$parameter to btec;                                           
grant select on v_$version to btec;                                             
grant execute on DBMS_NETWORK_ACL_ADMIN to btec;                                
grant select_catalog_role to btec;                                              
grant all on dba_network_acl_privileges to btec;                                
grant MANAGE SCHEDULER to btec;                                                 
grant alter user to btec;                                                       
grant drop public synonym to btec;                                              
GRANT DROP ANY PROCEDURE TO BTEC;                                               
GRANT DROP ANY INDEXTYPE TO BTEC;                                               
GRANT DROP ANY MATERIALIZED VIEW TO BTEC;                                       
GRANT DROP ANY SEQUENCE TO BTEC;                                                
GRANT DROP ANY TRIGGER TO BTEC;                                                 
GRANT DROP ANY TYPE TO BTEC;                                                    
GRANT DROP ANY VIEW TO BTEC;                                                    
GRANT DROP PUBLIC DATABASE LINK TO BTEC;                                        
GRANT GRANT ANY OBJECT PRIVILEGE TO BTEC;                                       
GRANT CREATE DATABASE LINK TO BTEC;                                             
GRANT ALL ON SYS.DBA_DB_LINKS TO BTEC;                                          
grant create any job to btec;                                                   
GRANT EXEMPT ACCESS POLICY TO BTEC;                                             
grant all on plh.ph_prescricoes TO BTEC;                                        
grant all on gh.sd_pess_hosp_def TO BTEC;                                       
grant all on gh.gr_cg_ref_codes TO BTEC;                                        
grant all on gh_pc.pc_area_priv_medico TO BTEC;                                 
grant all on enf.sd_enf_notas_enf TO BTEC;                                      
grant all on enf.sd_enf_tr_prioridade TO BTEC;                                  
grant all on gh.sd_ints TO BTEC;                                                
grant all on CPC_BD.ur_alta TO BTEC;                                            
grant all on gh.gh_fact_log TO BTEC;                                            
grant all on gh.sd_cons_marc TO BTEC;                                           
grant all on gh.sd_episodio TO BTEC;                                            
grant all on gh.gr_empr_inst TO BTEC;                                           
create synonym btec.gr_empr_inst for gh.gr_empr_inst;                           
create synonym btec.devolve_empresa for gh.devolve_empresa;                     
grant all on gh.devolve_empresa to btec;                                        
grant all on btec.BTECP_EXECUTE_IMMEDIATE to PH;                                
grant all on btec.BTECP_EXECUTE_IMMEDIATE to PLH;                               
grant all on btec.BTECP_EXECUTE_IMMEDIATE to LG;                                
grant all on btec.BTECP_EXECUTE_IMMEDIATE to PH_LG;                             
grant all on btec.BTECP_EXECUTE_IMMEDIATE to LG_SAUDE;                          
grant all on btec.BTECP_EXECUTE_IMMEDIATE to DIET;                              
grant all on sys.DBA_DATA_FILES to BTEC;                                        
grant all on sys.DBA_FREE_SPACE to BTEC;                                        
truncate table btec.bt_userpwd;                                                 
truncate table btec.bt_utilpwd;                                                 
truncate table btec.BT_SEGS_PREUPG;                                             
truncate table btec.BT_TBS_PREUPG;                                              
-- ***********************************************                              
-- Limpa tabelas Backup (XX)                                                    
-- ***********************************************                              
drop table btec.XXUNIF_PARAM_TABS_DIAS;                                         
drop table btec.XX_BCK_LE_UTILIZADOR;                                           
drop table btec.XX_BT_CLIENTES;                                                 
drop table btec.XX_BT_PROGRESSO_UPGRADE;                                        
drop table btec.XX_BT_PROGRESSO_UPGRADE_DET;                                    
drop table btec.XX_BT_UPG_ACCOES_20130715;                                      
drop table btec.XX_SERNADAS_BT_UPG_ACCOES;                                      
drop table btec.XX_TESTE;                                                       
drop table btec.XX_WORK_PLANNER_APAGD_MALO;                                     
exec btec.pk_upgrade_control.upg_limpa_registos_bd;                             
commit;                                                                         
INSERT INTO BTEC.BT_UPG_ACCOES_TIPO (ID_ACCAO,TIPO_UPGRADE,USER_CRI,DT_CRI,USER_ACT,DT_ACT)
VALUES(5000,'PREREL','BTEC',TO_DATE('2016-10-20 17:11:30', 'YYYY-MM-DD HH24:MI:SS'),NULL,NULL);
/
CREATE OR REPLACE 
PACKAGE btec.pk_upgrade_control

IS
/*
    vupg_fase_est_inicial  varchar2(2); -- := 'I';
    vupg_fase_est_ignorado varchar2(2); -- := 'IG';
    vupg_fase_est_erro     varchar2(2); -- := 'E';
    vupg_fase_est_em_curso varchar2(2); -- := 'EC';
    vupg_fase_est_final    varchar2(2); -- := 'F';
*/

    TYPE r_upg_accao IS RECORD
    (
        id                             NUMBER ,
        descr_accao                    VARCHAR2(100 CHAR),
        descr_accao_det                VARCHAR2(400 CHAR),
        flg_obrigatorio                VARCHAR2(1 CHAR),
        tipo_accao                     VARCHAR2(20 CHAR),
        codigo_accao                   VARCHAR2(2000 CHAR),
        n_ordem                        NUMBER,
        flg_fase                       VARCHAR2(10 CHAR),
        id_accao_relacionada           NUMBER,
        flg_corre_em_background        VARCHAR2(1 CHAR)
    );

    TYPE t_upg_accao IS TABLE OF r_upg_accao INDEX BY BINARY_INTEGER;


    TYPE r_upg_produto IS RECORD
    (
        id                             NUMBER ,
        nome                           VARCHAR2(30 CHAR)
    );

    TYPE t_upg_produto IS TABLE OF r_upg_produto INDEX BY BINARY_INTEGER;





    function obtem_cliente_id return number ; --> 74
    FUNCTION obtem_empresa_id RETURN varchar2 ; --> EMP1






    -- *******************************************************************************************************
    -- FUN��ES GERAIS PARA CONTROLO DE UPGRADE

    /*
    declare
        vid number;
    begin

    vid := pk_upgrade_control.upg_inicio (
            'ADMIN',
            74
            );

    end;
    */

    function  upg_get_sequencia return bt_progresso_upgrade.id%type;

    function upg_inicio (
        vuser_upg              bt_progresso_upgrade.user_upg%type,
        vid_cliente            bt_progresso_upgrade.id_cliente%type,
        vtipo_upgrade          bt_progresso_upgrade.tipo_upgrade%type

    ) return bt_progresso_upgrade.id%type;


    FUNCTION verifica_upgrade_iniciado RETURN VARCHAR2;

    FUNCTION get_id_upgrade_aberto RETURN bt_progresso_upgrade.id%type;

    procedure upg_fim (vid_upgrade bt_progresso_upgrade.id%type);

    -- ex: exec pk_upgrade_control.upg_limpa_registos_bd;
    procedure upg_limpa_registos_bd;
    -- *******************************************************************************************************








    -- *******************************************************************************************************
    -- FUN��ES GERAIS PARA CONTROLO DE FASES UPGRADE
    --function  upg_fase_cria (vid_upgrade_origem NUMBER, vid_tarefa NUMBER) return bt_progresso_upgrade_det.id%type;
    function upg_fase_cria (
        vid_upgrade_origem              NUMBER,
        vid_tarefa                      NUMBER,
        vid_produto                     number,
        vFICH_UPG_NOME                  varchar2
    ) return bt_progresso_upgrade_det.id%type;

    function upg_fase_cria_fich_upg (
        vid_upgrade_origem              NUMBER,
        vid_produto                     number,
        vFICH_UPG_NOME                  varchar2
    ) return bt_progresso_upgrade_det.id%type;

    function upg_fase_cria_fase_nrepet (
        vid_upgrade_origem              NUMBER,
        vid_tarefa_gen                  NUMBER,
        vid_produto                     number,
        vFICH_UPG_NOME                  varchar2
    ) return bt_progresso_upgrade_det.id%type;

    procedure upg_fase_cria_fase_compilacao (lista_in in Pck_Types.external_cursor);

    procedure upg_fase_inicio (vid_fase bt_progresso_upgrade_det.id%type);
    function  upg_fase_get_sequencia return bt_progresso_upgrade_det.id%type;
    FUNCTION  upg_fase_get_proxima (vid_upgrade bt_progresso_upgrade.id%type) RETURN bt_progresso_upgrade_det.id%type;
    function upg_fase_valida_existencia (
        vid_upgrade_origem              NUMBER,
        vid_tarefa                      NUMBER,
        vid_produto                     number,
        vFICH_UPG_NOME                  varchar2
    ) return bt_progresso_upgrade_det.id%type;

    procedure upg_fase_set_info_aux (vid_fase number, vinfo_auxiliar VARCHAR2);
    procedure upg_fase_set_dt_ini (vid_fase number, vdt_ini date);
    procedure upg_fase_set_dt_fim (vid_fase number, vdt_fim date);
    procedure upg_fase_set_erro (vid_fase number, voutput VARCHAR2);
    procedure upg_fase_set_estado (vid_fase number, vflg_estado VARCHAR2);
    procedure upg_fase_set_output (vid_fase number, voutput VARCHAR2);

    function  upg_fase_get_info_auxiliar (vid_fase  bt_progresso_upgrade_det.id%type) return bt_progresso_upgrade_det.info_auxiliar%type;
    function upg_fase_get_id_accao (vid_fase  bt_progresso_upgrade_det.id%type) return bt_progresso_upgrade_det.id_accao%type;
    procedure upg_fase_get_lista_accoes (
        vid_upgrade bt_progresso_upgrade.id%type,
        vflg_fase bt_upg_accoes.flg_fase%type,
        vtipo_upgrade bt_upg_accoes_tipo.tipo_upgrade%type, lista_out out Pck_Types.external_cursor);

    function get_fase_id_fase_relacionada (
        vid_fase                        number
    ) return bt_progresso_upgrade_det.id%type;

    procedure upg_fase_get_lista_accoes (
        vid_upgrade bt_progresso_upgrade.id%type,
        vid_produto bt_progresso_upgrade_det.id_produto%type,
        vflg_fase bt_upg_accoes.flg_fase%type,
        vtipo_upgrade bt_upg_accoes_tipo.tipo_upgrade%type, lista_out out Pck_Types.external_cursor);

     procedure upg_fase_ponto_situacao (
        vid_upgrade bt_progresso_upgrade.id%type,
        vid_produto bt_progresso_upgrade_det.id_produto%type,
        vflg_fase bt_upg_accoes.flg_fase%type,
        lista_out out Pck_Types.external_cursor);

    function  upg_fase_est_inicial   return bt_progresso_upgrade_det.flg_estado%type deterministic;
    function  upg_fase_est_ignorado  return bt_progresso_upgrade_det.flg_estado%type deterministic;
    function  upg_fase_est_erro      return bt_progresso_upgrade_det.flg_estado%type deterministic;
    function  upg_fase_est_em_curso  return bt_progresso_upgrade_det.flg_estado%type deterministic;
    function  upg_fase_est_final     return bt_progresso_upgrade_det.flg_estado%type deterministic;

    procedure upg_fase_ignorado (vid_fase bt_progresso_upgrade_det.id%type);
    procedure upg_fase_fim (vid_fase bt_progresso_upgrade_det.id%type) ;
    procedure upg_fase_ini (vid_fase bt_progresso_upgrade_det.id%type) ;

    procedure upg_fase_executa_sql (vid_fase  bt_progresso_upgrade_det.id%type, FLG_ESTADO in out bt_progresso_upgrade_det.FLG_ESTADO%type, output in out bt_progresso_upgrade_det.output%type) ;

    -- *******************************************************************************************************







    -- *******************************************************************************************************
    -- FUN��ES GERAIS PARA CONTROLO DE AC��ES

    /*
    -- ex:

    declare
        vid number;
    begin

    vid := pk_upgrade_control.upg_insere_accao (
            'Desactivar Jobs',
            'S�o parados todos os jobs na BD' ,
            'S',
            'SQL',
            'btec.pk_util.desactivar_todos_jobs (''S'', ''N'')',
            1200,
            'PRE_UPG',
            null,
            'GERAL',
            null
            );

    end;

    */

    function upg_insere_accao (
        vdescr_accao                    bt_upg_accoes.descr_accao%type,
        vdescr_accao_det                bt_upg_accoes.descr_accao_det%type,
        vflg_obrigatorio                bt_upg_accoes.flg_obrigatorio%type,
        vtipo_accao                     bt_upg_accoes.tipo_accao%type,
        vcodigo_accao                   bt_upg_accoes.codigo_accao%type,
        vn_ordem                        bt_upg_accoes.n_ordem%type,
        vflg_fase                       bt_upg_accoes.flg_fase%type,
        vid_accao_relacionada           bt_upg_accoes.id_accao_relacionada%type,
        vtipo_upgrade                   bt_upg_accoes_tipo.tipo_upgrade%type,
        vflg_corre_em_background        bt_upg_accoes.flg_corre_em_background%type

    ) return bt_upg_accoes.id%type;

    function upg_tarefa_get_id_fich_upg return bt_upg_accoes.id%type deterministic;
    function upg_tarefa_valid_se_e_final (vid_tarefa bt_upg_accoes.id%type) return varchar2 deterministic;
    function upg_tarefa_get_flg_fase ( vid_tarefa bt_upg_accoes.id%type ) return bt_upg_accoes.flg_fase%type;

    function upg_accao_get_lista (vflg_fase bt_upg_accoes.flg_fase%type, vtipo_upgrade bt_upg_accoes_tipo.tipo_upgrade%type ) return t_upg_accao;
    -- *******************************************************************************************************









    -- *******************************************************************************************************
    -- FUN��ES GERAIS PARA CONTROLO DE PRODUTOS
    procedure upg_produto_get_lista (lista_out out Pck_Types.external_cursor);
    -- *******************************************************************************************************





    -- *******************************************************************************************************
    -- FUN��ES GERAIS PARA CONTROLO DE CLIENTES
    procedure upg_cliente_set_cliente (vid_cliente bt_param_base.valor%type);
    procedure upg_cliente_get_lista (lista_out out Pck_Types.external_cursor);
    -- *******************************************************************************************************







    -- *******************************************************************************************************
    -- FUN��ES GERAIS PARA CONTROLO DE OBJECTOS DESCOMPILADOS
    procedure upg_descomp_get_lista_o (vid_upgrade bt_progresso_upgrade.id%type, lista_out out Pck_Types.external_cursor) ;
    procedure upg_descomp_get_lista_t (vid_upgrade bt_progresso_upgrade.id%type, vowner varchar2, lista_out out Pck_Types.external_cursor) ;
    procedure upg_descomp_get_lista_n (vid_upgrade bt_progresso_upgrade.id%type, vowner varchar2, vobject_type varchar2, lista_out out Pck_Types.external_cursor) ;
    procedure upg_descomp_get_info_obj (vowner varchar2, vobject_name varchar2, lista_out out Pck_Types.external_cursor);
    procedure upg_descomp_get_ref_obj (vowner varchar2, vobject_name varchar2, lista_out out Pck_Types.external_cursor);
    -- *******************************************************************************************************







END pk_upgrade_control;
/

CREATE OR REPLACE 
PACKAGE BODY btec.pk_upgrade_control
IS







    function obtem_cliente_id return number is
    begin
        return pk_conf.obtem_cliente_id;
    end;

    FUNCTION obtem_empresa_id RETURN varchar2 is
    begin
        return pk_conf.obtem_empresa_id;
    end;











    -- *******************************************************************************************************
    -- FUN��ES GERAIS PARA CONTROLO DE UPGRADE
    function  upg_get_sequencia return bt_progresso_upgrade.id%type
    IS
        vid   bt_progresso_upgrade.id%type;
    BEGIN

        SELECT   seq_progresso_upgrade.NEXTVAL INTO vid FROM DUAL;

        return vid;
    end;




    function upg_inicio (
        vuser_upg              bt_progresso_upgrade.user_upg%type,
        vid_cliente            bt_progresso_upgrade.id_cliente%type,
        vtipo_upgrade          bt_progresso_upgrade.tipo_upgrade%type

    ) return bt_progresso_upgrade.id%type
    IS
        vid   NUMBER;
    BEGIN

        --pk_bd.faz_log_accao ('upg_inicio1 '||vuser_upg||'-'||vid_cliente, null);

        vid := upg_get_sequencia;
        --pk_bd.faz_log_accao ('upg_inicio2', null);


        insert into bt_progresso_upgrade
        (
        id, user_upg, dt_ini, id_cliente, tipo_upgrade
        )
        values
        (
        vid, vuser_upg, sysdate, vid_cliente, vtipo_upgrade
        );


        for c in (
            SELECT id --, descr_accao, descr_accao_det, flg_obrigatorio,
                    --tipo_accao, codigo_accao, n_ordem, flg_fase
            FROM    bt_upg_accoes
            where   flg_fase in ('PRE_UPG', 'POS_UPG')
            and
                (
                    id in (select id_accao from bt_upg_accoes_tipo where tipo_upgrade = vtipo_upgrade)
                    or
                    id in (select id_accao from bt_upg_accoes_cliente where tipo_upgrade = vtipo_upgrade and id_cliente = vid_cliente)
                )
        ) loop
            declare
                vid_fase bt_progresso_upgrade_det.id%type;
            begin
                vid_fase := pk_upgrade_control.upg_fase_cria (vid, c.id, null, null);
                pk_upgrade_control.upg_fase_set_estado (vid_fase, pk_upgrade_control.upg_fase_est_inicial);
                pk_upgrade_control.upg_fase_set_dt_ini (vid_fase, sysdate);
            end;
        end loop;



        --pk_bd.faz_log_accao ('upg_inicio3 '||vid, null);

        return vid;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_inicio: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_inicio: '||sqlerrm);
        raise value_error;
    end;



    FUNCTION verifica_upgrade_iniciado
        RETURN VARCHAR2
    IS
        conta   NUMBER;
        aux varchar2(10);
    BEGIN
        SELECT   COUNT ( * )
          INTO   conta
          FROM   bt_progresso_upgrade
         WHERE   dt_fim IS NULL;

        IF conta > 0
        THEN
            RETURN 'S';
        END IF;

        RETURN 'N';

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.verifica_upgrade_iniciado: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.verifica_upgrade_iniciado: '||sqlerrm);
        raise value_error;
    end;



    FUNCTION get_id_upgrade_aberto
        RETURN bt_progresso_upgrade.id%type
    IS
        vid   bt_progresso_upgrade.id%type;
    BEGIN
        SELECT   id
          INTO   vid
          FROM   bt_progresso_upgrade
         WHERE   dt_fim IS NULL
         --and     rownum = 1
         ;

        RETURN vid;
    exception
    when no_data_found then
        return -1;
    when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.get_id_upgrade_aberto: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.get_id_upgrade_aberto: '||sqlerrm);
        raise value_error;
    END;



    procedure upg_fim (vid_upgrade bt_progresso_upgrade.id%type)
    IS
    BEGIN

        update bt_progresso_upgrade
        set dt_fim = sysdate
        where id = vid_upgrade;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fim: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fim: '||sqlerrm);
        raise value_error;
    end;


    procedure upg_limpa_registos_bd is
    begin
        delete from bt_progresso_upgrade_det;
        delete from bt_progresso_upgrade;
        -- acrescentado Bert�o
        update bt_param_base set valor = NULL where chave ='BT_CLIENTE_INST';

    end;
    -- *******************************************************************************************************











    -- *******************************************************************************************************
    -- FUN��ES GERAIS PARA CONTROLO DE FASES UPGRADE

    FUNCTION upg_fase_get_proxima (vid_upgrade bt_progresso_upgrade.id%type)
        RETURN bt_progresso_upgrade_det.id%type
    IS
        vid   bt_progresso_upgrade_det.id%type;
    BEGIN
        for c in (
            select  t1.id
            from    bt_progresso_upgrade_det t1, bt_upg_accoes t2
            where   1=1
            and     t1.id_accao = t2.id
            and     id_upgrade_origem = vid_upgrade
            and     flg_estado not in (
                    pk_upgrade_control.upg_fase_est_em_curso,
                    pk_upgrade_control.upg_fase_est_ignorado,
                    pk_upgrade_control.upg_fase_est_final
                    )
            order by n_ordem
            ) loop
            vid := c.id;
            exit;
        end loop;

        return vid;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_get_proxima: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_get_proxima: '||sqlerrm);
        raise value_error;
    end;


    function  upg_fase_get_sequencia return bt_progresso_upgrade_det.id%type
    IS
        vid   bt_progresso_upgrade_det.id%type;
    BEGIN

        SELECT   seq_progresso_upgrade_det.NEXTVAL INTO vid FROM DUAL;

        return vid;
    end;


    function upg_fase_cria (
        vid_upgrade_origem              NUMBER,
        vid_tarefa                      NUMBER,
        vid_produto                     number,
        vFICH_UPG_NOME                  varchar2
    ) return bt_progresso_upgrade_det.id%type
    IS
        vid   NUMBER;
    BEGIN
        /*pk_bd.faz_log_accao ('Inicio pk_ugrade_control.upg_fase_cria: '
        ||'-'||vid_upgrade_origem
        ||'-'||vid_tarefa
        ||'-'||vid_produto
        ||'-'||vFICH_UPG_NOME
        , null);*/

        vid := upg_fase_get_sequencia;

        insert into bt_progresso_upgrade_det
        (
        id, id_accao, id_upgrade_origem, id_produto, FICH_UPG_NOME
        )
        values
        (
        vid, vid_tarefa, vid_upgrade_origem, vid_produto, vFICH_UPG_NOME
        );

        return vid;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_cria: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_cria: '||sqlerrm);
        raise value_error;
    end;


    function upg_fase_valida_existencia (
        vid_upgrade_origem              NUMBER,
        vid_tarefa                      NUMBER,
        vid_produto                     number,
        vFICH_UPG_NOME                  varchar2
    ) return bt_progresso_upgrade_det.id%type is

        vres bt_progresso_upgrade_det.id%type := '-1';
    begin

        select  id
        into    vres
        from    bt_progresso_upgrade_det
        where   id_upgrade_origem = vid_upgrade_origem
        and     id_accao = vid_tarefa
        and     (vid_produto is null    or id_produto = vid_produto)
        and     (vfich_upg_nome is null or fich_upg_nome = vfich_upg_nome)
        ;

        return vres ;

    exception when others then
        return vres ;
    end;




    function upg_fase_cria_fich_upg (
        vid_upgrade_origem              NUMBER,
        vid_produto                     number,
        vFICH_UPG_NOME                  varchar2
    ) return bt_progresso_upgrade_det.id%type
    is
        vid_tarefa_fich_upg bt_progresso_upgrade_det.id_accao%type := pk_upgrade_control.upg_tarefa_get_id_fich_upg;
        vid_fase            bt_progresso_upgrade_det.id%type;

    begin

        vid_fase := upg_fase_cria_fase_nrepet ( vid_upgrade_origem, vid_tarefa_fich_upg, vid_produto, vFICH_UPG_NOME);

        return vid_fase;

    end;




    function upg_fase_cria_fase_nrepet (
        vid_upgrade_origem              NUMBER,
        vid_tarefa_gen                  NUMBER,
        vid_produto                     number,
        vFICH_UPG_NOME                  varchar2
    ) return bt_progresso_upgrade_det.id%type
    is
        vid_fase            bt_progresso_upgrade_det.id%type;
        vid_fase_encontrado bt_progresso_upgrade_det.id%type;

    begin

        vid_fase_encontrado := pk_upgrade_control.upg_fase_valida_existencia (vid_upgrade_origem, vid_tarefa_gen, vid_produto, vFICH_UPG_NOME);

        if vid_fase_encontrado = -1 then
            vid_fase := pk_upgrade_control.upg_fase_cria (vid_upgrade_origem, vid_tarefa_gen, vid_produto, vFICH_UPG_NOME);
            pk_upgrade_control.upg_fase_set_estado (vid_fase, pk_upgrade_control.upg_fase_est_inicial);
            pk_upgrade_control.upg_fase_set_dt_ini (vid_fase, sysdate);
        else
            vid_fase := vid_fase_encontrado;
        end if;

        return vid_fase;

    end;



    --aaa
    procedure upg_fase_cria_fase_compilacao (lista_in in Pck_Types.external_cursor) is
    begin
/*         open lista_out
         for
            SELECT id, nome
            FROM bt_produtos;
*/
        null;
    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_cria_fase_compilacao: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_cria_fase_compilacao: '||sqlerrm);
        raise value_error;
    end;








    procedure upg_fase_inicio (vid_fase bt_progresso_upgrade_det.id%type)
    IS
    BEGIN

        upg_fase_set_dt_ini (vid_fase, sysdate);
        upg_fase_set_estado (vid_fase, pk_upgrade_control.upg_fase_est_inicial);
        upg_fase_set_dt_fim (vid_fase, null);


    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_inicio: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_inicio: '||sqlerrm);
        raise value_error;
    end;


    procedure upg_fase_set_dt_ini (vid_fase number, vdt_ini date) is
    BEGIN

        update bt_progresso_upgrade_det
        set dt_ini = vdt_ini
        where id = vid_fase;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_set_dt_ini: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_set_dt_ini: '||sqlerrm);
        raise value_error;
    end;

    procedure upg_fase_set_dt_fim (vid_fase number, vdt_fim date) is
    BEGIN

        update bt_progresso_upgrade_det
        set dt_fim = vdt_fim
        where id = vid_fase;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_set_dt_fim: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_set_dt_fim: '||sqlerrm);
        raise value_error;
    end;


    procedure upg_fase_set_info_aux (
        vid_fase                        number,
        vinfo_auxiliar                  VARCHAR2
    )
    IS
    BEGIN

        update bt_progresso_upgrade_det
        set info_auxiliar = vinfo_auxiliar
        where id = vid_fase;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_set_info_aux: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_set_info_aux: '||sqlerrm);
        raise value_error;
    end;


    procedure upg_fase_set_erro (vid_fase number, voutput VARCHAR2)
    IS
    BEGIN

        update bt_progresso_upgrade_det
        set output = substr(voutput, 1, 2000), flg_estado = pk_upgrade_control.upg_fase_est_erro
        where id = vid_fase;

        commit;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_set_erro: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_set_erro: '||sqlerrm);
        raise value_error;
    end;

    procedure upg_fase_set_estado (vid_fase number, vflg_estado VARCHAR2)
    IS
    BEGIN

        update bt_progresso_upgrade_det
        set flg_estado = vflg_estado
        where id = vid_fase;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_set_estado: vflg_estado = '||vflg_estado||' erro = '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_set_estado: vflg_estado = '||vflg_estado||' erro = '||sqlerrm);
        raise value_error;
    end;

    procedure upg_fase_set_output (vid_fase number, voutput VARCHAR2)
    IS
    BEGIN

        update bt_progresso_upgrade_det
        set output = voutput
        where id = vid_fase;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_set_output: erro = '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_set_output: erro = '||sqlerrm);
        raise value_error;
    end;


    function upg_fase_get_info_auxiliar (vid_fase  bt_progresso_upgrade_det.id%type)
    return bt_progresso_upgrade_det.info_auxiliar%type
    IS
        vinfo_auxiliar bt_progresso_upgrade_det.info_auxiliar%type;
    BEGIN

        select  info_auxiliar
        into    vinfo_auxiliar
        from    bt_progresso_upgrade_det
        where   id = vid_fase;

        return vinfo_auxiliar;

    exception
    when no_data_found then return null;
    when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_get_info_auxiliar: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_get_info_auxiliar: '||sqlerrm);
        raise value_error;
    end;



    function upg_fase_get_id_accao (vid_fase  bt_progresso_upgrade_det.id%type)
    return bt_progresso_upgrade_det.id_accao%type
    IS
        vid_accao bt_progresso_upgrade_det.id_accao%type;
    BEGIN

        select  id_accao
        into    vid_accao
        from    bt_progresso_upgrade_det
        where   id = vid_fase;

        return vid_accao;

    exception
    when no_data_found then return null;
    when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_get_id_accao: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_get_id_accao: '||sqlerrm);
        raise value_error;
    end;



    function get_fase_id_fase_relacionada (
        vid_fase                        number
    ) return bt_progresso_upgrade_det.id%type
    IS
        vid_accao               bt_progresso_upgrade_det.id_accao%type;
        vid_upgrade_origem      bt_progresso_upgrade_det.id_upgrade_origem%type;
        vid_accao_relacionada   bt_upg_accoes.id%type;
        vid                     bt_progresso_upgrade_det.id%type;
    BEGIN


        select  id_accao, id_upgrade_origem
        into    vid_accao, vid_upgrade_origem
        from    bt_progresso_upgrade_det
        where   id = vid_fase;

        select  id_accao_relacionada
        into    vid_accao_relacionada
        from    bt_upg_accoes
        where   id = vid_accao;


        select  id
        into    vid
        from    bt_progresso_upgrade_det
        where   id_accao = vid_accao_relacionada
        and     id_upgrade_origem = vid_upgrade_origem;

        return vid;

    exception when others then
        --pk_bd.faz_log_accao ('Erro pk_ugrade_control.get_fase_id_fase_relacionada: '||sqlerrm, null);
        --dbms_output.put_line('Erro pk_ugrade_control.get_fase_id_fase_relacionada: '||sqlerrm);
        return null;
    end;





    function upg_fase_est_inicial return bt_progresso_upgrade_det.flg_estado%type deterministic is
    begin
        return 'I';
    end;
    function upg_fase_est_ignorado return bt_progresso_upgrade_det.flg_estado%type deterministic is
    begin
        return 'IG';
    end;
    function upg_fase_est_erro return bt_progresso_upgrade_det.flg_estado%type deterministic is
    begin
        return 'E';
    end;
    function upg_fase_est_em_curso return bt_progresso_upgrade_det.flg_estado%type deterministic is
    begin
        return 'EC';
    end;
    function upg_fase_est_final return bt_progresso_upgrade_det.flg_estado%type deterministic is
    begin
        return 'F';
    end;


    procedure upg_fase_ignorado (vid_fase bt_progresso_upgrade_det.id%type)
    IS
    BEGIN

        --pk_bd.faz_log_accao ('pk_ugrade_control.upg_fase_ignorado passo1 ', null);

        update bt_progresso_upgrade_det
        set flg_estado = pk_upgrade_control.upg_fase_est_ignorado, dt_fim = sysdate
        where id = vid_fase;

        --pk_bd.faz_log_accao ('pk_ugrade_control.upg_fase_ignorado passo2 '||sql%rowcount, null);

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_ignorado: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_ignorado: '||sqlerrm);
        raise value_error;
    end;


    procedure upg_fase_fim (vid_fase bt_progresso_upgrade_det.id%type)
    IS
    BEGIN

        update bt_progresso_upgrade_det
        set flg_estado = pk_upgrade_control.upg_fase_est_final, dt_fim = sysdate
        where id = vid_fase;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_fim: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_fim: '||sqlerrm);
        raise value_error;
    end;

    procedure upg_fase_ini (vid_fase bt_progresso_upgrade_det.id%type)
    IS
    BEGIN

        update bt_progresso_upgrade_det
        set flg_estado = pk_upgrade_control.upg_fase_est_inicial, dt_ini = sysdate, dt_fim = null
        where id = vid_fase;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_ini: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_ini: '||sqlerrm);
        raise value_error;
    end;

    procedure upg_fase_get_lista_accoes (
        vid_upgrade bt_progresso_upgrade.id%type,
        vflg_fase bt_upg_accoes.flg_fase%type,
        vtipo_upgrade bt_upg_accoes_tipo.tipo_upgrade%type, lista_out out Pck_Types.external_cursor) is
    begin
        upg_fase_get_lista_accoes (vid_upgrade, null, vflg_fase, vtipo_upgrade, lista_out);

    end;

    procedure upg_fase_get_lista_accoes (
        vid_upgrade bt_progresso_upgrade.id%type,
        vid_produto bt_progresso_upgrade_det.id_produto%type,
        vflg_fase bt_upg_accoes.flg_fase%type,
        vtipo_upgrade bt_upg_accoes_tipo.tipo_upgrade%type, lista_out out Pck_Types.external_cursor) is
    begin
        /*pk_bd.faz_log_accao ('Inicio pk_ugrade_control.upg_fase_get_lista_accoes: '
        ||'-'||vid_upgrade
        ||'-'||vid_produto
        ||'-'||vflg_fase
        ||'-'||vtipo_upgrade
        , null);*/


         open lista_out
         for
            select id, descr_accao, descr_accao_det, flg_obrigatorio,
                   tipo_accao, codigo_accao, n_ordem, flg_fase,
                   id_accao_relacionada, flg_estado, output,
                   dt_ini, dt_fim, user_terminal, id_fase,
                   id_produto, fich_upg_nome, flg_corre_em_background
            from
            (

                SELECT x0.id, x0.descr_accao, x0.descr_accao_det, x0.flg_obrigatorio,
                       x0.tipo_accao, x0.codigo_accao, x0.n_ordem, x0.flg_fase,
                       x0.id_accao_relacionada, x2.flg_estado, x2.output,
                       x2.dt_ini, x2.dt_fim, x2.user_terminal, x2.id id_fase,
                       x2.id_produto, x2.fich_upg_nome, x0.flg_corre_em_background
                  FROM bt_upg_accoes x0, bt_progresso_upgrade x1, bt_progresso_upgrade_det x2
                  where 1=1
                  and   (vflg_fase is null or flg_fase = vflg_fase)
                  and
                  (
                  x0.id in (select id_accao from bt_upg_accoes_tipo where tipo_upgrade = vtipo_upgrade)
                  --All actions for client and tipo_upg
                  or
                  x0.id in (select id_accao from bt_upg_accoes_cliente cli where cli.tipo_upgrade = vtipo_upgrade and cli.id_cliente =  x1.id_cliente)
                  )
                  and   (vid_produto is null or id_produto = vid_produto)
                  and   x1.id = x2.id_upgrade_origem
                  and   x1.id = vid_upgrade
                  and   x0.id = x2.id_accao
            )
            order by n_ordem,fich_upg_nome;
            null;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_get_lista_accoes: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_get_lista_accoes: '||sqlerrm);
        raise value_error;
    end;

    procedure upg_fase_ponto_situacao (
        vid_upgrade bt_progresso_upgrade.id%type,
        vid_produto bt_progresso_upgrade_det.id_produto%type,
        vflg_fase bt_upg_accoes.flg_fase%type,
        lista_out out Pck_Types.external_cursor) is
    begin
        /*pk_bd.faz_log_accao ('Inicio pk_ugrade_control.upg_fase_ponto_situacao: '
        ||'-'||vid_upgrade
        ||'-'||vid_produto
        ||'-'||vflg_fase
        , null);*/


         open lista_out
         for
            SELECT  x1.id id_upgrade, id_produto,
                    sum(decode(x2.flg_estado, 'F', 1, 'IG', 1, 0)) tarefas_realizadas ,
                    sum(1) tarefas_total
            FROM bt_upg_accoes x0, bt_progresso_upgrade x1, bt_progresso_upgrade_det x2
            where 1=1
            and   (vflg_fase is null or flg_fase = vflg_fase)
            and   (vid_produto is null or id_produto = vid_produto)
            and   x1.id = x2.id_upgrade_origem
            and   x1.id = vid_upgrade
            and   x0.id = x2.id_accao
            group by x1.id , x2.id_produto
            ;

            null;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_ponto_situacao: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_fase_ponto_situacao: '||sqlerrm);
        raise value_error;
    end;


    procedure upg_fase_executa_sql (
        vid_fase    bt_progresso_upgrade_det.id%type,
        FLG_ESTADO  in out bt_progresso_upgrade_det.FLG_ESTADO%type,
        output        in out bt_progresso_upgrade_det.output%type) is

        instrucao               bt_upg_accoes.codigo_accao%type;
        vtipo_accao             bt_upg_accoes.tipo_accao%type;
        vID_ACCAO_RELACIONADA   bt_upg_accoes.ID_ACCAO_RELACIONADA%type;
        msg_aviso   varchar2(2000);
        msg_erro    varchar2(2000);
    begin

        --pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_executa_sql1', null);

        select  codigo_accao, tipo_accao, ID_ACCAO_RELACIONADA
        into    instrucao, vtipo_accao, vID_ACCAO_RELACIONADA
        from    bt_upg_accoes
        where   id = (
            select  id_accao
            from    bt_progresso_upgrade_det
            where   id = vid_fase);

        if (vtipo_accao != 'SQL') then
            pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_executa_sql: '||'N�o � uma ac��o SQL!', null);
            raise value_error;
        end if;

        instrucao := 'declare vnumber_dummy number; begin vnumber_dummy := :vid_fase; :msg_aviso := null; :msg_erro := null; ' || instrucao || ' end;';

        --pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_executa_sql2 '||vid_fase, null);

        pk_upgrade_control.upg_fase_set_estado (vid_fase, pk_upgrade_control.upg_fase_est_em_curso);

        --pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_executa_sql3 '||instrucao, null);

        execute immediate instrucao using in vid_fase, in out msg_aviso, in out msg_erro;

        --pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_executa_sql4 ', null);

        if msg_erro is not null then
            pk_bd.faz_log_accao (msg_erro, null);
            pk_upgrade_control.upg_fase_set_erro (vid_fase, msg_erro);
            FLG_ESTADO  := pk_upgrade_control.upg_fase_est_erro;
            output        := msg_erro;
            return;
        end if;

        pk_upgrade_control.upg_fase_fim (vid_fase);
        pk_upgrade_control.upg_fase_set_output (vid_fase, null);

        --pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_executa_sql5 ', null);

        FLG_ESTADO  := 'F';
        output        := null;

        --pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_executa_sql6 ', null);


    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_fase_executa_sql: '||sqlerrm, null);
        pk_upgrade_control.upg_fase_set_erro (vid_fase, 'Erro pk_ugrade_control.upg_fase_executa_sql: '||sqlerrm);
        FLG_ESTADO  := pk_upgrade_control.upg_fase_est_erro;
        output        := 'Erro pk_ugrade_control.upg_fase_executa_sql: '||sqlerrm;

        raise value_error;
    end;
    -- *******************************************************************************************************
















    -- *******************************************************************************************************
    -- FUN��ES GERAIS PARA CONTROLO DE AC��ES

    function  upg_insere_accao (
        vdescr_accao                    bt_upg_accoes.descr_accao%type,
        vdescr_accao_det                bt_upg_accoes.descr_accao_det%type,
        vflg_obrigatorio                bt_upg_accoes.flg_obrigatorio%type,
        vtipo_accao                     bt_upg_accoes.tipo_accao%type,
        vcodigo_accao                   bt_upg_accoes.codigo_accao%type,
        vn_ordem                        bt_upg_accoes.n_ordem%type,
        vflg_fase                       bt_upg_accoes.flg_fase%type,
        vid_accao_relacionada           bt_upg_accoes.id_accao_relacionada%type,
        vtipo_upgrade                   bt_upg_accoes_tipo.tipo_upgrade%type,
        vflg_corre_em_background        bt_upg_accoes.flg_corre_em_background%type
    ) return bt_upg_accoes.id%type
    IS
        vid number;

    BEGIN
        SELECT   seq_upg_accoes.NEXTVAL INTO vid FROM DUAL;

        insert into bt_upg_accoes
        (
        id, descr_accao, descr_accao_det,
        flg_obrigatorio, tipo_accao, codigo_accao,
        n_ordem, flg_fase, id_accao_relacionada,
        flg_corre_em_background
        )
        values
        (
        vid, vdescr_accao, vdescr_accao_det,
        vflg_obrigatorio, vtipo_accao, vcodigo_accao,
        vn_ordem, vflg_fase, vid_accao_relacionada,
        vflg_corre_em_background
        );

        insert into bt_upg_accoes_tipo (id_accao, tipo_upgrade) values (vid, vtipo_upgrade);

        return vid;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_insere_accao: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_insere_accao: '||sqlerrm);
        raise value_error;
    end;


    function upg_tarefa_get_flg_fase ( vid_tarefa bt_upg_accoes.id%type ) return bt_upg_accoes.flg_fase%type is
        vflg_fase bt_upg_accoes.flg_fase%type;
    begin
        select  flg_fase
        into    vflg_fase
        from    bt_upg_accoes
        where   id = vid_tarefa;

        return vflg_fase;
    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_tarefa_get_flg_fase: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_tarefa_get_flg_fase: '||sqlerrm);
        raise value_error;
    end;


    function upg_tarefa_get_id_fich_upg return bt_upg_accoes.id%type deterministic is
    begin
        return '5000';
    end;


    function upg_tarefa_valid_se_e_final (vid_tarefa bt_upg_accoes.id%type) return varchar2 deterministic is
    begin

        if vid_tarefa >= 9500 then return 'S';
            return 'S';
        end if;
        return 'N';
    end;




    function upg_accao_get_lista
    (
    vflg_fase bt_upg_accoes.flg_fase%type, vtipo_upgrade bt_upg_accoes_tipo.tipo_upgrade%type

    )
    return t_upg_accao
    IS
        item_numero   NUMBER;
        vlista_a_processar t_upg_accao;

    BEGIn

        for c in (SELECT id, descr_accao, descr_accao_det, flg_obrigatorio,
                       tipo_accao, codigo_accao, n_ordem, flg_fase,
                       id_accao_relacionada, flg_corre_em_background
                  FROM bt_upg_accoes
                  where (vflg_fase is null or flg_fase = vflg_fase)
                  and id in (select id_accao from bt_upg_accoes_tipo where tipo_upgrade = vtipo_upgrade)
                  ) loop

            item_numero := vlista_a_processar.COUNT + 1;

            vlista_a_processar (item_numero).id := c.id;
            vlista_a_processar (item_numero).descr_accao := c.descr_accao;
            vlista_a_processar (item_numero).descr_accao_det := c.descr_accao_det;
            vlista_a_processar (item_numero).flg_obrigatorio := c.flg_obrigatorio;
            vlista_a_processar (item_numero).tipo_accao := c.tipo_accao;
            vlista_a_processar (item_numero).codigo_accao := c.codigo_accao;
            vlista_a_processar (item_numero).n_ordem := c.n_ordem;
            vlista_a_processar (item_numero).flg_fase := c.flg_fase;
            vlista_a_processar (item_numero).id_accao_relacionada := c.id_accao_relacionada;
            vlista_a_processar (item_numero).flg_corre_em_background := c.flg_corre_em_background;

        end loop;

        return vlista_a_processar;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_accao_get_lista: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_accao_get_lista: '||sqlerrm);
        raise value_error;
    end;

    -- *******************************************************************************************************

















    -- *******************************************************************************************************
    -- FUN��ES GERAIS PARA CONTROLO DE PRODUTOS

    procedure upg_produto_get_lista (lista_out out Pck_Types.external_cursor) is
    begin
         open lista_out
         for
            SELECT id, nome
            FROM bt_produtos;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_produto_get_lista: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_produto_get_lista: '||sqlerrm);
        raise value_error;
    end;

    -- *******************************************************************************************************









    -- *******************************************************************************************************
    -- FUN��ES GERAIS PARA CONTROLO DE CLIENTES


    procedure upg_cliente_set_cliente ( vid_cliente bt_param_base.valor%type ) IS
    BEGIN
         pk_conf.actualiza_param ('BT_CLIENTE_INST', vid_cliente);

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_cliente_set_cliente: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_cliente_set_cliente: '||sqlerrm);
        raise value_error;
    end;

    procedure upg_cliente_get_lista (lista_out out Pck_Types.external_cursor) is
    begin
         open lista_out
         for
            select id, nome
            from bt_clientes;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_cliente_get_lista: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_cliente_get_lista: '||sqlerrm);
        raise value_error;
    end;

    -- *******************************************************************************************************










    -- *******************************************************************************************************
    -- FUN��ES GERAIS PARA CONTROLO DE OBJECTOS DESCOMPILADOS
    procedure upg_descomp_get_lista_o (vid_upgrade bt_progresso_upgrade.id%type, lista_out out Pck_Types.external_cursor) is
    begin
         open lista_out for
            select  owner, count(*) as total
            from    dba_objects
            where   status = 'INVALID'
            group by owner
            order by owner;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_descomp_get_lista_o: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_descomp_get_lista_o: '||sqlerrm);
        raise value_error;
    end;

    procedure upg_descomp_get_lista_t (vid_upgrade bt_progresso_upgrade.id%type, vowner varchar2, lista_out out Pck_Types.external_cursor) is
    begin
         open lista_out for
            select  object_type, count(*) as total
            from    dba_objects
            where   status = 'INVALID'
            and     owner = vowner
            group by object_type
            order by object_type;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_descomp_get_lista_t: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_descomp_get_lista_t: '||sqlerrm);
        raise value_error;
    end;

    procedure upg_descomp_get_lista_n (vid_upgrade bt_progresso_upgrade.id%type, vowner varchar2, vobject_type varchar2, lista_out out Pck_Types.external_cursor) is
    begin
         open lista_out for
            select  object_name, created dt_cri, last_ddl_time dt_act,
            --sernadas -> depois ver isto, pois tenho de relacionar isto com a tabela dos obj_invalid do inicio deste upg
            'S' descompilado_neste_upg
            from    dba_objects
            where   status = 'INVALID'
            and     owner = vowner
            and     object_type = vobject_type
            order by object_name;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_descomp_get_lista_n: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_descomp_get_lista_n: '||sqlerrm);
        raise value_error;
    end;

    procedure upg_descomp_get_info_obj (vowner varchar2, vobject_name varchar2, lista_out out Pck_Types.external_cursor) is
    begin
         open lista_out for
            select  object_type,  sys.get_ddl(decode(object_type,'PACKAGE BODY', 'PACKAGE', object_type), vowner, vobject_name) object_ddl
            from    dba_objects
            where   owner = vowner
            and     object_name = vobject_name;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_descomp_get_info_obj: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_descomp_get_info_obj: '||sqlerrm);
        raise value_error;
    end;

    -- *******************************************************************************************************


    procedure upg_descomp_get_ref_obj (vowner varchar2, vobject_name varchar2, lista_out out Pck_Types.external_cursor) is
    begin
         open lista_out for
            select 'REFERENCES' as object_type, text as  object_ddl from
            (
                    select 1 as ORD, '--**********************************************' || chr(10) ||
                                     '--************** GRANTS ********************' || chr(10) ||
                                     '--**********************************************' || chr(10)
                                     as TEXT from dual
                union
                    select 2 as ORD, text from
                    (
                        SELECT 'grant '|| PRIVILEGE|| ' on '|| owner|| '.'|| table_name|| ' to '|| grantee ||
                        DECODE (grantable, 'YES', ' WITH GRANT OPTION;', ';') || chr(10) as TEXT
                        FROM dba_tab_privs
                        WHERE owner in(vowner)
                        and table_name like vobject_name
                        order by owner, grantee
                    )
                union
                    select 3 as ORD, '--**********************************************' || chr(10) ||
                                     '--************** END GRANTS **************' || chr(10) ||
                                     '--**********************************************' || chr(10)
                                     as TEXT from dual
                union
                    select 4 as ORD, '--**********************************************' || chr(10) ||
                                     '--**************** SYNONYMS ***************' || chr(10) ||
                                     '--**********************************************' || chr(10)
                                     as TEXT from dual
                union
                    select 5 as ORD, text from
                    (
                        select 'CREATE  OR REPLACE SYNONYM ' ||  OWNER || '.' || SYNONYM_NAME || ' FOR ' || Table_owner ||'.'||SYNONYM_NAME||';' || chr(10) as TEXT
                        from dba_synonyms a
                        WHERE a.table_owner in(vowner)
                        and table_name = vobject_name
                        order by owner
                    )
                union
                    select 6 as ORD, '--**********************************************' || chr(10) ||
                                     '--************ END SYNONYMS ************' || chr(10) ||
                                     '--**********************************************' || chr(10)
                                     as TEXT from dual
            )
            order by ord;

    exception when others then
        pk_bd.faz_log_accao ('Erro pk_ugrade_control.upg_descomp_get_ref_obj: '||sqlerrm, null);
        dbms_output.put_line('Erro pk_ugrade_control.upg_descomp_get_ref_obj: '||sqlerrm);
        raise value_error;
    end;




END pk_upgrade_control;
/

exit;                                    
