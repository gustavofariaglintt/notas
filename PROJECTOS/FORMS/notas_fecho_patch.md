# Notas Fecho de Patch FORMS 19.10 (11/0972019)

## Preparação

1. SVN Checkout dos repos:
   1. <https://svn.glintths.com/svn/NEFRO/NEFRO>
   2. <https://svn.glintths.com/svn/GISS/GISS>
   3. etc.

2. Instalação das ferramentas necessarias: \\\\glintthsdoc\\enf\\LINKS\\UTEIS\\
   1. GTools
   2. Patch Release
   3. Patch Manager (na pasta UTEIS, UTIL_PATCH_MANAGER.bat)

## Patch

1. Abrir patch release
2. Adicionar repo à lista se não estiver:
   1. Copiar o link (ex: <https://svn.glintths.com/svn/GISS/GISS/17R1>)
   2. Carregar no '+'
3. Selecionar na lista acima
4. Carregar em 'fechar um novo patch'
5. Confirmar que há alterações do lado direito da ferramenta (se não houver, não é necessário fechar patch)
6. Mudar campos no ponto '2' para de acordo com a imagem:

    ![Patch Notation][patchnotation]

    {VERSÃO}R{MES}.01
7. Confirmar que pasta de destino aponta para directório local (C:\\Fecho_Patch\\{PROJ})

   (Tendo acesso a esses projectos no GTools, em princípio passa a ser possível copiar directamente para a pasta remota, i.e. \\\\10.250.39.117\\PreReleases\\ForValidation\\)
8. Carregar em 'Fechar Patch'
9. Abrir gtools:
   1. Abir pasta partilhada e colar as coisas que estavam em C:\\Fecho_Patch
      1. Se for NF colar antes em NEFRO
   2. Check em PS
   3. Validar scripts (1º botão lado direito da linha)
   4. Carregar em deploy (4º botão)
      1. Para fechos de patch da versão 16 o deploy não corre no octopus
   5. Check em VEID
10. Abrir Octopus:
    1. User: HMS
    2. Pass: .Hms,
    3. Confirmar que corre bem
11. Mandar email para Equipa de Qualidade:
    1. SQL Navigator:
       1. DB: WORKPLANNER
       2. Schema: WORKPLANNER
       3. Pass: WORKPLANNER
    2. Query para ver quem vai receber os emails e qual a letra:

        ``` SQL
        SELECT * FROM enf_gera_mail_cfg WHERE LETRA IN ('a', 'e', 'n', 't');
        ```

    3. Para mandar o email:

``` SQL
DECLARE
    i_enviar_email_real   BOOLEAN := FALSE;
BEGIN
    enf_gera_mail (
        i_versoes             => '16R1.01.129.01.jm | GCX1907.01.01.01.jm',
        i_enviar_email_real   => i_enviar_email_real,
        i_texto_manual        => '<p>Os patchs desta semana têm as seguintes dependências: <b>19R1.07.01.01.zs</b>.</p>',
        i_n_dias_atividade    => '15');
END;
/
```

### Notas

1. Anatomia tem de ser compilado à mão (ANAPAT v17)
    1. o que é forms vai estar em publish/application/forms (\\\\10.250.39.117\\PreReleases\\ForValidation\\ANAPAT\\19R1001\\1910.01.01\\Publish\\Application\\Forms\\Fmb6)
    2. o que é plls vai estar em publish/application/system\\6 (\\\\10.250.39.117\\PreReleases\\ForValidation\\ANAPAT\\19R1201\\1912.01.01\\Publish\\Application\\System\\6)
    3. mremote -> demoaplias
    4. lá dentro -> D:\\GLINTTHSIAS\\GLINTTHS\\DEMO\\Compilador\\pll6i\\anapat_demopriv_pll.bat (se houver plls)
    5. Copiar ficheiros do ponto 1 para D:\\GLINTTHSIAS\\GLINTTHS\\DEMO\\Compilador\\fmb6i\\anapat -> .fmb
    6. Navegar até: D:\\GLINTTHSIAS\\GLINTTHS\\DEMO\\Compilador
    7. correr: ANAPAT_DEMOPRIV_11_02_Forms.bat
    8. copiar fmx gerado:
       1. de: D:\\GLINTTHSIAS\\GLINTTHS\\DEMO\\Compilador\\fmb11\\ANAPAT
          1. apagar tudo o que está nessa pasta (logs, fmb e fmx)
       2. para: \\\\demoaplIAS.glinttocm.com\\GLINTTHS\\DEMO\\EXECS
       3. D:\GLINTTHSIAS\GLINTTHS\DEMO \\ DEMO OCM

### REPORTS 16R1 (ENF (PRIV e PUB), ANAPAT (SÓ PRIV), NEFRO e GISS (dunno))

1. mRemote -> máquina Demoaplias
2. D:\\GLINTTHSIAS\\GLINTTHS\16R1\\Compilador\\rep6i{ANAPAT|ENF|GISS|NEFRO}
3. colar *.rfd
   1. \\10.250.39.117\\PreReleases\\ForValidation\\{PRODUTO}\\16R101\\1601.01.{miniVERSÃO}\\Publish\\Application\\Reports\\Rdf\\*.rdf (source)
4. D:\\GLINTTHSIAS\\GLINTTHS\\16R1\\Compilador
   1. Executar ANAPAT_{PRIV|PUB}16R1_11_03_Reports.bat
5. D:\\GLINTTHSIAS\\GLINTTHS\\16R1\\Compilador\\rep11\\{PRODUTO}
   1. copiar *.rep para: \\\\demoaplIAS.glinttocm.com\\GLINTTHS\\16R1\\REP (D:\\GLINTTHSIAS\\GLINTTHS\\16R1 -> atalho "16R1 OCM"\\REP)

[patchnotation]: patchnotation.png "Patch Notation"
