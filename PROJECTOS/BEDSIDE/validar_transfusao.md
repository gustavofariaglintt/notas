# Validar transfusão/unidade de transfusão

1. Abrir BedSide
2. Escolher paciente e copiar transfusionid:

   ![Transfusion ID][transfusionid]
3. SQL Navigator
   1. Schema: GPLATFORM
   2. Pass: GPLATFORM
4. Query:

``` SQL
SELECT tu.label, t.patientlabel, tu.status
  FROM glab_transfusionunit tu, glab_transfusion t
 WHERE     tu.transfusionid = t.transfusionid
       AND tu.transfusionid = '59F8358B-263C-2A91-E053-1501010A5596';
```

[transfusionid]: transfusionid.png "TranfusionID"
