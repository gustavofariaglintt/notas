# CONFIGURAÇÕES IMPRESSORA

+ SCHEMA: PRINTER
+ PASS:   PRINTER

Tabelas:

1. pr_cod_report
2. pr_cod_zone
3. pr_cod_printer
4. pr_cod_workstation
5. pr_cod_printer_zone
6. pr_cod_printer_report
7. pr_cod_workstation_zone

![Relação entre tabelas][printerschema]

Etiqueta a ser imprimida é definida noutro sítio (APENAS RELEVANTE PARA BEDSIDE TRACKER):

+ SCHEMA: GPLATFORM
+ Tabela: GINF_CONFIG
+ Key:    CONTAINER_LABEL_REPORT_ID

Value deve ser igual ao nome de um dos reports definido em ```pr_cod_report```

## Glintths.GenericReports

O serviço que trata da impressão das etiquetas é o Glintths.GenericReports

Há dois ficheiros que necessitam de ser configurados:

1. GenConfiguration.xml
2. ReportsConfiguration.xml

### GenConfiguration.xml

``` xml
<Companies>
  <Configuration>
        <CompanyName>CHVNG_DEV</CompanyName> <!-- {COMPANY NAME} -->
        <Params>
            <Param>
                <key>server_name</key>
                <value>SVMGHSIIS11</value> <!-- {IIS SERVER} -->
            </Param>
            <Param>
                <key>server_uri</key>
                <value>https://svmghsias02.chvngaia.min-saude.pt/reports/rwwebservice</value> <!-- {IAS URI} -->
            </Param>
            <Param>
                <key>server_version</key>
                <value>IAS11</value>
            </Param>
            <Param>
                <key>desformat</key>
                <value>pdf</value>
            </Param>
            <Param>
                <key>destype</key>
                <value>cache</value>
            </Param>
            <Param>
                <key>paramform</key>
                <value>no</value>
            </Param>
            <Param>
                <key>Copies</key>
                <value>1</value>
            </Param>
            <Param>
                <key>connectionstring</key>
                <value>GH/GH@DGHSDB06</value> <!-- {USER}/{PASS}@{DATABASE} -->
            </Param>
            <Param>
                <key>proxyurl</key>
                <value></value>
            </Param>
            <Param>
                <key>proxyusername</key>
                <value></value>
            </Param>
            <Param>
                <key>proxypassword</key>
                <value></value>
            </Param>
            <Param>
                <key>proxydomain</key>
                <value></value>
            </Param>
        </Params>
    </Configuration>
    <Configuration>
        ...
    </Configuration>
</Companies>
```

### ReportsConfiguration.xml template

``` xml
<Reports>
    <Report>
        <Name>ZB_BEDSIDE_SAMPLE</Name>
        <Title>Etiqueta colheita (BedSide)</Title>
        <ReportType>ZEBRAZPL</ReportType>
        <Params>
        <Param>
            <key>{FRIENDLYNAME}</key>
            <ParamType>REPLACE</ParamType>
            <value>{FRIENDLYNAME}</value>
        </Param>
        <Param>
            <key>{EPISODEDATE}</key>
            <ParamType>REPLACE</ParamType>
            <value>{EPISODEDATE}</value>
        </Param>
        <Param>
            <key>{CONTAINERTYPE}</key>
            <ParamType>REPLACE</ParamType>
            <value>{CONTAINERTYPE}</value>
        </Param>
        <Param>
            <key>{CONTAINERDESCRIPTION}</key>
            <ParamType>REPLACE</ParamType>
            <value>{CONTAINERDESCRIPTION}</value>
        </Param>
        <Param>
            <key>{SPECIMENCODE}</key>
            <ParamType>REPLACE</ParamType>
            <value>{SPECIMENCODE}</value>
        </Param>
        <Param>
            <key>{SPECIMENDESCRIPTION}</key>
            <ParamType>REPLACE</ParamType>
            <value>{SPECIMENDESCRIPTION}</value>
        </Param>
        <Param>
            <key>{LABELCODE}</key>
            <ParamType>REPLACE</ParamType>
            <value>{LABELCODE}</value>
        </Param>
        <Param>
            <key>{ORDER}</key>
            <ParamType>REPLACE</ParamType>
            <value>{ORDER}</value>
        </Param>
        <Param>
            <key>{PRIORITY}</key>
            <ParamType>REPLACE</ParamType>
            <value>{PRIORITY}</value>
        </Param>
        <Param>
            <key>{TYPE}</key>
            <ParamType>REPLACE</ParamType>
            <value>{TYPE}</value>
        </Param>
        <Param>
            <key>{VALUE}</key>
            <ParamType>REPLACE</ParamType>
            <value>{VALUE}</value>
        </Param>
        </Params>
        <PreviewParams>
        </PreviewParams>
    </Report>
    <Report>
        ...
    </Report>
</Reports>
```

[printerschema]: printer.png "Relação entre tabelas"
