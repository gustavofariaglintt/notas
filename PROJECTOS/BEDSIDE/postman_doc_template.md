# REQUESTTITLE

## Backend

### CoreWebApi

TODOController.cs -> **TODO**

### Database

#### Service Discovery

``` sql
SELECT *
  FROM ginf_config
 WHERE key IN ('S_TODO', 'MS_TODO');
```

#### Error Logging

```sql
  SELECT *
    FROM glog_ws
   WHERE     action LIKE '%TODO/TODO%'
         AND response NOT LIKE 'StatusCode = OK%'
         AND dt_request > SYSDATE - INTERVAL '15' MINUTE
ORDER BY 3 DESC;
```

## Frontend

TODOController.cs -> **TODO**

lorem ipsum dolor sit amet