(
SELECT pess.t_pess_hosp, pess.n_mecan, pess.nome, pess.user_sys,
       pessd.cod_serv, s.descr_serv, priv.ID id_privilegio, priv.cod_priv,
       priv.descr_priv,
       NVL ((SELECT valor
               FROM sd_pess_hosp_priv
              WHERE n_mecan = pess.n_mecan
                AND (cod_serv = pessd.cod_serv)
                AND id_privilegio = priv.ID),
            (SELECT DECODE (COUNT (*), 0, 'N', 'S')
               FROM gr_privilegios_assoc pa
              WHERE id_assoc = priv.ID
                AND EXISTS (
                       SELECT 0
                         FROM sd_pess_hosp_priv a
                        WHERE id_privilegio = pa.id_master
                          AND (cod_serv = pessd.cod_serv )
                          AND n_mecan = pessd.n_mecan
                          AND valor = 'S'
                          ))
           ) acesso
  FROM gr_privilegios priv,
       sd_pess_hosp_det pessd,
       sd_pess_hosp_def pess,
       sd_serv s
 WHERE pess.n_mecan = pessd.n_mecan
   AND pessd.cod_serv = s.cod_serv
   AND (pess.t_pess_hosp = priv.t_pess_hosp or priv.t_pess_hosp is null)
   AND NVL (pess.flg_activo, 'S') = 'S'
   AND priv.flag_obrig_serv  = 'S'

)
UNION ALL
(
SELECT pess.t_pess_hosp, pess.n_mecan, pess.nome, pess.user_sys,
       null, null, priv.ID id_privilegio, priv.cod_priv,
       priv.descr_priv,
       NVL ((SELECT valor
               FROM sd_pess_hosp_priv
              WHERE n_mecan = pess.n_mecan
                AND (cod_serv is null)
                AND id_privilegio = priv.ID),
            (SELECT DECODE (COUNT (*), 0, 'N', 'S')
               FROM gr_privilegios_assoc pa
              WHERE id_assoc = priv.ID
                AND EXISTS (
                       SELECT 0
                         FROM sd_pess_hosp_priv
                        WHERE id_privilegio = pa.id_master
                          AND (cod_serv is null)
                          AND n_mecan = pess.n_mecan
                          AND valor = 'S'))
           ) acesso
  FROM gr_privilegios priv,
       sd_pess_hosp_def pess
 WHERE (pess.t_pess_hosp = priv.t_pess_hosp or priv.t_pess_hosp is null)
   AND NVL (pess.flg_activo, 'S') = 'S'
   AND priv.flag_obrig_serv  = 'N'
)