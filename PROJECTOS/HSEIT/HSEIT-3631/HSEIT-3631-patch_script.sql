CREATE TABLE PC_CIRCUITO_CLINICO_ESTADO_NEW
AS (SELECT * FROM PC_CIRCUITO_CLINICO_ESTADO where 1 = 2)
/
begin
ga.ga_dml_vpd.execute_immediate('INSERT INTO PC_CIRCUITO_CLINICO_ESTADO_NEW
SELECT * FROM PC_CIRCUITO_CLINICO_ESTADO');
end;
/
CREATE TABLE PC_CIRCUITO_CLINICO_NEW
AS (SELECT * FROM PC_CIRCUITO_CLINICO where 1 = 2)
/
begin
ga.ga_dml_vpd.execute_immediate('INSERT INTO PC_CIRCUITO_CLINICO_NEW
SELECT * FROM PC_CIRCUITO_CLINICO');
end;
/
DELETE FROM PC_CIRCUITO_CLINICO_ESTADO;
/
DELETE FROM PC_CIRCUITO_CLINICO;
/
BEGIN
  GA_INSTALL_OBJECTS.add_alter_column
  (
      p_table_owner        => 'DOTNET',
      p_table_name         => 'pc_circuito_clinico',
      p_column_name        => 'ID_CIRCUITO',
      p_column_type        => 'NUMBER',
      p_column_length      => 30,
      p_column_precision   => 30,
      p_column_scale       => 0,
      p_nullable           => FALSE,
      p_default_value      => NULL
  );
END;
/
BEGIN
  GA_INSTALL_OBJECTS.add_alter_column
  (
      p_table_owner        => 'DOTNET',
      p_table_name         => 'pc_circuito_clinico_estado',
      p_column_name        => 'ID_CIRCUITO',
      p_column_type        => 'NUMBER',
      p_column_length      => 30,
      p_column_precision   => 30,
      p_column_scale       => 0,
      p_nullable           => FALSE,
      p_default_value      => NULL
  );
END;
/
BEGIN
  GA_INSTALL_OBJECTS.add_alter_column
  (
      p_table_owner        => 'DOTNET',
      p_table_name         => 'pc_circuito_clinico_estado',
      p_column_name        => 'id_estado',
      p_column_type        => 'NUMBER',
      p_column_length      => 30,
      p_column_precision   => 30,
      p_column_scale       => 0,
      p_nullable           => FALSE,
      p_default_value      => NULL
  );
END;
/
ALTER TRIGGER BI_PC_CIRCUITO_CLINICO DISABLE
/
ALTER TRIGGER BU_PC_CIRCUITO_CLINICO DISABLE
/
ALTER TRIGGER PC_CIRCUITO_CLINICO_AI_RESUMO DISABLE
/
BEGIN
GA.GA_DML_VPD.EXECUTE_IMMEDIATE('INSERT INTO DOTNET.PC_CIRCUITO_CLINICO
SELECT * FROM PC_CIRCUITO_CLINICO_NEW');
END;
/
ALTER TRIGGER BI_PC_CIRCUITO_CLINICO ENABLE
/
ALTER TRIGGER BU_PC_CIRCUITO_CLINICO ENABLE
/
ALTER TRIGGER PC_CIRCUITO_CLINICO_AI_RESUMO ENABLE
/
BEGIN
GA.GA_DML_VPD.EXECUTE_IMMEDIATE('INSERT INTO DOTNET.PC_CIRCUITO_CLINICO_ESTADO
SELECT * FROM PC_CIRCUITO_CLINICO_ESTADO_NEW');
END;
/
DROP TABLE PC_CIRCUITO_CLINICO_NEW
/
DROP TABLE PC_CIRCUITO_CLINICO_ESTADO_NEW
/
COMMIT
/