DECLARE
    v_modulo       VARCHAR2 (500);
    v_item         VARCHAR2 (100);
    v_msg          VARCHAR2 (250);
    v_t_episodio   VARCHAR2 (500);
    v_mandatory    VARCHAR2 (200);
BEGIN
    v_modulo := '''OPHTHALMOLOGY''';
    v_item := '''Ophthalmology.ObjectiveExamination''';
    v_msg := '''Exame Objetivo''';
    v_t_episodio := '''Consultas''';
    v_mandatory := '''S''';

    EXECUTE IMMEDIATE
    'INSERT INTO pc_mandatory_fields
        (modulo, item, msg)
     VALUES
        (' || v_modulo || ', ' || v_item || ', ' || v_msg || ')';
        
    EXECUTE IMMEDIATE
    'INSERT INTO pc_mandatory_fields_rules
        (modulo, item, t_episodio, mandatory)
    VALUES
       (' || v_modulo || ', ' || v_item || ', ' || v_t_episodio || ', ' || v_mandatory || ')';

    COMMIT;
END;
/