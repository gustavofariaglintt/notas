# Ophthalmology: Objective Examination as mandatory field

1. Infrastructure
   1. Models
      1. Create the class file representing the table (ex: ClinicalMandatoryFields.cs)
      2. Remember to declare as ```public class```
   2. Interfaces
      1. Create two interfaces for each table: one Repository and one Service (ex: Repository/IClinicalMandatoryFieldsRepository.cs and Services/IClinicalMandatoryFieldsService.cs)
      2. Only relevant fields?
   3. Repositories
      1. Create Repository Class based on the previous Interface (ex.ClinicalMandatoryFieldsRepository.cs)
      2. It will also be convenient (necessary?) to add a WHERE_CLAUSE string to Constants/Db.cs
   4. Dto
      1. Create a model for each table
