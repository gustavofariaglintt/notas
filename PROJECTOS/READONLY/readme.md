# Notas Debug ReadOnly

Para ver no fiddler:

``` xml
<system.net>
    <defaultProxy>
      <proxy proxyaddress="http://127.0.0.1:8888"/>
    </defaultProxy>
   </system.net>
```

Adicionar isto no fim dos ficheiros: (mas dentro do nó ```configuration```)

+ D:\\GIIS\\17R101\\EPR_MOBILE\\Glintths.Shell\\Web.Config
+ D:\\GIIS\\17R101\\GPLATFORM_BASE\\Glintt.GPlatform.APIGateway.CoreWebAPI\\Web.Config
