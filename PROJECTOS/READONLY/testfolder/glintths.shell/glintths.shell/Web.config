﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=152368
  -->
<configuration>
  <configSections>
    <section name="loggingConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging, Version=4.1.0.0, Culture=neutral, PublicKeyToken=null" />
    <section name="exceptionHandling" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration.ExceptionHandlingSettings, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling, Version=4.1.0.0, Culture=neutral, PublicKeyToken=null" />
    <sectionGroup name="system.web.webPages.razor" type="System.Web.WebPages.Razor.Configuration.RazorWebSectionGroup, System.Web.WebPages.Razor, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35">
      <section name="host" type="System.Web.WebPages.Razor.Configuration.HostSection, System.Web.WebPages.Razor, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" />
      <section name="pages" type="System.Web.WebPages.Razor.Configuration.RazorPagesSection, System.Web.WebPages.Razor, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" requirePermission="false" />
    </sectionGroup>
    <sectionGroup name="devExpress">
      <section name="themes" type="DevExpress.Web.ThemesConfigurationSection, DevExpress.Web.v18.2, Version=18.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" requirePermission="false" />
      <section name="compression" type="DevExpress.Web.CompressionConfigurationSection, DevExpress.Web.v18.2, Version=18.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" requirePermission="false" />
      <section name="settings" type="DevExpress.Web.SettingsConfigurationSection, DevExpress.Web.v18.2, Version=18.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" requirePermission="false" />
      <section name="errors" type="DevExpress.Web.ErrorsConfigurationSection, DevExpress.Web.v18.2, Version=18.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" requirePermission="false" />
    </sectionGroup>
    <section name="clientDependency" type="ClientDependency.Core.Config.ClientDependencySection, ClientDependency.Core" requirePermission="false" />
    <section name="bundleConfig" type="Glintths.Web.Common.Bundles.Configuration.BundleConfigurationSection" />
  </configSections>
  <connectionStrings configSource="ConnectionStrings.config" />
  <!-- Exception Handling -->
  <exceptionHandling configSource="ExceptionHandling.config" />
  <!-- Logging -->
  <loggingConfiguration configSource="LoggingConfiguration.config" />
  <appSettings>
    <add key="DefaultKeys" value="">
    </add>
    <!-- Resources -->
    <add key="LangResourcesPath" value="/EPR_MOBILE/Glintths.Shell/Scripts/Localization/" />
    <add key="DefaultUICulture" value="pt-PT" />
    <add key="AcceptedCultures" value="pt-PT" />
    <!-- Logs -->
    <add key="LOG_JSERRORS_TO_FILE" value="true" />
    <add key="ENABLE_LOG_TRAIL" value="true" />
    <!-- Json -->
    <add key="aspnet:MaxJsonDeserializerMembers" value="150000" />
    <!-- Geral -->
    <add key="DefaultUrl" value="http://localhost/EPR_MOBILE/Glintths.Shell" />
    <add key="CompanyDb" value="CPCHS_LOCAL_HSDEV" />
    <add key="ApplicationName" value="ClinicalDesktop" />
    <add key="ApplicationIsReadOnly" value="false" />
    <add key="PerformanceTraceGrouper" value="EPR_MOBILE" />
    <add key="PerformanceTraceIsEnabed" value="true" />
    <add key="webpages:Version" value="3.0.0.0" />
    <add key="PreserveLoginUrl" value="true" />
    <add key="INITCONFIG_GETDBNAME" value="true" />
    <add key="webpages:Enabled" value="false" />
    <add key="ClientValidationEnabled" value="true" />
    <add key="UnobtrusiveJavaScriptEnabled" value="true" />
    <add key="DefaultProvider" value="CPCHS_LOCAL_HSDEV" />
    <add key="DefaultModule" value="NAVIGATION" />
    <add key="FUNCTIONAL_MODE" value="JMS" />
    <add key="SecurityProviderVersion" value="V3" />
    <add key="ExternalAccessAuthMode" value="NONE" />
    <add key="LOGIN_TITLE" value="EPR Mobile" />
    <!-- Reports -->
    <add key="REPORTS_FOLDER" value="D:\Reports\" />
    <add key="REPORTS_URL" value="http://localhost/GlinttHS.Reports/" />
    <!-- AccessControl -->
    <add key="COPYRIGHT" value="© {0} Glintt, for Healthcare Solutions. All rights reserved." />
    <!-- Logotipo -->
    <add key="LOGIN_COMPANY_LOGO_PATH" value="http://localhost/EPR_MOBILE/Images/" />
    <!-- Shell -->
    <add key="ShellBasePath" value="/EPR_MOBILE/" />
    <add key="ReplaceUnderscore" value=",user_sys,utilizador_id,nomedoente,serv,nomemedico,titulomedico,benefdoente,atestadodireito,codcartaoefr," />
    <add key="SUPPORT_LOGIN" value="true" />
    <add key="ALLOW_LOGIN" value="true" />
    <add key="EndpointPrinterServiceWs" value="http://demopriv:9210/Glintths.PrintServer.WebService/PrinterService.svc" />
    <!--CHAVES DE METODOS DE INICIALIZAÇÃO-->
    <add key="INITCONFIG_GETMACHINENAME" value="true" />
    <add key="INITCONFIG_FORCEMACHINENAMEPARAM" value="false" />
    <add key="INITCONFIG_FORCECOMPANYNAME" value="false" />
    <add key="INITCONFIG_GETDBNAME" value="true" />
    <!-- TimeZone -->
    <add key="TimeZoneOffset" value="+1" />
    <add key="IgnoreTimeZone" value="false" />
    <add key="PdfURL" value="http://localhost/EPR_MOBILE/GlinttHS.Shell/Pages/PDFJS/web/viewer.html?file=" />
    <add key="GLOBALCARE" value="TRUE" />
    <add key="NOTIFS_SERVICE" value="http://glintthsiis/GPLATFORM/Glintths.NotificationManager/signalr/hubs" />
    <!-- gPlatform-->
    <add key="GPLATFORM-Application" value="EPR" />
    <add key="GPLATFORM-Version" value="1.0.0" />
    <add key="GPLATFORM-PlatformUrl" value="http://gplatformhost/Glintt.GPlatform.ServiceDiscovery.WebAPI" />
    <add key="shellAccessManagement" value="http://localhost:2020/GA/Glintths.Shell/" />
    <add key="accessManagementEndpoint" value="http://localhost:2020/GA/GlinttAccessManagement.Web/" />
    <add key="AccessManagementBaseUrl" value="http://localhost:2020/GA/GlinttAccessManagement.Web/" />
    <add key="baseUrl" value="http://localhost:2020/" />
    <add key="baseUrlGPlatform" value="http://gplatformhost/" />
    <!-- ativa/desativa o menu interfeatures-->
    <add key="generateInterfeatures" value="false" />
    <add key="ACTIVATE_PATHWAYS" value="true" />
  </appSettings>
  <system.web.webPages.razor>
    <host factoryType="System.Web.Mvc.MvcWebRazorHostFactory, System.Web.Mvc, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
    <pages pageBaseType="System.Web.Mvc.WebViewPage">
      <namespaces>
        <add namespace="System.Web.Mvc" />
        <add namespace="System.Web.Mvc.Ajax" />
        <add namespace="System.Web.Mvc.Html" />
        <add namespace="System.Web.Routing" />
        <add namespace="ClientDependency.Core.Mvc" />
        <add namespace="ClientDependency.Core" />
        <add namespace="Glintths.Web.Common.Extensions" />
      </namespaces>
    </pages>
  </system.web.webPages.razor>
  <system.web>
    <httpRuntime enableKernelOutputCache="false" maxQueryStringLength="32768" maxUrlLength="65536" targetFramework="4.5.2" maxRequestLength="512000" />
    <sessionState mode="StateServer" stateConnectionString="tcpip=localhost:42424" cookieless="false" timeout="2880">
    </sessionState>
    <compilation debug="true" targetFramework="4.0">
      <assemblies>
        <add assembly="System.Web.Abstractions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Helpers, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Routing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Mvc, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.WebPages, Version=2.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
      </assemblies>
    </compilation>
    <httpHandlers>
      <add path="*" verb="*" type="System.Web.HttpNotFoundHandler" />
      <add verb="*" path="DependencyHandler.axd" type="ClientDependency.Core.CompositeFiles.CompositeDependencyHandler, ClientDependency.Core " />
    </httpHandlers>
    <authentication mode="Forms">
      <forms loginUrl="~/Authentication/Login" timeout="2880" protection="All" name="EPRMOBILE" slidingExpiration="true" cookieless="UseCookies" enableCrossAppRedirects="true" />
    </authentication>
    <!--membership defaultProvider="DEFAULT">
      <providers>
        <clear />
        <add name="DEFAULT" defaultProvider="DEFAULT" type="Cpchs.Security.Providers.WSMembershipProvider, Cpchs.Security.Providers" connectionStringName="AccessFileName" enablePasswordRetrieval="false" enablePasswordReset="false" requiresUniqueEmail="false" requiresQuestionAndAnswer="false" minRequiredPasswordLength="1" minRequiredNonalphanumericCharacters="0" applicationName="DEFAULT" companyNames="DEFAULT" moduleNames="SECURITY_HTML" hashAlgorithmType="SHA1" passwordFormat="Hashed" serviceUrl="http://localhost/Cpchs.Security.Providers.WebServices/Membership/WebService/MembershipManagement.svc" serviceUrlAA3="http://localhost/Cpchs.Security.Providers3/Membership/WebService/MembershipManagement.svc" />
      </providers>
    </membership>
    <profile defaultProvider="DEFAULT">
      <providers>
        <clear />
        <add name="DEFAULT" defaultProvider="DEFAULT" type="Cpchs.Security.Providers.WSMembershipProvider, Cpchs.Security.Providers" connectionStringName="AccessFileName" enablePasswordRetrieval="false" enablePasswordReset="false" requiresUniqueEmail="false" requiresQuestionAndAnswer="false" minRequiredPasswordLength="1" minRequiredNonalphanumericCharacters="0" applicationName="DEFAULT" companyNames="DEFAULT" moduleNames="SECURITY_HTML" hashAlgorithmType="SHA1" passwordFormat="Hashed" serviceUrl="http://localhost/Cpchs.Security.Providers.WebServices/Membership/WebService/MembershipManagement.svc" serviceUrlAA3="http://localhost/Cpchs.Security.Providers3/Membership/WebService/MembershipManagement.svc" />
      </providers>
    </profile>
    <roleManager enabled="true" defaultProvider="DEFAULT">
      <providers>
        <clear />
        <add name="DEFAULT" defaultProvider="DEFAULT" type="Cpchs.Security.Providers.WSRoleProvider, Cpchs.Security.Providers" applicationName="DEFAULT" companyNames="DEFAULT" moduleNames="SECURITY_HTML" serviceUrl="http://localhost/Cpchs.Security.Providers.WebServices/Role/WebService/RoleManagement.svc" serviceUrlAA3="http://localhost/Cpchs.Security.Providers3/Role/WebService/RoleManagement.svc" />
      </providers>
    </roleManager-->
    <pages>
      <namespaces>
        <add namespace="System.Web.Helpers" />
        <add namespace="System.Web.Mvc" />
        <add namespace="System.Web.Mvc.Ajax" />
        <add namespace="System.Web.Mvc.Html" />
        <add namespace="System.Web.Routing" />
        <add namespace="System.Web.WebPages" />
      </namespaces>
    </pages>
    <httpModules>
      <!-- ** Need to add the dependency module -->
      <add name="ClientDependencyModule" type="ClientDependency.Core.Module.ClientDependencyModule, ClientDependency.Core" />
      <add type="DevExpress.Web.ASPxHttpHandlerModule, DevExpress.Web.v18.2, Version=18.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" name="ASPxHttpHandlerModule" />
    </httpModules>
    <customErrors mode="Off" />
    <globalization culture="pt-PT" uiCulture="pt-PT" />
  </system.web>
  <system.webServer>
    <validation validateIntegratedModeConfiguration="false" />
    <modules runAllManagedModulesForAllRequests="true">
      <remove name="ClientDependencyModule" />
      <add name="ClientDependencyModule" type="ClientDependency.Core.Module.ClientDependencyModule, ClientDependency.Core" />
      <add type="DevExpress.Web.ASPxHttpHandlerModule, DevExpress.Web.v18.2, Version=18.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" name="ASPxHttpHandlerModule" />
    </modules>
    <handlers>
      <!-- ** Need to add the dependency handler -->
      <remove name="DependencyHandler" />
      <add name="DependencyHandler" preCondition="integratedMode" verb="*" path="DependencyHandler.axd" type="ClientDependency.Core.CompositeFiles.CompositeDependencyHandler, ClientDependency.Core " />
      <add type="DevExpress.Web.ASPxHttpHandlerModule, DevExpress.Web.v18.2, Version=18.2.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" verb="GET,POST" path="DX.ashx" name="ASPxHttpHandlerModule" preCondition="integratedMode" />
    </handlers>
    <security>
      <requestFiltering>
        <requestLimits maxQueryString="56465" maxAllowedContentLength="524288000" />
      </requestFiltering>
    </security>
  </system.webServer>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <probing privatePath="bin/plugins/externalassemblies;bin/plugins/accesscontrol;bin/plugins/GenericUI;bin/plugins/clinicaldesktop;bin/plugins/ClinicalProcess;bin/plugins/Nursing;bin/plugins/GenericUI;bin/plugins/ClinicalDischarge;bin/plugins/Reports;bin/plugins/CTHEditor;bin/plugins/SurgicalProposal;bin/plugins/ExtraInfo;bin/plugins/ClinicalNotes;bin/plugins/PatientData;bin/plugins/ExternalCall;bin/plugins/Documentation;bin/plugins/GenericRecord;bin/plugins/Diagnostics;bin/plugins/AppointmentSchedule;bin/plugins/mcdtregister;bin/plugins/atd;bin/plugins/mcdt;bin/plugins/appointment;bin/plugins/externalassemblies;bin/plugins/accesscontrol;bin/plugins/Eida;bin/plugins/BioVitalSigns;bin/plugins/ClinicalCommon;bin/plugins/PDS;bin/plugins/Notifications;bin/plugins/Locker;bin/plugins/Plan;bin/plugins/MedicalIndications;bin/plugins/InitialEvaluation;bin/plugins/ConsumptionsLog;bin/plugins/Request;bin/plugins/DocViewer;bin/plugins/Surveillances;bin/plugins/GlycemicLog;bin/plugins/WaterBalance;bin/plugins/SurgicalCheck;bin/plugins/Wounds;bin/plugins/Header;bin/plugins/Drains;bin/plugins/ConsultationRequest;bin/plugins/Cytotoxic;bin/plugins/ConsultationRequest;bin/plugins/MandatoryFields;bin/plugins/Scales;bin/plugins/AdvancedSearch;bin/plugins/Pathways;bin/plugins/Casualties;bin/plugins/CIT;bin/plugins/Gastro;bin/plugins/VAI;" />
      <dependentAssembly>
        <assemblyIdentity name="System.ServiceModel" publicKeyToken="b77a5c561934e089" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Infragistics.Web.Mvc" publicKeyToken="7dd5c3163f2cd0cb" culture="neutral" />
        <bindingRedirect oldVersion="3.11.2.2084" newVersion="3.12.1.1010" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Configuration" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Abstractions" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Core" publicKeyToken="b77a5c561934e089" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System" publicKeyToken="b77a5c561934e089" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Xml.Linq" publicKeyToken="b77a5c561934e089" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Extensions" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Data" publicKeyToken="b77a5c561934e089" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Drawing" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Services" publicKeyToken="b03f5f7f11d50a3a" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-2.0.0.0" newVersion="2.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages.Razor" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-2.0.0.0" newVersion="2.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-2.0.0.0" newVersion="2.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <clientDependency version="13102001">
    <!-- This section is used for Web Forms only, the enableCompositeFiles="true" is optional and by default is set to true -->
    <fileRegistration defaultProvider="LoaderControlProvider">
      <providers>
        <add name="PageHeaderProvider" type="ClientDependency.Core.FileRegistration.Providers.PageHeaderProvider, ClientDependency.Core" enableCompositeFiles="true" />
        <add name="LazyLoadProvider" type="ClientDependency.Core.FileRegistration.Providers.LazyLoadProvider, ClientDependency.Core" enableCompositeFiles="true" />
        <add name="LoaderControlProvider" type="ClientDependency.Core.FileRegistration.Providers.LoaderControlProvider, ClientDependency.Core" enableCompositeFiles="true" />
      </providers>
    </fileRegistration>
    <!-- This section is used for MVC only -->
    <mvc defaultRenderer="StandardRenderer">
      <renderers>
        <add name="StandardRenderer" type="ClientDependency.Core.FileRegistration.Providers.StandardRenderer, ClientDependency.Core" enableCompositeFiles="true" />
        <add name="LazyLoadRenderer" type="ClientDependency.Core.FileRegistration.Providers.LazyLoadRenderer, ClientDependency.Core" enableCompositeFiles="true" />
      </renderers>
    </mvc>
    <compositeFiles defaultFileProcessingProvider="CompositeFileProcessor" compositeFileHandlerPath="~/DependencyHandler.axd">
      <fileProcessingProviders>
        <add name="CompositeFileProcessor" type="ClientDependency.Core.CompositeFiles.Providers.CompositeFileProcessingProvider, ClientDependency.Core" enableCssMinify="true" enableJsMinify="true" persistFiles="true" compositeFilePath="~/App_Data/ClientDependency" />
      </fileProcessingProviders>
      <fileMapProviders>
        <add name="XmlFileMap" type="ClientDependency.Core.CompositeFiles.Providers.XmlFileMapper, ClientDependency.Core" mapPath="~/App_Data/ClientDependency" />
      </fileMapProviders>
    </compositeFiles>
  </clientDependency>
  <system.serviceModel>
    <bindings>
      <basicHttpBinding>
        <binding name="ServiceBinding" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:10:00" sendTimeout="00:01:00" allowCookies="false" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" maxBufferSize="2147483647" maxBufferPoolSize="524288" maxReceivedMessageSize="2147483647" messageEncoding="Text" textEncoding="utf-8" transferMode="Streamed" useDefaultWebProxy="true">
          <readerQuotas maxDepth="2000000" maxStringContentLength="2147483647" maxArrayLength="2147483647" maxBytesPerRead="2147483647" maxNameTableCharCount="2147483647" />
          <security mode="None">
            <transport clientCredentialType="None" proxyCredentialType="None" realm="" />
            <message clientCredentialType="UserName" algorithmSuite="Default" />
          </security>
        </binding>
      </basicHttpBinding>
    </bindings>
    <client>
    </client>
    <behaviors>
      <endpointBehaviors>
        <behavior name="EndpointBehaviour">
          <dataContractSerializer maxItemsInObjectGraph="2147483647" />
        </behavior>
      </endpointBehaviors>
    </behaviors>
  </system.serviceModel>
  <!--maxJsonLength property -->
  <system.web.extensions>
    <scripting>
      <webServices>
        <jsonSerialization maxJsonLength="2147483644" />
      </webServices>
    </scripting>
  </system.web.extensions>
  <devExpress>
    <themes enableThemesAssembly="true" styleSheetTheme="" theme="Office365" baseColor="#cd7b29" font="14px 'Calibri'" />
    <compression enableHtmlCompression="true" enableCallbackCompression="true" enableResourceCompression="true" enableResourceMerging="true" />
    <settings doctypeMode="Html5" rightToLeft="false" embedRequiredClientLibraries="false" />
    <errors callbackErrorRedirectUrl="" />
  </devExpress>
  <bundleConfig configSource="Bundles.config" />
</configuration>