param (
    [string]$folderpath = $null,
    [string]$desiredParent = "Glintths.Shell",
    [string]$desiredLeaf = "Web.Config",
    [switch]$test
 )

# loads $filepath into a traversable DOM tree
function readXML($filepath) {

    if( Test-Path $filepath -PathType Leaf ) {
        [xml]$ret = Get-Content $filepath;
        return $ret;

    } else {
        Write-Host "File $($filepath) not found" -foregroundColor Red;
        return -1;
    }
}

# updates 'SessionState' node to the desired state
function updateSessionState($filepath) {
    $xml = readXML $filepath;
    $sessionstate = $xml.configuration.'system.web'.sessionstate;

    if ($null -ne $xml.configuration.'system.web'.sessionstate) {
        # clear 'sessionState' attributes and children
        $sessionstate.removeall();

        # Set required attributes
        $sessionstate.setattribute("mode", "StateServer");
        $sessionstate.setattribute("stateConnectionString", "tcpip=localhost:42424");
        $sessionstate.setattribute("cookieless", "false");
        $sessionstate.setattribute("timeout", "2880");

        # update file content
        $xml.save($filepath);

        Write-Host "$($filepath) successfully updated" -foregroundColor Green;
        return 0;
    }
    else {
        Write-Host "$($filepath) has no 'SessionState' node" -foregroundColor Yellow;
        # TODO: crio na mesma?
        return -1;
    }
}

# recursively navigates through every directory and file within $basepath
# in order to find files named 'Web.Config' that reside inside a directory named 'Glintths.Shell'
function getShellWebConfigs($basepath) {
    # operator @() is used in order to force the variable to hold an array
    # (for the cases where there is only at most one subfolder or file)
    $subfolders = @(Get-ChildItem -directory $basepath);
    $files = @(Get-ChildItem -file $basepath);
    $parentName = Split-Path -Path $basepath -Leaf;

    # only iterates through the leafs if parent folder name is the same as the desired one
    # (Glintths.Shell at the time of writing)
    if ($parentName -eq $desiredParent) {
        foreach ($file in $files) {
            # checks if file is named correctly before advancing
            if ($file.name -eq $desiredLeaf){
                Write-Host "Found one: $($file.fullname)" -ForegroundColor Green;

                # script can be run with a 'test' mode that doesn't alter files
                if (-Not $test) {
                    updateSessionState $file.fullname | Out-Null; 
                }
            }
        }
    }

    foreach ($folder in $subfolders) {
        # recursively navigate through the remaining folders
        getShellWebConfigs $folder.fullName;
    }

    return;
}

# $folderpath defaults to the current working directory if it is not set by the user
if ([string]::IsNullOrEmpty($folderpath)) {
    $folderpath = Get-Location;
}

getShellWebConfigs $folderpath
