BEGIN
  gplatform_patch.ginf_config_metadata_vpd (
      i_context             => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                 => 'S_API_LOCAL_ERESULTS',
      i_description         => 'Service API Internal',
      i_configgroup         => 'Services',
      i_allowedvalues       => '',
      i_allowedvaluestype   => '',
      i_required            => 'S',
      i_allowfacility       => 'N',
      i_requirefacility     => 'N',
      i_requireoverride     => 'N',
      i_allowmultiple       => 'N',
      i_version             => '1',
      i_hasresource         => 'N',
      i_search_value        => 'N',
      i_hasextrainfo        => 'N',
      i_extrainfoconfig     => '');
END;
/

BEGIN
  gplatform_patch.ginf_config_with_resource_vpd (
      i_context                => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                    => 'S_API_LOCAL_ERESULTS',
      i_system                 => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_new_system             => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_value                  => '#BASE_URL#GlinttHS.API.Internal.WS/Local/eResults/LocalEResultsService.svc',
      i_new_value              => '#BASE_URL#GlinttHS.API.Internal.WS/Local/eResults/LocalEResultsService.svc',
      i_smartkey               => 'S',
      i_isexternal             => 'N',
      i_externalendpoint       => '',
      i_version                => '1',
      i_position               => '',
      i_is_base_config         => 'S',
      i_extrainfo              => '',
      i_resource_domain        => NULL,
      i_resource_culture       => NULL,
      i_resource_text          => NULL,
      i_resource_search_text   => NULL);
END;
/

BEGIN
  gplatform_patch.ginf_config_metadata_vpd (
      i_context             => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                 => 'S_ER_ACTIVITIES_EXAMS',
      i_description         => 'Service API Internal',
      i_configgroup         => 'Services',
      i_allowedvalues       => '',
      i_allowedvaluestype   => '',
      i_required            => 'S',
      i_allowfacility       => 'N',
      i_requirefacility     => 'N',
      i_requireoverride     => 'N',
      i_allowmultiple       => 'N',
      i_version             => '1',
      i_hasresource         => 'N',
      i_search_value        => 'N',
      i_hasextrainfo        => 'N',
      i_extrainfoconfig     => '');
END;
/

BEGIN
  gplatform_patch.ginf_config_with_resource_vpd (
      i_context                => 'Glintt.GPlatform.ServiceDiscovery',
      i_key                    => 'S_ER_ACTIVITIES_EXAMS',
      i_system                 => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_new_system             => 'http://www.glintt.com/GPlatform/ServiceDescovery',
      i_value                  => '#BASE_URL#Cpchs.Activities.WCF.Host/ExamsManagementWS.svc',
      i_new_value              => '#BASE_URL#Cpchs.Activities.WCF.Host/ExamsManagementWS.svc',
      i_smartkey               => 'S',
      i_isexternal             => 'N',
      i_externalendpoint       => '',
      i_version                => '1',
      i_position               => '',
      i_is_base_config         => 'S',
      i_extrainfo              => '',
      i_resource_domain        => NULL,
      i_resource_culture       => NULL,
      i_resource_text          => NULL,
      i_resource_search_text   => NULL);
END;
/
