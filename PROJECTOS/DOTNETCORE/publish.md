# Publicar uma API DotnetCore no IIS

1. Abrir solução no VS
2. Solution Explorer
   1. Botão direito no Start-Up Project
   2. Publish
3. Configurações a ter em atenção:
   1. Connection
      1. Publish Method: File System
   2. Settings
      1. Configuration: Release - Any CPU
      2. Target Framework: netcoreapp2.2
      3. Deployment Mode: Framework-Dependent
      4. Target Runtime: Portable
4. Abrir IIS
5. Botão direito no site onde se vai publicar a aplicação
   1. Add Application
   2. Escolher caminho para a pasta onde residem as dll's (publish se não se mudou o nome no VS)
   3. No alias deve ficar o nome do Start-Up Project
