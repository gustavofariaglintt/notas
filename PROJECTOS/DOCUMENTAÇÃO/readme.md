# DOCUMENTAÇÃO

## SWS | clinical-api-old

|          |             |
|:--------:|:------------|
|**Descrição**|TODO(descrição)|
|**Especificação Técnica**|[SWAGGER(não operacional)](http://demoapliis.glinttocm.com:6002/EPR_MOBILE/ClinicalApi/api/documentation/index.html "glintt_sws/clinical-api-old")|
|**Postman**|TODO(postman file)|
|**Repositório**|[Bitbucket](https://bitbucket.org/glintt_sws/clinical-api-old/src/master/)|
|**Tecnologia**|TODO(tecnologia(s))|

## ClinicalSWS | api-clinical-activities

|          |             |
|:--------:|:------------|
|**Descrição**|Proxy para a secção da API DotnetCore do package GC_DOTNET_CODE.pck_clinical_activities, GC_DOTNET_CODE.PCKAPI_CLINICAL_ACTIVITIES_API e GC_DOTNET_CODE.PCKAPI_CLINICAL_ACTIVITIES|
|**Especificação Técnica**|[SWAGGER](http://demoapliis.glinttocm.com:6002/EPR_MOBILE/Glintt.Api.Clinical.Activities.Api/api/documentation/index.html "glintt_sws/api-clinical-activities")|
|**Postman**|TODO(link file)|
|**Repositório**|[Bitbucket](https://bitbucket.org/glintt_sws/api-clinical-activities/src/master/)|
|**Tecnologia**|GMonkey ( DotnetCode + PLSQL )|

## ClinicalSWS | api-clinical-opinion

|          |             |
|:--------:|:------------|
|**Descrição**|TODO(descrição)|
|**Especificação Técnica**|[SWAGGER(não operacional)](http://demoapliis.glinttocm.com:6002/EPR_MOBILE/Glintt.Api.Clinical.ClinicalOpinion.Api/api/documentation/index.html "glintt_sws/api-clinical-opinion")|
|**Postman**|TODO(postman file)|
|**Repositório**|[Bitbucket](https://bitbucket.org/glintt_sws/api-clinical-opinion/src/master/)|
|**Tecnologia**|TODO(tecnologia(s))|

## ClinicalSWS | api-clinical-dynamic

|          |             |
|:--------:|:------------|
|**Descrição**|TODO(descrição)|
|**Especificação Técnica**|[SWAGGER(não operacional)](http://demoapliis.glinttocm.com:6002/EPR_MOBILE/Glintt.Api.Clinical.Dynamic.Api/api/documentation/index.html "glintt_sws/api-clinical-dynamic")|
|**Postman**|TODO(postman file)|
|**Repositório**|[Bitbucket](https://bitbucket.org/glintt_sws/api-clinical-dynamic/src/master/)|
|**Tecnologia**|TODO(tecnologia(s))|

## ClinicalSWS | api-clinical-paramgeral

|          |             |
|:--------:|:------------|
|**Descrição**|TODO(descrição)|
|**Especificação Técnica**|[SWAGGER(não operacional)](http://demoapliis.glinttocm.com:6002/EPR_MOBILE/Glintt.Api.Clinical.ParamGeral.Api/api/documentation/index.html "glintt_sws/api-clinical-paramgeral")|
|**Postman**|TODO(postman file)|
|**Repositório**|[Bitbucket](https://bitbucket.org/glintt_sws/api-clinical-paramgeral/src/master/)|
|**Tecnologia**|TODO(tecnologia(s))|

## GCSifarma | SifarmaAPI

|          |             |
|:--------:|:------------|
|**Descrição**|TODO(descrição)|
|**Especificação Técnica**|[SWAGGER(não operacional)](http://demoapliis.glinttocm.com:6002/EPR_MOBILE/Glintt.Api.Clinical.Sifarma.API/api/documentation/index.html "glintt_sws/sifarmaapi")|
|**Postman**|TODO(postman file)|
|**Repositório**|[Bitbucket](https://bitbucket.org/glintt_sws/sifarmaapi/src/master/)|
|**Tecnologia**|TODO(tecnologia(s))|

## ClinicalSWS | api-clinical-vai

|          |             |
|:--------:|:------------|
|**Descrição**|TODO(descrição)|
|**Especificação Técnica**|[SWAGGER](http://demoapliis.glinttocm.com:6002/EPR_MOBILE/Glintt.Api.Clinical.Vai.Api/api/documentation/index.html "glintt_sws/api-clinical-vai")|
|**Postman**|TODO(postman file)|
|**Repositório**|[Bitbucket](https://bitbucket.org/glintt_sws/api-clinical-vai/src/master/)|
|**Tecnologia**|TODO(tecnologia(s))|

## ClinicalSWS | api-clinical-core

|          |             |
|:--------:|:------------|
|**Descrição**|TODO(descrição)|
|**Especificação Técnica**|[SWAGGER](http://10.250.39.103:6001/api/documentation/index.html "glintt_sws/api-clinical-core")|
|**Postman**|TODO(postman file)|
|**Repositório**|[Bitbucket](https://bitbucket.org/glintt_sws/api-clinical-core/src/master/)|
|**Tecnologia**|TODO(tecnologia(s))|

## ClinicalSWS | api-clinical-esp

|          |             |
|:--------:|:------------|
|**Descrição**|TODO(descrição)|
|**Especificação Técnica**|[SWAGGER](http://10.250.39.103:5002/api/documentation/index.html "glintt_sws/api-clinical-esp")|
|**Postman**|TODO(postman file)|
|**Repositório**|[Bitbucket](https://bitbucket.org/glintt_sws/api-clinical-esp/src/master/)|
|**Tecnologia**|TODO(tecnologia(s))|

## ClinicalSWS | api-clinical-enf

|          |             |
|:--------:|:------------|
|**Descrição**|TODO(descrição)|
|**Especificação Técnica**|[SWAGGER](http://link.todo "glintt_sws/api-clinical-enf")|
|**Postman**|TODO(postman file)|
|**Repositório**|[Bitbucket](https://bitbucket.org/glintt_sws/api-clinical-enf/src/master/)|
|**Tecnologia**|TODO(tecnologia(s))|
