# Fecho Patch Outubro (GPLATFORM)

Os tickets Glintt-HS-21913 e Glintt-HS-21914 foram desassociados do patch 09 de gplat, precisam de ser associados ao patch 10. Daí o query abaixo ter de ser corrido (SQL Server)

``` SQL
[dbo].[UpdateTicketVersionDescription]
'Glintt-HS-21914' --ticket a alterar Glintt-HS-21913 e Glintt-HS-21914
,'GCXYYMM.NEXT.01.01.zs' --versão a substituir
,'GCX1910.01.01.01.zs' --versão correta que deve ficar
GO
```
