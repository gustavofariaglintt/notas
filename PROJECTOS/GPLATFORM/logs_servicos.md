# Logs

Para ver requests e responses:

1. Solution: Core
2. GLab, GClinic ou GBase (em princípio)
3. Escolher o Client
4. Descobrir a Task (ex.: GetPendingVitalSigns)
5. Ir buscar a Key (ex: S_VITALSIGNS_PENDING)
6. Navigator (DB -> GPLATFORM -> .. ):
7. Query:

    ``` SQL
    SELECT *
        FROM ginf_config
        WHERE key = 'S_VITALSIGNS_CONFIRM';
    ```

8. Ir buscar o VALUE para fazer cross-reference com o ACTION do glog_ws

    ``` SQL
    SELECT *
        FROM glog_ws
        WHERE action LIKE '%/api/v1/transfusionunitvitalsigns/confirm'
        ORDER BY dt_request DESC;
    ```

    ``` SQL
    SELECT *
        FROM glog_ws
        WHERE response NOT LIKE 'StatusCode = OK%'
        ORDER BY dt_request DESC;
    ```

9. Analisar os requests e/ou responses.
