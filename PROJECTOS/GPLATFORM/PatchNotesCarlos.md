# Associar ticket a patch

1. No eticket pesquisar pelo ticket que se pretende associar
2. Clicar em Atividade
3. Encaminhamento para outra equipa
4. Glintt-HS - Produto - Área de Testes / Qualidade
5. Descrição do que foi feito
6. Clicar em Versões dentro da tab Atividade
7. Associar a versão Next do patch a fechar (Para associar à GPlatform o produto é "API Internal")
8. Ao associar a versão, caso apareça apenas a versão Next, em vez do patch atual. Seleciona-se a versão Next, sendo que depois deve ser alterado para a versão correta seguindo os passos indicados em "Atualização da versão no eTicket" no confluence (<https://glinttdev.atlassian.net/wiki/spaces/CLI/pages/369164290/Gui+o+de+Fecho+de+Patch+v17).>
9. Ao associar a versão, o problema e a ação devem ser bem descritos para não haver dúvidas. O Impacto verificado tem uma escala de 1 a 4 conforme o email "IMPORTANTE: Score de avaliação de risco de patch". PS: deveria ser criado uma página do confluence com isto.
10. Indicar que a atividade é Privada e gravar.
