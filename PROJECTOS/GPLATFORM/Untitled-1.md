# Bedside Tracker (GPLATFORM -> GBase + GCli + GLab)

|          |             |
|:--------:|:------------|
|**Descrição**|Informação/Tratamento/Administração de Colheitas/Transfusões de sangue|
|**Especificação Técnica**|N/A|
|**Chave GINF_CONFIG**|N/A (Tem um par ('S_%' e 'MS_') de chaves por pedido)|
|**Repositório**|$/GPlatform|
|**Pipelines**|[TFS](http://vtfs2013:8080/tfs/TFS2008_Collection/GPlatform/GPlatform%20Team/_build#_a=queued)|
|**Protocolo/Tecnologia**|REST -> .NET Framework + Entity Framework|
|**Postman**|TODO(postman file)| (Incompleto (mas dos pedidos já documentados já tem a informação das respectivas chaves da GINF_CONFIG))
|**Base de Dados**|GPLATFORM (SELECT * FROM user_objects WHERE ( object_name LIKE 'GLAB%' OR object_name LIKE 'GBASE%' OR object_name LIKE 'GCLI%') AND object_type = 'TABLE'|
