@echo off

REM DEV, CI, 17R1
REM SET ENVIRONMENT=DEV
REM SET ENVIRONMENT=CI
SET ENVIRONMENT=17R1

SET FOLDER=D:\GPlatform\%ENVIRONMENT%

@echo @@@ USING ENVIRONMENT=%ENVIRONMENT% AND FOLDER=%FOLDER% @@@

#@echo ### Nuget restore - CoreEntities... ###
#nuget restore %FOLDER%\Foundation\CoreEntities\CoreEntities.sln
#@echo ### BUILD - CoreEntities... ###
#MSBUILD /v:q %FOLDER%\Foundation\CoreEntities\CoreEntities.sln
#@echo ### ENDED BUILD - CoreEntities... ###

@echo ### Nuget restore - CoreRepositories... ###
nuget restore %FOLDER%\Microservices\Repositories\CoreRepositories\CoreRepositories.sln
@echo ### BUILD - CoreRepositories... ###
MSBUILD /v:q %FOLDER%\Microservices\Repositories\CoreRepositories\CoreRepositories.sln
@echo ### ENDED BUILD - CoreRepositories... ###

@echo ### Nuget restore - MS... ###
nuget restore %FOLDER%\Microservices\Services\CoreWebAPI\CoreWebAPI.sln
@echo ### BUILD - MS... ###
MSBUILD /v:q %FOLDER%\Microservices\Services\CoreWebAPI\CoreWebAPI.sln
@echo ### ENDED BUILD - MS... ###

@echo ### Nuget restore - Gateway... ###
nuget restore %FOLDER%\APIGateway\CoreWebAPI\GatewayCoreWebAPI.sln
@echo ### BUILD - Gateway... ###
MSBUILD /v:q %FOLDER%\APIGateway\CoreWebAPI\GatewayCoreWebAPI.sln
@echo ### ENDED BUILD - Gateway... ###

REM nuget restore %FOLDER%\Clients\Core\Core.sln
REM MSBUILD /v:q %FOLDER%\Clients\Core\Core.sln

PAUSE