# Email Hotfix

Boa tarde,

* Ticket original que provocou este erro – GLINTTHS-21914
* Em que situações ocorre, qual o problema exato – Validação de reações adversas repetidas no SIBAS e envio de reações adversas para o SIBAS à medida que é registado no BST
* Patch a partir do qual ocorre o erro, por versão – GCX1905.01.01.01.zs
* Sobre que patch(es) foi efetuado o hotfix - GCX1905.01.01.01.zs, GCX1906.01.01.01.zs, GCX1907.01.01.01.zs e GCX1908.01.01.01.zs
* Patch(es) onde vamos colocar a correção – GCX1909.01.01.01.zs
* Hotfix(es) gerado(s) - GCX1905.01.22.01.zs, GCX1906.01.12.01.zs, GCX1907.01.11.01.zs e GCX1908.01.05.01.zs

Atentamente,

<!-- markdownlint-capture -->
GCX1905.01.22.01.zs
GCX1906.01.12.01.zs
GCX1907.01.11.01.zs
GCX1908.01.05.01.zs

RT:
Glintt-HS-21913 -> terça (4) e quarta (8) : Envio de sinais vitais e reações adversas para o SIBAS à medida que é registado no BST
Glintt-HS-21914 -> quinta (8) : Validação de reações adversas repetidas no SIBAS
CHVNG-10648 -> sexta (8)
