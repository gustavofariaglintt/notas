# GPlatform - <http://vtfs2013:8080/tfs/tfs2008_collection/GPlatform/GPlatform%20Team>

## Schema DB

- GPLATFORM
- Utiliza a chave DBGPLATFORM da GING_CONFIG para se ligar à BD
- Tabelas:

``` SQL
SELECT *
    FROM user_objects
    WHERE ( object_name LIKE 'GLAB%'
            OR object_name LIKE 'GBASE%'
            OR object_name LIKE 'GCLI%')
    AND object_type = 'TABLE'
/
```

## Solutions

- CLIENTS:      C:\GPlatform\DEV\Clients\Core
- GATEWAY:      C:\GPlatform\DEV\APIGateway\CoreWebAPI
- SERVICES:     C:\GPlatform\DEV\Microservices\Services\CoreWebAPI
- REPOSITORIES: C:\GPlatform\DEV\Microservices\Repositories\CoreRepositories

### Services & Repositories related to BedsideTracker

- GBase
- GLab
- GClinic
