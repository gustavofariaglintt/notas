-- ----------------------------------------------------------------------------
-- INIT .NET CORE API
-- ----------------------------------------------------------------------------

PROCEDURE GetEFRByEpisode (
	p_t_doente      	IN  VARCHAR2,
	p_doente        	IN  VARCHAR2,
	p_t_episodio    	IN  VARCHAR2,
	p_episodio      	IN  VARCHAR2,
	o_cod_resp      	OUT VARCHAR2,
	o_cod_resp_descr	OUT VARCHAR2,
	o_num_cartao        OUT VARCHAR2,
	o_msg_erro			OUT VARCHAR2)
IS
BEGIN
	o_cod_resp := obter_cod_resp(p_t_doente, p_doente, p_t_episodio, p_episodio);
	o_cod_resp_descr := obter_cod_resp_descr(p_t_doente, p_doente, p_t_episodio, p_episodio);
	o_num_cartao := obter_cartao(p_t_doente, p_doente, p_t_episodio, p_episodio);
EXCEPTION
	WHEN OTHERS
	THEN
		o_msg_erro := 'ERRO: ' || SQLERRM;

END GetEFRByEpisode;

-- ----------------------------------------------------------------------------
-- END .NET CORE API
-- ----------------------------------------------------------------------------