PACKAGE BODY PCKAPI_CLINICAL_ACTIVITIES_API IS

    PROCEDURE GET_AF_MY_SCHEDULE_EPISODES
    (
        I_QUERY_PARAMETERS      IN      CLOB,
        I_URI_PARAMETERS        IN      CLOB,
        I_OBJECT                IN      CLOB,
        O_OBJECT                OUT     CLOB,
        O_REQUEST_STATUS        OUT     CLOB
    ) IS

        --| CONSTANTS
        c_PROP_CLINICAL         VARCHAR2(30) := 'clinical';
        c_PROP_ADMINISTRATIVE   VARCHAR2(30) := 'administrative';
        c_PROP_HAS_ALLERGIES    VARCHAR2(30) := 'has-allergies';
        c_PROP_IS_VIP           VARCHAR2(30) := 'is-vip';

        --| IGUAIS EM TODOS
        L_ID                    NUMBER;
        L_POS                   NUMBER;
        L_QUERY_PARAMETERS      GUTILSK_API_COMMON.TT_PARAM;
        L_URI_PARAMETERS        GUTILSK_API_COMMON.TT_PARAM;
        L_TIMESTAMP             TIMESTAMP := SYSTIMESTAMP;
        L_ERROR_MESSAGE         VARCHAR2(4000);

        --| ESPECIFICOS
        L_FILTERS               PCKAPI_CLINICAL_ACTIVITIES_TPS.T_AF_SCHEDULE_SEARCH;
        L_ANOS                  NUMBER;
        L_MESES                 NUMBER;
        L_DIAS                  NUMBER;

        TYPE T_INNER_SCHEDULE IS RECORD -- tipo usado localmente para o bulk collect
        (
            AREA                            VARCHAR2(30),
            EPISODE_TYPE                    VARCHAR2(30),
            EPISODE_NUMBER                  VARCHAR2(30),
            SERVICE_CODE                    VARCHAR2(10),
            SERVICE_DESCRIPTION             VARCHAR2(50),
            SPECIALTY_CODE                  VARCHAR2(10),
            SPECIALTY_DESCRIPTION           VARCHAR2(50),
            MEDICAL_ACT_DESCRIPTION         VARCHAR2(200),
            LOCATION                        VARCHAR2(200),
            COVERAGE_ENTITY_CODE            VARCHAR2(10),
            COVERAGE_ENTITY_NAME            VARCHAR2(200),
            RESPONSIBLE_DOCTOR_NUMBER       VARCHAR2(10),
            RESPONSIBLE_DOCTOR_LOGIN        VARCHAR2(30),
            RESPONSIBLE_DOCTOR_TITLE        VARCHAR2(12),
            RESPONSIBLE_DOCTOR_NAME         VARCHAR2(100),
            RESPONSIBLE_NURSE_NUMBER        VARCHAR2(10),
            RESPONSIBLE_NURSE_LOGIN         VARCHAR2(30),
            RESPONSIBLE_NURSE_TITLE         VARCHAR2(12),
            RESPONSIBLE_NURSE_NAME          VARCHAR2(100),
            APPOINTMENT_TIME                DATE,
            ATTENDANCE_TIME                 DATE,
            START_TIME                      DATE,
            END_TIME                        DATE,
            DISCHARGE_TIME                  DATE,
            HAS_DISCHARGE                   VARCHAR2(1),
            PATIENT_TYPE                    VARCHAR2(30),
            PATIENT_NUMBER                  VARCHAR2(30),
            PATIENT_NAME                    VARCHAR2(80),
            BIRTH_DATE                      DATE,
            GENDER                          VARCHAR2(1),
            URGENCY_PRIORITY_CODE           VARCHAR2(10),
            URGENCY_PRIORITY_DESCRIPTION    VARCHAR2(100),
            URGENCY_PRIORITY_COLOR          VARCHAR2(30),
            URGENCY_PRIORITY_ORDER          NUMBER,
            APPOINTMENT_STATE               VARCHAR2(10),
            APPOINTMENT_STATE_DESCRIPTION   VARCHAR2(200),
            APPOINTMENT_IS_EXTRA            VARCHAR2(1),
            SURGERY_TYPE                    VARCHAR2(2),
            OBSERVATIONS                    VARCHAR2(4000)
        );
        TYPE TT_INNER_SCHEDULE IS TABLE OF T_INNER_SCHEDULE INDEX BY BINARY_INTEGER;

        arr_INNER       TT_INNER_SCHEDULE;
        arr_AGENDA      PCKAPI_CLINICAL_ACTIVITIES_TPS.T_AF_SCHEDULE_OUTPUT;

        L_REFCURSOR     PCK_TYPES.EXTERNAL_CURSOR;

        --| variaveis para criar o SQL dinamico ( sim.. nao digam nada ao miguel ramos que estamos a usar sql dinamico)
        L_SQL                   CLOB;
        L_SQL_CONSULTATION      CLOB;
        L_SQL_URGENCY           CLOB;
        L_SQL_INTERNMENT        CLOB;
        L_SQL_SURGERY           CLOB;
        L_HAS_AT_LEAST_ONE_AREA BOOLEAN := FALSE;

        L_HAS_FILTER_SERVICES       BOOLEAN;
        L_HAS_FILTER_SPECIALTIES    BOOLEAN;
        L_FILTER_SERVICES           CLOB;
        L_FILTER_SPECIALTIES        CLOB;

        L_USER_SYS              VARCHAR2(30);
        L_PATIENT_IS_VIP        VARCHAR2(1000);
        L_PATIENT_HAS_ALLERGIES BOOLEAN;
        L_T_DOENTE_GROUP        VARCHAR2(30);
        L_PARAM_DIAS_URGENCY    NUMBER;

        FUNCTION F_INNER_EXISTS_AREA(i_AREA VARCHAR2) RETURN BOOLEAN IS
        BEGIN
            FOR i IN 1..L_FILTERS.FILTER_AREAS.COUNT
            LOOP
                IF L_FILTERS.FILTER_AREAS(i).CODE = i_AREA
                THEN
                    RETURN TRUE;
                END IF;
            END LOOP;
            RETURN FALSE;
        END F_INNER_EXISTS_AREA;

        FUNCTION F_INNER_CONVERT_LIST_TO_STRING
        (
            i_LIST PCKAPI_CLINICAL_ACTIVITIES_TPS.TT_FILTER_CODES
        ) RETURN CLOB
        IS
            L_CLOB CLOB := '(';
        BEGIN
            FOR i IN 1..i_LIST.COUNT
            LOOP
                IF i = 1
                THEN DBMS_LOB.append(L_CLOB,''''||i_LIST(i).CODE||'''');
                ELSE DBMS_LOB.append(L_CLOB,','''||i_LIST(i).CODE||'''');
                END IF;
            END LOOP;

            DBMS_LOB.append(L_CLOB,')');

            RETURN L_CLOB;

        END F_INNER_CONVERT_LIST_TO_STRING;

        FUNCTION F_INNER_PATIENT_HAS_ALLERGIES
        (   i_T_DOENTE      VARCHAR2,
            i_DOENTE        VARCHAR2,
            i_T_EPISODIO    VARCHAR2,
            i_EPISODIO      VARCHAR2
        ) RETURN BOOLEAN IS
            o_HAS_ALLERGIES VARCHAR2(1);
        BEGIN
            --| NULL    -> SEM ALERGIAS
            --| S       -> COM ALERGIAS

            SELECT 'S'
              INTO o_HAS_ALLERGIES
              FROM DUAL
             WHERE EXISTS
            (
                SELECT 1 FROM PC_CID_EPISODIO
                 WHERE T_DOENTE     = i_T_DOENTE
                   AND DOENTE       = i_DOENTE
                   --AND T_EPISODIO   = i_T_EPISODIO
                   --AND EPISODIO     = i_EPISODIO      -- visto com heloisa
                   AND TIPO IN ('ALERG','ALERG_SGICM')
                   AND nvl(ESTADO,'X') <> 'A'
                   AND nvl(DT_REG_FIM,ga_SYSDATE) >= ga_SYSDATE
            );

            RETURN o_HAS_ALLERGIES = 'S';

        EXCEPTION WHEN NO_DATA_FOUND THEN
            RETURN FALSE;
        END F_INNER_PATIENT_HAS_ALLERGIES;

        /*
        PROCEDURE STAMP(i_S NUMBER) IS BEGIN
            L_ERROR_MESSAGE := L_ERROR_MESSAGE || chr(10)||' -> '||(SYSTIMESTAMP-L_TIMESTAMP)|| ' -> '||i_S;
            L_TIMESTAMP     := SYSTIMESTAMP;
        END STAMP;
        */

    BEGIN

        --STAMP(0);
        L_POS := 0;
        SAVEPOINT SVP_ACTIVITIES_DOCTOR_SCHEDULE;

        L_ID := PCKAPI_LOGGING.START_LOG(
            i_PACKAGE           => $$PLSQL_UNIT,
            i_METHOD            => 'GET_AF_MY_SCHEDULE_EPISODES',
            i_QUERY_PARAMETERS  => I_QUERY_PARAMETERS,
            i_URI_PARAMETERS    => I_URI_PARAMETERS,
            i_I_OBJECT          => I_OBJECT
        );

        L_POS := 1;
        --L_QUERY_PARAMETERS  := GUTILSK_API_COMMON.deserialize_PARAMS(I_QUERY_PARAMETERS);
        --L_URI_PARAMETERS    := GUTILSK_API_COMMON.deserialize_PARAMS(I_URI_PARAMETERS);

        --STAMP(2);




        L_POS := 2;
        L_FILTERS           := PCKAPI_CLINICAL_ACTIVITIES_SER.t_af_schedule_search(i_OBJECT);

        L_POS := 2.1;
        <<VALIDATE_INPUTS>>
        BEGIN
            IF nvl(L_FILTERS.SKIP,-1) < 0 OR L_FILTERS.SKIP <> trunc(L_FILTERS.SKIP)
            THEN
                L_ERROR_MESSAGE := 'Invalid skip "'||L_FILTERS.SKIP||'"';
                RAISE GUTILSK_API_COMMON.E_SAIR_SEM_ERRO;
            ELSE
                arr_AGENDA.SKIP     := L_FILTERS.SKIP;
            END IF;

            IF nvl(L_FILTERS.TAKE,-1) < 0 OR L_FILTERS.TAKE <> trunc(L_FILTERS.TAKE)
            THEN
                L_ERROR_MESSAGE := 'Invalid take "'||L_FILTERS.TAKE||'"';
                RAISE GUTILSK_API_COMMON.E_SAIR_SEM_ERRO;
            ELSE
                arr_AGENDA.TAKE     := L_FILTERS.TAKE;
            END IF;

            arr_AGENDA.TOTAL    := 0;

        END VALIDATE_INPUTS;

        L_POS := 2.2;
        <<VALIDATE_USER>>
        BEGIN
            SELECT USER_SYS
              INTO L_USER_SYS
              FROM SD_PESS_HOSP_DEF
             WHERE N_MECAN = L_FILTERS.FILTER_DOCTOR;

        EXCEPTION WHEN NO_DATA_FOUND THEN
            L_ERROR_MESSAGE := 'Invalid user "'||L_FILTERS.FILTER_DOCTOR||'"';
            RAISE GUTILSK_API_COMMON.E_SAIR_SEM_ERRO;
        END VALIDATE_USER;

        L_POS := 2.3;
        L_HAS_FILTER_SERVICES       := L_FILTERS.FILTER_SERVICE.COUNT > 0;
        L_HAS_FILTER_SPECIALTIES    := L_FILTERS.FILTER_SPECIALTY.COUNT > 0;

        IF L_HAS_FILTER_SERVICES
        THEN
            L_FILTER_SERVICES := F_INNER_CONVERT_LIST_TO_STRING(L_FILTERS.FILTER_SERVICE);
        END IF;

        IF L_HAS_FILTER_SPECIALTIES
        THEN
            L_FILTER_SPECIALTIES := F_INNER_CONVERT_LIST_TO_STRING(L_FILTERS.FILTER_SPECIALTY);
        END IF;
        --STAMP(3);

        L_POS := 3.0;
        L_SQL := '
        WITH CFG AS
        (
            SELECT to_DATE(:1,''YYYY-MM-DD'')   REF_DATE,
                   :2                           REF_N_MECAN,
                   :3                           REF_USERSYS
              FROM DUAL
        )
        SELECT AREA,EPISODE_TYPE,EPISODE_NUMBER,SERVICE_CODE,SERVICE_DESCRIPTION,SPECIALTY_CODE,SPECIALTY_DESCRIPTION,MEDICAL_ACT_DESCRIPTION,
               LOCATION,COVERAGE_ENTITY_CODE,COVERAGE_ENTITY_NAME,
               RESPONSIBLE_DOCTOR_NUMBER,RESPONSIBLE_DOCTOR_LOGIN,RESPONSIBLE_DOCTOR_TITLE,RESPONSIBLE_DOCTOR_NAME,
               RESPONSIBLE_NURSE_NUMBER,RESPONSIBLE_NURSE_LOGIN,RESPONSIBLE_NURSE_TITLE,RESPONSIBLE_NURSE_NAME,
               APPOINTMENT_TIME,ATTENDANCE_TIME,START_TIME,END_TIME,DISCHARGE_TIME,HAS_DISCHARGE,PATIENT_TYPE,PATIENT_NUMBER,PATIENT_NAME,BIRTH_DATE,GENDER,
               URGENCY_PRIORITY_CODE,URGENCY_PRIORITY_DESCRIPTION,URGENCY_PRIORITY_COLOR,URGENCY_PRIORITY_ORDER,
               APPOINTMENT_STATE,APPOINTMENT_STATE_DESCRIPTION,APPOINTMENT_IS_EXTRA,SURGERY_TYPE,
               OBSERVATIONS
          FROM (';

        IF F_INNER_EXISTS_AREA('consultation') OR F_INNER_EXISTS_AREA('schedule')
        THEN
            L_SQL_CONSULTATION := '
SELECT 3                        AREA_ORDER,
       ''consultation''         AREA,
       ''Consultas''            EPISODE_TYPE,
       N_CONS                   EPISODE_NUMBER,
       to_CHAR(NULL)            SERVICE_CODE,
       to_CHAR(NULL)            SERVICE_DESCRIPTION,
       q0.COD_SERV              SPECIALTY_CODE,
       q1.DESCR_SERV            SPECIALTY_DESCRIPTION,
       PCK_ENTITIES_ENTITIES_BASE.devolve_descr_act_med(q0.COD_SERV,q0.T_ACT_MED,q0.T_ACT_MED_AUX)
       || CASE WHEN q6.FLAG_PRIM_VEZ = ''S''
               THEN CASE WHEN q0.FLAG_PRIM_VEZ = ''S''
                         THEN '' | Primeira''
                         WHEN q0.FLAG_PRIM_VEZ = ''N''
                         THEN '' | Subsequente''
                         END
               ELSE '''' END
       MEDICAL_ACT_DESCRIPTION,
       q3.DESCR_GAB             LOCATION,
       PK_EFR.obter_cod_resp(q0.T_DOENTE,q0.DOENTE,''Consultas'',q0.N_CONS)                             COVERAGE_ENTITY_CODE,
       PK_EFR.obtem_descr_efr(PK_EFR.obter_cod_resp(q0.T_DOENTE,q0.DOENTE,''Consultas'',q0.N_CONS))     COVERAGE_ENTITY_NAME,
       q0.MEDICO                RESPONSIBLE_DOCTOR_NUMBER,
       q5.USER_SYS              RESPONSIBLE_DOCTOR_LOGIN,
       q5.TITULO                RESPONSIBLE_DOCTOR_TITLE,
       q5.NOME                  RESPONSIBLE_DOCTOR_NAME,
       to_CHAR(NULL)            RESPONSIBLE_NURSE_NUMBER,
       to_CHAR(NULL)            RESPONSIBLE_NURSE_LOGIN,
       to_CHAR(NULL)            RESPONSIBLE_NURSE_TITLE,
       to_CHAR(NULL)            RESPONSIBLE_NURSE_NAME,
       q0.HR_CONS               APPOINTMENT_TIME,
       q0.HR_PR_CONS            ATTENDANCE_TIME,
       q0.HR_INICIO_REAL        START_TIME,
       q0.HR_FIM_REAL           END_TIME,
       to_DATE(NULL)            DISCHARGE_TIME,
       ''''                     HAS_DISCHARGE,
       q0.T_DOENTE              PATIENT_TYPE,
       q0.DOENTE                PATIENT_NUMBER,
       q2.NOME                  PATIENT_NAME,
       q2.DT_NASC               BIRTH_DATE,
       q2.SEXO                  GENDER,
       ''''                     URGENCY_PRIORITY_CODE,
       ''''                     URGENCY_PRIORITY_DESCRIPTION,
       ''''                     URGENCY_PRIORITY_COLOR,
       to_NUMBER(NULL)          URGENCY_PRIORITY_ORDER,
       q0.FLAG_ESTADO           APPOINTMENT_STATE,
       q4.RV_MEANING            APPOINTMENT_STATE_DESCRIPTION,
       nvl2(q0.N_AGENDA,''N'',''S'') APPOINTMENT_IS_EXTRA,
       ''''                     SURGERY_TYPE,
       q0.OBSERV_CONS           OBSERVATIONS
  FROM SD_CONS_MARC             q0
  JOIN SD_SERV                  q1 ON ( q0.COD_SERV = q1.COD_SERV AND nvl(q1.SERV_URG,''N'') = ''N'' )
  JOIN SD_DOENTE                q2 ON ( q0.T_DOENTE = q2.T_DOENTE AND q0.DOENTE = q2.DOENTE AND nvl(q2.FLAG_ANULADO,''N'') = ''N'')
  LEFT JOIN GR_GAB              q3 ON ( q0.COD_GAB  = q3.COD_GAB )
  JOIN (SELECT RV_LOW_VALUE,RV_MEANING FROM GR_CG_REF_CODES WHERE RV_DOMAIN = ''FLAG_ESTADO'') q4 ON ( q0.FLAG_ESTADO = q4.RV_LOW_VALUE )
  LEFT JOIN SD_PESS_HOSP_DEF    q5 ON ( q0.MEDICO   = q5.N_MECAN )
  LEFT JOIN SD_T_ACT_MED        q6 ON ( q0.COD_SERV = q6.COD_SERV AND q0.T_ACT_MED = q6.T_ACT_MED AND q0.T_ACT_MED = q0.T_ACT_MED_AUX )
  CROSS JOIN CFG
 WHERE q0.DT_CONS               = TRUNC( CFG.REF_DATE )
   AND q0.MEDICO                = CFG.REF_N_MECAN
   AND q0.FLAG_ESTADO           NOT IN ( ''A'',''R'' )
            ';

            IF L_HAS_FILTER_SPECIALTIES
            THEN
                DBMS_LOB.append(L_SQL_CONSULTATION,' AND q0.COD_SERV IN ');
                DBMS_LOB.append(L_SQL_CONSULTATION,L_FILTER_SPECIALTIES);
            END IF;

            DBMS_LOB.append(L_SQL,L_SQL_CONSULTATION);
            L_HAS_AT_LEAST_ONE_AREA := TRUE;
        END IF;

        IF ( F_INNER_EXISTS_AREA('urgency') OR F_INNER_EXISTS_AREA('schedule') )
           AND
           L_FILTERS.FILTER_SERVICE.COUNT = 0
           AND
           L_FILTERS.FILTER_SPECIALTY.COUNT = 0
           AND
           trunc(L_FILTERS.FILTER_DATE) <= ga_SYSDATE

        THEN

            BEGIN
                L_PARAM_DIAS_URGENCY := to_NUMBER( nvl(GHF_PARAM_BASE_MANUTENCAO('CPCBD_URG_TODAY_TIME_INTERVAL'),'2') );
            EXCEPTION WHEN OTHERS THEN
                L_PARAM_DIAS_URGENCY := 2;
            END;




            L_SQL_URGENCY := CASE WHEN L_HAS_AT_LEAST_ONE_AREA THEN ' UNION ALL ' ELSE '' END || '
SELECT 4                        AREA_ORDER,
       ''urgency''              AREA,
       q0.T_EPISODIO            EPISODE_TYPE,
       q0.EPISODIO              EPISODE_NUMBER,
       q0.COD_SERV              SERVICE_CODE,
       q1.DESCR_SERV            SERVICE_DESCRIPTION,
       to_char(q0.ID_ESPECIALIDADE)      SPECIALTY_CODE,
       q5.DESCR_ESPECIALIDADE   SPECIALTY_DESCRIPTION,
       ''''                     MEDICAL_ACT_DESCRIPTION,
       q0.LOCAL_DESCR           LOCATION,
       ''''                     COVERAGE_ENTITY_CODE,
       PK_EFR.obtem_descr_efr(PK_EFR.obter_cod_resp(q0.T_DOENTE,q0.DOENTE,q0.T_EPISODIO,q0.EPISODIO))   COVERAGE_ENTITY_NAME,
       q0.MEDICO_RESP           RESPONSIBLE_DOCTOR_NUMBER,
       q3.USER_SYS              RESPONSIBLE_DOCTOR_LOGIN,
       q3.TITULO                RESPONSIBLE_DOCTOR_TITLE,
       q3.NOME                  RESPONSIBLE_DOCTOR_NAME,
       q4.N_MECAN               RESPONSIBLE_NURSE_NUMBER,
       q0.ENFERMEIRO_RESP       RESPONSIBLE_NURSE_LOGIN,
       q4.TITULO                RESPONSIBLE_NURSE_TITLE,
       q4.NOME                  RESPONSIBLE_NURSE_NAME,
       TO_DATE(NULL)            APPOINTMENT_TIME,
       q0.DATA_ADMISSAO                ATTENDANCE_TIME,
       q0.DATA_PRIMEIRA_OBSERVACAO START_TIME,
       q0.DT_ALTA               END_TIME,
       TO_DATE(NULL)            DISCHARGE_TIME,
       '''' HAS_DISCHARGE,
       q0.T_DOENTE              PATIENT_TYPE,
       q0.DOENTE                PATIENT_NUMBER,
       q2.NOME                  PATIENT_NAME,
       q2.DT_NASC               BIRTH_DATE,
       q2.SEXO                  GENDER,
       q0.COD_GRAU_TRIAGEM      URGENCY_PRIORITY_CODE,
       q0.GRAU_TRIAGEM          URGENCY_PRIORITY_DESCRIPTION,
       q0.APRESENTACAO_TRIAGEM  URGENCY_PRIORITY_COLOR,
       q0.ORDEM_TRIAGEM         URGENCY_PRIORITY_ORDER,
       ''''                     APPOINTMENT_STATE,
       ''''                     APPOINTMENT_STATE_DESCRIPTION,
       ''''                     APPOINTMENT_IS_EXTRA,
       ''''                     SURGERY_TYPE,
       ''''                     OBSERVATIONS
  FROM URG_DADOS_ADIC           q0
  JOIN SD_SERV                  q1 ON ( q0.COD_SERV         = q1.COD_SERV )
  JOIN SD_DOENTE                q2 ON ( q0.T_DOENTE         = q2.T_DOENTE AND q0.DOENTE = q2.DOENTE AND nvl(q2.FLAG_ANULADO,''N'') = ''N'')
  LEFT JOIN SD_PESS_HOSP_DEF    q3 ON ( q0.MEDICO_RESP      = q3.N_MECAN )
  LEFT JOIN SD_PESS_HOSP_DEF    q4 ON ( q0.ENFERMEIRO_RESP  = q4.USER_SYS )
  LEFT JOIN GR_ESPECIALIDADES   q5 ON ( q0.ID_ESPECIALIDADE = q5.ID_ESPECIALIDADE )
  CROSS JOIN CFG
 WHERE q0.MEDICO_RESP           = CFG.REF_N_MECAN
            ';

            IF trunc(L_FILTERS.FILTER_DATE) = trunc(ga_SYSDATE)
            THEN
                L_SQL_URGENCY := L_SQL_URGENCY || '
                    AND q0.DT_CONS               BETWEEN CFG.REF_DATE - '||L_PARAM_DIAS_URGENCY||' AND CFG.REF_DATE
                    AND (q0.DT_ALTA IS NULL OR q0.DT_ALTA >= CFG.REF_DATE )
            ';
            ELSE
                L_SQL_URGENCY := L_SQL_URGENCY || ' AND q0.DT_CONS = CFG.REF_DATE ';
            END IF;

            DBMS_LOB.append(L_SQL,L_SQL_URGENCY);
            L_HAS_AT_LEAST_ONE_AREA := TRUE;
        END IF;

        IF ( F_INNER_EXISTS_AREA('internment') OR F_INNER_EXISTS_AREA('schedule') )
           AND
           trunc(L_FILTERS.FILTER_DATE) <= ga_SYSDATE
        THEN

            L_SQL_INTERNMENT := CASE WHEN L_HAS_AT_LEAST_ONE_AREA THEN ' UNION ALL ' ELSE '' END || '
SELECT AREA_ORDER,AREA,EPISODE_TYPE,EPISODE_NUMBER,SERVICE_CODE,SERVICE_DESCRIPTION,SPECIALTY_CODE,SPECIALTY_DESCRIPTION,MEDICAL_ACT_DESCRIPTION,
LOCATION,COVERAGE_ENTITY_CODE,COVERAGE_ENTITY_NAME,RESPONSIBLE_DOCTOR_NUMBER,RESPONSIBLE_DOCTOR_LOGIN,RESPONSIBLE_DOCTOR_TITLE,RESPONSIBLE_DOCTOR_NAME,
RESPONSIBLE_NURSE_NUMBER,RESPONSIBLE_NURSE_LOGIN,RESPONSIBLE_NURSE_TITLE,RESPONSIBLE_NURSE_NAME,APPOINTMENT_TIME,ATTENDANCE_TIME,START_TIME,END_TIME,
DISCHARGE_TIME,HAS_DISCHARGE,PATIENT_TYPE,PATIENT_NUMBER,PATIENT_NAME,BIRTH_DATE,GENDER,URGENCY_PRIORITY_CODE,URGENCY_PRIORITY_DESCRIPTION,
URGENCY_PRIORITY_COLOR,URGENCY_PRIORITY_ORDER,APPOINTMENT_STATE,APPOINTMENT_STATE_DESCRIPTION,APPOINTMENT_IS_EXTRA,SURGERY_TYPE,
OBSERVATIONS
  FROM
(
SELECT 2                        AREA_ORDER,
       ''internment''           AREA,
       CASE WHEN q0.FLAG_AMB_INT = ''A'' THEN ''Ambulatorio'' ELSE ''Internamentos'' END  EPISODE_TYPE,
       q0.N_INT                 EPISODE_NUMBER,
       q1.COD_SERV              SERVICE_CODE,
       q4.DESCR_SERV            SERVICE_DESCRIPTION,
       q1.COD_SERV_VALENCIA     SPECIALTY_CODE,
       q5.DESCR_SERV            SPECIALTY_DESCRIPTION,
       ''''                     MEDICAL_ACT_DESCRIPTION,
       nvl2( q1.SALA , q1.SALA||nvl2( q1.CAMA , '' | ''||q1.CAMA , '''') , q1.CAMA ) LOCATION,
       PK_EFR.obter_cod_resp(q0.T_DOENTE,q0.DOENTE,CASE WHEN q0.FLAG_AMB_INT = ''A'' THEN ''Ambulatorio'' ELSE ''Internamentos'' END,q0.N_INT)                            COVERAGE_ENTITY_CODE,
       PK_EFR.obtem_descr_efr(PK_EFR.obter_cod_resp(q0.T_DOENTE,q0.DOENTE,CASE WHEN q0.FLAG_AMB_INT = ''A'' THEN ''Ambulatorio'' ELSE ''Internamentos'' END,q0.N_INT))    COVERAGE_ENTITY_NAME,
       q1.N_MECAN               RESPONSIBLE_DOCTOR_NUMBER,
       q3.USER_SYS              RESPONSIBLE_DOCTOR_LOGIN,
       q3.TITULO                RESPONSIBLE_DOCTOR_TITLE,
       q3.NOME                  RESPONSIBLE_DOCTOR_NAME,
       q7.N_MECAN               RESPONSIBLE_NURSE_NUMBER,
       q6.ENF_RESP              RESPONSIBLE_NURSE_LOGIN,
       q7.TITULO                RESPONSIBLE_NURSE_TITLE,
       q7.NOME                  RESPONSIBLE_NURSE_NAME,
       to_DATE(NULL)            APPOINTMENT_TIME,
       trunc(q0.DT_INT)+(q0.HR_INT-trunc(q0.HR_INT)) ATTENDANCE_TIME,
       to_DATE(NULL)            START_TIME,
       to_DATE(NULL)            END_TIME,
       nvl2(    q0.DT_ALTA_CLINICA,
                trunc(q0.DT_ALTA_CLINICA)+(q0.HR_ALTA_CLINICA-trunc(q0.HR_ALTA_CLINICA)),
                CASE WHEN nvl(q0.DIAS_INT_PREV,0) > 0 THEN q0.DT_INT+q0.DIAS_INT_PREV ELSE to_DATE(NULL) END
           ) DISCHARGE_TIME,
       nvl2(q0.N_MECAN_ALTA,''S'',''N'')    HAS_DISCHARGE,
       q0.T_DOENTE              PATIENT_TYPE,
       q0.DOENTE                PATIENT_NUMBER,
       q2.NOME                  PATIENT_NAME,
       q2.DT_NASC               BIRTH_DATE,
       q2.SEXO                  GENDER,
       ''''                     URGENCY_PRIORITY_CODE,
       ''''                     URGENCY_PRIORITY_DESCRIPTION,
       ''''                     URGENCY_PRIORITY_COLOR,
       to_NUMBER(NULL)          URGENCY_PRIORITY_ORDER,
       ''''                     APPOINTMENT_STATE,
       ''''                     APPOINTMENT_STATE_DESCRIPTION,
       ''''                     APPOINTMENT_IS_EXTRA,
       ''''                     SURGERY_TYPE,
       ''''                     OBSERVATIONS,
       RANK() OVER (PARTITION BY q1.N_INT ORDER BY q1.DT_ENT_SERV DESC,q1.HR_ENT_SERV DESC) RNK_
  FROM SD_INTS      q0
  JOIN SD_INT_SERVS q1 ON ( q0.N_INT = q1.N_INT )
  JOIN SD_DOENTE    q2 ON ( q0.T_DOENTE = q2.T_DOENTE AND q0.DOENTE = q2.DOENTE AND nvl(q2.FLAG_ANULADO,''N'') = ''N'')
  LEFT JOIN SD_PESS_HOSP_DEF q3 ON ( q1.N_MECAN = q3.N_MECAN )
  LEFT JOIN SD_SERV      q4 ON ( q1.COD_SERV            = q4.COD_SERV )
  LEFT JOIN SD_SERV      q5 ON ( q1.COD_SERV_VALENCIA   = q5.COD_SERV )
  LEFT JOIN ENFPANEL_DADOS_ADIC q6 ON ( q0.N_INT = q6.N_INT )
  LEFT JOIN SD_PESS_HOSP_DEF    q7 ON ( q6.ENF_RESP = q7.USER_SYS )
  CROSS JOIN CFG
 WHERE q0.DT_INT <= trunc(CFG.REF_DATE+1)-INTERVAL ''1'' MINUTE
   AND ( q0.DT_ALTA IS NULL OR q0.DT_ALTA >= trunc(CFG.REF_DATE) )
   AND EXISTS ( SELECT 1
                  FROM SD_INTERV_EPISODIO q6
                 WHERE q6.T_DOENTE      = q0.T_DOENTE
                   AND q6.DOENTE        = q0.DOENTE
                   AND q6.T_EPISODIO    in (''Internamentos'',''Ambulatorio'')
                   AND q6.EPISODIO      = q0.N_INT
                   AND q6.N_MECAN       = CFG.REF_N_MECAN
                   AND q6.COD_FUNCAO IN (''ESP'',''INT'')
                   AND q6.DT_INICIO <= trunc(CFG.REF_DATE+1)-INTERVAL ''1'' MINUTE
                   AND ( q6.DT_FIM IS NULL OR q6.DT_FIM >= trunc(CFG.REF_DATE) )
               )
';

            IF L_HAS_FILTER_SPECIALTIES
            THEN
                DBMS_LOB.append(L_SQL_INTERNMENT,'
                AND EXISTS (    SELECT 1
                                  FROM SD_INT_SERVS q7
                                 WHERE q7.N_INT = q0.N_INT
                                   AND q7.DT_ENT_SERV <= trunc(CFG.REF_DATE+1)-INTERVAL ''1'' MINUTE
                                   AND ( q7.DT_ALTA_SERV IS NULL OR q7.DT_ALTA_SERV >= trunc(CFG.REF_DATE) )
                                   AND q7.COD_SERV_VALENCIA in
                ');
                DBMS_LOB.append(L_SQL_INTERNMENT,L_FILTER_SPECIALTIES);
                DBMS_LOB.append(L_SQL_INTERNMENT,'
                           )
                ');
            END IF;

            IF L_HAS_FILTER_SERVICES
            THEN
                DBMS_LOB.append(L_SQL_INTERNMENT,'
                AND EXISTS (    SELECT 1
                                  FROM SD_INT_SERVS q7
                                 WHERE q7.N_INT = q0.N_INT
                                   AND q7.DT_ENT_SERV <= trunc(CFG.REF_DATE+1)-INTERVAL ''1'' MINUTE
                                   AND ( q7.DT_ALTA_SERV IS NULL OR q7.DT_ALTA_SERV >= trunc(CFG.REF_DATE) )
                                   AND q7.COD_SERV in
                ');
                DBMS_LOB.append(L_SQL_INTERNMENT,L_FILTER_SERVICES);
                DBMS_LOB.append(L_SQL_INTERNMENT,'
                           )
                ');
            END IF;

            DBMS_LOB.append(L_SQL_INTERNMENT,'
            )
             WHERE RNK_ = 1
            ');

            DBMS_LOB.append(L_SQL,L_SQL_INTERNMENT);
            L_HAS_AT_LEAST_ONE_AREA := TRUE;
        END IF;


        IF F_INNER_EXISTS_AREA('surgery') OR F_INNER_EXISTS_AREA('schedule')
        THEN
             L_SQL_SURGERY := CASE WHEN L_HAS_AT_LEAST_ONE_AREA THEN ' UNION ALL ' ELSE '' END || '
            SELECT 1                        AREA_ORDER,
                   ''surgery''              AREA,
                   ''Plano-Oper''           EPISODE_TYPE,
                   q0.N_PLANO_OPER          EPISODE_NUMBER,
                   q0.COD_BLOCO             SERVICE_CODE,
                   q2.DESCR_SERV            SERVICE_DESCRIPTION,
                   q0.COD_SERV              SPECIALTY_CODE,
                   q3.DESCR_SERV            SPECIALTY_DESCRIPTION,
                   ''''                     MEDICAL_ACT_DESCRIPTION,
                   ''''                     LOCATION,
                   PK_EFR.obter_cod_resp(q0.T_DOENTE,q0.DOENTE,CASE WHEN q0.FLAG_AMB_INT = ''A'' THEN ''Ambulatorio'' ELSE ''Internamentos'' END,q0.N_INT)                            COVERAGE_ENTITY_CODE,
                   PK_EFR.obtem_descr_efr(PK_EFR.obter_cod_resp(q0.T_DOENTE,q0.DOENTE,CASE WHEN q0.FLAG_AMB_INT = ''A'' THEN ''Ambulatorio'' ELSE ''Internamentos'' END,q0.N_INT))    COVERAGE_ENTITY_NAME,
                   PCF_DEVOLVE_N_MECAN_CIR_P_V2(q0.T_DOENTE,q0.DOENTE,q0.N_PLANO_OPER) RESPONSIBLE_DOCTOR_NUMBER,
                   q4.USER_SYS              RESPONSIBLE_DOCTOR_LOGIN,
                   q4.TITULO                RESPONSIBLE_DOCTOR_TITLE,
                   q4.NOME                  RESPONSIBLE_DOCTOR_NAME,
                   '''' RESPONSIBLE_NURSE_NUMBER,
                   '''' RESPONSIBLE_NURSE_LOGIN,
                   '''' RESPONSIBLE_NURSE_TITLE,
                   '''' RESPONSIBLE_NURSE_NAME,
                   CASE WHEN PERIODO LIKE ''__:__''
                    THEN trunc(q0.DT_PLANO_OPER)+(to_DATE(PERIODO,''HH24:MI'')-trunc(to_DATE(PERIODO,''HH24:MI'')))
                    ELSE trunc(q0.DT_PLANO_OPER)
                    END APPOINTMENT_TIME,
                   q1.HR_ENTRADA_BLOCO      ATTENDANCE_TIME,
                   q1.HR_ENTRADA_SALA       START_TIME,
                   q1.HR_SAIDA_BLOCO        END_TIME,
                   to_DATE(NULL)            DISCHARGE_TIME,
                   ''''                     HAS_DISCHARGE,
                   q0.T_DOENTE              PATIENT_TYPE,
                   q0.DOENTE                PATIENT_NUMBER,
                   q5.NOME                  PATIENT_NAME,
                   q5.DT_NASC               BIRTH_DATE,
                   q5.SEXO                  GENDER,
                   ''''                     URGENCY_PRIORITY_CODE,
                   ''''                     URGENCY_PRIORITY_DESCRIPTION,
                   ''''                     URGENCY_PRIORITY_COLOR,
                   to_NUMBER(NULL)          URGENCY_PRIORITY_ORDER,
                   ''''                     APPOINTMENT_STATE,
                   ''''                     APPOINTMENT_STATE_DESCRIPTION,
                   ''''                     APPOINTMENT_IS_EXTRA,
                   q1.FLAG_PROG_URG         SURGERY_TYPE,
                   ''''                     OBSERVATIONS
              FROM SD_PLANO_OPER    q0
              LEFT JOIN SD_REG_OPER q1 ON ( GHF_DEVOLVE_N_REG_OPER(q0.T_DOENTE,q0.DOENTE,q0.N_PLANO_OPER) = q1.N_REG_OPER )
              JOIN SD_SERV          q2 ON ( q0.COD_BLOCO    = q2.COD_SERV )
              JOIN SD_SERV          q3 ON ( q0.COD_SERV     = q3.COD_SERV )
              LEFT JOIN SD_PESS_HOSP_DEF    q4 ON ( q4.N_MECAN = PCF_DEVOLVE_N_MECAN_CIR_P_V2(q0.T_DOENTE,q0.DOENTE,q0.N_PLANO_OPER) )
              JOIN SD_DOENTE        q5 ON ( q0.T_DOENTE = q5.T_DOENTE AND q0.DOENTE = q5.DOENTE AND nvl(q5.FLAG_ANULADO,''N'') = ''N'' )
              CROSS JOIN CFG
             WHERE q0.DT_PLANO_OPER = CFG.REF_DATE
               AND (
                        (   q1.N_REG_OPER IS NULL
                            AND
                            EXISTS (    SELECT 0
                                          FROM SDV_PLANO_INTERVENIENTE q6
                                         WHERE q6.N_PLANO_OPER  = q0.N_PLANO_OPER
                                           AND q6.N_MECAN       = CFG.REF_N_MECAN
                                   )
                        )
                        OR
                        (   q1.N_REG_OPER IS NOT NULL
                            AND
                            EXISTS (    SELECT 0
                                          FROM SD_INT_OPER q7
                                         WHERE q7.N_REG_OPER    = q1.N_REG_OPER
                                           AND q7.N_MECAN       = CFG.REF_N_MECAN
                                   )
                        )
                   )
            ';

            IF L_HAS_FILTER_SERVICES
            THEN
                DBMS_LOB.append(L_SQL_SURGERY,' AND q0.COD_BLOCO IN ');
                DBMS_LOB.append(L_SQL_SURGERY,L_FILTER_SERVICES);
            END IF;

            IF L_HAS_FILTER_SPECIALTIES
            THEN
                DBMS_LOB.append(L_SQL_SURGERY,' AND q0.COD_SERV IN ');
                DBMS_LOB.append(L_SQL_SURGERY,L_FILTER_SPECIALTIES);
            END IF;


            DBMS_LOB.append(L_SQL,L_SQL_SURGERY);
            L_HAS_AT_LEAST_ONE_AREA := TRUE;
        END IF;

        DBMS_LOB.append(L_SQL,' ) ');

        <<QUERY_ORDER>>
        DECLARE
            L_SQL_ORDER VARCHAR2(4000) := ' ORDER BY ';

            FUNCTION aux_INNER_SORT(i_BOOLEAN BOOLEAN) RETURN VARCHAR2
            IS
            BEGIN
                IF i_BOOLEAN
                THEN RETURN ' NULLS LAST, ';
                ELSE RETURN ' DESC NULLS LAST, ';
                END IF;
            END aux_INNER_SORT;

        BEGIN

            FOR i IN 1..L_FILTERS.SORT_MODE.COUNT
            LOOP
                IF L_FILTERS.SORT_MODE(i).FIELD = 'AREA'
                THEN
                    L_SQL_ORDER := L_SQL_ORDER || 'AREA_ORDER ';
                    L_SQL_ORDER := L_SQL_ORDER || aux_INNER_SORT(L_FILTERS.SORT_MODE(i).ASCENDING);
                ELSIF L_FILTERS.SORT_MODE(i).FIELD = 'PATIENT'
                THEN
                    L_SQL_ORDER := L_SQL_ORDER || 'PATIENT_NAME ';
                    L_SQL_ORDER := L_SQL_ORDER || aux_INNER_SORT(L_FILTERS.SORT_MODE(i).ASCENDING);
                ELSIF L_FILTERS.SORT_MODE(i).FIELD = 'AGE'
                THEN
                    L_SQL_ORDER := L_SQL_ORDER || '(SYSDATE-BIRTH_DATE) ';
                    L_SQL_ORDER := L_SQL_ORDER || aux_INNER_SORT(L_FILTERS.SORT_MODE(i).ASCENDING);
                ELSIF L_FILTERS.SORT_MODE(i).FIELD = 'SERVICE'
                THEN
                    L_SQL_ORDER := L_SQL_ORDER || 'SERVICE_DESCRIPTION ';
                    L_SQL_ORDER := L_SQL_ORDER || aux_INNER_SORT(L_FILTERS.SORT_MODE(i).ASCENDING);
                ELSIF L_FILTERS.SORT_MODE(i).FIELD = 'SPECIALTY'
                THEN
                    L_SQL_ORDER := L_SQL_ORDER || 'SPECIALTY_DESCRIPTION ';
                    L_SQL_ORDER := L_SQL_ORDER || aux_INNER_SORT(L_FILTERS.SORT_MODE(i).ASCENDING);
                ELSIF L_FILTERS.SORT_MODE(i).FIELD = 'MEDICAL_ACT'
                THEN
                    L_SQL_ORDER := L_SQL_ORDER || 'MEDICAL_ACT_DESCRIPTION ';
                    L_SQL_ORDER := L_SQL_ORDER || aux_INNER_SORT(L_FILTERS.SORT_MODE(i).ASCENDING);
                ELSIF L_FILTERS.SORT_MODE(i).FIELD = 'RESPONSIVE_DOCTOR'
                THEN
                    L_SQL_ORDER := L_SQL_ORDER || 'RESPONSIBLE_DOCTOR_NAME ';
                    L_SQL_ORDER := L_SQL_ORDER || aux_INNER_SORT(L_FILTERS.SORT_MODE(i).ASCENDING);
                ELSIF L_FILTERS.SORT_MODE(i).FIELD = 'RESPONSIBLE_NURSE'
                THEN
                    L_SQL_ORDER := L_SQL_ORDER || 'RESPONSIBLE_NURSE_NAME ';
                    L_SQL_ORDER := L_SQL_ORDER || aux_INNER_SORT(L_FILTERS.SORT_MODE(i).ASCENDING);
                ELSIF L_FILTERS.SORT_MODE(i).FIELD = 'COVERAGE'
                THEN
                    L_SQL_ORDER := L_SQL_ORDER || 'COVERAGE_ENTITY_NAME ';
                    L_SQL_ORDER := L_SQL_ORDER || aux_INNER_SORT(L_FILTERS.SORT_MODE(i).ASCENDING);
                END IF;

            END LOOP;

            L_SQL_ORDER := L_SQL_ORDER|| ' AREA_ORDER';

            L_SQL_ORDER := L_SQL_ORDER ||
                           ',CASE WHEN AREA = ''urgency''
                                  THEN URGENCY_PRIORITY_ORDER
                                  ELSE to_NUMBER(NULL)
                                  END
                            ,CASE WHEN AREA = ''consultation''
                                  THEN decode(APPOINTMENT_STATE,''E'',1
                                                               ,''P'',2
                                                               ,''MC'',3
                                                               ,''EF'',4
                                                               ,''OK'',5
                                                               ,''F'',6,7)
                                  ELSE NULL
                                  END
                            ,CASE WHEN AREA in (''consultation'',''surgery'')
                                  THEN APPOINTMENT_TIME
                                  ELSE to_DATE(NULL)
                                  END
                            ,CASE WHEN AREA in (''internment'')
                                  THEN ATTENDANCE_TIME
                                  ELSE to_DATE(NULL)
                                  END DESC
                            ,CASE WHEN AREA in (''urgency'')
                                  THEN ATTENDANCE_TIME
                                  ELSE to_DATE(NULL)
                                  END';

            DBMS_LOB.append(L_SQL,L_SQL_ORDER);

        END QUERY_ORDER;

        L_POS := 3.1;
/*
        enf.pck_pe_logging.log_clob('x','QUERY',L_SQL);
        enf.pck_pe_logging.log_clob('x','PARAMS','['||to_CHAR(L_FILTERS.FILTER_DATE,'YYYY-MM-DD')||']['||L_FILTERS.FILTER_DOCTOR||']['||L_USER_SYS||']');
*/
        L_POS := 4;
        --STAMP(4);
        IF L_HAS_AT_LEAST_ONE_AREA
        THEN

            L_POS := 4.1;

            OPEN L_REFCURSOR FOR L_SQL
            using   to_CHAR(L_FILTERS.FILTER_DATE,'YYYY-MM-DD'),
                    L_FILTERS.FILTER_DOCTOR,
                    L_USER_SYS;

            L_POS := 4.2;
            FETCH L_REFCURSOR BULK COLLECT INTO arr_INNER;

            L_POS := 4.3;
            arr_AGENDA.TOTAL    := arr_INNER.COUNT;
            arr_INNER.delete(L_FILTERS.SKIP + L_FILTERS.TAKE+1,arr_INNER.COUNT);
            arr_INNER.delete(1,L_FILTERS.SKIP);

            L_POS := 4.4;

            L_T_DOENTE_GROUP := nvl(GHF_PARAM_BASE_MANUTENCAO('GHAD_T_DOENTE_GRUPO'),'GRP');
            L_POS := 4.5;
        --STAMP(5);
            FOR i IN nvl(arr_INNER.FIRST,1)..nvl(arr_INNER.LAST,0)
            LOOP
                L_POS := 4.6;

                arr_AGENDA.EPISODES(i).AREA                          := arr_INNER(i).AREA;
                arr_AGENDA.EPISODES(i).EPISODE_TYPE                  := arr_INNER(i).EPISODE_TYPE;
                arr_AGENDA.EPISODES(i).EPISODE_NUMBER                := arr_INNER(i).EPISODE_NUMBER;
                arr_AGENDA.EPISODES(i).SERVICE.CODE                  := arr_INNER(i).SERVICE_CODE;
                arr_AGENDA.EPISODES(i).SERVICE.DESCRIPTION           := arr_INNER(i).SERVICE_DESCRIPTION;
                arr_AGENDA.EPISODES(i).SPECIALTY.CODE                := arr_INNER(i).SPECIALTY_CODE;
                arr_AGENDA.EPISODES(i).SPECIALTY.DESCRIPTION         := arr_INNER(i).SPECIALTY_DESCRIPTION;
                arr_AGENDA.EPISODES(i).LOCATION                      := arr_INNER(i).LOCATION;
                arr_AGENDA.EPISODES(i).MEDICAL_ACT.DESCRIPTION       := arr_INNER(i).MEDICAL_ACT_DESCRIPTION;
                arr_AGENDA.EPISODES(i).COVERAGE.ENTITY.CODE          := arr_INNER(i).COVERAGE_ENTITY_CODE;
                arr_AGENDA.EPISODES(i).COVERAGE.ENTITY.NAME          := arr_INNER(i).COVERAGE_ENTITY_NAME;

                IF arr_INNER(i).RESPONSIBLE_DOCTOR_LOGIN IS NOT NULL
                THEN
                    arr_AGENDA.EPISODES(i).RESPONSIBLE_DOCTOR.TYPE       := 'doctor';
                    arr_AGENDA.EPISODES(i).RESPONSIBLE_DOCTOR.NUMBER     := arr_INNER(i).RESPONSIBLE_DOCTOR_NUMBER;
                    arr_AGENDA.EPISODES(i).RESPONSIBLE_DOCTOR.LOGIN      := arr_INNER(i).RESPONSIBLE_DOCTOR_LOGIN;
                    arr_AGENDA.EPISODES(i).RESPONSIBLE_DOCTOR.TITLE      := arr_INNER(i).RESPONSIBLE_DOCTOR_TITLE;
                    arr_AGENDA.EPISODES(i).RESPONSIBLE_DOCTOR.NAME       := arr_INNER(i).RESPONSIBLE_DOCTOR_NAME;
                END IF;

                IF arr_INNER(i).RESPONSIBLE_NURSE_LOGIN IS NOT NULL
                THEN
                    arr_AGENDA.EPISODES(i).RESPONSIBLE_NURSE.TYPE       := 'nurse';
                    arr_AGENDA.EPISODES(i).RESPONSIBLE_NURSE.NUMBER     := arr_INNER(i).RESPONSIBLE_NURSE_NUMBER;
                    arr_AGENDA.EPISODES(i).RESPONSIBLE_NURSE.LOGIN      := arr_INNER(i).RESPONSIBLE_NURSE_LOGIN;
                    arr_AGENDA.EPISODES(i).RESPONSIBLE_NURSE.TITLE      := arr_INNER(i).RESPONSIBLE_NURSE_TITLE;
                    arr_AGENDA.EPISODES(i).RESPONSIBLE_NURSE.NAME       := arr_INNER(i).RESPONSIBLE_NURSE_NAME;
                END IF;

                arr_AGENDA.EPISODES(i).APPOINTMENT_TIME              := arr_INNER(i).APPOINTMENT_TIME;
                arr_AGENDA.EPISODES(i).ATTENDANCE_TIME               := arr_INNER(i).ATTENDANCE_TIME;
                arr_AGENDA.EPISODES(i).START_TIME                    := arr_INNER(i).START_TIME;
                arr_AGENDA.EPISODES(i).END_TIME                      := arr_INNER(i).END_TIME;
                arr_AGENDA.EPISODES(i).DISCHARGE_TIME                := arr_INNER(i).DISCHARGE_TIME;
                arr_AGENDA.EPISODES(i).HAS_DISCHARGE                 := arr_INNER(i).HAS_DISCHARGE = 'S';
                arr_AGENDA.EPISODES(i).OBSERVATIONS                  := arr_INNER(i).OBSERVATIONS;
                arr_AGENDA.EPISODES(i).PATIENT.TYPE                  := arr_INNER(i).PATIENT_TYPE;
                arr_AGENDA.EPISODES(i).PATIENT.NUMBER                := arr_INNER(i).PATIENT_NUMBER;

                arr_AGENDA.EPISODES(i).PATIENT.SHORT_NAME            := pcf_devolve_nome_tratado(arr_INNER(i).PATIENT_NAME);
                arr_AGENDA.EPISODES(i).PATIENT.NAME                  := arr_INNER(i).PATIENT_NAME;
                arr_AGENDA.EPISODES(i).PATIENT.BIRTH_DATE            := arr_INNER(i).BIRTH_DATE;
            L_POS := 8;

                arr_AGENDA.EPISODES(i).PATIENT.AGE := PCF_FORMATAR_IDADE
                                                        (   w_dt_nasc   => arr_INNER(i).BIRTH_DATE,
                                                            w_anos      => L_ANOS,
                                                            w_meses     => L_MESES,
                                                            w_dias      => L_DIAS
                                                        );

                arr_AGENDA.EPISODES(i).PATIENT.GENDER := CASE arr_INNER(i).GENDER WHEN 'M' THEN 'male' WHEN 'F' THEN 'female' ELSE 'other' END;
            L_POS := 9;

                L_PATIENT_HAS_ALLERGIES := F_INNER_PATIENT_HAS_ALLERGIES
                (   i_T_DOENTE    => arr_INNER(i).PATIENT_TYPE,
                    i_DOENTE      => arr_INNER(i).PATIENT_NUMBER,
                    i_T_EPISODIO  => arr_INNER(i).EPISODE_TYPE,
                    i_EPISODIO    => arr_INNER(i).EPISODE_NUMBER
                );
            L_POS := 10;

                IF L_PATIENT_HAS_ALLERGIES
                THEN
                    arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES(arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES.COUNT+1).SCOPE      := c_PROP_ADMINISTRATIVE;
                    arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES(arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES.COUNT).PROPERTY     := c_PROP_HAS_ALLERGIES;
                    arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES(arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES.COUNT).VALUE        := 'true';
                    arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES(arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES.COUNT).DESCRIPTION  := '';
                END IF;

                L_PATIENT_IS_VIP := GET_PATIENT_VIP_DESCRIPTION(arr_INNER(i).PATIENT_TYPE, arr_INNER(i).PATIENT_NUMBER);
                IF L_PATIENT_IS_VIP IS NOT NULL
                THEN
                    arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES(arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES.COUNT+1).SCOPE      := c_PROP_ADMINISTRATIVE;
                    arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES(arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES.COUNT).PROPERTY     := c_PROP_IS_VIP;
                    arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES(arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES.COUNT).VALUE        := 'true';
                    arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES(arr_AGENDA.EPISODES(i).PATIENT.OTHER_PROPERTIES.COUNT).DESCRIPTION  := L_PATIENT_IS_VIP;
                END IF;

                arr_AGENDA.EPISODES(i).DETAILS.URGENCY_PRIORITY                 := arr_INNER(i).URGENCY_PRIORITY_CODE;
                arr_AGENDA.EPISODES(i).DETAILS.URGENCY_PRIORITY_DESCRIPTION     := arr_INNER(i).URGENCY_PRIORITY_DESCRIPTION;
                arr_AGENDA.EPISODES(i).DETAILS.URGENCY_PRIORITY_COLOR           := arr_INNER(i).URGENCY_PRIORITY_COLOR;
                arr_AGENDA.EPISODES(i).DETAILS.APPOINTMENT_STATE                := arr_INNER(i).APPOINTMENT_STATE;
                arr_AGENDA.EPISODES(i).DETAILS.APPOINTMENT_STATE_DESCRIPTION    := arr_INNER(i).APPOINTMENT_STATE_DESCRIPTION;
                IF arr_INNER(i).SURGERY_TYPE = 'P'
                THEN
                    arr_AGENDA.EPISODES(i).DETAILS.SURGERY_TYPE := 'scheduled';
                ELSIF arr_INNER(i).SURGERY_TYPE = 'U'
                THEN
                    arr_AGENDA.EPISODES(i).DETAILS.SURGERY_TYPE := 'urgent';
                END IF;

                IF arr_INNER(i).APPOINTMENT_IS_EXTRA IS NOT NULL
                THEN
                    arr_AGENDA.EPISODES(i).DETAILS.APPOINTMENT_IS_EXTRA := arr_INNER(i).APPOINTMENT_IS_EXTRA = 'S';
                END IF;

                IF arr_INNER(i).EPISODE_TYPE = 'Consultas' AND arr_INNER(i).PATIENT_TYPE = L_T_DOENTE_GROUP
                THEN
                    arr_AGENDA.EPISODES(i).DETAILS.APPOINTMENT_IS_GROUP     := true;
                END IF;

            END LOOP;

        ELSE
            NULL; --TODO counts
        END IF;
        --STAMP(6);
        L_POS := 5;
        O_OBJECT            := PCKAPI_CLINICAL_ACTIVITIES_SER.tj_tafscheduleoutput(arr_AGENDA);
        L_POS := 5.1;
        O_REQUEST_STATUS    := GUTILSK_API_COMMON.REQUEST_STATUS(GUTILSK_API_COMMON.SC_200_OK);
        L_POS := 5.2;
        --STAMP(7);
        PCKAPI_LOGGING.END_LOG
        (
            i_id                   => L_ID,
            i_o_object             => O_OBJECT,
            i_request_status       => O_REQUEST_STATUS,
            i_exception_details    => NULL
        );
        --STAMP(8);
        --enf.PCK_PE_LOGGING.log('TEMPOS','X',L_ERROR_MESSAGE);
        L_POS := 5.3;

    EXCEPTION
    WHEN GUTILSK_API_COMMON.E_SAIR_SEM_ERRO THEN

        O_REQUEST_STATUS := GUTILSK_API_COMMON.REQUEST_STATUS(GUTILSK_API_COMMON.SC_400_BAD_REQUEST,L_ERROR_MESSAGE);

        PCKAPI_LOGGING.END_LOG
        (
            i_ID                   => L_ID,
            i_O_OBJECT             => O_OBJECT,
            i_REQUEST_STATUS       => O_REQUEST_STATUS,
            --i_ACTIONS              => NULL,
            i_EXCEPTION_DETAILS    => L_ERROR_MESSAGE
        );

        ROLLBACK TO SAVEPOINT SVP_ACTIVITIES_DOCTOR_SCHEDULE;

    WHEN OTHERS THEN

        L_ERROR_MESSAGE := SUBSTR('('||L_POS||')'||L_ERROR_MESSAGE|| ' | ' ||SQLERRM, 1, 2000);
        O_REQUEST_STATUS := GUTILSK_API_COMMON.REQUEST_STATUS(GUTILSK_API_COMMON.SC_500_INTERNAL_SERVER_ERROR,L_ERROR_MESSAGE);

        PCKAPI_LOGGING.END_LOG
        (
            i_id                   => L_ID,
            i_o_object             => O_OBJECT,
            i_request_status       => O_REQUEST_STATUS,
            --i_actions              => NULL,
            i_exception_details    => L_ERROR_MESSAGE
        );

        ROLLBACK TO SAVEPOINT SVP_ACTIVITIES_DOCTOR_SCHEDULE;

    END GET_AF_MY_SCHEDULE_EPISODES;

end PCKAPI_CLINICAL_ACTIVITIES_API;