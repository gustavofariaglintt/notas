# Glintt-HS-22037

Jira Clin_Dev&QA_Patch

+ **Epic** [GX-4481](https://glinttdev.atlassian.net/browse/GX-4481)
+ **User Story** [GX-4482](https://glinttdev.atlassian.net/browse/GX-4482)
+ **Defect** [GLB-7041](https://glinttdev.atlassian.net/browse/GLB-7041)

## Mensagem de Check-In

Glintt-HS-22037
GLB-7041
A ordem das colunas era ditada pelo campo ordem_sala_pos do último bloco passado (ignorava os anteriores).
Agora, basta haver um "A" para a ordem mostrada ser a correspondente.

## Changesets

### DEV

212710

### CI

212712

### 17R1

212713

## Alterações

Modules/Html/Glintt.ClinicalDesktop/ClinicalDesktop.DataAccess\DataManagement

## Utils

``` sql
select p.bloco, s.descr_serv, p.ordem_sala_pos
from PARAM_TACT_GERAIS p, sd_serv s
where p.bloco in ('440000', '410000')
and p.bloco = s.cod_serv;
```
