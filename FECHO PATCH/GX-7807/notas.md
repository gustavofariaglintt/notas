# NOTAS

## Consulta

PCK_MOBILE_DOC_APP.GetDocAppForServExec
PCK_MOBILE_DOC_APP.GetDocAppForDoctorAndServExec
PCK_MOBILE_DOC_APP.GetDocAppForRoom

Descrição:

``` sql
CASE
WHEN marc.t_act_med_aux != marc.t_act_med
THEN
    (SELECT initcap(descr_rubr)
        FROM fa_rubr r
        WHERE r.cod_rubr = marc.t_act_med)
ELSE
    (SELECT initcap(descr)
        FROM sd_t_act_med m
        WHERE m.cod_serv = marc.cod_serv
            AND m.t_act_med =
                    NVL (
                        marc.t_act_med_aux,
                        marc.t_act_med))
END
AS docappmedicalactdecription,
```

FLAG_PRIM_VEZ:

``` sql
marc.flag_prim_vez AS docappfirsttime,
--...
FROM sd_cons_marc marc,
```
