# CHVNG-10797

## Descrição

collecteddate desfazada uma hora

### Mensagem Check-In

CHVNG-10797: collecteddate from datetime to datetimeoffset' (entities)
CHVNG-10797: collecteddate from datetime to datetimeoffset' (repositories)

### Changesets

#### Dev

213086 - coreEntities
213241 - coreRepositories

#### CI

213247 - coreEntities
213248 - coreRepositories

#### 17R1

214121 - coreEntities
214130 - coreRepositories

### Merge

#### DEV -> CI

DONE

#### CI -> 17R1

TODO

### Patch

TODO

### Notas

Commits com os updates dos nugets vão separados dos outros porque têm de ser feitos à mão

Manage nuget packages -> coreRepositories, coreWebAPI, gateway, clients

Script para alterar o tipo das colunas: alter_table.sql

``` sql
select * from glab_validation where codetype <> 'Patient' and status = 'Sucess';

select * from glab_validation where validationgroupid = '1f0b0988-b80d-4ea6-a284-8448cb14aba2';

select * from glab_container where specimencollectionid = '1ECFA24F-092F-2BBA-E053-5C27FA0ACE79';

```
