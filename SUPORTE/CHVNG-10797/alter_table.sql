BEGIN
    ga_install_objects.execute_immediate(q'{

        DECLARE
            w_table_og      varchar2(100);
            w_table_bu      varchar2(100);
            w_table_exists  number;
            w_sql           long;

        BEGIN
            w_table_og   := 'GLAB_CONTAINER';
            w_table_bu   := 'GLAB_CONTAINER_BU';

            SELECT count(1)
            INTO w_table_exists
            FROM user_objects
            WHERE object_name = w_table_bu
            AND object_type = 'TABLE';

            IF w_table_exists <> '1' THEN

                w_sql := '
                    CREATE TABLE ' || w_table_bu || ' (
                        TENANTID varchar2(38),
                        SPECIMENCOLLECTIONID varchar2(38),
                        CONTAINERID varchar2(38),
                        COLLECTEDDATE TIMESTAMP WITH TIME ZONE,
                        PRINTDATE TIMESTAMP WITH TIME ZONE
                    )
                ';

                EXECUTE IMMEDIATE w_sql;

                w_sql := '
                    INSERT INTO ' || w_table_bu || '
                    (tenantid, specimencollectionid, containerid, collecteddate, printdate)
                    SELECT tenantid, specimencollectionid, containerid, collecteddate, printdate
                    FROM ' || w_table_og || '
                ';
                
                EXECUTE IMMEDIATE w_sql;

                w_sql := '
                    UPDATE ' || w_table_og || '
                    SET collecteddate = NULL
                ';

                EXECUTE IMMEDIATE w_sql;

                w_sql := '
                    UPDATE ' || w_table_og || '
                    SET printdate = NULL
                ';

                EXECUTE IMMEDIATE w_sql;

                w_sql := '
                    ALTER TABLE ' || w_table_og || '
                    MODIFY collecteddate TIMESTAMP WITH TIME ZONE
                ';

                EXECUTE IMMEDIATE w_sql;

                w_sql := '
                    ALTER TABLE ' || w_table_og || '
                    MODIFY printdate TIMESTAMP WITH TIME ZONE
                ';

                EXECUTE IMMEDIATE w_sql;

                w_sql := '
                    UPDATE ' || w_table_og || '
                    SET (collecteddate, printdate) = (
                        SELECT collecteddate, printdate
                        FROM ' || w_table_bu || '
                        WHERE ' || w_table_og || '.tenantid = ' || w_table_bu || '.tenantid
                        AND ' || w_table_og || '.specimencollectionid = ' || w_table_bu || '.specimencollectionid
                        AND ' || w_table_og || '.containerid = ' || w_table_bu || '.containerid
                    )
                    WHERE EXISTS (
                        SELECT tenantid, specimencollectionid, containerid
                        FROM ' || w_table_bu || '
                        WHERE ' || w_table_og || '.tenantid = ' || w_table_bu || '.tenantid
                        AND ' || w_table_og || '.specimencollectionid = ' || w_table_bu || '.specimencollectionid
                        AND ' || w_table_og || '.containerid = ' || w_table_bu || '.containerid
                    )
                ';

                EXECUTE IMMEDIATE w_sql;

                w_sql := '
                    DROP TABLE ' || w_table_bu || '
                ';

                EXECUTE IMMEDIATE w_sql;

            END IF;

            COMMIT;

        EXCEPTION
            WHEN OTHERS THEN
                ROLLBACK;
                RAISE;

        END;
        
    }');
END;
/