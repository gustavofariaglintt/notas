# GX-11316

## Jira

**Issue** [GX-11316](https://glinttdev.atlassian.net/browse/GX-11316)

## Observações do Agendamento

**Cliente** CHVNG

### Mensagem Check-In

GX-11316: filtro extra por id aquando da validação da label da transfusão

### Changesets

|       TFS      |   DEV    |    CI    |   17R1   |
|---------------:|:--------:|:--------:|:--------:|
| **Changesets** |  221370  |  221379  |  xxxxxx  |
| **Changesets** |  221434  |  221435  |  xxxxxx  |
|                |    --    |    --    |    --    |

### Backend Lógica

#### C\#

![Changeset](changeset.png "Changeset")

##### Repositories/CoreRepositories

###### Glintt.GPlatform.MS.CoreRepositories.Base/GLab/IGLabRepository.cs

```diff
  #region TRANSFUSIONUNIT
  Task<List<TransfusionUnit>> GetUnitsByTransfusionId(string transfusionId, int skip, int take);
  Task<List<TransfusionUnit>> GetUnitsByTransfusionUnitIds(List<string> transfusionUnitIds);
  Task<TransfusionUnit> GetTransfusionUnitByPrimaryKey(string transfusionId, string transfusionUnitId);
  Task<TransfusionUnit> GetTransfusionUnitByLabel(string codeTypeSystem, string codeType, string codeRead);
+ Task<TransfusionUnit> GetTransfusionUnitByLabelAndId(string codeTypeSystem, string codeType, string codeRead, string transfusionId);
  Task<List<TransfusionUnit>> GetPerformedUnitsByTransfusionId(List<string> transfusionId, int skip, int take);
  Task<TransfusionUnit> SetTransfusionUnitStatus(string transfusionUnitId, string statusTransfusionUnitId);
  Task<TransfusionUnit> UpdateTransfusionUnit(TransfusionUnit transfusionUnit);
```

###### Glintt.GPlatform.MS.CoreRepositories.GLab/TransfusionUnitRepository.cs

```diff
  public async Task<TransfusionUnit> GetTransfusionUnitByLabel(string codeTypeSystem, string codeType, string codeRead)
  {
      var transfusionUnit = await EFContext.GLAB_TRANSFUSIONUNIT.AsNoTracking()
          .Where(x => x.TENANTID == DbFunctions.AsNonUnicode(BaseCallData.TenantID) &&
                      x.LABEL == DbFunctions.AsNonUnicode(codeRead))
          .FirstOrDefaultAsync();

      return GLabRepositoryMapper.Instance.Map<TransfusionUnit>(transfusionUnit);
  }

+ public async Task<TransfusionUnit> GetTransfusionUnitByLabelAndId(string codeTypeSystem, string codeType, string codeRead, string transfusionId)
+ {
+     var transfusionUnit = await EFContext.GLAB_TRANSFUSIONUNIT.AsNoTracking()
+         .Where(x => x.TENANTID == DbFunctions.AsNonUnicode(BaseCallData.TenantID) &&
+                     x.LABEL == DbFunctions.AsNonUnicode(codeRead) &&
+                     x.TRANSFUSIONID == DbFunctions.AsNonUnicode(transfusionId))
+         .FirstOrDefaultAsync();

+     return GLabRepositoryMapper.Instance.Map<TransfusionUnit>(transfusionUnit);
+ }
```

###### Glintt.GPlatform.MS.CoreRepositories.GLab.UnitTests/UnitTest1.cs

```diff
  [TestMethod]
  public void GetTransfusionUnitByLabel()
  {
      using (var Repository = new GLabRepository())
      {
          Repository.InitializeRepository(BaseCallData).Wait();
          using (var task = Repository.GetTransfusionUnitByLabel("", "", "21101013210"))
          {
              task.Wait();
              Assert.IsNotNull(task.Result);
          }
      }
  }

+ [TestMethod]
+ public void GetTransfusionUnitByLabelAndId()
+ {
+     using (var Repository = new GLabRepository())
+     {
+         Repository.InitializeRepository(BaseCallData).Wait();
+         using (var task = Repository.GetTransfusionUnitByLabelAndId("", "", "21101013210", "420EEA542A4B2826E0531327FA0AE2FC"))
+         {
+             task.Wait();
+             Assert.IsNotNull(task.Result);
+         }
+     }
+ }
```

##### Services/CoreWebApi

###### Glintt.GPlatform.MS.CoreWebApi.GLab/Controllers/TransfusionUnitController.cs

```diff
  // GET api/transfusionunit/ValidateSelected?transfusionId={transfusionId}&transfusionUnitSelectedId={transfusionUnitSelectedId}&codeTypeSystem={codeTypeSystem}&codeType={codeType}&codeRead={codeRead}
  /// <summary>
  /// Validate if the codeReaded is the TransfusionUnitSelected or if is PatientLabel
  /// </summary>
  /// <param name="transfusionId"></param>
  /// <param name="transfusionUnitSelectedId"></param>
  /// <param name="codeType"></param>
  /// <param name="codeTypeSystem"></param>
  /// <param name="codeRead"></param>
  /// <returns></returns>
  [Route("ValidateSelected")]
  public async Task GetTransfusionUnitIsTheActive(string transfusionId, string transfusionUnitSelectedId, string codeTypeSystem, string codeType, string codeRead)
  {
      using (var Repository = BaseUnityContainer.Instance.Resolve<IGLabRepository>())
      {
          try
          {
              //como o metodo so lança exceções não fez sentido retornar booleanos
              await Repository.InitializeRepository(GetBaseCallData());

              Transfusion transfusion = await Repository.GetTransfusionById(transfusionId);
              if (transfusion == null) throw new ArgumentException(Resources.Resources.TransfusionNotFound);
              //se for a do doente tá tudo bem
              if (transfusion.PatientLabel == codeRead) return;

-             TransfusionUnit transfusionUnit = await Repository.GetTransfusionUnitByLabel(codeTypeSystem, codeType, codeRead);
+             TransfusionUnit transfusionUnit = await Repository.GetTransfusionUnitByLabelAndId(codeTypeSystem, codeType, codeRead, transfusionId);
              if (transfusionUnit == null) throw new ArgumentException(Resources.Resources.TransfusionUnitCodeDoesntMatchWithAnyUnit);
              //valida se é a unidade de transfusão picada 
              if (transfusionUnit.TransfusionUnitId != transfusionUnitSelectedId) throw new ArgumentException(Resources.Resources.TransfusionUnitNotMatchWithLabel);

          }
          catch (Exception e)
          {
              throw APIErrorManager.GetException(System.Net.HttpStatusCode.BadRequest, "", e.Message, e);
          }
      }
  }
```
