# HSEIT-3257

## Backend

### Glintths.Base.Internal

#### Glintths.Base.Internal.Interfaces

##### Local/ILocalConfigsRepository.cs

``` cs
#region gr_unid_saude
List<LocalHealthUnit> GetHealthUnits(OperationContext operationContext, string group, string destinationCode, string filter, List<string> deepProperties, int skip, int take); //<-- add parameter string destinationCode
```

### GlinttHS.API

#### Glintths.API.Internal.WS.Interfaces

##### Local/Configs/ILocalConfigsService.cs

``` cs
List<LocalHealthUnit> GetHealthUnits(SessionContext sessionContext, string group, string filter, string destinationCode, List<string> deepProperties, int skip, int take); // <-- string destinationCode,
```

#### Glintths.API.Workflows

##### Local/Configs/LocalConfigsRepositoryWF.cs

``` cs
public List<LocalHealthUnit> GetHealthUnits(SessionContext sessionContext, string group, string destinationCode, string filter, List<string> deepProperties, int skip, int take) // <-- string destinationCode,
{
    try
    {
        return _LocalConfigsRepository.GetHealthUnits(GetOperationContext(sessionContext), group, destinationCode, filter, deepProperties, skip, take); // <-- destinationCode,
    }
    catch (Exception ex)
    {
        Logging.LogExceptionToEventViewer(_sourceToLog, "Error on GetHealthUnits", ex, true);
        throw ExceptionsManager.TreatException(ex);
    }
}
```

#### GlinttHS.API.Internal.WS

##### Local/Configs/LocalConfigsService.svc.cs

``` cs
public List<LocalHealthUnit> GetHealthUnits(SessionContext sessionContext, string group, string filter, string destinationCode, List<string> deepProperties, int skip, int take) // <-- string destinationCode,
{
    try
    {
        return _localConfigsRepositoryWF.GetHealthUnits(sessionContext, group, filter, destinationCode, deepProperties, skip, take); // <-- destinationCode,
    }
    catch (Exception ex)
    {
        throw InternalWebServiceUtilities.ThrowGetAPIServiceException(ex, "Ocorreu um erro ao obter as unidades de saúde.");
    }
}
```

### GlinttHS.Services.Local.Configs

#### GlinttHS.Services.Local.Configs.DataModel

##### LocalConfigsDataModel.edmx

Actualizar DataModel:

+ Vai ser necessário fazer a ligação entre ```SDV_PROV_DEST.RV_LOW_VALUE``` (aka ```GR_PROV_DEST.COD_PROV_DEST``` (este valor é o ```destinationCode```)) e ```GR_UNID_SAUDE.T_U_SAUDE```
+ Internamento e Urgência fazem-no através de uma tabela intermédia: ```GR_UNID_SAUDE_DEST_INT``` e ```GR_UNID_SAUDE_DEST_URG```. Consulta Externa não tem uma tabela para esse propósito

#### GlinttHS.Services.Local.Configs.Repository

##### LocalConfigsRepositoryHealthUnit.cs

GetHealthUnits e GetHealthUnitsBase precisam de receber novo parâmetro ```string destinationCode```

Actualizar todas as referências a essas funções.

``` cs
private IQueryable<GR_UNID_SAUDE> GetHealthUnitsBase(ref string filter, ref string destinationCode, LocalConfigsDataEntities dbContext)
{
    IQueryable<GR_UNID_SAUDE> query = GetGR_UNID_SAUDEBaseQuery(dbContext);
    if (!string.IsNullOrEmpty(filter))
    {
        filter = filter.ToUpper();
        string finalSearch = StringUtilities.CleanStringName(filter);
        query = query.Where(x => x.DESC_U_SAUDE.ToUpper().Contains(EntityFunctions.AsNonUnicode(finalSearch)));
    }

    //+++++++
    if (!string.IsNullOrEmpty(destinationCode))
    {
        destinationCode = destinationCode.ToUpper();
        string destinationSearch = StringUtilities.CleanStringName(destinationCode);
        query = query.Where(x => ...); // fazer aqui o join das tabelas de forma a poder saber quais as unidades de saude associadas a um certo destino
    }

    //ativos:
    query = query.Where(q => q.FLG_ACTIVO == null || q.FLG_ACTIVO == EntityFunctions.AsNonUnicode("S"));
    return query;
}
```

## Frontend

### GeneralData.ts

Update Service Reference LocalConfigsServiceWS

``` ts
url: this.actions.GetHealthUnitIntURL,
data: {
    filter: request.term,
    destinationCode: this.destinationExternalConsultCombo.getComboSelectedValue(),  //<--
    skip: (page * 5),
    take: 5
},
```

### ClinicalDischarge_BaseController.cs

``` cs
public JsonResult GetHealthUnitInt(string filter, string destinationCode, int skip, int take) //<-- add parameter destinationCode
{
    try
    {
        BaseInformation bi = new BaseInformation()
        {
            CompanyDB = getCompany(),
            Username = getUserName()
        };

        string group = ConfigurationManager.AppSettings["FUNCTIONAL_MODE"];
        string[] deepProperties = null;

        LocalConfigsService localConfigsService = new LocalConfigsService();
        var result = localConfigsService.GetHealthUnits(bi, group, filter, destinationCode, deepProperties, skip, take); //<--
        return new JsonResult()
        {
            Data = new
            {
                result = result
            }
        };
    }
```
