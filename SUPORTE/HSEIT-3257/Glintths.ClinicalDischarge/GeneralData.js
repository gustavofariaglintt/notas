/// <reference path="../typings/index.d.ts" />
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "globalCare", "./Base", "./Prolongation"], function (require, exports, globalCare_1, Base_1, Prolongation_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var GeneralData = /** @class */ (function (_super) {
        __extends(GeneralData, _super);
        function GeneralData() {
            var _this = _super.call(this, "GeneralData") || this;
            _this.group = "CLINICALDISCHARGE_GENERALDATA";
            _this.formControllsData = {};
            _this.requestControllsList = [];
            _this.mandatoryControllsList = [];
            _this.options = { actionslist: [] };
            _this.pendingInfoList = [];
            _this.selectedPendingInfo = undefined;
            _this.canSave = false;
            _this.canSaveConsult = false;
            _this.canSaveUrg = false;
            _this.allergiesCheck = false;
            _this.cancelDischarge = function () {
                _this.hideControlls(true);
                _this.canSave = false;
                _this.setEditMode();
            };
            _this.openPIM = function () {
                globalCare_1.NavigationManager.PushView({
                    id: "PIM_Base",
                    application: "ClinicalProcess",
                    level: 4,
                    url: _this.actions.OpenPIMURL,
                    data: {}
                });
            };
            _this.activeSaveDischarge = function (active) {
                for (var i = 0; i < _this.options.actionslist.length; i++) {
                    if (_this.options.actionslist[i].id == "btn_save")
                        _this.options.actionslist[i].disable = !active;
                }
                globalCare_1.MenuManager.UpdateActionsBar(_this.options.actionslist, globalCare_1.EActionBar.ACTION_LIST);
            };
            _this.activeConfirmedDischarge = function (active) {
                for (var i = 0; i < _this.options.actionslist.length; i++) {
                    if (_this.options.actionslist[i].id == "btn_discharge")
                        _this.options.actionslist[i].disable = !active;
                }
                globalCare_1.MenuManager.UpdateActionsBar(_this.options.actionslist, globalCare_1.EActionBar.ACTION_LIST);
            };
            _this.activeCancelDischarge = function (active) {
                for (var i = 0; i < _this.options.actionslist.length; i++) {
                    if (_this.options.actionslist[i].id == "btn_cancel_discharge")
                        _this.options.actionslist[i].disable = !active;
                }
                globalCare_1.MenuManager.UpdateActionsBar(_this.options.actionslist, globalCare_1.EActionBar.ACTION_LIST);
            };
            _this.destinationChanged = function (e, elem, item) {
                if (item != undefined)
                    _this.setDestination(item);
                else
                    _this.setDestination(undefined);
                _this.changeDestination().then(function () {
                    _this.enableControllsButtons();
                });
            };
            _this.transferenceReasonChanged = function (evt, ui, element) {
                if (element != undefined)
                    _this.setTransferenceReason(element);
                else
                    _this.setTransferenceReason(undefined);
            };
            _this.transferenceReasonExtConsChanged = function (evt, ui, element) {
                if (element != undefined)
                    _this.setTransferenceReasonExtCons(element);
                else
                    _this.setTransferenceReasonExtCons(undefined);
            };
            _this.deathTypeChanged = function (evt, ui, element) {
                if (element != undefined)
                    _this.setDeathType(element);
                else
                    _this.setDeathType(undefined);
            };
            _this.selectHealthUnitInt = function (event, item) {
                if (!!item) {
                    _this.setHealthUnitInt(item);
                }
                else {
                    _this.setHealthUnitInt(undefined);
                }
            };
            _this.selectHealthUnitExtCons = function (event, item) {
                if (!!item) {
                    _this.setHealthUnitExtCons(item);
                }
                else {
                    _this.setHealthUnitExtCons(undefined);
                }
            };
            _this.setExitStatus = function (param) {
                Base_1.Base.getInstance().clinicalDischarge.ExitStatus = param;
                _this.setObjectState("MODIFIED");
            };
            _this.exitStatusChanged = function (evt, ui, element) {
                if (element != undefined)
                    _this.setExitStatus(element);
                else
                    _this.setExitStatus(undefined);
            };
            _this.selectExternalServiceInt = function (event, item) {
                if (!!item) {
                    _this.setExternalServiceInt(item);
                }
                else {
                    _this.setExternalServiceInt(undefined);
                }
            };
            _this.foreseenDateChanged = function (plug, val) {
                if (!!val) {
                    _this.setforeseenDate(val);
                }
                else {
                    _this.setforeseenDate(null);
                }
            };
            _this.setforeseenDate = function (param) {
                Base_1.Base.getInstance().clinicalDischarge.ForeseenDate = param;
            };
            _this.deathDateChanged = function (plug, val) {
                if (!!val) {
                    _this.setDeathDate(val, true);
                }
                else {
                    _this.setDeathDate(null, false);
                }
            };
            _this.dateChanged = function (plug, val) {
                if (!!val) {
                    _this.setDate(val, true);
                }
                else {
                    _this.setDate(null, false);
                }
            };
            _this.GeneralObservationsChanged = function (value) {
                _this.setGeneralObservations(value);
            };
            _this.PrescriptionReviewChanged = function (value) {
                _this.setPrescriptionReview(value);
                if (value == 'S')
                    _this.openPIM();
            };
            _this.MechanicalVentilationChanged = function (value) {
                _this.setMechanicalVentilation(value);
            };
            _this.NosocomialInfectionChanged = function (value) {
                _this.setNosocomialInfection(value);
            };
            _this.NosocomialInfectionAgentChanged = function (value) {
                _this.setNosocomialInfectionAgent(value);
            };
            _this.AllergiesChanged = function (value) {
                _this.setAllergies(value);
            };
            _this.NewInternmentChanged = function (value) {
                _this.setNewInternment(value);
            };
            _this.PatientRiskChanged = function (value) {
                _this.setPatientRisk(value);
            };
            _this.destinationExternalConsultChanged = function (evt, ui, element) {
                _this.canSaveConsult = true;
                if (element != undefined)
                    _this.setDestinationExternalConsult(element);
                else
                    _this.setDestinationExternalConsult(undefined);
                _this.changeDestination();
            };
            _this.selectServiceExternalConsultChanged = function (event, item) {
                if (item !== null) {
                    _this.setExternalServiceExternalConsult(item);
                }
                else {
                    _this.setExternalServiceExternalConsult(undefined);
                }
            };
            _this.PrevisionDaysChanged = function (value) {
                if (value < 0) {
                    value = 0;
                    _this.previsionDays.setValue(0, false);
                }
                _this.setPrevisionDays(value);
            };
            _this.setPrevisionDays = function (param) {
                Base_1.Base.getInstance().clinicalDischarge.PrevisionDays = param;
                _this.setObjectState("MODIFIED");
            };
            _this.selectService = function (event, item) {
                if (item !== null) {
                    _this.setService(item);
                }
                else {
                    _this.setService(undefined);
                }
            };
            _this.selectHealthUnit = function (event, item) {
                if (item !== null) {
                    _this.setHealthUnit(item);
                }
                else {
                    _this.setHealthUnit(undefined);
                }
            };
            _this.setService = function (param) {
                Base_1.Base.getInstance().clinicalDischarge.Service = param;
                if (!Base_1.Base.getInstance().clinicalDischarge.HasClinicalDischarge && Base_1.Base.getInstance().clinicalDischarge.BedProperties.Visible)
                    _this.requestBed();
                _this.setObjectState("MODIFIED");
            };
            _this.selectSpecialty = function (event, item) {
                if (item !== null) {
                    _this.setSpecialty(item);
                }
                else {
                    _this.setSpecialty(undefined);
                }
            };
            _this.selectExternalService = function (event, item) {
                if (item !== null) {
                    _this.setExternalService(item);
                }
                else {
                    _this.setExternalService(undefined);
                }
            };
            _this.setExternalService = function (param) {
                Base_1.Base.getInstance().clinicalDischarge.ExternalService = param;
                _this.setObjectState("MODIFIED");
            };
            _this.setHealthUnit = function (param) {
                Base_1.Base.getInstance().clinicalDischarge.HealthUnit = param;
                _this.setObjectState("MODIFIED");
            };
            _this.bedChanged = function (evt, ui, element) {
                if (element != undefined)
                    _this.setBed(element);
                else
                    _this.setBed(undefined);
            };
            _this.doctorChanged = function (evt, ui, element) {
                if (element != undefined)
                    _this.setDoctor(element);
                else
                    _this.setDoctor(undefined);
            };
            _this.ObservationsChanged = function (value) {
                _this.setObservations(value);
            };
            _this.setSpecialty = function (param) {
                Base_1.Base.getInstance().clinicalDischarge.Speciality = param;
                if (!Base_1.Base.getInstance().clinicalDischarge.HasClinicalDischarge && Base_1.Base.getInstance().clinicalDischarge.DoctorProperties.Visible)
                    _this.requestDoctor();
                _this.setObjectState("MODIFIED");
            };
            _this.setBed = function (param) {
                Base_1.Base.getInstance().clinicalDischarge.Bed = param;
                _this.setObjectState("MODIFIED");
            };
            _this.setDoctor = function (param) {
                Base_1.Base.getInstance().clinicalDischarge.Doctor = param;
                _this.setObjectState("MODIFIED");
            };
            _this.setObservations = function (param) {
                Base_1.Base.getInstance().clinicalDischarge.Observations = param;
                _this.setObjectState("MODIFIED");
            };
            _this.accompanimentChanged = function (evt, ui, element) {
                if (element != undefined)
                    _this.setAccompaniment(element);
                else
                    _this.setAccompaniment(undefined);
            };
            _this.setAccompaniment = function (param) {
                Base_1.Base.getInstance().clinicalDischarge.Accompaniment = param;
                _this.setObjectState("MODIFIED");
            };
            _this.openAllergies = function () {
                Base_1.Base.getInstance().openAllergies();
            };
            if (GeneralData._instance) {
                throw new Error("ERROR");
            }
            GeneralData._instance = _this;
            return _this;
        }
        GeneralData.getInstance = function () {
            return GeneralData._instance;
        };
        GeneralData.prototype.initialize = function (initdata) {
            this.actions = initdata.URLs;
            this.context = initdata.Data.context;
            this.group = "CLINICALDISCHARGE_GENERALDATA";
            this.canSave = false;
            this.canSaveConsult = false;
            this.canSaveUrg = false;
            this.allergiesCheck = false;
            this.loadControls();
            this.applyTranslate();
            this.options = { actionslist: [] };
            ;
        };
        GeneralData.prototype.loadControls = function () {
            this.initializeControlls();
        };
        GeneralData.prototype.initializeControlls = function () {
            var _this = this;
            if (this.context == "INT") {
                //#region DATE
                var dischargeDateTimeOptions = new globalCare_1.GlinttDatePickerOptions("DD-MM-YYYY HH:mm", "", "");
                dischargeDateTimeOptions.isMaxDateNow = true;
                dischargeDateTimeOptions.format = "L HH:mm";
                dischargeDateTimeOptions.showIcon = false;
                dischargeDateTimeOptions.placeholder = "dd/mm/aaaa hh:mm";
                dischargeDateTimeOptions.showCleanIcon = false;
                dischargeDateTimeOptions.includeTime = true;
                dischargeDateTimeOptions.includeDateFormatSelection = true;
                dischargeDateTimeOptions.theme = "MEDIUM";
                dischargeDateTimeOptions.onChange = this.dateChanged;
                this.dischargeDateTime = globalCare_1.ControlsManager.addGlinttDatePicker("#cd_form_date", this.group, dischargeDateTimeOptions);
                //#endregion
                //#region DESTINATION
                var destinationOptions = new globalCare_1.GlinttComboOptions("id_cd_form_destination", "name_cd_form_destination", "Meaning", "LowValue", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                destinationOptions.mode = "dropdown";
                destinationOptions.isFullComboBox = false;
                destinationOptions.theme = "MEDIUM";
                destinationOptions.onSelectionChanged = this.destinationChanged;
                destinationOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.destinationCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_destination", this.group, destinationOptions);
                //#endregion
                //#region EXIT STATUS
                var exitStatusOptions = new globalCare_1.GlinttComboOptions("id_cd_form_exitstatus", "name_cd_form_exitstatus", "Meaning", "LowValue", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                exitStatusOptions.mode = "dropdown";
                exitStatusOptions.isFullComboBox = false;
                exitStatusOptions.theme = "MEDIUM";
                exitStatusOptions.onSelectionChanged = this.exitStatusChanged;
                exitStatusOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.exitStatusCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_exitstatus", this.group, exitStatusOptions);
                //#endregion
                //#region TRANSFERENCE REASONS
                var transfReasonOptions = new globalCare_1.GlinttComboOptions("id_cd_form_transferencereasonint", "name_cd_form_transferencereasonint", "Meaning", "LowValue", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                transfReasonOptions.mode = "dropdown";
                transfReasonOptions.showDropDownButton = true;
                transfReasonOptions.isFullComboBox = false;
                transfReasonOptions.theme = "MEDIUM";
                transfReasonOptions.onSelectionChanged = this.transferenceReasonChanged;
                transfReasonOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.transfReasonCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_transferencereasonint", this.group, transfReasonOptions);
                //#endregion
                //#region HEALTH UNIT
                var transfHealthUnitOptions = new globalCare_1.GSearcherOptions(i18n.get("CLINICAL_DISCHARGE_SELECT"), "Description");
                transfHealthUnitOptions.minLength = 0;
                transfHealthUnitOptions.disableKeyboard = false;
                transfHealthUnitOptions.allowFilter = true;
                transfHealthUnitOptions.theme = "MEDIUM;";
                transfHealthUnitOptions.includeClearIcon = true;
                transfHealthUnitOptions.allowAutoSelect = true;
                transfHealthUnitOptions.select = this.selectHealthUnitInt;
                transfHealthUnitOptions.template = '<div class="cd-gsearcher-template">{{Description}}</div>';
                transfHealthUnitOptions.source = function (request, response, page, plugin) {
                    if (!!_this.dischargeDateTime.getValue()) {
                        _super.prototype.serviceRequest.call(_this, {
                            id: "CP_SEARCH_HEALTH_UNIT",
                            type: "POST",
                            url: _this.actions.GetHealthUnitURL,
                            data: {
                                filter: request.term,
                                skip: (page * 5),
                                take: 5,
                                destinyCode: _this.destinationCombo.getComboSelectedValue(),
                                externalServiceCode: Base_1.Base.getInstance().clinicalDischarge.ExternalService ? Base_1.Base.getInstance().clinicalDischarge.ExternalService.Code : null,
                                dischargeDate: _this.dischargeDateTime.getValue()
                            },
                            cancellable: false
                        }).then(function (data) {
                            response(data.result, 1, page++);
                        });
                    }
                    else {
                        response([], 0, 0);
                    }
                };
                this.transfHealthUnit = globalCare_1.ControlsManager.addGSearcher("#cd_form_healthunitint", this.group, transfHealthUnitOptions);
                //#endregion
                //#region EXTERNALSERVICEINT
                var externalServiceIntOptions = new globalCare_1.GSearcherOptions(i18n.get("CLINICAL_DISCHARGE_SELECT"), "Description");
                externalServiceIntOptions.minLength = 0;
                externalServiceIntOptions.disableKeyboard = false;
                externalServiceIntOptions.allowFilter = true;
                externalServiceIntOptions.theme = "MEDIUM";
                externalServiceIntOptions.includeClearIcon = true;
                externalServiceIntOptions.allowAutoSelect = true;
                externalServiceIntOptions.select = this.selectExternalServiceInt;
                externalServiceIntOptions.template = '<div class="cd-gsearcher-template">{{Description}}</div>';
                externalServiceIntOptions.source = function (request, response, page, plugin) {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "CP_SEARCH_EXTERNAL_SERVICE_INT",
                        type: "POST",
                        url: _this.actions.GetExternalServiceIntURL,
                        data: {
                            filter: request.term,
                            skip: (page * 5),
                            take: 5
                        },
                        cancellable: false
                    }).then(function (data) {
                        response(data.result, 1, page++);
                    });
                };
                this.externalServiceInt = globalCare_1.ControlsManager.addGSearcher("#cd_form_externalserviceint", this.group, externalServiceIntOptions);
                //#endregion
                //#region DEATH DATE
                var deathDateOptions = new globalCare_1.GlinttDatePickerOptions("DD-MM-YYYY HH:mm", "", "");
                deathDateOptions.isMaxDateNow = true;
                deathDateOptions.format = "L HH:mm";
                deathDateOptions.placeholder = "dd-mm-aaaa hh:mm";
                deathDateOptions.showIcon = false;
                deathDateOptions.showCleanIcon = false;
                deathDateOptions.includeTime = true;
                deathDateOptions.theme = "MEDIUM";
                deathDateOptions.onChange = this.deathDateChanged;
                this.deathDateTime = globalCare_1.ControlsManager.addGlinttDatePicker("#cd_form_deathdate", this.group, deathDateOptions);
                //#endregion
                //#region DEATH TYPE
                var deathTypeOptions = new globalCare_1.GlinttComboOptions("id_cd_form_deathtype", "name_cd_form_deathtype", "Meaning", "LowValue", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                deathTypeOptions.mode = "dropdown";
                deathTypeOptions.showDropDownButton = false;
                deathTypeOptions.dropDownOnFocus = true;
                deathTypeOptions.isFullComboBox = false;
                deathTypeOptions.theme = "MEDIUM";
                deathTypeOptions.onSelectionChanged = this.deathTypeChanged;
                deathTypeOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.deathTypeCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_deathtype", this.group, deathTypeOptions);
                //#endregion
                //#region PRESCRIPTION REVIEW
                var prescriptionOptions = new globalCare_1.GlinttRadioButtonOptions();
                prescriptionOptions.propertyId = "Code";
                prescriptionOptions.propertyLabel = "Description";
                prescriptionOptions.class = "g-radio";
                prescriptionOptions.dataSource = [
                    { 'Code': 'S', 'Description': i18n.get("CLINICAL_DISCHARGE_YES") },
                    { 'Code': 'N', 'Description': i18n.get("CLINICAL_DISCHARGE_NO") }
                ];
                prescriptionOptions.onSelectionChanged = function (event, checked, value) {
                    if (!!value) {
                        _this.PrescriptionReviewChanged(value);
                    }
                };
                this.prescription = globalCare_1.ControlsManager.addGlinttRadioButton("#cd_form_prescription", this.group, prescriptionOptions);
                //#endregion
                //#region MECHANICAL VENTILATION
                var mechanicalVentilationOptions = new globalCare_1.GlinttRadioButtonOptions();
                mechanicalVentilationOptions.propertyId = "Code";
                mechanicalVentilationOptions.propertyLabel = "Description";
                mechanicalVentilationOptions.class = "g-radio";
                mechanicalVentilationOptions.dataSource = [
                    { 'Code': 'S', 'Description': i18n.get("CLINICAL_DISCHARGE_YES") },
                    { 'Code': 'N', 'Description': i18n.get("CLINICAL_DISCHARGE_NO") }
                ];
                mechanicalVentilationOptions.onSelectionChanged = function (event, checked, value) {
                    if (!!value) {
                        _this.MechanicalVentilationChanged(value);
                    }
                };
                this.mechanicalVentilation = globalCare_1.ControlsManager.addGlinttRadioButton("#cd_form_mechanical_ventilation", this.group, mechanicalVentilationOptions);
                //#endregion
                //#region NOSOCOMIAL INFECTION
                var nosocomialInfectionOptions = new globalCare_1.GlinttRadioButtonOptions();
                nosocomialInfectionOptions.propertyId = "Code";
                nosocomialInfectionOptions.propertyLabel = "Description";
                nosocomialInfectionOptions.class = "g-radio";
                nosocomialInfectionOptions.dataSource = [
                    { 'Code': 'S', 'Description': i18n.get("CLINICAL_DISCHARGE_YES") },
                    { 'Code': 'N', 'Description': i18n.get("CLINICAL_DISCHARGE_NO") }
                ];
                nosocomialInfectionOptions.onSelectionChanged = function (event, checked, value) {
                    if (!!value) {
                        _this.NosocomialInfectionChanged(value);
                        if (value == "S") {
                            _this.nosocomialInfectionAgent.setIsEditable(true);
                        }
                        else if (value == "N") {
                            _this.setNosocomialInfectionAgent("");
                            _this.nosocomialInfectionAgent.setValue(Base_1.Base.getInstance().clinicalDischarge.NosocomialInfectionAgent);
                            _this.nosocomialInfectionAgent.setIsEditable(false);
                        }
                    }
                };
                this.nosocomialInfection = globalCare_1.ControlsManager.addGlinttRadioButton("#cd_form_nosocomial_infection", this.group, nosocomialInfectionOptions);
                var nosocomialInfectionAgentOptions = new globalCare_1.GlinttTextBoxOptions(i18n.get("CLINICAL_DISCHARGE_FORM_NOSOCOMIAL_INFECTION_AGENT_PLACEHOLDER"));
                nosocomialInfectionAgentOptions.isEditable = false;
                nosocomialInfectionAgentOptions.theme = "MEDIUM";
                nosocomialInfectionAgentOptions.addPlaceholder = true;
                nosocomialInfectionAgentOptions.onTextChanged = this.NosocomialInfectionAgentChanged;
                this.nosocomialInfectionAgent = globalCare_1.ControlsManager.addGlinttTextBox("#cd_form_nosocomial_infection_agent", this.group, nosocomialInfectionAgentOptions);
                //#endregion
                //#region ALLERGIES
                var allergiesOptions = new globalCare_1.GlinttRadioButtonOptions();
                allergiesOptions.propertyId = "Code";
                allergiesOptions.propertyLabel = "Description";
                allergiesOptions.class = "g-radio";
                allergiesOptions.dataSource = [
                    { 'Code': 'S', 'Description': i18n.get("CLINICAL_DISCHARGE_YES") },
                    { 'Code': 'N', 'Description': i18n.get("CLINICAL_DISCHARGE_NO") }
                ];
                allergiesOptions.onSelectionChanged = function (event, checked, value) {
                    if (!!value) {
                        if (value == "S") {
                            if (!_this.allergiesCheck) {
                                _this.allergiesCheck = true;
                                _this.openAllergies();
                            }
                        }
                        _this.AllergiesChanged(value);
                    }
                };
                this.allergies = globalCare_1.ControlsManager.addGlinttRadioButton("#cd_form_allergies", this.group, allergiesOptions);
                //#endregion
                //#region NEWINTERNMENT
                var newinternmentOptions = new globalCare_1.GlinttRadioButtonOptions();
                newinternmentOptions.propertyId = "Code";
                newinternmentOptions.propertyLabel = "Description";
                newinternmentOptions.class = "g-radio";
                newinternmentOptions.dataSource = [
                    { 'Code': 'S', 'Description': i18n.get("CLINICAL_DISCHARGE_YES") },
                    { 'Code': 'N', 'Description': i18n.get("CLINICAL_DISCHARGE_NO") }
                ];
                newinternmentOptions.onSelectionChanged = function (event, checked, value) {
                    if (!!value) {
                        _this.NewInternmentChanged(value);
                    }
                };
                this.newinternment = globalCare_1.ControlsManager.addGlinttRadioButton("#cd_form_newinternment", this.group, newinternmentOptions);
                //#endregion
                //#region RISK
                var patientRiskOptions = new globalCare_1.GlinttTextBoxOptions(i18n.get("CLINICAL_DISCHARGE_FORM_RISK_PLACEHOLDER"));
                patientRiskOptions.isEditable = true;
                patientRiskOptions.theme = "MEDIUM";
                patientRiskOptions.addPlaceholder = true;
                patientRiskOptions.onTextChanged = this.PatientRiskChanged;
                this.patientRisk = globalCare_1.ControlsManager.addGlinttTextBox("#cd_form_risk", this.group, patientRiskOptions);
                //#endregion
                //#region OBSERVATIONS
                var obsOptions = new globalCare_1.GlinttTextAreaOptions();
                obsOptions.theme = "MEDIUM";
                obsOptions.placeholder = i18n.get("CLINICAL_DISCHARGE_WRITE");
                obsOptions.onTextChanged = this.GeneralObservationsChanged;
                this.obsInput = globalCare_1.ControlsManager.addGlinttTextArea("#cd_form_generalobservations_input", this.group, obsOptions);
                //#endregion
            }
            if (this.context == "CONS_EXT") {
                //#region DATE
                var dischargeDateTimeOptions = new globalCare_1.GlinttDatePickerOptions("DD-MM-YYYY HH:mm", "", "");
                dischargeDateTimeOptions.isMaxDateNow = true;
                dischargeDateTimeOptions.format = "L HH:mm";
                dischargeDateTimeOptions.showIcon = false;
                dischargeDateTimeOptions.showCleanIcon = false;
                dischargeDateTimeOptions.includeTime = true;
                dischargeDateTimeOptions.includeDateFormatSelection = true;
                dischargeDateTimeOptions.placeholder = "dd/mm/aaaa hh:mm";
                dischargeDateTimeOptions.theme = "MEDIUM";
                dischargeDateTimeOptions.onChange = this.dateChanged;
                this.dischargeDateTime = globalCare_1.ControlsManager.addGlinttDatePicker("#cd_form_date", this.group, dischargeDateTimeOptions);
                //#endregion
                //#region DESTINATION-EXTERNAL-CONSULT
                var destinationExternalConsultOptions = new globalCare_1.GlinttComboOptions("id_cd_form_destinationexternalconsult", "name_cd_form_destinationexternalconsult", "Description", "Code", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                destinationExternalConsultOptions.mode = "dropdown";
                destinationExternalConsultOptions.isFullComboBox = false;
                destinationExternalConsultOptions.theme = "MEDIUM";
                destinationExternalConsultOptions.onSelectionChanged = this.destinationExternalConsultChanged;
                destinationExternalConsultOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.destinationExternalConsultCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_destinationexternalconsult", this.group, destinationExternalConsultOptions);
                //#endregion
                //#region TRANSFERENCE REASON EXTERNAL CONSULT
                var transferenceReasonExternalConsultOptions = new globalCare_1.GlinttComboOptions("id_cd_form_transferencereasonexternalconsult", "name_cd_form_transferencereasonexternalconsult", "Meaning", "LowValue", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                transferenceReasonExternalConsultOptions.mode = "dropdown";
                transferenceReasonExternalConsultOptions.isFullComboBox = false;
                transferenceReasonExternalConsultOptions.theme = "MEDIUM";
                transferenceReasonExternalConsultOptions.onSelectionChanged = this.transferenceReasonExtConsChanged;
                transferenceReasonExternalConsultOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.transfReasonExtConsCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_transferencereasonexternalconsult", this.group, transferenceReasonExternalConsultOptions);
                //#endregion
                //#region HEALTHUNIT EXTERNAL CONSULT
                var transfHealthUnitExtConsOptions = new globalCare_1.GSearcherOptions(i18n.get("CLINICAL_DISCHARGE_SELECT"), "Description");
                transfHealthUnitExtConsOptions.minLength = 0;
                transfHealthUnitExtConsOptions.disableKeyboard = false;
                transfHealthUnitExtConsOptions.allowFilter = true;
                transfHealthUnitExtConsOptions.theme = "MEDIUM;";
                transfHealthUnitExtConsOptions.includeClearIcon = true;
                transfHealthUnitExtConsOptions.allowAutoSelect = true;
                transfHealthUnitExtConsOptions.select = this.selectHealthUnitExtCons;
                transfHealthUnitExtConsOptions.template = '<div class="cd-gsearcher-template">{{Description}}</div>';
                transfHealthUnitExtConsOptions.source = function (request, response, page, plugin) {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "CP_SEARCH_HEALTH_UNIT",
                        type: "POST",
                        url: _this.actions.GetHealthUnitIntURL,
                        data: {
                            filter: request.term,
                            destinationCode: _this.destinationExternalConsultCombo.getComboSelectedValue(),
                            skip: (page * 5),
                            take: 5
                        },
                        cancellable: false
                    }).then(function (data) {
                        response(data.result, 1, page++);
                    });
                };
                this.transfHealthUnitExtCons = globalCare_1.ControlsManager.addGSearcher("#cd_form_healthunitexternalconsult", this.group, transfHealthUnitExtConsOptions);
                //#endregion
                //#region EXTERNALSERVICE EXTERNALCONSULT
                var externalServiceExternalConsultOptions = new globalCare_1.GSearcherOptions(i18n.get("CLINICAL_DISCHARGE_SELECT"), "Description");
                externalServiceExternalConsultOptions.minLength = 0;
                externalServiceExternalConsultOptions.disableKeyboard = false;
                externalServiceExternalConsultOptions.allowFilter = true;
                externalServiceExternalConsultOptions.theme = "MEDIUM";
                externalServiceExternalConsultOptions.includeClearIcon = true;
                externalServiceExternalConsultOptions.allowAutoSelect = true;
                externalServiceExternalConsultOptions.select = this.selectExternalServiceInt;
                externalServiceExternalConsultOptions.template = '<div class="cd-gsearcher-template">{{Description}}</div>';
                externalServiceExternalConsultOptions.source = function (request, response, page, plugin) {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "CP_SEARCH_EXTERNAL_SERVICE_INT",
                        type: "POST",
                        url: _this.actions.GetExternalServiceIntURL,
                        data: {
                            filter: request.term,
                            skip: (page * 5),
                            take: 5
                        },
                        cancellable: false
                    }).then(function (data) {
                        response(data.result, 1, page++);
                    });
                };
                externalServiceExternalConsultOptions.select = this.selectServiceExternalConsultChanged;
                this.externalServiceExternalConsult = globalCare_1.ControlsManager.addGSearcher("#cd_form_externalserviceexternalconsult", this.group, externalServiceExternalConsultOptions);
                //#endregion
            }
            if (this.context == "URG") {
                //#region DATE
                var dischargeDateTimeOptions = new globalCare_1.GlinttDatePickerOptions("DD-MM-YYYY HH:mm", "", "");
                dischargeDateTimeOptions.isMaxDateNow = true;
                dischargeDateTimeOptions.format = "L HH:mm";
                dischargeDateTimeOptions.showIcon = false;
                dischargeDateTimeOptions.showCleanIcon = false;
                dischargeDateTimeOptions.includeTime = true;
                dischargeDateTimeOptions.includeDateFormatSelection = true;
                dischargeDateTimeOptions.placeholder = "dd/mm/aaaa hh:mm";
                dischargeDateTimeOptions.theme = "MEDIUM";
                dischargeDateTimeOptions.onChange = this.dateChanged;
                this.dischargeDateTime = globalCare_1.ControlsManager.addGlinttDatePicker("#cd_form_date", this.group, dischargeDateTimeOptions);
                //#endregion
                //#region DESTINATION
                var destinationOptions = new globalCare_1.GlinttComboOptions("id_cd_form_destination", "name_cd_form_destination", "Meaning", "LowValue", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                destinationOptions.mode = "dropdown";
                destinationOptions.isFullComboBox = false;
                destinationOptions.theme = "MEDIUM";
                destinationOptions.onSelectionChanged = this.destinationChanged;
                destinationOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.destinationCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_destination", this.group, destinationOptions);
                //#endregion
                //#region EXIT STATUS
                var exitStatusOptions = new globalCare_1.GlinttComboOptions("id_cd_form_exitstatus", "name_cd_form_exitstatus", "Meaning", "LowValue", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                exitStatusOptions.mode = "dropdown";
                exitStatusOptions.isFullComboBox = false;
                exitStatusOptions.theme = "MEDIUM";
                exitStatusOptions.onSelectionChanged = this.exitStatusChanged;
                exitStatusOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.exitStatusCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_exitstatus", this.group, exitStatusOptions);
                //#endregion
                //#region DEATH DATE
                var deathDateOptions = new globalCare_1.GlinttDatePickerOptions("DD-MM-YYYY HH:mm", "", "");
                deathDateOptions.isMaxDateNow = true;
                deathDateOptions.format = "L HH:mm";
                deathDateOptions.placeholder = "dd-mm-aaaa hh:mm";
                deathDateOptions.showIcon = false;
                deathDateOptions.showCleanIcon = false;
                deathDateOptions.includeTime = true;
                deathDateOptions.theme = "MEDIUM";
                deathDateOptions.onChange = this.deathDateChanged;
                this.deathDateTime = globalCare_1.ControlsManager.addGlinttDatePicker("#cd_form_deathdate", this.group, deathDateOptions);
                //#endregion
                //#region DEATH TYPE
                var deathTypeOptions = new globalCare_1.GlinttComboOptions("id_cd_form_deathtype", "name_cd_form_deathtype", "Meaning", "LowValue", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                deathTypeOptions.mode = "dropdown";
                deathTypeOptions.showDropDownButton = false;
                deathTypeOptions.dropDownOnFocus = true;
                deathTypeOptions.isFullComboBox = false;
                deathTypeOptions.theme = "MEDIUM";
                deathTypeOptions.onSelectionChanged = this.deathTypeChanged;
                deathTypeOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.deathTypeCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_deathtype", this.group, deathTypeOptions);
                //#endregion
                //#region ForeseenDate
                var foreseenDateOptions = new globalCare_1.GlinttDatePickerOptions("DD-MM-YYYY", "", "");
                foreseenDateOptions.isMaxDateNow = true;
                foreseenDateOptions.format = "L";
                foreseenDateOptions.showIcon = false;
                foreseenDateOptions.placeholder = "dd/mm/aaaa";
                foreseenDateOptions.showCleanIcon = false;
                foreseenDateOptions.includeTime = false;
                foreseenDateOptions.includeDateFormatSelection = true;
                foreseenDateOptions.theme = "MEDIUM";
                foreseenDateOptions.value = CommonNS.getMomentDateNow().toDate();
                foreseenDateOptions.onChange = this.foreseenDateChanged;
                this.foreseenDate = globalCare_1.ControlsManager.addGlinttDatePicker("#cd_form_foreseenDate", this.group, foreseenDateOptions);
                //#endregion
                //#region Prevision Days
                var previsionDaysOptions = new globalCare_1.GlinttTextBoxOptions("");
                previsionDaysOptions.isEditable = true;
                previsionDaysOptions.theme = "MEDIUM";
                previsionDaysOptions.type = "number";
                previsionDaysOptions.onTextChanged = this.PrevisionDaysChanged;
                this.previsionDays = globalCare_1.ControlsManager.addGlinttTextBox("#cd_form_previsionDays", this.group, previsionDaysOptions);
                //#endregion
                //#region Service
                var serviceOptions = new globalCare_1.GSearcherOptions(i18n.get("CLINICAL_DISCHARGE_SELECT"), "Name");
                serviceOptions.minLength = 0;
                serviceOptions.disableKeyboard = false;
                serviceOptions.allowFilter = true;
                serviceOptions.theme = "MEDIUM;";
                serviceOptions.includeClearIcon = true;
                serviceOptions.allowAutoSelect = true;
                serviceOptions.select = this.selectService;
                serviceOptions.template = '<div class="cd-gsearcher-template">{{Name}}</div>';
                serviceOptions.source = function (request, response, page, plugin) {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "CP_SEARCH_SERVICE",
                        type: "POST",
                        url: _this.actions.GetServiceURL,
                        data: {
                            filter: request.term,
                            skip: (page * 5),
                            take: 5
                        },
                        cancellable: false
                    }).then(function (data) {
                        response(data.result, 1, page++);
                    });
                };
                this.service = globalCare_1.ControlsManager.addGSearcher("#cd_form_service", this.group, serviceOptions);
                //#endregion
                //#region Speciality
                var specialityOptions = new globalCare_1.GSearcherOptions(i18n.get("CLINICAL_DISCHARGE_SELECT"), "Name");
                specialityOptions.minLength = 0;
                specialityOptions.disableKeyboard = false;
                specialityOptions.allowFilter = true;
                specialityOptions.theme = "MEDIUM;";
                specialityOptions.includeClearIcon = true;
                specialityOptions.allowAutoSelect = true;
                specialityOptions.select = this.selectSpecialty;
                specialityOptions.template = '<div class="cd-gsearcher-template">{{Name}}</div>';
                specialityOptions.source = function (request, response, page, plugin) {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "CP_SEARCH_SPECIALTY",
                        type: "POST",
                        url: _this.actions.GetSpecialtyURL,
                        data: {
                            filter: request.term,
                            skip: (page * 5),
                            take: 5
                        },
                        cancellable: false
                    }).then(function (data) {
                        response(data.result, 1, page++);
                    });
                };
                this.speciality = globalCare_1.ControlsManager.addGSearcher("#cd_form_speciality", this.group, specialityOptions);
                //#endregion
                //#region Bed
                var bedOptions = new globalCare_1.GlinttComboOptions("id_cd_form_bed", "name_cd_form_bed", "Meaning", "LowValue", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                bedOptions.mode = "dropdown";
                bedOptions.isFullComboBox = false;
                bedOptions.theme = "MEDIUM";
                bedOptions.onSelectionChanged = this.bedChanged;
                bedOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.bedCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_bed", this.group, bedOptions);
                //#endregion
                //#region Doctor
                var doctorOptions = new globalCare_1.GlinttComboOptions("id_cd_form_doctor", "name_cd_form_doctor", "Name", "PersonnelNumber", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                doctorOptions.mode = "dropdown";
                doctorOptions.isFullComboBox = false;
                doctorOptions.theme = "MEDIUM";
                doctorOptions.onSelectionChanged = this.doctorChanged;
                doctorOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.doctorCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_doctor", this.group, doctorOptions);
                //#endregion
                //#region HealthUnitInt
                var transfHealthUnitOptions = new globalCare_1.GSearcherOptions(i18n.get("CLINICAL_DISCHARGE_SELECT"), "Description");
                transfHealthUnitOptions.minLength = 0;
                transfHealthUnitOptions.disableKeyboard = false;
                transfHealthUnitOptions.allowFilter = true;
                transfHealthUnitOptions.theme = "MEDIUM;";
                transfHealthUnitOptions.includeClearIcon = true;
                transfHealthUnitOptions.allowAutoSelect = true;
                transfHealthUnitOptions.select = this.selectHealthUnitInt;
                transfHealthUnitOptions.template = '<div class="cd-gsearcher-template">{{Description}}</div>';
                transfHealthUnitOptions.source = function (request, response, page, plugin) {
                    if (!!_this.dischargeDateTime.getValue()) {
                        _super.prototype.serviceRequest.call(_this, {
                            id: "CP_SEARCH_HEALTH_UNIT",
                            type: "POST",
                            url: _this.actions.GetHealthUnitURL,
                            data: {
                                filter: request.term,
                                skip: (page * 5),
                                take: 5,
                                destinyCode: _this.destinationCombo.getComboSelectedValue(),
                                externalServiceCode: Base_1.Base.getInstance().clinicalDischarge.ExternalService ? Base_1.Base.getInstance().clinicalDischarge.ExternalService.Code : null,
                                dischargeDate: _this.dischargeDateTime.getValue()
                            },
                            cancellable: false
                        }).then(function (data) {
                            response(data.result, 1, page++);
                        });
                    }
                    else {
                        response([], 0, 0);
                    }
                };
                this.transfHealthUnit = globalCare_1.ControlsManager.addGSearcher("#cd_form_healthunitint", this.group, transfHealthUnitOptions);
                //#endregion
                //#region HealthUnit
                var urgHealthUnitOptions = new globalCare_1.GSearcherOptions(i18n.get("CLINICAL_DISCHARGE_SELECT"), "Description");
                urgHealthUnitOptions.minLength = 0;
                urgHealthUnitOptions.disableKeyboard = false;
                urgHealthUnitOptions.allowFilter = true;
                urgHealthUnitOptions.theme = "MEDIUM;";
                urgHealthUnitOptions.includeClearIcon = true;
                urgHealthUnitOptions.allowAutoSelect = true;
                urgHealthUnitOptions.select = this.selectHealthUnit;
                urgHealthUnitOptions.template = '<div class="cd-gsearcher-template">{{Description}}</div>';
                urgHealthUnitOptions.source = function (request, response, page, plugin) {
                    if (!!_this.dischargeDateTime.getValue()) {
                        _super.prototype.serviceRequest.call(_this, {
                            id: "CP_SEARCH_HEALTH_UNIT",
                            type: "POST",
                            url: _this.actions.GetHealthUnitURL,
                            data: {
                                filter: request.term,
                                skip: (page * 5),
                                take: 5,
                                destinyCode: _this.destinationCombo.getComboSelectedValue(),
                                externalServiceCode: Base_1.Base.getInstance().clinicalDischarge.ExternalService ? Base_1.Base.getInstance().clinicalDischarge.ExternalService.Code : null,
                                dischargeDate: _this.dischargeDateTime.getValue()
                            },
                            cancellable: false
                        }).then(function (data) {
                            response(data.result, 1, page++);
                        });
                    }
                    else {
                        response([], 0, 0);
                    }
                };
                this.urgHealthUnit = globalCare_1.ControlsManager.addGSearcher("#cd_form_urg_healthunit", this.group, urgHealthUnitOptions);
                //#endregion
                //#region TransferenceReason
                var transfReasonOptions = new globalCare_1.GlinttComboOptions("id_cd_form_transferencereasonint", "name_cd_form_transferencereasonint", "Meaning", "LowValue", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                transfReasonOptions.mode = "dropdown";
                transfReasonOptions.isFullComboBox = false;
                transfReasonOptions.theme = "MEDIUM";
                transfReasonOptions.onSelectionChanged = this.transferenceReasonChanged;
                transfReasonOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.transfReasonCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_transferencereasonint", this.group, transfReasonOptions);
                ////#endregion
                //#region ExternalService
                var externalServiceOptions = new globalCare_1.GSearcherOptions(i18n.get("CLINICAL_DISCHARGE_SELECT"), "Description");
                externalServiceOptions.minLength = 0;
                externalServiceOptions.disableKeyboard = false;
                externalServiceOptions.allowFilter = true;
                externalServiceOptions.theme = "MEDIUM";
                externalServiceOptions.includeClearIcon = true;
                externalServiceOptions.allowAutoSelect = true;
                externalServiceOptions.select = this.selectExternalService;
                externalServiceOptions.template = '<div class="cd-gsearcher-template">{{Description}}</div>';
                externalServiceOptions.source = function (request, response, page, plugin) {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "CP_SEARCH_EXTERNAL_SERVICE",
                        type: "POST",
                        url: _this.actions.GetExternalServiceIntURL,
                        data: {
                            filter: request.term,
                            skip: (page * 5),
                            take: 5
                        },
                        cancellable: false
                    }).then(function (data) {
                        response(data.result, 1, page++);
                    });
                };
                this.externalService = globalCare_1.ControlsManager.addGSearcher("#cd_form_externalservice", this.group, externalServiceOptions);
                //#endregion
                //#region Accompaniment
                var accompanimentOptions = new globalCare_1.GlinttComboOptions("id_cd_form_accompaniment", "name_cd_form_accompaniment", "Meaning", "LowValue", i18n.get("CLINICAL_DISCHARGE_SELECT"));
                accompanimentOptions.mode = "dropdown";
                accompanimentOptions.isFullComboBox = false;
                accompanimentOptions.theme = "MEDIUM";
                accompanimentOptions.onSelectionChanged = this.accompanimentChanged;
                accompanimentOptions.noMatchFoundText = i18n.get("NO_DATA_FOUND");
                this.accompanimentCombo = globalCare_1.ControlsManager.addGlinttCombo("#cd_form_accompaniment", this.group, accompanimentOptions);
                //#endregion
                //#region OBSERVATIONS
                var obsOptions = new globalCare_1.GlinttTextAreaOptions();
                obsOptions.theme = "MEDIUM";
                obsOptions.placeholder = i18n.get("CLINICAL_DISCHARGE_WRITE");
                obsOptions.onTextChanged = this.GeneralObservationsChanged;
                this.obsInput = globalCare_1.ControlsManager.addGlinttTextArea("#cd_form_generalobservations_input", this.group, obsOptions);
                //#endregion
            }
        };
        GeneralData.prototype.applyTranslate = function () {
            i18n.applyResources("#general_data_container");
        };
        GeneralData.prototype.loadInitialData = function () {
            this.setGeneralData();
        };
        GeneralData.prototype.initGeneralData = function (requestControllsList) {
            var _this = this;
            if (!!Base_1.Base.getInstance().clinicalDischarge.Date) {
                this.dischargeDateTime.setValue({ date: Base_1.Base.getInstance().clinicalDischarge.Date, doFireEvent: false });
                this.setDate(Base_1.Base.getInstance().clinicalDischarge.Date, true);
            }
            else {
                this.dischargeDateTime.setValue({ date: CommonNS.getMomentDateNow().toDate(), doFireEvent: false });
                this.setDate(CommonNS.getMomentDateNow().toDate(), true);
            }
            if (requestControllsList && requestControllsList.length && this.context == "INT") {
                for (var i = 0; i < requestControllsList.length; i++) {
                    switch (requestControllsList[i]) {
                        case "ExitStatus":
                            $("#div_cd_form_exitstatus").removeClass('hidden');
                            this.requestExitStatus();
                            break;
                        case "Destination":
                            $("#div_destination_int").removeClass('hidden');
                            this.requestDestination();
                            break;
                        case "TransferenceReasonInt":
                            $("#div_cd_form_transferencereasonint").removeClass('hidden');
                            $("#block").removeClass('hidden');
                            $("#cd_form_block1").removeClass('hidden');
                            this.canSave = true;
                            this.requestTransferenceReason();
                            break;
                        case "HealthUnitInt":
                            this.transfHealthUnit.setSelectedItem(Base_1.Base.getInstance().clinicalDischarge.HealthUnitInt, null);
                            $("#div_cd_form_health_unit").removeClass('hidden');
                            break;
                        case "ExternalServiceInt":
                            this.externalServiceInt.setSelectedItem(Base_1.Base.getInstance().clinicalDischarge.ExternalServiceInt, null);
                            $("#div_cd_form_external_service").removeClass('hidden');
                            break;
                        case "DeathDate":
                            $("#div_deathdate").removeClass('hidden');
                            this.deathDateTime.setValue({ date: CommonNS.getMomentDateNow().toDate(), doFireEvent: false });
                            if (requestControllsList.indexOf("TransferenceReasonInt") == -1)
                                $("#div_cd_form_transferencereasonint").addClass('hidden');
                            else
                                $("#div_cd_form_transferencereasonint").removeClass('hidden');
                            //$("#div_cd_form_exitstatus").addClass('hidden');
                            $("#div_cd_form_health_unit").addClass('hidden');
                            $("#div_cd_form_external_service").addClass('hidden');
                            $("#block").removeClass('hidden');
                            $("#cd_form_block1").removeClass('hidden');
                            $("#cd_form_block_observations").addClass("hidden");
                            $("#btn_cancel").removeClass('hidden');
                            $("#btn_confirm").removeClass('hidden');
                            this.canSave = false;
                            globalCare_1.EventManager.addEvent($('#btn_confirm'), 'click', this.group, function () {
                                if (_this.canSave === false) {
                                    _this.canSave = true;
                                }
                                $("#btn_confirm").attr("disabled", "true");
                                $("#btn_confirm").css("background", '');
                                _this.deathDateTime.setEditable(false);
                                _this.deathTypeCombo.SetComboEnabled(false);
                            });
                            //Evento botao cancelar
                            globalCare_1.EventManager.addEvent($('#btn_cancel'), 'click', this.group, function () {
                                _this.setDeathDate(null, false);
                                //$('#cd_form_confirm_death_input').data('GlinttCheckBox').setValue(true, true)
                                //Limpar combo e permitir todos os destinos
                                if (Base_1.Base.getInstance().clinicalDischarge.DestinationProperties.Visible == true) {
                                    $('#cd_form_destination').data('GlinttCombo').clearSelection();
                                    $('#cd_form_destination').data('GlinttCombo').setDataSource(_this.formControllsData.Destination);
                                }
                                else if (Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsultProperties.Visible == true) {
                                    $('#cd_form_destinationexternalconsult').data('GlinttCombo').clearSelection();
                                }
                                _this.setDestination(undefined);
                                //Recarregar formulario apos mudar destino
                                _this.canSave = false;
                                _this.changeDestination();
                                $("#btn_cancel").addClass('hidden');
                                $("#btn_confirm").addClass('hidden');
                                $("#div_deathdate").addClass('hidden');
                                $("#div_deathtype").addClass('hidden');
                                $("#div_cd_form_exitstatus").removeClass('hidden');
                                $("#btn_confirm").removeAttr("disabled");
                                $("#btn_confirm").css("background", "#BEDB9C");
                            });
                            break;
                        case "DeathType":
                            $("#div_deathtype").removeClass('hidden');
                            this.requestDeathType();
                            break;
                        case "PrescriptionReview":
                            $("#cd_form_block_prescription_review").removeClass("hidden");
                            this.prescription.setCheckedById(Base_1.Base.getInstance().clinicalDischarge.PrescriptionReview, false);
                            break;
                        case "MechanicalVentilation":
                            $("#cd_form_block_mechanical_ventilation").removeClass("hidden");
                            this.mechanicalVentilation.setCheckedById(Base_1.Base.getInstance().clinicalDischarge.MechanicalVentilation, false);
                            break;
                        case "NosocomialInfection":
                            $("#cd_form_block_nosocomial_infection").removeClass("hidden");
                            this.nosocomialInfection.setCheckedById(Base_1.Base.getInstance().clinicalDischarge.NosocomialInfection, false);
                            if (Base_1.Base.getInstance().mode == "EDIT") {
                                if (Base_1.Base.getInstance().clinicalDischarge.NosocomialInfection == "S") {
                                    this.nosocomialInfectionAgent.setIsEditable(true);
                                }
                                else {
                                    this.nosocomialInfectionAgent.setIsEditable(false);
                                }
                            }
                            this.nosocomialInfectionAgent.setValue(Base_1.Base.getInstance().clinicalDischarge.NosocomialInfectionAgent);
                            break;
                        case "Allergies":
                            $("#cd_form_block_allergies").removeClass("hidden");
                            if (Base_1.Base.getInstance().clinicalDischarge.Allergies == 'S') {
                                this.allergiesCheck = true;
                            }
                            this.allergies.setCheckedById(Base_1.Base.getInstance().clinicalDischarge.Allergies, false);
                            break;
                        case "NewInternment":
                            $("#cd_form_block_newinternment").removeClass("hidden");
                            this.newinternment.setCheckedById(Base_1.Base.getInstance().clinicalDischarge.NewInternment, false);
                            break;
                        case "PatientRisk":
                            $("#cd_form_block_risk").removeClass("hidden");
                            this.patientRisk.setValue(Base_1.Base.getInstance().clinicalDischarge.PatientRisk);
                            break;
                        case "GeneralObservations":
                            $("#cd_form_block_observations").removeClass("hidden");
                            this.obsInput.setValue(Base_1.Base.getInstance().clinicalDischarge.GeneralObservations);
                            break;
                    }
                }
            }
            if (requestControllsList && requestControllsList.length && this.context == "CONS_EXT") {
                for (var i = 0; i < requestControllsList.length; i++) {
                    switch (requestControllsList[i]) {
                        case "DestinationExternalConsult":
                            $("#div_destination_externalCons").removeClass('hidden');
                            this.requestDestination();
                            //this.canSaveConsult = true; HSEIT-3715
                            break;
                        case "TransferenceReasonExternalConsult":
                            this.requestTransferenceReasonExternalConsult();
                            $("#block").removeClass('hidden');
                            $("#cd_form_block1").removeClass('hidden');
                            $("#div_cd_form_transferencereasonconsult").removeClass('hidden');
                            break;
                        case "ExternalServiceExternalConsult":
                            this.externalServiceExternalConsult.setSelectedItem(Base_1.Base.getInstance().clinicalDischarge.ExternalServiceExternalConsult, null);
                            $("#div_cd_form_external_service_consult").removeClass('hidden');
                            break;
                        case "HealthUnitExternalConsult":
                            this.transfHealthUnitExtCons.setSelectedItem(Base_1.Base.getInstance().clinicalDischarge.HealthUnitExternalConsult, null);
                            $("#div_cd_form_health_unit_consult").removeClass('hidden');
                            break;
                    }
                }
            }
            if (requestControllsList && requestControllsList.length && this.context == "URG") {
                for (var i = 0; i < requestControllsList.length; i++) {
                    switch (requestControllsList[i]) {
                        case "DeathDate":
                            $("#div_deathdate").removeClass('hidden');
                            this.deathDateTime.setValue({ date: CommonNS.getMomentDateNow().toDate(), doFireEvent: false });
                            $("#cd_form_block1").removeClass('hidden');
                            $("#block").removeClass('hidden');
                            $("#cd_form_block_observations").addClass("hidden");
                            $("#btn_cancel").removeClass('hidden');
                            $("#btn_confirm").removeClass('hidden');
                            this.canSaveUrg = false;
                            globalCare_1.EventManager.addEvent($('#btn_confirm'), 'click', this.group, function () {
                                if (_this.canSaveUrg === false) {
                                    _this.canSaveUrg = true;
                                }
                                $("#btn_confirm").attr("disabled", "true");
                                $("#btn_confirm").css("background", '');
                                _this.deathDateTime.setEditable(false);
                                _this.deathTypeCombo.SetComboEnabled(false);
                            });
                            //Evento botao cancelar
                            globalCare_1.EventManager.addEvent($('#btn_cancel'), 'click', this.group, function () {
                                _this.setDeathDate(null, false);
                                //Limpar combo e permitir todos os destinos
                                if (Base_1.Base.getInstance().clinicalDischarge.DestinationProperties.Visible == true) {
                                    $('#cd_form_destination').data('GlinttCombo').clearSelection();
                                    $('#cd_form_destination').data('GlinttCombo').setDataSource(_this.formControllsData.Destination);
                                }
                                else if (Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsultProperties.Visible == true) {
                                    $('#cd_form_destinationexternalconsult').data('GlinttCombo').clearSelection();
                                }
                                _this.setDestination(undefined);
                                //Recarregar formulario apos mudar destino
                                _this.canSaveUrg = false;
                                _this.changeDestination();
                                $("#btn_cancel").addClass('hidden');
                                $("#btn_confirm").addClass('hidden');
                                $("#div_deathdate").addClass('hidden');
                                $("#div_deathtype").addClass('hidden');
                                $("#div_cd_form_exitstatus").removeClass('hidden');
                                $("#btn_confirm").removeAttr("disabled");
                                _this.deathDateTime.setEditable(true);
                                _this.deathTypeCombo.SetComboEnabled(true);
                                $("#btn_confirm").css("background", "#BEDB9C");
                            });
                            break;
                        case "ExitStatus":
                            $("#div_cd_form_exitstatus").removeClass('hidden');
                            this.requestExitStatus();
                            break;
                        case "Destination":
                            $("#div_destination_int").removeClass('hidden');
                            this.requestDestination();
                            this.canSaveUrg = true;
                            $("#block").removeClass("hidden");
                            break;
                        case "DeathType":
                            $("#div_deathtype").removeClass('hidden');
                            this.requestDeathType();
                            break;
                        case "ForeseenDate":
                            $("#cd_form_block1").removeClass('hidden');
                            $("#block").removeClass('hidden');
                            $("#div_foreseenDate").removeClass('hidden');
                            this.foreseenDate.setValue({ date: Base_1.Base.getInstance().clinicalDischarge.ForeseenDate, doFireEvent: false });
                            break;
                        case "PrevisionDays":
                            $("#div_previsionDays").removeClass('hidden');
                            this.previsionDays.setValue(Base_1.Base.getInstance().clinicalDischarge.PrevisionDays, false);
                            break;
                        case "Service":
                            $("#div_cd_form_service").removeClass('hidden');
                            this.service.setSelectedItem(Base_1.Base.getInstance().clinicalDischarge.Service, false);
                            break;
                        case "Speciality":
                            $("#div_cd_form_speciality").removeClass('hidden');
                            this.speciality.setSelectedItem(Base_1.Base.getInstance().clinicalDischarge.Speciality, false);
                            break;
                        case "Bed":
                            $("#cd_form_block1").removeClass('hidden');
                            $("#block").removeClass('hidden');
                            $("#div_cd_form_bed").removeClass('hidden');
                            this.requestBed();
                            break;
                        case "Doctor":
                            $("#div_cd_form_doctor").removeClass('hidden');
                            this.requestDoctor();
                            break;
                        case "HealthUnitInt":
                            $("#div_cd_form_health_unit").removeClass('hidden');
                            break;
                        case "HealthUnit":
                            this.urgHealthUnit.setSelectedItem(Base_1.Base.getInstance().clinicalDischarge.HealthUnit, null);
                            $("#div_cd_form_urg_healthunit").removeClass('hidden');
                            break;
                        case "TransferenceReason":
                            $("#div_cd_form_transferencereasonint").removeClass('hidden');
                            this.requestTransferenceReason();
                            break;
                        case "ExternalService":
                            $("#div_cd_form_externalservice").removeClass('hidden');
                            break;
                        case "Accompaniment":
                            $("#div_cd_form_accompaniment").removeClass('hidden');
                            this.requestAccompaniment();
                            break;
                        case "GeneralObservations":
                            $("#cd_form_block_observations").removeClass("hidden");
                            this.obsInput.setValue(Base_1.Base.getInstance().clinicalDischarge.GeneralObservations);
                            break;
                    }
                }
            }
        };
        GeneralData.prototype.setGeneralData = function () {
            //formatar data
            if (Base_1.Base.getInstance().clinicalDischarge.EpisodeStartDate != null && moment(Base_1.Base.getInstance().clinicalDischarge.EpisodeStartDate).isValid() == true) {
                Base_1.Base.getInstance().clinicalDischarge.EpisodeStartDate = moment(Base_1.Base.getInstance().clinicalDischarge.EpisodeStartDate).toDate();
            }
            this.renderGeneralDataForm();
        };
        GeneralData.prototype.requestDestination = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                globalCare_1.ProcessingManager.start({ id: 'EPR_CLINICALDISCHARGE_REQUESTDESTINATION' });
                if (_this.formControllsData.Destination != undefined || _this.formControllsData.DestinationExternalConsult != undefined) {
                    if (_this.context === "CONS_EXT") {
                        if (Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsult != null && (Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsult.Code == "O" || Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsult.Code == "OC")) {
                            _this.destinationExternalConsultCombo.setDataSource(_this.formControllsData.DestinationExternalConsultObito);
                        }
                        else {
                            _this.destinationExternalConsultCombo.setDataSource(_this.formControllsData.DestinationExternalConsult);
                        }
                    }
                    else {
                        if (Base_1.Base.getInstance().clinicalDischarge.Destination != null && (Base_1.Base.getInstance().clinicalDischarge.Destination.Type == "O" || Base_1.Base.getInstance().clinicalDischarge.Destination.Type == "OC")) {
                            _this.destinationCombo.setDataSource(_this.formControllsData.DestinationObito);
                        }
                        else {
                            _this.destinationCombo.setDataSource(_this.formControllsData.Destination);
                        }
                    }
                    _this.fillDestinationCombo();
                    globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_REQUESTDESTINATION' });
                    resolve();
                }
                else {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "CD_REQUEST_DESTINATION",
                        type: "POST",
                        url: _this.actions.GetDestinationURL,
                        cancellable: false
                    }).then(function (data) {
                        if (Base_1.Base.getInstance().ajaxIsOnError(data, "CD_REQUEST_DESTINATION") == true)
                            return;
                        if (_this.context == "CONS_EXT") {
                            _this.formControllsData.DestinationExternalConsultObito = [];
                            for (var i = 0; i < data.result.length; i++) {
                                if (data.result[i].Code == "O" || data.result[i].Code == "OC") {
                                    _this.formControllsData.DestinationExternalConsultObito.push(data.result[i]);
                                }
                            }
                            _this.formControllsData.DestinationExternalConsult = data.result;
                            if (Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsult != null && (Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsult.Code == "O" || Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsult.Code == "OC")) {
                                _this.destinationExternalConsultCombo.setDataSource(_this.formControllsData.DestinationExternalConsultObito);
                            }
                            else {
                                _this.destinationExternalConsultCombo.setDataSource(_this.formControllsData.DestinationExternalConsult);
                            }
                        }
                        else {
                            _this.formControllsData.DestinationObito = [];
                            for (var i = 0; i < data.result.length; i++) {
                                if (data.result[i].Type == "O" || data.result[i].Type == "OC") {
                                    _this.formControllsData.DestinationObito.push(data.result[i]);
                                }
                            }
                            _this.formControllsData.Destination = data.result;
                            if (Base_1.Base.getInstance().clinicalDischarge.Destination != null && (Base_1.Base.getInstance().clinicalDischarge.Destination.Type == "O" || Base_1.Base.getInstance().clinicalDischarge.Destination.Type == "OC")) {
                                _this.destinationCombo.setDataSource(_this.formControllsData.DestinationObito);
                            }
                            else {
                                _this.destinationCombo.setDataSource(_this.formControllsData.Destination);
                            }
                        }
                        _this.fillDestinationCombo();
                        resolve();
                        globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_REQUESTDESTINATION' });
                    });
                }
            });
        };
        GeneralData.prototype.fillDestinationCombo = function () {
            if (this.context === "CONS_EXT") {
                if (Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsult != null) {
                    this.destinationExternalConsultCombo.setSelectedByCode(Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsult, false);
                }
                else {
                    this.destinationExternalConsultCombo.clearSelection();
                }
            }
            else {
                if (Base_1.Base.getInstance().clinicalDischarge.Destination != null) {
                    this.destinationCombo.setSelectedByCode(Base_1.Base.getInstance().clinicalDischarge.Destination, false);
                }
                else {
                    this.destinationCombo.clearSelection();
                }
            }
        };
        GeneralData.prototype.requestExitStatus = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                globalCare_1.ProcessingManager.start({ id: 'EPR_CLINICALDISCHARGE_REQUESTEXITSTATUS' });
                if (_this.formControllsData.ExitStatus != undefined) {
                    _this.exitStatusCombo.setDataSource(_this.formControllsData.ExitStatus);
                    _this.fillExitStatus();
                    resolve();
                    globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_REQUESTEXITSTATUS' });
                }
                else {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "CD_REQUEST_EXIT_STATUS",
                        type: "POST",
                        url: _this.actions.GetExitStatusURL,
                        cancellable: false
                    }).then(function (data) {
                        _this.formControllsData.ExitStatus = data.result;
                        _this.exitStatusCombo.setDataSource(_this.formControllsData.ExitStatus);
                        _this.fillExitStatus();
                        globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_REQUESTEXITSTATUS' });
                        resolve();
                    });
                }
            });
        };
        GeneralData.prototype.fillExitStatus = function () {
            if (Base_1.Base.getInstance().clinicalDischarge.ExitStatus != null) {
                this.exitStatusCombo.setSelectedByCode(Base_1.Base.getInstance().clinicalDischarge.ExitStatus, false);
            }
            else {
                this.exitStatusCombo.clearSelection();
            }
        };
        GeneralData.prototype.checkEpisodeCodif = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                if (Base_1.Base.getInstance().clinicalDischarge.IsEpisodeCodif == true) {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "CD_CHECK_EPISODE_COFIG",
                        type: "POST",
                        url: _this.actions.CheckEpisodeCodifURL,
                        data: {
                            clinicalDischarge: Base_1.Base.getInstance().clinicalDischarge
                        },
                        cancellable: false
                    }).then(function (data) {
                        //Permite realizar a alta clinica mas podendo alterar apenas o estado de sa�da;
                        //Desabilita todos os campos excepto: data e hora, estdo de saida e obs;
                        if (data.result == "I") {
                            _this.allowOnlyExitStatus();
                        }
                        //Desabilita tudo e n�o permite realizar a alta clinica
                        else if (data.result == "N") {
                            //this.blockClinicalDischarge();
                        }
                        else {
                            if (data.msg != undefined && data.msg != "") {
                                var options = {
                                    AlarmisticType: globalCare_1.AlarmisticType.ERROR_ALARMISTIC,
                                    Description: data.msg,
                                    Title: i18n.get("EPR_DIALOG_ERROR")
                                };
                                globalCare_1.AlarmisticManager.displayAlarmistic(options);
                            }
                            else {
                                var options = {
                                    AlarmisticType: globalCare_1.AlarmisticType.ERROR_ALARMISTIC,
                                    Description: i18n.get("CLINICAL_DISCHARGE_ERROR_CHECK_ENCODED_EPISODE"),
                                    Title: i18n.get("EPR_DIALOG_ERROR")
                                };
                                globalCare_1.AlarmisticManager.displayAlarmistic(options);
                            }
                            return;
                        }
                        if (data.msg != undefined && data.msg != "") {
                            var options = {
                                AlarmisticType: globalCare_1.AlarmisticType.ERROR_ALARMISTIC,
                                Description: data.msg,
                                Title: i18n.get("Eresults_Dialog_Warning")
                            };
                            globalCare_1.AlarmisticManager.displayAlarmistic(options);
                        }
                        resolve();
                    });
                }
            });
        };
        GeneralData.prototype.changeDestination = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _super.prototype.serviceRequest.call(_this, {
                    id: "CD_REQUEST_CHANGE_DESTINATION",
                    type: "POST",
                    url: _this.actions.SetClinicalDischargeFieldPropertiesURL,
                    data: {
                        clinicalDischarge: Base_1.Base.getInstance().clinicalDischarge
                    },
                    cancellable: false
                }).then(function (data) {
                    Base_1.Base.getInstance().clinicalDischarge = data.Result;
                    if (_this.canSave == false)
                        _this.hideControlls(true);
                    else
                        _this.hideControlls(false);
                    _this.refreshForm();
                    Base_1.Base.getInstance().loadPendingInfoData();
                    resolve();
                });
            });
        };
        GeneralData.prototype.requestDeathType = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                globalCare_1.ProcessingManager.start({ id: 'EPR_CLINICALDISCHARGE_REQUESTDEATHTYPE' });
                if (_this.formControllsData.DeathType != undefined) {
                    _this.deathTypeCombo.setDataSource(_this.formControllsData.DeathType);
                    _this.fillDeathTypeCombo();
                    globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_REQUESTDEATHTYPE' });
                    resolve();
                }
                else {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "EPR_CLINICALDISCHARGE_REQUESTDEATHTYPE",
                        type: "POST",
                        url: _this.actions.GetDeathTypeURL,
                        cancellable: false
                    }).then(function (data) {
                        _this.formControllsData.DeathType = data.result;
                        if (_this.deathTypeCombo) {
                            _this.deathTypeCombo.setDataSource(_this.formControllsData.DeathType);
                        }
                        _this.fillDeathTypeCombo();
                        globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_REQUESTDEATHTYPE' });
                        resolve();
                    });
                }
            });
        };
        GeneralData.prototype.fillDeathTypeCombo = function () {
            if (Base_1.Base.getInstance().clinicalDischarge.DeathType != null) {
                this.deathTypeCombo.setSelectedByCode(Base_1.Base.getInstance().clinicalDischarge.DeathType, false);
            }
            else {
                this.deathTypeCombo.clearSelection();
            }
        };
        GeneralData.prototype.destroy = function () {
            globalCare_1.ControlsManager.destroy(this.group);
            globalCare_1.MenuManager.DestroyActionsBar(globalCare_1.EActionBar.ACTION_LIST);
            globalCare_1.MenuManager.DestroyActionsBar(globalCare_1.EActionBar.TOP_LIST);
            globalCare_1.EventManager.removeEventsByGroup(this.group);
            this.context = undefined;
            this.formControllsData = {};
            this.mandatoryControllsList = [];
            this.exitStatusCombo = undefined;
            this.destinationCombo = undefined;
            this.destinationExternalConsultCombo = undefined;
            this.transfReasonCombo = undefined;
            this.transfReasonExtConsCombo = undefined;
            this.externalServiceExternalConsult = undefined;
            this.transfHealthUnitExtCons = undefined;
            this.deathDateTime = undefined;
            this.deathTypeCombo = undefined;
            this.isDeathConfirmed = undefined;
            this.prescription = undefined;
            this.mechanicalVentilation = undefined;
            this.nosocomialInfection = undefined;
            this.nosocomialInfectionAgent = undefined;
            this.allergies = undefined;
            this.newinternment = undefined;
            this.patientRisk = undefined;
            this.pendingInfoList = [];
            this.selectedPendingInfo = undefined;
            this.obsInput = undefined;
            this.is_form_valid = undefined;
            this.active_validate = undefined;
            this.canSave = undefined;
            this.canSaveConsult = undefined;
            this.accompanimentCombo = undefined;
            this.externalService = undefined;
            this.urgHealthUnit = undefined;
            this.doctorCombo = undefined;
            this.bedCombo = undefined;
            this.speciality = undefined;
            this.service = undefined;
            this.previsionDays = undefined;
            this.foreseenDate = undefined;
            this.options = undefined;
            this.group = undefined;
        };
        GeneralData.prototype.getContext = function () {
            if (this.context == "INT") {
                return {
                    actions: this.actions,
                    data: this.data,
                    group: this.group,
                    options: this.options,
                    context: this.context,
                    formControllsData: this.formControllsData,
                    pending_info: this.pendingInfoList,
                    requestControllsList: this.requestControllsList,
                    mandatoryControllsList: this.mandatoryControllsList,
                    dischargeDateTime: this.dischargeDateTime.getValue(),
                    exitStatusCombo: this.exitStatusCombo.getComboSelected(),
                    destinationCombo: this.destinationCombo.getComboSelected(),
                    transfReasonCombo: this.transfReasonCombo.getComboSelected(),
                    deathDateTime: this.deathDateTime.getValue(),
                    deathTypeCombo: this.deathTypeCombo.getComboSelected(),
                    isDeathConfirmed: this.isDeathConfirmed,
                    prescription: this.prescription.getCheckedValue(),
                    transfHealthUnit: this.transfHealthUnit.getSelectedItem(),
                    externalServiceInt: this.externalServiceInt.getSelectedItem(),
                    btn_confirm: this.btn_confirm,
                    pendingInfoList: this.pendingInfoList,
                    selectedPendingInfo: this.selectedPendingInfo,
                    obsInput: this.obsInput.getValue(),
                    is_form_valid: this.is_form_valid,
                    active_validate: this.active_validate,
                    canSave: this.canSave
                };
            }
            if (this.context == "CONS_EXT") {
                return {
                    actions: this.actions,
                    data: this.data,
                    group: this.group,
                    context: this.context,
                    options: this.options,
                    formControllsData: this.formControllsData,
                    requestControllsList: this.requestControllsList,
                    mandatoryControllsList: this.mandatoryControllsList,
                    dischargeDateTime: this.dischargeDateTime.getValue(),
                    destinationExternalConsultCombo: this.destinationExternalConsultCombo.getComboSelected(),
                    transfReasonExtConsCombo: this.transfReasonExtConsCombo.getComboSelected(),
                    externalServiceExternalConsult: this.externalServiceExternalConsult.getSelectedItem(),
                    transfHealthUnitExtCons: this.transfHealthUnitExtCons.getSelectedItem(),
                    pendingInfoList: this.pendingInfoList,
                    selectedPendingInfo: this.selectedPendingInfo,
                    is_form_valid: this.is_form_valid,
                    active_validate: this.active_validate,
                    canSaveConsult: this.canSaveConsult,
                };
            }
            if (this.context == "URG") {
                return {
                    actions: this.actions,
                    data: this.data,
                    group: this.group,
                    context: this.context,
                    formControllsData: this.formControllsData,
                    mandatoryControllsList: this.mandatoryControllsList,
                    dischargeDateTime: this.dischargeDateTime.getValue(),
                    exitStatusCombo: this.exitStatusCombo.getComboSelected(),
                    destinationCombo: this.destinationCombo.getComboSelected(),
                    transfReasonCombo: this.transfReasonCombo.getComboSelected(),
                    deathDateTime: this.deathDateTime.getValue(),
                    deathTypeCombo: this.deathTypeCombo.getComboSelected(),
                    isDeathConfirmed: this.isDeathConfirmed,
                    btn_confirm: this.btn_confirm,
                    accompanimentCombo: this.accompanimentCombo.getComboSelected(),
                    externalService: this.externalService.getSelectedItem(),
                    urgHealthUnit: this.urgHealthUnit.getSelectedItem(),
                    doctorCombo: this.doctorCombo.getComboSelected(),
                    bedCombo: this.bedCombo.getComboSelected(),
                    speciality: this.speciality.getSelectedItem(),
                    service: this.service.getSelectedItem(),
                    previsionDays: this.previsionDays.getValue(),
                    foreseenDate: this.foreseenDate.getValue(),
                    pendingInfoList: this.pendingInfoList,
                    selectedPendingInfo: this.selectedPendingInfo,
                    is_form_valid: this.is_form_valid,
                    active_validate: this.active_validate,
                    canSaveUrg: this.canSaveUrg,
                };
            }
        };
        GeneralData.prototype.setContext = function (data) {
            if (this.context == "INT") {
                this.options = data.options;
                this.actions = data.actions;
                this.data = data.data;
                this.group = data.group;
                this.context = data.context;
                this.formControllsData = data.formControllsData;
                this.pendingInfoList = data.pending_info;
                this.mandatoryControllsList = data.mandatoryControllsList;
                this.requestControllsList = data.requestControllsList;
                this.initGeneralData(data.requestControllsList);
                if (data.dischargeDateTime)
                    this.dischargeDateTime.setValue({ date: data.dischargeDateTime });
                if (data.exitStatusCombo)
                    (data.exitStatusCombo, false);
                if (data.destinationCombo)
                    this.destinationCombo.setSelectedByCode(data.destinationCombo.LowValue, false);
                if (data.transfReasonCombo)
                    this.transfReasonCombo.setSelectedByCode(data.transfReasonCombo.LowValue, false);
                if (data.deathDateTime)
                    this.deathDateTime.setValue({ date: data.deathDateTime });
                if (data.deathTypeCombo)
                    this.deathTypeCombo.setSelectedByValue(data.deathTypeCombo, "Meaning", false);
                this.isDeathConfirmed = data.isDeathConfirmed;
                this.prescription.setCheckedById(data.prescription, false);
                this.transfHealthUnit.setSelectedItem(data.transfHealthUnit, false);
                this.externalServiceInt.setSelectedItem(data.externalServiceInt, false);
                this.btn_confirm = data.btn_confirm;
                Base_1.Base.getInstance().loadPendingInfoData();
                this.selectedPendingInfo = data.selectedPendingInfo;
                this.obsInput.setValue(data.obsInput);
                this.is_form_valid = data.is_form_valid;
                this.active_validate = data.active_validate;
                this.canSave = data.canSave;
            }
            if (this.context == "CONS_EXT") {
                this.actions = data.actions;
                this.data = data.data;
                this.group = data.group;
                this.context = data.context;
                this.options = data.options;
                this.formControllsData = data.formControllsData;
                this.requestControllsList = data.requestControllsList;
                this.mandatoryControllsList = data.mandatoryControllsList;
                this.initGeneralData(data.requestControllsList);
                if (data.dischargeDateTime)
                    this.dischargeDateTime.setValue({ date: data.dischargeDateTime });
                if (data.destinationExternalConsultCombo)
                    this.destinationExternalConsultCombo.setSelectedByCode(data.destinationExternalConsultCombo.Code, false);
                if (data.transfReasonExtConsCombo)
                    this.transfReasonExtConsCombo.setSelectedByCode(data.transfReasonExtConsCombo.LowValue, false);
                if (data.externalServiceExternalConsult)
                    this.externalServiceExternalConsult.setSelectedItem(data.externalServiceExternalConsult, false);
                if (data.transfHealthUnitExtCons)
                    this.transfHealthUnitExtCons.setSelectedItem(data.transfHealthUnitExtCons, false);
                Base_1.Base.getInstance().loadPendingInfoData();
                this.selectedPendingInfo = data.selectedPendingInfo;
                this.is_form_valid = data.is_form_valid;
                this.active_validate = data.active_validate;
                this.canSaveConsult = data.canSaveConsult;
            }
            if (this.context == "URG") {
                this.actions = data.actions;
                this.data = data.data;
                this.group = data.group;
                this.context = data.context;
                this.formControllsData = data.formControllsData;
                this.requestControllsList = data.requestControllsList;
                this.mandatoryControllsList = data.mandatoryControllsList;
                this.initGeneralData(data.requestControllsList);
                if (data.dischargeDateTime)
                    this.dischargeDateTime.setValue({ date: data.dischargeDateTime });
                if (data.exitStatusCombo)
                    this.exitStatusCombo.setSelectedByCode(data.exitStatusCombo.LowValue, false);
                if (data.destinationCombo)
                    this.destinationCombo.setSelectedByCode(data.destinationCombo.LowValue, false);
                if (data.transfReasonCombo)
                    this.transfReasonCombo.setSelectedByCode(data.transfReasonCombo.LowValue, false);
                if (data.deathDateTime)
                    this.deathDateTime.setValue({ date: data.deathDateTime });
                if (data.deathTypeCombo)
                    this.deathTypeCombo.setSelectedByCode(data.deathTypeCombo.Code, false);
                this.isDeathConfirmed = data.isDeathConfirmed;
                this.btn_confirm = data.btn_confirm;
                if (data.accompanimentCombo)
                    this.accompanimentCombo.setSelectedByCode(data.accompanimentCombo.LowValue, false);
                if (data.externalService)
                    this.externalService.setSelectedItem(data.externalService, false);
                if (data.urgHealthUnit)
                    this.urgHealthUnit.setSelectedItem(data.urgHealthUnit, false);
                if (data.doctorCombo)
                    this.doctorCombo.setSelectedByCode(data.doctorCombo.PersonnelNumber, false);
                if (data.bedCombo)
                    this.bedCombo.setSelectedByCode(data.bedCombo.LowValue, false);
                if (data.speciality)
                    this.speciality.setSelectedItem(data.speciality, false);
                if (data.service)
                    this.service.setSelectedItem(data.service, false);
                if (data.previsionDays)
                    this.previsionDays.setValue(data.previsionDays);
                if (data.foreseenDate)
                    this.foreseenDate.setValue({ date: data.foreseenDate });
                Base_1.Base.getInstance().loadPendingInfoData();
                this.selectedPendingInfo = data.selectedPendingInfo;
                this.is_form_valid = data.is_form_valid;
                this.active_validate = data.active_validate;
                this.canSaveUrg = data.canSaveUrg;
            }
        };
        GeneralData.prototype.setViewMode = function () {
            this.createActionsBar();
            if (this.context == "INT") {
                this.dischargeDateTime.setEditable(false);
                this.destinationCombo.SetComboEnabled(false);
                this.exitStatusCombo.SetComboEnabled(false);
                this.transfReasonCombo.SetComboEnabled(false);
                this.transfHealthUnit.setEditableState(false);
                this.externalServiceInt.setEditableState(false);
                this.deathDateTime.setEditable(false);
                this.deathTypeCombo.SetComboEnabled(false);
                this.prescription.disable();
                this.mechanicalVentilation.disable();
                this.nosocomialInfection.disable();
                this.nosocomialInfectionAgent.setIsEditable(false);
                this.allergies.disable();
                this.newinternment.disable();
                this.patientRisk.setIsEditable(false);
                this.obsInput.setIsEditable(false);
                $("#btn_confirm").attr("disabled", "true");
                $("#btn_cancel").attr("disabled", "true");
                $("#btn_confirm").css("background", "");
                $("#btn_cancel").css("background", "");
                $("#cd_form_destination").closest('.g-form-group').removeClass('required');
                $("#cd_form_destination").closest('.g-form-group').addClass('required-text');
                $("#cd_form_date").closest('.g-form-group').removeClass('required');
                $("#cd_form_date").closest('.g-form-group').addClass('required-text');
                Prolongation_1.Prolongation.getInstance().setViewMode();
            }
            if (this.context == "CONS_EXT") {
                this.dischargeDateTime.setEditable(false);
                this.destinationExternalConsultCombo.SetComboEnabled(false);
                this.transfReasonExtConsCombo.SetComboEnabled(false);
                this.transfHealthUnitExtCons.setEditableState(false);
                this.externalServiceExternalConsult.setEditableState(false);
                if (!!this.obsInput)
                    this.obsInput.setIsEditable(false);
                $("#cd_form_destinationexternalconsult").closest('.g-form-group').removeClass('required');
                $("#cd_form_destinationexternalconsult").closest('.g-form-group').addClass('required-text');
                $("#cd_form_date").closest('.g-form-group').removeClass('required');
                $("#cd_form_date").closest('.g-form-group').addClass('required-text');
            }
            if (this.context == "URG") {
                this.dischargeDateTime.setEditable(false);
                this.destinationCombo.SetComboEnabled(false);
                this.exitStatusCombo.SetComboEnabled(false);
                this.transfReasonCombo.SetComboEnabled(false);
                this.foreseenDate.setEditable(false);
                this.previsionDays.setIsEditable(false);
                this.service.setEditableState(false);
                this.deathDateTime.setEditable(false);
                this.deathTypeCombo.SetComboEnabled(false);
                this.speciality.setEditableState(false);
                this.bedCombo.SetComboEnabled(false);
                this.doctorCombo.SetComboEnabled(false);
                this.urgHealthUnit.setEditableState(false);
                this.externalService.setEditableState(false);
                this.accompanimentCombo.SetComboEnabled(false);
                this.obsInput.setIsEditable(false);
                $("#cd_form_destination").closest('.g-form-group').removeClass('required');
                $("#cd_form_destination").closest('.g-form-group').addClass('required-text');
                $("#cd_form_date").closest('.g-form-group').removeClass('required');
                $("#cd_form_date").closest('.g-form-group').addClass('required-text');
            }
            this.enableControllsButtons();
        };
        GeneralData.prototype.setEditMode = function () {
            this.createActionsBar();
            if (this.context == "INT") {
                this.dischargeDateTime.setEditable(true);
                this.destinationCombo.SetComboEnabled(true);
                this.exitStatusCombo.SetComboEnabled(true);
                this.transfReasonCombo.SetComboEnabled(true);
                this.transfHealthUnit.setEditableState(true);
                this.externalServiceInt.setEditableState(true);
                this.deathDateTime.setEditable(true);
                this.deathTypeCombo.SetComboEnabled(true);
                this.prescription.enable();
                this.mechanicalVentilation.enable();
                this.nosocomialInfection.enable();
                this.nosocomialInfectionAgent.setIsEditable(true);
                this.allergies.enable();
                this.newinternment.enable();
                this.patientRisk.setIsEditable(true);
                this.obsInput.setIsEditable(true);
                $("#btn_confirm").removeAttr("disabled");
                $("#btn_cancel").removeAttr("disabled");
                $("#btn_confirm").css("background", "#BEDB9C");
                $("#btn_cancel").css("background", "#EC7A51");
                $("#cd_form_destination").closest('.g-form-group').addClass('required');
                $("#cd_form_destination").closest('.g-form-group').removeClass('required-text');
                $("#cd_form_date").closest('.g-form-group').addClass('required');
                $("#cd_form_date").closest('.g-form-group').removeClass('required-text');
                Prolongation_1.Prolongation.getInstance().setEditMode();
            }
            if (this.context == "CONS_EXT") {
                this.dischargeDateTime.setEditable(true);
                this.destinationExternalConsultCombo.SetComboEnabled(true);
                this.transfReasonExtConsCombo.SetComboEnabled(true);
                this.transfHealthUnitExtCons.setEditableState(true);
                this.externalServiceExternalConsult.setEditableState(true);
                $("#cd_form_destinationexternalconsult").closest('.g-form-group').addClass('required');
                $("#cd_form_destinationexternalconsult").closest('.g-form-group').removeClass('required-text');
                $("#cd_form_date").closest('.g-form-group').addClass('required');
                $("#cd_form_date").closest('.g-form-group').removeClass('required-text');
                this.refreshForm();
            }
            if (this.context == "URG") {
                this.dischargeDateTime.setEditable(true);
                this.destinationCombo.SetComboEnabled(true);
                this.exitStatusCombo.SetComboEnabled(true);
                this.transfReasonCombo.SetComboEnabled(true);
                this.foreseenDate.setEditable(true);
                this.previsionDays.setIsEditable(true);
                this.service.setEditableState(true);
                this.speciality.setEditableState(true);
                this.deathDateTime.setEditable(true);
                this.deathTypeCombo.SetComboEnabled(true);
                this.bedCombo.SetComboEnabled(true);
                this.doctorCombo.SetComboEnabled(true);
                this.urgHealthUnit.setEditableState(true);
                this.externalService.setEditableState(true);
                this.accompanimentCombo.SetComboEnabled(true);
                this.obsInput.setIsEditable(true);
                $("#btn_confirm").removeAttr("disabled");
                $("#btn_cancel").removeAttr("disabled");
                $("#btn_confirm").css("background", "#BEDB9C");
                $("#btn_cancel").css("background", "#EC7A51");
                $("#cd_form_destination").closest('.g-form-group').addClass('required');
                $("#cd_form_destination").closest('.g-form-group').removeClass('required-text');
                $("#cd_form_date").closest('.g-form-group').addClass('required');
                $("#cd_form_date").closest('.g-form-group').removeClass('required-text');
            }
            this.refreshForm();
        };
        GeneralData.prototype.allowOnlyExitStatus = function () {
            this.disableAllForms(["#cd_form_date", "#cd_form_exitstatus", "#cd_form_generalobservations_input"]);
        };
        GeneralData.prototype.renderGeneralDataForm = function () {
            var showBlock0 = false, showBlock1 = false, showBlock2 = false, showBlock3 = false, showBlock4 = false, showBlock5 = false, showBlock6 = false, showBlock7 = false, showBlock8 = false, showBlock9 = false, showBlock10 = false, showBlock11 = false;
            this.requestControllsList = [];
            this.mandatoryControllsList = [];
            //falta alterar antes checkin
            if (Base_1.Base.getInstance().clinicalDischarge.DeathDateProperties.Visible == true) {
                showBlock1 = true;
                this.requestControllsList.push("DeathDate");
                $('#cd_form_deathdate').closest('.cd_form_element_100').removeClass('hidden');
                if (Base_1.Base.getInstance().clinicalDischarge.DeathDateProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("DeathDate");
                    $('#cd_form_deathdate').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.DateProperties.Visible == true) {
                showBlock0 = true;
                this.requestControllsList.push("Date");
                $('#cd_form_date').closest('.cd_form_element_50').removeClass('hidden');
                if (Base_1.Base.getInstance().clinicalDischarge.DateProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("Date");
                    $('#cd_form_date').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.ExitStatusProperties.Visible == true) {
                showBlock0 = true;
                this.requestControllsList.push("ExitStatus");
                $('#cd_form_exitstatus').closest('.cd_form_element_50').removeClass('hidden');
                if (Base_1.Base.getInstance().clinicalDischarge.ExitStatusProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("ExitStatus");
                    $('#cd_form_exitstatus').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.DestinationProperties.Visible == true) {
                showBlock0 = true;
                this.requestControllsList.push("Destination");
                $('#cd_form_destination').closest('.cd_form_element_50').removeClass('hidden');
                if (Base_1.Base.getInstance().clinicalDischarge.DestinationProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("Destination");
                    $('#cd_form_destination').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsultProperties.Visible == true) {
                showBlock1 = true;
                this.requestControllsList.push("DestinationExternalConsult");
                $('#cd_form_destinationexternalconsult').closest('.cd_form_element_50').removeClass('hidden');
                if (Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsultProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("DestinationExternalConsult");
                    $('#cd_form_destinationexternalconsult').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonIntProperties.Visible == true) {
                showBlock1 = true;
                this.requestControllsList.push("TransferenceReasonInt");
                //$('#cd_form_transferencereasonint').closest('.cd_form_element_50').removeClass('hidden');
                $('#div_cd_form_transferencereasonint').removeClass('hidden');
                if (Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonIntProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("TransferenceReasonInt");
                    $('#cd_form_transferencereasonint').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.HealthUnitIntProperties.Visible == true) {
                showBlock1 = true;
                this.requestControllsList.push("HealthUnitInt");
                $('#cd_form_healthunitint').closest('.cd_form_element_50').removeClass('hidden');
                if (Base_1.Base.getInstance().clinicalDischarge.HealthUnitIntProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("HealthUnitInt");
                    $('#cd_form_healthunitint').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                }
            }
            else {
                $('#div_cd_form_health_unit').addClass('hidden');
            }
            if (Base_1.Base.getInstance().clinicalDischarge.ExternalServiceIntProperties.Visible == true) {
                showBlock1 = true;
                this.requestControllsList.push("ExternalServiceInt");
                $('#cd_form_externalserviceint').closest('.cd_form_element_50').removeClass('hidden');
                if (Base_1.Base.getInstance().clinicalDischarge.ExternalServiceIntProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("ExternalServiceInt");
                    $('#cd_form_externalserviceint').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.DeathTypeProperties.Visible == true) {
                showBlock1 = true;
                this.requestControllsList.push("DeathType");
                $('#cd_form_deathtype').closest('.cd_form_element_50').removeClass('hidden');
                if (Base_1.Base.getInstance().clinicalDischarge.DeathTypeProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("DeathType");
                    $('#cd_form_deathtype').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                }
            }
            else {
                $('#div_deathtype').addClass('hidden');
            }
            if (Base_1.Base.getInstance().clinicalDischarge.PrescriptionReviewProperties.Visible == true) {
                showBlock5 = true;
                if (Base_1.Base.getInstance().clinicalDischarge.PrescriptionReviewProperties.Parameters != undefined || Base_1.Base.getInstance().clinicalDischarge.PrescriptionReviewProperties.Parameters != null) {
                    for (var i = 0; i < Base_1.Base.getInstance().clinicalDischarge.PrescriptionReviewProperties.Parameters.length; i++) {
                        if (Base_1.Base.getInstance().clinicalDischarge.PrescriptionReviewProperties.Parameters[i].Name == "TITLE") {
                            $("#cd_form_prescription_question").html(Base_1.Base.getInstance().clinicalDischarge.PrescriptionReviewProperties.Parameters[i].Value);
                        }
                    }
                }
                this.requestControllsList.push("PrescriptionReview");
                if (Base_1.Base.getInstance().clinicalDischarge.PrescriptionReviewProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("PrescriptionReview");
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.MechanicalVentilationProperties.Visible == true) {
                showBlock6 = true;
                this.requestControllsList.push("MechanicalVentilation");
                if (Base_1.Base.getInstance().clinicalDischarge.MechanicalVentilationProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("MechanicalVentilation");
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.NosocomialInfectionProperties.Visible == true) {
                showBlock7 = true;
                this.requestControllsList.push("NosocomialInfection");
                if (Base_1.Base.getInstance().clinicalDischarge.NosocomialInfectionProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("NosocomialInfection");
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.AllergiesProperties.Visible == true) {
                showBlock8 = true;
                this.requestControllsList.push("Allergies");
                if (Base_1.Base.getInstance().clinicalDischarge.AllergiesProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("Allergies");
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.NewInternmentProperties.Visible == true) {
                showBlock9 = true;
                this.requestControllsList.push("NewInternment");
                if (Base_1.Base.getInstance().clinicalDischarge.NewInternmentProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("NewInternment");
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.PatientRiskProperties.Visible == true) {
                showBlock10 = true;
                this.requestControllsList.push("PatientRisk");
                if (Base_1.Base.getInstance().clinicalDischarge.PatientRiskProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("PatientRisk");
                }
            }
            if (Base_1.Base.getInstance().clinicalDischarge.GeneralObservationsProperties.Visible == true) {
                showBlock11 = true;
                this.requestControllsList.push("GeneralObservations");
                $('#cd_form_generalobservations_input').removeClass('hidden');
                if (Base_1.Base.getInstance().clinicalDischarge.GeneralObservationsProperties.Mandatory == true) {
                    this.mandatoryControllsList.push("GeneralObservations");
                    $('#cd_form_block_observations .cd_form_mandatory').removeClass('hidden');
                }
            }
            switch (this.context) {
                case "INT":
                    break;
                case "URG":
                    if (Base_1.Base.getInstance().clinicalDischarge.IsClinicalDischargeForInternment == "S") {
                        if (Base_1.Base.getInstance().clinicalDischarge.ForeseenDateProperties.Visible == true) {
                            showBlock2 = true;
                            this.requestControllsList.push("ForeseenDate");
                            $('#cd_form_foreseendate').closest('.cd_form_element_50').removeClass('hidden');
                            if (Base_1.Base.getInstance().clinicalDischarge.ForeseenDateProperties.Mandatory == true) {
                                this.mandatoryControllsList.push("ForeseenDate");
                                $('#cd_form_foreseendate').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                            }
                        }
                        if (Base_1.Base.getInstance().clinicalDischarge.PrevisionDaysProperties.Visible == true) {
                            showBlock2 = true;
                            this.requestControllsList.push("PrevisionDays");
                            $('#cd_form_previsiondays').closest('.cd_form_element_50').removeClass('hidden');
                            if (Base_1.Base.getInstance().clinicalDischarge.PrevisionDaysProperties.Mandatory == true) {
                                this.mandatoryControllsList.push("PrevisionDays");
                                $('#cd_form_previsiondays').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                            }
                        }
                        if (Base_1.Base.getInstance().clinicalDischarge.ServiceProperties.Visible == true) {
                            showBlock2 = true;
                            this.requestControllsList.push("Service");
                            $('#cd_form_service').closest('.cd_form_element_50').removeClass('hidden');
                            if (Base_1.Base.getInstance().clinicalDischarge.ServiceProperties.Mandatory == true) {
                                this.mandatoryControllsList.push("Service");
                                $('#cd_form_service').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                            }
                        }
                        if (Base_1.Base.getInstance().clinicalDischarge.SpecialityProperties.Visible == true) {
                            showBlock2 = true;
                            this.requestControllsList.push("Speciality");
                            $('#cd_form_speciality').closest('.cd_form_element_50').removeClass('hidden');
                            if (Base_1.Base.getInstance().clinicalDischarge.SpecialityProperties.Mandatory == true) {
                                this.mandatoryControllsList.push("Speciality");
                                $('#cd_form_speciality').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                            }
                        }
                        if (Base_1.Base.getInstance().clinicalDischarge.BedProperties.Visible == true) {
                            showBlock2 = true;
                            this.requestControllsList.push("Bed");
                            $('#cd_form_bed').closest('.cd_form_element_50').removeClass('hidden');
                            if (Base_1.Base.getInstance().clinicalDischarge.BedProperties.Mandatory == true) {
                                this.mandatoryControllsList.push("Bed");
                                $('#cd_form_bed').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                            }
                        }
                        if (Base_1.Base.getInstance().clinicalDischarge.DoctorProperties.Visible == true) {
                            showBlock2 = true;
                            this.requestControllsList.push("Doctor");
                            $('#cd_form_doctor').closest('.cd_form_element_50').removeClass('hidden');
                            if (Base_1.Base.getInstance().clinicalDischarge.DoctorProperties.Mandatory == true) {
                                this.mandatoryControllsList.push("Doctor");
                                $('#cd_form_doctor').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                            }
                        }
                        if (Base_1.Base.getInstance().clinicalDischarge.ObservationsProperties.Visible == true) {
                            showBlock2 = true;
                            this.requestControllsList.push("Observations");
                            $('#cd_form_observations').closest('.cd_form_element_100').removeClass('hidden');
                            if (Base_1.Base.getInstance().clinicalDischarge.ObservationsProperties.Mandatory == true) {
                                this.mandatoryControllsList.push("Observations");
                                $('#cd_form_mandatory_observations_id').removeClass('hidden');
                            }
                        }
                    }
                    if (Base_1.Base.getInstance().clinicalDischarge.IsClinicalDischargeForTransference == "S") {
                        if (Base_1.Base.getInstance().clinicalDischarge.HealthUnitProperties.Visible == true) {
                            showBlock1 = true;
                            this.requestControllsList.push("HealthUnit");
                            $('#cd_form_healthunit').closest('.cd_form_element_50').removeClass('hidden');
                            if (Base_1.Base.getInstance().clinicalDischarge.HealthUnitProperties.Mandatory == true) {
                                this.mandatoryControllsList.push("HealthUnit");
                                $('#cd_form_healthunit').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                            }
                        }
                        if (Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonProperties.Visible == true) {
                            showBlock1 = true;
                            showBlock3 = true;
                            this.requestControllsList.push("TransferenceReason");
                            $('#cd_form_transferencereason').closest('.cd_form_element_50').removeClass('hidden');
                            if (Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonProperties.Mandatory == true) {
                                this.mandatoryControllsList.push("TransferenceReason");
                                $('#cd_form_transferencereason').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                            }
                        }
                        if (Base_1.Base.getInstance().clinicalDischarge.ExternalServiceProperties.Visible == true) {
                            showBlock3 = true;
                            this.requestControllsList.push("ExternalService");
                            $('#cd_form_externalservice').closest('.cd_form_element_50').removeClass('hidden');
                            if (Base_1.Base.getInstance().clinicalDischarge.ExternalServiceProperties.Mandatory == true) {
                                this.mandatoryControllsList.push("ExternalService");
                                $('#cd_form_externalservice').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                            }
                        }
                        if (Base_1.Base.getInstance().clinicalDischarge.AccompanimentProperties.Visible == true) {
                            showBlock3 = true;
                            this.requestControllsList.push("Accompaniment");
                            $('#cd_form_accompaniment').closest('.cd_form_element_50').removeClass('hidden');
                            if (Base_1.Base.getInstance().clinicalDischarge.AccompanimentProperties.Mandatory == true) {
                                this.mandatoryControllsList.push("Accompaniment");
                                $('#cd_form_accompaniment').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                            }
                        }
                    }
                    break;
                case "CONS_EXT":
                    if (Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonExternalConsultProperties.Visible == true) {
                        showBlock1 = true;
                        this.requestControllsList.push("TransferenceReasonExternalConsult");
                        $('#cd_form_transferencereasonexternalconsult').closest('.cd_form_element_50').removeClass('hidden');
                        if (Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonExternalConsultProperties.Mandatory == true) {
                            this.mandatoryControllsList.push("TransferenceReasonExternalConsult");
                            $('#cd_form_transferencereasonexternalconsult').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                        }
                    }
                    if (Base_1.Base.getInstance().clinicalDischarge.HealthUnitExternalConsultProperties.Visible == true) {
                        showBlock1 = true;
                        this.requestControllsList.push("HealthUnitExternalConsult");
                        $('#cd_form_healthunitexternalconsult').closest('.cd_form_element_50').removeClass('hidden');
                        if (Base_1.Base.getInstance().clinicalDischarge.HealthUnitExternalConsultProperties.Mandatory == true) {
                            this.mandatoryControllsList.push("HealthUnitExternalConsult");
                            $('#cd_form_healthunitexternalconsult').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                        }
                    }
                    if (Base_1.Base.getInstance().clinicalDischarge.ExternalServiceExternalConsultProperties.Visible == true) {
                        showBlock1 = true;
                        this.requestControllsList.push("ExternalServiceExternalConsult");
                        $('#cd_form_externalserviceexternalconsult').closest('.cd_form_element_50').removeClass('hidden');
                        if (Base_1.Base.getInstance().clinicalDischarge.ExternalServiceExternalConsultProperties.Mandatory == true) {
                            this.mandatoryControllsList.push("ExternalServiceExternalConsult");
                            $('#cd_form_externalserviceexternalconsult').closest('.cd_form_element_50').find('.cd_form_mandatory').removeClass('hidden');
                        }
                    }
                    break;
            }
            this.initGeneralData(this.requestControllsList);
            this.checkEpisodeCodif();
            if (showBlock11) {
                var remainingPixels = Number($('#clinicalDischarge_form').height()) - Number($('#cd_form_block0').height()) - Number($('#cd_form_middle_block').height()) - Number($('#cd_form_block_observations .cd_block_title').height()) - 10;
                $('#cd_form_generalobservations_input').css('max-height', remainingPixels);
                $('#cd_form_generalobservations_input').css('resize', 'none');
            }
            if (showBlock0 == true)
                $('#cd_form_block0').removeClass('hidden');
            if (showBlock1 == true) {
                $('#cd_form_block1').removeClass('hidden');
                $('#block').removeClass('hidden');
            }
            if (showBlock5 == true)
                $('#cd_form_block_prescription_review').removeClass('hidden');
            if (showBlock6 == true)
                $('#cd_form_block_mechanical_ventilation').removeClass('hidden');
            if (showBlock7 == true)
                $('#cd_form_block_nosocomial_infection').removeClass('hidden');
            if (showBlock8 == true)
                $('#cd_form_block_allergies').removeClass('hidden');
            if (showBlock9 == true)
                $('#cd_form_block_newinternment').removeClass('hidden');
            if (showBlock10 == true)
                $('#cd_form_block_risk').removeClass('hidden');
            if (showBlock11 == true)
                $('#cd_form_block_observations').removeClass('hidden');
            globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_RENDERGENERALDATAFORM' });
        };
        GeneralData.prototype.createActionsBar = function () {
            var _this = this;
            var list = [];
            if (this.context === "INT") {
                list = [{
                        id: "btn_save",
                        description: i18n.get("CLINICAL_DISCHARGE_GENERIC_GRAVAR"),
                        type: "g-btn-info",
                        size: "g-sm-sm",
                        icon: "g-icon-tools-gravar",
                        //disable: this.context === "INT" ? !this.activeSave : this.activeSave,
                        disable: Base_1.Base.getInstance().clinicalDischarge.HasClinicalDischarge,
                        callback: function () {
                            Base_1.Base.getInstance().saveData();
                        }
                    },
                    {
                        id: "btn_cancel_discharge",
                        description: i18n.get("CLINICAL_DISCHARGE_GENERIC_ANULAR"),
                        type: "g-btn-cancel",
                        size: "g-sm-sm",
                        icon: "g-icon-documentos-doc-eliminar",
                        disable: Base_1.Base.getInstance().clinicalDischarge.HasClinicalDischarge ? false : true,
                        callback: function () {
                            globalCare_1.DialogManager.customDialog2({
                                title: i18n.get("CLINICAL_DISCHARGE_GENERIC_ANULAR"),
                                content: '<div>' + i18n.get("CD_CANCEL_DISCHARGE") + '</div>',
                                height: '150px',
                                width: '400px',
                                dialogClass: 'no-close-button',
                                closeOnEscape: false,
                                id: 'cancelDischargePopup',
                                resizable: false,
                                buttons: [
                                    {
                                        text: i18n.msgStore.PEM_BTN_NO,
                                        icon: "g-icon-tools-x",
                                        class: "g-btn g-sm-md g-btn-cancel",
                                        click: function () {
                                            $("#cancelDischargePopup").dialog("close");
                                        }
                                    },
                                    {
                                        text: i18n.msgStore.PEM_BTN_YES,
                                        icon: "g-icon-tools-visto",
                                        class: "g-btn g-sm-md g-btn-success",
                                        click: function () {
                                            if (Base_1.Base.getInstance().clinicalDischarge.AdministrativeDischargeDate == null) {
                                                //mudar object state para MODIFIED
                                                _this.setObjectState("MODIFIED");
                                                if (_this.context == "URG") {
                                                    Base_1.Base.getInstance().cancelUrgencyDischarge();
                                                }
                                                else {
                                                    Base_1.Base.getInstance().cancelDischarge().then(function () {
                                                        _this.enableControllsButtons();
                                                    });
                                                }
                                            }
                                            else {
                                                globalCare_1.AlarmisticManager.displayAlarmistic({
                                                    AlarmisticType: globalCare_1.AlarmisticType.ERROR_ALARMISTIC,
                                                    Description: i18n.get("CD_ADMIN_DISCH"),
                                                    Title: i18n.get("EPR_DIALOG_ERROR")
                                                });
                                            }
                                            $("#cancelDischargePopup").dialog("close");
                                        }
                                    }
                                ]
                            });
                        }
                    },
                    {
                        id: "btn_discharge",
                        description: i18n.get("CLINICAL_DISCHARGE_GENERIC_FINISH_DISCHARGE"),
                        type: "g-btn-success",
                        size: "g-xl-sm",
                        icon: "g-icon-tools-visto",
                        //disable: this.context === "INT" ? !this.activeSave : this.activeSave,
                        disable: Base_1.Base.getInstance().clinicalDischarge.HasClinicalDischarge,
                        callback: function () {
                            if (_this.activeSave()) {
                                Base_1.Base.getInstance().finishDischarge();
                            }
                            else {
                                var options = {
                                    AlarmisticType: globalCare_1.AlarmisticType.ERROR_ALARMISTIC,
                                    Description: i18n.get("CLINICAL_DISCHARGE_PENDING_ACTIONS_NOT_CONFIRMED"),
                                    Title: i18n.get("EPR_DIALOG_ERROR")
                                };
                                globalCare_1.AlarmisticManager.displayAlarmistic(options);
                            }
                        }
                    }];
            }
            else {
                list = [{
                        id: "btn_cancel_discharge",
                        description: i18n.get("CLINICAL_DISCHARGE_GENERIC_ANULAR"),
                        type: "g-btn-cancel",
                        size: "g-sm-sm",
                        icon: "g-icon-documentos-doc-eliminar",
                        disable: Base_1.Base.getInstance().mode === "EDIT",
                        callback: function () {
                            globalCare_1.DialogManager.customDialog2({
                                title: i18n.get("CLINICAL_DISCHARGE_GENERIC_ANULAR"),
                                content: '<div>' + i18n.get("CD_CANCEL_DISCHARGE") + '</div>',
                                height: '150px',
                                width: '400px',
                                dialogClass: 'no-close-button',
                                closeOnEscape: false,
                                id: 'cancelDischargePopup',
                                resizable: false,
                                buttons: [
                                    {
                                        text: i18n.msgStore.PEM_BTN_NO,
                                        icon: "g-icon-tools-x",
                                        class: "g-btn g-sm-md g-btn-cancel",
                                        click: function () {
                                            $("#cancelDischargePopup").dialog("close");
                                        }
                                    },
                                    {
                                        text: i18n.msgStore.PEM_BTN_YES,
                                        icon: "g-icon-tools-visto",
                                        class: "g-btn g-sm-md g-btn-success",
                                        click: function () {
                                            if (Base_1.Base.getInstance().clinicalDischarge.AdministrativeDischargeDate == null) {
                                                //mudar object state para MODIFIED
                                                _this.setObjectState("MODIFIED");
                                                if (_this.context == "URG") {
                                                    Base_1.Base.getInstance().cancelUrgencyDischarge();
                                                }
                                                else {
                                                    Base_1.Base.getInstance().cancelDischarge();
                                                    _this.canSaveConsult = false;
                                                }
                                            }
                                            else {
                                                var options = {
                                                    AlarmisticType: globalCare_1.AlarmisticType.ERROR_ALARMISTIC,
                                                    Description: i18n.get("EPR_ERROR_DATE_DESTINATION"),
                                                    Title: i18n.get("EPR_DIALOG_ERROR")
                                                };
                                                globalCare_1.AlarmisticManager.displayAlarmistic(options);
                                            }
                                            $("#cancelDischargePopup").dialog("close");
                                        }
                                    }
                                ]
                            });
                        }
                    },
                    {
                        id: "btn_discharge",
                        description: i18n.get("CLINICAL_DISCHARGE_GENERIC_FINISH_DISCHARGE"),
                        type: "g-btn-success",
                        size: "g-xl-sm",
                        icon: "g-icon-tools-visto",
                        disable: Base_1.Base.getInstance().mode === "VIEW",
                        callback: function () {
                            if (_this.activeSave()) {
                                Base_1.Base.getInstance().finishDischarge();
                            }
                            else {
                                var options = {
                                    AlarmisticType: globalCare_1.AlarmisticType.ERROR_ALARMISTIC,
                                    Description: i18n.get("CLINICAL_DISCHARGE_ERROR_DATE_DESTINATION"),
                                    Title: i18n.get("EPR_DIALOG_ERROR")
                                };
                                globalCare_1.AlarmisticManager.displayAlarmistic(options);
                            }
                        }
                    }];
            }
            this.options = {
                actionslist: list
            };
            globalCare_1.MenuManager.UpdateActionsBar(this.options.actionslist, globalCare_1.EActionBar.ACTION_LIST);
            globalCare_1.MenuManager.UpdateActionsBar([], globalCare_1.EActionBar.TOP_LIST);
        };
        GeneralData.prototype.activeSave = function () {
            if (this.canSaveConsult == true && !!this.dischargeDateTime.getValue() ||
                this.canSaveUrg == true && !!this.dischargeDateTime.getValue() ||
                this.context == "INT" && !!this.dischargeDateTime.getValue()) {
                return true;
            }
            else {
                return false;
            }
        };
        GeneralData.prototype.refreshForm = function () {
            if (Base_1.Base.getInstance().clinicalDischarge.EpisodeStartDate != null && Base_1.Base.getInstance().clinicalDischarge.EpisodeStartDate != undefined && moment(Base_1.Base.getInstance().clinicalDischarge.EpisodeStartDate).isValid() == true) {
                Base_1.Base.getInstance().clinicalDischarge.EpisodeStartDate = moment(Base_1.Base.getInstance().clinicalDischarge.EpisodeStartDate).toDate();
            }
            this.renderGeneralDataForm();
        };
        GeneralData.prototype.hideControlls = function (flag) {
            if (this.context == "INT") {
                $('#div_cd_form_exitstatus').addClass('hidden');
                $("#cd_form_block1").addClass('hidden');
                $("#cd_form_block_prescription_review").addClass('hidden');
                $("#cd_form_block_mechanical_ventilation").addClass('hidden');
                $("#cd_form_block_nosocomial_infection").addClass('hidden');
                $("#cd_form_block_allergies").addClass('hidden');
                $("#cd_form_block_newinternment").addClass('hidden');
                $("#cd_form_block_risk").addClass('hidden');
                $("#cd_form_block_observations").addClass('hidden');
                $("#div_cd_form_health_unit").addClass('hidden');
                $("#div_cd_form_external_service").addClass('hidden');
                $("#cd_form_block_observations").addClass('hidden');
                $("#div_cd_form_transferencereasonint").addClass('hidden');
            }
            if (this.context == "CONS_EXT") {
                $('#div_cd_form_exitstatus').addClass('hidden');
                $("#cd_form_block1").addClass('hidden');
                $("#div_cd_form_transferencereasonconsult").addClass('hidden');
                $("#div_cd_form_health_unit_consult").addClass('hidden');
                $("#div_cd_form_external_service_consult").addClass('hidden');
            }
            if (this.context == "URG") {
                $('#div_cd_form_exitstatus').addClass('hidden');
                $("#cd_form_block1").addClass('hidden');
                $("#block").addClass('hidden');
                $("#div_foreseenDate").addClass('hidden');
                $("#div_previsionDays").addClass('hidden');
                $("#div_cd_form_service").addClass('hidden');
                $("#div_cd_form_speciality").addClass('hidden');
                $("#div_cd_form_bed").addClass('hidden');
                $("#div_cd_form_health_unit").addClass('hidden');
                $("#div_cd_form_doctor").addClass('hidden');
                $("#div_cd_form_urg_healthunit").addClass('hidden');
                $("#div_cd_form_transferencereasonint").addClass('hidden');
                $("#div_cd_form_externalservice").addClass('hidden');
                $("#div_cd_form_accompaniment").addClass('hidden');
                $("#cd_form_block_observations").addClass('hidden');
            }
        };
        GeneralData.prototype.enableControllsButtons = function () {
            if (Base_1.Base.getInstance().clinicalDischarge != undefined) {
                this.is_form_valid = this.validateMandatoryFields();
                this.checkDadosGerais(this.is_form_valid);
                this.active_validate = this.validateInfoFaltaMandatoryFields();
                if (!this.ExistsClinicalDischarge({ ClinicalDischarge: Base_1.Base.getInstance().clinicalDischarge })) {
                    //se houver alteracoes pendentes
                    if (Base_1.Base.getInstance().hasPendingChanges()) {
                        this.activeSaveDischarge(true);
                        if (this.active_validate && this.is_form_valid) {
                            this.activeConfirmedDischarge(true);
                        }
                        else {
                            this.activeConfirmedDischarge(false);
                        }
                    }
                    else {
                        this.activeSaveDischarge(false);
                        this.activeConfirmedDischarge(false);
                    }
                }
                else {
                    this.activeSaveDischarge(false);
                    this.activeConfirmedDischarge(false);
                    if (Base_1.Base.getInstance().clinicalDischarge.IsEpisodeCodif) {
                        $('#cd_controls_cancel_btn').attr('disabled', "true");
                    }
                    else {
                        $('#cd_controls_cancel_btn').removeAttr('disabled');
                    }
                }
            }
        };
        GeneralData.prototype.validateInfoFaltaMandatoryFields = function () {
            for (var i = 0; i < this.pendingInfoList.length; i++) {
                if (this.pendingInfoList[i].Required == true && this.pendingInfoList[i].checked != true) {
                    return false;
                }
            }
            return true;
        };
        GeneralData.prototype.checkDadosGerais = function (check) {
            for (var i = 0; i < this.pendingInfoList.length; i++) {
                if (this.pendingInfoList[i].Type == 0) {
                    this.pendingInfoList[i].checked = check;
                    $('#clinicalDischarge_pending_info_list').data('GList').refreshItem(this.pendingInfoList[i].row_id, this.pendingInfoList[i], this.pendingInfoList[i]);
                    break;
                }
            }
        };
        GeneralData.prototype.disableAllForms = function (exceptionsArray) {
            //------------------------------Block0
            if ($.inArray('#cd_form_date', exceptionsArray) == -1 && this.dischargeDateTime != undefined) {
                this.dischargeDateTime.disable();
            }
            if ($.inArray('#cd_form_exitstatus', exceptionsArray) == -1 && this.exitStatusCombo != undefined) {
                this.exitStatusCombo.disable();
            }
            if ($.inArray('#cd_form_destination', exceptionsArray) == -1 && this.destinationCombo != undefined) {
                this.destinationCombo.disable();
            }
            if ($.inArray('#cd_form_destinationexternalconsult', exceptionsArray) == -1 && this.destinationExternalConsultCombo != undefined) {
                this.destinationExternalConsultCombo.disable();
            }
            //------------------------------Block1
            if ($.inArray('#cd_form_transferencereasonint', exceptionsArray) == -1 && this.transfReasonCombo != undefined) {
                this.transfReasonCombo.disable();
            }
            if ($.inArray('#cd_form_healthunitint', exceptionsArray) == -1 && this.transfHealthUnit != undefined) {
                this.transfHealthUnit.disable();
            }
            if ($.inArray('#cd_form_externalserviceint', exceptionsArray) == -1 && this.externalServiceInt != undefined) {
                this.externalServiceInt.disable();
            }
            if ($.inArray('#cd_form_deathdate', exceptionsArray) == -1 && this.deathDateTime != undefined) {
                this.deathDateTime.disable();
            }
            if ($.inArray('#cd_form_deathtype', exceptionsArray) == -1 && this.deathTypeCombo != undefined) {
                this.deathTypeCombo.disable();
            }
            //------------------------------Block2
            if ($.inArray('#cd_form_foreseendate', exceptionsArray) == -1 && this.foreseenDate != undefined) {
                this.foreseenDate.disable();
            }
            if ($.inArray('#cd_form_previsiondays', exceptionsArray) == -1 && this.previsionDays != undefined) {
                this.previsionDays.disable();
            }
            if ($.inArray('#cd_form_service', exceptionsArray) == -1 && this.service != undefined) {
                this.service.disable();
            }
            if ($.inArray('#cd_form_speciality', exceptionsArray) == -1 && this.speciality != undefined) {
                this.speciality.disable();
            }
            if ($.inArray('#cd_form_bed', exceptionsArray) == -1 && this.bedCombo != undefined) {
                this.bedCombo.disable();
            }
            if ($.inArray('#cd_form_doctor', exceptionsArray) == -1 && this.doctorCombo != undefined) {
                this.doctorCombo.disable();
            }
            if ($.inArray('#cd_form_observations', exceptionsArray) == -1 && $('#cd_form_observations').data('GlinttTextBox') != undefined) {
                $('#cd_form_observations').data('GlinttTextBox').setIsEditable(false);
            }
            //------------------------------Block3
            if ($.inArray('#cd_form_healthunit', exceptionsArray) == -1 && $('#cd_form_healthunit').data('GSearcher') != undefined) {
                $('#cd_form_healthunit').data('GSearcher').setEditableState(false);
            }
            if ($.inArray('#cd_form_transferencereason', exceptionsArray) == -1 && $('#cd_form_transferencereason').data('GlinttCombo') != undefined) {
                $('#cd_form_transferencereason').data('GlinttCombo').SetComboEnabled(false);
                $('#cd_form_transferencereason').addClass('toDisable');
            }
            if ($.inArray('#cd_form_externalservice', exceptionsArray) == -1 && this.externalService != undefined) {
                this.externalService.disable();
            }
            if ($.inArray('#cd_form_accompaniment', exceptionsArray) == -1 && this.accompanimentCombo != undefined) {
                this.accompanimentCombo.disable();
            }
            //------------------------------Block4
            if ($.inArray('#cd_form_transferencereasonexternalconsult', exceptionsArray) == -1 && this.transfReasonExtConsCombo != undefined) {
                this.transfReasonExtConsCombo.disable();
            }
            if ($.inArray('#cd_form_healthunitexternalconsult', exceptionsArray) == -1 && this.transfHealthUnitExtCons != undefined) {
                this.transfHealthUnitExtCons.disable();
            }
            if ($.inArray('#cd_form_externalserviceexternalconsult', exceptionsArray) == -1 && this.externalServiceExternalConsult != undefined) {
                this.externalServiceExternalConsult.disable();
            }
            //------------------------------Block5
            if ($.inArray('#cd_form_prescription', exceptionsArray) == -1 && this.prescription != undefined) {
                this.prescription.disable();
            }
            //------------------------------Block6
            if ($.inArray('#cd_form_mechanical_ventilation', exceptionsArray) == -1 && this.mechanicalVentilation != undefined) {
                this.mechanicalVentilation.disable();
            }
            //------------------------------Block7
            if ($.inArray('#cd_form_nosocomial_infection', exceptionsArray) == -1 && this.nosocomialInfection != undefined) {
                this.nosocomialInfection.disable();
                this.nosocomialInfectionAgent.disable();
            }
            //------------------------------Block8
            if ($.inArray('#cd_form_allergies', exceptionsArray) == -1 && this.allergies != undefined) {
                this.allergies.disable();
            }
            //------------------------------Block9
            if ($.inArray('#cd_form_newinternment', exceptionsArray) == -1 && this.newinternment != undefined) {
                this.newinternment.disable();
            }
            //------------------------------Block10
            if ($.inArray('#cd_form_risk', exceptionsArray) == -1 && this.patientRisk != undefined) {
                this.patientRisk.disable();
            }
            //------------------------------Block11
            if ($.inArray('#cd_form_generalobservations_input', exceptionsArray) == -1 && $('#cd_form_generalobservations_input').data('GlinttTextArea') != undefined) {
                $('#cd_form_generalobservations_input').data('GlinttTextArea').setIsEditable(false);
            }
        };
        GeneralData.prototype.setObjectState = function (param) {
            Base_1.Base.getInstance().clinicalDischarge.ObjectState = param;
            if (Base_1.Base.getInstance().clinicalDischarge.ObjectState == "MODIFIED") {
                Base_1.Base.getInstance().setPendingChanges(true);
                this.enableControllsButtons();
            }
        };
        ;
        GeneralData.prototype.deathDateConfirm = function () {
            if (Base_1.Base.getInstance().clinicalDischarge.DeathDateProperties != undefined && Base_1.Base.getInstance().clinicalDischarge.DeathDateProperties != null && Base_1.Base.getInstance().clinicalDischarge.DeathDateProperties.Visible == true) {
                return this.isDeathConfirmed;
            }
            else {
                return true;
            }
        };
        GeneralData.prototype.setDestination = function (param) {
            Base_1.Base.getInstance().clinicalDischarge.Destination = param;
            this.setObjectState("MODIFIED");
        };
        ;
        GeneralData.prototype.ExistsClinicalDischarge = function (params) {
            if (this.context == "URG")
                return params.ClinicalDischarge.HasClinicalDischargeAuthorization || params.ClinicalDischarge.HasClinicalDischarge;
            else
                return params.ClinicalDischarge.HasClinicalDischarge;
        };
        GeneralData.prototype.setTransferenceReason = function (param) {
            if (this.context == "URG") {
                Base_1.Base.getInstance().clinicalDischarge.TransferenceReason = param;
            }
            else {
                Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonInt = param;
            }
            this.setObjectState("MODIFIED");
        };
        ;
        GeneralData.prototype.setTransferenceReasonExtCons = function (param) {
            Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonExternalConsult = param;
            this.setObjectState("MODIFIED");
        };
        ;
        GeneralData.prototype.setDeathType = function (param) {
            Base_1.Base.getInstance().clinicalDischarge.DeathType = param;
            this.setObjectState("MODIFIED");
        };
        ;
        GeneralData.prototype.validateMandatoryFields = function () {
            this.cleanMandatoryFields();
            if (this.mandatoryControllsList.length != 0) {
                var valid = true;
                for (var i = 0; i < this.mandatoryControllsList.length; i++) {
                    switch (this.mandatoryControllsList[i]) {
                        //block0
                        case "Date":
                            if (Base_1.Base.getInstance().clinicalDischarge.Date == null) {
                                valid = false;
                            }
                            break;
                        case "ExitStatus":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.ExitStatus) {
                                $("#cd_form_exitstatus").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_exitstatus").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_exitstatus").closest('.g-form-group').removeClass('required');
                                $("#cd_form_exitstatus").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "Destination":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.Destination) {
                                $("#cd_form_destination").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_destination").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_destination").closest('.g-form-group').removeClass('required');
                                $("#cd_form_destination").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "DestinationExternalConsult":
                            if (Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsult == null) {
                                $("#cd_form_destinationexternalconsult").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_destinationexternalconsult").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_destinationexternalconsult").closest('.g-form-group').removeClass('required');
                                $("#cd_form_destinationexternalconsult").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        //block1
                        case "TransferenceReasonInt":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonInt) {
                                $("#cd_form_transferencereasonint").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_transferencereasonint").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_transferencereasonint").closest('.g-form-group').removeClass('required');
                                $("#cd_form_transferencereasonint").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "HealthUnitInt":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.HealthUnitInt) {
                                $("#cd_form_healthunitint").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_healthunitint").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_healthunitint").closest('.g-form-group').removeClass('required');
                                $("#cd_form_healthunitint").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "ExternalServiceInt":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.ExternalServiceInt) {
                                $("#cd_form_externalserviceint").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_externalserviceint").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_externalserviceint").closest('.g-form-group').removeClass('required');
                                $("#cd_form_externalserviceint").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "DeathDate":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.DeathDate) {
                                $("#cd_form_deathdate").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_deathdate").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_deathdate").closest('.g-form-group').removeClass('required');
                                $("#cd_form_deathdate").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "DeathType":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.DeathType) {
                                $("#cd_form_deathtype").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_deathtype").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_deathtype").closest('.g-form-group').removeClass('required');
                                $("#cd_form_deathtype").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        //block2
                        case "ForeseenDate":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.ForeseenDate) {
                                $("#cd_form_foreseenDate").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_foreseenDate").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_foreseenDate").closest('.g-form-group').removeClass('required');
                                $("#cd_form_foreseenDate").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "PrevisionDays":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.PrevisionDays) {
                                $("#cd_form_previsionDays").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_previsionDays").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_previsionDays").closest('.g-form-group').removeClass('required');
                                $("#cd_form_previsionDays").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "Service":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.Service) {
                                $("#cd_form_service").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_service").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_service").closest('.g-form-group').removeClass('required');
                                $("#cd_form_service").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "Speciality":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.Speciality) {
                                $("#cd_form_speciality").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_speciality").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_speciality").closest('.g-form-group').removeClass('required');
                                $("#cd_form_speciality").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "Bed":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.Bed) {
                                $("#cd_form_bed").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_bed").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_bed").closest('.g-form-group').removeClass('required');
                                $("#cd_form_bed").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "Doctor":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.Doctor) {
                                $("#cd_form_doctor").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_doctor").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_doctor").closest('.g-form-group').removeClass('required');
                                $("#cd_form_doctor").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "Observations":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.Observations) {
                                $("#cd_form_generalobservations_input").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_generalobservations_input").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_generalobservations_input").closest('.g-form-group').removeClass('required');
                                $("#cd_form_generalobservations_input").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        //block3
                        case "HealthUnit":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.HealthUnit) {
                                $("#cd_form_urg_healthunit").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_urg_healthunit").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_urg_healthunit").closest('.g-form-group').removeClass('required');
                                $("#cd_form_urg_healthunit").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "TransferenceReason":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.TransferenceReason) {
                                $("#cd_form_transferencereasonint").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_transferencereasonint").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_transferencereasonint").closest('.g-form-group').removeClass('required');
                                $("#cd_form_transferencereasonint").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "ExternalService":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.ExternalService) {
                                $("#cd_form_externalservice").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_externalservice").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_externalservice").closest('.g-form-group').removeClass('required');
                                $("#cd_form_externalservice").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "Accompaniment":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.Accompaniment) {
                                $("#cd_form_accompaniment").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_accompaniment").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_accompaniment").closest('.g-form-group').removeClass('required');
                                $("#cd_form_accompaniment").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        //block4
                        case "TransferenceReasonExternalConsult":
                            if (Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonExternalConsult == null) {
                                $("#cd_form_transferencereasonexternalconsult").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_transferencereasonexternalconsult").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_transferencereasonexternalconsult").closest('.g-form-group').removeClass('required');
                                $("#cd_form_transferencereasonexternalconsult").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "HealthUnitExternalConsult":
                            if (Base_1.Base.getInstance().clinicalDischarge.HealthUnitExternalConsult == null) {
                                $("#cd_form_healthunitexternalconsult").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_healthunitexternalconsult").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_healthunitexternalconsult").closest('.g-form-group').removeClass('required');
                                $("#cd_form_healthunitexternalconsult").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        case "ExternalServiceExternalConsult":
                            if (Base_1.Base.getInstance().clinicalDischarge.ExternalServiceExternalConsult == null) {
                                $("#div_cd_form_external_service_consult").closest('.g-form-group').removeClass('required-text');
                                $("#div_cd_form_external_service_consult").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#div_cd_form_external_service_consult").closest('.g-form-group').removeClass('required');
                                $("#div_cd_form_external_service_consult").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        //block5
                        case "PrescriptionReview":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.PrescriptionReview) {
                                $("#cd_form_prescription").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_prescription").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_prescription").closest('.g-form-group').removeClass('required');
                                $("#cd_form_prescription").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        //block6
                        case "MechanicalVentilation":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.MechanicalVentilation) {
                                $("#cd_form_mechanical_ventilation").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_mechanical_ventilation").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_mechanical_ventilation").closest('.g-form-group').removeClass('required');
                                $("#cd_form_mechanical_ventilation").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        //block7
                        case "NosocomialInfection":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.NosocomialInfection) {
                                $("#cd_form_nosocomial_infection").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_nosocomial_infection").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_nosocomial_infection").closest('.g-form-group').removeClass('required');
                                $("#cd_form_nosocomial_infection").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        //block8
                        case "Allergies":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.Allergies) {
                                $("#cd_form_allergies").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_allergies").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_allergies").closest('.g-form-group').removeClass('required');
                                $("#cd_form_allergies").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        //block9
                        case "NewInternment":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.NewInternment) {
                                $("#cd_form_newinternment").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_newinternment").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_newinternment").closest('.g-form-group').removeClass('required');
                                $("#cd_form_newinternment").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        //block10
                        case "PatientRisk":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.PatientRisk) {
                                $("#cd_form_risk").closest('.g-form-group').removeClass('required-text');
                                $("#cd_form_risk").closest('.g-form-group').addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_risk").closest('.g-form-group').removeClass('required');
                                $("#cd_form_risk").closest('.g-form-group').addClass('required-text');
                            }
                            break;
                        //block11
                        case "GeneralObservations":
                            if (!!!Base_1.Base.getInstance().clinicalDischarge.GeneralObservations) {
                                $("#cd_form_generalobservations_input").removeClass('required-text');
                                $("#cd_form_generalobservations_input").addClass('required');
                                valid = false;
                            }
                            else {
                                $("#cd_form_generalobservations_input").removeClass('required');
                                $("#cd_form_generalobservations_input").addClass('required-text');
                            }
                            break;
                    }
                }
                if (valid) {
                    return true;
                }
                else {
                    return false;
                }
            }
            return true;
        };
        GeneralData.prototype.cleanMandatoryFields = function () {
            $("#cd_form_exitstatus").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_exitstatus").closest('.g-form-group').removeClass('required');
            $("#cd_form_destination").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_destination").closest('.g-form-group').removeClass('required');
            $("#cd_form_transferencereasonint").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_transferencereasonint").closest('.g-form-group').removeClass('required');
            $("#cd_form_healthunitint").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_healthunitint").closest('.g-form-group').removeClass('required');
            $("#cd_form_prescription").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_prescription").closest('.g-form-group').removeClass('required');
            $("#cd_form_mechanical_ventilation").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_mechanical_ventilation").closest('.g-form-group').removeClass('required');
            $("#cd_form_nosocomial_infection").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_nosocomial_infection").closest('.g-form-group').removeClass('required');
            $("#cd_form_allergies").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_allergies").closest('.g-form-group').removeClass('required');
            $("#cd_form_newinternment").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_newinternment").closest('.g-form-group').removeClass('required');
            $("#cd_form_risk").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_risk").closest('.g-form-group').removeClass('required');
            $("#cd_form_generalobservations_input").removeClass('required-text');
            $("#cd_form_generalobservations_input").removeClass('required');
            $("#cd_form_externalserviceint").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_externalserviceint").closest('.g-form-group').removeClass('required');
            $("#cd_form_transferencereasonexternalconsult").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_transferencereasonexternalconsult").closest('.g-form-group').removeClass('required');
            $("#cd_form_healthunitexternalconsult").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_healthunitexternalconsult").closest('.g-form-group').removeClass('required');
            $("#cd_form_externalserviceexternalconsult").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_externalserviceexternalconsult").closest('.g-form-group').removeClass('required');
            $("#cd_form_externalservice").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_externalservice").closest('.g-form-group').removeClass('required');
            $("#cd_form_urg_healthunit").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_urg_healthunit").closest('.g-form-group').removeClass('required');
            $("#cd_form_doctor").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_doctor").closest('.g-form-group').removeClass('required');
            $("#cd_form_bed").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_bed").closest('.g-form-group').removeClass('required');
            $("#cd_form_speciality").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_speciality").closest('.g-form-group').removeClass('required');
            $("#cd_form_service").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_service").closest('.g-form-group').removeClass('required');
            $("#cd_form_previsionDays").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_previsionDays").closest('.g-form-group').removeClass('required');
            $("#cd_form_foreseenDate").closest('.g-form-group').removeClass('required-text');
            $("#cd_form_foreseenDate").closest('.g-form-group').removeClass('required');
        };
        GeneralData.prototype.requestTransferenceReason = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                globalCare_1.ProcessingManager.start({ id: 'EPR_CLINICALDISCHARGE_REQUESTTRANSFERENCEREASON' });
                if ((_this.context == "INT" && _this.formControllsData.TransferenceReasonInt != undefined) ||
                    (_this.context == "URG" && _this.formControllsData.TransferenceReason != undefined)) {
                    if (_this.context == "INT") {
                        _this.transfReasonCombo.setDataSource(_this.formControllsData.TransferenceReasonInt);
                    }
                    else {
                        _this.transfReasonCombo.setDataSource(_this.formControllsData.TransferenceReason);
                    }
                    _this.fillRequestTransferenceReasonIntCombo();
                    globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_REQUESTTRANSFERENCEREASON' });
                    resolve();
                }
                else {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "EPR_CLINICALDISCHARGE_REQUESTTRANSFERENCEREASON",
                        type: "POST",
                        url: _this.actions.GetTransferenceReasonURL,
                        data: {
                            context: _this.context
                        },
                        cancellable: false
                    }).then(function (data) {
                        if (_this.context == "INT") {
                            _this.formControllsData.TransferenceReasonInt = data.result;
                            _this.transfReasonCombo.setDataSource(_this.formControllsData.TransferenceReasonInt);
                        }
                        else {
                            _this.formControllsData.TransferenceReason = data.result;
                            _this.transfReasonCombo.setDataSource(_this.formControllsData.TransferenceReason);
                        }
                        _this.fillRequestTransferenceReasonIntCombo();
                        globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_REQUESTTRANSFERENCEREASON' });
                        resolve();
                    });
                }
            });
        };
        GeneralData.prototype.fillRequestTransferenceReasonIntCombo = function () {
            if (this.context == "INT" && Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonInt != null) {
                this.transfReasonCombo.setSelectedByCode(Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonInt, true);
            }
            else {
                this.transfReasonCombo.clearSelection();
            }
            if (this.context == "URG" && Base_1.Base.getInstance().clinicalDischarge.TransferenceReason != null) {
                this.transfReasonCombo.setSelectedByCode(Base_1.Base.getInstance().clinicalDischarge.TransferenceReason, true);
            }
            else {
                this.transfReasonCombo.clearSelection();
            }
        };
        GeneralData.prototype.setHealthUnitInt = function (param) {
            Base_1.Base.getInstance().clinicalDischarge.HealthUnitInt = param;
            this.setObjectState("MODIFIED");
        };
        ;
        GeneralData.prototype.setHealthUnitExtCons = function (param) {
            Base_1.Base.getInstance().clinicalDischarge.HealthUnitExternalConsult = param;
            this.setObjectState("MODIFIED");
        };
        ;
        GeneralData.prototype.setExternalServiceInt = function (param) {
            Base_1.Base.getInstance().clinicalDischarge.ExternalServiceInt = param;
            this.setObjectState("MODIFIED");
        };
        ;
        GeneralData.prototype.setDeathDate = function (param, changeDischarge) {
            Base_1.Base.getInstance().clinicalDischarge.DeathDate = param;
            if (changeDischarge != false) {
                this.setObjectState("MODIFIED");
                if (param == null) {
                    return;
                }
                else {
                    $("#cd_form_deathdate").closest('.g-form-group').removeClass('required');
                    $("#cd_form_deathdate").closest('.g-form-group').addClass('required-text');
                }
            }
            else {
                $("#cd_form_deathdate").closest('.g-form-group').addClass('required');
                $("#cd_form_deathdate").closest('.g-form-group').removeClass('required-text');
            }
        };
        ;
        GeneralData.prototype.setDate = function (param, changeDischarge) {
            Base_1.Base.getInstance().clinicalDischarge.Date = param;
            if (changeDischarge != false) {
                this.setObjectState("MODIFIED");
                if (param == null) {
                    return;
                }
                else {
                    $("#cd_form_date").closest('.g-form-group').removeClass('required');
                    $("#cd_form_date").closest('.g-form-group').addClass('required-text');
                }
            }
            else {
                $("#cd_form_date").closest('.g-form-group').addClass('required');
                $("#cd_form_date").closest('.g-form-group').removeClass('required-text');
            }
        };
        ;
        GeneralData.prototype.setGeneralObservations = function (param) {
            Base_1.Base.getInstance().clinicalDischarge.GeneralObservations = param;
            this.setObjectState("MODIFIED");
        };
        ;
        GeneralData.prototype.setPrescriptionReview = function (param) {
            if (param != null) {
                Base_1.Base.getInstance().clinicalDischarge.PrescriptionReview = param;
                this.setObjectState("MODIFIED");
            }
        };
        ;
        GeneralData.prototype.setMechanicalVentilation = function (param) {
            if (param != null) {
                Base_1.Base.getInstance().clinicalDischarge.MechanicalVentilation = param;
                this.setObjectState("MODIFIED");
            }
        };
        ;
        GeneralData.prototype.setNosocomialInfection = function (param) {
            if (param != null) {
                Base_1.Base.getInstance().clinicalDischarge.NosocomialInfection = param;
                this.setObjectState("MODIFIED");
            }
        };
        ;
        GeneralData.prototype.setNosocomialInfectionAgent = function (param) {
            if (param != null) {
                Base_1.Base.getInstance().clinicalDischarge.NosocomialInfectionAgent = param;
                this.setObjectState("MODIFIED");
            }
        };
        ;
        GeneralData.prototype.setAllergies = function (param) {
            if (param != null) {
                Base_1.Base.getInstance().clinicalDischarge.Allergies = param;
                this.setObjectState("MODIFIED");
            }
        };
        ;
        GeneralData.prototype.setNewInternment = function (param) {
            if (param != null) {
                Base_1.Base.getInstance().clinicalDischarge.NewInternment = param;
                this.setObjectState("MODIFIED");
            }
        };
        ;
        GeneralData.prototype.setPatientRisk = function (param) {
            if (param != null) {
                Base_1.Base.getInstance().clinicalDischarge.PatientRisk = param;
                this.setObjectState("MODIFIED");
            }
        };
        ;
        GeneralData.prototype.setDestinationExternalConsult = function (param) {
            $("#cd_form_destinationexternalconsult").closest('.g-form-group').removeClass('required');
            $("#cd_form_destinationexternalconsult").closest('.g-form-group').addClass('required-text');
            Base_1.Base.getInstance().clinicalDischarge.DestinationExternalConsult = param;
            this.setObjectState("MODIFIED");
        };
        ;
        GeneralData.prototype.setExternalServiceExternalConsult = function (param) {
            Base_1.Base.getInstance().clinicalDischarge.ExternalServiceExternalConsult = param;
            this.setObjectState("MODIFIED");
        };
        ;
        GeneralData.prototype.requestDoctor = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                //TT - verificar parametros de envio, perguntar a alguem
                _super.prototype.serviceRequest.call(_this, {
                    id: "EPR_CLINICALDISCHARGE_REQUESTDOCTOR",
                    type: "POST",
                    url: _this.actions.GetDoctorURL,
                    data: {
                        privilege: "ESP",
                        specialtyId: null
                    },
                    cancellable: false
                }).then(function (data) {
                    _this.formControllsData.Doctor = data.result;
                    _this.doctorCombo.setDataSource(_this.formControllsData.Doctor);
                    _this.fillRequestDoctorCombo();
                    resolve();
                });
            });
        };
        GeneralData.prototype.fillRequestDoctorCombo = function () {
            if (Base_1.Base.getInstance().clinicalDischarge.Doctor != null) {
                this.doctorCombo.setSelectedByCode(Base_1.Base.getInstance().clinicalDischarge.Doctor, true);
            }
            else {
                this.doctorCombo.clearSelection();
            }
        };
        GeneralData.prototype.requestTransferenceReasonExternalConsult = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                globalCare_1.ProcessingManager.start({ id: 'EPR_CLINICALDISCHARGE_REQUESTTRANSFREASONEXTLCONSULT' });
                if (_this.formControllsData.TransferenceReasonExternalConsult != undefined) {
                    _this.transfReasonExtConsCombo.setDataSource(_this.formControllsData.TransferenceReasonExternalConsult);
                    _this.fillRequestTransferenceReasonExternalConsult();
                    globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_REQUESTTRANSFREASONEXTLCONSULT' });
                    resolve();
                }
                else {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "EPR_CLINICALDISCHARGE_REQUESTTRANSFREASONEXTLCONSULT",
                        type: "POST",
                        url: _this.actions.GetTransferenceReasonURL,
                        data: {
                            context: _this.context
                        },
                        cancellable: false
                    }).then(function (data) {
                        _this.formControllsData.TransferenceReasonExternalConsult = data.result;
                        _this.transfReasonExtConsCombo.setDataSource(_this.formControllsData.TransferenceReasonExternalConsult);
                        _this.fillRequestTransferenceReasonExternalConsult();
                        globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_REQUESTTRANSFREASONEXTLCONSULT' });
                        resolve();
                    });
                }
            });
        };
        GeneralData.prototype.fillRequestTransferenceReasonExternalConsult = function () {
            if (Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonExternalConsult != null) {
                this.transfReasonExtConsCombo.setSelectedByCode(Base_1.Base.getInstance().clinicalDischarge.TransferenceReasonExternalConsult, true);
            }
            else {
                this.transfReasonExtConsCombo.clearSelection();
            }
            if (!$('#cd_form_transferencereasonexternalconsult').hasClass('toDisable'))
                this.transfReasonExtConsCombo.SetComboEnabled(true);
        };
        GeneralData.prototype.requestBed = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                _super.prototype.serviceRequest.call(_this, {
                    id: "EPR_CLINICALDISCHARGE_REQUESTBED",
                    type: "POST",
                    url: _this.actions.GetBedURL,
                    data: {
                        serviceId: null
                    },
                    cancellable: false
                }).then(function (data) {
                    if (!!data.result) {
                        _this.formControllsData.Bed = data.result;
                        _this.bedCombo.setDataSource(_this.formControllsData.Bed);
                        _this.fillRequestBedCombo();
                    }
                    resolve();
                });
            });
        };
        GeneralData.prototype.fillRequestBedCombo = function () {
            if (Base_1.Base.getInstance().clinicalDischarge.Bed != null) {
                this.bedCombo.setSelectedByCode(Base_1.Base.getInstance().clinicalDischarge.Bed, true);
            }
            else {
                this.bedCombo.clearSelection();
            }
            if (!$('#cd_form_bed').hasClass('toDisable'))
                this.bedCombo.SetComboEnabled(true);
        };
        GeneralData.prototype.requestAccompaniment = function () {
            var _this = this;
            return new Promise(function (resolve, reject) {
                globalCare_1.ProcessingManager.start({ id: 'EPR_CLINICALDISCHARGE_REQUESTACCOMPANIMENT' });
                if (_this.formControllsData.Accompaniment != undefined) {
                    _this.accompanimentCombo.setDataSource(_this.formControllsData.Accompaniment);
                    _this.fillAccompanimentCombo();
                    globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_REQUESTACCOMPANIMENT' });
                    resolve();
                }
                else {
                    _super.prototype.serviceRequest.call(_this, {
                        id: "EPR_CLINICALDISCHARGE_REQUESTACCOMPANIMENT",
                        type: "POST",
                        url: _this.actions.GetAccompanimentURL,
                        cancellable: false
                    }).then(function (data) {
                        _this.formControllsData.Accompaniment = data.result;
                        _this.accompanimentCombo.setDataSource(_this.formControllsData.Accompaniment);
                        _this.fillAccompanimentCombo();
                        globalCare_1.ProcessingManager.stop({ id: 'EPR_CLINICALDISCHARGE_REQUESTACCOMPANIMENT' });
                        resolve();
                    });
                }
            });
        };
        GeneralData.prototype.fillAccompanimentCombo = function () {
            if (Base_1.Base.getInstance().clinicalDischarge.Accompaniment != null) {
                this.accompanimentCombo.setSelectedByCode(Base_1.Base.getInstance().clinicalDischarge.Accompaniment, true);
            }
            else {
                this.accompanimentCombo.clearSelection();
            }
            if (!$('#cd_form_accompaniment').hasClass('toDisable') && Base_1.Base.getInstance().clinicalDischarge.HasClinicalDischarge)
                this.accompanimentCombo.SetComboEnabled(true);
        };
        GeneralData._instance = new GeneralData();
        return GeneralData;
    }(globalCare_1.BaseView));
    exports.GeneralData = GeneralData;
});
