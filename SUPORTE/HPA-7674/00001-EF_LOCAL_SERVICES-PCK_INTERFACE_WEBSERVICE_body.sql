CREATE OR REPLACE 
PACKAGE BODY pck_interface_webservice IS
    --|*************************************************************************
    --|*************************************************************************
    FUNCTION argV(i_ARG VARCHAR2) RETURN VARCHAR2   IS BEGIN RETURN           args_in(i_ARG).valor                  ; EXCEPTION WHEN NO_DATA_FOUND THEN RETURN to_CHAR  (NULL); END;
    FUNCTION argN(i_ARG VARCHAR2) RETURN NUMBER     IS BEGIN RETURN to_NUMBER(args_in(i_ARG).valor                 ); EXCEPTION WHEN NO_DATA_FOUND THEN RETURN to_NUMBER(NULL); END;
    FUNCTION argD(i_ARG VARCHAR2) RETURN DATE       IS BEGIN RETURN to_DATE  (args_in(i_ARG).valor,DATE_FORMAT_MASK); EXCEPTION WHEN NO_DATA_FOUND THEN RETURN to_DATE  (NULL); END;
    FUNCTION argD2(i_ARG VARCHAR2) RETURN DATE       IS BEGIN RETURN to_DATE  (args_in(i_ARG).valor,DATE_FORMAT_MASK_WITH_SS); EXCEPTION WHEN NO_DATA_FOUND THEN RETURN to_DATE  (NULL); END;

    --|*************************************************************************
    --|*************************************************************************
    FUNCTION convert_JSON2LIST(i_ARGS CLOB) RETURN tp_LIST_OF_ARGUMENT
    IS
        args_LIST tp_LIST_OF_ARGUMENT;
        json_GLOBAL             d_json;
        json_LIST_OF_KEYS       d_json_list;
        json_LIST_OF_KEYS_count NUMBER;
        json_KEY                VARCHAR2(30);
        i NUMBER := 1;
        tmp_valor varchar2(32000);

        dummy_V VARCHAR2(4000);
        dummy_N NUMBER;
        dummy_D DATE;
    BEGIN

        --| Criar o tipo JSON com a estrutura que vinha do CLOB
        json_GLOBAL := d_json(i_ARGS);
        --| Obter a lista das COLUNAS com altera��es
        json_LIST_OF_KEYS       := d_json.get_KEYS(json_GLOBAL);
        json_LIST_OF_KEYS_count := d_json_list.count(json_LIST_OF_KEYS);

        LOOP

            IF i > json_LIST_OF_KEYS_count THEN EXIT; END IF;

            --| Obter o NOME do argumento
            json_KEY := json_LIST_OF_KEYS.get(i).get_string;
            --| Obter o VALOR e o TIPO do argumento
            tmp_valor                   := d_json_ext.get_json_value(json_GLOBAL,json_KEY||'.value').get_string;
            args_LIST(json_KEY).VALOR   := replace(replace(replace(tmp_valor, '\\n','\n'), '\\r','\r'), '\\t', '\t');
            args_LIST(json_KEY).TIPO    := d_json_ext.get_json_value(json_GLOBAL,json_KEY||'.type').get_string;

            --| Garantir que os argumentos s�o v�lidos
            IF args_LIST(json_KEY).TIPO = 'NUMBER'
            THEN
                BEGIN
                    dummy_N := to_NUMBER(args_LIST(json_KEY).VALOR);
                EXCEPTION WHEN OTHERS THEN
                    err_.DELETE;
                    err_(1) := json_KEY;
                    err_(2) := args_LIST(json_KEY).VALOR;
                raise_application_error (-20001,'Argumento n�o � um n�mero v�lido.');
                END;
            ELSIF args_LIST(json_KEY).TIPO = 'DATE'
            THEN
                BEGIN
                    dummy_D := to_DATE( args_LIST(json_KEY).VALOR , DATE_FORMAT_MASK_WITH_SS );
                EXCEPTION WHEN OTHERS THEN
                    err_.DELETE;
                    err_(1) := json_KEY;
                    err_(2) := args_LIST(json_KEY).VALOR;
                    raise_application_error (-20001,'Argumento n�o � uma data v�lida');
                END;
           /* ELSIF args_LIST(json_KEY).TIPO = 'DATE_SS'
            THEN
                BEGIN
                    dummy_D := to_DATE( args_LIST(json_KEY).VALOR , 'YYYY-MM-DD HH24:MI:SS' );
                EXCEPTION WHEN OTHERS THEN
                    err_.DELETE;
                    err_(1) := json_KEY;
                    err_(2) := args_LIST(json_KEY).VALOR;
                    raise_application_error (-20001,'Argumento n�o � uma data v�lida (com segundos)');
                END;*/
            END IF;

            i:=i+1;

        END LOOP;

        --| Retorna o ARRAY com os argumentos indexados pelo NOME do argumento
        RETURN args_LIST;

    EXCEPTION WHEN OTHERS THEN
        insere('ERRO','PCK_ENF_INTERFACE_WEBSERVICES.convert_JSON2LIST',SQLERRM,'[i_ARGS='||i_ARGS||']');
        RAISE;
        RETURN args_LIST;
    END;

    --|*************************************************************************
    --|*************************************************************************
    PROCEDURE process_ROW( i_ROW IN OUT SD_INTERFACE_WEBSERVICE%ROWTYPE )
    IS
        p_result VARCHAR2(100);
    BEGIN

        --| Descodificar a lista de argumentos JSON recebida
        args_in := convert_JSON2LIST(i_ROW.ARGS_IN);

         IF i_ROW.AMBITO = 'ALTA_CLINICA' AND i_ROW.FUNCIONALIDADE = 'STARTINPATIENTSO'
        THEN

            pck_local_activity.StartInpatientSO(
                p_t_episodio=> argV('T_EPISODIO'  ),
                p_episodio=> argV('EPISODIO'  ),
                p_n_mecan=> argV('NMECAN'  ),
                p_t_doente=> argV('T_DOENTE'  ),
                p_doente=> argV('DOENTE'  ),
                p_service=> argV('SERV'  ),
                p_usersys=> argV('UTILIZADOR'  ),
                p_result=> p_result);
        ELSIF i_ROW.AMBITO = 'ALTA_CLINICA' AND i_ROW.FUNCIONALIDADE = 'ENDINPATIENTSO'
        THEN

            pck_local_activity.EndInpatientSO(
                p_t_episodio=> argV('T_EPISODIO'  ),
                p_episodio=> argV('EPISODIO'  ),
                p_n_mecan=> argV('NMECAN'  ),
                p_t_doente=> argV('T_DOENTE'  ),
                p_doente=> argV('DOENTE'  ),
                p_service=> argV('SERV'  ),
                p_usersys=> argV('UTILIZADOR'  ),
                p_result=> p_result);
        ELSIF  i_ROW.AMBITO = 'ALTA_CLINICA' AND i_ROW.FUNCIONALIDADE = 'UPDATEINTERNMENTINFORMATION'
        THEN
           pck_local_discharge.updateinpatientschedule(
                p_cod_u_saude_d=> argV('p_cod_u_saude_d'  ),
                p_dest_pos_alta=> argV('p_dest_pos_alta'  ),
                p_estado_saida=> argV('p_estado_saida'  ),
                p_motivo_transf_saida=> argV('p_motivo_transf_saida'  ),
                p_n_mecan_alta=> argV('p_n_mecan_alta'  ),
                p_observ=> argV('p_observ'  ),
                p_dt_alta_clinica=> argD2('p_dt_alta_clinica'  ),
                p_hr_alta_clinica=> argD2('p_hr_alta_clinica'  ),
                p_cod_espec_ext=> argV('p_cod_espec_ext'  ),
                p_tipo_obito=> argV('p_tipo_obito'  ),
                pf_n_int=> argV('pf_n_int'  ),
                p_user_act=> argV('p_user_act'  ),
                p_flg_reinter=> argV('p_flg_reintern'  ),
                p_anula=> argV('p_anula'  ),
                p_revisao_terapeutica=> argV('p_revisao_terapeutica'  ),
                p_ventilacao_mecanica=> argV('p_ventilacao_mecanica'  ),
                p_infecao_nosocomial=> argV('p_infecao_nosocomial'  ),
                p_agente_etiologico=> argV('p_agente_etiologico'  ),
                p_alergias=> argV('p_alergias'  ),
                p_reinternamento=> argV('p_reinternamento'  ),
                p_doente_risco=> argV('p_doente_risco'  )  );

        ELSIF i_ROW.AMBITO = 'ALTA_CLINICA' AND i_ROW.FUNCIONALIDADE = 'REGISTEREXTCONSULTDISCHARGE'
        THEN
            pck_local_discharge.RegisterExtConsultDischarge(
                p_t_doente=> argV('p_t_doente'  ),
                p_doente=> argV('p_doente'  ),
                p_n_cons=> argV('p_n_cons'  ),
                p_cod_serv=> argV('p_cod_serv'  ),
                p_t_act_med=> argV('p_t_act_med'  ),
                p_dt_alta=> argD2('p_dt_alta'  ),
                p_n_mecan_alta=> argV('p_n_mecan_alta'  ),
                p_episodio_ini=> argV('p_episodio_ini'  ),
                p_cod_destino=> argV('p_cod_destino'  ),
                p_unid_saude_dest=> argV('p_unid_saude_dest'  ),
                p_motivo_saida=> argV('p_motivo_saida'  ),
                p_cod_espec_ext=> argV('p_cod_espec_ext'  ),
                p_flg_clin=> argV('p_flg_clin'  ));
        ELSIF i_ROW.AMBITO = 'ALTA_CLINICA' AND i_ROW.FUNCIONALIDADE = 'CANCELEXTERNALCONSULTDISCHARGE'
        THEN
            pck_local_discharge.CancelExternalConsultDischarge(
                p_flag_anulada=> argV('p_flag_anulada'  ),
                p_t_doente=> argV('p_t_doente'  ),
                p_doente=> argV('p_doente'  ),
                p_cod_serv=> argV('p_cod_serv'  ),
                p_t_act_med=> argV('p_t_act_med'  ),
                p_n_cons=> argV('p_n_cons'  ),
                p_user_act=> argV('p_user_act'  ));
        ELSIF i_ROW.AMBITO = 'ALTA_CLINICA' AND i_ROW.FUNCIONALIDADE = 'ADDSURGICALPROPOSAL'
        THEN
            pck_local_surgery.AddSurgicalProposal(
                p_t_doente=> argV('p_t_doente'  ),
                p_doente=> argV('p_doente'  ),
                p_cod_serv=> argV('p_cod_serv'  ),
                p_n_mec=> argV('p_n_mec'  ),
                p_cod_c_saude=> argV('p_cod_c_saude'  ),
                p_cod_serv_req=> argV('p_cod_serv_req'  ),
                p_n_mec_req=> argV('p_n_mec_req'  ),
                p_dt_ped_inter=> argD('p_dt_ped_inter'  ),
                p_dt_prev_inter=> argD('p_dt_prev_inter'  ),
                p_reason=> argV('p_reason'  ),
                p_flag_cirurgia=> argV('p_flag_cirurgia'  ),
                p_prioridade=> argV('p_prioridade'  ),
                p_obs=> argV('p_obs'  ),
                p_dias_prev_inter=> argV('p_dias_prev_inter'  ),
                p_dt_registo=> argD('p_dt_registo'  ),
                p_estado=> argV('p_estado'  ),
                p_t_episodio_pai=> argV('p_t_episodio_pai'  ),
                p_episodio_pai=> argV('p_episodio_pai'  ),
                p_flag_carta=> argV('p_flag_carta'  ),
                p_cod_equipa=> argV('p_cod_equipa'  ),
                p_regime_inte=> argV('p_regime_inte'  ),
                p_cod_serv_int=> argV('p_cod_serv_int'  ),
                p_flg_uci=> argV('p_flg_uci'  ),
                p_flg_recobro=> argV('p_flg_recobro'  ),
                p_flag_amb_int=> argV('p_flag_amb_int'  ),
                p_cod_tecn_cir=> argV('p_cod_tecn_cir'  ),
                p_tipo_anestesia=> argV('p_tipo_anestesia'  ),
                p_dur_oper=> argV('p_dur_oper'  ),
                p_previsao_oper=> argD('p_previsao_oper'  ),
                p_cod_bloco=> argV('p_cod_bloco'  ),
                p_dt_consent_doe=> argD('p_dt_consent_doe'  ),
                p_flg_proc_sub=> argV('p_flg_proc_sub'  ),
                p_cod_mot_proc_ante=> argV('p_cod_mot_proc_ante'  ),
                p_cod_asa=> argV('p_cod_asa'  ),
                p_flg_aval_pre_oper=> argV('p_flg_aval_pre_oper'  ),
                p_cod_lateralidade=> argV('p_cod_lateralidade'  ),
                p_flg_nec_sup_peri_oper=> argV('p_flg_nec_sup_peri_oper'  ),
                p_flg_diag_assoc=> argV('p_flg_diag_assoc'  ),
                p_obs_nivel_prioridade=> argV('p_obs_nivel_prioridade'  ),
                p_obs_peri_oper=> argV('p_obs_peri_oper'  ),
                p_obs_diag_assoc=> argV('p_obs_diag_assoc'  ),
                p_obs_tipo_episodio=> argV('p_obs_tipo_episodio'  ),
                p_medicacao=> argV('p_medicacao'  ),
                p_sector=> argV('p_sector'  ),
                p_flg_cirurgico=> argV('p_flg_cirurgico'  ),
                p_user_cri=> argV('p_user_cri'  ),
                p_flg_plano_continuidade=> argV('p_flg_plano_continuidade'  ),
                p_motivo_pedido=> argV('p_motivo_pedido'  ),
                p_mot_alt_dt_prev_inter=> argV('p_mot_alt_dt_prev_inter'  ),
                p_chave_obj_ext=> argN('p_chave_obj_ext'  ),
                p_hr_prev_inter=> argD('p_hr_prev_inter'  ),
                p_hr_prev_alta_int=> argD('p_hr_prev_alta_int'  ),
                p_dt_pretendida=> argD('p_dt_pretendida'  ),
                p_cod_enf=> argV('p_cod_enf'  ),
                p_cod_cama=> argV('p_cod_cama'  ),
                p_cod_sala=> argV('p_cod_sala'  ),
                p_tipo_diaria=> argN('p_tipo_diaria'  ),
                p_id_tipos_lista_espera=> argN('p_id_tipos_lista_espera'  ),
                p_dt_avaliacao=> argD('p_dt_avaliacao'  ),
                p_cod_classif=> argV('p_cod_classif'  ),
                p_n_ped_inte=> argN('p_n_ped_inte'  ));
        ELSIF i_ROW.AMBITO = 'ALTA_CLINICA' AND i_ROW.FUNCIONALIDADE = 'UPDATEPATIENTDEATHINFORMATION'
        THEN

            pck_local_patient.UpdatePatientDeathInformation(
                i_t_doente=> argV('i_t_doente'),
                i_doente=> argV('i_doente'),
                i_t_episodio=> argV('i_t_episodio'),
                i_episodio=> argV('i_episodio'  ),
                i_dt_alta_episodio=> argD('i_dt_alta_episodio'),
                i_dt_verif_obito=> argD('i_dt_verif_obito'),
                i_n_decl_obito=> argV('i_n_decl_obito'),
                i_autopsia=> argV('i_autopsia'),
                i_t_autopsia=> argV('i_t_autopsia'),
                i_utilizador_reg=> argV('i_utilizador_reg'),
                i_modo => argV('i_modo'),
                o_msg=> p_result);
        END IF;

        i_ROW.ESTADO := nvl(i_ROW.ESTADO,'OK');

    EXCEPTION WHEN OTHERS THEN
        i_ROW.ESTADO := SQLERRM;
        insere('ERRO','pck_interface_webservice.process_ROW',SQLERRM,'[AMBITO='||i_ROW.AMBITO||'][FUNCIONALIDADE='||i_ROW.FUNCIONALIDADE||'][USER_CRI='||i_ROW.USER_CRI||'][ARGS_IN='||i_ROW.ARGS_IN||']');
        RAISE;
    END;

    --|*************************************************************************
    --|*************************************************************************
    PROCEDURE process_ROW( i_ID NUMBER )
    IS
        row_INTERFACE_WEBSERVICE SD_INTERFACE_WEBSERVICE%ROWTYPE;
    BEGIN

        --| Obter a linha
        SELECT *
          INTO row_INTERFACE_WEBSERVICE
          FROM SD_INTERFACE_WEBSERVICE
         WHERE ID = i_ID;

        --| Process�-la
        process_ROW(row_INTERFACE_WEBSERVICE);

        UPDATE SD_INTERFACE_WEBSERVICE
           SET ARGS_OUT = row_INTERFACE_WEBSERVICE.ARGS_OUT
              ,ESTADO   = row_INTERFACE_WEBSERVICE.ESTADO
         WHERE ID       = i_ID;

    END;

    --|*************************************************************************
    --|*************************************************************************
    PROCEDURE insere_LOG_ERR
    (
        i_ID                NUMBER,
        i_AMBITO            VARCHAR2,
        i_FUNCIONALIDADE    VARCHAR2,
        i_ARGS_IN           CLOB,
        i_ESTADO            VARCHAR2,
        i_USER_CRI          VARCHAR2,
        i_DT_CRI            DATE,
        i_GVS_AUDSID        NUMBER,
        i_GVS_CLIENT_INFO   VARCHAR2
    ) IS PRAGMA AUTONOMOUS_TRANSACTION;
    BEGIN

        INSERT INTO SD_INTERFACE_WEBSERVICE_ER
        (ID,AMBITO,FUNCIONALIDADE,ARGS_IN,ESTADO,USER_CRI,DT_CRI,GVS_AUDSID,GVS_CLIENT_INFO)
        VALUES
        (i_ID,i_AMBITO,i_FUNCIONALIDADE,i_ARGS_IN,i_ESTADO,i_USER_CRI,i_DT_CRI,i_GVS_AUDSID,i_GVS_CLIENT_INFO);

        COMMIT;

    EXCEPTION WHEN OTHERS THEN
        ROLLBACK;
        insere('ERRO','PCK_ENF_INTERFACE_WEBSERVICE.insere_LOG_ERR',SQLERRM,'');
    END;

    PROCEDURE insere (i_tipo VARCHAR2, i_origem VARCHAR2, i_msg VARCHAR2, i_args VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      INSERT INTO sd_interface_ws_log
                  (tipo, origem, msg, args)
           VALUES (i_tipo, i_origem, i_msg, i_args);

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN
         NULL;
   END;
END;
/
