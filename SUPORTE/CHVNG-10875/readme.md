# CHVNG-10875

## Suporte

### Descrição

BEDSIDE: O filtro do serviço requesitante não está a filtrar

### Mensagem Check-In

CHVNG-10875
TODO

### Changesets

|       TFS      |   DEV    |    CI    |   17R1   |
|---------------:|:--------:|:--------:|:--------:|
| **Changesets** |  xxxxxx  |  xxxxxx  |  xxxxxx  |
|                |    --    |    --    |    --    |

|       SVN      |  HSDEV  |  DEMOQ  | DEMOPRIV |
|---------------:|:-------:|:-------:|:--------:|
|  **Revision**  |  xxxxx  |  xxxxx  |  xxxxxx  |
|                |   ---   |    --   |    --    |

### Notas

TODO

### Backend Configs

#### Controller Frontend URL

<http://demoapliis.glinttocm.com:6002/EPR_MOBILE/Glintths.Shell/xxxx/yyyy.svc>

#### Chave GINF_CONFIG

| key | value |
|:---:|:-----:|
|"S_GINF_CONFIG_KEY"|```"#BASE_URL#pathtofile.svc"```|

#### Projecto Backend

##### Cpchs.Modules.ActivitiesManagement.WebServices

+ [x] **IIS** Services/Glintths.WebServices
+ [x] **Lógica** Services/Glintths.BusinessRules
+ [x] **Data Model** Services/Glintths.BusinessEntities

#### Configurações

N/A

### Backend Lógica

#### C\#

TODO (changeset tree w/ changes to code)

#### DB

+ SCHEMA
  + PACKAGE
    + procedure

### Notas Frontend

N/A
